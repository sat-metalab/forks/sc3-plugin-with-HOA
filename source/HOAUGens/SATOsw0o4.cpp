/* ------------------------------------------------------------
author: "AmbisonicDecoderToolkit"
copyright: "(c) Aaron J. Heller 2013"
license: "BSD 3-Clause License"
name: "SATOsw0o4"
version: "1.2"
Code generated with Faust 2.5.12 (https://faust.grame.fr)
Compilation options: cpp, -scal -ftz 0
------------------------------------------------------------ */

#ifndef  __mydsp_H__
#define  __mydsp_H__

//-------------------------------------------------------------------
// FAUST architecture file for SuperCollider.
// Copyright (C) 2005-2012 Stefan Kersten.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//-------------------------------------------------------------------

// If other than 'faust2sc --prefix Faust' is used, sed this as well:
#if !defined(SC_FAUST_PREFIX)
#define SC_FAUST_PREFIX "Faust"
#endif

#include <map>
#include <string>
#include <string.h>
#include <SC_PlugIn.h>

/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __dsp__
#define __dsp__

#include <string>

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

class UI;
struct Meta;

/**
 * DSP memory manager.
 */

struct dsp_memory_manager {
    
    virtual ~dsp_memory_manager() {}
    
    virtual void* allocate(size_t size) = 0;
    virtual void destroy(void* ptr) = 0;
    
};

/**
* Signal processor definition.
*/

class dsp {

    public:

        dsp() {}
        virtual ~dsp() {}

        /* Return instance number of audio inputs */
        virtual int getNumInputs() = 0;
    
        /* Return instance number of audio outputs */
        virtual int getNumOutputs() = 0;
    
        /**
         * Trigger the ui_interface parameter with instance specific calls
         * to 'addBtton', 'addVerticalSlider'... in order to build the UI.
         *
         * @param ui_interface - the user interface builder
         */
        virtual void buildUserInterface(UI* ui_interface) = 0;
    
        /* Returns the sample rate currently used by the instance */
        virtual int getSampleRate() = 0;
    
        /**
         * Global init, calls the following methods:
         * - static class 'classInit': static tables initialization
         * - 'instanceInit': constants and instance state initialization
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void init(int samplingRate) = 0;

        /**
         * Init instance state
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void instanceInit(int samplingRate) = 0;

        /**
         * Init instance constant state
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void instanceConstants(int samplingRate) = 0;
    
        /* Init default control parameters values */
        virtual void instanceResetUserInterface() = 0;
    
        /* Init instance state (delay lines...) */
        virtual void instanceClear() = 0;
 
        /**
         * Return a clone of the instance.
         *
         * @return a copy of the instance on success, otherwise a null pointer.
         */
        virtual dsp* clone() = 0;
    
        /**
         * Trigger the Meta* parameter with instance specific calls to 'declare' (key, value) metadata.
         *
         * @param m - the Meta* meta user
         */
        virtual void metadata(Meta* m) = 0;
    
        /**
         * DSP instance computation, to be called with successive in/out audio buffers.
         *
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         *
         */
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) = 0;
    
        /**
         * DSP instance computation: alternative method to be used by subclasses.
         *
         * @param date_usec - the timestamp in microsec given by audio driver.
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         *
         */
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { compute(count, inputs, outputs); }
       
};

/**
 * Generic DSP decorator.
 */

class decorator_dsp : public dsp {

    protected:

        dsp* fDSP;

    public:

        decorator_dsp(dsp* dsp = 0):fDSP(dsp) {}
        virtual ~decorator_dsp() { delete fDSP; }

        virtual int getNumInputs() { return fDSP->getNumInputs(); }
        virtual int getNumOutputs() { return fDSP->getNumOutputs(); }
        virtual void buildUserInterface(UI* ui_interface) { fDSP->buildUserInterface(ui_interface); }
        virtual int getSampleRate() { return fDSP->getSampleRate(); }
        virtual void init(int samplingRate) { fDSP->init(samplingRate); }
        virtual void instanceInit(int samplingRate) { fDSP->instanceInit(samplingRate); }
        virtual void instanceConstants(int samplingRate) { fDSP->instanceConstants(samplingRate); }
        virtual void instanceResetUserInterface() { fDSP->instanceResetUserInterface(); }
        virtual void instanceClear() { fDSP->instanceClear(); }
        virtual decorator_dsp* clone() { return new decorator_dsp(fDSP->clone()); }
        virtual void metadata(Meta* m) { fDSP->metadata(m); }
        // Beware: subclasses usually have to overload the two 'compute' methods
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(count, inputs, outputs); }
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(date_usec, count, inputs, outputs); }
    
};

/**
 * DSP factory class.
 */

class dsp_factory {
    
    protected:
    
        // So that to force sub-classes to use deleteDSPFactory(dsp_factory* factory);
        virtual ~dsp_factory() {}
    
    public:
    
        virtual std::string getName() = 0;
        virtual std::string getSHAKey() = 0;
        virtual std::string getDSPCode() = 0;
    
        virtual dsp* createDSPInstance() = 0;
    
        virtual void setMemoryManager(dsp_memory_manager* manager) = 0;
        virtual dsp_memory_manager* getMemoryManager() = 0;
    
};

/**
 * On Intel set FZ (Flush to Zero) and DAZ (Denormals Are Zero)
 * flags to avoid costly denormals.
 */

#ifdef __SSE__
    #include <xmmintrin.h>
    #ifdef __SSE2__
        #define AVOIDDENORMALS _mm_setcsr(_mm_getcsr() | 0x8040)
    #else
        #define AVOIDDENORMALS _mm_setcsr(_mm_getcsr() | 0x8000)
    #endif
#else
    #define AVOIDDENORMALS
#endif

#endif
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __UI_H__
#define __UI_H__

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

/*******************************************************************************
 * UI : Faust DSP User Interface
 * User Interface as expected by the buildUserInterface() method of a DSP.
 * This abstract class contains only the method that the Faust compiler can
 * generate to describe a DSP user interface.
 ******************************************************************************/

struct Soundfile;

class UI
{

    public:

        UI() {}

        virtual ~UI() {}

        // -- widget's layouts

        virtual void openTabBox(const char* label) = 0;
        virtual void openHorizontalBox(const char* label) = 0;
        virtual void openVerticalBox(const char* label) = 0;
        virtual void closeBox() = 0;

        // -- active widgets

        virtual void addButton(const char* label, FAUSTFLOAT* zone) = 0;
        virtual void addCheckButton(const char* label, FAUSTFLOAT* zone) = 0;
        virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;
        virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;
        virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;

        // -- passive widgets

        virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) = 0;
        virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) = 0;
    
        // -- soundfiles
    
        virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) = 0;

        // -- metadata declarations

        virtual void declare(FAUSTFLOAT*, const char*, const char*) {}
};

#endif
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/
 
#ifndef __misc__
#define __misc__

#include <algorithm>
#include <map>
#include <string.h>
#include <stdlib.h>

/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __meta__
#define __meta__

struct Meta
{
    virtual void declare(const char* key, const char* value) = 0;
    virtual ~Meta() {};
};

#endif

using std::max;
using std::min;

struct XXXX_Meta : std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key]=value; }
};

struct MY_Meta : Meta, std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key]=value; }
};

inline int lsr(int x, int n)	{ return int(((unsigned int)x) >> n); }

inline int int2pow2(int x)		{ int r = 0; while ((1<<r) < x) r++; return r; }

inline long lopt(char* argv[], const char* name, long def)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return atoi(argv[i+1]);
	return def;
}

inline bool isopt(char* argv[], const char* name)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return true;
	return false;
}

inline const char* lopts(char* argv[], const char* name, const char* def)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return argv[i+1];
	return def;
}

#endif


using namespace std;

#if defined(__GNUC__) && __GNUC__ >= 4
    #define FAUST_EXPORT __attribute__((visibility("default")))
#else
    #define FAUST_EXPORT  SC_API_EXPORT
#endif

#ifdef WIN32
    #define STRDUP _strdup
#else
    #define STRDUP strdup
#endif

//----------------------------------------------------------------------------
// Vector intrinsics
//----------------------------------------------------------------------------


//----------------------------------------------------------------------------
// Metadata
//----------------------------------------------------------------------------

class MetaData : public Meta
               , public std::map<std::string, std::string>
{
public:
    void declare(const char* key, const char* value)
    {
        (*this)[key] = value;
    }
};

//----------------------------------------------------------------------------
// Control counter
//----------------------------------------------------------------------------

class ControlCounter : public UI
{
public:
    ControlCounter()
        : mNumControlInputs(0),
          mNumControlOutputs(0)
    {}

    size_t getNumControls() const { return getNumControlInputs(); }
    size_t getNumControlInputs() const { return mNumControlInputs; }
    size_t getNumControlOutputs() const { return mNumControlOutputs; }

    // Layout widgets
    virtual void openTabBox(const char* label) { }
    virtual void openHorizontalBox(const char* label) { }
    virtual void openVerticalBox(const char* label) { }
    virtual void closeBox() { }

    // Active widgets
    virtual void addButton(const char* label, FAUSTFLOAT* zone)
    { addControlInput(); }
    virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
    { addControlInput(); }
    virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }
    virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }
    virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }

    // Passive widgets
    virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
    { addControlOutput(); }
    virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
    { addControlOutput(); }
    
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}

protected:
    void addControlInput() { mNumControlInputs++; }
    void addControlOutput() { mNumControlOutputs++; }

private:
    size_t mNumControlInputs;
    size_t mNumControlOutputs;
};

//----------------------------------------------------------------------------
// UI control
//----------------------------------------------------------------------------

struct Control
{
    typedef void (*UpdateFunction)(Control* self, FAUSTFLOAT value);

    UpdateFunction updateFunction;
    FAUSTFLOAT* zone;
    FAUSTFLOAT min, max;

    inline void update(FAUSTFLOAT value)
    {
        (*updateFunction)(this, value);
    }

    static void simpleUpdate(Control* self, FAUSTFLOAT value)
    {
        *self->zone = value;
    }
    static void boundedUpdate(Control* self, FAUSTFLOAT value)
    {
        *self->zone = sc_clip(value, self->min, self->max);
    }
};

//----------------------------------------------------------------------------
// Control allocator
//----------------------------------------------------------------------------

class ControlAllocator : public UI
{
public:
    ControlAllocator(Control* controls)
        : mControls(controls)
    { }

    // Layout widgets
    virtual void openTabBox(const char* label) { }
    virtual void openHorizontalBox(const char* label) { }
    virtual void openVerticalBox(const char* label) { }
    virtual void closeBox() { }

    // Active widgets
    virtual void addButton(const char* label, FAUSTFLOAT* zone)
    { addSimpleControl(zone); }
    virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
    { addSimpleControl(zone); }
    virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }
    virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }
    virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }

    // Passive widgets
    virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
    virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}

private:
    void addControl(Control::UpdateFunction updateFunction, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT /* step */)
    {
        Control* ctrl        = mControls++;
        ctrl->updateFunction = updateFunction;
        ctrl->zone           = zone;
        ctrl->min            = min;
        ctrl->max            = max;
    }
    void addSimpleControl(FAUSTFLOAT* zone)
    {
        addControl(Control::simpleUpdate, zone, 0.f, 0.f, 0.f);
    }
    void addBoundedControl(FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    {
        addControl(Control::boundedUpdate, zone, min, max, step);
    }

private:
    Control* mControls;
};

//----------------------------------------------------------------------------
// FAUST generated code
//----------------------------------------------------------------------------

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif 

#include <cmath>
#include <math.h>

float mydsp_faustpower2_f(float value) {
	return (value * value);
	
}

#ifndef FAUSTCLASS 
#define FAUSTCLASS mydsp
#endif
#ifdef __APPLE__ 
#define exp10f __exp10f
#define exp10 __exp10
#endif

class mydsp : public dsp {
	
 private:
	
	FAUSTFLOAT fCheckbox0;
	FAUSTFLOAT fHslider0;
	float fRec0[2];
	int fSamplingFreq;
	int iConst0;
	float fConst1;
	float fConst2;
	float fConst3;
	float fConst4;
	FAUSTFLOAT fHslider1;
	float fRec4[2];
	float fRec5[3];
	FAUSTFLOAT fHslider2;
	float fRec6[2];
	float fRec7[3];
	float fRec8[3];
	float fConst5;
	float fRec3[2];
	float fRec1[2];
	float fConst6;
	float fConst7;
	float fConst8;
	float fConst9;
	float fConst10;
	float fConst11;
	float fRec21[3];
	float fRec22[3];
	float fRec23[3];
	float fRec24[3];
	float fRec25[3];
	float fRec26[3];
	float fRec27[3];
	float fRec28[3];
	float fRec29[3];
	float fConst12;
	float fConst13;
	float fRec20[2];
	float fRec18[2];
	float fRec17[2];
	float fRec15[2];
	float fRec14[2];
	float fRec12[2];
	float fRec11[2];
	float fRec9[2];
	float fConst14;
	float fConst15;
	float fRec36[3];
	float fRec37[3];
	float fRec38[3];
	float fRec39[3];
	float fRec40[3];
	float fConst16;
	float fConst17;
	float fRec35[2];
	float fRec33[2];
	float fRec32[2];
	float fRec30[2];
	float fConst18;
	float fConst19;
	float fConst20;
	float fRec50[3];
	float fRec51[3];
	float fRec52[3];
	float fRec53[3];
	float fRec54[3];
	float fRec55[3];
	float fRec56[3];
	float fConst21;
	float fConst22;
	float fRec49[2];
	float fRec47[2];
	float fRec46[2];
	float fRec44[2];
	float fConst23;
	float fRec43[2];
	float fRec41[2];
	float fRec57[3];
	int IOTA;
	float fVec0[2048];
	int iConst24;
	float fConst25;
	float fConst26;
	float fConst27;
	float fConst28;
	float fConst29;
	float fConst30;
	float fConst31;
	float fRec69[2];
	float fRec67[2];
	float fRec66[2];
	float fRec64[2];
	float fRec63[2];
	float fRec61[2];
	float fRec60[2];
	float fRec58[2];
	float fConst32;
	float fConst33;
	float fConst34;
	float fConst35;
	float fConst36;
	float fConst37;
	float fRec78[2];
	float fRec76[2];
	float fRec75[2];
	float fRec73[2];
	float fRec72[2];
	float fRec70[2];
	float fConst38;
	float fConst39;
	float fConst40;
	float fRec81[2];
	float fRec79[2];
	float fConst41;
	float fConst42;
	float fConst43;
	float fConst44;
	float fRec87[2];
	float fRec85[2];
	float fRec84[2];
	float fRec82[2];
	float fVec1[2048];
	int iConst45;
	float fConst46;
	float fConst47;
	float fConst48;
	float fConst49;
	float fConst50;
	float fConst51;
	float fConst52;
	float fRec99[2];
	float fRec97[2];
	float fRec96[2];
	float fRec94[2];
	float fRec93[2];
	float fRec91[2];
	float fRec90[2];
	float fRec88[2];
	float fConst53;
	float fConst54;
	float fConst55;
	float fConst56;
	float fConst57;
	float fConst58;
	float fRec108[2];
	float fRec106[2];
	float fRec105[2];
	float fRec103[2];
	float fRec102[2];
	float fRec100[2];
	float fConst59;
	float fConst60;
	float fConst61;
	float fRec111[2];
	float fRec109[2];
	float fConst62;
	float fConst63;
	float fConst64;
	float fConst65;
	float fRec117[2];
	float fRec115[2];
	float fRec114[2];
	float fRec112[2];
	float fVec2[2048];
	int iConst66;
	float fRec120[2];
	float fRec118[2];
	float fRec126[2];
	float fRec124[2];
	float fRec123[2];
	float fRec121[2];
	float fRec135[2];
	float fRec133[2];
	float fRec132[2];
	float fRec130[2];
	float fRec129[2];
	float fRec127[2];
	float fRec147[2];
	float fRec145[2];
	float fRec144[2];
	float fRec142[2];
	float fRec141[2];
	float fRec139[2];
	float fRec138[2];
	float fRec136[2];
	float fVec3[2048];
	float fRec150[2];
	float fRec148[2];
	float fRec156[2];
	float fRec154[2];
	float fRec153[2];
	float fRec151[2];
	float fRec165[2];
	float fRec163[2];
	float fRec162[2];
	float fRec160[2];
	float fRec159[2];
	float fRec157[2];
	float fRec177[2];
	float fRec175[2];
	float fRec174[2];
	float fRec172[2];
	float fRec171[2];
	float fRec169[2];
	float fRec168[2];
	float fRec166[2];
	float fVec4[2048];
	float fRec186[2];
	float fRec184[2];
	float fRec183[2];
	float fRec181[2];
	float fRec180[2];
	float fRec178[2];
	float fRec192[2];
	float fRec190[2];
	float fRec189[2];
	float fRec187[2];
	float fRec204[2];
	float fRec202[2];
	float fRec201[2];
	float fRec199[2];
	float fRec198[2];
	float fRec196[2];
	float fRec195[2];
	float fRec193[2];
	float fRec207[2];
	float fRec205[2];
	float fVec5[2048];
	float fRec210[2];
	float fRec208[2];
	float fRec216[2];
	float fRec214[2];
	float fRec213[2];
	float fRec211[2];
	float fRec225[2];
	float fRec223[2];
	float fRec222[2];
	float fRec220[2];
	float fRec219[2];
	float fRec217[2];
	float fRec237[2];
	float fRec235[2];
	float fRec234[2];
	float fRec232[2];
	float fRec231[2];
	float fRec229[2];
	float fRec228[2];
	float fRec226[2];
	float fVec6[2048];
	float fConst67;
	float fConst68;
	float fConst69;
	float fRec240[2];
	float fRec238[2];
	float fConst70;
	float fConst71;
	float fConst72;
	float fConst73;
	float fRec246[2];
	float fRec244[2];
	float fRec243[2];
	float fRec241[2];
	float fConst74;
	float fConst75;
	float fConst76;
	float fConst77;
	float fConst78;
	float fConst79;
	float fRec255[2];
	float fRec253[2];
	float fRec252[2];
	float fRec250[2];
	float fRec249[2];
	float fRec247[2];
	float fConst80;
	float fConst81;
	float fConst82;
	float fConst83;
	float fConst84;
	float fConst85;
	float fConst86;
	float fRec267[2];
	float fRec265[2];
	float fRec264[2];
	float fRec262[2];
	float fRec261[2];
	float fRec259[2];
	float fRec258[2];
	float fRec256[2];
	float fVec7[1024];
	int iConst87;
	float fConst88;
	float fConst89;
	float fConst90;
	float fConst91;
	float fConst92;
	float fConst93;
	float fRec276[2];
	float fRec274[2];
	float fRec273[2];
	float fRec271[2];
	float fRec270[2];
	float fRec268[2];
	float fConst94;
	float fConst95;
	float fConst96;
	float fRec279[2];
	float fRec277[2];
	float fConst97;
	float fConst98;
	float fConst99;
	float fConst100;
	float fRec285[2];
	float fRec283[2];
	float fRec282[2];
	float fRec280[2];
	float fConst101;
	float fConst102;
	float fConst103;
	float fConst104;
	float fConst105;
	float fConst106;
	float fConst107;
	float fRec297[2];
	float fRec295[2];
	float fRec294[2];
	float fRec292[2];
	float fRec291[2];
	float fRec289[2];
	float fRec288[2];
	float fRec286[2];
	float fVec8[1024];
	int iConst108;
	float fRec300[2];
	float fRec298[2];
	float fRec306[2];
	float fRec304[2];
	float fRec303[2];
	float fRec301[2];
	float fRec315[2];
	float fRec313[2];
	float fRec312[2];
	float fRec310[2];
	float fRec309[2];
	float fRec307[2];
	float fRec327[2];
	float fRec325[2];
	float fRec324[2];
	float fRec322[2];
	float fRec321[2];
	float fRec319[2];
	float fRec318[2];
	float fRec316[2];
	float fVec9[1024];
	float fRec339[2];
	float fRec337[2];
	float fRec336[2];
	float fRec334[2];
	float fRec333[2];
	float fRec331[2];
	float fRec330[2];
	float fRec328[2];
	float fRec348[2];
	float fRec346[2];
	float fRec345[2];
	float fRec343[2];
	float fRec342[2];
	float fRec340[2];
	float fRec351[2];
	float fRec349[2];
	float fRec357[2];
	float fRec355[2];
	float fRec354[2];
	float fRec352[2];
	float fVec10[1024];
	float fRec369[2];
	float fRec367[2];
	float fRec366[2];
	float fRec364[2];
	float fRec363[2];
	float fRec361[2];
	float fRec360[2];
	float fRec358[2];
	float fRec378[2];
	float fRec376[2];
	float fRec375[2];
	float fRec373[2];
	float fRec372[2];
	float fRec370[2];
	float fRec381[2];
	float fRec379[2];
	float fRec387[2];
	float fRec385[2];
	float fRec384[2];
	float fRec382[2];
	float fVec11[1024];
	float fRec399[2];
	float fRec397[2];
	float fRec396[2];
	float fRec394[2];
	float fRec393[2];
	float fRec391[2];
	float fRec390[2];
	float fRec388[2];
	float fRec408[2];
	float fRec406[2];
	float fRec405[2];
	float fRec403[2];
	float fRec402[2];
	float fRec400[2];
	float fRec411[2];
	float fRec409[2];
	float fRec417[2];
	float fRec415[2];
	float fRec414[2];
	float fRec412[2];
	float fVec12[1024];
	float fRec429[2];
	float fRec427[2];
	float fRec426[2];
	float fRec424[2];
	float fRec423[2];
	float fRec421[2];
	float fRec420[2];
	float fRec418[2];
	float fRec438[2];
	float fRec436[2];
	float fRec435[2];
	float fRec433[2];
	float fRec432[2];
	float fRec430[2];
	float fRec441[2];
	float fRec439[2];
	float fRec447[2];
	float fRec445[2];
	float fRec444[2];
	float fRec442[2];
	float fVec13[1024];
	float fRec459[2];
	float fRec457[2];
	float fRec456[2];
	float fRec454[2];
	float fRec453[2];
	float fRec451[2];
	float fRec450[2];
	float fRec448[2];
	float fRec468[2];
	float fRec466[2];
	float fRec465[2];
	float fRec463[2];
	float fRec462[2];
	float fRec460[2];
	float fRec471[2];
	float fRec469[2];
	float fRec477[2];
	float fRec475[2];
	float fRec474[2];
	float fRec472[2];
	float fVec14[1024];
	float fRec489[2];
	float fRec487[2];
	float fRec486[2];
	float fRec484[2];
	float fRec483[2];
	float fRec481[2];
	float fRec480[2];
	float fRec478[2];
	float fRec498[2];
	float fRec496[2];
	float fRec495[2];
	float fRec493[2];
	float fRec492[2];
	float fRec490[2];
	float fRec501[2];
	float fRec499[2];
	float fRec507[2];
	float fRec505[2];
	float fRec504[2];
	float fRec502[2];
	float fVec15[1024];
	float fRec513[2];
	float fRec511[2];
	float fRec510[2];
	float fRec508[2];
	float fRec516[2];
	float fRec514[2];
	float fRec525[2];
	float fRec523[2];
	float fRec522[2];
	float fRec520[2];
	float fRec519[2];
	float fRec517[2];
	float fRec537[2];
	float fRec535[2];
	float fRec534[2];
	float fRec532[2];
	float fRec531[2];
	float fRec529[2];
	float fRec528[2];
	float fRec526[2];
	float fVec16[1024];
	float fRec549[2];
	float fRec547[2];
	float fRec546[2];
	float fRec544[2];
	float fRec543[2];
	float fRec541[2];
	float fRec540[2];
	float fRec538[2];
	float fRec558[2];
	float fRec556[2];
	float fRec555[2];
	float fRec553[2];
	float fRec552[2];
	float fRec550[2];
	float fRec561[2];
	float fRec559[2];
	float fRec567[2];
	float fRec565[2];
	float fRec564[2];
	float fRec562[2];
	float fVec17[1024];
	float fRec579[2];
	float fRec577[2];
	float fRec576[2];
	float fRec574[2];
	float fRec573[2];
	float fRec571[2];
	float fRec570[2];
	float fRec568[2];
	float fRec588[2];
	float fRec586[2];
	float fRec585[2];
	float fRec583[2];
	float fRec582[2];
	float fRec580[2];
	float fRec591[2];
	float fRec589[2];
	float fRec597[2];
	float fRec595[2];
	float fRec594[2];
	float fRec592[2];
	float fVec18[1024];
	float fConst109;
	float fConst110;
	float fConst111;
	float fConst112;
	float fConst113;
	float fConst114;
	float fConst115;
	float fRec609[2];
	float fRec607[2];
	float fRec606[2];
	float fRec604[2];
	float fRec603[2];
	float fRec601[2];
	float fRec600[2];
	float fRec598[2];
	float fConst116;
	float fConst117;
	float fConst118;
	float fConst119;
	float fConst120;
	float fConst121;
	float fRec618[2];
	float fRec616[2];
	float fRec615[2];
	float fRec613[2];
	float fRec612[2];
	float fRec610[2];
	float fConst122;
	float fConst123;
	float fConst124;
	float fRec621[2];
	float fRec619[2];
	float fConst125;
	float fConst126;
	float fConst127;
	float fConst128;
	float fRec627[2];
	float fRec625[2];
	float fRec624[2];
	float fRec622[2];
	float fVec19[3];
	int iConst129;
	float fConst130;
	float fConst131;
	float fConst132;
	float fRec630[2];
	float fRec628[2];
	float fConst133;
	float fConst134;
	float fConst135;
	float fConst136;
	float fConst137;
	float fConst138;
	float fConst139;
	float fRec642[2];
	float fRec640[2];
	float fRec639[2];
	float fRec637[2];
	float fRec636[2];
	float fRec634[2];
	float fRec633[2];
	float fRec631[2];
	float fConst140;
	float fConst141;
	float fConst142;
	float fConst143;
	float fRec648[2];
	float fRec646[2];
	float fRec645[2];
	float fRec643[2];
	float fConst144;
	float fConst145;
	float fConst146;
	float fConst147;
	float fConst148;
	float fConst149;
	float fRec657[2];
	float fRec655[2];
	float fRec654[2];
	float fRec652[2];
	float fRec651[2];
	float fRec649[2];
	float fRec669[2];
	float fRec667[2];
	float fRec666[2];
	float fRec664[2];
	float fRec663[2];
	float fRec661[2];
	float fRec660[2];
	float fRec658[2];
	float fRec678[2];
	float fRec676[2];
	float fRec675[2];
	float fRec673[2];
	float fRec672[2];
	float fRec670[2];
	float fRec681[2];
	float fRec679[2];
	float fRec687[2];
	float fRec685[2];
	float fRec684[2];
	float fRec682[2];
	float fRec699[2];
	float fRec697[2];
	float fRec696[2];
	float fRec694[2];
	float fRec693[2];
	float fRec691[2];
	float fRec690[2];
	float fRec688[2];
	float fRec708[2];
	float fRec706[2];
	float fRec705[2];
	float fRec703[2];
	float fRec702[2];
	float fRec700[2];
	float fRec711[2];
	float fRec709[2];
	float fRec717[2];
	float fRec715[2];
	float fRec714[2];
	float fRec712[2];
	float fVec20[3];
	float fRec729[2];
	float fRec727[2];
	float fRec726[2];
	float fRec724[2];
	float fRec723[2];
	float fRec721[2];
	float fRec720[2];
	float fRec718[2];
	float fRec738[2];
	float fRec736[2];
	float fRec735[2];
	float fRec733[2];
	float fRec732[2];
	float fRec730[2];
	float fRec741[2];
	float fRec739[2];
	float fRec747[2];
	float fRec745[2];
	float fRec744[2];
	float fRec742[2];
	float fRec756[2];
	float fRec754[2];
	float fRec753[2];
	float fRec751[2];
	float fRec750[2];
	float fRec748[2];
	float fRec762[2];
	float fRec760[2];
	float fRec759[2];
	float fRec757[2];
	float fRec774[2];
	float fRec772[2];
	float fRec771[2];
	float fRec769[2];
	float fRec768[2];
	float fRec766[2];
	float fRec765[2];
	float fRec763[2];
	float fRec777[2];
	float fRec775[2];
	float fRec789[2];
	float fRec787[2];
	float fRec786[2];
	float fRec784[2];
	float fRec783[2];
	float fRec781[2];
	float fRec780[2];
	float fRec778[2];
	float fRec798[2];
	float fRec796[2];
	float fRec795[2];
	float fRec793[2];
	float fRec792[2];
	float fRec790[2];
	float fRec801[2];
	float fRec799[2];
	float fRec807[2];
	float fRec805[2];
	float fRec804[2];
	float fRec802[2];
	float fVec21[3];
	float fRec819[2];
	float fRec817[2];
	float fRec816[2];
	float fRec814[2];
	float fRec813[2];
	float fRec811[2];
	float fRec810[2];
	float fRec808[2];
	float fRec828[2];
	float fRec826[2];
	float fRec825[2];
	float fRec823[2];
	float fRec822[2];
	float fRec820[2];
	float fRec831[2];
	float fRec829[2];
	float fRec837[2];
	float fRec835[2];
	float fRec834[2];
	float fRec832[2];
	float fRec849[2];
	float fRec847[2];
	float fRec846[2];
	float fRec844[2];
	float fRec843[2];
	float fRec841[2];
	float fRec840[2];
	float fRec838[2];
	float fRec858[2];
	float fRec856[2];
	float fRec855[2];
	float fRec853[2];
	float fRec852[2];
	float fRec850[2];
	float fRec861[2];
	float fRec859[2];
	float fRec867[2];
	float fRec865[2];
	float fRec864[2];
	float fRec862[2];
	float fRec879[2];
	float fRec877[2];
	float fRec876[2];
	float fRec874[2];
	float fRec873[2];
	float fRec871[2];
	float fRec870[2];
	float fRec868[2];
	float fRec888[2];
	float fRec886[2];
	float fRec885[2];
	float fRec883[2];
	float fRec882[2];
	float fRec880[2];
	float fRec891[2];
	float fRec889[2];
	float fRec897[2];
	float fRec895[2];
	float fRec894[2];
	float fRec892[2];
	float fVec22[3];
	float fRec909[2];
	float fRec907[2];
	float fRec906[2];
	float fRec904[2];
	float fRec903[2];
	float fRec901[2];
	float fRec900[2];
	float fRec898[2];
	float fRec918[2];
	float fRec916[2];
	float fRec915[2];
	float fRec913[2];
	float fRec912[2];
	float fRec910[2];
	float fRec921[2];
	float fRec919[2];
	float fRec927[2];
	float fRec925[2];
	float fRec924[2];
	float fRec922[2];
	float fRec939[2];
	float fRec937[2];
	float fRec936[2];
	float fRec934[2];
	float fRec933[2];
	float fRec931[2];
	float fRec930[2];
	float fRec928[2];
	float fRec948[2];
	float fRec946[2];
	float fRec945[2];
	float fRec943[2];
	float fRec942[2];
	float fRec940[2];
	float fRec951[2];
	float fRec949[2];
	float fRec957[2];
	float fRec955[2];
	float fRec954[2];
	float fRec952[2];
	
 public:
	
	void metadata(Meta* m) { 
		m->declare("author", "AmbisonicDecoderToolkit");
		m->declare("copyright", "(c) Aaron J. Heller 2013");
		m->declare("license", "BSD 3-Clause License");
		m->declare("name", "SATOsw0o4");
		m->declare("version", "1.2");
	}

	virtual int getNumInputs() {
		return 25;
		
	}
	virtual int getNumOutputs() {
		return 31;
		
	}
	virtual int getInputRate(int channel) {
		int rate;
		switch (channel) {
			case 0: {
				rate = 1;
				break;
			}
			case 1: {
				rate = 1;
				break;
			}
			case 2: {
				rate = 1;
				break;
			}
			case 3: {
				rate = 1;
				break;
			}
			case 4: {
				rate = 1;
				break;
			}
			case 5: {
				rate = 1;
				break;
			}
			case 6: {
				rate = 1;
				break;
			}
			case 7: {
				rate = 1;
				break;
			}
			case 8: {
				rate = 1;
				break;
			}
			case 9: {
				rate = 1;
				break;
			}
			case 10: {
				rate = 1;
				break;
			}
			case 11: {
				rate = 1;
				break;
			}
			case 12: {
				rate = 1;
				break;
			}
			case 13: {
				rate = 1;
				break;
			}
			case 14: {
				rate = 1;
				break;
			}
			case 15: {
				rate = 1;
				break;
			}
			case 16: {
				rate = 1;
				break;
			}
			case 17: {
				rate = 1;
				break;
			}
			case 18: {
				rate = 1;
				break;
			}
			case 19: {
				rate = 1;
				break;
			}
			case 20: {
				rate = 1;
				break;
			}
			case 21: {
				rate = 1;
				break;
			}
			case 22: {
				rate = 1;
				break;
			}
			case 23: {
				rate = 1;
				break;
			}
			case 24: {
				rate = 1;
				break;
			}
			default: {
				rate = -1;
				break;
			}
			
		}
		return rate;
		
	}
	virtual int getOutputRate(int channel) {
		int rate;
		switch (channel) {
			case 0: {
				rate = 1;
				break;
			}
			case 1: {
				rate = 1;
				break;
			}
			case 2: {
				rate = 1;
				break;
			}
			case 3: {
				rate = 1;
				break;
			}
			case 4: {
				rate = 1;
				break;
			}
			case 5: {
				rate = 1;
				break;
			}
			case 6: {
				rate = 1;
				break;
			}
			case 7: {
				rate = 1;
				break;
			}
			case 8: {
				rate = 1;
				break;
			}
			case 9: {
				rate = 1;
				break;
			}
			case 10: {
				rate = 1;
				break;
			}
			case 11: {
				rate = 1;
				break;
			}
			case 12: {
				rate = 1;
				break;
			}
			case 13: {
				rate = 1;
				break;
			}
			case 14: {
				rate = 1;
				break;
			}
			case 15: {
				rate = 1;
				break;
			}
			case 16: {
				rate = 1;
				break;
			}
			case 17: {
				rate = 1;
				break;
			}
			case 18: {
				rate = 1;
				break;
			}
			case 19: {
				rate = 1;
				break;
			}
			case 20: {
				rate = 1;
				break;
			}
			case 21: {
				rate = 1;
				break;
			}
			case 22: {
				rate = 1;
				break;
			}
			case 23: {
				rate = 1;
				break;
			}
			case 24: {
				rate = 1;
				break;
			}
			case 25: {
				rate = 1;
				break;
			}
			case 26: {
				rate = 1;
				break;
			}
			case 27: {
				rate = 1;
				break;
			}
			case 28: {
				rate = 1;
				break;
			}
			case 29: {
				rate = 1;
				break;
			}
			case 30: {
				rate = 1;
				break;
			}
			default: {
				rate = -1;
				break;
			}
			
		}
		return rate;
		
	}
	
	static void classInit(int samplingFreq) {
		
	}
	
	virtual void instanceConstants(int samplingFreq) {
		fSamplingFreq = samplingFreq;
		iConst0 = min(192000, max(1, fSamplingFreq));
		fConst1 = float(iConst0);
		fConst2 = ((28.482542f / fConst1) + 1.0f);
		fConst3 = (1.0f / fConst2);
		fConst4 = (3.14159274f / float(iConst0));
		fConst5 = (0.0f - (56.9650841f / (fConst1 * fConst2)));
		fConst6 = mydsp_faustpower2_f(fConst1);
		fConst7 = ((((7414.97852f / fConst1) + 164.98288f) / fConst1) + 1.0f);
		fConst8 = (0.0f - (29659.9141f / (fConst6 * fConst7)));
		fConst9 = (0.0f - (((29659.9141f / fConst1) + 329.965759f) / (fConst1 * fConst7)));
		fConst10 = ((((9319.53711f / fConst1) + 119.842537f) / fConst1) + 1.0f);
		fConst11 = (1.0f / (fConst7 * fConst10));
		fConst12 = (0.0f - (37278.1484f / (fConst6 * fConst10)));
		fConst13 = (0.0f - (((37278.1484f / fConst1) + 239.685074f) / (fConst1 * fConst10)));
		fConst14 = ((((2433.76538f / fConst1) + 85.4476242f) / fConst1) + 1.0f);
		fConst15 = (1.0f / fConst14);
		fConst16 = (0.0f - (9735.06152f / (fConst6 * fConst14)));
		fConst17 = (0.0f - (((9735.06152f / fConst1) + 170.895248f) / (fConst1 * fConst14)));
		fConst18 = ((66.1417389f / fConst1) + 1.0f);
		fConst19 = ((((5240.24805f / fConst1) + 104.75351f) / fConst1) + 1.0f);
		fConst20 = (1.0f / (fConst18 * fConst19));
		fConst21 = (0.0f - (((20960.9922f / fConst1) + 209.507019f) / (fConst1 * fConst19)));
		fConst22 = (0.0f - (20960.9922f / (fConst6 * fConst19)));
		fConst23 = (0.0f - (132.283478f / (fConst1 * fConst18)));
		iConst24 = int(((0.0106405718f * float(iConst0)) + 0.5f));
		fConst25 = ((((5590.23682f / fConst1) + 143.25145f) / fConst1) + 1.0f);
		fConst26 = (0.0f - (22360.9473f / (fConst6 * fConst25)));
		fConst27 = (0.0f - (((22360.9473f / fConst1) + 286.502899f) / (fConst1 * fConst25)));
		fConst28 = ((((7026.10596f / fConst1) + 104.056969f) / fConst1) + 1.0f);
		fConst29 = (1.0f / (fConst25 * fConst28));
		fConst30 = (0.0f - (28104.4238f / (fConst6 * fConst28)));
		fConst31 = (0.0f - (((28104.4238f / fConst1) + 208.113937f) / (fConst1 * fConst28)));
		fConst32 = ((57.4295998f / fConst1) + 1.0f);
		fConst33 = (0.0f - (114.8592f / (fConst1 * fConst32)));
		fConst34 = ((((3950.68286f / fConst1) + 90.955452f) / fConst1) + 1.0f);
		fConst35 = (1.0f / (fConst32 * fConst34));
		fConst36 = (0.0f - (15802.7314f / (fConst6 * fConst34)));
		fConst37 = (0.0f - (((15802.7314f / fConst1) + 181.910904f) / (fConst1 * fConst34)));
		fConst38 = ((24.7308426f / fConst1) + 1.0f);
		fConst39 = (1.0f / fConst38);
		fConst40 = (0.0f - (49.4616852f / (fConst1 * fConst38)));
		fConst41 = ((((1834.84363f / fConst1) + 74.1925278f) / fConst1) + 1.0f);
		fConst42 = (1.0f / fConst41);
		fConst43 = (0.0f - (7339.37451f / (fConst6 * fConst41)));
		fConst44 = (0.0f - (((7339.37451f / fConst1) + 148.385056f) / (fConst1 * fConst41)));
		iConst45 = int(((0.00797751546f * float(iConst0)) + 0.5f));
		fConst46 = ((((5593.46094f / fConst1) + 143.292755f) / fConst1) + 1.0f);
		fConst47 = (0.0f - (22373.8438f / (fConst6 * fConst46)));
		fConst48 = (0.0f - (((22373.8438f / fConst1) + 286.58551f) / (fConst1 * fConst46)));
		fConst49 = ((((7030.15771f / fConst1) + 104.086967f) / fConst1) + 1.0f);
		fConst50 = (1.0f / (fConst46 * fConst49));
		fConst51 = (0.0f - (28120.6309f / (fConst6 * fConst49)));
		fConst52 = (0.0f - (((28120.6309f / fConst1) + 208.173935f) / (fConst1 * fConst49)));
		fConst53 = ((57.4461555f / fConst1) + 1.0f);
		fConst54 = (0.0f - (114.892311f / (fConst1 * fConst53)));
		fConst55 = ((((3952.96118f / fConst1) + 90.9816742f) / fConst1) + 1.0f);
		fConst56 = (1.0f / (fConst53 * fConst55));
		fConst57 = (0.0f - (15811.8447f / (fConst6 * fConst55)));
		fConst58 = (0.0f - (((15811.8447f / fConst1) + 181.963348f) / (fConst1 * fConst55)));
		fConst59 = ((24.7379723f / fConst1) + 1.0f);
		fConst60 = (1.0f / fConst59);
		fConst61 = (0.0f - (49.4759445f / (fConst1 * fConst59)));
		fConst62 = ((((1835.90173f / fConst1) + 74.213913f) / fConst1) + 1.0f);
		fConst63 = (1.0f / fConst62);
		fConst64 = (0.0f - (7343.60693f / (fConst6 * fConst62)));
		fConst65 = (0.0f - (((7343.60693f / fConst1) + 148.427826f) / (fConst1 * fConst62)));
		iConst66 = int(((0.00798334274f * float(iConst0)) + 0.5f));
		fConst67 = ((21.4885197f / fConst1) + 1.0f);
		fConst68 = (1.0f / fConst67);
		fConst69 = (0.0f - (42.9770393f / (fConst1 * fConst67)));
		fConst70 = ((((1385.26929f / fConst1) + 64.4655533f) / fConst1) + 1.0f);
		fConst71 = (1.0f / fConst70);
		fConst72 = (0.0f - (5541.07715f / (fConst6 * fConst70)));
		fConst73 = (0.0f - (((5541.07715f / fConst1) + 128.931107f) / (fConst1 * fConst70)));
		fConst74 = ((49.900322f / fConst1) + 1.0f);
		fConst75 = (0.0f - (99.8006439f / (fConst1 * fConst74)));
		fConst76 = ((((2982.68457f / fConst1) + 79.0307922f) / fConst1) + 1.0f);
		fConst77 = (1.0f / (fConst74 * fConst76));
		fConst78 = (0.0f - (11930.7383f / (fConst6 * fConst76)));
		fConst79 = (0.0f - (((11930.7383f / fConst1) + 158.061584f) / (fConst1 * fConst76)));
		fConst80 = ((((4220.51416f / fConst1) + 124.470551f) / fConst1) + 1.0f);
		fConst81 = (0.0f - (16882.0566f / (fConst6 * fConst80)));
		fConst82 = (0.0f - (((16882.0566f / fConst1) + 248.941101f) / (fConst1 * fConst80)));
		fConst83 = ((((5304.56592f / fConst1) + 90.4146347f) / fConst1) + 1.0f);
		fConst84 = (1.0f / (fConst80 * fConst83));
		fConst85 = (0.0f - (21218.2637f / (fConst6 * fConst83)));
		fConst86 = (0.0f - (((21218.2637f / fConst1) + 180.829269f) / (fConst1 * fConst83)));
		iConst87 = int(((0.00492694648f * float(iConst0)) + 0.5f));
		fConst88 = ((49.8691025f / fConst1) + 1.0f);
		fConst89 = (0.0f - (99.738205f / (fConst1 * fConst88)));
		fConst90 = ((((2978.95337f / fConst1) + 78.9813385f) / fConst1) + 1.0f);
		fConst91 = (1.0f / (fConst88 * fConst90));
		fConst92 = (0.0f - (11915.8135f / (fConst6 * fConst90)));
		fConst93 = (0.0f - (((11915.8135f / fConst1) + 157.962677f) / (fConst1 * fConst90)));
		fConst94 = ((21.4750729f / fConst1) + 1.0f);
		fConst95 = (1.0f / fConst94);
		fConst96 = (0.0f - (42.9501457f / (fConst1 * fConst94)));
		fConst97 = ((((1383.53638f / fConst1) + 64.4252167f) / fConst1) + 1.0f);
		fConst98 = (1.0f / fConst97);
		fConst99 = (0.0f - (5534.14551f / (fConst6 * fConst97)));
		fConst100 = (0.0f - (((5534.14551f / fConst1) + 128.850433f) / (fConst1 * fConst97)));
		fConst101 = ((((4215.23438f / fConst1) + 124.39267f) / fConst1) + 1.0f);
		fConst102 = (0.0f - (16860.9375f / (fConst6 * fConst101)));
		fConst103 = (0.0f - (((16860.9375f / fConst1) + 248.785339f) / (fConst1 * fConst101)));
		fConst104 = ((((5297.92969f / fConst1) + 90.3580627f) / fConst1) + 1.0f);
		fConst105 = (1.0f / (fConst101 * fConst104));
		fConst106 = (0.0f - (21191.7188f / (fConst6 * fConst104)));
		fConst107 = (0.0f - (((21191.7188f / fConst1) + 180.716125f) / (fConst1 * fConst104)));
		iConst108 = int(((0.00491237827f * float(iConst0)) + 0.5f));
		fConst109 = ((((2875.55737f / fConst1) + 102.741272f) / fConst1) + 1.0f);
		fConst110 = (0.0f - (11502.2295f / (fConst6 * fConst109)));
		fConst111 = (0.0f - (((11502.2295f / fConst1) + 205.482544f) / (fConst1 * fConst109)));
		fConst112 = ((((3614.15283f / fConst1) + 74.6306229f) / fConst1) + 1.0f);
		fConst113 = (1.0f / (fConst109 * fConst112));
		fConst114 = (0.0f - (14456.6113f / (fConst6 * fConst112)));
		fConst115 = (0.0f - (((14456.6113f / fConst1) + 149.261246f) / (fConst1 * fConst112)));
		fConst116 = ((41.1890411f / fConst1) + 1.0f);
		fConst117 = (0.0f - (82.3780823f / (fConst1 * fConst116)));
		fConst118 = ((((2032.1886f / fConst1) + 65.2340927f) / fConst1) + 1.0f);
		fConst119 = (1.0f / (fConst116 * fConst118));
		fConst120 = (0.0f - (8128.75439f / (fConst6 * fConst118)));
		fConst121 = (0.0f - (((8128.75439f / fConst1) + 130.468185f) / (fConst1 * fConst118)));
		fConst122 = ((17.7371902f / fConst1) + 1.0f);
		fConst123 = (1.0f / fConst122);
		fConst124 = (0.0f - (35.4743805f / (fConst1 * fConst122)));
		fConst125 = ((((943.82373f / fConst1) + 53.2115707f) / fConst1) + 1.0f);
		fConst126 = (1.0f / fConst125);
		fConst127 = (0.0f - (3775.29492f / (fConst6 * fConst125)));
		fConst128 = (0.0f - (((3775.29492f / fConst1) + 106.423141f) / (fConst1 * fConst125)));
		iConst129 = int(((5.82725761e-06f * float(iConst0)) + 0.5f));
		fConst130 = ((17.7335243f / fConst1) + 1.0f);
		fConst131 = (1.0f / fConst130);
		fConst132 = (0.0f - (35.4670486f / (fConst1 * fConst130)));
		fConst133 = ((((2874.3689f / fConst1) + 102.720039f) / fConst1) + 1.0f);
		fConst134 = (0.0f - (11497.4756f / (fConst6 * fConst133)));
		fConst135 = (0.0f - (((11497.4756f / fConst1) + 205.440079f) / (fConst1 * fConst133)));
		fConst136 = ((((3612.65894f / fConst1) + 74.6151962f) / fConst1) + 1.0f);
		fConst137 = (1.0f / (fConst133 * fConst136));
		fConst138 = (0.0f - (14450.6357f / (fConst6 * fConst136)));
		fConst139 = (0.0f - (((14450.6357f / fConst1) + 149.230392f) / (fConst1 * fConst136)));
		fConst140 = ((((943.433594f / fConst1) + 53.200573f) / fConst1) + 1.0f);
		fConst141 = (1.0f / fConst140);
		fConst142 = (0.0f - (3773.73438f / (fConst6 * fConst140)));
		fConst143 = (0.0f - (((3773.73438f / fConst1) + 106.401146f) / (fConst1 * fConst140)));
		fConst144 = ((41.1805305f / fConst1) + 1.0f);
		fConst145 = (0.0f - (82.3610611f / (fConst1 * fConst144)));
		fConst146 = ((((2031.34863f / fConst1) + 65.2206116f) / fConst1) + 1.0f);
		fConst147 = (1.0f / (fConst144 * fConst146));
		fConst148 = (0.0f - (8125.39453f / (fConst6 * fConst146)));
		fConst149 = (0.0f - (((8125.39453f / fConst1) + 130.441223f) / (fConst1 * fConst146)));
		
	}
	
	virtual void instanceResetUserInterface() {
		fCheckbox0 = FAUSTFLOAT(0.0f);
		fHslider0 = FAUSTFLOAT(-10.0f);
		fHslider1 = FAUSTFLOAT(400.0f);
		fHslider2 = FAUSTFLOAT(0.0f);
		
	}
	
	virtual void instanceClear() {
		for (int l0 = 0; (l0 < 2); l0 = (l0 + 1)) {
			fRec0[l0] = 0.0f;
			
		}
		for (int l1 = 0; (l1 < 2); l1 = (l1 + 1)) {
			fRec4[l1] = 0.0f;
			
		}
		for (int l2 = 0; (l2 < 3); l2 = (l2 + 1)) {
			fRec5[l2] = 0.0f;
			
		}
		for (int l3 = 0; (l3 < 2); l3 = (l3 + 1)) {
			fRec6[l3] = 0.0f;
			
		}
		for (int l4 = 0; (l4 < 3); l4 = (l4 + 1)) {
			fRec7[l4] = 0.0f;
			
		}
		for (int l5 = 0; (l5 < 3); l5 = (l5 + 1)) {
			fRec8[l5] = 0.0f;
			
		}
		for (int l6 = 0; (l6 < 2); l6 = (l6 + 1)) {
			fRec3[l6] = 0.0f;
			
		}
		for (int l7 = 0; (l7 < 2); l7 = (l7 + 1)) {
			fRec1[l7] = 0.0f;
			
		}
		for (int l8 = 0; (l8 < 3); l8 = (l8 + 1)) {
			fRec21[l8] = 0.0f;
			
		}
		for (int l9 = 0; (l9 < 3); l9 = (l9 + 1)) {
			fRec22[l9] = 0.0f;
			
		}
		for (int l10 = 0; (l10 < 3); l10 = (l10 + 1)) {
			fRec23[l10] = 0.0f;
			
		}
		for (int l11 = 0; (l11 < 3); l11 = (l11 + 1)) {
			fRec24[l11] = 0.0f;
			
		}
		for (int l12 = 0; (l12 < 3); l12 = (l12 + 1)) {
			fRec25[l12] = 0.0f;
			
		}
		for (int l13 = 0; (l13 < 3); l13 = (l13 + 1)) {
			fRec26[l13] = 0.0f;
			
		}
		for (int l14 = 0; (l14 < 3); l14 = (l14 + 1)) {
			fRec27[l14] = 0.0f;
			
		}
		for (int l15 = 0; (l15 < 3); l15 = (l15 + 1)) {
			fRec28[l15] = 0.0f;
			
		}
		for (int l16 = 0; (l16 < 3); l16 = (l16 + 1)) {
			fRec29[l16] = 0.0f;
			
		}
		for (int l17 = 0; (l17 < 2); l17 = (l17 + 1)) {
			fRec20[l17] = 0.0f;
			
		}
		for (int l18 = 0; (l18 < 2); l18 = (l18 + 1)) {
			fRec18[l18] = 0.0f;
			
		}
		for (int l19 = 0; (l19 < 2); l19 = (l19 + 1)) {
			fRec17[l19] = 0.0f;
			
		}
		for (int l20 = 0; (l20 < 2); l20 = (l20 + 1)) {
			fRec15[l20] = 0.0f;
			
		}
		for (int l21 = 0; (l21 < 2); l21 = (l21 + 1)) {
			fRec14[l21] = 0.0f;
			
		}
		for (int l22 = 0; (l22 < 2); l22 = (l22 + 1)) {
			fRec12[l22] = 0.0f;
			
		}
		for (int l23 = 0; (l23 < 2); l23 = (l23 + 1)) {
			fRec11[l23] = 0.0f;
			
		}
		for (int l24 = 0; (l24 < 2); l24 = (l24 + 1)) {
			fRec9[l24] = 0.0f;
			
		}
		for (int l25 = 0; (l25 < 3); l25 = (l25 + 1)) {
			fRec36[l25] = 0.0f;
			
		}
		for (int l26 = 0; (l26 < 3); l26 = (l26 + 1)) {
			fRec37[l26] = 0.0f;
			
		}
		for (int l27 = 0; (l27 < 3); l27 = (l27 + 1)) {
			fRec38[l27] = 0.0f;
			
		}
		for (int l28 = 0; (l28 < 3); l28 = (l28 + 1)) {
			fRec39[l28] = 0.0f;
			
		}
		for (int l29 = 0; (l29 < 3); l29 = (l29 + 1)) {
			fRec40[l29] = 0.0f;
			
		}
		for (int l30 = 0; (l30 < 2); l30 = (l30 + 1)) {
			fRec35[l30] = 0.0f;
			
		}
		for (int l31 = 0; (l31 < 2); l31 = (l31 + 1)) {
			fRec33[l31] = 0.0f;
			
		}
		for (int l32 = 0; (l32 < 2); l32 = (l32 + 1)) {
			fRec32[l32] = 0.0f;
			
		}
		for (int l33 = 0; (l33 < 2); l33 = (l33 + 1)) {
			fRec30[l33] = 0.0f;
			
		}
		for (int l34 = 0; (l34 < 3); l34 = (l34 + 1)) {
			fRec50[l34] = 0.0f;
			
		}
		for (int l35 = 0; (l35 < 3); l35 = (l35 + 1)) {
			fRec51[l35] = 0.0f;
			
		}
		for (int l36 = 0; (l36 < 3); l36 = (l36 + 1)) {
			fRec52[l36] = 0.0f;
			
		}
		for (int l37 = 0; (l37 < 3); l37 = (l37 + 1)) {
			fRec53[l37] = 0.0f;
			
		}
		for (int l38 = 0; (l38 < 3); l38 = (l38 + 1)) {
			fRec54[l38] = 0.0f;
			
		}
		for (int l39 = 0; (l39 < 3); l39 = (l39 + 1)) {
			fRec55[l39] = 0.0f;
			
		}
		for (int l40 = 0; (l40 < 3); l40 = (l40 + 1)) {
			fRec56[l40] = 0.0f;
			
		}
		for (int l41 = 0; (l41 < 2); l41 = (l41 + 1)) {
			fRec49[l41] = 0.0f;
			
		}
		for (int l42 = 0; (l42 < 2); l42 = (l42 + 1)) {
			fRec47[l42] = 0.0f;
			
		}
		for (int l43 = 0; (l43 < 2); l43 = (l43 + 1)) {
			fRec46[l43] = 0.0f;
			
		}
		for (int l44 = 0; (l44 < 2); l44 = (l44 + 1)) {
			fRec44[l44] = 0.0f;
			
		}
		for (int l45 = 0; (l45 < 2); l45 = (l45 + 1)) {
			fRec43[l45] = 0.0f;
			
		}
		for (int l46 = 0; (l46 < 2); l46 = (l46 + 1)) {
			fRec41[l46] = 0.0f;
			
		}
		for (int l47 = 0; (l47 < 3); l47 = (l47 + 1)) {
			fRec57[l47] = 0.0f;
			
		}
		IOTA = 0;
		for (int l48 = 0; (l48 < 2048); l48 = (l48 + 1)) {
			fVec0[l48] = 0.0f;
			
		}
		for (int l49 = 0; (l49 < 2); l49 = (l49 + 1)) {
			fRec69[l49] = 0.0f;
			
		}
		for (int l50 = 0; (l50 < 2); l50 = (l50 + 1)) {
			fRec67[l50] = 0.0f;
			
		}
		for (int l51 = 0; (l51 < 2); l51 = (l51 + 1)) {
			fRec66[l51] = 0.0f;
			
		}
		for (int l52 = 0; (l52 < 2); l52 = (l52 + 1)) {
			fRec64[l52] = 0.0f;
			
		}
		for (int l53 = 0; (l53 < 2); l53 = (l53 + 1)) {
			fRec63[l53] = 0.0f;
			
		}
		for (int l54 = 0; (l54 < 2); l54 = (l54 + 1)) {
			fRec61[l54] = 0.0f;
			
		}
		for (int l55 = 0; (l55 < 2); l55 = (l55 + 1)) {
			fRec60[l55] = 0.0f;
			
		}
		for (int l56 = 0; (l56 < 2); l56 = (l56 + 1)) {
			fRec58[l56] = 0.0f;
			
		}
		for (int l57 = 0; (l57 < 2); l57 = (l57 + 1)) {
			fRec78[l57] = 0.0f;
			
		}
		for (int l58 = 0; (l58 < 2); l58 = (l58 + 1)) {
			fRec76[l58] = 0.0f;
			
		}
		for (int l59 = 0; (l59 < 2); l59 = (l59 + 1)) {
			fRec75[l59] = 0.0f;
			
		}
		for (int l60 = 0; (l60 < 2); l60 = (l60 + 1)) {
			fRec73[l60] = 0.0f;
			
		}
		for (int l61 = 0; (l61 < 2); l61 = (l61 + 1)) {
			fRec72[l61] = 0.0f;
			
		}
		for (int l62 = 0; (l62 < 2); l62 = (l62 + 1)) {
			fRec70[l62] = 0.0f;
			
		}
		for (int l63 = 0; (l63 < 2); l63 = (l63 + 1)) {
			fRec81[l63] = 0.0f;
			
		}
		for (int l64 = 0; (l64 < 2); l64 = (l64 + 1)) {
			fRec79[l64] = 0.0f;
			
		}
		for (int l65 = 0; (l65 < 2); l65 = (l65 + 1)) {
			fRec87[l65] = 0.0f;
			
		}
		for (int l66 = 0; (l66 < 2); l66 = (l66 + 1)) {
			fRec85[l66] = 0.0f;
			
		}
		for (int l67 = 0; (l67 < 2); l67 = (l67 + 1)) {
			fRec84[l67] = 0.0f;
			
		}
		for (int l68 = 0; (l68 < 2); l68 = (l68 + 1)) {
			fRec82[l68] = 0.0f;
			
		}
		for (int l69 = 0; (l69 < 2048); l69 = (l69 + 1)) {
			fVec1[l69] = 0.0f;
			
		}
		for (int l70 = 0; (l70 < 2); l70 = (l70 + 1)) {
			fRec99[l70] = 0.0f;
			
		}
		for (int l71 = 0; (l71 < 2); l71 = (l71 + 1)) {
			fRec97[l71] = 0.0f;
			
		}
		for (int l72 = 0; (l72 < 2); l72 = (l72 + 1)) {
			fRec96[l72] = 0.0f;
			
		}
		for (int l73 = 0; (l73 < 2); l73 = (l73 + 1)) {
			fRec94[l73] = 0.0f;
			
		}
		for (int l74 = 0; (l74 < 2); l74 = (l74 + 1)) {
			fRec93[l74] = 0.0f;
			
		}
		for (int l75 = 0; (l75 < 2); l75 = (l75 + 1)) {
			fRec91[l75] = 0.0f;
			
		}
		for (int l76 = 0; (l76 < 2); l76 = (l76 + 1)) {
			fRec90[l76] = 0.0f;
			
		}
		for (int l77 = 0; (l77 < 2); l77 = (l77 + 1)) {
			fRec88[l77] = 0.0f;
			
		}
		for (int l78 = 0; (l78 < 2); l78 = (l78 + 1)) {
			fRec108[l78] = 0.0f;
			
		}
		for (int l79 = 0; (l79 < 2); l79 = (l79 + 1)) {
			fRec106[l79] = 0.0f;
			
		}
		for (int l80 = 0; (l80 < 2); l80 = (l80 + 1)) {
			fRec105[l80] = 0.0f;
			
		}
		for (int l81 = 0; (l81 < 2); l81 = (l81 + 1)) {
			fRec103[l81] = 0.0f;
			
		}
		for (int l82 = 0; (l82 < 2); l82 = (l82 + 1)) {
			fRec102[l82] = 0.0f;
			
		}
		for (int l83 = 0; (l83 < 2); l83 = (l83 + 1)) {
			fRec100[l83] = 0.0f;
			
		}
		for (int l84 = 0; (l84 < 2); l84 = (l84 + 1)) {
			fRec111[l84] = 0.0f;
			
		}
		for (int l85 = 0; (l85 < 2); l85 = (l85 + 1)) {
			fRec109[l85] = 0.0f;
			
		}
		for (int l86 = 0; (l86 < 2); l86 = (l86 + 1)) {
			fRec117[l86] = 0.0f;
			
		}
		for (int l87 = 0; (l87 < 2); l87 = (l87 + 1)) {
			fRec115[l87] = 0.0f;
			
		}
		for (int l88 = 0; (l88 < 2); l88 = (l88 + 1)) {
			fRec114[l88] = 0.0f;
			
		}
		for (int l89 = 0; (l89 < 2); l89 = (l89 + 1)) {
			fRec112[l89] = 0.0f;
			
		}
		for (int l90 = 0; (l90 < 2048); l90 = (l90 + 1)) {
			fVec2[l90] = 0.0f;
			
		}
		for (int l91 = 0; (l91 < 2); l91 = (l91 + 1)) {
			fRec120[l91] = 0.0f;
			
		}
		for (int l92 = 0; (l92 < 2); l92 = (l92 + 1)) {
			fRec118[l92] = 0.0f;
			
		}
		for (int l93 = 0; (l93 < 2); l93 = (l93 + 1)) {
			fRec126[l93] = 0.0f;
			
		}
		for (int l94 = 0; (l94 < 2); l94 = (l94 + 1)) {
			fRec124[l94] = 0.0f;
			
		}
		for (int l95 = 0; (l95 < 2); l95 = (l95 + 1)) {
			fRec123[l95] = 0.0f;
			
		}
		for (int l96 = 0; (l96 < 2); l96 = (l96 + 1)) {
			fRec121[l96] = 0.0f;
			
		}
		for (int l97 = 0; (l97 < 2); l97 = (l97 + 1)) {
			fRec135[l97] = 0.0f;
			
		}
		for (int l98 = 0; (l98 < 2); l98 = (l98 + 1)) {
			fRec133[l98] = 0.0f;
			
		}
		for (int l99 = 0; (l99 < 2); l99 = (l99 + 1)) {
			fRec132[l99] = 0.0f;
			
		}
		for (int l100 = 0; (l100 < 2); l100 = (l100 + 1)) {
			fRec130[l100] = 0.0f;
			
		}
		for (int l101 = 0; (l101 < 2); l101 = (l101 + 1)) {
			fRec129[l101] = 0.0f;
			
		}
		for (int l102 = 0; (l102 < 2); l102 = (l102 + 1)) {
			fRec127[l102] = 0.0f;
			
		}
		for (int l103 = 0; (l103 < 2); l103 = (l103 + 1)) {
			fRec147[l103] = 0.0f;
			
		}
		for (int l104 = 0; (l104 < 2); l104 = (l104 + 1)) {
			fRec145[l104] = 0.0f;
			
		}
		for (int l105 = 0; (l105 < 2); l105 = (l105 + 1)) {
			fRec144[l105] = 0.0f;
			
		}
		for (int l106 = 0; (l106 < 2); l106 = (l106 + 1)) {
			fRec142[l106] = 0.0f;
			
		}
		for (int l107 = 0; (l107 < 2); l107 = (l107 + 1)) {
			fRec141[l107] = 0.0f;
			
		}
		for (int l108 = 0; (l108 < 2); l108 = (l108 + 1)) {
			fRec139[l108] = 0.0f;
			
		}
		for (int l109 = 0; (l109 < 2); l109 = (l109 + 1)) {
			fRec138[l109] = 0.0f;
			
		}
		for (int l110 = 0; (l110 < 2); l110 = (l110 + 1)) {
			fRec136[l110] = 0.0f;
			
		}
		for (int l111 = 0; (l111 < 2048); l111 = (l111 + 1)) {
			fVec3[l111] = 0.0f;
			
		}
		for (int l112 = 0; (l112 < 2); l112 = (l112 + 1)) {
			fRec150[l112] = 0.0f;
			
		}
		for (int l113 = 0; (l113 < 2); l113 = (l113 + 1)) {
			fRec148[l113] = 0.0f;
			
		}
		for (int l114 = 0; (l114 < 2); l114 = (l114 + 1)) {
			fRec156[l114] = 0.0f;
			
		}
		for (int l115 = 0; (l115 < 2); l115 = (l115 + 1)) {
			fRec154[l115] = 0.0f;
			
		}
		for (int l116 = 0; (l116 < 2); l116 = (l116 + 1)) {
			fRec153[l116] = 0.0f;
			
		}
		for (int l117 = 0; (l117 < 2); l117 = (l117 + 1)) {
			fRec151[l117] = 0.0f;
			
		}
		for (int l118 = 0; (l118 < 2); l118 = (l118 + 1)) {
			fRec165[l118] = 0.0f;
			
		}
		for (int l119 = 0; (l119 < 2); l119 = (l119 + 1)) {
			fRec163[l119] = 0.0f;
			
		}
		for (int l120 = 0; (l120 < 2); l120 = (l120 + 1)) {
			fRec162[l120] = 0.0f;
			
		}
		for (int l121 = 0; (l121 < 2); l121 = (l121 + 1)) {
			fRec160[l121] = 0.0f;
			
		}
		for (int l122 = 0; (l122 < 2); l122 = (l122 + 1)) {
			fRec159[l122] = 0.0f;
			
		}
		for (int l123 = 0; (l123 < 2); l123 = (l123 + 1)) {
			fRec157[l123] = 0.0f;
			
		}
		for (int l124 = 0; (l124 < 2); l124 = (l124 + 1)) {
			fRec177[l124] = 0.0f;
			
		}
		for (int l125 = 0; (l125 < 2); l125 = (l125 + 1)) {
			fRec175[l125] = 0.0f;
			
		}
		for (int l126 = 0; (l126 < 2); l126 = (l126 + 1)) {
			fRec174[l126] = 0.0f;
			
		}
		for (int l127 = 0; (l127 < 2); l127 = (l127 + 1)) {
			fRec172[l127] = 0.0f;
			
		}
		for (int l128 = 0; (l128 < 2); l128 = (l128 + 1)) {
			fRec171[l128] = 0.0f;
			
		}
		for (int l129 = 0; (l129 < 2); l129 = (l129 + 1)) {
			fRec169[l129] = 0.0f;
			
		}
		for (int l130 = 0; (l130 < 2); l130 = (l130 + 1)) {
			fRec168[l130] = 0.0f;
			
		}
		for (int l131 = 0; (l131 < 2); l131 = (l131 + 1)) {
			fRec166[l131] = 0.0f;
			
		}
		for (int l132 = 0; (l132 < 2048); l132 = (l132 + 1)) {
			fVec4[l132] = 0.0f;
			
		}
		for (int l133 = 0; (l133 < 2); l133 = (l133 + 1)) {
			fRec186[l133] = 0.0f;
			
		}
		for (int l134 = 0; (l134 < 2); l134 = (l134 + 1)) {
			fRec184[l134] = 0.0f;
			
		}
		for (int l135 = 0; (l135 < 2); l135 = (l135 + 1)) {
			fRec183[l135] = 0.0f;
			
		}
		for (int l136 = 0; (l136 < 2); l136 = (l136 + 1)) {
			fRec181[l136] = 0.0f;
			
		}
		for (int l137 = 0; (l137 < 2); l137 = (l137 + 1)) {
			fRec180[l137] = 0.0f;
			
		}
		for (int l138 = 0; (l138 < 2); l138 = (l138 + 1)) {
			fRec178[l138] = 0.0f;
			
		}
		for (int l139 = 0; (l139 < 2); l139 = (l139 + 1)) {
			fRec192[l139] = 0.0f;
			
		}
		for (int l140 = 0; (l140 < 2); l140 = (l140 + 1)) {
			fRec190[l140] = 0.0f;
			
		}
		for (int l141 = 0; (l141 < 2); l141 = (l141 + 1)) {
			fRec189[l141] = 0.0f;
			
		}
		for (int l142 = 0; (l142 < 2); l142 = (l142 + 1)) {
			fRec187[l142] = 0.0f;
			
		}
		for (int l143 = 0; (l143 < 2); l143 = (l143 + 1)) {
			fRec204[l143] = 0.0f;
			
		}
		for (int l144 = 0; (l144 < 2); l144 = (l144 + 1)) {
			fRec202[l144] = 0.0f;
			
		}
		for (int l145 = 0; (l145 < 2); l145 = (l145 + 1)) {
			fRec201[l145] = 0.0f;
			
		}
		for (int l146 = 0; (l146 < 2); l146 = (l146 + 1)) {
			fRec199[l146] = 0.0f;
			
		}
		for (int l147 = 0; (l147 < 2); l147 = (l147 + 1)) {
			fRec198[l147] = 0.0f;
			
		}
		for (int l148 = 0; (l148 < 2); l148 = (l148 + 1)) {
			fRec196[l148] = 0.0f;
			
		}
		for (int l149 = 0; (l149 < 2); l149 = (l149 + 1)) {
			fRec195[l149] = 0.0f;
			
		}
		for (int l150 = 0; (l150 < 2); l150 = (l150 + 1)) {
			fRec193[l150] = 0.0f;
			
		}
		for (int l151 = 0; (l151 < 2); l151 = (l151 + 1)) {
			fRec207[l151] = 0.0f;
			
		}
		for (int l152 = 0; (l152 < 2); l152 = (l152 + 1)) {
			fRec205[l152] = 0.0f;
			
		}
		for (int l153 = 0; (l153 < 2048); l153 = (l153 + 1)) {
			fVec5[l153] = 0.0f;
			
		}
		for (int l154 = 0; (l154 < 2); l154 = (l154 + 1)) {
			fRec210[l154] = 0.0f;
			
		}
		for (int l155 = 0; (l155 < 2); l155 = (l155 + 1)) {
			fRec208[l155] = 0.0f;
			
		}
		for (int l156 = 0; (l156 < 2); l156 = (l156 + 1)) {
			fRec216[l156] = 0.0f;
			
		}
		for (int l157 = 0; (l157 < 2); l157 = (l157 + 1)) {
			fRec214[l157] = 0.0f;
			
		}
		for (int l158 = 0; (l158 < 2); l158 = (l158 + 1)) {
			fRec213[l158] = 0.0f;
			
		}
		for (int l159 = 0; (l159 < 2); l159 = (l159 + 1)) {
			fRec211[l159] = 0.0f;
			
		}
		for (int l160 = 0; (l160 < 2); l160 = (l160 + 1)) {
			fRec225[l160] = 0.0f;
			
		}
		for (int l161 = 0; (l161 < 2); l161 = (l161 + 1)) {
			fRec223[l161] = 0.0f;
			
		}
		for (int l162 = 0; (l162 < 2); l162 = (l162 + 1)) {
			fRec222[l162] = 0.0f;
			
		}
		for (int l163 = 0; (l163 < 2); l163 = (l163 + 1)) {
			fRec220[l163] = 0.0f;
			
		}
		for (int l164 = 0; (l164 < 2); l164 = (l164 + 1)) {
			fRec219[l164] = 0.0f;
			
		}
		for (int l165 = 0; (l165 < 2); l165 = (l165 + 1)) {
			fRec217[l165] = 0.0f;
			
		}
		for (int l166 = 0; (l166 < 2); l166 = (l166 + 1)) {
			fRec237[l166] = 0.0f;
			
		}
		for (int l167 = 0; (l167 < 2); l167 = (l167 + 1)) {
			fRec235[l167] = 0.0f;
			
		}
		for (int l168 = 0; (l168 < 2); l168 = (l168 + 1)) {
			fRec234[l168] = 0.0f;
			
		}
		for (int l169 = 0; (l169 < 2); l169 = (l169 + 1)) {
			fRec232[l169] = 0.0f;
			
		}
		for (int l170 = 0; (l170 < 2); l170 = (l170 + 1)) {
			fRec231[l170] = 0.0f;
			
		}
		for (int l171 = 0; (l171 < 2); l171 = (l171 + 1)) {
			fRec229[l171] = 0.0f;
			
		}
		for (int l172 = 0; (l172 < 2); l172 = (l172 + 1)) {
			fRec228[l172] = 0.0f;
			
		}
		for (int l173 = 0; (l173 < 2); l173 = (l173 + 1)) {
			fRec226[l173] = 0.0f;
			
		}
		for (int l174 = 0; (l174 < 2048); l174 = (l174 + 1)) {
			fVec6[l174] = 0.0f;
			
		}
		for (int l175 = 0; (l175 < 2); l175 = (l175 + 1)) {
			fRec240[l175] = 0.0f;
			
		}
		for (int l176 = 0; (l176 < 2); l176 = (l176 + 1)) {
			fRec238[l176] = 0.0f;
			
		}
		for (int l177 = 0; (l177 < 2); l177 = (l177 + 1)) {
			fRec246[l177] = 0.0f;
			
		}
		for (int l178 = 0; (l178 < 2); l178 = (l178 + 1)) {
			fRec244[l178] = 0.0f;
			
		}
		for (int l179 = 0; (l179 < 2); l179 = (l179 + 1)) {
			fRec243[l179] = 0.0f;
			
		}
		for (int l180 = 0; (l180 < 2); l180 = (l180 + 1)) {
			fRec241[l180] = 0.0f;
			
		}
		for (int l181 = 0; (l181 < 2); l181 = (l181 + 1)) {
			fRec255[l181] = 0.0f;
			
		}
		for (int l182 = 0; (l182 < 2); l182 = (l182 + 1)) {
			fRec253[l182] = 0.0f;
			
		}
		for (int l183 = 0; (l183 < 2); l183 = (l183 + 1)) {
			fRec252[l183] = 0.0f;
			
		}
		for (int l184 = 0; (l184 < 2); l184 = (l184 + 1)) {
			fRec250[l184] = 0.0f;
			
		}
		for (int l185 = 0; (l185 < 2); l185 = (l185 + 1)) {
			fRec249[l185] = 0.0f;
			
		}
		for (int l186 = 0; (l186 < 2); l186 = (l186 + 1)) {
			fRec247[l186] = 0.0f;
			
		}
		for (int l187 = 0; (l187 < 2); l187 = (l187 + 1)) {
			fRec267[l187] = 0.0f;
			
		}
		for (int l188 = 0; (l188 < 2); l188 = (l188 + 1)) {
			fRec265[l188] = 0.0f;
			
		}
		for (int l189 = 0; (l189 < 2); l189 = (l189 + 1)) {
			fRec264[l189] = 0.0f;
			
		}
		for (int l190 = 0; (l190 < 2); l190 = (l190 + 1)) {
			fRec262[l190] = 0.0f;
			
		}
		for (int l191 = 0; (l191 < 2); l191 = (l191 + 1)) {
			fRec261[l191] = 0.0f;
			
		}
		for (int l192 = 0; (l192 < 2); l192 = (l192 + 1)) {
			fRec259[l192] = 0.0f;
			
		}
		for (int l193 = 0; (l193 < 2); l193 = (l193 + 1)) {
			fRec258[l193] = 0.0f;
			
		}
		for (int l194 = 0; (l194 < 2); l194 = (l194 + 1)) {
			fRec256[l194] = 0.0f;
			
		}
		for (int l195 = 0; (l195 < 1024); l195 = (l195 + 1)) {
			fVec7[l195] = 0.0f;
			
		}
		for (int l196 = 0; (l196 < 2); l196 = (l196 + 1)) {
			fRec276[l196] = 0.0f;
			
		}
		for (int l197 = 0; (l197 < 2); l197 = (l197 + 1)) {
			fRec274[l197] = 0.0f;
			
		}
		for (int l198 = 0; (l198 < 2); l198 = (l198 + 1)) {
			fRec273[l198] = 0.0f;
			
		}
		for (int l199 = 0; (l199 < 2); l199 = (l199 + 1)) {
			fRec271[l199] = 0.0f;
			
		}
		for (int l200 = 0; (l200 < 2); l200 = (l200 + 1)) {
			fRec270[l200] = 0.0f;
			
		}
		for (int l201 = 0; (l201 < 2); l201 = (l201 + 1)) {
			fRec268[l201] = 0.0f;
			
		}
		for (int l202 = 0; (l202 < 2); l202 = (l202 + 1)) {
			fRec279[l202] = 0.0f;
			
		}
		for (int l203 = 0; (l203 < 2); l203 = (l203 + 1)) {
			fRec277[l203] = 0.0f;
			
		}
		for (int l204 = 0; (l204 < 2); l204 = (l204 + 1)) {
			fRec285[l204] = 0.0f;
			
		}
		for (int l205 = 0; (l205 < 2); l205 = (l205 + 1)) {
			fRec283[l205] = 0.0f;
			
		}
		for (int l206 = 0; (l206 < 2); l206 = (l206 + 1)) {
			fRec282[l206] = 0.0f;
			
		}
		for (int l207 = 0; (l207 < 2); l207 = (l207 + 1)) {
			fRec280[l207] = 0.0f;
			
		}
		for (int l208 = 0; (l208 < 2); l208 = (l208 + 1)) {
			fRec297[l208] = 0.0f;
			
		}
		for (int l209 = 0; (l209 < 2); l209 = (l209 + 1)) {
			fRec295[l209] = 0.0f;
			
		}
		for (int l210 = 0; (l210 < 2); l210 = (l210 + 1)) {
			fRec294[l210] = 0.0f;
			
		}
		for (int l211 = 0; (l211 < 2); l211 = (l211 + 1)) {
			fRec292[l211] = 0.0f;
			
		}
		for (int l212 = 0; (l212 < 2); l212 = (l212 + 1)) {
			fRec291[l212] = 0.0f;
			
		}
		for (int l213 = 0; (l213 < 2); l213 = (l213 + 1)) {
			fRec289[l213] = 0.0f;
			
		}
		for (int l214 = 0; (l214 < 2); l214 = (l214 + 1)) {
			fRec288[l214] = 0.0f;
			
		}
		for (int l215 = 0; (l215 < 2); l215 = (l215 + 1)) {
			fRec286[l215] = 0.0f;
			
		}
		for (int l216 = 0; (l216 < 1024); l216 = (l216 + 1)) {
			fVec8[l216] = 0.0f;
			
		}
		for (int l217 = 0; (l217 < 2); l217 = (l217 + 1)) {
			fRec300[l217] = 0.0f;
			
		}
		for (int l218 = 0; (l218 < 2); l218 = (l218 + 1)) {
			fRec298[l218] = 0.0f;
			
		}
		for (int l219 = 0; (l219 < 2); l219 = (l219 + 1)) {
			fRec306[l219] = 0.0f;
			
		}
		for (int l220 = 0; (l220 < 2); l220 = (l220 + 1)) {
			fRec304[l220] = 0.0f;
			
		}
		for (int l221 = 0; (l221 < 2); l221 = (l221 + 1)) {
			fRec303[l221] = 0.0f;
			
		}
		for (int l222 = 0; (l222 < 2); l222 = (l222 + 1)) {
			fRec301[l222] = 0.0f;
			
		}
		for (int l223 = 0; (l223 < 2); l223 = (l223 + 1)) {
			fRec315[l223] = 0.0f;
			
		}
		for (int l224 = 0; (l224 < 2); l224 = (l224 + 1)) {
			fRec313[l224] = 0.0f;
			
		}
		for (int l225 = 0; (l225 < 2); l225 = (l225 + 1)) {
			fRec312[l225] = 0.0f;
			
		}
		for (int l226 = 0; (l226 < 2); l226 = (l226 + 1)) {
			fRec310[l226] = 0.0f;
			
		}
		for (int l227 = 0; (l227 < 2); l227 = (l227 + 1)) {
			fRec309[l227] = 0.0f;
			
		}
		for (int l228 = 0; (l228 < 2); l228 = (l228 + 1)) {
			fRec307[l228] = 0.0f;
			
		}
		for (int l229 = 0; (l229 < 2); l229 = (l229 + 1)) {
			fRec327[l229] = 0.0f;
			
		}
		for (int l230 = 0; (l230 < 2); l230 = (l230 + 1)) {
			fRec325[l230] = 0.0f;
			
		}
		for (int l231 = 0; (l231 < 2); l231 = (l231 + 1)) {
			fRec324[l231] = 0.0f;
			
		}
		for (int l232 = 0; (l232 < 2); l232 = (l232 + 1)) {
			fRec322[l232] = 0.0f;
			
		}
		for (int l233 = 0; (l233 < 2); l233 = (l233 + 1)) {
			fRec321[l233] = 0.0f;
			
		}
		for (int l234 = 0; (l234 < 2); l234 = (l234 + 1)) {
			fRec319[l234] = 0.0f;
			
		}
		for (int l235 = 0; (l235 < 2); l235 = (l235 + 1)) {
			fRec318[l235] = 0.0f;
			
		}
		for (int l236 = 0; (l236 < 2); l236 = (l236 + 1)) {
			fRec316[l236] = 0.0f;
			
		}
		for (int l237 = 0; (l237 < 1024); l237 = (l237 + 1)) {
			fVec9[l237] = 0.0f;
			
		}
		for (int l238 = 0; (l238 < 2); l238 = (l238 + 1)) {
			fRec339[l238] = 0.0f;
			
		}
		for (int l239 = 0; (l239 < 2); l239 = (l239 + 1)) {
			fRec337[l239] = 0.0f;
			
		}
		for (int l240 = 0; (l240 < 2); l240 = (l240 + 1)) {
			fRec336[l240] = 0.0f;
			
		}
		for (int l241 = 0; (l241 < 2); l241 = (l241 + 1)) {
			fRec334[l241] = 0.0f;
			
		}
		for (int l242 = 0; (l242 < 2); l242 = (l242 + 1)) {
			fRec333[l242] = 0.0f;
			
		}
		for (int l243 = 0; (l243 < 2); l243 = (l243 + 1)) {
			fRec331[l243] = 0.0f;
			
		}
		for (int l244 = 0; (l244 < 2); l244 = (l244 + 1)) {
			fRec330[l244] = 0.0f;
			
		}
		for (int l245 = 0; (l245 < 2); l245 = (l245 + 1)) {
			fRec328[l245] = 0.0f;
			
		}
		for (int l246 = 0; (l246 < 2); l246 = (l246 + 1)) {
			fRec348[l246] = 0.0f;
			
		}
		for (int l247 = 0; (l247 < 2); l247 = (l247 + 1)) {
			fRec346[l247] = 0.0f;
			
		}
		for (int l248 = 0; (l248 < 2); l248 = (l248 + 1)) {
			fRec345[l248] = 0.0f;
			
		}
		for (int l249 = 0; (l249 < 2); l249 = (l249 + 1)) {
			fRec343[l249] = 0.0f;
			
		}
		for (int l250 = 0; (l250 < 2); l250 = (l250 + 1)) {
			fRec342[l250] = 0.0f;
			
		}
		for (int l251 = 0; (l251 < 2); l251 = (l251 + 1)) {
			fRec340[l251] = 0.0f;
			
		}
		for (int l252 = 0; (l252 < 2); l252 = (l252 + 1)) {
			fRec351[l252] = 0.0f;
			
		}
		for (int l253 = 0; (l253 < 2); l253 = (l253 + 1)) {
			fRec349[l253] = 0.0f;
			
		}
		for (int l254 = 0; (l254 < 2); l254 = (l254 + 1)) {
			fRec357[l254] = 0.0f;
			
		}
		for (int l255 = 0; (l255 < 2); l255 = (l255 + 1)) {
			fRec355[l255] = 0.0f;
			
		}
		for (int l256 = 0; (l256 < 2); l256 = (l256 + 1)) {
			fRec354[l256] = 0.0f;
			
		}
		for (int l257 = 0; (l257 < 2); l257 = (l257 + 1)) {
			fRec352[l257] = 0.0f;
			
		}
		for (int l258 = 0; (l258 < 1024); l258 = (l258 + 1)) {
			fVec10[l258] = 0.0f;
			
		}
		for (int l259 = 0; (l259 < 2); l259 = (l259 + 1)) {
			fRec369[l259] = 0.0f;
			
		}
		for (int l260 = 0; (l260 < 2); l260 = (l260 + 1)) {
			fRec367[l260] = 0.0f;
			
		}
		for (int l261 = 0; (l261 < 2); l261 = (l261 + 1)) {
			fRec366[l261] = 0.0f;
			
		}
		for (int l262 = 0; (l262 < 2); l262 = (l262 + 1)) {
			fRec364[l262] = 0.0f;
			
		}
		for (int l263 = 0; (l263 < 2); l263 = (l263 + 1)) {
			fRec363[l263] = 0.0f;
			
		}
		for (int l264 = 0; (l264 < 2); l264 = (l264 + 1)) {
			fRec361[l264] = 0.0f;
			
		}
		for (int l265 = 0; (l265 < 2); l265 = (l265 + 1)) {
			fRec360[l265] = 0.0f;
			
		}
		for (int l266 = 0; (l266 < 2); l266 = (l266 + 1)) {
			fRec358[l266] = 0.0f;
			
		}
		for (int l267 = 0; (l267 < 2); l267 = (l267 + 1)) {
			fRec378[l267] = 0.0f;
			
		}
		for (int l268 = 0; (l268 < 2); l268 = (l268 + 1)) {
			fRec376[l268] = 0.0f;
			
		}
		for (int l269 = 0; (l269 < 2); l269 = (l269 + 1)) {
			fRec375[l269] = 0.0f;
			
		}
		for (int l270 = 0; (l270 < 2); l270 = (l270 + 1)) {
			fRec373[l270] = 0.0f;
			
		}
		for (int l271 = 0; (l271 < 2); l271 = (l271 + 1)) {
			fRec372[l271] = 0.0f;
			
		}
		for (int l272 = 0; (l272 < 2); l272 = (l272 + 1)) {
			fRec370[l272] = 0.0f;
			
		}
		for (int l273 = 0; (l273 < 2); l273 = (l273 + 1)) {
			fRec381[l273] = 0.0f;
			
		}
		for (int l274 = 0; (l274 < 2); l274 = (l274 + 1)) {
			fRec379[l274] = 0.0f;
			
		}
		for (int l275 = 0; (l275 < 2); l275 = (l275 + 1)) {
			fRec387[l275] = 0.0f;
			
		}
		for (int l276 = 0; (l276 < 2); l276 = (l276 + 1)) {
			fRec385[l276] = 0.0f;
			
		}
		for (int l277 = 0; (l277 < 2); l277 = (l277 + 1)) {
			fRec384[l277] = 0.0f;
			
		}
		for (int l278 = 0; (l278 < 2); l278 = (l278 + 1)) {
			fRec382[l278] = 0.0f;
			
		}
		for (int l279 = 0; (l279 < 1024); l279 = (l279 + 1)) {
			fVec11[l279] = 0.0f;
			
		}
		for (int l280 = 0; (l280 < 2); l280 = (l280 + 1)) {
			fRec399[l280] = 0.0f;
			
		}
		for (int l281 = 0; (l281 < 2); l281 = (l281 + 1)) {
			fRec397[l281] = 0.0f;
			
		}
		for (int l282 = 0; (l282 < 2); l282 = (l282 + 1)) {
			fRec396[l282] = 0.0f;
			
		}
		for (int l283 = 0; (l283 < 2); l283 = (l283 + 1)) {
			fRec394[l283] = 0.0f;
			
		}
		for (int l284 = 0; (l284 < 2); l284 = (l284 + 1)) {
			fRec393[l284] = 0.0f;
			
		}
		for (int l285 = 0; (l285 < 2); l285 = (l285 + 1)) {
			fRec391[l285] = 0.0f;
			
		}
		for (int l286 = 0; (l286 < 2); l286 = (l286 + 1)) {
			fRec390[l286] = 0.0f;
			
		}
		for (int l287 = 0; (l287 < 2); l287 = (l287 + 1)) {
			fRec388[l287] = 0.0f;
			
		}
		for (int l288 = 0; (l288 < 2); l288 = (l288 + 1)) {
			fRec408[l288] = 0.0f;
			
		}
		for (int l289 = 0; (l289 < 2); l289 = (l289 + 1)) {
			fRec406[l289] = 0.0f;
			
		}
		for (int l290 = 0; (l290 < 2); l290 = (l290 + 1)) {
			fRec405[l290] = 0.0f;
			
		}
		for (int l291 = 0; (l291 < 2); l291 = (l291 + 1)) {
			fRec403[l291] = 0.0f;
			
		}
		for (int l292 = 0; (l292 < 2); l292 = (l292 + 1)) {
			fRec402[l292] = 0.0f;
			
		}
		for (int l293 = 0; (l293 < 2); l293 = (l293 + 1)) {
			fRec400[l293] = 0.0f;
			
		}
		for (int l294 = 0; (l294 < 2); l294 = (l294 + 1)) {
			fRec411[l294] = 0.0f;
			
		}
		for (int l295 = 0; (l295 < 2); l295 = (l295 + 1)) {
			fRec409[l295] = 0.0f;
			
		}
		for (int l296 = 0; (l296 < 2); l296 = (l296 + 1)) {
			fRec417[l296] = 0.0f;
			
		}
		for (int l297 = 0; (l297 < 2); l297 = (l297 + 1)) {
			fRec415[l297] = 0.0f;
			
		}
		for (int l298 = 0; (l298 < 2); l298 = (l298 + 1)) {
			fRec414[l298] = 0.0f;
			
		}
		for (int l299 = 0; (l299 < 2); l299 = (l299 + 1)) {
			fRec412[l299] = 0.0f;
			
		}
		for (int l300 = 0; (l300 < 1024); l300 = (l300 + 1)) {
			fVec12[l300] = 0.0f;
			
		}
		for (int l301 = 0; (l301 < 2); l301 = (l301 + 1)) {
			fRec429[l301] = 0.0f;
			
		}
		for (int l302 = 0; (l302 < 2); l302 = (l302 + 1)) {
			fRec427[l302] = 0.0f;
			
		}
		for (int l303 = 0; (l303 < 2); l303 = (l303 + 1)) {
			fRec426[l303] = 0.0f;
			
		}
		for (int l304 = 0; (l304 < 2); l304 = (l304 + 1)) {
			fRec424[l304] = 0.0f;
			
		}
		for (int l305 = 0; (l305 < 2); l305 = (l305 + 1)) {
			fRec423[l305] = 0.0f;
			
		}
		for (int l306 = 0; (l306 < 2); l306 = (l306 + 1)) {
			fRec421[l306] = 0.0f;
			
		}
		for (int l307 = 0; (l307 < 2); l307 = (l307 + 1)) {
			fRec420[l307] = 0.0f;
			
		}
		for (int l308 = 0; (l308 < 2); l308 = (l308 + 1)) {
			fRec418[l308] = 0.0f;
			
		}
		for (int l309 = 0; (l309 < 2); l309 = (l309 + 1)) {
			fRec438[l309] = 0.0f;
			
		}
		for (int l310 = 0; (l310 < 2); l310 = (l310 + 1)) {
			fRec436[l310] = 0.0f;
			
		}
		for (int l311 = 0; (l311 < 2); l311 = (l311 + 1)) {
			fRec435[l311] = 0.0f;
			
		}
		for (int l312 = 0; (l312 < 2); l312 = (l312 + 1)) {
			fRec433[l312] = 0.0f;
			
		}
		for (int l313 = 0; (l313 < 2); l313 = (l313 + 1)) {
			fRec432[l313] = 0.0f;
			
		}
		for (int l314 = 0; (l314 < 2); l314 = (l314 + 1)) {
			fRec430[l314] = 0.0f;
			
		}
		for (int l315 = 0; (l315 < 2); l315 = (l315 + 1)) {
			fRec441[l315] = 0.0f;
			
		}
		for (int l316 = 0; (l316 < 2); l316 = (l316 + 1)) {
			fRec439[l316] = 0.0f;
			
		}
		for (int l317 = 0; (l317 < 2); l317 = (l317 + 1)) {
			fRec447[l317] = 0.0f;
			
		}
		for (int l318 = 0; (l318 < 2); l318 = (l318 + 1)) {
			fRec445[l318] = 0.0f;
			
		}
		for (int l319 = 0; (l319 < 2); l319 = (l319 + 1)) {
			fRec444[l319] = 0.0f;
			
		}
		for (int l320 = 0; (l320 < 2); l320 = (l320 + 1)) {
			fRec442[l320] = 0.0f;
			
		}
		for (int l321 = 0; (l321 < 1024); l321 = (l321 + 1)) {
			fVec13[l321] = 0.0f;
			
		}
		for (int l322 = 0; (l322 < 2); l322 = (l322 + 1)) {
			fRec459[l322] = 0.0f;
			
		}
		for (int l323 = 0; (l323 < 2); l323 = (l323 + 1)) {
			fRec457[l323] = 0.0f;
			
		}
		for (int l324 = 0; (l324 < 2); l324 = (l324 + 1)) {
			fRec456[l324] = 0.0f;
			
		}
		for (int l325 = 0; (l325 < 2); l325 = (l325 + 1)) {
			fRec454[l325] = 0.0f;
			
		}
		for (int l326 = 0; (l326 < 2); l326 = (l326 + 1)) {
			fRec453[l326] = 0.0f;
			
		}
		for (int l327 = 0; (l327 < 2); l327 = (l327 + 1)) {
			fRec451[l327] = 0.0f;
			
		}
		for (int l328 = 0; (l328 < 2); l328 = (l328 + 1)) {
			fRec450[l328] = 0.0f;
			
		}
		for (int l329 = 0; (l329 < 2); l329 = (l329 + 1)) {
			fRec448[l329] = 0.0f;
			
		}
		for (int l330 = 0; (l330 < 2); l330 = (l330 + 1)) {
			fRec468[l330] = 0.0f;
			
		}
		for (int l331 = 0; (l331 < 2); l331 = (l331 + 1)) {
			fRec466[l331] = 0.0f;
			
		}
		for (int l332 = 0; (l332 < 2); l332 = (l332 + 1)) {
			fRec465[l332] = 0.0f;
			
		}
		for (int l333 = 0; (l333 < 2); l333 = (l333 + 1)) {
			fRec463[l333] = 0.0f;
			
		}
		for (int l334 = 0; (l334 < 2); l334 = (l334 + 1)) {
			fRec462[l334] = 0.0f;
			
		}
		for (int l335 = 0; (l335 < 2); l335 = (l335 + 1)) {
			fRec460[l335] = 0.0f;
			
		}
		for (int l336 = 0; (l336 < 2); l336 = (l336 + 1)) {
			fRec471[l336] = 0.0f;
			
		}
		for (int l337 = 0; (l337 < 2); l337 = (l337 + 1)) {
			fRec469[l337] = 0.0f;
			
		}
		for (int l338 = 0; (l338 < 2); l338 = (l338 + 1)) {
			fRec477[l338] = 0.0f;
			
		}
		for (int l339 = 0; (l339 < 2); l339 = (l339 + 1)) {
			fRec475[l339] = 0.0f;
			
		}
		for (int l340 = 0; (l340 < 2); l340 = (l340 + 1)) {
			fRec474[l340] = 0.0f;
			
		}
		for (int l341 = 0; (l341 < 2); l341 = (l341 + 1)) {
			fRec472[l341] = 0.0f;
			
		}
		for (int l342 = 0; (l342 < 1024); l342 = (l342 + 1)) {
			fVec14[l342] = 0.0f;
			
		}
		for (int l343 = 0; (l343 < 2); l343 = (l343 + 1)) {
			fRec489[l343] = 0.0f;
			
		}
		for (int l344 = 0; (l344 < 2); l344 = (l344 + 1)) {
			fRec487[l344] = 0.0f;
			
		}
		for (int l345 = 0; (l345 < 2); l345 = (l345 + 1)) {
			fRec486[l345] = 0.0f;
			
		}
		for (int l346 = 0; (l346 < 2); l346 = (l346 + 1)) {
			fRec484[l346] = 0.0f;
			
		}
		for (int l347 = 0; (l347 < 2); l347 = (l347 + 1)) {
			fRec483[l347] = 0.0f;
			
		}
		for (int l348 = 0; (l348 < 2); l348 = (l348 + 1)) {
			fRec481[l348] = 0.0f;
			
		}
		for (int l349 = 0; (l349 < 2); l349 = (l349 + 1)) {
			fRec480[l349] = 0.0f;
			
		}
		for (int l350 = 0; (l350 < 2); l350 = (l350 + 1)) {
			fRec478[l350] = 0.0f;
			
		}
		for (int l351 = 0; (l351 < 2); l351 = (l351 + 1)) {
			fRec498[l351] = 0.0f;
			
		}
		for (int l352 = 0; (l352 < 2); l352 = (l352 + 1)) {
			fRec496[l352] = 0.0f;
			
		}
		for (int l353 = 0; (l353 < 2); l353 = (l353 + 1)) {
			fRec495[l353] = 0.0f;
			
		}
		for (int l354 = 0; (l354 < 2); l354 = (l354 + 1)) {
			fRec493[l354] = 0.0f;
			
		}
		for (int l355 = 0; (l355 < 2); l355 = (l355 + 1)) {
			fRec492[l355] = 0.0f;
			
		}
		for (int l356 = 0; (l356 < 2); l356 = (l356 + 1)) {
			fRec490[l356] = 0.0f;
			
		}
		for (int l357 = 0; (l357 < 2); l357 = (l357 + 1)) {
			fRec501[l357] = 0.0f;
			
		}
		for (int l358 = 0; (l358 < 2); l358 = (l358 + 1)) {
			fRec499[l358] = 0.0f;
			
		}
		for (int l359 = 0; (l359 < 2); l359 = (l359 + 1)) {
			fRec507[l359] = 0.0f;
			
		}
		for (int l360 = 0; (l360 < 2); l360 = (l360 + 1)) {
			fRec505[l360] = 0.0f;
			
		}
		for (int l361 = 0; (l361 < 2); l361 = (l361 + 1)) {
			fRec504[l361] = 0.0f;
			
		}
		for (int l362 = 0; (l362 < 2); l362 = (l362 + 1)) {
			fRec502[l362] = 0.0f;
			
		}
		for (int l363 = 0; (l363 < 1024); l363 = (l363 + 1)) {
			fVec15[l363] = 0.0f;
			
		}
		for (int l364 = 0; (l364 < 2); l364 = (l364 + 1)) {
			fRec513[l364] = 0.0f;
			
		}
		for (int l365 = 0; (l365 < 2); l365 = (l365 + 1)) {
			fRec511[l365] = 0.0f;
			
		}
		for (int l366 = 0; (l366 < 2); l366 = (l366 + 1)) {
			fRec510[l366] = 0.0f;
			
		}
		for (int l367 = 0; (l367 < 2); l367 = (l367 + 1)) {
			fRec508[l367] = 0.0f;
			
		}
		for (int l368 = 0; (l368 < 2); l368 = (l368 + 1)) {
			fRec516[l368] = 0.0f;
			
		}
		for (int l369 = 0; (l369 < 2); l369 = (l369 + 1)) {
			fRec514[l369] = 0.0f;
			
		}
		for (int l370 = 0; (l370 < 2); l370 = (l370 + 1)) {
			fRec525[l370] = 0.0f;
			
		}
		for (int l371 = 0; (l371 < 2); l371 = (l371 + 1)) {
			fRec523[l371] = 0.0f;
			
		}
		for (int l372 = 0; (l372 < 2); l372 = (l372 + 1)) {
			fRec522[l372] = 0.0f;
			
		}
		for (int l373 = 0; (l373 < 2); l373 = (l373 + 1)) {
			fRec520[l373] = 0.0f;
			
		}
		for (int l374 = 0; (l374 < 2); l374 = (l374 + 1)) {
			fRec519[l374] = 0.0f;
			
		}
		for (int l375 = 0; (l375 < 2); l375 = (l375 + 1)) {
			fRec517[l375] = 0.0f;
			
		}
		for (int l376 = 0; (l376 < 2); l376 = (l376 + 1)) {
			fRec537[l376] = 0.0f;
			
		}
		for (int l377 = 0; (l377 < 2); l377 = (l377 + 1)) {
			fRec535[l377] = 0.0f;
			
		}
		for (int l378 = 0; (l378 < 2); l378 = (l378 + 1)) {
			fRec534[l378] = 0.0f;
			
		}
		for (int l379 = 0; (l379 < 2); l379 = (l379 + 1)) {
			fRec532[l379] = 0.0f;
			
		}
		for (int l380 = 0; (l380 < 2); l380 = (l380 + 1)) {
			fRec531[l380] = 0.0f;
			
		}
		for (int l381 = 0; (l381 < 2); l381 = (l381 + 1)) {
			fRec529[l381] = 0.0f;
			
		}
		for (int l382 = 0; (l382 < 2); l382 = (l382 + 1)) {
			fRec528[l382] = 0.0f;
			
		}
		for (int l383 = 0; (l383 < 2); l383 = (l383 + 1)) {
			fRec526[l383] = 0.0f;
			
		}
		for (int l384 = 0; (l384 < 1024); l384 = (l384 + 1)) {
			fVec16[l384] = 0.0f;
			
		}
		for (int l385 = 0; (l385 < 2); l385 = (l385 + 1)) {
			fRec549[l385] = 0.0f;
			
		}
		for (int l386 = 0; (l386 < 2); l386 = (l386 + 1)) {
			fRec547[l386] = 0.0f;
			
		}
		for (int l387 = 0; (l387 < 2); l387 = (l387 + 1)) {
			fRec546[l387] = 0.0f;
			
		}
		for (int l388 = 0; (l388 < 2); l388 = (l388 + 1)) {
			fRec544[l388] = 0.0f;
			
		}
		for (int l389 = 0; (l389 < 2); l389 = (l389 + 1)) {
			fRec543[l389] = 0.0f;
			
		}
		for (int l390 = 0; (l390 < 2); l390 = (l390 + 1)) {
			fRec541[l390] = 0.0f;
			
		}
		for (int l391 = 0; (l391 < 2); l391 = (l391 + 1)) {
			fRec540[l391] = 0.0f;
			
		}
		for (int l392 = 0; (l392 < 2); l392 = (l392 + 1)) {
			fRec538[l392] = 0.0f;
			
		}
		for (int l393 = 0; (l393 < 2); l393 = (l393 + 1)) {
			fRec558[l393] = 0.0f;
			
		}
		for (int l394 = 0; (l394 < 2); l394 = (l394 + 1)) {
			fRec556[l394] = 0.0f;
			
		}
		for (int l395 = 0; (l395 < 2); l395 = (l395 + 1)) {
			fRec555[l395] = 0.0f;
			
		}
		for (int l396 = 0; (l396 < 2); l396 = (l396 + 1)) {
			fRec553[l396] = 0.0f;
			
		}
		for (int l397 = 0; (l397 < 2); l397 = (l397 + 1)) {
			fRec552[l397] = 0.0f;
			
		}
		for (int l398 = 0; (l398 < 2); l398 = (l398 + 1)) {
			fRec550[l398] = 0.0f;
			
		}
		for (int l399 = 0; (l399 < 2); l399 = (l399 + 1)) {
			fRec561[l399] = 0.0f;
			
		}
		for (int l400 = 0; (l400 < 2); l400 = (l400 + 1)) {
			fRec559[l400] = 0.0f;
			
		}
		for (int l401 = 0; (l401 < 2); l401 = (l401 + 1)) {
			fRec567[l401] = 0.0f;
			
		}
		for (int l402 = 0; (l402 < 2); l402 = (l402 + 1)) {
			fRec565[l402] = 0.0f;
			
		}
		for (int l403 = 0; (l403 < 2); l403 = (l403 + 1)) {
			fRec564[l403] = 0.0f;
			
		}
		for (int l404 = 0; (l404 < 2); l404 = (l404 + 1)) {
			fRec562[l404] = 0.0f;
			
		}
		for (int l405 = 0; (l405 < 1024); l405 = (l405 + 1)) {
			fVec17[l405] = 0.0f;
			
		}
		for (int l406 = 0; (l406 < 2); l406 = (l406 + 1)) {
			fRec579[l406] = 0.0f;
			
		}
		for (int l407 = 0; (l407 < 2); l407 = (l407 + 1)) {
			fRec577[l407] = 0.0f;
			
		}
		for (int l408 = 0; (l408 < 2); l408 = (l408 + 1)) {
			fRec576[l408] = 0.0f;
			
		}
		for (int l409 = 0; (l409 < 2); l409 = (l409 + 1)) {
			fRec574[l409] = 0.0f;
			
		}
		for (int l410 = 0; (l410 < 2); l410 = (l410 + 1)) {
			fRec573[l410] = 0.0f;
			
		}
		for (int l411 = 0; (l411 < 2); l411 = (l411 + 1)) {
			fRec571[l411] = 0.0f;
			
		}
		for (int l412 = 0; (l412 < 2); l412 = (l412 + 1)) {
			fRec570[l412] = 0.0f;
			
		}
		for (int l413 = 0; (l413 < 2); l413 = (l413 + 1)) {
			fRec568[l413] = 0.0f;
			
		}
		for (int l414 = 0; (l414 < 2); l414 = (l414 + 1)) {
			fRec588[l414] = 0.0f;
			
		}
		for (int l415 = 0; (l415 < 2); l415 = (l415 + 1)) {
			fRec586[l415] = 0.0f;
			
		}
		for (int l416 = 0; (l416 < 2); l416 = (l416 + 1)) {
			fRec585[l416] = 0.0f;
			
		}
		for (int l417 = 0; (l417 < 2); l417 = (l417 + 1)) {
			fRec583[l417] = 0.0f;
			
		}
		for (int l418 = 0; (l418 < 2); l418 = (l418 + 1)) {
			fRec582[l418] = 0.0f;
			
		}
		for (int l419 = 0; (l419 < 2); l419 = (l419 + 1)) {
			fRec580[l419] = 0.0f;
			
		}
		for (int l420 = 0; (l420 < 2); l420 = (l420 + 1)) {
			fRec591[l420] = 0.0f;
			
		}
		for (int l421 = 0; (l421 < 2); l421 = (l421 + 1)) {
			fRec589[l421] = 0.0f;
			
		}
		for (int l422 = 0; (l422 < 2); l422 = (l422 + 1)) {
			fRec597[l422] = 0.0f;
			
		}
		for (int l423 = 0; (l423 < 2); l423 = (l423 + 1)) {
			fRec595[l423] = 0.0f;
			
		}
		for (int l424 = 0; (l424 < 2); l424 = (l424 + 1)) {
			fRec594[l424] = 0.0f;
			
		}
		for (int l425 = 0; (l425 < 2); l425 = (l425 + 1)) {
			fRec592[l425] = 0.0f;
			
		}
		for (int l426 = 0; (l426 < 1024); l426 = (l426 + 1)) {
			fVec18[l426] = 0.0f;
			
		}
		for (int l427 = 0; (l427 < 2); l427 = (l427 + 1)) {
			fRec609[l427] = 0.0f;
			
		}
		for (int l428 = 0; (l428 < 2); l428 = (l428 + 1)) {
			fRec607[l428] = 0.0f;
			
		}
		for (int l429 = 0; (l429 < 2); l429 = (l429 + 1)) {
			fRec606[l429] = 0.0f;
			
		}
		for (int l430 = 0; (l430 < 2); l430 = (l430 + 1)) {
			fRec604[l430] = 0.0f;
			
		}
		for (int l431 = 0; (l431 < 2); l431 = (l431 + 1)) {
			fRec603[l431] = 0.0f;
			
		}
		for (int l432 = 0; (l432 < 2); l432 = (l432 + 1)) {
			fRec601[l432] = 0.0f;
			
		}
		for (int l433 = 0; (l433 < 2); l433 = (l433 + 1)) {
			fRec600[l433] = 0.0f;
			
		}
		for (int l434 = 0; (l434 < 2); l434 = (l434 + 1)) {
			fRec598[l434] = 0.0f;
			
		}
		for (int l435 = 0; (l435 < 2); l435 = (l435 + 1)) {
			fRec618[l435] = 0.0f;
			
		}
		for (int l436 = 0; (l436 < 2); l436 = (l436 + 1)) {
			fRec616[l436] = 0.0f;
			
		}
		for (int l437 = 0; (l437 < 2); l437 = (l437 + 1)) {
			fRec615[l437] = 0.0f;
			
		}
		for (int l438 = 0; (l438 < 2); l438 = (l438 + 1)) {
			fRec613[l438] = 0.0f;
			
		}
		for (int l439 = 0; (l439 < 2); l439 = (l439 + 1)) {
			fRec612[l439] = 0.0f;
			
		}
		for (int l440 = 0; (l440 < 2); l440 = (l440 + 1)) {
			fRec610[l440] = 0.0f;
			
		}
		for (int l441 = 0; (l441 < 2); l441 = (l441 + 1)) {
			fRec621[l441] = 0.0f;
			
		}
		for (int l442 = 0; (l442 < 2); l442 = (l442 + 1)) {
			fRec619[l442] = 0.0f;
			
		}
		for (int l443 = 0; (l443 < 2); l443 = (l443 + 1)) {
			fRec627[l443] = 0.0f;
			
		}
		for (int l444 = 0; (l444 < 2); l444 = (l444 + 1)) {
			fRec625[l444] = 0.0f;
			
		}
		for (int l445 = 0; (l445 < 2); l445 = (l445 + 1)) {
			fRec624[l445] = 0.0f;
			
		}
		for (int l446 = 0; (l446 < 2); l446 = (l446 + 1)) {
			fRec622[l446] = 0.0f;
			
		}
		for (int l447 = 0; (l447 < 3); l447 = (l447 + 1)) {
			fVec19[l447] = 0.0f;
			
		}
		for (int l448 = 0; (l448 < 2); l448 = (l448 + 1)) {
			fRec630[l448] = 0.0f;
			
		}
		for (int l449 = 0; (l449 < 2); l449 = (l449 + 1)) {
			fRec628[l449] = 0.0f;
			
		}
		for (int l450 = 0; (l450 < 2); l450 = (l450 + 1)) {
			fRec642[l450] = 0.0f;
			
		}
		for (int l451 = 0; (l451 < 2); l451 = (l451 + 1)) {
			fRec640[l451] = 0.0f;
			
		}
		for (int l452 = 0; (l452 < 2); l452 = (l452 + 1)) {
			fRec639[l452] = 0.0f;
			
		}
		for (int l453 = 0; (l453 < 2); l453 = (l453 + 1)) {
			fRec637[l453] = 0.0f;
			
		}
		for (int l454 = 0; (l454 < 2); l454 = (l454 + 1)) {
			fRec636[l454] = 0.0f;
			
		}
		for (int l455 = 0; (l455 < 2); l455 = (l455 + 1)) {
			fRec634[l455] = 0.0f;
			
		}
		for (int l456 = 0; (l456 < 2); l456 = (l456 + 1)) {
			fRec633[l456] = 0.0f;
			
		}
		for (int l457 = 0; (l457 < 2); l457 = (l457 + 1)) {
			fRec631[l457] = 0.0f;
			
		}
		for (int l458 = 0; (l458 < 2); l458 = (l458 + 1)) {
			fRec648[l458] = 0.0f;
			
		}
		for (int l459 = 0; (l459 < 2); l459 = (l459 + 1)) {
			fRec646[l459] = 0.0f;
			
		}
		for (int l460 = 0; (l460 < 2); l460 = (l460 + 1)) {
			fRec645[l460] = 0.0f;
			
		}
		for (int l461 = 0; (l461 < 2); l461 = (l461 + 1)) {
			fRec643[l461] = 0.0f;
			
		}
		for (int l462 = 0; (l462 < 2); l462 = (l462 + 1)) {
			fRec657[l462] = 0.0f;
			
		}
		for (int l463 = 0; (l463 < 2); l463 = (l463 + 1)) {
			fRec655[l463] = 0.0f;
			
		}
		for (int l464 = 0; (l464 < 2); l464 = (l464 + 1)) {
			fRec654[l464] = 0.0f;
			
		}
		for (int l465 = 0; (l465 < 2); l465 = (l465 + 1)) {
			fRec652[l465] = 0.0f;
			
		}
		for (int l466 = 0; (l466 < 2); l466 = (l466 + 1)) {
			fRec651[l466] = 0.0f;
			
		}
		for (int l467 = 0; (l467 < 2); l467 = (l467 + 1)) {
			fRec649[l467] = 0.0f;
			
		}
		for (int l468 = 0; (l468 < 2); l468 = (l468 + 1)) {
			fRec669[l468] = 0.0f;
			
		}
		for (int l469 = 0; (l469 < 2); l469 = (l469 + 1)) {
			fRec667[l469] = 0.0f;
			
		}
		for (int l470 = 0; (l470 < 2); l470 = (l470 + 1)) {
			fRec666[l470] = 0.0f;
			
		}
		for (int l471 = 0; (l471 < 2); l471 = (l471 + 1)) {
			fRec664[l471] = 0.0f;
			
		}
		for (int l472 = 0; (l472 < 2); l472 = (l472 + 1)) {
			fRec663[l472] = 0.0f;
			
		}
		for (int l473 = 0; (l473 < 2); l473 = (l473 + 1)) {
			fRec661[l473] = 0.0f;
			
		}
		for (int l474 = 0; (l474 < 2); l474 = (l474 + 1)) {
			fRec660[l474] = 0.0f;
			
		}
		for (int l475 = 0; (l475 < 2); l475 = (l475 + 1)) {
			fRec658[l475] = 0.0f;
			
		}
		for (int l476 = 0; (l476 < 2); l476 = (l476 + 1)) {
			fRec678[l476] = 0.0f;
			
		}
		for (int l477 = 0; (l477 < 2); l477 = (l477 + 1)) {
			fRec676[l477] = 0.0f;
			
		}
		for (int l478 = 0; (l478 < 2); l478 = (l478 + 1)) {
			fRec675[l478] = 0.0f;
			
		}
		for (int l479 = 0; (l479 < 2); l479 = (l479 + 1)) {
			fRec673[l479] = 0.0f;
			
		}
		for (int l480 = 0; (l480 < 2); l480 = (l480 + 1)) {
			fRec672[l480] = 0.0f;
			
		}
		for (int l481 = 0; (l481 < 2); l481 = (l481 + 1)) {
			fRec670[l481] = 0.0f;
			
		}
		for (int l482 = 0; (l482 < 2); l482 = (l482 + 1)) {
			fRec681[l482] = 0.0f;
			
		}
		for (int l483 = 0; (l483 < 2); l483 = (l483 + 1)) {
			fRec679[l483] = 0.0f;
			
		}
		for (int l484 = 0; (l484 < 2); l484 = (l484 + 1)) {
			fRec687[l484] = 0.0f;
			
		}
		for (int l485 = 0; (l485 < 2); l485 = (l485 + 1)) {
			fRec685[l485] = 0.0f;
			
		}
		for (int l486 = 0; (l486 < 2); l486 = (l486 + 1)) {
			fRec684[l486] = 0.0f;
			
		}
		for (int l487 = 0; (l487 < 2); l487 = (l487 + 1)) {
			fRec682[l487] = 0.0f;
			
		}
		for (int l488 = 0; (l488 < 2); l488 = (l488 + 1)) {
			fRec699[l488] = 0.0f;
			
		}
		for (int l489 = 0; (l489 < 2); l489 = (l489 + 1)) {
			fRec697[l489] = 0.0f;
			
		}
		for (int l490 = 0; (l490 < 2); l490 = (l490 + 1)) {
			fRec696[l490] = 0.0f;
			
		}
		for (int l491 = 0; (l491 < 2); l491 = (l491 + 1)) {
			fRec694[l491] = 0.0f;
			
		}
		for (int l492 = 0; (l492 < 2); l492 = (l492 + 1)) {
			fRec693[l492] = 0.0f;
			
		}
		for (int l493 = 0; (l493 < 2); l493 = (l493 + 1)) {
			fRec691[l493] = 0.0f;
			
		}
		for (int l494 = 0; (l494 < 2); l494 = (l494 + 1)) {
			fRec690[l494] = 0.0f;
			
		}
		for (int l495 = 0; (l495 < 2); l495 = (l495 + 1)) {
			fRec688[l495] = 0.0f;
			
		}
		for (int l496 = 0; (l496 < 2); l496 = (l496 + 1)) {
			fRec708[l496] = 0.0f;
			
		}
		for (int l497 = 0; (l497 < 2); l497 = (l497 + 1)) {
			fRec706[l497] = 0.0f;
			
		}
		for (int l498 = 0; (l498 < 2); l498 = (l498 + 1)) {
			fRec705[l498] = 0.0f;
			
		}
		for (int l499 = 0; (l499 < 2); l499 = (l499 + 1)) {
			fRec703[l499] = 0.0f;
			
		}
		for (int l500 = 0; (l500 < 2); l500 = (l500 + 1)) {
			fRec702[l500] = 0.0f;
			
		}
		for (int l501 = 0; (l501 < 2); l501 = (l501 + 1)) {
			fRec700[l501] = 0.0f;
			
		}
		for (int l502 = 0; (l502 < 2); l502 = (l502 + 1)) {
			fRec711[l502] = 0.0f;
			
		}
		for (int l503 = 0; (l503 < 2); l503 = (l503 + 1)) {
			fRec709[l503] = 0.0f;
			
		}
		for (int l504 = 0; (l504 < 2); l504 = (l504 + 1)) {
			fRec717[l504] = 0.0f;
			
		}
		for (int l505 = 0; (l505 < 2); l505 = (l505 + 1)) {
			fRec715[l505] = 0.0f;
			
		}
		for (int l506 = 0; (l506 < 2); l506 = (l506 + 1)) {
			fRec714[l506] = 0.0f;
			
		}
		for (int l507 = 0; (l507 < 2); l507 = (l507 + 1)) {
			fRec712[l507] = 0.0f;
			
		}
		for (int l508 = 0; (l508 < 3); l508 = (l508 + 1)) {
			fVec20[l508] = 0.0f;
			
		}
		for (int l509 = 0; (l509 < 2); l509 = (l509 + 1)) {
			fRec729[l509] = 0.0f;
			
		}
		for (int l510 = 0; (l510 < 2); l510 = (l510 + 1)) {
			fRec727[l510] = 0.0f;
			
		}
		for (int l511 = 0; (l511 < 2); l511 = (l511 + 1)) {
			fRec726[l511] = 0.0f;
			
		}
		for (int l512 = 0; (l512 < 2); l512 = (l512 + 1)) {
			fRec724[l512] = 0.0f;
			
		}
		for (int l513 = 0; (l513 < 2); l513 = (l513 + 1)) {
			fRec723[l513] = 0.0f;
			
		}
		for (int l514 = 0; (l514 < 2); l514 = (l514 + 1)) {
			fRec721[l514] = 0.0f;
			
		}
		for (int l515 = 0; (l515 < 2); l515 = (l515 + 1)) {
			fRec720[l515] = 0.0f;
			
		}
		for (int l516 = 0; (l516 < 2); l516 = (l516 + 1)) {
			fRec718[l516] = 0.0f;
			
		}
		for (int l517 = 0; (l517 < 2); l517 = (l517 + 1)) {
			fRec738[l517] = 0.0f;
			
		}
		for (int l518 = 0; (l518 < 2); l518 = (l518 + 1)) {
			fRec736[l518] = 0.0f;
			
		}
		for (int l519 = 0; (l519 < 2); l519 = (l519 + 1)) {
			fRec735[l519] = 0.0f;
			
		}
		for (int l520 = 0; (l520 < 2); l520 = (l520 + 1)) {
			fRec733[l520] = 0.0f;
			
		}
		for (int l521 = 0; (l521 < 2); l521 = (l521 + 1)) {
			fRec732[l521] = 0.0f;
			
		}
		for (int l522 = 0; (l522 < 2); l522 = (l522 + 1)) {
			fRec730[l522] = 0.0f;
			
		}
		for (int l523 = 0; (l523 < 2); l523 = (l523 + 1)) {
			fRec741[l523] = 0.0f;
			
		}
		for (int l524 = 0; (l524 < 2); l524 = (l524 + 1)) {
			fRec739[l524] = 0.0f;
			
		}
		for (int l525 = 0; (l525 < 2); l525 = (l525 + 1)) {
			fRec747[l525] = 0.0f;
			
		}
		for (int l526 = 0; (l526 < 2); l526 = (l526 + 1)) {
			fRec745[l526] = 0.0f;
			
		}
		for (int l527 = 0; (l527 < 2); l527 = (l527 + 1)) {
			fRec744[l527] = 0.0f;
			
		}
		for (int l528 = 0; (l528 < 2); l528 = (l528 + 1)) {
			fRec742[l528] = 0.0f;
			
		}
		for (int l529 = 0; (l529 < 2); l529 = (l529 + 1)) {
			fRec756[l529] = 0.0f;
			
		}
		for (int l530 = 0; (l530 < 2); l530 = (l530 + 1)) {
			fRec754[l530] = 0.0f;
			
		}
		for (int l531 = 0; (l531 < 2); l531 = (l531 + 1)) {
			fRec753[l531] = 0.0f;
			
		}
		for (int l532 = 0; (l532 < 2); l532 = (l532 + 1)) {
			fRec751[l532] = 0.0f;
			
		}
		for (int l533 = 0; (l533 < 2); l533 = (l533 + 1)) {
			fRec750[l533] = 0.0f;
			
		}
		for (int l534 = 0; (l534 < 2); l534 = (l534 + 1)) {
			fRec748[l534] = 0.0f;
			
		}
		for (int l535 = 0; (l535 < 2); l535 = (l535 + 1)) {
			fRec762[l535] = 0.0f;
			
		}
		for (int l536 = 0; (l536 < 2); l536 = (l536 + 1)) {
			fRec760[l536] = 0.0f;
			
		}
		for (int l537 = 0; (l537 < 2); l537 = (l537 + 1)) {
			fRec759[l537] = 0.0f;
			
		}
		for (int l538 = 0; (l538 < 2); l538 = (l538 + 1)) {
			fRec757[l538] = 0.0f;
			
		}
		for (int l539 = 0; (l539 < 2); l539 = (l539 + 1)) {
			fRec774[l539] = 0.0f;
			
		}
		for (int l540 = 0; (l540 < 2); l540 = (l540 + 1)) {
			fRec772[l540] = 0.0f;
			
		}
		for (int l541 = 0; (l541 < 2); l541 = (l541 + 1)) {
			fRec771[l541] = 0.0f;
			
		}
		for (int l542 = 0; (l542 < 2); l542 = (l542 + 1)) {
			fRec769[l542] = 0.0f;
			
		}
		for (int l543 = 0; (l543 < 2); l543 = (l543 + 1)) {
			fRec768[l543] = 0.0f;
			
		}
		for (int l544 = 0; (l544 < 2); l544 = (l544 + 1)) {
			fRec766[l544] = 0.0f;
			
		}
		for (int l545 = 0; (l545 < 2); l545 = (l545 + 1)) {
			fRec765[l545] = 0.0f;
			
		}
		for (int l546 = 0; (l546 < 2); l546 = (l546 + 1)) {
			fRec763[l546] = 0.0f;
			
		}
		for (int l547 = 0; (l547 < 2); l547 = (l547 + 1)) {
			fRec777[l547] = 0.0f;
			
		}
		for (int l548 = 0; (l548 < 2); l548 = (l548 + 1)) {
			fRec775[l548] = 0.0f;
			
		}
		for (int l549 = 0; (l549 < 2); l549 = (l549 + 1)) {
			fRec789[l549] = 0.0f;
			
		}
		for (int l550 = 0; (l550 < 2); l550 = (l550 + 1)) {
			fRec787[l550] = 0.0f;
			
		}
		for (int l551 = 0; (l551 < 2); l551 = (l551 + 1)) {
			fRec786[l551] = 0.0f;
			
		}
		for (int l552 = 0; (l552 < 2); l552 = (l552 + 1)) {
			fRec784[l552] = 0.0f;
			
		}
		for (int l553 = 0; (l553 < 2); l553 = (l553 + 1)) {
			fRec783[l553] = 0.0f;
			
		}
		for (int l554 = 0; (l554 < 2); l554 = (l554 + 1)) {
			fRec781[l554] = 0.0f;
			
		}
		for (int l555 = 0; (l555 < 2); l555 = (l555 + 1)) {
			fRec780[l555] = 0.0f;
			
		}
		for (int l556 = 0; (l556 < 2); l556 = (l556 + 1)) {
			fRec778[l556] = 0.0f;
			
		}
		for (int l557 = 0; (l557 < 2); l557 = (l557 + 1)) {
			fRec798[l557] = 0.0f;
			
		}
		for (int l558 = 0; (l558 < 2); l558 = (l558 + 1)) {
			fRec796[l558] = 0.0f;
			
		}
		for (int l559 = 0; (l559 < 2); l559 = (l559 + 1)) {
			fRec795[l559] = 0.0f;
			
		}
		for (int l560 = 0; (l560 < 2); l560 = (l560 + 1)) {
			fRec793[l560] = 0.0f;
			
		}
		for (int l561 = 0; (l561 < 2); l561 = (l561 + 1)) {
			fRec792[l561] = 0.0f;
			
		}
		for (int l562 = 0; (l562 < 2); l562 = (l562 + 1)) {
			fRec790[l562] = 0.0f;
			
		}
		for (int l563 = 0; (l563 < 2); l563 = (l563 + 1)) {
			fRec801[l563] = 0.0f;
			
		}
		for (int l564 = 0; (l564 < 2); l564 = (l564 + 1)) {
			fRec799[l564] = 0.0f;
			
		}
		for (int l565 = 0; (l565 < 2); l565 = (l565 + 1)) {
			fRec807[l565] = 0.0f;
			
		}
		for (int l566 = 0; (l566 < 2); l566 = (l566 + 1)) {
			fRec805[l566] = 0.0f;
			
		}
		for (int l567 = 0; (l567 < 2); l567 = (l567 + 1)) {
			fRec804[l567] = 0.0f;
			
		}
		for (int l568 = 0; (l568 < 2); l568 = (l568 + 1)) {
			fRec802[l568] = 0.0f;
			
		}
		for (int l569 = 0; (l569 < 3); l569 = (l569 + 1)) {
			fVec21[l569] = 0.0f;
			
		}
		for (int l570 = 0; (l570 < 2); l570 = (l570 + 1)) {
			fRec819[l570] = 0.0f;
			
		}
		for (int l571 = 0; (l571 < 2); l571 = (l571 + 1)) {
			fRec817[l571] = 0.0f;
			
		}
		for (int l572 = 0; (l572 < 2); l572 = (l572 + 1)) {
			fRec816[l572] = 0.0f;
			
		}
		for (int l573 = 0; (l573 < 2); l573 = (l573 + 1)) {
			fRec814[l573] = 0.0f;
			
		}
		for (int l574 = 0; (l574 < 2); l574 = (l574 + 1)) {
			fRec813[l574] = 0.0f;
			
		}
		for (int l575 = 0; (l575 < 2); l575 = (l575 + 1)) {
			fRec811[l575] = 0.0f;
			
		}
		for (int l576 = 0; (l576 < 2); l576 = (l576 + 1)) {
			fRec810[l576] = 0.0f;
			
		}
		for (int l577 = 0; (l577 < 2); l577 = (l577 + 1)) {
			fRec808[l577] = 0.0f;
			
		}
		for (int l578 = 0; (l578 < 2); l578 = (l578 + 1)) {
			fRec828[l578] = 0.0f;
			
		}
		for (int l579 = 0; (l579 < 2); l579 = (l579 + 1)) {
			fRec826[l579] = 0.0f;
			
		}
		for (int l580 = 0; (l580 < 2); l580 = (l580 + 1)) {
			fRec825[l580] = 0.0f;
			
		}
		for (int l581 = 0; (l581 < 2); l581 = (l581 + 1)) {
			fRec823[l581] = 0.0f;
			
		}
		for (int l582 = 0; (l582 < 2); l582 = (l582 + 1)) {
			fRec822[l582] = 0.0f;
			
		}
		for (int l583 = 0; (l583 < 2); l583 = (l583 + 1)) {
			fRec820[l583] = 0.0f;
			
		}
		for (int l584 = 0; (l584 < 2); l584 = (l584 + 1)) {
			fRec831[l584] = 0.0f;
			
		}
		for (int l585 = 0; (l585 < 2); l585 = (l585 + 1)) {
			fRec829[l585] = 0.0f;
			
		}
		for (int l586 = 0; (l586 < 2); l586 = (l586 + 1)) {
			fRec837[l586] = 0.0f;
			
		}
		for (int l587 = 0; (l587 < 2); l587 = (l587 + 1)) {
			fRec835[l587] = 0.0f;
			
		}
		for (int l588 = 0; (l588 < 2); l588 = (l588 + 1)) {
			fRec834[l588] = 0.0f;
			
		}
		for (int l589 = 0; (l589 < 2); l589 = (l589 + 1)) {
			fRec832[l589] = 0.0f;
			
		}
		for (int l590 = 0; (l590 < 2); l590 = (l590 + 1)) {
			fRec849[l590] = 0.0f;
			
		}
		for (int l591 = 0; (l591 < 2); l591 = (l591 + 1)) {
			fRec847[l591] = 0.0f;
			
		}
		for (int l592 = 0; (l592 < 2); l592 = (l592 + 1)) {
			fRec846[l592] = 0.0f;
			
		}
		for (int l593 = 0; (l593 < 2); l593 = (l593 + 1)) {
			fRec844[l593] = 0.0f;
			
		}
		for (int l594 = 0; (l594 < 2); l594 = (l594 + 1)) {
			fRec843[l594] = 0.0f;
			
		}
		for (int l595 = 0; (l595 < 2); l595 = (l595 + 1)) {
			fRec841[l595] = 0.0f;
			
		}
		for (int l596 = 0; (l596 < 2); l596 = (l596 + 1)) {
			fRec840[l596] = 0.0f;
			
		}
		for (int l597 = 0; (l597 < 2); l597 = (l597 + 1)) {
			fRec838[l597] = 0.0f;
			
		}
		for (int l598 = 0; (l598 < 2); l598 = (l598 + 1)) {
			fRec858[l598] = 0.0f;
			
		}
		for (int l599 = 0; (l599 < 2); l599 = (l599 + 1)) {
			fRec856[l599] = 0.0f;
			
		}
		for (int l600 = 0; (l600 < 2); l600 = (l600 + 1)) {
			fRec855[l600] = 0.0f;
			
		}
		for (int l601 = 0; (l601 < 2); l601 = (l601 + 1)) {
			fRec853[l601] = 0.0f;
			
		}
		for (int l602 = 0; (l602 < 2); l602 = (l602 + 1)) {
			fRec852[l602] = 0.0f;
			
		}
		for (int l603 = 0; (l603 < 2); l603 = (l603 + 1)) {
			fRec850[l603] = 0.0f;
			
		}
		for (int l604 = 0; (l604 < 2); l604 = (l604 + 1)) {
			fRec861[l604] = 0.0f;
			
		}
		for (int l605 = 0; (l605 < 2); l605 = (l605 + 1)) {
			fRec859[l605] = 0.0f;
			
		}
		for (int l606 = 0; (l606 < 2); l606 = (l606 + 1)) {
			fRec867[l606] = 0.0f;
			
		}
		for (int l607 = 0; (l607 < 2); l607 = (l607 + 1)) {
			fRec865[l607] = 0.0f;
			
		}
		for (int l608 = 0; (l608 < 2); l608 = (l608 + 1)) {
			fRec864[l608] = 0.0f;
			
		}
		for (int l609 = 0; (l609 < 2); l609 = (l609 + 1)) {
			fRec862[l609] = 0.0f;
			
		}
		for (int l610 = 0; (l610 < 2); l610 = (l610 + 1)) {
			fRec879[l610] = 0.0f;
			
		}
		for (int l611 = 0; (l611 < 2); l611 = (l611 + 1)) {
			fRec877[l611] = 0.0f;
			
		}
		for (int l612 = 0; (l612 < 2); l612 = (l612 + 1)) {
			fRec876[l612] = 0.0f;
			
		}
		for (int l613 = 0; (l613 < 2); l613 = (l613 + 1)) {
			fRec874[l613] = 0.0f;
			
		}
		for (int l614 = 0; (l614 < 2); l614 = (l614 + 1)) {
			fRec873[l614] = 0.0f;
			
		}
		for (int l615 = 0; (l615 < 2); l615 = (l615 + 1)) {
			fRec871[l615] = 0.0f;
			
		}
		for (int l616 = 0; (l616 < 2); l616 = (l616 + 1)) {
			fRec870[l616] = 0.0f;
			
		}
		for (int l617 = 0; (l617 < 2); l617 = (l617 + 1)) {
			fRec868[l617] = 0.0f;
			
		}
		for (int l618 = 0; (l618 < 2); l618 = (l618 + 1)) {
			fRec888[l618] = 0.0f;
			
		}
		for (int l619 = 0; (l619 < 2); l619 = (l619 + 1)) {
			fRec886[l619] = 0.0f;
			
		}
		for (int l620 = 0; (l620 < 2); l620 = (l620 + 1)) {
			fRec885[l620] = 0.0f;
			
		}
		for (int l621 = 0; (l621 < 2); l621 = (l621 + 1)) {
			fRec883[l621] = 0.0f;
			
		}
		for (int l622 = 0; (l622 < 2); l622 = (l622 + 1)) {
			fRec882[l622] = 0.0f;
			
		}
		for (int l623 = 0; (l623 < 2); l623 = (l623 + 1)) {
			fRec880[l623] = 0.0f;
			
		}
		for (int l624 = 0; (l624 < 2); l624 = (l624 + 1)) {
			fRec891[l624] = 0.0f;
			
		}
		for (int l625 = 0; (l625 < 2); l625 = (l625 + 1)) {
			fRec889[l625] = 0.0f;
			
		}
		for (int l626 = 0; (l626 < 2); l626 = (l626 + 1)) {
			fRec897[l626] = 0.0f;
			
		}
		for (int l627 = 0; (l627 < 2); l627 = (l627 + 1)) {
			fRec895[l627] = 0.0f;
			
		}
		for (int l628 = 0; (l628 < 2); l628 = (l628 + 1)) {
			fRec894[l628] = 0.0f;
			
		}
		for (int l629 = 0; (l629 < 2); l629 = (l629 + 1)) {
			fRec892[l629] = 0.0f;
			
		}
		for (int l630 = 0; (l630 < 3); l630 = (l630 + 1)) {
			fVec22[l630] = 0.0f;
			
		}
		for (int l631 = 0; (l631 < 2); l631 = (l631 + 1)) {
			fRec909[l631] = 0.0f;
			
		}
		for (int l632 = 0; (l632 < 2); l632 = (l632 + 1)) {
			fRec907[l632] = 0.0f;
			
		}
		for (int l633 = 0; (l633 < 2); l633 = (l633 + 1)) {
			fRec906[l633] = 0.0f;
			
		}
		for (int l634 = 0; (l634 < 2); l634 = (l634 + 1)) {
			fRec904[l634] = 0.0f;
			
		}
		for (int l635 = 0; (l635 < 2); l635 = (l635 + 1)) {
			fRec903[l635] = 0.0f;
			
		}
		for (int l636 = 0; (l636 < 2); l636 = (l636 + 1)) {
			fRec901[l636] = 0.0f;
			
		}
		for (int l637 = 0; (l637 < 2); l637 = (l637 + 1)) {
			fRec900[l637] = 0.0f;
			
		}
		for (int l638 = 0; (l638 < 2); l638 = (l638 + 1)) {
			fRec898[l638] = 0.0f;
			
		}
		for (int l639 = 0; (l639 < 2); l639 = (l639 + 1)) {
			fRec918[l639] = 0.0f;
			
		}
		for (int l640 = 0; (l640 < 2); l640 = (l640 + 1)) {
			fRec916[l640] = 0.0f;
			
		}
		for (int l641 = 0; (l641 < 2); l641 = (l641 + 1)) {
			fRec915[l641] = 0.0f;
			
		}
		for (int l642 = 0; (l642 < 2); l642 = (l642 + 1)) {
			fRec913[l642] = 0.0f;
			
		}
		for (int l643 = 0; (l643 < 2); l643 = (l643 + 1)) {
			fRec912[l643] = 0.0f;
			
		}
		for (int l644 = 0; (l644 < 2); l644 = (l644 + 1)) {
			fRec910[l644] = 0.0f;
			
		}
		for (int l645 = 0; (l645 < 2); l645 = (l645 + 1)) {
			fRec921[l645] = 0.0f;
			
		}
		for (int l646 = 0; (l646 < 2); l646 = (l646 + 1)) {
			fRec919[l646] = 0.0f;
			
		}
		for (int l647 = 0; (l647 < 2); l647 = (l647 + 1)) {
			fRec927[l647] = 0.0f;
			
		}
		for (int l648 = 0; (l648 < 2); l648 = (l648 + 1)) {
			fRec925[l648] = 0.0f;
			
		}
		for (int l649 = 0; (l649 < 2); l649 = (l649 + 1)) {
			fRec924[l649] = 0.0f;
			
		}
		for (int l650 = 0; (l650 < 2); l650 = (l650 + 1)) {
			fRec922[l650] = 0.0f;
			
		}
		for (int l651 = 0; (l651 < 2); l651 = (l651 + 1)) {
			fRec939[l651] = 0.0f;
			
		}
		for (int l652 = 0; (l652 < 2); l652 = (l652 + 1)) {
			fRec937[l652] = 0.0f;
			
		}
		for (int l653 = 0; (l653 < 2); l653 = (l653 + 1)) {
			fRec936[l653] = 0.0f;
			
		}
		for (int l654 = 0; (l654 < 2); l654 = (l654 + 1)) {
			fRec934[l654] = 0.0f;
			
		}
		for (int l655 = 0; (l655 < 2); l655 = (l655 + 1)) {
			fRec933[l655] = 0.0f;
			
		}
		for (int l656 = 0; (l656 < 2); l656 = (l656 + 1)) {
			fRec931[l656] = 0.0f;
			
		}
		for (int l657 = 0; (l657 < 2); l657 = (l657 + 1)) {
			fRec930[l657] = 0.0f;
			
		}
		for (int l658 = 0; (l658 < 2); l658 = (l658 + 1)) {
			fRec928[l658] = 0.0f;
			
		}
		for (int l659 = 0; (l659 < 2); l659 = (l659 + 1)) {
			fRec948[l659] = 0.0f;
			
		}
		for (int l660 = 0; (l660 < 2); l660 = (l660 + 1)) {
			fRec946[l660] = 0.0f;
			
		}
		for (int l661 = 0; (l661 < 2); l661 = (l661 + 1)) {
			fRec945[l661] = 0.0f;
			
		}
		for (int l662 = 0; (l662 < 2); l662 = (l662 + 1)) {
			fRec943[l662] = 0.0f;
			
		}
		for (int l663 = 0; (l663 < 2); l663 = (l663 + 1)) {
			fRec942[l663] = 0.0f;
			
		}
		for (int l664 = 0; (l664 < 2); l664 = (l664 + 1)) {
			fRec940[l664] = 0.0f;
			
		}
		for (int l665 = 0; (l665 < 2); l665 = (l665 + 1)) {
			fRec951[l665] = 0.0f;
			
		}
		for (int l666 = 0; (l666 < 2); l666 = (l666 + 1)) {
			fRec949[l666] = 0.0f;
			
		}
		for (int l667 = 0; (l667 < 2); l667 = (l667 + 1)) {
			fRec957[l667] = 0.0f;
			
		}
		for (int l668 = 0; (l668 < 2); l668 = (l668 + 1)) {
			fRec955[l668] = 0.0f;
			
		}
		for (int l669 = 0; (l669 < 2); l669 = (l669 + 1)) {
			fRec954[l669] = 0.0f;
			
		}
		for (int l670 = 0; (l670 < 2); l670 = (l670 + 1)) {
			fRec952[l670] = 0.0f;
			
		}
		
	}
	
	virtual void init(int samplingFreq) {
		classInit(samplingFreq);
		instanceInit(samplingFreq);
	}
	virtual void instanceInit(int samplingFreq) {
		instanceConstants(samplingFreq);
		instanceResetUserInterface();
		instanceClear();
	}
	
	virtual mydsp* clone() {
		return new mydsp();
	}
	virtual int getSampleRate() {
		return fSamplingFreq;
		
	}
	
	virtual void buildUserInterface(UI* ui_interface) {
		ui_interface->openVerticalBox("SATOsw0o4");
		ui_interface->declare(&fHslider0, "unit", "dB");
		ui_interface->addHorizontalSlider("gain", &fHslider0, -10.0f, -30.0f, 10.0f, 1.0f);
		ui_interface->declare(&fHslider2, "unit", "dB");
		ui_interface->addHorizontalSlider("lf/hf", &fHslider2, 0.0f, -3.0f, 3.0f, 0.100000001f);
		ui_interface->addCheckButton("mute", &fCheckbox0);
		ui_interface->declare(&fHslider1, "unit", "Hz");
		ui_interface->addHorizontalSlider("xover", &fHslider1, 400.0f, 200.0f, 800.0f, 20.0f);
		ui_interface->closeBox();
		
	}
	
	virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) {
		FAUSTFLOAT* input0 = inputs[0];
		FAUSTFLOAT* input1 = inputs[1];
		FAUSTFLOAT* input2 = inputs[2];
		FAUSTFLOAT* input3 = inputs[3];
		FAUSTFLOAT* input4 = inputs[4];
		FAUSTFLOAT* input5 = inputs[5];
		FAUSTFLOAT* input6 = inputs[6];
		FAUSTFLOAT* input7 = inputs[7];
		FAUSTFLOAT* input8 = inputs[8];
		FAUSTFLOAT* input9 = inputs[9];
		FAUSTFLOAT* input10 = inputs[10];
		FAUSTFLOAT* input11 = inputs[11];
		FAUSTFLOAT* input12 = inputs[12];
		FAUSTFLOAT* input13 = inputs[13];
		FAUSTFLOAT* input14 = inputs[14];
		FAUSTFLOAT* input15 = inputs[15];
		FAUSTFLOAT* input16 = inputs[16];
		FAUSTFLOAT* input17 = inputs[17];
		FAUSTFLOAT* input18 = inputs[18];
		FAUSTFLOAT* input19 = inputs[19];
		FAUSTFLOAT* input20 = inputs[20];
		FAUSTFLOAT* input21 = inputs[21];
		FAUSTFLOAT* input22 = inputs[22];
		FAUSTFLOAT* input23 = inputs[23];
		FAUSTFLOAT* input24 = inputs[24];
		FAUSTFLOAT* output0 = outputs[0];
		FAUSTFLOAT* output1 = outputs[1];
		FAUSTFLOAT* output2 = outputs[2];
		FAUSTFLOAT* output3 = outputs[3];
		FAUSTFLOAT* output4 = outputs[4];
		FAUSTFLOAT* output5 = outputs[5];
		FAUSTFLOAT* output6 = outputs[6];
		FAUSTFLOAT* output7 = outputs[7];
		FAUSTFLOAT* output8 = outputs[8];
		FAUSTFLOAT* output9 = outputs[9];
		FAUSTFLOAT* output10 = outputs[10];
		FAUSTFLOAT* output11 = outputs[11];
		FAUSTFLOAT* output12 = outputs[12];
		FAUSTFLOAT* output13 = outputs[13];
		FAUSTFLOAT* output14 = outputs[14];
		FAUSTFLOAT* output15 = outputs[15];
		FAUSTFLOAT* output16 = outputs[16];
		FAUSTFLOAT* output17 = outputs[17];
		FAUSTFLOAT* output18 = outputs[18];
		FAUSTFLOAT* output19 = outputs[19];
		FAUSTFLOAT* output20 = outputs[20];
		FAUSTFLOAT* output21 = outputs[21];
		FAUSTFLOAT* output22 = outputs[22];
		FAUSTFLOAT* output23 = outputs[23];
		FAUSTFLOAT* output24 = outputs[24];
		FAUSTFLOAT* output25 = outputs[25];
		FAUSTFLOAT* output26 = outputs[26];
		FAUSTFLOAT* output27 = outputs[27];
		FAUSTFLOAT* output28 = outputs[28];
		FAUSTFLOAT* output29 = outputs[29];
		FAUSTFLOAT* output30 = outputs[30];
		float fSlow0 = (0.00100000005f * (float((float(fCheckbox0) < 0.5f)) * powf(10.0f, (0.0500000007f * float(fHslider0)))));
		float fSlow1 = (0.00100000005f * float(fHslider1));
		float fSlow2 = (0.00100000005f * powf(10.0f, (0.0500000007f * float(fHslider2))));
		for (int i = 0; (i < count); i = (i + 1)) {
			fRec0[0] = (fSlow0 + (0.999000013f * fRec0[1]));
			fRec4[0] = (fSlow1 + (0.999000013f * fRec4[1]));
			float fTemp0 = tanf((fConst4 * fRec4[0]));
			float fTemp1 = mydsp_faustpower2_f(fTemp0);
			float fTemp2 = ((fTemp0 * (fTemp0 + -2.0f)) + 1.0f);
			float fTemp3 = (fTemp1 + -1.0f);
			float fTemp4 = ((fTemp0 * (fTemp0 + 2.0f)) + 1.0f);
			fRec5[0] = (float(input2[i]) - (((fTemp2 * fRec5[2]) + (2.0f * (fTemp3 * fRec5[1]))) / fTemp4));
			fRec6[0] = (fSlow2 + (0.999000013f * fRec6[1]));
			float fTemp5 = (fRec6[0] * fTemp4);
			float fTemp6 = (0.0f - (2.0f / fTemp4));
			float fTemp7 = (((fTemp1 * (fRec5[2] + (fRec5[0] + (2.0f * fRec5[1])))) / fTemp5) + (0.906179845f * (fRec6[0] * (0.0f - ((fTemp6 * fRec5[1]) + ((fRec5[0] + fRec5[2]) / fTemp4))))));
			fRec7[0] = (float(input3[i]) - (((fTemp2 * fRec7[2]) + (2.0f * (fTemp3 * fRec7[1]))) / fTemp4));
			float fTemp8 = (((fTemp1 * (fRec7[2] + (fRec7[0] + (2.0f * fRec7[1])))) / fTemp5) + (0.906179845f * (fRec6[0] * (0.0f - ((fTemp6 * fRec7[1]) + ((fRec7[0] + fRec7[2]) / fTemp4))))));
			fRec8[0] = (float(input1[i]) - (((fTemp2 * fRec8[2]) + (2.0f * (fTemp3 * fRec8[1]))) / fTemp4));
			float fTemp9 = (((fTemp1 * (fRec8[2] + (fRec8[0] + (2.0f * fRec8[1])))) / fTemp5) + (0.906179845f * (fRec6[0] * (0.0f - ((fTemp6 * fRec8[1]) + ((fRec8[0] + fRec8[2]) / fTemp4))))));
			float fTemp10 = (fConst3 * (((0.144552425f * fTemp7) + (4.70970008e-06f * fTemp8)) - (4.80300014e-06f * fTemp9)));
			float fTemp11 = (fConst5 * fRec1[1]);
			fRec3[0] = (fTemp10 + (fRec3[1] + fTemp11));
			fRec1[0] = fRec3[0];
			float fRec2 = (fTemp11 + fTemp10);
			float fTemp12 = (fConst8 * fRec9[1]);
			float fTemp13 = (fConst9 * fRec12[1]);
			fRec21[0] = (float(input16[i]) - (((fRec21[2] * fTemp2) + (2.0f * (fRec21[1] * fTemp3))) / fTemp4));
			float fTemp14 = ((((fRec21[2] + (fRec21[0] + (2.0f * fRec21[1]))) * fTemp1) / fTemp5) + (0.245735466f * (fRec6[0] * (0.0f - ((fRec21[1] * fTemp6) + ((fRec21[0] + fRec21[2]) / fTemp4))))));
			fRec22[0] = (float(input18[i]) - (((fRec22[2] * fTemp2) + (2.0f * (fRec22[1] * fTemp3))) / fTemp4));
			float fTemp15 = ((((fRec22[2] + (fRec22[0] + (2.0f * fRec22[1]))) * fTemp1) / fTemp5) + (0.245735466f * (fRec6[0] * (0.0f - ((fRec22[1] * fTemp6) + ((fRec22[0] + fRec22[2]) / fTemp4))))));
			fRec23[0] = (float(input20[i]) - (((fRec23[2] * fTemp2) + (2.0f * (fRec23[1] * fTemp3))) / fTemp4));
			float fTemp16 = ((((fRec23[2] + (fRec23[0] + (2.0f * fRec23[1]))) * fTemp1) / fTemp5) + (0.245735466f * (fRec6[0] * (0.0f - ((fRec23[1] * fTemp6) + ((fRec23[0] + fRec23[2]) / fTemp4))))));
			fRec24[0] = (float(input22[i]) - (((fRec24[2] * fTemp2) + (2.0f * (fRec24[1] * fTemp3))) / fTemp4));
			float fTemp17 = ((((fRec24[2] + (fRec24[0] + (2.0f * fRec24[1]))) * fTemp1) / fTemp5) + (0.245735466f * (fRec6[0] * (0.0f - ((fRec24[1] * fTemp6) + ((fRec24[0] + fRec24[2]) / fTemp4))))));
			fRec25[0] = (float(input17[i]) - (((fRec25[2] * fTemp2) + (2.0f * (fRec25[1] * fTemp3))) / fTemp4));
			float fTemp18 = ((((fRec25[2] + (fRec25[0] + (2.0f * fRec25[1]))) * fTemp1) / fTemp5) + (0.245735466f * (fRec6[0] * (0.0f - ((fRec25[1] * fTemp6) + ((fRec25[0] + fRec25[2]) / fTemp4))))));
			fRec26[0] = (float(input19[i]) - (((fRec26[2] * fTemp2) + (2.0f * (fRec26[1] * fTemp3))) / fTemp4));
			float fTemp19 = ((((fRec26[2] + (fRec26[0] + (2.0f * fRec26[1]))) * fTemp1) / fTemp5) + (0.245735466f * (fRec6[0] * (0.0f - ((fRec26[1] * fTemp6) + ((fRec26[0] + fRec26[2]) / fTemp4))))));
			fRec27[0] = (float(input21[i]) - (((fRec27[2] * fTemp2) + (2.0f * (fRec27[1] * fTemp3))) / fTemp4));
			float fTemp20 = ((((fRec27[2] + (fRec27[0] + (2.0f * fRec27[1]))) * fTemp1) / fTemp5) + (0.245735466f * (fRec6[0] * (0.0f - ((fRec27[1] * fTemp6) + ((fRec27[0] + fRec27[2]) / fTemp4))))));
			fRec28[0] = (float(input23[i]) - (((fRec28[2] * fTemp2) + (2.0f * (fRec28[1] * fTemp3))) / fTemp4));
			float fTemp21 = ((((fRec28[2] + (fRec28[0] + (2.0f * fRec28[1]))) * fTemp1) / fTemp5) + (0.245735466f * (fRec6[0] * (0.0f - ((fRec28[1] * fTemp6) + ((fRec28[0] + fRec28[2]) / fTemp4))))));
			fRec29[0] = (float(input24[i]) - (((fRec29[2] * fTemp2) + (2.0f * (fRec29[1] * fTemp3))) / fTemp4));
			float fTemp22 = ((((fRec29[2] + (fRec29[0] + (2.0f * fRec29[1]))) * fTemp1) / fTemp5) + (0.245735466f * (fRec6[0] * (0.0f - ((fRec29[1] * fTemp6) + ((fRec29[0] + fRec29[2]) / fTemp4))))));
			float fTemp23 = (fConst11 * (((((1.73160004e-06f * fTemp14) + (4.22958001e-05f * fTemp15)) + (0.0567824654f * fTemp16)) + (1.84860005e-06f * fTemp17)) - (((((8.83719986e-06f * fTemp18) + (1.39949998e-06f * fTemp19)) + (4.73799986e-07f * fTemp20)) + (1.07545002e-05f * fTemp21)) + (4.48160017e-06f * fTemp22))));
			float fTemp24 = (fConst12 * fRec15[1]);
			float fTemp25 = (fConst13 * fRec18[1]);
			fRec20[0] = (fTemp23 + (fTemp24 + (fRec20[1] + fTemp25)));
			fRec18[0] = fRec20[0];
			float fRec19 = ((fTemp25 + fTemp24) + fTemp23);
			fRec17[0] = (fRec18[0] + fRec17[1]);
			fRec15[0] = fRec17[0];
			float fRec16 = fRec19;
			fRec14[0] = (fTemp12 + (fTemp13 + (fRec16 + fRec14[1])));
			fRec12[0] = fRec14[0];
			float fRec13 = (fTemp12 + (fRec16 + fTemp13));
			fRec11[0] = (fRec12[0] + fRec11[1]);
			fRec9[0] = fRec11[0];
			float fRec10 = fRec13;
			fRec36[0] = (float(input4[i]) - (((fTemp2 * fRec36[2]) + (2.0f * (fTemp3 * fRec36[1]))) / fTemp4));
			float fTemp26 = (((fTemp1 * (fRec36[2] + (fRec36[0] + (2.0f * fRec36[1])))) / fTemp5) + (0.731742859f * (fRec6[0] * (0.0f - ((fTemp6 * fRec36[1]) + ((fRec36[0] + fRec36[2]) / fTemp4))))));
			fRec37[0] = (float(input6[i]) - (((fTemp2 * fRec37[2]) + (2.0f * (fTemp3 * fRec37[1]))) / fTemp4));
			float fTemp27 = (((fTemp1 * (fRec37[2] + (fRec37[0] + (2.0f * fRec37[1])))) / fTemp5) + (0.731742859f * (fRec6[0] * (0.0f - ((fTemp6 * fRec37[1]) + ((fRec37[0] + fRec37[2]) / fTemp4))))));
			fRec38[0] = (float(input7[i]) - (((fTemp2 * fRec38[2]) + (2.0f * (fTemp3 * fRec38[1]))) / fTemp4));
			float fTemp28 = (((fTemp1 * (fRec38[2] + (fRec38[0] + (2.0f * fRec38[1])))) / fTemp5) + (0.731742859f * (fRec6[0] * (0.0f - ((fTemp6 * fRec38[1]) + ((fRec38[0] + fRec38[2]) / fTemp4))))));
			fRec39[0] = (float(input8[i]) - (((fTemp2 * fRec39[2]) + (2.0f * (fTemp3 * fRec39[1]))) / fTemp4));
			float fTemp29 = (((fTemp1 * (fRec39[2] + (fRec39[0] + (2.0f * fRec39[1])))) / fTemp5) + (0.731742859f * (fRec6[0] * (0.0f - ((fTemp6 * fRec39[1]) + ((fRec39[0] + fRec39[2]) / fTemp4))))));
			fRec40[0] = (float(input5[i]) - (((fTemp2 * fRec40[2]) + (2.0f * (fTemp3 * fRec40[1]))) / fTemp4));
			float fTemp30 = (((fTemp1 * (fRec40[2] + (fRec40[0] + (2.0f * fRec40[1])))) / fTemp5) + (0.731742859f * (fRec6[0] * (0.0f - ((fTemp6 * fRec40[1]) + ((fRec40[0] + fRec40[2]) / fTemp4))))));
			float fTemp31 = (fConst15 * (((((1.98604994e-05f * fTemp26) + (0.141460821f * fTemp27)) + (6.64250001e-06f * fTemp28)) + (1.14950001e-06f * fTemp29)) - (7.13349982e-06f * fTemp30)));
			float fTemp32 = (fConst16 * fRec30[1]);
			float fTemp33 = (fConst17 * fRec33[1]);
			fRec35[0] = (fTemp31 + (fTemp32 + (fRec35[1] + fTemp33)));
			fRec33[0] = fRec35[0];
			float fRec34 = ((fTemp33 + fTemp32) + fTemp31);
			fRec32[0] = (fRec33[0] + fRec32[1]);
			fRec30[0] = fRec32[0];
			float fRec31 = fRec34;
			fRec50[0] = (float(input12[i]) - (((fTemp2 * fRec50[2]) + (2.0f * (fTemp3 * fRec50[1]))) / fTemp4));
			float fTemp34 = ((0.50103116f * (fRec6[0] * (0.0f - (((fRec50[0] + fRec50[2]) / fTemp4) + (fTemp6 * fRec50[1]))))) + ((fTemp1 * (fRec50[2] + (fRec50[0] + (2.0f * fRec50[1])))) / fTemp5));
			fRec51[0] = (float(input13[i]) - (((fRec51[2] * fTemp2) + (2.0f * (fRec51[1] * fTemp3))) / fTemp4));
			float fTemp35 = ((((fRec51[2] + (fRec51[0] + (2.0f * fRec51[1]))) * fTemp1) / fTemp5) + (0.50103116f * (fRec6[0] * (0.0f - ((fRec51[1] * fTemp6) + ((fRec51[0] + fRec51[2]) / fTemp4))))));
			fRec52[0] = (float(input14[i]) - (((fRec52[2] * fTemp2) + (2.0f * (fRec52[1] * fTemp3))) / fTemp4));
			float fTemp36 = ((((fRec52[2] + (fRec52[0] + (2.0f * fRec52[1]))) * fTemp1) / fTemp5) + (0.50103116f * (fRec6[0] * (0.0f - ((fRec52[1] * fTemp6) + ((fRec52[0] + fRec52[2]) / fTemp4))))));
			fRec53[0] = (float(input10[i]) - (((fTemp2 * fRec53[2]) + (2.0f * (fTemp3 * fRec53[1]))) / fTemp4));
			float fTemp37 = (((fTemp1 * (fRec53[2] + (fRec53[0] + (2.0f * fRec53[1])))) / fTemp5) + (0.50103116f * (fRec6[0] * (0.0f - ((fTemp6 * fRec53[1]) + ((fRec53[0] + fRec53[2]) / fTemp4))))));
			fRec54[0] = (float(input15[i]) - (((fRec54[2] * fTemp2) + (2.0f * (fRec54[1] * fTemp3))) / fTemp4));
			float fTemp38 = ((((fRec54[2] + (fRec54[0] + (2.0f * fRec54[1]))) * fTemp1) / fTemp5) + (0.50103116f * (fRec6[0] * (0.0f - ((fRec54[1] * fTemp6) + ((fRec54[0] + fRec54[2]) / fTemp4))))));
			fRec55[0] = (float(input9[i]) - (((fTemp2 * fRec55[2]) + (2.0f * (fTemp3 * fRec55[1]))) / fTemp4));
			float fTemp39 = (((fTemp1 * (fRec55[2] + (fRec55[0] + (2.0f * fRec55[1])))) / fTemp5) + (0.50103116f * (fRec6[0] * (0.0f - ((fTemp6 * fRec55[1]) + ((fRec55[0] + fRec55[2]) / fTemp4))))));
			fRec56[0] = (float(input11[i]) - (((fTemp2 * fRec56[2]) + (2.0f * (fTemp3 * fRec56[1]))) / fTemp4));
			float fTemp40 = (((fTemp1 * (fRec56[2] + (fRec56[0] + (2.0f * fRec56[1])))) / fTemp5) + (0.50103116f * (fRec6[0] * (0.0f - ((fTemp6 * fRec56[1]) + ((fRec56[0] + fRec56[2]) / fTemp4))))));
			float fTemp41 = (fConst20 * (((((0.105663024f * fTemp34) + (4.44899979e-06f * fTemp35)) + (1.9603001e-06f * fTemp36)) + (3.67071007e-05f * fTemp37)) - (((5.70290013e-06f * fTemp38) + (4.7333001e-06f * fTemp39)) + (5.63999993e-06f * fTemp40))));
			float fTemp42 = (fConst21 * fRec47[1]);
			float fTemp43 = (fConst22 * fRec44[1]);
			fRec49[0] = (((fTemp41 + fRec49[1]) + fTemp42) + fTemp43);
			fRec47[0] = fRec49[0];
			float fRec48 = ((fTemp41 + fTemp42) + fTemp43);
			fRec46[0] = (fRec47[0] + fRec46[1]);
			fRec44[0] = fRec46[0];
			float fRec45 = fRec48;
			float fTemp44 = (fConst23 * fRec41[1]);
			fRec43[0] = ((fRec45 + fRec43[1]) + fTemp44);
			fRec41[0] = fRec43[0];
			float fRec42 = (fRec45 + fTemp44);
			fRec57[0] = (float(input0[i]) - (((fRec57[2] * fTemp2) + (2.0f * (fRec57[1] * fTemp3))) / fTemp4));
			float fTemp45 = (((fTemp1 * (fRec57[2] + (fRec57[0] + (2.0f * fRec57[1])))) / fTemp5) + (fRec6[0] * (0.0f - ((fRec57[1] * fTemp6) + ((fRec57[0] + fRec57[2]) / fTemp4)))));
			fVec0[(IOTA & 2047)] = ((fRec2 + (fRec10 + (fRec31 + fRec42))) + (0.0951942503f * fTemp45));
			output0[i] = FAUSTFLOAT((0.622610331f * (fRec0[0] * fVec0[((IOTA - iConst24) & 2047)])));
			float fTemp46 = (fConst26 * fRec58[1]);
			float fTemp47 = (fConst27 * fRec61[1]);
			float fTemp48 = (fConst29 * ((((0.025524132f * fTemp15) + (0.00431362074f * fTemp19)) + (0.0002112191f * fTemp22)) - ((0.0322232991f * fTemp21) + ((0.0426399745f * fTemp17) + ((((0.0116217826f * fTemp14) + (0.0200883429f * fTemp18)) + (0.0332100652f * fTemp16)) + (0.00292050908f * fTemp20))))));
			float fTemp49 = (fConst30 * fRec64[1]);
			float fTemp50 = (fConst31 * fRec67[1]);
			fRec69[0] = (fTemp48 + (fTemp49 + (fRec69[1] + fTemp50)));
			fRec67[0] = fRec69[0];
			float fRec68 = ((fTemp50 + fTemp49) + fTemp48);
			fRec66[0] = (fRec67[0] + fRec66[1]);
			fRec64[0] = fRec66[0];
			float fRec65 = fRec68;
			fRec63[0] = (fTemp46 + (fTemp47 + (fRec65 + fRec63[1])));
			fRec61[0] = fRec63[0];
			float fRec62 = (fTemp46 + (fRec65 + fTemp47));
			fRec60[0] = (fRec61[0] + fRec60[1]);
			fRec58[0] = fRec60[0];
			float fRec59 = fRec62;
			float fTemp51 = (fConst33 * fRec70[1]);
			float fTemp52 = (fConst35 * (((0.0532056242f * fTemp40) + ((0.0387602337f * fTemp37) + (0.0135681545f * fTemp35))) - ((0.012015922f * fTemp39) + ((0.0226701032f * fTemp38) + ((0.0242027957f * fTemp34) + (0.0523547642f * fTemp36))))));
			float fTemp53 = (fConst36 * fRec73[1]);
			float fTemp54 = (fConst37 * fRec76[1]);
			fRec78[0] = (fTemp52 + (fTemp53 + (fRec78[1] + fTemp54)));
			fRec76[0] = fRec78[0];
			float fRec77 = ((fTemp54 + fTemp53) + fTemp52);
			fRec75[0] = (fRec76[0] + fRec75[1]);
			fRec73[0] = fRec75[0];
			float fRec74 = fRec77;
			fRec72[0] = (fTemp51 + (fRec74 + fRec72[1]));
			fRec70[0] = fRec72[0];
			float fRec71 = (fRec74 + fTemp51);
			float fTemp55 = (fConst39 * (((0.0640498549f * fTemp9) + (0.0632267669f * fTemp7)) + (0.0215868391f * fTemp8)));
			float fTemp56 = (fConst40 * fRec79[1]);
			fRec81[0] = (fTemp55 + (fRec81[1] + fTemp56));
			fRec79[0] = fRec81[0];
			float fRec80 = (fTemp56 + fTemp55);
			float fTemp57 = (fConst42 * (((((0.0286792312f * fTemp26) + (0.0842565075f * fTemp30)) + (0.0191042144f * fTemp27)) + (0.0264974907f * fTemp28)) - (0.0353156179f * fTemp29)));
			float fTemp58 = (fConst43 * fRec82[1]);
			float fTemp59 = (fConst44 * fRec85[1]);
			fRec87[0] = (fTemp57 + (fTemp58 + (fRec87[1] + fTemp59)));
			fRec85[0] = fRec87[0];
			float fRec86 = ((fTemp59 + fTemp58) + fTemp57);
			fRec84[0] = (fRec85[0] + fRec84[1]);
			fRec82[0] = fRec84[0];
			float fRec83 = fRec86;
			fVec1[(IOTA & 2047)] = ((0.0579515621f * fTemp45) + (fRec59 + (fRec71 + (fRec80 + fRec83))));
			output1[i] = FAUSTFLOAT((0.717061102f * (fRec0[0] * fVec1[((IOTA - iConst45) & 2047)])));
			float fTemp60 = (fConst47 * fRec88[1]);
			float fTemp61 = (fConst48 * fRec91[1]);
			float fTemp62 = (fConst50 * (((0.0258102696f * fTemp21) + ((1.35194996e-05f * fTemp17) + (((1.16617002e-05f * fTemp14) + (0.0258032493f * fTemp18)) + (0.00226937444f * fTemp20)))) - ((((0.0498330481f * fTemp15) + (0.00227920571f * fTemp19)) + (0.0348369852f * fTemp16)) + (0.00663256412f * fTemp22))));
			float fTemp63 = (fConst51 * fRec94[1]);
			float fTemp64 = (fConst52 * fRec97[1]);
			fRec99[0] = (fTemp62 + (fTemp63 + (fRec99[1] + fTemp64)));
			fRec97[0] = fRec99[0];
			float fRec98 = ((fTemp64 + fTemp63) + fTemp62);
			fRec96[0] = (fRec97[0] + fRec96[1]);
			fRec94[0] = fRec96[0];
			float fRec95 = fRec98;
			fRec93[0] = (fTemp60 + (fTemp61 + (fRec95 + fRec93[1])));
			fRec91[0] = fRec93[0];
			float fRec92 = (fTemp60 + (fRec95 + fTemp61));
			fRec90[0] = (fRec91[0] + fRec90[1]);
			fRec88[0] = fRec90[0];
			float fRec89 = fRec92;
			float fTemp65 = (fConst54 * fRec100[1]);
			float fTemp66 = (fConst56 * (((0.0378993526f * fTemp40) + ((0.0171615649f * fTemp39) + ((5.23010021e-06f * fTemp36) + (0.017173158f * fTemp38)))) - ((0.0685905889f * fTemp37) + ((0.0300105829f * fTemp34) + (0.0379156061f * fTemp35)))));
			float fTemp67 = (fConst57 * fRec103[1]);
			float fTemp68 = (fConst58 * fRec106[1]);
			fRec108[0] = (fTemp66 + (fTemp67 + (fRec108[1] + fTemp68)));
			fRec106[0] = fRec108[0];
			float fRec107 = ((fTemp68 + fTemp67) + fTemp66);
			fRec105[0] = (fRec106[0] + fRec105[1]);
			fRec103[0] = fRec105[0];
			float fRec104 = fRec107;
			fRec102[0] = (fTemp65 + (fRec104 + fRec102[1]));
			fRec100[0] = fRec102[0];
			float fRec101 = (fRec104 + fTemp65);
			float fTemp69 = (fConst60 * (((0.0527678393f * fTemp9) + (0.0669136494f * fTemp7)) - (0.0527719259f * fTemp8)));
			float fTemp70 = (fConst61 * fRec109[1]);
			fRec111[0] = (fTemp69 + (fRec111[1] + fTemp70));
			fRec109[0] = fRec111[0];
			float fRec110 = (fTemp70 + fTemp69);
			float fTemp71 = (fConst63 * (((0.066809155f * fTemp30) + (0.0161149334f * fTemp27)) - (((0.0488329753f * fTemp26) + (0.0668209344f * fTemp28)) + (1.61180003e-06f * fTemp29))));
			float fTemp72 = (fConst64 * fRec112[1]);
			float fTemp73 = (fConst65 * fRec115[1]);
			fRec117[0] = (fTemp71 + (fTemp72 + (fRec117[1] + fTemp73)));
			fRec115[0] = fRec117[0];
			float fRec116 = ((fTemp73 + fTemp72) + fTemp71);
			fRec114[0] = (fRec115[0] + fRec114[1]);
			fRec112[0] = fRec114[0];
			float fRec113 = fRec116;
			fVec2[(IOTA & 2047)] = ((0.0633188263f * fTemp45) + (fRec89 + (fRec101 + (fRec110 + fRec113))));
			output2[i] = FAUSTFLOAT((0.716854393f * (fRec0[0] * fVec2[((IOTA - iConst66) & 2047)])));
			float fTemp74 = (fConst40 * fRec118[1]);
			float fTemp75 = (fConst39 * ((0.0595454238f * fTemp7) - ((0.0156096201f * fTemp9) + (0.0582364872f * fTemp8))));
			fRec120[0] = ((fRec120[1] + fTemp74) + fTemp75);
			fRec118[0] = fRec120[0];
			float fRec119 = (fTemp74 + fTemp75);
			float fTemp76 = (fConst42 * ((((0.02052035f * fTemp26) + (0.022095751f * fTemp27)) + (0.0355290733f * fTemp29)) - ((0.0212301388f * fTemp30) + (0.0792069137f * fTemp28))));
			float fTemp77 = (fConst43 * fRec121[1]);
			float fTemp78 = (fConst44 * fRec124[1]);
			fRec126[0] = (fTemp76 + (fTemp77 + (fRec126[1] + fTemp78)));
			fRec124[0] = fRec126[0];
			float fRec125 = ((fTemp78 + fTemp77) + fTemp76);
			fRec123[0] = (fRec124[0] + fRec123[1]);
			fRec121[0] = fRec123[0];
			float fRec122 = fRec125;
			float fTemp79 = (fConst33 * fRec127[1]);
			float fTemp80 = (fConst35 * (((0.0304364227f * fTemp37) + (0.0526964627f * fTemp36)) - ((0.0145489927f * fTemp40) + ((0.0175366681f * fTemp39) + (((0.018388411f * fTemp34) + (0.0542868599f * fTemp35)) + (0.0175308287f * fTemp38))))));
			float fTemp81 = (fConst36 * fRec130[1]);
			float fTemp82 = (fConst37 * fRec133[1]);
			fRec135[0] = (fTemp80 + (fTemp81 + (fRec135[1] + fTemp82)));
			fRec133[0] = fRec135[0];
			float fRec134 = ((fTemp82 + fTemp81) + fTemp80);
			fRec132[0] = (fRec132[1] + fRec133[0]);
			fRec130[0] = fRec132[0];
			float fRec131 = fRec134;
			fRec129[0] = ((fRec129[1] + fTemp79) + fRec131);
			fRec127[0] = fRec129[0];
			float fRec128 = (fTemp79 + fRec131);
			float fTemp83 = (fConst26 * fRec136[1]);
			float fTemp84 = (fConst27 * fRec139[1]);
			float fTemp85 = (fConst29 * (((((0.0118943388f * fTemp14) + (0.0247731693f * fTemp15)) + (0.0428953283f * fTemp17)) + (0.00686916383f * fTemp22)) - (((0.0097140763f * fTemp20) + (((0.0265180059f * fTemp18) + (0.00259988382f * fTemp19)) + (0.03157847f * fTemp16))) + (0.0265038051f * fTemp21))));
			float fTemp86 = (fConst30 * fRec142[1]);
			float fTemp87 = (fConst31 * fRec145[1]);
			fRec147[0] = (fTemp85 + (fTemp86 + (fRec147[1] + fTemp87)));
			fRec145[0] = fRec147[0];
			float fRec146 = ((fTemp87 + fTemp86) + fTemp85);
			fRec144[0] = (fRec144[1] + fRec145[0]);
			fRec142[0] = fRec144[0];
			float fRec143 = fRec146;
			fRec141[0] = ((fTemp83 + (fRec141[1] + fTemp84)) + fRec143);
			fRec139[0] = fRec141[0];
			float fRec140 = ((fTemp84 + fTemp83) + fRec143);
			fRec138[0] = (fRec138[1] + fRec139[0]);
			fRec136[0] = fRec138[0];
			float fRec137 = fRec140;
			fVec3[(IOTA & 2047)] = ((((fRec119 + fRec122) + fRec128) + fRec137) + (0.0525917113f * fTemp45));
			output3[i] = FAUSTFLOAT((0.717061102f * (fRec0[0] * fVec3[((IOTA - iConst45) & 2047)])));
			float fTemp88 = (fConst39 * ((0.063209936f * fTemp7) - ((0.0640460402f * fTemp9) + (0.0216016546f * fTemp8))));
			float fTemp89 = (fConst40 * fRec148[1]);
			fRec150[0] = (fTemp88 + (fRec150[1] + fTemp89));
			fRec148[0] = fRec150[0];
			float fRec149 = (fTemp89 + fTemp88);
			float fTemp90 = (fConst42 * (((0.0287043639f * fTemp26) + (0.0190891717f * fTemp27)) - (((0.0842392817f * fTemp30) + (0.0265128072f * fTemp28)) + (0.0353211984f * fTemp29))));
			float fTemp91 = (fConst43 * fRec151[1]);
			float fTemp92 = (fConst44 * fRec154[1]);
			fRec156[0] = (fTemp90 + (fTemp91 + (fRec156[1] + fTemp92)));
			fRec154[0] = fRec156[0];
			float fRec155 = ((fTemp92 + fTemp91) + fTemp90);
			fRec153[0] = (fRec153[1] + fRec154[0]);
			fRec151[0] = fRec153[0];
			float fRec152 = fRec155;
			float fTemp93 = (fConst33 * fRec157[1]);
			float fTemp94 = (fConst35 * (((0.0387873575f * fTemp37) + ((0.0120212426f * fTemp39) + (0.0226971731f * fTemp38))) - ((0.0531799942f * fTemp40) + (((0.0242017414f * fTemp34) + (0.0135731231f * fTemp35)) + (0.0523548201f * fTemp36)))));
			float fTemp95 = (fConst36 * fRec160[1]);
			float fTemp96 = (fConst37 * fRec163[1]);
			fRec165[0] = (fTemp94 + (fTemp95 + (fRec165[1] + fTemp96)));
			fRec163[0] = fRec165[0];
			float fRec164 = ((fTemp96 + fTemp95) + fTemp94);
			fRec162[0] = (fRec162[1] + fRec163[0]);
			fRec160[0] = fRec162[0];
			float fRec161 = fRec164;
			fRec159[0] = ((fRec159[1] + fTemp93) + fRec161);
			fRec157[0] = fRec159[0];
			float fRec158 = (fTemp93 + fRec161);
			float fTemp97 = (fConst26 * fRec166[1]);
			float fTemp98 = (fConst27 * fRec169[1]);
			float fTemp99 = (fConst29 * (((0.0002056183f * fTemp22) + ((((0.0201007035f * fTemp18) + (0.0255371984f * fTemp15)) + (0.00292492588f * fTemp20)) + (0.0322504453f * fTemp21))) - ((((0.0116404695f * fTemp14) + (0.00430144789f * fTemp19)) + (0.0331932008f * fTemp16)) + (0.0426293314f * fTemp17))));
			float fTemp100 = (fConst30 * fRec172[1]);
			float fTemp101 = (fConst31 * fRec175[1]);
			fRec177[0] = (fTemp99 + (fTemp100 + (fRec177[1] + fTemp101)));
			fRec175[0] = fRec177[0];
			float fRec176 = ((fTemp101 + fTemp100) + fTemp99);
			fRec174[0] = (fRec174[1] + fRec175[0]);
			fRec172[0] = fRec174[0];
			float fRec173 = fRec176;
			fRec171[0] = ((fTemp97 + (fRec171[1] + fTemp98)) + fRec173);
			fRec169[0] = fRec171[0];
			float fRec170 = ((fTemp98 + fTemp97) + fRec173);
			fRec168[0] = (fRec168[1] + fRec169[0]);
			fRec166[0] = fRec168[0];
			float fRec167 = fRec170;
			fVec4[(IOTA & 2047)] = ((0.0579424426f * fTemp45) + (((fRec149 + fRec152) + fRec158) + fRec167));
			output4[i] = FAUSTFLOAT((0.717061102f * (fRec0[0] * fVec4[((IOTA - iConst45) & 2047)])));
			float fTemp102 = (fConst54 * fRec178[1]);
			float fTemp103 = (fConst56 * (((0.0379130989f * fTemp35) + (5.81179984e-06f * fTemp36)) - ((0.0379246771f * fTemp40) + ((0.0685903355f * fTemp37) + ((0.0171904024f * fTemp39) + ((0.0299918056f * fTemp34) + (0.0171739701f * fTemp38)))))));
			float fTemp104 = (fConst57 * fRec181[1]);
			float fTemp105 = (fConst58 * fRec184[1]);
			fRec186[0] = (fTemp103 + (fTemp104 + (fRec186[1] + fTemp105)));
			fRec184[0] = fRec186[0];
			float fRec185 = ((fTemp105 + fTemp104) + fTemp103);
			fRec183[0] = (fRec184[0] + fRec183[1]);
			fRec181[0] = fRec183[0];
			float fRec182 = fRec185;
			fRec180[0] = (fTemp102 + (fRec182 + fRec180[1]));
			fRec178[0] = fRec180[0];
			float fRec179 = (fRec182 + fTemp102);
			float fTemp106 = (fConst63 * ((((0.0161357336f * fTemp27) + (0.0668131635f * fTemp28)) + (1.16084002e-05f * fTemp29)) - ((0.0488342457f * fTemp26) + (0.0668111816f * fTemp30))));
			float fTemp107 = (fConst64 * fRec187[1]);
			float fTemp108 = (fConst65 * fRec190[1]);
			fRec192[0] = (fTemp106 + (fTemp107 + (fRec192[1] + fTemp108)));
			fRec190[0] = fRec192[0];
			float fRec191 = ((fTemp108 + fTemp107) + fTemp106);
			fRec189[0] = (fRec190[0] + fRec189[1]);
			fRec187[0] = fRec189[0];
			float fRec188 = fRec191;
			float fTemp109 = (fConst47 * fRec193[1]);
			float fTemp110 = (fConst48 * fRec196[1]);
			float fTemp111 = (fConst50 * ((0.00225335499f * fTemp19) - ((0.00666434877f * fTemp22) + ((0.0258102771f * fTemp21) + ((8.92810021e-06f * fTemp17) + (((((6.97999985e-06f * fTemp14) + (0.0258209221f * fTemp18)) + (0.0498308949f * fTemp15)) + (0.0348385312f * fTemp16)) + (0.00226649758f * fTemp20)))))));
			float fTemp112 = (fConst51 * fRec199[1]);
			float fTemp113 = (fConst52 * fRec202[1]);
			fRec204[0] = (fTemp111 + (fTemp112 + (fRec204[1] + fTemp113)));
			fRec202[0] = fRec204[0];
			float fRec203 = ((fTemp113 + fTemp112) + fTemp111);
			fRec201[0] = (fRec202[0] + fRec201[1]);
			fRec199[0] = fRec201[0];
			float fRec200 = fRec203;
			fRec198[0] = (fTemp109 + (fTemp110 + (fRec200 + fRec198[1])));
			fRec196[0] = fRec198[0];
			float fRec197 = (fTemp109 + (fRec200 + fTemp110));
			fRec195[0] = (fRec196[0] + fRec195[1]);
			fRec193[0] = fRec195[0];
			float fRec194 = fRec197;
			float fTemp114 = (fConst60 * (((0.0669162646f * fTemp7) + (0.0527649f * fTemp8)) - (0.0527561419f * fTemp9)));
			float fTemp115 = (fConst61 * fRec205[1]);
			fRec207[0] = (fTemp114 + (fRec207[1] + fTemp115));
			fRec205[0] = fRec207[0];
			float fRec206 = (fTemp115 + fTemp114);
			fVec5[(IOTA & 2047)] = ((0.06331072f * fTemp45) + ((fRec179 + (fRec188 + fRec194)) + fRec206));
			output5[i] = FAUSTFLOAT((0.716854393f * (fRec0[0] * fVec5[((IOTA - iConst66) & 2047)])));
			float fTemp116 = (fConst39 * (((0.0156095503f * fTemp9) + (0.0595348626f * fTemp7)) + (0.0582405664f * fTemp8)));
			float fTemp117 = (fConst40 * fRec208[1]);
			fRec210[0] = (fTemp116 + (fRec210[1] + fTemp117));
			fRec208[0] = fRec210[0];
			float fRec209 = (fTemp117 + fTemp116);
			float fTemp118 = (fConst42 * (((((0.0205246415f * fTemp26) + (0.0212266501f * fTemp30)) + (0.0220794007f * fTemp27)) + (0.0792068467f * fTemp28)) + (0.035535343f * fTemp29)));
			float fTemp119 = (fConst43 * fRec211[1]);
			float fTemp120 = (fConst44 * fRec214[1]);
			fRec216[0] = (fTemp118 + (fTemp119 + (fRec216[1] + fTemp120)));
			fRec214[0] = fRec216[0];
			float fRec215 = ((fTemp120 + fTemp119) + fTemp118);
			fRec213[0] = (fRec213[1] + fRec214[0]);
			fRec211[0] = fRec213[0];
			float fRec212 = fRec215;
			float fTemp121 = (fConst33 * fRec217[1]);
			float fTemp122 = (fConst35 * (((0.0145412851f * fTemp40) + ((0.0304406285f * fTemp37) + ((0.0175386928f * fTemp39) + ((0.0175272785f * fTemp38) + ((0.0542752631f * fTemp35) + (0.0527098142f * fTemp36)))))) - (0.0184011627f * fTemp34)));
			float fTemp123 = (fConst36 * fRec220[1]);
			float fTemp124 = (fConst37 * fRec223[1]);
			fRec225[0] = (fTemp122 + (fTemp123 + (fRec225[1] + fTemp124)));
			fRec223[0] = fRec225[0];
			float fRec224 = ((fTemp124 + fTemp123) + fTemp122);
			fRec222[0] = (fRec222[1] + fRec223[0]);
			fRec220[0] = fRec222[0];
			float fRec221 = fRec224;
			fRec219[0] = ((fRec219[1] + fTemp121) + fRec221);
			fRec217[0] = fRec219[0];
			float fRec218 = (fTemp121 + fRec221);
			float fTemp125 = (fConst26 * fRec226[1]);
			float fTemp126 = (fConst27 * fRec229[1]);
			float fTemp127 = (fConst29 * (((0.00685405731f * fTemp22) + ((0.0265102237f * fTemp21) + ((0.0429091901f * fTemp17) + (((((0.0118854297f * fTemp14) + (0.0265230574f * fTemp18)) + (0.0247748103f * fTemp15)) + (0.00259165652f * fTemp19)) + (0.00969489478f * fTemp20))))) - (0.0315765627f * fTemp16)));
			float fTemp128 = (fConst30 * fRec232[1]);
			float fTemp129 = (fConst31 * fRec235[1]);
			fRec237[0] = (fTemp127 + (fTemp128 + (fRec237[1] + fTemp129)));
			fRec235[0] = fRec237[0];
			float fRec236 = ((fTemp129 + fTemp128) + fTemp127);
			fRec234[0] = (fRec234[1] + fRec235[0]);
			fRec232[0] = fRec234[0];
			float fRec233 = fRec236;
			fRec231[0] = ((fTemp125 + (fRec231[1] + fTemp126)) + fRec233);
			fRec229[0] = fRec231[0];
			float fRec230 = ((fTemp126 + fTemp125) + fRec233);
			fRec228[0] = (fRec228[1] + fRec229[0]);
			fRec226[0] = fRec228[0];
			float fRec227 = fRec230;
			fVec6[(IOTA & 2047)] = ((0.052587714f * fTemp45) + (((fRec209 + fRec212) + fRec218) + fRec227));
			output6[i] = FAUSTFLOAT((0.717061102f * (fRec0[0] * fVec6[((IOTA - iConst45) & 2047)])));
			float fTemp130 = (fConst68 * (((0.0479171462f * fTemp9) + (0.0110148918f * fTemp7)) - (0.00271321135f * fTemp8)));
			float fTemp131 = (fConst69 * fRec238[1]);
			fRec240[0] = (fTemp130 + (fRec240[1] + fTemp131));
			fRec238[0] = fRec240[0];
			float fRec239 = (fTemp131 + fTemp130);
			float fTemp132 = (fConst71 * ((0.0218717307f * fTemp30) - ((((0.0049932464f * fTemp26) + (0.0249028355f * fTemp27)) + (0.0025010386f * fTemp28)) + (0.047650516f * fTemp29))));
			float fTemp133 = (fConst72 * fRec241[1]);
			float fTemp134 = (fConst73 * fRec244[1]);
			fRec246[0] = (fTemp132 + (fTemp133 + (fRec246[1] + fTemp134)));
			fRec244[0] = fRec246[0];
			float fRec245 = ((fTemp134 + fTemp133) + fTemp132);
			fRec243[0] = (fRec243[1] + fRec244[0]);
			fRec241[0] = fRec243[0];
			float fRec242 = fRec245;
			float fTemp135 = (fConst75 * fRec247[1]);
			float fTemp136 = (fConst77 * (((0.000259017193f * fTemp35) + (0.00624397956f * fTemp38)) - ((0.0273077898f * fTemp40) + ((0.00537092704f * fTemp37) + ((0.0437610447f * fTemp39) + ((0.0189718362f * fTemp34) + (0.0239363015f * fTemp36)))))));
			float fTemp137 = (fConst78 * fRec250[1]);
			float fTemp138 = (fConst79 * fRec253[1]);
			fRec255[0] = (fTemp136 + (fTemp137 + (fRec255[1] + fTemp138)));
			fRec253[0] = fRec255[0];
			float fRec254 = ((fTemp138 + fTemp137) + fTemp136);
			fRec252[0] = (fRec252[1] + fRec253[0]);
			fRec250[0] = fRec252[0];
			float fRec251 = fRec254;
			fRec249[0] = ((fRec249[1] + fTemp135) + fRec251);
			fRec247[0] = fRec249[0];
			float fRec248 = (fTemp135 + fRec251);
			float fTemp139 = (fConst81 * fRec256[1]);
			float fTemp140 = (fConst82 * fRec259[1]);
			float fTemp141 = (fConst84 * (((0.0377700105f * fTemp22) + ((0.00749708479f * fTemp21) + ((0.0205384661f * fTemp17) + ((0.00247007096f * fTemp20) + ((0.00634144247f * fTemp14) + (0.0111316806f * fTemp16)))))) - (((0.0225200728f * fTemp18) + (0.000971430622f * fTemp15)) + (0.0267060734f * fTemp19))));
			float fTemp142 = (fConst85 * fRec262[1]);
			float fTemp143 = (fConst86 * fRec265[1]);
			fRec267[0] = (fTemp141 + (fTemp142 + (fRec267[1] + fTemp143)));
			fRec265[0] = fRec267[0];
			float fRec266 = ((fTemp143 + fTemp142) + fTemp141);
			fRec264[0] = (fRec264[1] + fRec265[0]);
			fRec262[0] = fRec264[0];
			float fRec263 = fRec266;
			fRec261[0] = ((fTemp139 + (fRec261[1] + fTemp140)) + fRec263);
			fRec259[0] = fRec261[0];
			float fRec260 = ((fTemp140 + fTemp139) + fRec263);
			fRec258[0] = (fRec258[1] + fRec259[0]);
			fRec256[0] = fRec258[0];
			float fRec257 = fRec260;
			fVec7[(IOTA & 1023)] = ((0.0296740122f * fTemp45) + (((fRec239 + fRec242) + fRec248) + fRec257));
			output7[i] = FAUSTFLOAT((0.825255752f * (fRec0[0] * fVec7[((IOTA - iConst87) & 1023)])));
			float fTemp144 = (fConst89 * fRec268[1]);
			float fTemp145 = (fConst91 * (((0.00235916674f * fTemp39) + ((0.0203794725f * fTemp35) + (0.050384786f * fTemp38))) - ((0.0347099751f * fTemp40) + ((0.00613554753f * fTemp37) + ((0.00433786446f * fTemp34) + (0.00180905988f * fTemp36))))));
			float fTemp146 = (fConst92 * fRec271[1]);
			float fTemp147 = (fConst93 * fRec274[1]);
			fRec276[0] = (fTemp145 + (fTemp146 + (fRec276[1] + fTemp147)));
			fRec274[0] = fRec276[0];
			float fRec275 = ((fTemp147 + fTemp146) + fTemp145);
			fRec273[0] = (fRec273[1] + fRec274[0]);
			fRec271[0] = fRec273[0];
			float fRec272 = fRec275;
			fRec270[0] = ((fRec270[1] + fTemp144) + fRec272);
			fRec268[0] = fRec270[0];
			float fRec269 = (fTemp144 + fRec272);
			float fTemp148 = (fConst95 * (((0.0460188352f * fTemp9) + (0.00239910278f * fTemp7)) - (0.0275209881f * fTemp8)));
			float fTemp149 = (fConst96 * fRec277[1]);
			fRec279[0] = (fTemp148 + (fRec279[1] + fTemp149));
			fRec277[0] = fRec279[0];
			float fRec278 = (fTemp149 + fTemp148);
			float fTemp150 = (fConst98 * ((0.00402585231f * fTemp30) - ((((0.047910817f * fTemp26) + (0.0318393186f * fTemp27)) + (0.00305585004f * fTemp28)) + (0.0257446915f * fTemp29))));
			float fTemp151 = (fConst99 * fRec280[1]);
			float fTemp152 = (fConst100 * fRec283[1]);
			fRec285[0] = (fTemp150 + (fTemp151 + (fRec285[1] + fTemp152)));
			fRec283[0] = fRec285[0];
			float fRec284 = ((fTemp152 + fTemp151) + fTemp150);
			fRec282[0] = (fRec282[1] + fRec283[0]);
			fRec280[0] = fRec282[0];
			float fRec281 = fRec284;
			float fTemp153 = (fConst102 * fRec286[1]);
			float fTemp154 = (fConst103 * fRec289[1]);
			float fTemp155 = (fConst105 * (((0.00713762827f * fTemp21) + ((0.01698534f * fTemp17) + ((0.00396325951f * fTemp20) + ((((0.0358571522f * fTemp14) + (0.00241139205f * fTemp18)) + (0.0298297461f * fTemp15)) + (0.0233165901f * fTemp16))))) - ((0.00528474478f * fTemp19) + (0.0239895768f * fTemp22))));
			float fTemp156 = (fConst106 * fRec292[1]);
			float fTemp157 = (fConst107 * fRec295[1]);
			fRec297[0] = (fTemp155 + (fTemp156 + (fRec297[1] + fTemp157)));
			fRec295[0] = fRec297[0];
			float fRec296 = ((fTemp157 + fTemp156) + fTemp155);
			fRec294[0] = (fRec294[1] + fRec295[0]);
			fRec292[0] = fRec294[0];
			float fRec293 = fRec296;
			fRec291[0] = ((fTemp153 + (fRec291[1] + fTemp154)) + fRec293);
			fRec289[0] = fRec291[0];
			float fRec290 = ((fTemp154 + fTemp153) + fRec293);
			fRec288[0] = (fRec288[1] + fRec289[0]);
			fRec286[0] = fRec288[0];
			float fRec287 = fRec290;
			fVec8[(IOTA & 1023)] = ((0.0324214213f * fTemp45) + (((fRec269 + fRec278) + fRec281) + fRec287));
			output8[i] = FAUSTFLOAT((0.825772464f * (fRec0[0] * fVec8[((IOTA - iConst108) & 1023)])));
			float fTemp158 = (fConst95 * (((0.0206746776f * fTemp9) + (0.00365679874f * fTemp7)) - (0.0392839573f * fTemp8)));
			float fTemp159 = (fConst96 * fRec298[1]);
			fRec300[0] = (fTemp158 + (fRec300[1] + fTemp159));
			fRec298[0] = fRec300[0];
			float fRec299 = (fTemp159 + fTemp158);
			float fTemp160 = (fConst98 * (((0.00495913578f * fTemp30) + (0.0259169079f * fTemp29)) - (((0.0375389159f * fTemp26) + (0.0261093918f * fTemp27)) + (0.00595670892f * fTemp28))));
			float fTemp161 = (fConst99 * fRec301[1]);
			float fTemp162 = (fConst100 * fRec304[1]);
			fRec306[0] = (fTemp160 + (fTemp161 + (fRec306[1] + fTemp162)));
			fRec304[0] = fRec306[0];
			float fRec305 = ((fTemp162 + fTemp161) + fTemp160);
			fRec303[0] = (fRec303[1] + fRec304[0]);
			fRec301[0] = fRec303[0];
			float fRec302 = fRec305;
			float fTemp163 = (fConst89 * fRec307[1]);
			float fTemp164 = (fConst91 * (((0.0430174358f * fTemp39) + ((0.029415274f * fTemp35) + (0.00186196622f * fTemp36))) - ((0.0149666639f * fTemp40) + ((0.00955280382f * fTemp37) + ((0.00693474943f * fTemp34) + (0.00538756512f * fTemp38))))));
			float fTemp165 = (fConst92 * fRec310[1]);
			float fTemp166 = (fConst93 * fRec313[1]);
			fRec315[0] = (fTemp164 + (fTemp165 + (fRec315[1] + fTemp166)));
			fRec313[0] = fRec315[0];
			float fRec314 = ((fTemp166 + fTemp165) + fTemp164);
			fRec312[0] = (fRec312[1] + fRec313[0]);
			fRec310[0] = fRec312[0];
			float fRec311 = fRec314;
			fRec309[0] = ((fRec309[1] + fTemp163) + fRec311);
			fRec307[0] = fRec309[0];
			float fRec308 = (fTemp163 + fRec311);
			float fTemp167 = (fConst105 * ((((0.00834930222f * fTemp20) + (((0.00999347214f * fTemp18) + (0.0227805395f * fTemp15)) + (0.0188161395f * fTemp16))) + (0.00512609445f * fTemp21)) - ((((0.036219418f * fTemp14) + (0.00697720563f * fTemp19)) + (0.0171614718f * fTemp17)) + (0.0133182807f * fTemp22))));
			float fTemp168 = (fConst106 * fRec322[1]);
			float fTemp169 = (fConst107 * fRec325[1]);
			fRec327[0] = (fTemp167 + (fTemp168 + (fRec327[1] + fTemp169)));
			fRec325[0] = fRec327[0];
			float fRec326 = ((fTemp169 + fTemp168) + fTemp167);
			fRec324[0] = (fRec324[1] + fRec325[0]);
			fRec322[0] = fRec324[0];
			float fRec323 = fRec326;
			float fTemp170 = (fConst102 * fRec316[1]);
			float fTemp171 = (fConst103 * fRec319[1]);
			fRec321[0] = (fRec323 + (fTemp170 + (fRec321[1] + fTemp171)));
			fRec319[0] = fRec321[0];
			float fRec320 = ((fTemp171 + fTemp170) + fRec323);
			fRec318[0] = (fRec318[1] + fRec319[0]);
			fRec316[0] = fRec318[0];
			float fRec317 = fRec320;
			fVec9[(IOTA & 1023)] = ((0.0267503597f * fTemp45) + (((fRec299 + fRec302) + fRec308) + fRec317));
			output9[i] = FAUSTFLOAT((0.825772464f * (fRec0[0] * fVec9[((IOTA - iConst108) & 1023)])));
			float fTemp172 = (fConst81 * fRec328[1]);
			float fTemp173 = (fConst82 * fRec331[1]);
			float fTemp174 = (fConst84 * ((((0.0267242976f * fTemp20) + ((0.0075121671f * fTemp18) + (0.0111189736f * fTemp16))) + (0.0377496704f * fTemp22)) - ((0.0225267373f * fTemp21) + ((((0.00636390084f * fTemp14) + (0.000970638008f * fTemp15)) + (0.00247365818f * fTemp19)) + (0.0205242913f * fTemp17)))));
			float fTemp175 = (fConst85 * fRec334[1]);
			float fTemp176 = (fConst86 * fRec337[1]);
			fRec339[0] = (fTemp174 + (fTemp175 + (fRec339[1] + fTemp176)));
			fRec337[0] = fRec339[0];
			float fRec338 = ((fTemp176 + fTemp175) + fTemp174);
			fRec336[0] = (fRec337[0] + fRec336[1]);
			fRec334[0] = fRec336[0];
			float fRec335 = fRec338;
			fRec333[0] = (fTemp172 + (fTemp173 + (fRec335 + fRec333[1])));
			fRec331[0] = fRec333[0];
			float fRec332 = (fTemp172 + (fRec335 + fTemp173));
			fRec330[0] = (fRec331[0] + fRec330[1]);
			fRec328[0] = fRec330[0];
			float fRec329 = fRec332;
			float fTemp177 = (fConst75 * fRec340[1]);
			float fTemp178 = (fConst77 * (((0.00626526307f * fTemp39) + ((0.0272907466f * fTemp35) + (0.02394839f * fTemp36))) - ((0.000260878587f * fTemp40) + ((0.00538331689f * fTemp37) + ((0.0189852081f * fTemp34) + (0.0437391959f * fTemp38))))));
			float fTemp179 = (fConst78 * fRec343[1]);
			float fTemp180 = (fConst79 * fRec346[1]);
			fRec348[0] = (fTemp178 + (fTemp179 + (fRec348[1] + fTemp180)));
			fRec346[0] = fRec348[0];
			float fRec347 = ((fTemp180 + fTemp179) + fTemp178);
			fRec345[0] = (fRec346[0] + fRec345[1]);
			fRec343[0] = fRec345[0];
			float fRec344 = fRec347;
			fRec342[0] = (fTemp177 + (fRec344 + fRec342[1]));
			fRec340[0] = fRec342[0];
			float fRec341 = (fRec344 + fTemp177);
			float fTemp181 = (fConst68 * (((0.00272243051f * fTemp9) + (0.0110227168f * fTemp7)) - (0.0479010195f * fTemp8)));
			float fTemp182 = (fConst69 * fRec349[1]);
			fRec351[0] = (fTemp181 + (fRec351[1] + fTemp182));
			fRec349[0] = fRec351[0];
			float fRec350 = (fTemp182 + fTemp181);
			float fTemp183 = (fConst71 * (((0.00250748661f * fTemp30) + (0.0476305895f * fTemp29)) - (((0.00501013687f * fTemp26) + (0.0248913877f * fTemp27)) + (0.0218859967f * fTemp28))));
			float fTemp184 = (fConst72 * fRec352[1]);
			float fTemp185 = (fConst73 * fRec355[1]);
			fRec357[0] = (fTemp183 + (fTemp184 + (fRec357[1] + fTemp185)));
			fRec355[0] = fRec357[0];
			float fRec356 = ((fTemp185 + fTemp184) + fTemp183);
			fRec354[0] = (fRec355[0] + fRec354[1]);
			fRec352[0] = fRec354[0];
			float fRec353 = fRec356;
			fVec10[(IOTA & 1023)] = ((0.029665513f * fTemp45) + (fRec329 + (fRec341 + (fRec350 + fRec353))));
			output10[i] = FAUSTFLOAT((0.825255752f * (fRec0[0] * fVec10[((IOTA - iConst87) & 1023)])));
			float fTemp186 = (fConst102 * fRec358[1]);
			float fTemp187 = (fConst103 * fRec361[1]);
			float fTemp188 = (fConst105 * ((((0.0146306194f * fTemp20) + (((0.0382039882f * fTemp14) + (0.0112971468f * fTemp19)) + (0.0201456752f * fTemp16))) + (0.00749935955f * fTemp21)) - ((((0.0167528167f * fTemp18) + (0.0277264602f * fTemp15)) + (0.0171313267f * fTemp17)) + (0.0293669961f * fTemp22))));
			float fTemp189 = (fConst106 * fRec364[1]);
			float fTemp190 = (fConst107 * fRec367[1]);
			fRec369[0] = (fTemp188 + (fTemp189 + (fRec369[1] + fTemp190)));
			fRec367[0] = fRec369[0];
			float fRec368 = ((fTemp190 + fTemp189) + fTemp188);
			fRec366[0] = (fRec367[0] + fRec366[1]);
			fRec364[0] = fRec366[0];
			float fRec365 = fRec368;
			fRec363[0] = (fTemp186 + (fTemp187 + (fRec365 + fRec363[1])));
			fRec361[0] = fRec363[0];
			float fRec362 = (fTemp186 + (fRec365 + fTemp187));
			fRec360[0] = (fRec361[0] + fRec360[1]);
			fRec358[0] = fRec360[0];
			float fRec359 = fRec362;
			float fTemp191 = (fConst89 * fRec370[1]);
			float fTemp192 = (fConst91 * (((0.0211702045f * fTemp40) + ((0.0181888249f * fTemp37) + ((0.00623383f * fTemp38) + ((0.036154028f * fTemp35) + (0.00429912098f * fTemp36))))) - ((0.0584637783f * fTemp39) + (0.013727935f * fTemp34))));
			float fTemp193 = (fConst92 * fRec373[1]);
			float fTemp194 = (fConst93 * fRec376[1]);
			fRec378[0] = (fTemp192 + (fTemp193 + (fRec378[1] + fTemp194)));
			fRec376[0] = fRec378[0];
			float fRec377 = ((fTemp194 + fTemp193) + fTemp192);
			fRec375[0] = (fRec376[0] + fRec375[1]);
			fRec373[0] = fRec375[0];
			float fRec374 = fRec377;
			fRec372[0] = (fTemp191 + (fRec374 + fRec372[1]));
			fRec370[0] = fRec372[0];
			float fRec371 = (fRec374 + fTemp191);
			float fTemp195 = (fConst95 * ((0.00846940465f * fTemp7) - ((0.0355539881f * fTemp9) + (0.0561628602f * fTemp8))));
			float fTemp196 = (fConst96 * fRec379[1]);
			fRec381[0] = (fTemp195 + (fRec381[1] + fTemp196));
			fRec379[0] = fRec381[0];
			float fRec380 = (fTemp196 + fTemp195);
			float fTemp197 = (fConst98 * (((0.0591501631f * fTemp26) + (0.0283936244f * fTemp29)) - (((0.0104484083f * fTemp30) + (0.0363615789f * fTemp27)) + (0.0130950417f * fTemp28))));
			float fTemp198 = (fConst99 * fRec382[1]);
			float fTemp199 = (fConst100 * fRec385[1]);
			fRec387[0] = (fTemp197 + (fTemp198 + (fRec387[1] + fTemp199)));
			fRec385[0] = fRec387[0];
			float fRec386 = ((fTemp199 + fTemp198) + fTemp197);
			fRec384[0] = (fRec385[0] + fRec384[1]);
			fRec382[0] = fRec384[0];
			float fRec383 = fRec386;
			fVec11[(IOTA & 1023)] = ((0.0410089977f * fTemp45) + (fRec359 + (fRec371 + (fRec380 + fRec383))));
			output11[i] = FAUSTFLOAT((0.825772464f * (fRec0[0] * fVec11[((IOTA - iConst108) & 1023)])));
			float fTemp200 = (fConst102 * fRec388[1]);
			float fTemp201 = (fConst103 * fRec391[1]);
			float fTemp202 = (fConst105 * (((0.0100131826f * fTemp21) + ((0.0175028779f * fTemp17) + ((0.00562107656f * fTemp20) + ((0.00914157555f * fTemp19) + (0.0188116413f * fTemp16))))) - ((((0.0390624069f * fTemp14) + (0.000321889587f * fTemp18)) + (0.0225733947f * fTemp15)) + (0.00839614682f * fTemp22))));
			float fTemp203 = (fConst106 * fRec394[1]);
			float fTemp204 = (fConst107 * fRec397[1]);
			fRec399[0] = (fTemp202 + (fTemp203 + (fRec399[1] + fTemp204)));
			fRec397[0] = fRec399[0];
			float fRec398 = ((fTemp204 + fTemp203) + fTemp202);
			fRec396[0] = (fRec397[0] + fRec396[1]);
			fRec394[0] = fRec396[0];
			float fRec395 = fRec398;
			fRec393[0] = (fTemp200 + (fTemp201 + (fRec395 + fRec393[1])));
			fRec391[0] = fRec393[0];
			float fRec392 = (fTemp200 + (fRec395 + fTemp201));
			fRec390[0] = (fRec391[0] + fRec390[1]);
			fRec388[0] = fRec390[0];
			float fRec389 = fRec392;
			float fTemp205 = (fConst91 * ((((((0.0144589525f * fTemp35) + (0.0430146195f * fTemp38)) + (0.010089837f * fTemp39)) + (0.00807257369f * fTemp37)) + (0.0296988003f * fTemp40)) - ((0.00694228988f * fTemp34) + (0.00445707561f * fTemp36))));
			float fTemp206 = (fConst93 * fRec406[1]);
			float fTemp207 = (fConst92 * fRec403[1]);
			fRec408[0] = (((fTemp205 + fRec408[1]) + fTemp206) + fTemp207);
			fRec406[0] = fRec408[0];
			float fRec407 = ((fTemp205 + fTemp206) + fTemp207);
			fRec405[0] = (fRec406[0] + fRec405[1]);
			fRec403[0] = fRec405[0];
			float fRec404 = fRec407;
			float fTemp208 = (fConst89 * fRec400[1]);
			fRec402[0] = ((fRec404 + fRec402[1]) + fTemp208);
			fRec400[0] = fRec402[0];
			float fRec401 = (fRec404 + fTemp208);
			float fTemp209 = (fConst95 * ((0.0036629024f * fTemp7) - ((0.040098194f * fTemp9) + (0.0192500781f * fTemp8))));
			float fTemp210 = (fConst96 * fRec409[1]);
			fRec411[0] = (fTemp209 + (fRec411[1] + fTemp210));
			fRec409[0] = fRec411[0];
			float fRec410 = (fTemp210 + fTemp209);
			float fTemp211 = (fConst98 * ((0.0358814336f * fTemp26) - ((((0.00659731729f * fTemp30) + (0.0261035636f * fTemp27)) + (0.00387390563f * fTemp28)) + (0.0287762228f * fTemp29))));
			float fTemp212 = (fConst99 * fRec412[1]);
			float fTemp213 = (fConst100 * fRec415[1]);
			fRec417[0] = (fTemp211 + (fTemp212 + (fRec417[1] + fTemp213)));
			fRec415[0] = fRec417[0];
			float fRec416 = ((fTemp213 + fTemp212) + fTemp211);
			fRec414[0] = (fRec415[0] + fRec414[1]);
			fRec412[0] = fRec414[0];
			float fRec413 = fRec416;
			fVec12[(IOTA & 1023)] = ((fRec389 + (fRec401 + (fRec410 + fRec413))) + (0.0267459098f * fTemp45));
			output12[i] = FAUSTFLOAT((0.825772464f * (fRec0[0] * fVec12[((IOTA - iConst108) & 1023)])));
			float fTemp214 = (fConst81 * fRec418[1]);
			float fTemp215 = (fConst82 * fRec421[1]);
			float fTemp216 = (fConst84 * (((((((0.00632829405f * fTemp14) + (0.0225127544f * fTemp18)) + (0.0267035142f * fTemp19)) + (0.011113028f * fTemp16)) + (0.0205247216f * fTemp17)) + (0.0377550609f * fTemp22)) - (((0.000994727248f * fTemp15) + (0.00247249682f * fTemp20)) + (0.00751692522f * fTemp21))));
			float fTemp217 = (fConst85 * fRec424[1]);
			float fTemp218 = (fConst86 * fRec427[1]);
			fRec429[0] = (fTemp216 + (fTemp217 + (fRec429[1] + fTemp218)));
			fRec427[0] = fRec429[0];
			float fRec428 = ((fTemp218 + fTemp217) + fTemp216);
			fRec426[0] = (fRec427[0] + fRec426[1]);
			fRec424[0] = fRec426[0];
			float fRec425 = fRec428;
			fRec423[0] = (fTemp214 + (fTemp215 + (fRec425 + fRec423[1])));
			fRec421[0] = fRec423[0];
			float fRec422 = (fTemp214 + (fRec425 + fTemp215));
			fRec420[0] = (fRec421[0] + fRec420[1]);
			fRec418[0] = fRec420[0];
			float fRec419 = fRec422;
			float fTemp219 = (fConst75 * fRec430[1]);
			float fTemp220 = (fConst77 * (((0.0437380597f * fTemp39) + (0.0272824895f * fTemp40)) - ((0.00538961869f * fTemp37) + ((0.00623743841f * fTemp38) + (((0.0189735387f * fTemp34) + (0.000248278608f * fTemp35)) + (0.0239343904f * fTemp36))))));
			float fTemp221 = (fConst78 * fRec433[1]);
			float fTemp222 = (fConst79 * fRec436[1]);
			fRec438[0] = (fTemp220 + (fTemp221 + (fRec438[1] + fTemp222)));
			fRec436[0] = fRec438[0];
			float fRec437 = ((fTemp222 + fTemp221) + fTemp220);
			fRec435[0] = (fRec436[0] + fRec435[1]);
			fRec433[0] = fRec435[0];
			float fRec434 = fRec437;
			fRec432[0] = (fTemp219 + (fRec434 + fRec432[1]));
			fRec430[0] = fRec432[0];
			float fRec431 = (fRec434 + fTemp219);
			float fTemp223 = (fConst68 * (((0.0110191144f * fTemp7) + (0.00271522603f * fTemp8)) - (0.0478933044f * fTemp9)));
			float fTemp224 = (fConst69 * fRec439[1]);
			fRec441[0] = (fTemp223 + (fRec441[1] + fTemp224));
			fRec439[0] = fRec441[0];
			float fRec440 = (fTemp224 + fTemp223);
			float fTemp225 = (fConst71 * ((0.0025117835f * fTemp28) - ((((0.00499273418f * fTemp26) + (0.0218761284f * fTemp30)) + (0.0248842426f * fTemp27)) + (0.0476244874f * fTemp29))));
			float fTemp226 = (fConst72 * fRec442[1]);
			float fTemp227 = (fConst73 * fRec445[1]);
			fRec447[0] = (fTemp225 + (fTemp226 + (fRec447[1] + fTemp227)));
			fRec445[0] = fRec447[0];
			float fRec446 = ((fTemp227 + fTemp226) + fTemp225);
			fRec444[0] = (fRec445[0] + fRec444[1]);
			fRec442[0] = fRec444[0];
			float fRec443 = fRec446;
			fVec13[(IOTA & 1023)] = ((0.0296611208f * fTemp45) + (fRec419 + (fRec431 + (fRec440 + fRec443))));
			output13[i] = FAUSTFLOAT((0.825255752f * (fRec0[0] * fVec13[((IOTA - iConst87) & 1023)])));
			float fTemp228 = (fConst102 * fRec448[1]);
			float fTemp229 = (fConst103 * fRec451[1]);
			float fTemp230 = (fConst105 * ((((((0.036236804f * fTemp14) + (0.0227813944f * fTemp15)) + (0.00838163868f * fTemp19)) + (0.0188177116f * fTemp16)) + (0.0171661526f * fTemp17)) - ((0.0133097423f * fTemp22) + (((0.00510163652f * fTemp18) + (0.00698627252f * fTemp20)) + (0.0100233201f * fTemp21)))));
			float fTemp231 = (fConst106 * fRec454[1]);
			float fTemp232 = (fConst107 * fRec457[1]);
			fRec459[0] = (fTemp230 + (fTemp231 + (fRec459[1] + fTemp232)));
			fRec457[0] = fRec459[0];
			float fRec458 = ((fTemp232 + fTemp231) + fTemp230);
			fRec456[0] = (fRec457[0] + fRec456[1]);
			fRec454[0] = fRec456[0];
			float fRec455 = fRec458;
			fRec453[0] = (fTemp228 + (fTemp229 + (fRec455 + fRec453[1])));
			fRec451[0] = fRec453[0];
			float fRec452 = (fTemp228 + (fRec455 + fTemp229));
			fRec450[0] = (fRec451[0] + fRec450[1]);
			fRec448[0] = fRec450[0];
			float fRec449 = fRec452;
			float fTemp233 = (fConst89 * fRec460[1]);
			float fTemp234 = (fConst91 * (((0.00540215429f * fTemp39) + (0.0294207837f * fTemp40)) - ((0.00956894178f * fTemp37) + ((0.043026831f * fTemp38) + (((0.0069561433f * fTemp34) + (0.0149665643f * fTemp35)) + (0.00188986782f * fTemp36))))));
			float fTemp235 = (fConst92 * fRec463[1]);
			float fTemp236 = (fConst93 * fRec466[1]);
			fRec468[0] = (fTemp234 + (fTemp235 + (fRec468[1] + fTemp236)));
			fRec466[0] = fRec468[0];
			float fRec467 = ((fTemp236 + fTemp235) + fTemp234);
			fRec465[0] = (fRec466[0] + fRec465[1]);
			fRec463[0] = fRec465[0];
			float fRec464 = fRec467;
			fRec462[0] = (fTemp233 + (fRec464 + fRec462[1]));
			fRec460[0] = fRec462[0];
			float fRec461 = (fRec464 + fTemp233);
			float fTemp237 = (fConst95 * (((0.0036679192f * fTemp7) + (0.0206729658f * fTemp8)) - (0.0392920151f * fTemp9)));
			float fTemp238 = (fConst96 * fRec469[1]);
			fRec471[0] = ((fTemp237 + fRec471[1]) + fTemp238);
			fRec469[0] = fRec471[0];
			float fRec470 = (fTemp237 + fTemp238);
			float fTemp239 = (fConst98 * ((0.00496469717f * fTemp28) - ((((0.0375401974f * fTemp26) + (0.00598042877f * fTemp30)) + (0.0261131581f * fTemp27)) + (0.0259301588f * fTemp29))));
			float fTemp240 = (fConst99 * fRec472[1]);
			float fTemp241 = (fConst100 * fRec475[1]);
			fRec477[0] = (fTemp239 + (fTemp240 + (fRec477[1] + fTemp241)));
			fRec475[0] = fRec477[0];
			float fRec476 = ((fTemp241 + fTemp240) + fTemp239);
			fRec474[0] = (fRec475[0] + fRec474[1]);
			fRec472[0] = fRec474[0];
			float fRec473 = fRec476;
			fVec14[(IOTA & 1023)] = ((fRec449 + (fRec461 + (fRec470 + fRec473))) + (0.026753651f * fTemp45));
			output14[i] = FAUSTFLOAT((0.825772464f * (fRec0[0] * fVec14[((IOTA - iConst108) & 1023)])));
			float fTemp242 = (fConst102 * fRec478[1]);
			float fTemp243 = (fConst103 * fRec481[1]);
			float fTemp244 = (fConst105 * ((((0.0298299026f * fTemp15) + (0.00395053998f * fTemp19)) + (0.0233223774f * fTemp16)) - ((0.0239861924f * fTemp22) + ((0.00238312478f * fTemp21) + ((0.0169941448f * fTemp17) + (((0.0358645692f * fTemp14) + (0.00714203669f * fTemp18)) + (0.00528561603f * fTemp20)))))));
			float fTemp245 = (fConst106 * fRec484[1]);
			float fTemp246 = (fConst107 * fRec487[1]);
			fRec489[0] = (fTemp244 + (fTemp245 + (fRec489[1] + fTemp246)));
			fRec487[0] = fRec489[0];
			float fRec488 = ((fTemp246 + fTemp245) + fTemp244);
			fRec486[0] = (fRec487[0] + fRec486[1]);
			fRec484[0] = fRec486[0];
			float fRec485 = fRec488;
			fRec483[0] = (fTemp242 + (fTemp243 + (fRec485 + fRec483[1])));
			fRec481[0] = fRec483[0];
			float fRec482 = (fTemp242 + (fRec485 + fTemp243));
			fRec480[0] = (fRec481[0] + fRec480[1]);
			fRec478[0] = fRec480[0];
			float fRec479 = fRec482;
			float fTemp247 = (fConst89 * fRec490[1]);
			float fTemp248 = (fConst91 * (((0.0203805249f * fTemp40) + (0.00182607665f * fTemp36)) - ((0.0061250506f * fTemp37) + ((0.0503935479f * fTemp39) + (((0.00433409773f * fTemp34) + (0.034719348f * fTemp35)) + (0.0023518533f * fTemp38))))));
			float fTemp249 = (fConst92 * fRec493[1]);
			float fTemp250 = (fConst93 * fRec496[1]);
			fRec498[0] = (fTemp248 + (fTemp249 + (fRec498[1] + fTemp250)));
			fRec496[0] = fRec498[0];
			float fRec497 = ((fTemp250 + fTemp249) + fTemp248);
			fRec495[0] = (fRec496[0] + fRec495[1]);
			fRec493[0] = fRec495[0];
			float fRec494 = fRec497;
			fRec492[0] = (fTemp247 + (fRec494 + fRec492[1]));
			fRec490[0] = fRec492[0];
			float fRec491 = (fRec494 + fTemp247);
			float fTemp251 = (fConst95 * (((0.00239751418f * fTemp7) + (0.0460310802f * fTemp8)) - (0.0275247861f * fTemp9)));
			float fTemp252 = (fConst96 * fRec499[1]);
			fRec501[0] = (fTemp251 + (fRec501[1] + fTemp252));
			fRec499[0] = fRec501[0];
			float fRec500 = (fTemp252 + fTemp251);
			float fTemp253 = (fConst98 * (((0.00402801251f * fTemp28) + (0.0257554352f * fTemp29)) - (((0.0479180031f * fTemp26) + (0.00304666511f * fTemp30)) + (0.031846758f * fTemp27))));
			float fTemp254 = (fConst99 * fRec502[1]);
			float fTemp255 = (fConst100 * fRec505[1]);
			fRec507[0] = (fTemp253 + (fTemp254 + (fRec507[1] + fTemp255)));
			fRec505[0] = fRec507[0];
			float fRec506 = ((fTemp255 + fTemp254) + fTemp253);
			fRec504[0] = (fRec505[0] + fRec504[1]);
			fRec502[0] = fRec504[0];
			float fRec503 = fRec506;
			fVec15[(IOTA & 1023)] = ((0.0324291773f * fTemp45) + (fRec479 + (fRec491 + (fRec500 + fRec503))));
			output15[i] = FAUSTFLOAT((0.825772464f * (fRec0[0] * fVec15[((IOTA - iConst108) & 1023)])));
			float fTemp256 = (fConst71 * (((0.0218786057f * fTemp28) + (0.0476387031f * fTemp29)) - (((0.0049914103f * fTemp26) + (0.00250061741f * fTemp30)) + (0.0248947348f * fTemp27))));
			float fTemp257 = (fConst72 * fRec508[1]);
			float fTemp258 = (fConst73 * fRec511[1]);
			fRec513[0] = (fTemp256 + (fTemp257 + (fRec513[1] + fTemp258)));
			fRec511[0] = fRec513[0];
			float fRec512 = ((fTemp258 + fTemp257) + fTemp256);
			fRec510[0] = (fRec511[0] + fRec510[1]);
			fRec508[0] = fRec510[0];
			float fRec509 = fRec512;
			float fTemp259 = (fConst68 * (((0.0110191759f * fTemp7) + (0.0479075424f * fTemp8)) - (0.00271278713f * fTemp9)));
			float fTemp260 = (fConst69 * fRec514[1]);
			fRec516[0] = (fTemp259 + (fRec516[1] + fTemp260));
			fRec514[0] = fRec516[0];
			float fRec515 = (fTemp260 + fTemp259);
			float fTemp261 = (fConst75 * fRec517[1]);
			float fTemp262 = (fConst77 * (((0.0002575533f * fTemp40) + ((0.023940945f * fTemp36) + (0.0437487811f * fTemp38))) - ((0.00536769675f * fTemp37) + ((0.00624047266f * fTemp39) + ((0.0189775303f * fTemp34) + (0.027295744f * fTemp35))))));
			float fTemp263 = (fConst78 * fRec520[1]);
			float fTemp264 = (fConst79 * fRec523[1]);
			fRec525[0] = (fTemp262 + (fTemp263 + (fRec525[1] + fTemp264)));
			fRec523[0] = fRec525[0];
			float fRec524 = ((fTemp264 + fTemp263) + fTemp262);
			fRec522[0] = (fRec523[0] + fRec522[1]);
			fRec520[0] = fRec522[0];
			float fRec521 = fRec524;
			fRec519[0] = (fTemp261 + (fRec521 + fRec519[1]));
			fRec517[0] = fRec519[0];
			float fRec518 = (fRec521 + fTemp261);
			float fTemp265 = (fConst81 * fRec526[1]);
			float fTemp266 = (fConst82 * fRec529[1]);
			float fTemp267 = (fConst84 * (((0.0377592891f * fTemp22) + (((0.0024657764f * fTemp19) + (0.0111223757f * fTemp16)) + (0.0225220621f * fTemp21))) - ((0.020530086f * fTemp17) + ((((0.00633708993f * fTemp14) + (0.00748936273f * fTemp18)) + (0.000972020498f * fTemp15)) + (0.0267130192f * fTemp20)))));
			float fTemp268 = (fConst85 * fRec532[1]);
			float fTemp269 = (fConst86 * fRec535[1]);
			fRec537[0] = (fTemp267 + (fTemp268 + (fRec537[1] + fTemp269)));
			fRec535[0] = fRec537[0];
			float fRec536 = ((fTemp269 + fTemp268) + fTemp267);
			fRec534[0] = (fRec535[0] + fRec534[1]);
			fRec532[0] = fRec534[0];
			float fRec533 = fRec536;
			fRec531[0] = (fTemp265 + (fTemp266 + (fRec533 + fRec531[1])));
			fRec529[0] = fRec531[0];
			float fRec530 = (fTemp265 + (fRec533 + fTemp266));
			fRec528[0] = (fRec529[0] + fRec528[1]);
			fRec526[0] = fRec528[0];
			float fRec527 = fRec530;
			fVec16[(IOTA & 1023)] = ((0.0296692289f * fTemp45) + (fRec509 + (fRec515 + (fRec518 + fRec527))));
			output16[i] = FAUSTFLOAT((0.825255752f * (fRec0[0] * fVec16[((IOTA - iConst87) & 1023)])));
			float fTemp270 = (fConst102 * fRec538[1]);
			float fTemp271 = (fConst103 * fRec541[1]);
			float fTemp272 = (fConst105 * ((((0.038214758f * fTemp14) + (0.0167307854f * fTemp18)) + (0.0201459322f * fTemp16)) - ((0.0293682199f * fTemp22) + ((0.00749705313f * fTemp21) + ((0.017142918f * fTemp17) + (((0.0277294759f * fTemp15) + (0.0112831919f * fTemp19)) + (0.0146096526f * fTemp20)))))));
			float fTemp273 = (fConst106 * fRec544[1]);
			float fTemp274 = (fConst107 * fRec547[1]);
			fRec549[0] = (fTemp272 + (fTemp273 + (fRec549[1] + fTemp274)));
			fRec547[0] = fRec549[0];
			float fRec548 = ((fTemp274 + fTemp273) + fTemp272);
			fRec546[0] = (fRec547[0] + fRec546[1]);
			fRec544[0] = fRec546[0];
			float fRec545 = fRec548;
			fRec543[0] = (fTemp270 + (fTemp271 + (fRec545 + fRec543[1])));
			fRec541[0] = fRec543[0];
			float fRec542 = (fTemp270 + (fRec545 + fTemp271));
			fRec540[0] = (fRec541[0] + fRec540[1]);
			fRec538[0] = fRec540[0];
			float fRec539 = fRec542;
			float fTemp275 = (fConst89 * fRec550[1]);
			float fTemp276 = (fConst91 * (((0.0181694906f * fTemp37) + ((0.0584710091f * fTemp39) + (0.00428620027f * fTemp36))) - ((0.0211695898f * fTemp40) + (((0.0137110623f * fTemp34) + (0.0361602344f * fTemp35)) + (0.00623232685f * fTemp38)))));
			float fTemp277 = (fConst92 * fRec553[1]);
			float fTemp278 = (fConst93 * fRec556[1]);
			fRec558[0] = (fTemp276 + (fTemp277 + (fRec558[1] + fTemp278)));
			fRec556[0] = fRec558[0];
			float fRec557 = ((fTemp278 + fTemp277) + fTemp276);
			fRec555[0] = (fRec556[0] + fRec555[1]);
			fRec553[0] = fRec555[0];
			float fRec554 = fRec557;
			fRec552[0] = (fTemp275 + (fRec554 + fRec552[1]));
			fRec550[0] = fRec552[0];
			float fRec551 = (fRec554 + fTemp275);
			float fTemp279 = (fConst95 * (((0.0355585851f * fTemp9) + (0.00846020691f * fTemp7)) + (0.0561676435f * fTemp8)));
			float fTemp280 = (fConst96 * fRec559[1]);
			fRec561[0] = (fTemp279 + (fRec561[1] + fTemp280));
			fRec559[0] = fRec561[0];
			float fRec560 = (fTemp280 + fTemp279);
			float fTemp281 = (fConst98 * (((((0.05915609f * fTemp26) + (0.0104403626f * fTemp30)) + (0.013077273f * fTemp28)) + (0.028395867f * fTemp29)) - (0.0363653377f * fTemp27)));
			float fTemp282 = (fConst99 * fRec562[1]);
			float fTemp283 = (fConst100 * fRec565[1]);
			fRec567[0] = (fTemp281 + (fTemp282 + (fRec567[1] + fTemp283)));
			fRec565[0] = fRec567[0];
			float fRec566 = ((fTemp283 + fTemp282) + fTemp281);
			fRec564[0] = (fRec565[0] + fRec564[1]);
			fRec562[0] = fRec564[0];
			float fRec563 = fRec566;
			fVec17[(IOTA & 1023)] = ((0.0410131775f * fTemp45) + (fRec539 + (fRec551 + (fRec560 + fRec563))));
			output17[i] = FAUSTFLOAT((0.825772464f * (fRec0[0] * fVec17[((IOTA - iConst108) & 1023)])));
			float fTemp284 = (fConst102 * fRec568[1]);
			float fTemp285 = (fConst103 * fRec571[1]);
			float fTemp286 = (fConst105 * ((((0.0003267102f * fTemp18) + (0.0188121404f * fTemp16)) + (0.0175062921f * fTemp17)) - ((0.00839922298f * fTemp22) + (((((0.0390599146f * fTemp14) + (0.0225756876f * fTemp15)) + (0.00917239767f * fTemp19)) + (0.00563843688f * fTemp20)) + (0.0100301197f * fTemp21)))));
			float fTemp287 = (fConst106 * fRec574[1]);
			float fTemp288 = (fConst107 * fRec577[1]);
			fRec579[0] = (fTemp286 + (fTemp287 + (fRec579[1] + fTemp288)));
			fRec577[0] = fRec579[0];
			float fRec578 = ((fTemp288 + fTemp287) + fTemp286);
			fRec576[0] = (fRec577[0] + fRec576[1]);
			fRec574[0] = fRec576[0];
			float fRec575 = fRec578;
			fRec573[0] = (fTemp284 + (fTemp285 + (fRec575 + fRec573[1])));
			fRec571[0] = fRec573[0];
			float fRec572 = (fTemp284 + (fRec575 + fTemp285));
			fRec570[0] = (fRec571[0] + fRec570[1]);
			fRec568[0] = fRec570[0];
			float fRec569 = fRec572;
			float fTemp289 = (fConst89 * fRec580[1]);
			float fTemp290 = (fConst91 * ((0.00809177849f * fTemp37) - ((0.0297035649f * fTemp40) + ((0.0100868177f * fTemp39) + ((((0.014461129f * fTemp35) + (0.00696406513f * fTemp34)) + (0.00446618395f * fTemp36)) + (0.0430178829f * fTemp38))))));
			float fTemp291 = (fConst92 * fRec583[1]);
			float fTemp292 = (fConst93 * fRec586[1]);
			fRec588[0] = (fTemp290 + (fTemp291 + (fRec588[1] + fTemp292)));
			fRec586[0] = fRec588[0];
			float fRec587 = ((fTemp292 + fTemp291) + fTemp290);
			fRec585[0] = (fRec586[0] + fRec585[1]);
			fRec583[0] = fRec585[0];
			float fRec584 = fRec587;
			fRec582[0] = (fTemp289 + (fRec584 + fRec582[1]));
			fRec580[0] = fRec582[0];
			float fRec581 = (fRec584 + fTemp289);
			float fTemp293 = (fConst95 * (((0.0401038378f * fTemp9) + (0.00367245008f * fTemp7)) + (0.0192545932f * fTemp8)));
			float fTemp294 = (fConst96 * fRec589[1]);
			fRec591[0] = (fTemp293 + (fRec591[1] + fTemp294));
			fRec589[0] = fRec591[0];
			float fRec590 = (fTemp294 + fTemp293);
			float fTemp295 = (fConst98 * ((((0.0358876549f * fTemp26) + (0.00661438983f * fTemp30)) + (0.00388430688f * fTemp28)) - ((0.0261082873f * fTemp27) + (0.0287773926f * fTemp29))));
			float fTemp296 = (fConst99 * fRec592[1]);
			float fTemp297 = (fConst100 * fRec595[1]);
			fRec597[0] = (fTemp295 + (fTemp296 + (fRec597[1] + fTemp297)));
			fRec595[0] = fRec597[0];
			float fRec596 = ((fTemp297 + fTemp296) + fTemp295);
			fRec594[0] = (fRec595[0] + fRec594[1]);
			fRec592[0] = fRec594[0];
			float fRec593 = fRec596;
			fVec18[(IOTA & 1023)] = ((0.0267503932f * fTemp45) + (fRec569 + (fRec581 + (fRec590 + fRec593))));
			output18[i] = FAUSTFLOAT((0.825772464f * (fRec0[0] * fVec18[((IOTA - iConst108) & 1023)])));
			float fTemp298 = (fConst110 * fRec598[1]);
			float fTemp299 = (fConst111 * fRec601[1]);
			float fTemp300 = (fConst113 * ((((((1.53417004e-05f * fTemp14) + (0.0477514416f * fTemp18)) + (0.020346595f * fTemp19)) + (8.0187001e-06f * fTemp20)) + (0.040051911f * fTemp22)) - ((6.14549981e-06f * fTemp21) + (((4.3588002e-06f * fTemp15) + (0.00278774789f * fTemp16)) + (0.0099853361f * fTemp17)))));
			float fTemp301 = (fConst114 * fRec604[1]);
			float fTemp302 = (fConst115 * fRec607[1]);
			fRec609[0] = (fTemp300 + (fTemp301 + (fRec609[1] + fTemp302)));
			fRec607[0] = fRec609[0];
			float fRec608 = ((fTemp302 + fTemp301) + fTemp300);
			fRec606[0] = (fRec607[0] + fRec606[1]);
			fRec604[0] = fRec606[0];
			float fRec605 = fRec608;
			fRec603[0] = (fTemp298 + (fTemp299 + (fRec605 + fRec603[1])));
			fRec601[0] = fRec603[0];
			float fRec602 = (fTemp298 + (fRec605 + fTemp299));
			fRec600[0] = (fRec601[0] + fRec600[1]);
			fRec598[0] = fRec600[0];
			float fRec599 = fRec602;
			float fTemp303 = (fConst117 * fRec610[1]);
			float fTemp304 = (fConst119 * (((0.000564895803f * fTemp40) + ((8.20439982e-06f * fTemp37) + ((1.86639008e-05f * fTemp38) + ((0.0250918176f * fTemp34) + (0.0537228286f * fTemp36))))) - ((0.0512112677f * fTemp39) + (3.33759999e-06f * fTemp35))));
			float fTemp305 = (fConst120 * fRec613[1]);
			float fTemp306 = (fConst121 * fRec616[1]);
			fRec618[0] = (fTemp304 + (fTemp305 + (fRec618[1] + fTemp306)));
			fRec616[0] = fRec618[0];
			float fRec617 = ((fTemp306 + fTemp305) + fTemp304);
			fRec615[0] = (fRec616[0] + fRec615[1]);
			fRec613[0] = fRec615[0];
			float fRec614 = fRec617;
			fRec612[0] = (fTemp303 + (fRec614 + fRec612[1]));
			fRec610[0] = fRec612[0];
			float fRec611 = (fRec614 + fTemp303);
			float fTemp307 = (fConst123 * ((0.0668016002f * fTemp9) - ((0.0337675065f * fTemp7) + (1.07285996e-05f * fTemp8))));
			float fTemp308 = (fConst124 * fRec619[1]);
			fRec621[0] = (fTemp307 + (fRec621[1] + fTemp308));
			fRec619[0] = fRec621[0];
			float fRec620 = (fTemp308 + fTemp307);
			float fTemp309 = (fConst126 * ((6.85509985e-06f * fTemp28) - ((((1.71022002e-05f * fTemp26) + (0.055088263f * fTemp30)) + (0.0130083254f * fTemp27)) + (0.0606702417f * fTemp29))));
			float fTemp310 = (fConst127 * fRec622[1]);
			float fTemp311 = (fConst128 * fRec625[1]);
			fRec627[0] = (fTemp309 + (fTemp310 + (fRec627[1] + fTemp311)));
			fRec625[0] = fRec627[0];
			float fRec626 = ((fTemp311 + fTemp310) + fTemp309);
			fRec624[0] = (fRec625[0] + fRec624[1]);
			fRec622[0] = fRec624[0];
			float fRec623 = fRec626;
			fVec19[0] = ((0.0466528162f * fTemp45) + (fRec599 + (fRec611 + (fRec620 + fRec623))));
			output19[i] = FAUSTFLOAT((0.999793351f * (fRec0[0] * fVec19[iConst129])));
			float fTemp312 = (fConst131 * ((0.0406398699f * fTemp9) - ((0.0313559137f * fTemp7) + (0.0234596878f * fTemp8))));
			float fTemp313 = (fConst132 * fRec628[1]);
			fRec630[0] = (fTemp312 + (fRec630[1] + fTemp313));
			fRec628[0] = fRec630[0];
			float fRec629 = (fTemp313 + fTemp312);
			float fTemp314 = (fConst134 * fRec631[1]);
			float fTemp315 = (fConst135 * fRec634[1]);
			float fTemp316 = (fConst137 * ((((0.0251543671f * fTemp14) + (2.20640004e-06f * fTemp18)) + (0.0108541893f * fTemp19)) - ((0.0145028802f * fTemp22) + ((0.0428572521f * fTemp21) + ((0.011761561f * fTemp17) + (((0.0203849878f * fTemp15) + (0.0129730217f * fTemp16)) + (0.00626636297f * fTemp20)))))));
			float fTemp317 = (fConst138 * fRec637[1]);
			float fTemp318 = (fConst139 * fRec640[1]);
			fRec642[0] = (fTemp316 + (fTemp317 + (fRec642[1] + fTemp318)));
			fRec640[0] = fRec642[0];
			float fRec641 = ((fTemp318 + fTemp317) + fTemp316);
			fRec639[0] = (fRec640[0] + fRec639[1]);
			fRec637[0] = fRec639[0];
			float fRec638 = fRec641;
			fRec636[0] = (fTemp314 + (fTemp315 + (fRec638 + fRec636[1])));
			fRec634[0] = fRec636[0];
			float fRec635 = (fTemp314 + (fRec638 + fTemp315));
			fRec633[0] = (fRec634[0] + fRec633[1]);
			fRec631[0] = fRec633[0];
			float fRec632 = fRec635;
			float fTemp319 = (fConst141 * ((0.0250726212f * fTemp28) - ((((0.0359448791f * fTemp26) + (0.0434271991f * fTemp30)) + (0.000599099381f * fTemp27)) + (0.020761501f * fTemp29))));
			float fTemp320 = (fConst142 * fRec643[1]);
			float fTemp321 = (fConst143 * fRec646[1]);
			fRec648[0] = (fTemp319 + (fTemp320 + (fRec648[1] + fTemp321)));
			fRec646[0] = fRec648[0];
			float fRec647 = ((fTemp321 + fTemp320) + fTemp319);
			fRec645[0] = (fRec646[0] + fRec645[1]);
			fRec643[0] = fRec645[0];
			float fRec644 = fRec647;
			float fTemp322 = (fConst145 * fRec649[1]);
			float fTemp323 = (fConst147 * (((0.0143792909f * fTemp40) + ((0.0418010689f * fTemp37) + ((0.0352554694f * fTemp38) + ((0.0202015545f * fTemp34) + (0.0241348408f * fTemp36))))) - ((1.2337e-05f * fTemp39) + (0.00830581039f * fTemp35))));
			float fTemp324 = (fConst148 * fRec652[1]);
			float fTemp325 = (fConst149 * fRec655[1]);
			fRec657[0] = (fTemp323 + (fTemp324 + (fRec657[1] + fTemp325)));
			fRec655[0] = fRec657[0];
			float fRec656 = ((fTemp325 + fTemp324) + fTemp323);
			fRec654[0] = (fRec655[0] + fRec654[1]);
			fRec652[0] = fRec654[0];
			float fRec653 = fRec656;
			fRec651[0] = (fTemp322 + (fRec653 + fRec651[1]));
			fRec649[0] = fRec651[0];
			float fRec650 = (fRec653 + fTemp322);
			output20[i] = FAUSTFLOAT((fRec0[0] * ((0.0344887897f * fTemp45) + (fRec629 + (fRec632 + (fRec644 + fRec650))))));
			float fTemp326 = (fConst134 * fRec658[1]);
			float fTemp327 = (fConst135 * fRec661[1]);
			float fTemp328 = (fConst137 * (((0.009207814f * fTemp19) + (0.011788087f * fTemp17)) - ((0.026046088f * fTemp22) + ((((((0.0248568896f * fTemp14) + (0.0453077331f * fTemp18)) + (0.0125348074f * fTemp15)) + (0.00785371754f * fTemp16)) + (0.0136526348f * fTemp20)) + (0.00287043699f * fTemp21)))));
			float fTemp329 = (fConst138 * fRec664[1]);
			float fTemp330 = (fConst139 * fRec667[1]);
			fRec669[0] = (fTemp328 + (fTemp329 + (fRec669[1] + fTemp330)));
			fRec667[0] = fRec669[0];
			float fRec668 = ((fTemp330 + fTemp329) + fTemp328);
			fRec666[0] = (fRec667[0] + fRec666[1]);
			fRec664[0] = fRec666[0];
			float fRec665 = fRec668;
			fRec663[0] = (fTemp326 + (fTemp327 + (fRec665 + fRec663[1])));
			fRec661[0] = fRec663[0];
			float fRec662 = (fTemp326 + (fRec665 + fTemp327));
			fRec660[0] = (fRec661[0] + fRec660[1]);
			fRec658[0] = fRec660[0];
			float fRec659 = fRec662;
			float fTemp331 = (fConst145 * fRec670[1]);
			float fTemp332 = (fConst147 * (((0.00239910092f * fTemp40) + ((0.0450634062f * fTemp37) + ((0.0432547703f * fTemp39) + ((0.0226365346f * fTemp34) + (0.00831752829f * fTemp38))))) - ((0.00851497054f * fTemp35) + (0.0239610206f * fTemp36))));
			float fTemp333 = (fConst148 * fRec673[1]);
			float fTemp334 = (fConst149 * fRec676[1]);
			fRec678[0] = (fTemp332 + (fTemp333 + (fRec678[1] + fTemp334)));
			fRec676[0] = fRec678[0];
			float fRec677 = ((fTemp334 + fTemp333) + fTemp332);
			fRec675[0] = (fRec676[0] + fRec675[1]);
			fRec673[0] = fRec675[0];
			float fRec674 = fRec677;
			fRec672[0] = (fTemp331 + (fRec674 + fRec672[1]));
			fRec670[0] = fRec672[0];
			float fRec671 = (fRec674 + fTemp331);
			float fTemp335 = (fConst131 * ((0.0308367889f * fTemp9) - ((0.0325823613f * fTemp7) + (0.047908619f * fTemp8))));
			float fTemp336 = (fConst132 * fRec679[1]);
			fRec681[0] = (fTemp335 + (fRec681[1] + fTemp336));
			fRec679[0] = fRec681[0];
			float fRec680 = (fTemp336 + fTemp335);
			float fTemp337 = (fConst141 * (((0.0452136435f * fTemp28) + (0.0206034295f * fTemp29)) - (((0.0471394435f * fTemp26) + (0.0269582253f * fTemp30)) + (0.00680943299f * fTemp27))));
			float fTemp338 = (fConst142 * fRec682[1]);
			float fTemp339 = (fConst143 * fRec685[1]);
			fRec687[0] = (fTemp337 + (fTemp338 + (fRec687[1] + fTemp339)));
			fRec685[0] = fRec687[0];
			float fRec686 = ((fTemp339 + fTemp338) + fTemp337);
			fRec684[0] = (fRec685[0] + fRec684[1]);
			fRec682[0] = fRec684[0];
			float fRec683 = fRec686;
			output21[i] = FAUSTFLOAT((fRec0[0] * ((0.0406066403f * fTemp45) + (fRec659 + (fRec671 + (fRec680 + fRec683))))));
			float fTemp340 = (fConst110 * fRec688[1]);
			float fTemp341 = (fConst111 * fRec691[1]);
			float fTemp342 = (fConst113 * (((0.0400748141f * fTemp22) + ((0.0477536209f * fTemp21) + (((4.40240001e-06f * fTemp14) + (2.36560004e-06f * fTemp19)) + (0.00998531654f * fTemp17)))) - ((((1.58520004e-06f * fTemp18) + (2.50020003e-06f * fTemp15)) + (0.00279862061f * fTemp16)) + (0.0203480348f * fTemp20))));
			float fTemp343 = (fConst114 * fRec694[1]);
			float fTemp344 = (fConst115 * fRec697[1]);
			fRec699[0] = (fTemp342 + (fTemp343 + (fRec699[1] + fTemp344)));
			fRec697[0] = fRec699[0];
			float fRec698 = ((fTemp344 + fTemp343) + fTemp342);
			fRec696[0] = (fRec697[0] + fRec696[1]);
			fRec694[0] = fRec696[0];
			float fRec695 = fRec698;
			fRec693[0] = (fTemp340 + (fTemp341 + (fRec695 + fRec693[1])));
			fRec691[0] = fRec693[0];
			float fRec692 = (fTemp340 + (fRec695 + fTemp341));
			fRec690[0] = (fRec691[0] + fRec690[1]);
			fRec688[0] = fRec690[0];
			float fRec689 = fRec692;
			float fTemp345 = (fConst117 * fRec700[1]);
			float fTemp346 = (fConst119 * (((1.21499994e-07f * fTemp40) + ((7.28500027e-07f * fTemp37) + (0.0250976384f * fTemp34))) - ((3.07470009e-06f * fTemp39) + ((0.0512235425f * fTemp38) + ((0.000566295581f * fTemp35) + (0.0537225828f * fTemp36))))));
			float fTemp347 = (fConst120 * fRec703[1]);
			float fTemp348 = (fConst121 * fRec706[1]);
			fRec708[0] = (fTemp346 + (fTemp347 + (fRec708[1] + fTemp348)));
			fRec706[0] = fRec708[0];
			float fRec707 = ((fTemp348 + fTemp347) + fTemp346);
			fRec705[0] = (fRec706[0] + fRec705[1]);
			fRec703[0] = fRec705[0];
			float fRec704 = fRec707;
			fRec702[0] = (fTemp345 + (fRec704 + fRec702[1]));
			fRec700[0] = fRec702[0];
			float fRec701 = (fRec704 + fTemp345);
			float fTemp349 = (fConst123 * (0.0f - (((9.95899995e-07f * fTemp9) + (0.0337603986f * fTemp7)) + (0.0667955354f * fTemp8))));
			float fTemp350 = (fConst124 * fRec709[1]);
			fRec711[0] = (fTemp349 + (fRec711[1] + fTemp350));
			fRec709[0] = fRec711[0];
			float fRec710 = (fTemp350 + fTemp349);
			float fTemp351 = (fConst126 * (((((1.849e-06f * fTemp26) + (2.55600014e-07f * fTemp30)) + (0.0550848991f * fTemp28)) + (0.0606719367f * fTemp29)) - (0.0130111389f * fTemp27)));
			float fTemp352 = (fConst127 * fRec712[1]);
			float fTemp353 = (fConst128 * fRec715[1]);
			fRec717[0] = (fTemp351 + (fTemp352 + (fRec717[1] + fTemp353)));
			fRec715[0] = fRec717[0];
			float fRec716 = ((fTemp353 + fTemp352) + fTemp351);
			fRec714[0] = (fRec715[0] + fRec714[1]);
			fRec712[0] = fRec714[0];
			float fRec713 = fRec716;
			fVec20[0] = ((0.0466445908f * fTemp45) + (fRec689 + (fRec701 + (fRec710 + fRec713))));
			output22[i] = FAUSTFLOAT((0.999793351f * (fRec0[0] * fVec20[iConst129])));
			float fTemp354 = (fConst134 * fRec718[1]);
			float fTemp355 = (fConst135 * fRec721[1]);
			float fTemp356 = (fConst137 * (((3.61189996e-06f * fTemp21) + ((((0.0251501054f * fTemp14) + (0.0428467207f * fTemp18)) + (0.0203895811f * fTemp15)) + (0.0117731281f * fTemp17))) - ((((0.0062495796f * fTemp19) + (0.0129695972f * fTemp16)) + (0.0108326115f * fTemp20)) + (0.0145213213f * fTemp22))));
			float fTemp357 = (fConst138 * fRec724[1]);
			float fTemp358 = (fConst139 * fRec727[1]);
			fRec729[0] = (fTemp356 + (fTemp357 + (fRec729[1] + fTemp358)));
			fRec727[0] = fRec729[0];
			float fRec728 = ((fTemp358 + fTemp357) + fTemp356);
			fRec726[0] = (fRec727[0] + fRec726[1]);
			fRec724[0] = fRec726[0];
			float fRec725 = fRec728;
			fRec723[0] = (fTemp354 + (fTemp355 + (fRec725 + fRec723[1])));
			fRec721[0] = fRec723[0];
			float fRec722 = (fTemp354 + (fRec725 + fTemp355));
			fRec720[0] = (fRec721[0] + fRec720[1]);
			fRec718[0] = fRec720[0];
			float fRec719 = fRec722;
			float fTemp359 = (fConst145 * fRec730[1]);
			float fTemp360 = (fConst147 * ((0.0201849788f * fTemp34) - ((0.00831092708f * fTemp40) + ((0.0417956226f * fTemp37) + ((0.0352582783f * fTemp39) + ((6.92699984e-07f * fTemp38) + ((0.0143922055f * fTemp35) + (0.0241336562f * fTemp36))))))));
			float fTemp361 = (fConst148 * fRec733[1]);
			float fTemp362 = (fConst149 * fRec736[1]);
			fRec738[0] = (fTemp360 + (fTemp361 + (fRec738[1] + fTemp362)));
			fRec736[0] = fRec738[0];
			float fRec737 = ((fTemp362 + fTemp361) + fTemp360);
			fRec735[0] = (fRec736[0] + fRec735[1]);
			fRec733[0] = fRec735[0];
			float fRec734 = fRec737;
			fRec732[0] = (fTemp359 + (fRec734 + fRec732[1]));
			fRec730[0] = fRec732[0];
			float fRec731 = (fRec734 + fTemp359);
			float fTemp363 = (fConst131 * (0.0f - (((0.0234640706f * fTemp9) + (0.0313628912f * fTemp7)) + (0.0406417958f * fTemp8))));
			float fTemp364 = (fConst132 * fRec739[1]);
			fRec741[0] = (fTemp363 + (fRec741[1] + fTemp364));
			fRec739[0] = fRec741[0];
			float fRec740 = (fTemp364 + fTemp363);
			float fTemp365 = (fConst141 * (((((0.0359500684f * fTemp26) + (0.0250729267f * fTemp30)) + (0.0434282646f * fTemp28)) + (0.0207570828f * fTemp29)) - (0.000587342074f * fTemp27)));
			float fTemp366 = (fConst142 * fRec742[1]);
			float fTemp367 = (fConst143 * fRec745[1]);
			fRec747[0] = (fTemp365 + (fTemp366 + (fRec747[1] + fTemp367)));
			fRec745[0] = fRec747[0];
			float fRec746 = ((fTemp367 + fTemp366) + fTemp365);
			fRec744[0] = (fRec745[0] + fRec744[1]);
			fRec742[0] = fRec744[0];
			float fRec743 = fRec746;
			output23[i] = FAUSTFLOAT((fRec0[0] * ((0.034495037f * fTemp45) + (fRec719 + (fRec731 + (fRec740 + fRec743))))));
			float fTemp368 = (fConst145 * fRec748[1]);
			float fTemp369 = (fConst147 * (((0.0432581492f * fTemp38) + ((0.0226408653f * fTemp34) + (0.0239630714f * fTemp36))) - ((0.00850997958f * fTemp40) + ((0.0450532325f * fTemp37) + ((0.00829337724f * fTemp39) + (0.0023992199f * fTemp35))))));
			float fTemp370 = (fConst148 * fRec751[1]);
			float fTemp371 = (fConst149 * fRec754[1]);
			fRec756[0] = (fTemp369 + (fTemp370 + (fRec756[1] + fTemp371)));
			fRec754[0] = fRec756[0];
			float fRec755 = ((fTemp371 + fTemp370) + fTemp369);
			fRec753[0] = (fRec754[0] + fRec753[1]);
			fRec751[0] = fRec753[0];
			float fRec752 = fRec755;
			fRec750[0] = (fTemp368 + (fRec752 + fRec750[1]));
			fRec748[0] = fRec750[0];
			float fRec749 = (fRec752 + fTemp368);
			float fTemp372 = (fConst141 * ((((0.0471209586f * fTemp26) + (0.0452032685f * fTemp30)) + (0.0269464068f * fTemp28)) - ((0.00681215944f * fTemp27) + (0.0206162035f * fTemp29))));
			float fTemp373 = (fConst142 * fRec757[1]);
			float fTemp374 = (fConst143 * fRec760[1]);
			fRec762[0] = (fTemp372 + (fTemp373 + (fRec762[1] + fTemp374)));
			fRec760[0] = fRec762[0];
			float fRec761 = ((fTemp374 + fTemp373) + fTemp372);
			fRec759[0] = (fRec760[0] + fRec759[1]);
			fRec757[0] = fRec759[0];
			float fRec758 = fRec761;
			float fTemp375 = (fConst139 * fRec772[1]);
			float fTemp376 = (fConst138 * fRec769[1]);
			float fTemp377 = (fConst137 * (((0.00286312122f * fTemp18) + (0.0125351297f * fTemp15)) - ((0.0260353144f * fTemp22) + ((0.0453069843f * fTemp21) + ((0.0117803868f * fTemp17) + ((((0.0248816088f * fTemp14) + (0.0136577887f * fTemp19)) + (0.00786233228f * fTemp16)) + (0.00921548437f * fTemp20)))))));
			fRec774[0] = (((fRec774[1] + fTemp375) + fTemp376) + fTemp377);
			fRec772[0] = fRec774[0];
			float fRec773 = ((fTemp375 + fTemp376) + fTemp377);
			fRec771[0] = (fRec772[0] + fRec771[1]);
			fRec769[0] = fRec771[0];
			float fRec770 = fRec773;
			float fTemp378 = (fConst135 * fRec766[1]);
			float fTemp379 = (fConst134 * fRec763[1]);
			fRec768[0] = (((fRec770 + fRec768[1]) + fTemp378) + fTemp379);
			fRec766[0] = fRec768[0];
			float fRec767 = ((fRec770 + fTemp378) + fTemp379);
			fRec765[0] = (fRec766[0] + fRec765[1]);
			fRec763[0] = fRec765[0];
			float fRec764 = fRec767;
			float fTemp380 = (fConst131 * (0.0f - (((0.0478971489f * fTemp9) + (0.0325675346f * fTemp7)) + (0.0308167655f * fTemp8))));
			float fTemp381 = (fConst132 * fRec775[1]);
			fRec777[0] = (fTemp380 + (fRec777[1] + fTemp381));
			fRec775[0] = fRec777[0];
			float fRec776 = (fTemp381 + fTemp380);
			output24[i] = FAUSTFLOAT((fRec0[0] * ((fRec749 + (fRec758 + (fRec764 + fRec776))) + (0.0405882597f * fTemp45))));
			float fTemp382 = (fConst110 * fRec778[1]);
			float fTemp383 = (fConst111 * fRec781[1]);
			float fTemp384 = (fConst113 * (((1.85119995e-06f * fTemp20) + (0.040046867f * fTemp22)) - ((5.11999986e-07f * fTemp21) + ((((((8.20379955e-06f * fTemp14) + (0.0477588959f * fTemp18)) + (1.82190001e-06f * fTemp15)) + (0.0203590505f * fTemp19)) + (0.00281510339f * fTemp16)) + (0.00999389309f * fTemp17)))));
			float fTemp385 = (fConst114 * fRec784[1]);
			float fTemp386 = (fConst115 * fRec787[1]);
			fRec789[0] = (fTemp384 + (fTemp385 + (fRec789[1] + fTemp386)));
			fRec787[0] = fRec789[0];
			float fRec788 = ((fTemp386 + fTemp385) + fTemp384);
			fRec786[0] = (fRec787[0] + fRec786[1]);
			fRec784[0] = fRec786[0];
			float fRec785 = fRec788;
			fRec783[0] = (fTemp382 + (fTemp383 + (fRec785 + fRec783[1])));
			fRec781[0] = fRec783[0];
			float fRec782 = (fTemp382 + (fRec785 + fTemp383));
			fRec780[0] = (fRec781[0] + fRec780[1]);
			fRec778[0] = fRec780[0];
			float fRec779 = fRec782;
			float fTemp387 = (fConst117 * fRec790[1]);
			float fTemp388 = (fConst119 * (((0.0512015373f * fTemp39) + ((7.54509983e-06f * fTemp38) + ((0.0537262298f * fTemp36) + ((0.0251128487f * fTemp34) + (3.62999998e-07f * fTemp35))))) - ((7.6409998e-07f * fTemp37) + (0.000570559001f * fTemp40))));
			float fTemp389 = (fConst120 * fRec793[1]);
			float fTemp390 = (fConst121 * fRec796[1]);
			fRec798[0] = (fTemp388 + (fTemp389 + (fRec798[1] + fTemp390)));
			fRec796[0] = fRec798[0];
			float fRec797 = ((fTemp390 + fTemp389) + fTemp388);
			fRec795[0] = (fRec796[0] + fRec795[1]);
			fRec793[0] = fRec795[0];
			float fRec794 = fRec797;
			fRec792[0] = (fTemp387 + (fRec794 + fRec792[1]));
			fRec790[0] = fRec792[0];
			float fRec791 = (fRec794 + fTemp387);
			float fTemp391 = (fConst123 * (0.0f - (((0.0667844266f * fTemp9) + (0.0337536409f * fTemp7)) + (3.20599997e-06f * fTemp8))));
			float fTemp392 = (fConst124 * fRec799[1]);
			fRec801[0] = (fTemp391 + (fRec801[1] + fTemp392));
			fRec799[0] = fRec801[0];
			float fRec800 = (fTemp392 + fTemp391);
			float fTemp393 = (fConst126 * ((((5.71520013e-06f * fTemp26) + (0.055085998f * fTemp30)) + (1.07580001e-06f * fTemp28)) - ((0.0130164139f * fTemp27) + (0.0606563874f * fTemp29))));
			float fTemp394 = (fConst127 * fRec802[1]);
			float fTemp395 = (fConst128 * fRec805[1]);
			fRec807[0] = (fTemp393 + (fTemp394 + (fRec807[1] + fTemp395)));
			fRec805[0] = fRec807[0];
			float fRec806 = ((fTemp395 + fTemp394) + fTemp393);
			fRec804[0] = (fRec805[0] + fRec804[1]);
			fRec802[0] = fRec804[0];
			float fRec803 = fRec806;
			fVec21[0] = ((0.0466346815f * fTemp45) + (fRec779 + (fRec791 + (fRec800 + fRec803))));
			output25[i] = FAUSTFLOAT((0.999793351f * (fRec0[0] * fVec21[iConst129])));
			float fTemp396 = (fConst137 * (((((0.0248633679f * fTemp14) + (0.00285660941f * fTemp18)) + (0.00921179168f * fTemp20)) + (0.0453070663f * fTemp21)) - ((0.0260481983f * fTemp22) + ((((0.0125271007f * fTemp15) + (0.0136521002f * fTemp19)) + (0.00784836058f * fTemp16)) + (0.01179442f * fTemp17)))));
			float fTemp397 = (fConst139 * fRec817[1]);
			float fTemp398 = (fConst138 * fRec814[1]);
			fRec819[0] = (((fTemp396 + fRec819[1]) + fTemp397) + fTemp398);
			fRec817[0] = fRec819[0];
			float fRec818 = ((fTemp396 + fTemp397) + fTemp398);
			fRec816[0] = (fRec817[0] + fRec816[1]);
			fRec814[0] = fRec816[0];
			float fRec815 = fRec818;
			float fTemp399 = (fConst135 * fRec811[1]);
			float fTemp400 = (fConst134 * fRec808[1]);
			fRec813[0] = (((fRec815 + fRec813[1]) + fTemp399) + fTemp400);
			fRec811[0] = fRec813[0];
			float fRec812 = ((fRec815 + fTemp399) + fTemp400);
			fRec810[0] = (fRec811[0] + fRec810[1]);
			fRec808[0] = fRec810[0];
			float fRec809 = fRec812;
			float fTemp401 = (fConst145 * fRec820[1]);
			float fTemp402 = (fConst147 * (((0.0450488664f * fTemp37) + ((0.0239670761f * fTemp36) + ((0.0226290841f * fTemp34) + (0.00239059958f * fTemp35)))) - ((0.00851257704f * fTemp40) + ((0.00831089541f * fTemp39) + (0.0432544388f * fTemp38)))));
			float fTemp403 = (fConst148 * fRec823[1]);
			float fTemp404 = (fConst149 * fRec826[1]);
			fRec828[0] = (fTemp402 + (fTemp403 + (fRec828[1] + fTemp404)));
			fRec826[0] = fRec828[0];
			float fRec827 = ((fTemp404 + fTemp403) + fTemp402);
			fRec825[0] = (fRec826[0] + fRec825[1]);
			fRec823[0] = fRec825[0];
			float fRec824 = fRec827;
			fRec822[0] = (fTemp401 + (fRec824 + fRec822[1]));
			fRec820[0] = fRec822[0];
			float fRec821 = (fRec824 + fTemp401);
			float fTemp405 = (fConst131 * ((0.0308232103f * fTemp8) - ((0.0478976816f * fTemp9) + (0.0325714648f * fTemp7))));
			float fTemp406 = (fConst132 * fRec829[1]);
			fRec831[0] = (fTemp405 + (fRec831[1] + fTemp406));
			fRec829[0] = fRec831[0];
			float fRec830 = (fTemp406 + fTemp405);
			float fTemp407 = (fConst141 * ((0.0452032611f * fTemp30) - ((((0.0471277423f * fTemp26) + (0.00680800574f * fTemp27)) + (0.0269425102f * fTemp28)) + (0.0206062663f * fTemp29))));
			float fTemp408 = (fConst142 * fRec832[1]);
			float fTemp409 = (fConst143 * fRec835[1]);
			fRec837[0] = (fTemp407 + (fTemp408 + (fRec837[1] + fTemp409)));
			fRec835[0] = fRec837[0];
			float fRec836 = ((fTemp409 + fTemp408) + fTemp407);
			fRec834[0] = (fRec835[0] + fRec834[1]);
			fRec832[0] = fRec834[0];
			float fRec833 = fRec836;
			output26[i] = FAUSTFLOAT((fRec0[0] * ((fRec809 + (fRec821 + (fRec830 + fRec833))) + (0.0405937769f * fTemp45))));
			float fTemp410 = (fConst134 * fRec838[1]);
			float fTemp411 = (fConst135 * fRec841[1]);
			float fTemp412 = (fConst137 * (((0.0117666787f * fTemp17) + ((0.0428507105f * fTemp18) + (0.0108253248f * fTemp20))) - ((0.0145012243f * fTemp22) + (((((0.0251691379f * fTemp14) + (0.0203875322f * fTemp15)) + (0.00624662498f * fTemp19)) + (0.0129594402f * fTemp16)) + (1.57025006e-05f * fTemp21)))));
			float fTemp413 = (fConst138 * fRec844[1]);
			float fTemp414 = (fConst139 * fRec847[1]);
			fRec849[0] = (fTemp412 + (fTemp413 + (fRec849[1] + fTemp414)));
			fRec847[0] = fRec849[0];
			float fRec848 = ((fTemp414 + fTemp413) + fTemp412);
			fRec846[0] = (fRec847[0] + fRec846[1]);
			fRec844[0] = fRec846[0];
			float fRec845 = fRec848;
			fRec843[0] = (fTemp410 + (fTemp411 + (fRec845 + fRec843[1])));
			fRec841[0] = fRec843[0];
			float fRec842 = (fTemp410 + (fRec845 + fTemp411));
			fRec840[0] = (fRec841[0] + fRec840[1]);
			fRec838[0] = fRec840[0];
			float fRec839 = fRec842;
			float fTemp415 = (fConst145 * fRec850[1]);
			float fTemp416 = (fConst147 * (((0.0417933054f * fTemp37) + (((0.0201795623f * fTemp34) + (0.0143922288f * fTemp35)) + (2.27710007e-05f * fTemp38))) - ((0.00831146073f * fTemp40) + ((0.0352645926f * fTemp39) + (0.0241428446f * fTemp36)))));
			float fTemp417 = (fConst148 * fRec853[1]);
			float fTemp418 = (fConst149 * fRec856[1]);
			fRec858[0] = (fTemp416 + (fTemp417 + (fRec858[1] + fTemp418)));
			fRec856[0] = fRec858[0];
			float fRec857 = ((fTemp418 + fTemp417) + fTemp416);
			fRec855[0] = (fRec856[0] + fRec855[1]);
			fRec853[0] = fRec855[0];
			float fRec854 = fRec857;
			fRec852[0] = (fTemp415 + (fRec854 + fRec852[1]));
			fRec850[0] = fRec852[0];
			float fRec851 = (fRec854 + fTemp415);
			float fTemp419 = (fConst131 * ((0.040651273f * fTemp8) - ((0.0234596319f * fTemp9) + (0.0313677862f * fTemp7))));
			float fTemp420 = (fConst132 * fRec859[1]);
			fRec861[0] = (fTemp419 + (fRec861[1] + fTemp420));
			fRec859[0] = fRec861[0];
			float fRec860 = (fTemp420 + fTemp419);
			float fTemp421 = (fConst141 * (((0.0250699241f * fTemp30) + (0.0207735673f * fTemp29)) - (((0.0359471589f * fTemp26) + (0.000585046422f * fTemp27)) + (0.0434349068f * fTemp28))));
			float fTemp422 = (fConst142 * fRec862[1]);
			float fTemp423 = (fConst143 * fRec865[1]);
			fRec867[0] = (fTemp421 + (fTemp422 + (fRec867[1] + fTemp423)));
			fRec865[0] = fRec867[0];
			float fRec866 = ((fTemp423 + fTemp422) + fTemp421);
			fRec864[0] = (fRec865[0] + fRec864[1]);
			fRec862[0] = fRec864[0];
			float fRec863 = fRec866;
			output27[i] = FAUSTFLOAT((fRec0[0] * ((0.0345001929f * fTemp45) + (fRec839 + (fRec851 + (fRec860 + fRec863))))));
			float fTemp424 = (fConst110 * fRec868[1]);
			float fTemp425 = (fConst111 * fRec871[1]);
			float fTemp426 = (fConst113 * ((((0.0099847177f * fTemp17) + ((1.65057008e-05f * fTemp14) + (0.0203569382f * fTemp20))) + (0.0400542766f * fTemp22)) - (((((3.80749998e-06f * fTemp18) + (1.98699993e-07f * fTemp15)) + (2.92949994e-06f * fTemp19)) + (0.0028100044f * fTemp16)) + (0.0477563478f * fTemp21))));
			float fTemp427 = (fConst114 * fRec874[1]);
			float fTemp428 = (fConst115 * fRec877[1]);
			fRec879[0] = (fTemp426 + (fTemp427 + (fRec879[1] + fTemp428)));
			fRec877[0] = fRec879[0];
			float fRec878 = ((fTemp428 + fTemp427) + fTemp426);
			fRec876[0] = (fRec877[0] + fRec876[1]);
			fRec874[0] = fRec876[0];
			float fRec875 = fRec878;
			fRec873[0] = (fTemp424 + (fTemp425 + (fRec875 + fRec873[1])));
			fRec871[0] = fRec873[0];
			float fRec872 = (fTemp424 + (fRec875 + fTemp425));
			fRec870[0] = (fRec871[0] + fRec870[1]);
			fRec868[0] = fRec870[0];
			float fRec869 = fRec872;
			float fTemp429 = (fConst117 * fRec880[1]);
			float fTemp430 = (fConst119 * (((1.68932002e-05f * fTemp39) + (((0.0251081437f * fTemp34) + (0.000557872816f * fTemp35)) + (0.0512077697f * fTemp38))) - ((9.43600014e-07f * fTemp40) + ((4.47489992e-06f * fTemp37) + (0.0537186339f * fTemp36)))));
			float fTemp431 = (fConst120 * fRec883[1]);
			float fTemp432 = (fConst121 * fRec886[1]);
			fRec888[0] = (fTemp430 + (fTemp431 + (fRec888[1] + fTemp432)));
			fRec886[0] = fRec888[0];
			float fRec887 = ((fTemp432 + fTemp431) + fTemp430);
			fRec885[0] = (fRec886[0] + fRec885[1]);
			fRec883[0] = fRec885[0];
			float fRec884 = fRec887;
			fRec882[0] = (fTemp429 + (fRec884 + fRec882[1]));
			fRec880[0] = fRec882[0];
			float fRec881 = (fRec884 + fTemp429);
			float fTemp433 = (fConst123 * (((7.68480004e-06f * fTemp9) + (0.0667851865f * fTemp8)) - (0.0337466374f * fTemp7)));
			float fTemp434 = (fConst124 * fRec889[1]);
			fRec891[0] = (fTemp433 + (fRec891[1] + fTemp434));
			fRec889[0] = fRec891[0];
			float fRec890 = (fTemp434 + fTemp433);
			float fTemp435 = (fConst126 * (((1.38392998e-05f * fTemp26) + (0.0606603697f * fTemp29)) - (((3.01280011e-06f * fTemp30) + (0.0130227739f * fTemp27)) + (0.0550744645f * fTemp28))));
			float fTemp436 = (fConst127 * fRec892[1]);
			float fTemp437 = (fConst128 * fRec895[1]);
			fRec897[0] = (fTemp435 + (fTemp436 + (fRec897[1] + fTemp437)));
			fRec895[0] = fRec897[0];
			float fRec896 = ((fTemp437 + fTemp436) + fTemp435);
			fRec894[0] = (fRec895[0] + fRec894[1]);
			fRec892[0] = fRec894[0];
			float fRec893 = fRec896;
			fVec22[0] = ((0.0466337539f * fTemp45) + (fRec869 + (fRec881 + (fRec890 + fRec893))));
			output28[i] = FAUSTFLOAT((0.999793351f * (fRec0[0] * fVec22[iConst129])));
			float fTemp438 = (fConst134 * fRec898[1]);
			float fTemp439 = (fConst135 * fRec901[1]);
			float fTemp440 = (fConst137 * (((0.0117736124f * fTemp17) + ((((0.025155291f * fTemp14) + (0.0203920864f * fTemp15)) + (0.00623980444f * fTemp19)) + (0.0108105885f * fTemp20))) - ((0.0145004606f * fTemp22) + (((0.0428427048f * fTemp18) + (0.0129428785f * fTemp16)) + (1.37705001e-05f * fTemp21)))));
			float fTemp441 = (fConst138 * fRec904[1]);
			float fTemp442 = (fConst139 * fRec907[1]);
			fRec909[0] = (fTemp440 + (fTemp441 + (fRec909[1] + fTemp442)));
			fRec907[0] = fRec909[0];
			float fRec908 = ((fTemp442 + fTemp441) + fTemp440);
			fRec906[0] = (fRec907[0] + fRec906[1]);
			fRec904[0] = fRec906[0];
			float fRec905 = fRec908;
			fRec903[0] = (fTemp438 + (fTemp439 + (fRec905 + fRec903[1])));
			fRec901[0] = fRec903[0];
			float fRec902 = (fTemp438 + (fRec905 + fTemp439));
			fRec900[0] = (fRec901[0] + fRec900[1]);
			fRec898[0] = fRec900[0];
			float fRec899 = fRec902;
			float fTemp443 = (fConst145 * fRec910[1]);
			float fTemp444 = (fConst147 * (((0.00831724796f * fTemp40) + ((0.0352444984f * fTemp39) + (((0.0201503728f * fTemp34) + (0.0144056249f * fTemp35)) + (1.74662e-05f * fTemp38)))) - ((0.0417816304f * fTemp37) + (0.0241350662f * fTemp36))));
			float fTemp445 = (fConst148 * fRec913[1]);
			float fTemp446 = (fConst149 * fRec916[1]);
			fRec918[0] = (fTemp444 + (fTemp445 + (fRec918[1] + fTemp446)));
			fRec916[0] = fRec918[0];
			float fRec917 = ((fTemp446 + fTemp445) + fTemp444);
			fRec915[0] = (fRec916[0] + fRec915[1]);
			fRec913[0] = fRec915[0];
			float fRec914 = fRec917;
			fRec912[0] = (fTemp443 + (fRec914 + fRec912[1]));
			fRec910[0] = fRec912[0];
			float fRec911 = (fRec914 + fTemp443);
			float fTemp447 = (fConst131 * (((0.0234466158f * fTemp9) + (0.0406251438f * fTemp8)) - (0.031370163f * fTemp7)));
			float fTemp448 = (fConst132 * fRec919[1]);
			fRec921[0] = (fTemp447 + (fRec921[1] + fTemp448));
			fRec919[0] = fRec921[0];
			float fRec920 = (fTemp448 + fTemp447);
			float fTemp449 = (fConst141 * (((0.0359259993f * fTemp26) + (0.0207571592f * fTemp29)) - (((0.0250631403f * fTemp30) + (0.000557037478f * fTemp27)) + (0.0434222557f * fTemp28))));
			float fTemp450 = (fConst142 * fRec922[1]);
			float fTemp451 = (fConst143 * fRec925[1]);
			fRec927[0] = (fTemp449 + (fTemp450 + (fRec927[1] + fTemp451)));
			fRec925[0] = fRec927[0];
			float fRec926 = ((fTemp451 + fTemp450) + fTemp449);
			fRec924[0] = (fRec925[0] + fRec924[1]);
			fRec922[0] = fRec924[0];
			float fRec923 = fRec926;
			output29[i] = FAUSTFLOAT((fRec0[0] * ((0.0344871767f * fTemp45) + (fRec899 + (fRec911 + (fRec920 + fRec923))))));
			float fTemp452 = (fConst134 * fRec928[1]);
			float fTemp453 = (fConst135 * fRec931[1]);
			float fTemp454 = (fConst137 * (((((0.0125401132f * fTemp15) + (0.0136542032f * fTemp19)) + (0.00921658147f * fTemp20)) + (0.0453105569f * fTemp21)) - (((((0.0248650871f * fTemp14) + (0.00286275242f * fTemp18)) + (0.00787734985f * fTemp16)) + (0.0117901973f * fTemp17)) + (0.026033219f * fTemp22))));
			float fTemp455 = (fConst138 * fRec934[1]);
			float fTemp456 = (fConst139 * fRec937[1]);
			fRec939[0] = (fTemp454 + (fTemp455 + (fRec939[1] + fTemp456)));
			fRec937[0] = fRec939[0];
			float fRec938 = ((fTemp456 + fTemp455) + fTemp454);
			fRec936[0] = (fRec937[0] + fRec936[1]);
			fRec934[0] = fRec936[0];
			float fRec935 = fRec938;
			fRec933[0] = (fTemp452 + (fTemp453 + (fRec935 + fRec933[1])));
			fRec931[0] = fRec933[0];
			float fRec932 = (fTemp452 + (fRec935 + fTemp453));
			fRec930[0] = (fRec931[0] + fRec930[1]);
			fRec928[0] = fRec930[0];
			float fRec929 = fRec932;
			float fTemp457 = (fConst145 * fRec940[1]);
			float fTemp458 = (fConst147 * (((0.00851999596f * fTemp40) + ((0.00830107834f * fTemp39) + ((0.0239660088f * fTemp36) + ((0.0226506926f * fTemp34) + (0.00239855843f * fTemp35))))) - ((0.0450569987f * fTemp37) + (0.0432474576f * fTemp38))));
			float fTemp459 = (fConst148 * fRec943[1]);
			float fTemp460 = (fConst149 * fRec946[1]);
			fRec948[0] = (fTemp458 + (fTemp459 + (fRec948[1] + fTemp460)));
			fRec946[0] = fRec948[0];
			float fRec947 = ((fTemp460 + fTemp459) + fTemp458);
			fRec945[0] = (fRec946[0] + fRec945[1]);
			fRec943[0] = fRec945[0];
			float fRec944 = fRec947;
			fRec942[0] = (fTemp457 + (fRec944 + fRec942[1]));
			fRec940[0] = fRec942[0];
			float fRec941 = (fRec944 + fTemp457);
			float fTemp461 = (fConst131 * (((0.0478948392f * fTemp9) + (0.0308185238f * fTemp8)) - (0.0325662233f * fTemp7)));
			float fTemp462 = (fConst132 * fRec949[1]);
			fRec951[0] = (fTemp461 + (fRec951[1] + fTemp462));
			fRec949[0] = fRec951[0];
			float fRec950 = (fTemp462 + fTemp461);
			float fTemp463 = (fConst141 * ((0.0471193865f * fTemp26) - ((((0.0452092178f * fTemp30) + (0.006815203f * fTemp27)) + (0.0269478485f * fTemp28)) + (0.0206080098f * fTemp29))));
			float fTemp464 = (fConst142 * fRec952[1]);
			float fTemp465 = (fConst143 * fRec955[1]);
			fRec957[0] = (fTemp463 + (fTemp464 + (fRec957[1] + fTemp465)));
			fRec955[0] = fRec957[0];
			float fRec956 = ((fTemp465 + fTemp464) + fTemp463);
			fRec954[0] = (fRec955[0] + fRec954[1]);
			fRec952[0] = fRec954[0];
			float fRec953 = fRec956;
			output30[i] = FAUSTFLOAT((fRec0[0] * ((0.0405861326f * fTemp45) + (fRec929 + (fRec941 + (fRec950 + fRec953))))));
			fRec0[1] = fRec0[0];
			fRec4[1] = fRec4[0];
			fRec5[2] = fRec5[1];
			fRec5[1] = fRec5[0];
			fRec6[1] = fRec6[0];
			fRec7[2] = fRec7[1];
			fRec7[1] = fRec7[0];
			fRec8[2] = fRec8[1];
			fRec8[1] = fRec8[0];
			fRec3[1] = fRec3[0];
			fRec1[1] = fRec1[0];
			fRec21[2] = fRec21[1];
			fRec21[1] = fRec21[0];
			fRec22[2] = fRec22[1];
			fRec22[1] = fRec22[0];
			fRec23[2] = fRec23[1];
			fRec23[1] = fRec23[0];
			fRec24[2] = fRec24[1];
			fRec24[1] = fRec24[0];
			fRec25[2] = fRec25[1];
			fRec25[1] = fRec25[0];
			fRec26[2] = fRec26[1];
			fRec26[1] = fRec26[0];
			fRec27[2] = fRec27[1];
			fRec27[1] = fRec27[0];
			fRec28[2] = fRec28[1];
			fRec28[1] = fRec28[0];
			fRec29[2] = fRec29[1];
			fRec29[1] = fRec29[0];
			fRec20[1] = fRec20[0];
			fRec18[1] = fRec18[0];
			fRec17[1] = fRec17[0];
			fRec15[1] = fRec15[0];
			fRec14[1] = fRec14[0];
			fRec12[1] = fRec12[0];
			fRec11[1] = fRec11[0];
			fRec9[1] = fRec9[0];
			fRec36[2] = fRec36[1];
			fRec36[1] = fRec36[0];
			fRec37[2] = fRec37[1];
			fRec37[1] = fRec37[0];
			fRec38[2] = fRec38[1];
			fRec38[1] = fRec38[0];
			fRec39[2] = fRec39[1];
			fRec39[1] = fRec39[0];
			fRec40[2] = fRec40[1];
			fRec40[1] = fRec40[0];
			fRec35[1] = fRec35[0];
			fRec33[1] = fRec33[0];
			fRec32[1] = fRec32[0];
			fRec30[1] = fRec30[0];
			fRec50[2] = fRec50[1];
			fRec50[1] = fRec50[0];
			fRec51[2] = fRec51[1];
			fRec51[1] = fRec51[0];
			fRec52[2] = fRec52[1];
			fRec52[1] = fRec52[0];
			fRec53[2] = fRec53[1];
			fRec53[1] = fRec53[0];
			fRec54[2] = fRec54[1];
			fRec54[1] = fRec54[0];
			fRec55[2] = fRec55[1];
			fRec55[1] = fRec55[0];
			fRec56[2] = fRec56[1];
			fRec56[1] = fRec56[0];
			fRec49[1] = fRec49[0];
			fRec47[1] = fRec47[0];
			fRec46[1] = fRec46[0];
			fRec44[1] = fRec44[0];
			fRec43[1] = fRec43[0];
			fRec41[1] = fRec41[0];
			fRec57[2] = fRec57[1];
			fRec57[1] = fRec57[0];
			IOTA = (IOTA + 1);
			fRec69[1] = fRec69[0];
			fRec67[1] = fRec67[0];
			fRec66[1] = fRec66[0];
			fRec64[1] = fRec64[0];
			fRec63[1] = fRec63[0];
			fRec61[1] = fRec61[0];
			fRec60[1] = fRec60[0];
			fRec58[1] = fRec58[0];
			fRec78[1] = fRec78[0];
			fRec76[1] = fRec76[0];
			fRec75[1] = fRec75[0];
			fRec73[1] = fRec73[0];
			fRec72[1] = fRec72[0];
			fRec70[1] = fRec70[0];
			fRec81[1] = fRec81[0];
			fRec79[1] = fRec79[0];
			fRec87[1] = fRec87[0];
			fRec85[1] = fRec85[0];
			fRec84[1] = fRec84[0];
			fRec82[1] = fRec82[0];
			fRec99[1] = fRec99[0];
			fRec97[1] = fRec97[0];
			fRec96[1] = fRec96[0];
			fRec94[1] = fRec94[0];
			fRec93[1] = fRec93[0];
			fRec91[1] = fRec91[0];
			fRec90[1] = fRec90[0];
			fRec88[1] = fRec88[0];
			fRec108[1] = fRec108[0];
			fRec106[1] = fRec106[0];
			fRec105[1] = fRec105[0];
			fRec103[1] = fRec103[0];
			fRec102[1] = fRec102[0];
			fRec100[1] = fRec100[0];
			fRec111[1] = fRec111[0];
			fRec109[1] = fRec109[0];
			fRec117[1] = fRec117[0];
			fRec115[1] = fRec115[0];
			fRec114[1] = fRec114[0];
			fRec112[1] = fRec112[0];
			fRec120[1] = fRec120[0];
			fRec118[1] = fRec118[0];
			fRec126[1] = fRec126[0];
			fRec124[1] = fRec124[0];
			fRec123[1] = fRec123[0];
			fRec121[1] = fRec121[0];
			fRec135[1] = fRec135[0];
			fRec133[1] = fRec133[0];
			fRec132[1] = fRec132[0];
			fRec130[1] = fRec130[0];
			fRec129[1] = fRec129[0];
			fRec127[1] = fRec127[0];
			fRec147[1] = fRec147[0];
			fRec145[1] = fRec145[0];
			fRec144[1] = fRec144[0];
			fRec142[1] = fRec142[0];
			fRec141[1] = fRec141[0];
			fRec139[1] = fRec139[0];
			fRec138[1] = fRec138[0];
			fRec136[1] = fRec136[0];
			fRec150[1] = fRec150[0];
			fRec148[1] = fRec148[0];
			fRec156[1] = fRec156[0];
			fRec154[1] = fRec154[0];
			fRec153[1] = fRec153[0];
			fRec151[1] = fRec151[0];
			fRec165[1] = fRec165[0];
			fRec163[1] = fRec163[0];
			fRec162[1] = fRec162[0];
			fRec160[1] = fRec160[0];
			fRec159[1] = fRec159[0];
			fRec157[1] = fRec157[0];
			fRec177[1] = fRec177[0];
			fRec175[1] = fRec175[0];
			fRec174[1] = fRec174[0];
			fRec172[1] = fRec172[0];
			fRec171[1] = fRec171[0];
			fRec169[1] = fRec169[0];
			fRec168[1] = fRec168[0];
			fRec166[1] = fRec166[0];
			fRec186[1] = fRec186[0];
			fRec184[1] = fRec184[0];
			fRec183[1] = fRec183[0];
			fRec181[1] = fRec181[0];
			fRec180[1] = fRec180[0];
			fRec178[1] = fRec178[0];
			fRec192[1] = fRec192[0];
			fRec190[1] = fRec190[0];
			fRec189[1] = fRec189[0];
			fRec187[1] = fRec187[0];
			fRec204[1] = fRec204[0];
			fRec202[1] = fRec202[0];
			fRec201[1] = fRec201[0];
			fRec199[1] = fRec199[0];
			fRec198[1] = fRec198[0];
			fRec196[1] = fRec196[0];
			fRec195[1] = fRec195[0];
			fRec193[1] = fRec193[0];
			fRec207[1] = fRec207[0];
			fRec205[1] = fRec205[0];
			fRec210[1] = fRec210[0];
			fRec208[1] = fRec208[0];
			fRec216[1] = fRec216[0];
			fRec214[1] = fRec214[0];
			fRec213[1] = fRec213[0];
			fRec211[1] = fRec211[0];
			fRec225[1] = fRec225[0];
			fRec223[1] = fRec223[0];
			fRec222[1] = fRec222[0];
			fRec220[1] = fRec220[0];
			fRec219[1] = fRec219[0];
			fRec217[1] = fRec217[0];
			fRec237[1] = fRec237[0];
			fRec235[1] = fRec235[0];
			fRec234[1] = fRec234[0];
			fRec232[1] = fRec232[0];
			fRec231[1] = fRec231[0];
			fRec229[1] = fRec229[0];
			fRec228[1] = fRec228[0];
			fRec226[1] = fRec226[0];
			fRec240[1] = fRec240[0];
			fRec238[1] = fRec238[0];
			fRec246[1] = fRec246[0];
			fRec244[1] = fRec244[0];
			fRec243[1] = fRec243[0];
			fRec241[1] = fRec241[0];
			fRec255[1] = fRec255[0];
			fRec253[1] = fRec253[0];
			fRec252[1] = fRec252[0];
			fRec250[1] = fRec250[0];
			fRec249[1] = fRec249[0];
			fRec247[1] = fRec247[0];
			fRec267[1] = fRec267[0];
			fRec265[1] = fRec265[0];
			fRec264[1] = fRec264[0];
			fRec262[1] = fRec262[0];
			fRec261[1] = fRec261[0];
			fRec259[1] = fRec259[0];
			fRec258[1] = fRec258[0];
			fRec256[1] = fRec256[0];
			fRec276[1] = fRec276[0];
			fRec274[1] = fRec274[0];
			fRec273[1] = fRec273[0];
			fRec271[1] = fRec271[0];
			fRec270[1] = fRec270[0];
			fRec268[1] = fRec268[0];
			fRec279[1] = fRec279[0];
			fRec277[1] = fRec277[0];
			fRec285[1] = fRec285[0];
			fRec283[1] = fRec283[0];
			fRec282[1] = fRec282[0];
			fRec280[1] = fRec280[0];
			fRec297[1] = fRec297[0];
			fRec295[1] = fRec295[0];
			fRec294[1] = fRec294[0];
			fRec292[1] = fRec292[0];
			fRec291[1] = fRec291[0];
			fRec289[1] = fRec289[0];
			fRec288[1] = fRec288[0];
			fRec286[1] = fRec286[0];
			fRec300[1] = fRec300[0];
			fRec298[1] = fRec298[0];
			fRec306[1] = fRec306[0];
			fRec304[1] = fRec304[0];
			fRec303[1] = fRec303[0];
			fRec301[1] = fRec301[0];
			fRec315[1] = fRec315[0];
			fRec313[1] = fRec313[0];
			fRec312[1] = fRec312[0];
			fRec310[1] = fRec310[0];
			fRec309[1] = fRec309[0];
			fRec307[1] = fRec307[0];
			fRec327[1] = fRec327[0];
			fRec325[1] = fRec325[0];
			fRec324[1] = fRec324[0];
			fRec322[1] = fRec322[0];
			fRec321[1] = fRec321[0];
			fRec319[1] = fRec319[0];
			fRec318[1] = fRec318[0];
			fRec316[1] = fRec316[0];
			fRec339[1] = fRec339[0];
			fRec337[1] = fRec337[0];
			fRec336[1] = fRec336[0];
			fRec334[1] = fRec334[0];
			fRec333[1] = fRec333[0];
			fRec331[1] = fRec331[0];
			fRec330[1] = fRec330[0];
			fRec328[1] = fRec328[0];
			fRec348[1] = fRec348[0];
			fRec346[1] = fRec346[0];
			fRec345[1] = fRec345[0];
			fRec343[1] = fRec343[0];
			fRec342[1] = fRec342[0];
			fRec340[1] = fRec340[0];
			fRec351[1] = fRec351[0];
			fRec349[1] = fRec349[0];
			fRec357[1] = fRec357[0];
			fRec355[1] = fRec355[0];
			fRec354[1] = fRec354[0];
			fRec352[1] = fRec352[0];
			fRec369[1] = fRec369[0];
			fRec367[1] = fRec367[0];
			fRec366[1] = fRec366[0];
			fRec364[1] = fRec364[0];
			fRec363[1] = fRec363[0];
			fRec361[1] = fRec361[0];
			fRec360[1] = fRec360[0];
			fRec358[1] = fRec358[0];
			fRec378[1] = fRec378[0];
			fRec376[1] = fRec376[0];
			fRec375[1] = fRec375[0];
			fRec373[1] = fRec373[0];
			fRec372[1] = fRec372[0];
			fRec370[1] = fRec370[0];
			fRec381[1] = fRec381[0];
			fRec379[1] = fRec379[0];
			fRec387[1] = fRec387[0];
			fRec385[1] = fRec385[0];
			fRec384[1] = fRec384[0];
			fRec382[1] = fRec382[0];
			fRec399[1] = fRec399[0];
			fRec397[1] = fRec397[0];
			fRec396[1] = fRec396[0];
			fRec394[1] = fRec394[0];
			fRec393[1] = fRec393[0];
			fRec391[1] = fRec391[0];
			fRec390[1] = fRec390[0];
			fRec388[1] = fRec388[0];
			fRec408[1] = fRec408[0];
			fRec406[1] = fRec406[0];
			fRec405[1] = fRec405[0];
			fRec403[1] = fRec403[0];
			fRec402[1] = fRec402[0];
			fRec400[1] = fRec400[0];
			fRec411[1] = fRec411[0];
			fRec409[1] = fRec409[0];
			fRec417[1] = fRec417[0];
			fRec415[1] = fRec415[0];
			fRec414[1] = fRec414[0];
			fRec412[1] = fRec412[0];
			fRec429[1] = fRec429[0];
			fRec427[1] = fRec427[0];
			fRec426[1] = fRec426[0];
			fRec424[1] = fRec424[0];
			fRec423[1] = fRec423[0];
			fRec421[1] = fRec421[0];
			fRec420[1] = fRec420[0];
			fRec418[1] = fRec418[0];
			fRec438[1] = fRec438[0];
			fRec436[1] = fRec436[0];
			fRec435[1] = fRec435[0];
			fRec433[1] = fRec433[0];
			fRec432[1] = fRec432[0];
			fRec430[1] = fRec430[0];
			fRec441[1] = fRec441[0];
			fRec439[1] = fRec439[0];
			fRec447[1] = fRec447[0];
			fRec445[1] = fRec445[0];
			fRec444[1] = fRec444[0];
			fRec442[1] = fRec442[0];
			fRec459[1] = fRec459[0];
			fRec457[1] = fRec457[0];
			fRec456[1] = fRec456[0];
			fRec454[1] = fRec454[0];
			fRec453[1] = fRec453[0];
			fRec451[1] = fRec451[0];
			fRec450[1] = fRec450[0];
			fRec448[1] = fRec448[0];
			fRec468[1] = fRec468[0];
			fRec466[1] = fRec466[0];
			fRec465[1] = fRec465[0];
			fRec463[1] = fRec463[0];
			fRec462[1] = fRec462[0];
			fRec460[1] = fRec460[0];
			fRec471[1] = fRec471[0];
			fRec469[1] = fRec469[0];
			fRec477[1] = fRec477[0];
			fRec475[1] = fRec475[0];
			fRec474[1] = fRec474[0];
			fRec472[1] = fRec472[0];
			fRec489[1] = fRec489[0];
			fRec487[1] = fRec487[0];
			fRec486[1] = fRec486[0];
			fRec484[1] = fRec484[0];
			fRec483[1] = fRec483[0];
			fRec481[1] = fRec481[0];
			fRec480[1] = fRec480[0];
			fRec478[1] = fRec478[0];
			fRec498[1] = fRec498[0];
			fRec496[1] = fRec496[0];
			fRec495[1] = fRec495[0];
			fRec493[1] = fRec493[0];
			fRec492[1] = fRec492[0];
			fRec490[1] = fRec490[0];
			fRec501[1] = fRec501[0];
			fRec499[1] = fRec499[0];
			fRec507[1] = fRec507[0];
			fRec505[1] = fRec505[0];
			fRec504[1] = fRec504[0];
			fRec502[1] = fRec502[0];
			fRec513[1] = fRec513[0];
			fRec511[1] = fRec511[0];
			fRec510[1] = fRec510[0];
			fRec508[1] = fRec508[0];
			fRec516[1] = fRec516[0];
			fRec514[1] = fRec514[0];
			fRec525[1] = fRec525[0];
			fRec523[1] = fRec523[0];
			fRec522[1] = fRec522[0];
			fRec520[1] = fRec520[0];
			fRec519[1] = fRec519[0];
			fRec517[1] = fRec517[0];
			fRec537[1] = fRec537[0];
			fRec535[1] = fRec535[0];
			fRec534[1] = fRec534[0];
			fRec532[1] = fRec532[0];
			fRec531[1] = fRec531[0];
			fRec529[1] = fRec529[0];
			fRec528[1] = fRec528[0];
			fRec526[1] = fRec526[0];
			fRec549[1] = fRec549[0];
			fRec547[1] = fRec547[0];
			fRec546[1] = fRec546[0];
			fRec544[1] = fRec544[0];
			fRec543[1] = fRec543[0];
			fRec541[1] = fRec541[0];
			fRec540[1] = fRec540[0];
			fRec538[1] = fRec538[0];
			fRec558[1] = fRec558[0];
			fRec556[1] = fRec556[0];
			fRec555[1] = fRec555[0];
			fRec553[1] = fRec553[0];
			fRec552[1] = fRec552[0];
			fRec550[1] = fRec550[0];
			fRec561[1] = fRec561[0];
			fRec559[1] = fRec559[0];
			fRec567[1] = fRec567[0];
			fRec565[1] = fRec565[0];
			fRec564[1] = fRec564[0];
			fRec562[1] = fRec562[0];
			fRec579[1] = fRec579[0];
			fRec577[1] = fRec577[0];
			fRec576[1] = fRec576[0];
			fRec574[1] = fRec574[0];
			fRec573[1] = fRec573[0];
			fRec571[1] = fRec571[0];
			fRec570[1] = fRec570[0];
			fRec568[1] = fRec568[0];
			fRec588[1] = fRec588[0];
			fRec586[1] = fRec586[0];
			fRec585[1] = fRec585[0];
			fRec583[1] = fRec583[0];
			fRec582[1] = fRec582[0];
			fRec580[1] = fRec580[0];
			fRec591[1] = fRec591[0];
			fRec589[1] = fRec589[0];
			fRec597[1] = fRec597[0];
			fRec595[1] = fRec595[0];
			fRec594[1] = fRec594[0];
			fRec592[1] = fRec592[0];
			fRec609[1] = fRec609[0];
			fRec607[1] = fRec607[0];
			fRec606[1] = fRec606[0];
			fRec604[1] = fRec604[0];
			fRec603[1] = fRec603[0];
			fRec601[1] = fRec601[0];
			fRec600[1] = fRec600[0];
			fRec598[1] = fRec598[0];
			fRec618[1] = fRec618[0];
			fRec616[1] = fRec616[0];
			fRec615[1] = fRec615[0];
			fRec613[1] = fRec613[0];
			fRec612[1] = fRec612[0];
			fRec610[1] = fRec610[0];
			fRec621[1] = fRec621[0];
			fRec619[1] = fRec619[0];
			fRec627[1] = fRec627[0];
			fRec625[1] = fRec625[0];
			fRec624[1] = fRec624[0];
			fRec622[1] = fRec622[0];
			fVec19[2] = fVec19[1];
			fVec19[1] = fVec19[0];
			fRec630[1] = fRec630[0];
			fRec628[1] = fRec628[0];
			fRec642[1] = fRec642[0];
			fRec640[1] = fRec640[0];
			fRec639[1] = fRec639[0];
			fRec637[1] = fRec637[0];
			fRec636[1] = fRec636[0];
			fRec634[1] = fRec634[0];
			fRec633[1] = fRec633[0];
			fRec631[1] = fRec631[0];
			fRec648[1] = fRec648[0];
			fRec646[1] = fRec646[0];
			fRec645[1] = fRec645[0];
			fRec643[1] = fRec643[0];
			fRec657[1] = fRec657[0];
			fRec655[1] = fRec655[0];
			fRec654[1] = fRec654[0];
			fRec652[1] = fRec652[0];
			fRec651[1] = fRec651[0];
			fRec649[1] = fRec649[0];
			fRec669[1] = fRec669[0];
			fRec667[1] = fRec667[0];
			fRec666[1] = fRec666[0];
			fRec664[1] = fRec664[0];
			fRec663[1] = fRec663[0];
			fRec661[1] = fRec661[0];
			fRec660[1] = fRec660[0];
			fRec658[1] = fRec658[0];
			fRec678[1] = fRec678[0];
			fRec676[1] = fRec676[0];
			fRec675[1] = fRec675[0];
			fRec673[1] = fRec673[0];
			fRec672[1] = fRec672[0];
			fRec670[1] = fRec670[0];
			fRec681[1] = fRec681[0];
			fRec679[1] = fRec679[0];
			fRec687[1] = fRec687[0];
			fRec685[1] = fRec685[0];
			fRec684[1] = fRec684[0];
			fRec682[1] = fRec682[0];
			fRec699[1] = fRec699[0];
			fRec697[1] = fRec697[0];
			fRec696[1] = fRec696[0];
			fRec694[1] = fRec694[0];
			fRec693[1] = fRec693[0];
			fRec691[1] = fRec691[0];
			fRec690[1] = fRec690[0];
			fRec688[1] = fRec688[0];
			fRec708[1] = fRec708[0];
			fRec706[1] = fRec706[0];
			fRec705[1] = fRec705[0];
			fRec703[1] = fRec703[0];
			fRec702[1] = fRec702[0];
			fRec700[1] = fRec700[0];
			fRec711[1] = fRec711[0];
			fRec709[1] = fRec709[0];
			fRec717[1] = fRec717[0];
			fRec715[1] = fRec715[0];
			fRec714[1] = fRec714[0];
			fRec712[1] = fRec712[0];
			fVec20[2] = fVec20[1];
			fVec20[1] = fVec20[0];
			fRec729[1] = fRec729[0];
			fRec727[1] = fRec727[0];
			fRec726[1] = fRec726[0];
			fRec724[1] = fRec724[0];
			fRec723[1] = fRec723[0];
			fRec721[1] = fRec721[0];
			fRec720[1] = fRec720[0];
			fRec718[1] = fRec718[0];
			fRec738[1] = fRec738[0];
			fRec736[1] = fRec736[0];
			fRec735[1] = fRec735[0];
			fRec733[1] = fRec733[0];
			fRec732[1] = fRec732[0];
			fRec730[1] = fRec730[0];
			fRec741[1] = fRec741[0];
			fRec739[1] = fRec739[0];
			fRec747[1] = fRec747[0];
			fRec745[1] = fRec745[0];
			fRec744[1] = fRec744[0];
			fRec742[1] = fRec742[0];
			fRec756[1] = fRec756[0];
			fRec754[1] = fRec754[0];
			fRec753[1] = fRec753[0];
			fRec751[1] = fRec751[0];
			fRec750[1] = fRec750[0];
			fRec748[1] = fRec748[0];
			fRec762[1] = fRec762[0];
			fRec760[1] = fRec760[0];
			fRec759[1] = fRec759[0];
			fRec757[1] = fRec757[0];
			fRec774[1] = fRec774[0];
			fRec772[1] = fRec772[0];
			fRec771[1] = fRec771[0];
			fRec769[1] = fRec769[0];
			fRec768[1] = fRec768[0];
			fRec766[1] = fRec766[0];
			fRec765[1] = fRec765[0];
			fRec763[1] = fRec763[0];
			fRec777[1] = fRec777[0];
			fRec775[1] = fRec775[0];
			fRec789[1] = fRec789[0];
			fRec787[1] = fRec787[0];
			fRec786[1] = fRec786[0];
			fRec784[1] = fRec784[0];
			fRec783[1] = fRec783[0];
			fRec781[1] = fRec781[0];
			fRec780[1] = fRec780[0];
			fRec778[1] = fRec778[0];
			fRec798[1] = fRec798[0];
			fRec796[1] = fRec796[0];
			fRec795[1] = fRec795[0];
			fRec793[1] = fRec793[0];
			fRec792[1] = fRec792[0];
			fRec790[1] = fRec790[0];
			fRec801[1] = fRec801[0];
			fRec799[1] = fRec799[0];
			fRec807[1] = fRec807[0];
			fRec805[1] = fRec805[0];
			fRec804[1] = fRec804[0];
			fRec802[1] = fRec802[0];
			fVec21[2] = fVec21[1];
			fVec21[1] = fVec21[0];
			fRec819[1] = fRec819[0];
			fRec817[1] = fRec817[0];
			fRec816[1] = fRec816[0];
			fRec814[1] = fRec814[0];
			fRec813[1] = fRec813[0];
			fRec811[1] = fRec811[0];
			fRec810[1] = fRec810[0];
			fRec808[1] = fRec808[0];
			fRec828[1] = fRec828[0];
			fRec826[1] = fRec826[0];
			fRec825[1] = fRec825[0];
			fRec823[1] = fRec823[0];
			fRec822[1] = fRec822[0];
			fRec820[1] = fRec820[0];
			fRec831[1] = fRec831[0];
			fRec829[1] = fRec829[0];
			fRec837[1] = fRec837[0];
			fRec835[1] = fRec835[0];
			fRec834[1] = fRec834[0];
			fRec832[1] = fRec832[0];
			fRec849[1] = fRec849[0];
			fRec847[1] = fRec847[0];
			fRec846[1] = fRec846[0];
			fRec844[1] = fRec844[0];
			fRec843[1] = fRec843[0];
			fRec841[1] = fRec841[0];
			fRec840[1] = fRec840[0];
			fRec838[1] = fRec838[0];
			fRec858[1] = fRec858[0];
			fRec856[1] = fRec856[0];
			fRec855[1] = fRec855[0];
			fRec853[1] = fRec853[0];
			fRec852[1] = fRec852[0];
			fRec850[1] = fRec850[0];
			fRec861[1] = fRec861[0];
			fRec859[1] = fRec859[0];
			fRec867[1] = fRec867[0];
			fRec865[1] = fRec865[0];
			fRec864[1] = fRec864[0];
			fRec862[1] = fRec862[0];
			fRec879[1] = fRec879[0];
			fRec877[1] = fRec877[0];
			fRec876[1] = fRec876[0];
			fRec874[1] = fRec874[0];
			fRec873[1] = fRec873[0];
			fRec871[1] = fRec871[0];
			fRec870[1] = fRec870[0];
			fRec868[1] = fRec868[0];
			fRec888[1] = fRec888[0];
			fRec886[1] = fRec886[0];
			fRec885[1] = fRec885[0];
			fRec883[1] = fRec883[0];
			fRec882[1] = fRec882[0];
			fRec880[1] = fRec880[0];
			fRec891[1] = fRec891[0];
			fRec889[1] = fRec889[0];
			fRec897[1] = fRec897[0];
			fRec895[1] = fRec895[0];
			fRec894[1] = fRec894[0];
			fRec892[1] = fRec892[0];
			fVec22[2] = fVec22[1];
			fVec22[1] = fVec22[0];
			fRec909[1] = fRec909[0];
			fRec907[1] = fRec907[0];
			fRec906[1] = fRec906[0];
			fRec904[1] = fRec904[0];
			fRec903[1] = fRec903[0];
			fRec901[1] = fRec901[0];
			fRec900[1] = fRec900[0];
			fRec898[1] = fRec898[0];
			fRec918[1] = fRec918[0];
			fRec916[1] = fRec916[0];
			fRec915[1] = fRec915[0];
			fRec913[1] = fRec913[0];
			fRec912[1] = fRec912[0];
			fRec910[1] = fRec910[0];
			fRec921[1] = fRec921[0];
			fRec919[1] = fRec919[0];
			fRec927[1] = fRec927[0];
			fRec925[1] = fRec925[0];
			fRec924[1] = fRec924[0];
			fRec922[1] = fRec922[0];
			fRec939[1] = fRec939[0];
			fRec937[1] = fRec937[0];
			fRec936[1] = fRec936[0];
			fRec934[1] = fRec934[0];
			fRec933[1] = fRec933[0];
			fRec931[1] = fRec931[0];
			fRec930[1] = fRec930[0];
			fRec928[1] = fRec928[0];
			fRec948[1] = fRec948[0];
			fRec946[1] = fRec946[0];
			fRec945[1] = fRec945[0];
			fRec943[1] = fRec943[0];
			fRec942[1] = fRec942[0];
			fRec940[1] = fRec940[0];
			fRec951[1] = fRec951[0];
			fRec949[1] = fRec949[0];
			fRec957[1] = fRec957[0];
			fRec955[1] = fRec955[0];
			fRec954[1] = fRec954[0];
			fRec952[1] = fRec952[0];
			
		}
		
	}

	
};

//----------------------------------------------------------------------------
// SuperCollider/Faust interface
//----------------------------------------------------------------------------

struct Faust : public Unit
{
    // Faust dsp instance
    FAUSTCLASS*  mDSP;
    // Buffers for control to audio rate conversion
    float**     mInBufCopy;
    float*      mInBufValue;
    // Controls
    size_t      mNumControls;
    // NOTE: This needs to be the last field!
    //
    // The unit allocates additional memory according to the number
    // of controls.
    Control     mControls[0];

    int getNumAudioInputs() { return mDSP->getNumInputs(); }
};

// Global state

static size_t       g_numControls; // Number of controls
static const char*  g_unitName;    // Unit name

// Initialize the global state with unit name and sample rate.
void initState(const std::string& name, int sampleRate);

// Return the unit size in bytes, including static fields and controls.
static size_t unitSize();

// Convert a file name to a valid unit name.
static std::string fileNameToUnitName(const std::string& fileName);

// Convert the XML unit name to a valid class name.
static std::string normalizeClassName(const std::string& name);

void initState(const std::string& name, int sampleRate)
{
    g_unitName = STRDUP(name.c_str());

    mydsp* dsp = new FAUSTCLASS;
    ControlCounter* cc = new ControlCounter;

    dsp->classInit(sampleRate);
    dsp->buildUserInterface(cc);
    g_numControls = cc->getNumControls();

    delete dsp;
    delete cc;
}

size_t unitSize()
{
    return sizeof(Faust) + g_numControls * sizeof(Control);
}

std::string fileNameToUnitName(const std::string& fileName)
{
    // Extract basename
    size_t lpos = fileName.rfind('/', fileName.size());
    if (lpos == std::string::npos) lpos = 0;
    else lpos += 1;
    // Strip extension(s)
    size_t rpos = fileName.find('.', lpos);
    // Return substring
    return fileName.substr(lpos, rpos > lpos ? rpos - lpos : 0);
}

// Globals

static InterfaceTable* ft;

// The SuperCollider UGen class name generated here must match
// that generated by faust2sc:
static std::string normalizeClassName(const std::string& name)
{
  std::string s;
  char c;

  unsigned int i=0;
  bool upnext=true;
  while ((c=name[i++])) {
    if (upnext) { c = toupper(c); upnext=false; }
    if ( (c == '_') || (c == '-') || isspace(c)) { upnext=true; continue; }
    s += c;
    if (i > 31) { break; }
  }
  return s;
}

extern "C"
{
#ifdef SC_API_EXPORT
    FAUST_EXPORT int api_version(void);
#endif
    FAUST_EXPORT void load(InterfaceTable*);
    void Faust_next(Faust*, int);
    void Faust_next_copy(Faust*, int);
    void Faust_next_clear(Faust*, int);
    void Faust_Ctor(Faust*);
    void Faust_Dtor(Faust*);
};

inline static void fillBuffer(float* dst, int n, float v)
{
    Fill(n, dst, v);
}

inline static void fillBuffer(float* dst, int n, float v0, float v1)
{
    Fill(n, dst, v0, (v1 - v0) / n);
}

inline static void copyBuffer(float* dst, int n, float* src)
{
    Copy(n, dst, src);
}

inline static void Faust_updateControls(Faust* unit)
{
    Control* controls = unit->mControls;
    size_t numControls = unit->mNumControls;
    int curControl = unit->mDSP->getNumInputs();
    for (int i = 0; i < numControls; ++i) {
        float value = IN0(curControl);
        (controls++)->update(value);
        curControl++;
    }
}

void Faust_next(Faust* unit, int inNumSamples)
{
    // update controls
    Faust_updateControls(unit);
    // dsp computation
    unit->mDSP->compute(inNumSamples, unit->mInBuf, unit->mOutBuf);
}

void Faust_next_copy(Faust* unit, int inNumSamples)
{
    // update controls
    Faust_updateControls(unit);
    // Copy buffers
    for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
        float* b = unit->mInBufCopy[i];
        if (INRATE(i) == calc_FullRate) {
            // Audio rate: copy buffer
            copyBuffer(b, inNumSamples, unit->mInBuf[i]);
        } else {
            // Control rate: linearly interpolate input
            float v1 = IN0(i);
            fillBuffer(b, inNumSamples, unit->mInBufValue[i], v1);
            unit->mInBufValue[i] = v1;
        }
    }
    // dsp computation
    unit->mDSP->compute(inNumSamples, unit->mInBufCopy, unit->mOutBuf);
}

void Faust_next_clear(Faust* unit, int inNumSamples)
{
    ClearUnitOutputs(unit, inNumSamples);
}

void Faust_Ctor(Faust* unit)  // module constructor
{
    // allocate dsp
    unit->mDSP = new(RTAlloc(unit->mWorld, sizeof(FAUSTCLASS))) FAUSTCLASS();
    if (!unit->mDSP) {
        Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
        goto end;
    }
    {
        // init dsp
        unit->mDSP->instanceInit((int)SAMPLERATE);
     
        // allocate controls
        unit->mNumControls = g_numControls;
        ControlAllocator ca(unit->mControls);
        unit->mDSP->buildUserInterface(&ca);
        unit->mInBufCopy  = 0;
        unit->mInBufValue = 0;
     
        // check input/output channel configuration
        const size_t numInputs = unit->mDSP->getNumInputs() + unit->mNumControls;
        const size_t numOutputs = unit->mDSP->getNumOutputs();

        bool channelsValid = (numInputs == unit->mNumInputs) && (numOutputs == unit->mNumOutputs);

        if (channelsValid) {
            bool rateValid = true;
            for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
                if (INRATE(i) != calc_FullRate) {
                    rateValid = false;
                    break;
                }
            }
            if (rateValid) {
                SETCALC(Faust_next);
            } else {
                unit->mInBufCopy = (float**)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*sizeof(float*));
                if (!unit->mInBufCopy) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                // Allocate memory for input buffer copies (numInputs * bufLength)
                // and linear interpolation state (numInputs)
                // = numInputs * (bufLength + 1)
                unit->mInBufValue = (float*)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*sizeof(float));
                if (!unit->mInBufValue) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                // Aquire memory for interpolator state.
                float* mem = (float*)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*BUFLENGTH*sizeof(float));
                if (mem) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
                    // Initialize interpolator.
                    unit->mInBufValue[i] = IN0(i);
                    // Aquire buffer memory.
                    unit->mInBufCopy[i] = mem;
                    mem += BUFLENGTH;
                }
                SETCALC(Faust_next_copy);
            }
    #if !defined(NDEBUG)
            Print("Faust[%s]:\n", g_unitName);
            Print("    Inputs:   %d\n"
                  "    Outputs:  %d\n"
                  "    Callback: %s\n",
                  numInputs, numOutputs,
                  unit->mCalcFunc == (UnitCalcFunc)Faust_next ? "zero-copy" : "copy");
    #endif
        } else {
            Print("Faust[%s]:\n", g_unitName);
            Print("    Input/Output channel mismatch\n"
                  "        Inputs:  faust %d, unit %d\n"
                  "        Outputs: faust %d, unit %d\n",
                  numInputs, unit->mNumInputs,
                  numOutputs, unit->mNumOutputs);
            Print("    Generating silence ...\n");
            SETCALC(Faust_next_clear);
        }
    }
    
end:
    // Fix for https://github.com/grame-cncm/faust/issues/13
    ClearUnitOutputs(unit, 1);
}

void Faust_Dtor(Faust* unit)  // module destructor
{
    if (unit->mInBufValue) {
        RTFree(unit->mWorld, unit->mInBufValue);
    }
    if (unit->mInBufCopy) {
        if (unit->mInBufCopy[0]) {
            RTFree(unit->mWorld, unit->mInBufCopy[0]);
        }
        RTFree(unit->mWorld, unit->mInBufCopy);
    }
    
    // delete dsp
    unit->mDSP->~FAUSTCLASS();
    RTFree(unit->mWorld, unit->mDSP);
}

#ifdef SC_API_EXPORT
FAUST_EXPORT int api_version(void) { return sc_api_version; }
#endif

FAUST_EXPORT void load(InterfaceTable* inTable)
{
    ft = inTable;

    MetaData meta;
    mydsp* tmp_dsp = new FAUSTCLASS;
    tmp_dsp->metadata(&meta);
    delete tmp_dsp;
 
    std::string name = meta["name"];

    if (name.empty()) {
        name = fileNameToUnitName(__FILE__);
    }
  
    name = normalizeClassName(name);

#if !defined(NDEBUG) & defined(SC_API_EXPORT)
    Print("Faust: supercollider.cpp: sc_api_version = %d\n", sc_api_version);
#endif

    if (name.empty()) {
        // Catch empty name
        Print("Faust [supercollider.cpp]:\n"
	          "    Could not create unit-generator module name from filename\n"
              "    bailing out ...\n");
        return;
    }

    if (strncmp(name.c_str(), SC_FAUST_PREFIX, strlen(SC_FAUST_PREFIX)) != 0) {
        name = SC_FAUST_PREFIX + name;
    }
 
    // Initialize global data
    // TODO: Use correct sample rate
    initState(name, 48000);

    // Register ugen
    (*ft->fDefineUnit)(
        (char*)name.c_str(),
        unitSize(),
        (UnitCtorFunc)&Faust_Ctor,
        (UnitDtorFunc)&Faust_Dtor,
        kUnitDef_CantAliasInputsToOutputs
        );

#if !defined(NDEBUG)
    Print("Faust: %s numControls=%d\n", name.c_str(), g_numControls);
#endif // NDEBUG
}

#ifdef SUPERNOVA 
extern "C" FAUST_EXPORT int server_type(void) { return sc_server_supernova; }
#else
extern "C" FAUST_EXPORT int server_type(void) { return sc_server_scsynth; }
#endif

// EOF

#endif
