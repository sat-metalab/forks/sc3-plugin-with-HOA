/* ------------------------------------------------------------
author: "AmbisonicDecoderToolkit"
copyright: "(c) Aaron J. Heller 2013"
license: "BSD 3-Clause License"
name: "SATOswD3o4"
version: "1.2"
Code generated with Faust 2.5.12 (https://faust.grame.fr)
Compilation options: cpp, -scal -ftz 0
------------------------------------------------------------ */

#ifndef  __mydsp_H__
#define  __mydsp_H__

//-------------------------------------------------------------------
// FAUST architecture file for SuperCollider.
// Copyright (C) 2005-2012 Stefan Kersten.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//-------------------------------------------------------------------

// If other than 'faust2sc --prefix Faust' is used, sed this as well:
#if !defined(SC_FAUST_PREFIX)
#define SC_FAUST_PREFIX "Faust"
#endif

#include <map>
#include <string>
#include <string.h>
#include <SC_PlugIn.h>

/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __dsp__
#define __dsp__

#include <string>

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

class UI;
struct Meta;

/**
 * DSP memory manager.
 */

struct dsp_memory_manager {
    
    virtual ~dsp_memory_manager() {}
    
    virtual void* allocate(size_t size) = 0;
    virtual void destroy(void* ptr) = 0;
    
};

/**
* Signal processor definition.
*/

class dsp {

    public:

        dsp() {}
        virtual ~dsp() {}

        /* Return instance number of audio inputs */
        virtual int getNumInputs() = 0;
    
        /* Return instance number of audio outputs */
        virtual int getNumOutputs() = 0;
    
        /**
         * Trigger the ui_interface parameter with instance specific calls
         * to 'addBtton', 'addVerticalSlider'... in order to build the UI.
         *
         * @param ui_interface - the user interface builder
         */
        virtual void buildUserInterface(UI* ui_interface) = 0;
    
        /* Returns the sample rate currently used by the instance */
        virtual int getSampleRate() = 0;
    
        /**
         * Global init, calls the following methods:
         * - static class 'classInit': static tables initialization
         * - 'instanceInit': constants and instance state initialization
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void init(int samplingRate) = 0;

        /**
         * Init instance state
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void instanceInit(int samplingRate) = 0;

        /**
         * Init instance constant state
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void instanceConstants(int samplingRate) = 0;
    
        /* Init default control parameters values */
        virtual void instanceResetUserInterface() = 0;
    
        /* Init instance state (delay lines...) */
        virtual void instanceClear() = 0;
 
        /**
         * Return a clone of the instance.
         *
         * @return a copy of the instance on success, otherwise a null pointer.
         */
        virtual dsp* clone() = 0;
    
        /**
         * Trigger the Meta* parameter with instance specific calls to 'declare' (key, value) metadata.
         *
         * @param m - the Meta* meta user
         */
        virtual void metadata(Meta* m) = 0;
    
        /**
         * DSP instance computation, to be called with successive in/out audio buffers.
         *
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         *
         */
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) = 0;
    
        /**
         * DSP instance computation: alternative method to be used by subclasses.
         *
         * @param date_usec - the timestamp in microsec given by audio driver.
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         *
         */
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { compute(count, inputs, outputs); }
       
};

/**
 * Generic DSP decorator.
 */

class decorator_dsp : public dsp {

    protected:

        dsp* fDSP;

    public:

        decorator_dsp(dsp* dsp = 0):fDSP(dsp) {}
        virtual ~decorator_dsp() { delete fDSP; }

        virtual int getNumInputs() { return fDSP->getNumInputs(); }
        virtual int getNumOutputs() { return fDSP->getNumOutputs(); }
        virtual void buildUserInterface(UI* ui_interface) { fDSP->buildUserInterface(ui_interface); }
        virtual int getSampleRate() { return fDSP->getSampleRate(); }
        virtual void init(int samplingRate) { fDSP->init(samplingRate); }
        virtual void instanceInit(int samplingRate) { fDSP->instanceInit(samplingRate); }
        virtual void instanceConstants(int samplingRate) { fDSP->instanceConstants(samplingRate); }
        virtual void instanceResetUserInterface() { fDSP->instanceResetUserInterface(); }
        virtual void instanceClear() { fDSP->instanceClear(); }
        virtual decorator_dsp* clone() { return new decorator_dsp(fDSP->clone()); }
        virtual void metadata(Meta* m) { fDSP->metadata(m); }
        // Beware: subclasses usually have to overload the two 'compute' methods
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(count, inputs, outputs); }
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(date_usec, count, inputs, outputs); }
    
};

/**
 * DSP factory class.
 */

class dsp_factory {
    
    protected:
    
        // So that to force sub-classes to use deleteDSPFactory(dsp_factory* factory);
        virtual ~dsp_factory() {}
    
    public:
    
        virtual std::string getName() = 0;
        virtual std::string getSHAKey() = 0;
        virtual std::string getDSPCode() = 0;
    
        virtual dsp* createDSPInstance() = 0;
    
        virtual void setMemoryManager(dsp_memory_manager* manager) = 0;
        virtual dsp_memory_manager* getMemoryManager() = 0;
    
};

/**
 * On Intel set FZ (Flush to Zero) and DAZ (Denormals Are Zero)
 * flags to avoid costly denormals.
 */

#ifdef __SSE__
    #include <xmmintrin.h>
    #ifdef __SSE2__
        #define AVOIDDENORMALS _mm_setcsr(_mm_getcsr() | 0x8040)
    #else
        #define AVOIDDENORMALS _mm_setcsr(_mm_getcsr() | 0x8000)
    #endif
#else
    #define AVOIDDENORMALS
#endif

#endif
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __UI_H__
#define __UI_H__

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

/*******************************************************************************
 * UI : Faust DSP User Interface
 * User Interface as expected by the buildUserInterface() method of a DSP.
 * This abstract class contains only the method that the Faust compiler can
 * generate to describe a DSP user interface.
 ******************************************************************************/

struct Soundfile;

class UI
{

    public:

        UI() {}

        virtual ~UI() {}

        // -- widget's layouts

        virtual void openTabBox(const char* label) = 0;
        virtual void openHorizontalBox(const char* label) = 0;
        virtual void openVerticalBox(const char* label) = 0;
        virtual void closeBox() = 0;

        // -- active widgets

        virtual void addButton(const char* label, FAUSTFLOAT* zone) = 0;
        virtual void addCheckButton(const char* label, FAUSTFLOAT* zone) = 0;
        virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;
        virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;
        virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;

        // -- passive widgets

        virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) = 0;
        virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) = 0;
    
        // -- soundfiles
    
        virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) = 0;

        // -- metadata declarations

        virtual void declare(FAUSTFLOAT*, const char*, const char*) {}
};

#endif
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/
 
#ifndef __misc__
#define __misc__

#include <algorithm>
#include <map>
#include <string.h>
#include <stdlib.h>

/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __meta__
#define __meta__

struct Meta
{
    virtual void declare(const char* key, const char* value) = 0;
    virtual ~Meta() {};
};

#endif

using std::max;
using std::min;

struct XXXX_Meta : std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key]=value; }
};

struct MY_Meta : Meta, std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key]=value; }
};

inline int lsr(int x, int n)	{ return int(((unsigned int)x) >> n); }

inline int int2pow2(int x)		{ int r = 0; while ((1<<r) < x) r++; return r; }

inline long lopt(char* argv[], const char* name, long def)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return atoi(argv[i+1]);
	return def;
}

inline bool isopt(char* argv[], const char* name)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return true;
	return false;
}

inline const char* lopts(char* argv[], const char* name, const char* def)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return argv[i+1];
	return def;
}

#endif


using namespace std;

#if defined(__GNUC__) && __GNUC__ >= 4
    #define FAUST_EXPORT __attribute__((visibility("default")))
#else
    #define FAUST_EXPORT  SC_API_EXPORT
#endif

#ifdef WIN32
    #define STRDUP _strdup
#else
    #define STRDUP strdup
#endif

//----------------------------------------------------------------------------
// Vector intrinsics
//----------------------------------------------------------------------------


//----------------------------------------------------------------------------
// Metadata
//----------------------------------------------------------------------------

class MetaData : public Meta
               , public std::map<std::string, std::string>
{
public:
    void declare(const char* key, const char* value)
    {
        (*this)[key] = value;
    }
};

//----------------------------------------------------------------------------
// Control counter
//----------------------------------------------------------------------------

class ControlCounter : public UI
{
public:
    ControlCounter()
        : mNumControlInputs(0),
          mNumControlOutputs(0)
    {}

    size_t getNumControls() const { return getNumControlInputs(); }
    size_t getNumControlInputs() const { return mNumControlInputs; }
    size_t getNumControlOutputs() const { return mNumControlOutputs; }

    // Layout widgets
    virtual void openTabBox(const char* label) { }
    virtual void openHorizontalBox(const char* label) { }
    virtual void openVerticalBox(const char* label) { }
    virtual void closeBox() { }

    // Active widgets
    virtual void addButton(const char* label, FAUSTFLOAT* zone)
    { addControlInput(); }
    virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
    { addControlInput(); }
    virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }
    virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }
    virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }

    // Passive widgets
    virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
    { addControlOutput(); }
    virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
    { addControlOutput(); }
    
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}

protected:
    void addControlInput() { mNumControlInputs++; }
    void addControlOutput() { mNumControlOutputs++; }

private:
    size_t mNumControlInputs;
    size_t mNumControlOutputs;
};

//----------------------------------------------------------------------------
// UI control
//----------------------------------------------------------------------------

struct Control
{
    typedef void (*UpdateFunction)(Control* self, FAUSTFLOAT value);

    UpdateFunction updateFunction;
    FAUSTFLOAT* zone;
    FAUSTFLOAT min, max;

    inline void update(FAUSTFLOAT value)
    {
        (*updateFunction)(this, value);
    }

    static void simpleUpdate(Control* self, FAUSTFLOAT value)
    {
        *self->zone = value;
    }
    static void boundedUpdate(Control* self, FAUSTFLOAT value)
    {
        *self->zone = sc_clip(value, self->min, self->max);
    }
};

//----------------------------------------------------------------------------
// Control allocator
//----------------------------------------------------------------------------

class ControlAllocator : public UI
{
public:
    ControlAllocator(Control* controls)
        : mControls(controls)
    { }

    // Layout widgets
    virtual void openTabBox(const char* label) { }
    virtual void openHorizontalBox(const char* label) { }
    virtual void openVerticalBox(const char* label) { }
    virtual void closeBox() { }

    // Active widgets
    virtual void addButton(const char* label, FAUSTFLOAT* zone)
    { addSimpleControl(zone); }
    virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
    { addSimpleControl(zone); }
    virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }
    virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }
    virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }

    // Passive widgets
    virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
    virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}

private:
    void addControl(Control::UpdateFunction updateFunction, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT /* step */)
    {
        Control* ctrl        = mControls++;
        ctrl->updateFunction = updateFunction;
        ctrl->zone           = zone;
        ctrl->min            = min;
        ctrl->max            = max;
    }
    void addSimpleControl(FAUSTFLOAT* zone)
    {
        addControl(Control::simpleUpdate, zone, 0.f, 0.f, 0.f);
    }
    void addBoundedControl(FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    {
        addControl(Control::boundedUpdate, zone, min, max, step);
    }

private:
    Control* mControls;
};

//----------------------------------------------------------------------------
// FAUST generated code
//----------------------------------------------------------------------------

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif 

#include <cmath>
#include <math.h>

float mydsp_faustpower2_f(float value) {
	return (value * value);
	
}

#ifndef FAUSTCLASS 
#define FAUSTCLASS mydsp
#endif
#ifdef __APPLE__ 
#define exp10f __exp10f
#define exp10 __exp10
#endif

class mydsp : public dsp {
	
 private:
	
	FAUSTFLOAT fCheckbox0;
	FAUSTFLOAT fHslider0;
	float fRec0[2];
	int fSamplingFreq;
	int iConst0;
	float fConst1;
	FAUSTFLOAT fHslider1;
	float fRec1[2];
	float fConst2;
	float fConst3;
	float fConst4;
	float fConst5;
	float fConst6;
	float fConst7;
	float fConst8;
	float fConst9;
	float fRec11[2];
	float fRec9[2];
	float fRec8[2];
	float fRec6[2];
	float fRec5[2];
	float fRec3[2];
	float fRec2[3];
	FAUSTFLOAT fHslider2;
	float fRec12[2];
	float fConst10;
	float fConst11;
	float fConst12;
	float fConst13;
	float fConst14;
	float fConst15;
	float fConst16;
	float fRec25[2];
	float fRec23[2];
	float fRec22[2];
	float fRec20[2];
	float fRec19[2];
	float fRec17[2];
	float fRec16[2];
	float fRec14[2];
	float fRec13[3];
	float fRec38[2];
	float fRec36[2];
	float fRec35[2];
	float fRec33[2];
	float fRec32[2];
	float fRec30[2];
	float fRec29[2];
	float fRec27[2];
	float fRec26[3];
	float fRec51[2];
	float fRec49[2];
	float fRec48[2];
	float fRec46[2];
	float fRec45[2];
	float fRec43[2];
	float fRec42[2];
	float fRec40[2];
	float fRec39[3];
	float fConst17;
	float fConst18;
	float fConst19;
	float fConst20;
	float fRec58[2];
	float fRec56[2];
	float fRec55[2];
	float fRec53[2];
	float fRec52[3];
	float fConst21;
	float fConst22;
	float fConst23;
	float fRec62[2];
	float fRec60[2];
	float fRec59[3];
	float fRec63[3];
	float fRec67[2];
	float fRec65[2];
	float fRec64[3];
	float fRec74[2];
	float fRec72[2];
	float fRec71[2];
	float fRec69[2];
	float fRec68[3];
	float fRec84[2];
	float fRec82[2];
	float fRec81[2];
	float fRec79[2];
	float fRec78[2];
	float fRec76[2];
	float fRec75[3];
	float fRec94[2];
	float fRec92[2];
	float fRec91[2];
	float fRec89[2];
	float fRec88[2];
	float fRec86[2];
	float fRec85[3];
	float fRec101[2];
	float fRec99[2];
	float fRec98[2];
	float fRec96[2];
	float fRec95[3];
	float fRec111[2];
	float fRec109[2];
	float fRec108[2];
	float fRec106[2];
	float fRec105[2];
	float fRec103[2];
	float fRec102[3];
	float fRec121[2];
	float fRec119[2];
	float fRec118[2];
	float fRec116[2];
	float fRec115[2];
	float fRec113[2];
	float fRec112[3];
	float fRec134[2];
	float fRec132[2];
	float fRec131[2];
	float fRec129[2];
	float fRec128[2];
	float fRec126[2];
	float fRec125[2];
	float fRec123[2];
	float fRec122[3];
	float fRec147[2];
	float fRec145[2];
	float fRec144[2];
	float fRec142[2];
	float fRec141[2];
	float fRec139[2];
	float fRec138[2];
	float fRec136[2];
	float fRec135[3];
	float fRec160[2];
	float fRec158[2];
	float fRec157[2];
	float fRec155[2];
	float fRec154[2];
	float fRec152[2];
	float fRec151[2];
	float fRec149[2];
	float fRec148[3];
	float fRec173[2];
	float fRec171[2];
	float fRec170[2];
	float fRec168[2];
	float fRec167[2];
	float fRec165[2];
	float fRec164[2];
	float fRec162[2];
	float fRec161[3];
	float fRec186[2];
	float fRec184[2];
	float fRec183[2];
	float fRec181[2];
	float fRec180[2];
	float fRec178[2];
	float fRec177[2];
	float fRec175[2];
	float fRec174[3];
	float fRec193[2];
	float fRec191[2];
	float fRec190[2];
	float fRec188[2];
	float fRec187[3];
	float fRec197[2];
	float fRec195[2];
	float fRec194[3];
	float fRec204[2];
	float fRec202[2];
	float fRec201[2];
	float fRec199[2];
	float fRec198[3];
	float fRec217[2];
	float fRec215[2];
	float fRec214[2];
	float fRec212[2];
	float fRec211[2];
	float fRec209[2];
	float fRec208[2];
	float fRec206[2];
	float fRec205[3];
	float fRec227[2];
	float fRec225[2];
	float fRec224[2];
	float fRec222[2];
	float fRec221[2];
	float fRec219[2];
	float fRec218[3];
	float fRec237[2];
	float fRec235[2];
	float fRec234[2];
	float fRec232[2];
	float fRec231[2];
	float fRec229[2];
	float fRec228[3];
	float fVec0[12];
	int iConst24;
	float fVec1[13];
	int iConst25;
	float fVec2[12];
	float fVec3[12];
	float fVec4[13];
	float fVec5[12];
	int IOTA;
	float fVec6[32];
	int iConst26;
	float fVec7[32];
	int iConst27;
	float fVec8[32];
	float fVec9[32];
	float fVec10[32];
	float fVec11[32];
	float fVec12[32];
	float fVec13[32];
	float fVec14[32];
	float fVec15[32];
	float fVec16[32];
	float fVec17[32];
	float fVec18[64];
	int iConst28;
	float fVec19[64];
	int iConst29;
	float fVec20[64];
	float fVec21[64];
	float fVec22[64];
	float fVec23[64];
	float fVec24[64];
	float fVec25[64];
	float fVec26[64];
	float fVec27[64];
	float fVec28[64];
	float fVec29[64];
	
 public:
	
	void metadata(Meta* m) { 
		m->declare("author", "AmbisonicDecoderToolkit");
		m->declare("copyright", "(c) Aaron J. Heller 2013");
		m->declare("license", "BSD 3-Clause License");
		m->declare("name", "SATOswD3o4");
		m->declare("version", "1.2");
	}

	virtual int getNumInputs() {
		return 25;
		
	}
	virtual int getNumOutputs() {
		return 31;
		
	}
	virtual int getInputRate(int channel) {
		int rate;
		switch (channel) {
			case 0: {
				rate = 1;
				break;
			}
			case 1: {
				rate = 1;
				break;
			}
			case 2: {
				rate = 1;
				break;
			}
			case 3: {
				rate = 1;
				break;
			}
			case 4: {
				rate = 1;
				break;
			}
			case 5: {
				rate = 1;
				break;
			}
			case 6: {
				rate = 1;
				break;
			}
			case 7: {
				rate = 1;
				break;
			}
			case 8: {
				rate = 1;
				break;
			}
			case 9: {
				rate = 1;
				break;
			}
			case 10: {
				rate = 1;
				break;
			}
			case 11: {
				rate = 1;
				break;
			}
			case 12: {
				rate = 1;
				break;
			}
			case 13: {
				rate = 1;
				break;
			}
			case 14: {
				rate = 1;
				break;
			}
			case 15: {
				rate = 1;
				break;
			}
			case 16: {
				rate = 1;
				break;
			}
			case 17: {
				rate = 1;
				break;
			}
			case 18: {
				rate = 1;
				break;
			}
			case 19: {
				rate = 1;
				break;
			}
			case 20: {
				rate = 1;
				break;
			}
			case 21: {
				rate = 1;
				break;
			}
			case 22: {
				rate = 1;
				break;
			}
			case 23: {
				rate = 1;
				break;
			}
			case 24: {
				rate = 1;
				break;
			}
			default: {
				rate = -1;
				break;
			}
			
		}
		return rate;
		
	}
	virtual int getOutputRate(int channel) {
		int rate;
		switch (channel) {
			case 0: {
				rate = 1;
				break;
			}
			case 1: {
				rate = 1;
				break;
			}
			case 2: {
				rate = 1;
				break;
			}
			case 3: {
				rate = 1;
				break;
			}
			case 4: {
				rate = 1;
				break;
			}
			case 5: {
				rate = 1;
				break;
			}
			case 6: {
				rate = 1;
				break;
			}
			case 7: {
				rate = 1;
				break;
			}
			case 8: {
				rate = 1;
				break;
			}
			case 9: {
				rate = 1;
				break;
			}
			case 10: {
				rate = 1;
				break;
			}
			case 11: {
				rate = 1;
				break;
			}
			case 12: {
				rate = 1;
				break;
			}
			case 13: {
				rate = 1;
				break;
			}
			case 14: {
				rate = 1;
				break;
			}
			case 15: {
				rate = 1;
				break;
			}
			case 16: {
				rate = 1;
				break;
			}
			case 17: {
				rate = 1;
				break;
			}
			case 18: {
				rate = 1;
				break;
			}
			case 19: {
				rate = 1;
				break;
			}
			case 20: {
				rate = 1;
				break;
			}
			case 21: {
				rate = 1;
				break;
			}
			case 22: {
				rate = 1;
				break;
			}
			case 23: {
				rate = 1;
				break;
			}
			case 24: {
				rate = 1;
				break;
			}
			case 25: {
				rate = 1;
				break;
			}
			case 26: {
				rate = 1;
				break;
			}
			case 27: {
				rate = 1;
				break;
			}
			case 28: {
				rate = 1;
				break;
			}
			case 29: {
				rate = 1;
				break;
			}
			case 30: {
				rate = 1;
				break;
			}
			default: {
				rate = -1;
				break;
			}
			
		}
		return rate;
		
	}
	
	static void classInit(int samplingFreq) {
		
	}
	
	virtual void instanceConstants(int samplingFreq) {
		fSamplingFreq = samplingFreq;
		iConst0 = min(192000, max(1, fSamplingFreq));
		fConst1 = (3.14159274f / float(iConst0));
		fConst2 = float(iConst0);
		fConst3 = ((44.4325409f / fConst2) + 1.0f);
		fConst4 = (0.0f - (88.8650818f / (fConst2 * fConst3)));
		fConst5 = ((((2364.84619f / fConst2) + 70.3710632f) / fConst2) + 1.0f);
		fConst6 = (1.0f / (fConst3 * fConst5));
		fConst7 = mydsp_faustpower2_f(fConst2);
		fConst8 = (0.0f - (9459.38477f / (fConst7 * fConst5)));
		fConst9 = (0.0f - (((9459.38477f / fConst2) + 140.742126f) / (fConst2 * fConst5)));
		fConst10 = ((((3346.26953f / fConst2) + 110.831802f) / fConst2) + 1.0f);
		fConst11 = (0.0f - (13385.0781f / (fConst7 * fConst10)));
		fConst12 = (0.0f - (((13385.0781f / fConst2) + 221.663605f) / (fConst2 * fConst10)));
		fConst13 = ((((4205.76904f / fConst2) + 80.5075302f) / fConst2) + 1.0f);
		fConst14 = (1.0f / (fConst10 * fConst13));
		fConst15 = (0.0f - (16823.0762f / (fConst7 * fConst13)));
		fConst16 = (0.0f - (((16823.0762f / fConst2) + 161.01506f) / (fConst2 * fConst13)));
		fConst17 = ((((1098.32227f / fConst2) + 57.4018021f) / fConst2) + 1.0f);
		fConst18 = (1.0f / fConst17);
		fConst19 = (0.0f - (4393.28906f / (fConst7 * fConst17)));
		fConst20 = (0.0f - (((4393.28906f / fConst2) + 114.803604f) / (fConst2 * fConst17)));
		fConst21 = ((19.133934f / fConst2) + 1.0f);
		fConst22 = (1.0f / fConst21);
		fConst23 = (0.0f - (38.267868f / (fConst2 * fConst21)));
		iConst24 = int(((5.24453171e-05f * float(iConst0)) + 0.5f));
		iConst25 = int(((5.8272577e-05f * float(iConst0)) + 0.5f));
		iConst26 = int(((0.000145681435f * float(iConst0)) + 0.5f));
		iConst27 = int(((0.000134026923f * float(iConst0)) + 0.5f));
		iConst28 = int(((0.000262226589f * float(iConst0)) + 0.5f));
		iConst29 = int(((0.000256399333f * float(iConst0)) + 0.5f));
		
	}
	
	virtual void instanceResetUserInterface() {
		fCheckbox0 = FAUSTFLOAT(0.0f);
		fHslider0 = FAUSTFLOAT(-10.0f);
		fHslider1 = FAUSTFLOAT(400.0f);
		fHslider2 = FAUSTFLOAT(0.0f);
		
	}
	
	virtual void instanceClear() {
		for (int l0 = 0; (l0 < 2); l0 = (l0 + 1)) {
			fRec0[l0] = 0.0f;
			
		}
		for (int l1 = 0; (l1 < 2); l1 = (l1 + 1)) {
			fRec1[l1] = 0.0f;
			
		}
		for (int l2 = 0; (l2 < 2); l2 = (l2 + 1)) {
			fRec11[l2] = 0.0f;
			
		}
		for (int l3 = 0; (l3 < 2); l3 = (l3 + 1)) {
			fRec9[l3] = 0.0f;
			
		}
		for (int l4 = 0; (l4 < 2); l4 = (l4 + 1)) {
			fRec8[l4] = 0.0f;
			
		}
		for (int l5 = 0; (l5 < 2); l5 = (l5 + 1)) {
			fRec6[l5] = 0.0f;
			
		}
		for (int l6 = 0; (l6 < 2); l6 = (l6 + 1)) {
			fRec5[l6] = 0.0f;
			
		}
		for (int l7 = 0; (l7 < 2); l7 = (l7 + 1)) {
			fRec3[l7] = 0.0f;
			
		}
		for (int l8 = 0; (l8 < 3); l8 = (l8 + 1)) {
			fRec2[l8] = 0.0f;
			
		}
		for (int l9 = 0; (l9 < 2); l9 = (l9 + 1)) {
			fRec12[l9] = 0.0f;
			
		}
		for (int l10 = 0; (l10 < 2); l10 = (l10 + 1)) {
			fRec25[l10] = 0.0f;
			
		}
		for (int l11 = 0; (l11 < 2); l11 = (l11 + 1)) {
			fRec23[l11] = 0.0f;
			
		}
		for (int l12 = 0; (l12 < 2); l12 = (l12 + 1)) {
			fRec22[l12] = 0.0f;
			
		}
		for (int l13 = 0; (l13 < 2); l13 = (l13 + 1)) {
			fRec20[l13] = 0.0f;
			
		}
		for (int l14 = 0; (l14 < 2); l14 = (l14 + 1)) {
			fRec19[l14] = 0.0f;
			
		}
		for (int l15 = 0; (l15 < 2); l15 = (l15 + 1)) {
			fRec17[l15] = 0.0f;
			
		}
		for (int l16 = 0; (l16 < 2); l16 = (l16 + 1)) {
			fRec16[l16] = 0.0f;
			
		}
		for (int l17 = 0; (l17 < 2); l17 = (l17 + 1)) {
			fRec14[l17] = 0.0f;
			
		}
		for (int l18 = 0; (l18 < 3); l18 = (l18 + 1)) {
			fRec13[l18] = 0.0f;
			
		}
		for (int l19 = 0; (l19 < 2); l19 = (l19 + 1)) {
			fRec38[l19] = 0.0f;
			
		}
		for (int l20 = 0; (l20 < 2); l20 = (l20 + 1)) {
			fRec36[l20] = 0.0f;
			
		}
		for (int l21 = 0; (l21 < 2); l21 = (l21 + 1)) {
			fRec35[l21] = 0.0f;
			
		}
		for (int l22 = 0; (l22 < 2); l22 = (l22 + 1)) {
			fRec33[l22] = 0.0f;
			
		}
		for (int l23 = 0; (l23 < 2); l23 = (l23 + 1)) {
			fRec32[l23] = 0.0f;
			
		}
		for (int l24 = 0; (l24 < 2); l24 = (l24 + 1)) {
			fRec30[l24] = 0.0f;
			
		}
		for (int l25 = 0; (l25 < 2); l25 = (l25 + 1)) {
			fRec29[l25] = 0.0f;
			
		}
		for (int l26 = 0; (l26 < 2); l26 = (l26 + 1)) {
			fRec27[l26] = 0.0f;
			
		}
		for (int l27 = 0; (l27 < 3); l27 = (l27 + 1)) {
			fRec26[l27] = 0.0f;
			
		}
		for (int l28 = 0; (l28 < 2); l28 = (l28 + 1)) {
			fRec51[l28] = 0.0f;
			
		}
		for (int l29 = 0; (l29 < 2); l29 = (l29 + 1)) {
			fRec49[l29] = 0.0f;
			
		}
		for (int l30 = 0; (l30 < 2); l30 = (l30 + 1)) {
			fRec48[l30] = 0.0f;
			
		}
		for (int l31 = 0; (l31 < 2); l31 = (l31 + 1)) {
			fRec46[l31] = 0.0f;
			
		}
		for (int l32 = 0; (l32 < 2); l32 = (l32 + 1)) {
			fRec45[l32] = 0.0f;
			
		}
		for (int l33 = 0; (l33 < 2); l33 = (l33 + 1)) {
			fRec43[l33] = 0.0f;
			
		}
		for (int l34 = 0; (l34 < 2); l34 = (l34 + 1)) {
			fRec42[l34] = 0.0f;
			
		}
		for (int l35 = 0; (l35 < 2); l35 = (l35 + 1)) {
			fRec40[l35] = 0.0f;
			
		}
		for (int l36 = 0; (l36 < 3); l36 = (l36 + 1)) {
			fRec39[l36] = 0.0f;
			
		}
		for (int l37 = 0; (l37 < 2); l37 = (l37 + 1)) {
			fRec58[l37] = 0.0f;
			
		}
		for (int l38 = 0; (l38 < 2); l38 = (l38 + 1)) {
			fRec56[l38] = 0.0f;
			
		}
		for (int l39 = 0; (l39 < 2); l39 = (l39 + 1)) {
			fRec55[l39] = 0.0f;
			
		}
		for (int l40 = 0; (l40 < 2); l40 = (l40 + 1)) {
			fRec53[l40] = 0.0f;
			
		}
		for (int l41 = 0; (l41 < 3); l41 = (l41 + 1)) {
			fRec52[l41] = 0.0f;
			
		}
		for (int l42 = 0; (l42 < 2); l42 = (l42 + 1)) {
			fRec62[l42] = 0.0f;
			
		}
		for (int l43 = 0; (l43 < 2); l43 = (l43 + 1)) {
			fRec60[l43] = 0.0f;
			
		}
		for (int l44 = 0; (l44 < 3); l44 = (l44 + 1)) {
			fRec59[l44] = 0.0f;
			
		}
		for (int l45 = 0; (l45 < 3); l45 = (l45 + 1)) {
			fRec63[l45] = 0.0f;
			
		}
		for (int l46 = 0; (l46 < 2); l46 = (l46 + 1)) {
			fRec67[l46] = 0.0f;
			
		}
		for (int l47 = 0; (l47 < 2); l47 = (l47 + 1)) {
			fRec65[l47] = 0.0f;
			
		}
		for (int l48 = 0; (l48 < 3); l48 = (l48 + 1)) {
			fRec64[l48] = 0.0f;
			
		}
		for (int l49 = 0; (l49 < 2); l49 = (l49 + 1)) {
			fRec74[l49] = 0.0f;
			
		}
		for (int l50 = 0; (l50 < 2); l50 = (l50 + 1)) {
			fRec72[l50] = 0.0f;
			
		}
		for (int l51 = 0; (l51 < 2); l51 = (l51 + 1)) {
			fRec71[l51] = 0.0f;
			
		}
		for (int l52 = 0; (l52 < 2); l52 = (l52 + 1)) {
			fRec69[l52] = 0.0f;
			
		}
		for (int l53 = 0; (l53 < 3); l53 = (l53 + 1)) {
			fRec68[l53] = 0.0f;
			
		}
		for (int l54 = 0; (l54 < 2); l54 = (l54 + 1)) {
			fRec84[l54] = 0.0f;
			
		}
		for (int l55 = 0; (l55 < 2); l55 = (l55 + 1)) {
			fRec82[l55] = 0.0f;
			
		}
		for (int l56 = 0; (l56 < 2); l56 = (l56 + 1)) {
			fRec81[l56] = 0.0f;
			
		}
		for (int l57 = 0; (l57 < 2); l57 = (l57 + 1)) {
			fRec79[l57] = 0.0f;
			
		}
		for (int l58 = 0; (l58 < 2); l58 = (l58 + 1)) {
			fRec78[l58] = 0.0f;
			
		}
		for (int l59 = 0; (l59 < 2); l59 = (l59 + 1)) {
			fRec76[l59] = 0.0f;
			
		}
		for (int l60 = 0; (l60 < 3); l60 = (l60 + 1)) {
			fRec75[l60] = 0.0f;
			
		}
		for (int l61 = 0; (l61 < 2); l61 = (l61 + 1)) {
			fRec94[l61] = 0.0f;
			
		}
		for (int l62 = 0; (l62 < 2); l62 = (l62 + 1)) {
			fRec92[l62] = 0.0f;
			
		}
		for (int l63 = 0; (l63 < 2); l63 = (l63 + 1)) {
			fRec91[l63] = 0.0f;
			
		}
		for (int l64 = 0; (l64 < 2); l64 = (l64 + 1)) {
			fRec89[l64] = 0.0f;
			
		}
		for (int l65 = 0; (l65 < 2); l65 = (l65 + 1)) {
			fRec88[l65] = 0.0f;
			
		}
		for (int l66 = 0; (l66 < 2); l66 = (l66 + 1)) {
			fRec86[l66] = 0.0f;
			
		}
		for (int l67 = 0; (l67 < 3); l67 = (l67 + 1)) {
			fRec85[l67] = 0.0f;
			
		}
		for (int l68 = 0; (l68 < 2); l68 = (l68 + 1)) {
			fRec101[l68] = 0.0f;
			
		}
		for (int l69 = 0; (l69 < 2); l69 = (l69 + 1)) {
			fRec99[l69] = 0.0f;
			
		}
		for (int l70 = 0; (l70 < 2); l70 = (l70 + 1)) {
			fRec98[l70] = 0.0f;
			
		}
		for (int l71 = 0; (l71 < 2); l71 = (l71 + 1)) {
			fRec96[l71] = 0.0f;
			
		}
		for (int l72 = 0; (l72 < 3); l72 = (l72 + 1)) {
			fRec95[l72] = 0.0f;
			
		}
		for (int l73 = 0; (l73 < 2); l73 = (l73 + 1)) {
			fRec111[l73] = 0.0f;
			
		}
		for (int l74 = 0; (l74 < 2); l74 = (l74 + 1)) {
			fRec109[l74] = 0.0f;
			
		}
		for (int l75 = 0; (l75 < 2); l75 = (l75 + 1)) {
			fRec108[l75] = 0.0f;
			
		}
		for (int l76 = 0; (l76 < 2); l76 = (l76 + 1)) {
			fRec106[l76] = 0.0f;
			
		}
		for (int l77 = 0; (l77 < 2); l77 = (l77 + 1)) {
			fRec105[l77] = 0.0f;
			
		}
		for (int l78 = 0; (l78 < 2); l78 = (l78 + 1)) {
			fRec103[l78] = 0.0f;
			
		}
		for (int l79 = 0; (l79 < 3); l79 = (l79 + 1)) {
			fRec102[l79] = 0.0f;
			
		}
		for (int l80 = 0; (l80 < 2); l80 = (l80 + 1)) {
			fRec121[l80] = 0.0f;
			
		}
		for (int l81 = 0; (l81 < 2); l81 = (l81 + 1)) {
			fRec119[l81] = 0.0f;
			
		}
		for (int l82 = 0; (l82 < 2); l82 = (l82 + 1)) {
			fRec118[l82] = 0.0f;
			
		}
		for (int l83 = 0; (l83 < 2); l83 = (l83 + 1)) {
			fRec116[l83] = 0.0f;
			
		}
		for (int l84 = 0; (l84 < 2); l84 = (l84 + 1)) {
			fRec115[l84] = 0.0f;
			
		}
		for (int l85 = 0; (l85 < 2); l85 = (l85 + 1)) {
			fRec113[l85] = 0.0f;
			
		}
		for (int l86 = 0; (l86 < 3); l86 = (l86 + 1)) {
			fRec112[l86] = 0.0f;
			
		}
		for (int l87 = 0; (l87 < 2); l87 = (l87 + 1)) {
			fRec134[l87] = 0.0f;
			
		}
		for (int l88 = 0; (l88 < 2); l88 = (l88 + 1)) {
			fRec132[l88] = 0.0f;
			
		}
		for (int l89 = 0; (l89 < 2); l89 = (l89 + 1)) {
			fRec131[l89] = 0.0f;
			
		}
		for (int l90 = 0; (l90 < 2); l90 = (l90 + 1)) {
			fRec129[l90] = 0.0f;
			
		}
		for (int l91 = 0; (l91 < 2); l91 = (l91 + 1)) {
			fRec128[l91] = 0.0f;
			
		}
		for (int l92 = 0; (l92 < 2); l92 = (l92 + 1)) {
			fRec126[l92] = 0.0f;
			
		}
		for (int l93 = 0; (l93 < 2); l93 = (l93 + 1)) {
			fRec125[l93] = 0.0f;
			
		}
		for (int l94 = 0; (l94 < 2); l94 = (l94 + 1)) {
			fRec123[l94] = 0.0f;
			
		}
		for (int l95 = 0; (l95 < 3); l95 = (l95 + 1)) {
			fRec122[l95] = 0.0f;
			
		}
		for (int l96 = 0; (l96 < 2); l96 = (l96 + 1)) {
			fRec147[l96] = 0.0f;
			
		}
		for (int l97 = 0; (l97 < 2); l97 = (l97 + 1)) {
			fRec145[l97] = 0.0f;
			
		}
		for (int l98 = 0; (l98 < 2); l98 = (l98 + 1)) {
			fRec144[l98] = 0.0f;
			
		}
		for (int l99 = 0; (l99 < 2); l99 = (l99 + 1)) {
			fRec142[l99] = 0.0f;
			
		}
		for (int l100 = 0; (l100 < 2); l100 = (l100 + 1)) {
			fRec141[l100] = 0.0f;
			
		}
		for (int l101 = 0; (l101 < 2); l101 = (l101 + 1)) {
			fRec139[l101] = 0.0f;
			
		}
		for (int l102 = 0; (l102 < 2); l102 = (l102 + 1)) {
			fRec138[l102] = 0.0f;
			
		}
		for (int l103 = 0; (l103 < 2); l103 = (l103 + 1)) {
			fRec136[l103] = 0.0f;
			
		}
		for (int l104 = 0; (l104 < 3); l104 = (l104 + 1)) {
			fRec135[l104] = 0.0f;
			
		}
		for (int l105 = 0; (l105 < 2); l105 = (l105 + 1)) {
			fRec160[l105] = 0.0f;
			
		}
		for (int l106 = 0; (l106 < 2); l106 = (l106 + 1)) {
			fRec158[l106] = 0.0f;
			
		}
		for (int l107 = 0; (l107 < 2); l107 = (l107 + 1)) {
			fRec157[l107] = 0.0f;
			
		}
		for (int l108 = 0; (l108 < 2); l108 = (l108 + 1)) {
			fRec155[l108] = 0.0f;
			
		}
		for (int l109 = 0; (l109 < 2); l109 = (l109 + 1)) {
			fRec154[l109] = 0.0f;
			
		}
		for (int l110 = 0; (l110 < 2); l110 = (l110 + 1)) {
			fRec152[l110] = 0.0f;
			
		}
		for (int l111 = 0; (l111 < 2); l111 = (l111 + 1)) {
			fRec151[l111] = 0.0f;
			
		}
		for (int l112 = 0; (l112 < 2); l112 = (l112 + 1)) {
			fRec149[l112] = 0.0f;
			
		}
		for (int l113 = 0; (l113 < 3); l113 = (l113 + 1)) {
			fRec148[l113] = 0.0f;
			
		}
		for (int l114 = 0; (l114 < 2); l114 = (l114 + 1)) {
			fRec173[l114] = 0.0f;
			
		}
		for (int l115 = 0; (l115 < 2); l115 = (l115 + 1)) {
			fRec171[l115] = 0.0f;
			
		}
		for (int l116 = 0; (l116 < 2); l116 = (l116 + 1)) {
			fRec170[l116] = 0.0f;
			
		}
		for (int l117 = 0; (l117 < 2); l117 = (l117 + 1)) {
			fRec168[l117] = 0.0f;
			
		}
		for (int l118 = 0; (l118 < 2); l118 = (l118 + 1)) {
			fRec167[l118] = 0.0f;
			
		}
		for (int l119 = 0; (l119 < 2); l119 = (l119 + 1)) {
			fRec165[l119] = 0.0f;
			
		}
		for (int l120 = 0; (l120 < 2); l120 = (l120 + 1)) {
			fRec164[l120] = 0.0f;
			
		}
		for (int l121 = 0; (l121 < 2); l121 = (l121 + 1)) {
			fRec162[l121] = 0.0f;
			
		}
		for (int l122 = 0; (l122 < 3); l122 = (l122 + 1)) {
			fRec161[l122] = 0.0f;
			
		}
		for (int l123 = 0; (l123 < 2); l123 = (l123 + 1)) {
			fRec186[l123] = 0.0f;
			
		}
		for (int l124 = 0; (l124 < 2); l124 = (l124 + 1)) {
			fRec184[l124] = 0.0f;
			
		}
		for (int l125 = 0; (l125 < 2); l125 = (l125 + 1)) {
			fRec183[l125] = 0.0f;
			
		}
		for (int l126 = 0; (l126 < 2); l126 = (l126 + 1)) {
			fRec181[l126] = 0.0f;
			
		}
		for (int l127 = 0; (l127 < 2); l127 = (l127 + 1)) {
			fRec180[l127] = 0.0f;
			
		}
		for (int l128 = 0; (l128 < 2); l128 = (l128 + 1)) {
			fRec178[l128] = 0.0f;
			
		}
		for (int l129 = 0; (l129 < 2); l129 = (l129 + 1)) {
			fRec177[l129] = 0.0f;
			
		}
		for (int l130 = 0; (l130 < 2); l130 = (l130 + 1)) {
			fRec175[l130] = 0.0f;
			
		}
		for (int l131 = 0; (l131 < 3); l131 = (l131 + 1)) {
			fRec174[l131] = 0.0f;
			
		}
		for (int l132 = 0; (l132 < 2); l132 = (l132 + 1)) {
			fRec193[l132] = 0.0f;
			
		}
		for (int l133 = 0; (l133 < 2); l133 = (l133 + 1)) {
			fRec191[l133] = 0.0f;
			
		}
		for (int l134 = 0; (l134 < 2); l134 = (l134 + 1)) {
			fRec190[l134] = 0.0f;
			
		}
		for (int l135 = 0; (l135 < 2); l135 = (l135 + 1)) {
			fRec188[l135] = 0.0f;
			
		}
		for (int l136 = 0; (l136 < 3); l136 = (l136 + 1)) {
			fRec187[l136] = 0.0f;
			
		}
		for (int l137 = 0; (l137 < 2); l137 = (l137 + 1)) {
			fRec197[l137] = 0.0f;
			
		}
		for (int l138 = 0; (l138 < 2); l138 = (l138 + 1)) {
			fRec195[l138] = 0.0f;
			
		}
		for (int l139 = 0; (l139 < 3); l139 = (l139 + 1)) {
			fRec194[l139] = 0.0f;
			
		}
		for (int l140 = 0; (l140 < 2); l140 = (l140 + 1)) {
			fRec204[l140] = 0.0f;
			
		}
		for (int l141 = 0; (l141 < 2); l141 = (l141 + 1)) {
			fRec202[l141] = 0.0f;
			
		}
		for (int l142 = 0; (l142 < 2); l142 = (l142 + 1)) {
			fRec201[l142] = 0.0f;
			
		}
		for (int l143 = 0; (l143 < 2); l143 = (l143 + 1)) {
			fRec199[l143] = 0.0f;
			
		}
		for (int l144 = 0; (l144 < 3); l144 = (l144 + 1)) {
			fRec198[l144] = 0.0f;
			
		}
		for (int l145 = 0; (l145 < 2); l145 = (l145 + 1)) {
			fRec217[l145] = 0.0f;
			
		}
		for (int l146 = 0; (l146 < 2); l146 = (l146 + 1)) {
			fRec215[l146] = 0.0f;
			
		}
		for (int l147 = 0; (l147 < 2); l147 = (l147 + 1)) {
			fRec214[l147] = 0.0f;
			
		}
		for (int l148 = 0; (l148 < 2); l148 = (l148 + 1)) {
			fRec212[l148] = 0.0f;
			
		}
		for (int l149 = 0; (l149 < 2); l149 = (l149 + 1)) {
			fRec211[l149] = 0.0f;
			
		}
		for (int l150 = 0; (l150 < 2); l150 = (l150 + 1)) {
			fRec209[l150] = 0.0f;
			
		}
		for (int l151 = 0; (l151 < 2); l151 = (l151 + 1)) {
			fRec208[l151] = 0.0f;
			
		}
		for (int l152 = 0; (l152 < 2); l152 = (l152 + 1)) {
			fRec206[l152] = 0.0f;
			
		}
		for (int l153 = 0; (l153 < 3); l153 = (l153 + 1)) {
			fRec205[l153] = 0.0f;
			
		}
		for (int l154 = 0; (l154 < 2); l154 = (l154 + 1)) {
			fRec227[l154] = 0.0f;
			
		}
		for (int l155 = 0; (l155 < 2); l155 = (l155 + 1)) {
			fRec225[l155] = 0.0f;
			
		}
		for (int l156 = 0; (l156 < 2); l156 = (l156 + 1)) {
			fRec224[l156] = 0.0f;
			
		}
		for (int l157 = 0; (l157 < 2); l157 = (l157 + 1)) {
			fRec222[l157] = 0.0f;
			
		}
		for (int l158 = 0; (l158 < 2); l158 = (l158 + 1)) {
			fRec221[l158] = 0.0f;
			
		}
		for (int l159 = 0; (l159 < 2); l159 = (l159 + 1)) {
			fRec219[l159] = 0.0f;
			
		}
		for (int l160 = 0; (l160 < 3); l160 = (l160 + 1)) {
			fRec218[l160] = 0.0f;
			
		}
		for (int l161 = 0; (l161 < 2); l161 = (l161 + 1)) {
			fRec237[l161] = 0.0f;
			
		}
		for (int l162 = 0; (l162 < 2); l162 = (l162 + 1)) {
			fRec235[l162] = 0.0f;
			
		}
		for (int l163 = 0; (l163 < 2); l163 = (l163 + 1)) {
			fRec234[l163] = 0.0f;
			
		}
		for (int l164 = 0; (l164 < 2); l164 = (l164 + 1)) {
			fRec232[l164] = 0.0f;
			
		}
		for (int l165 = 0; (l165 < 2); l165 = (l165 + 1)) {
			fRec231[l165] = 0.0f;
			
		}
		for (int l166 = 0; (l166 < 2); l166 = (l166 + 1)) {
			fRec229[l166] = 0.0f;
			
		}
		for (int l167 = 0; (l167 < 3); l167 = (l167 + 1)) {
			fRec228[l167] = 0.0f;
			
		}
		for (int l168 = 0; (l168 < 12); l168 = (l168 + 1)) {
			fVec0[l168] = 0.0f;
			
		}
		for (int l169 = 0; (l169 < 13); l169 = (l169 + 1)) {
			fVec1[l169] = 0.0f;
			
		}
		for (int l170 = 0; (l170 < 12); l170 = (l170 + 1)) {
			fVec2[l170] = 0.0f;
			
		}
		for (int l171 = 0; (l171 < 12); l171 = (l171 + 1)) {
			fVec3[l171] = 0.0f;
			
		}
		for (int l172 = 0; (l172 < 13); l172 = (l172 + 1)) {
			fVec4[l172] = 0.0f;
			
		}
		for (int l173 = 0; (l173 < 12); l173 = (l173 + 1)) {
			fVec5[l173] = 0.0f;
			
		}
		IOTA = 0;
		for (int l174 = 0; (l174 < 32); l174 = (l174 + 1)) {
			fVec6[l174] = 0.0f;
			
		}
		for (int l175 = 0; (l175 < 32); l175 = (l175 + 1)) {
			fVec7[l175] = 0.0f;
			
		}
		for (int l176 = 0; (l176 < 32); l176 = (l176 + 1)) {
			fVec8[l176] = 0.0f;
			
		}
		for (int l177 = 0; (l177 < 32); l177 = (l177 + 1)) {
			fVec9[l177] = 0.0f;
			
		}
		for (int l178 = 0; (l178 < 32); l178 = (l178 + 1)) {
			fVec10[l178] = 0.0f;
			
		}
		for (int l179 = 0; (l179 < 32); l179 = (l179 + 1)) {
			fVec11[l179] = 0.0f;
			
		}
		for (int l180 = 0; (l180 < 32); l180 = (l180 + 1)) {
			fVec12[l180] = 0.0f;
			
		}
		for (int l181 = 0; (l181 < 32); l181 = (l181 + 1)) {
			fVec13[l181] = 0.0f;
			
		}
		for (int l182 = 0; (l182 < 32); l182 = (l182 + 1)) {
			fVec14[l182] = 0.0f;
			
		}
		for (int l183 = 0; (l183 < 32); l183 = (l183 + 1)) {
			fVec15[l183] = 0.0f;
			
		}
		for (int l184 = 0; (l184 < 32); l184 = (l184 + 1)) {
			fVec16[l184] = 0.0f;
			
		}
		for (int l185 = 0; (l185 < 32); l185 = (l185 + 1)) {
			fVec17[l185] = 0.0f;
			
		}
		for (int l186 = 0; (l186 < 64); l186 = (l186 + 1)) {
			fVec18[l186] = 0.0f;
			
		}
		for (int l187 = 0; (l187 < 64); l187 = (l187 + 1)) {
			fVec19[l187] = 0.0f;
			
		}
		for (int l188 = 0; (l188 < 64); l188 = (l188 + 1)) {
			fVec20[l188] = 0.0f;
			
		}
		for (int l189 = 0; (l189 < 64); l189 = (l189 + 1)) {
			fVec21[l189] = 0.0f;
			
		}
		for (int l190 = 0; (l190 < 64); l190 = (l190 + 1)) {
			fVec22[l190] = 0.0f;
			
		}
		for (int l191 = 0; (l191 < 64); l191 = (l191 + 1)) {
			fVec23[l191] = 0.0f;
			
		}
		for (int l192 = 0; (l192 < 64); l192 = (l192 + 1)) {
			fVec24[l192] = 0.0f;
			
		}
		for (int l193 = 0; (l193 < 64); l193 = (l193 + 1)) {
			fVec25[l193] = 0.0f;
			
		}
		for (int l194 = 0; (l194 < 64); l194 = (l194 + 1)) {
			fVec26[l194] = 0.0f;
			
		}
		for (int l195 = 0; (l195 < 64); l195 = (l195 + 1)) {
			fVec27[l195] = 0.0f;
			
		}
		for (int l196 = 0; (l196 < 64); l196 = (l196 + 1)) {
			fVec28[l196] = 0.0f;
			
		}
		for (int l197 = 0; (l197 < 64); l197 = (l197 + 1)) {
			fVec29[l197] = 0.0f;
			
		}
		
	}
	
	virtual void init(int samplingFreq) {
		classInit(samplingFreq);
		instanceInit(samplingFreq);
	}
	virtual void instanceInit(int samplingFreq) {
		instanceConstants(samplingFreq);
		instanceResetUserInterface();
		instanceClear();
	}
	
	virtual mydsp* clone() {
		return new mydsp();
	}
	virtual int getSampleRate() {
		return fSamplingFreq;
		
	}
	
	virtual void buildUserInterface(UI* ui_interface) {
		ui_interface->openVerticalBox("SATOswD3o4");
		ui_interface->declare(&fHslider0, "unit", "dB");
		ui_interface->addHorizontalSlider("gain", &fHslider0, -10.0f, -30.0f, 10.0f, 1.0f);
		ui_interface->declare(&fHslider2, "unit", "dB");
		ui_interface->addHorizontalSlider("lf/hf", &fHslider2, 0.0f, -3.0f, 3.0f, 0.100000001f);
		ui_interface->addCheckButton("mute", &fCheckbox0);
		ui_interface->declare(&fHslider1, "unit", "Hz");
		ui_interface->addHorizontalSlider("xover", &fHslider1, 400.0f, 200.0f, 800.0f, 20.0f);
		ui_interface->closeBox();
		
	}
	
	virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) {
		FAUSTFLOAT* input0 = inputs[0];
		FAUSTFLOAT* input1 = inputs[1];
		FAUSTFLOAT* input2 = inputs[2];
		FAUSTFLOAT* input3 = inputs[3];
		FAUSTFLOAT* input4 = inputs[4];
		FAUSTFLOAT* input5 = inputs[5];
		FAUSTFLOAT* input6 = inputs[6];
		FAUSTFLOAT* input7 = inputs[7];
		FAUSTFLOAT* input8 = inputs[8];
		FAUSTFLOAT* input9 = inputs[9];
		FAUSTFLOAT* input10 = inputs[10];
		FAUSTFLOAT* input11 = inputs[11];
		FAUSTFLOAT* input12 = inputs[12];
		FAUSTFLOAT* input13 = inputs[13];
		FAUSTFLOAT* input14 = inputs[14];
		FAUSTFLOAT* input15 = inputs[15];
		FAUSTFLOAT* input16 = inputs[16];
		FAUSTFLOAT* input17 = inputs[17];
		FAUSTFLOAT* input18 = inputs[18];
		FAUSTFLOAT* input19 = inputs[19];
		FAUSTFLOAT* input20 = inputs[20];
		FAUSTFLOAT* input21 = inputs[21];
		FAUSTFLOAT* input22 = inputs[22];
		FAUSTFLOAT* input23 = inputs[23];
		FAUSTFLOAT* input24 = inputs[24];
		FAUSTFLOAT* output0 = outputs[0];
		FAUSTFLOAT* output1 = outputs[1];
		FAUSTFLOAT* output2 = outputs[2];
		FAUSTFLOAT* output3 = outputs[3];
		FAUSTFLOAT* output4 = outputs[4];
		FAUSTFLOAT* output5 = outputs[5];
		FAUSTFLOAT* output6 = outputs[6];
		FAUSTFLOAT* output7 = outputs[7];
		FAUSTFLOAT* output8 = outputs[8];
		FAUSTFLOAT* output9 = outputs[9];
		FAUSTFLOAT* output10 = outputs[10];
		FAUSTFLOAT* output11 = outputs[11];
		FAUSTFLOAT* output12 = outputs[12];
		FAUSTFLOAT* output13 = outputs[13];
		FAUSTFLOAT* output14 = outputs[14];
		FAUSTFLOAT* output15 = outputs[15];
		FAUSTFLOAT* output16 = outputs[16];
		FAUSTFLOAT* output17 = outputs[17];
		FAUSTFLOAT* output18 = outputs[18];
		FAUSTFLOAT* output19 = outputs[19];
		FAUSTFLOAT* output20 = outputs[20];
		FAUSTFLOAT* output21 = outputs[21];
		FAUSTFLOAT* output22 = outputs[22];
		FAUSTFLOAT* output23 = outputs[23];
		FAUSTFLOAT* output24 = outputs[24];
		FAUSTFLOAT* output25 = outputs[25];
		FAUSTFLOAT* output26 = outputs[26];
		FAUSTFLOAT* output27 = outputs[27];
		FAUSTFLOAT* output28 = outputs[28];
		FAUSTFLOAT* output29 = outputs[29];
		FAUSTFLOAT* output30 = outputs[30];
		float fSlow0 = (0.00100000005f * (float((float(fCheckbox0) < 0.5f)) * powf(10.0f, (0.0500000007f * float(fHslider0)))));
		float fSlow1 = (0.00100000005f * float(fHslider1));
		float fSlow2 = (0.00100000005f * powf(10.0f, (0.0500000007f * float(fHslider2))));
		for (int i = 0; (i < count); i = (i + 1)) {
			fRec0[0] = (fSlow0 + (0.999000013f * fRec0[1]));
			fRec1[0] = (fSlow1 + (0.999000013f * fRec1[1]));
			float fTemp0 = tanf((fConst1 * fRec1[0]));
			float fTemp1 = mydsp_faustpower2_f(fTemp0);
			float fTemp2 = (fConst4 * fRec3[1]);
			float fTemp3 = (fConst6 * float(input10[i]));
			float fTemp4 = (fConst8 * fRec6[1]);
			float fTemp5 = (fConst9 * fRec9[1]);
			fRec11[0] = (fTemp3 + (fTemp4 + (fRec11[1] + fTemp5)));
			fRec9[0] = fRec11[0];
			float fRec10 = ((fTemp5 + fTemp4) + fTemp3);
			fRec8[0] = (fRec9[0] + fRec8[1]);
			fRec6[0] = fRec8[0];
			float fRec7 = fRec10;
			fRec5[0] = (fTemp2 + (fRec7 + fRec5[1]));
			fRec3[0] = fRec5[0];
			float fRec4 = (fRec7 + fTemp2);
			float fTemp6 = (((fTemp0 + -2.0f) * fTemp0) + 1.0f);
			float fTemp7 = (fTemp1 + -1.0f);
			float fTemp8 = (((fTemp0 + 2.0f) * fTemp0) + 1.0f);
			fRec2[0] = (fRec4 - (((fTemp6 * fRec2[2]) + (2.0f * (fRec2[1] * fTemp7))) / fTemp8));
			fRec12[0] = (fSlow2 + (0.999000013f * fRec12[1]));
			float fTemp9 = (fRec12[0] * fTemp8);
			float fTemp10 = (0.0f - (2.0f / fTemp8));
			float fTemp11 = (((fTemp1 * (fRec2[2] + (fRec2[0] + (2.0f * fRec2[1])))) / fTemp9) + (0.50103116f * (fRec12[0] * (0.0f - ((fTemp10 * fRec2[1]) + ((fRec2[0] + fRec2[2]) / fTemp8))))));
			float fTemp12 = (fConst11 * fRec14[1]);
			float fTemp13 = (fConst12 * fRec17[1]);
			float fTemp14 = (fConst14 * float(input23[i]));
			float fTemp15 = (fConst15 * fRec20[1]);
			float fTemp16 = (fConst16 * fRec23[1]);
			fRec25[0] = (fTemp14 + (fTemp15 + (fRec25[1] + fTemp16)));
			fRec23[0] = fRec25[0];
			float fRec24 = ((fTemp16 + fTemp15) + fTemp14);
			fRec22[0] = (fRec23[0] + fRec22[1]);
			fRec20[0] = fRec22[0];
			float fRec21 = fRec24;
			fRec19[0] = (fTemp12 + (fTemp13 + (fRec21 + fRec19[1])));
			fRec17[0] = fRec19[0];
			float fRec18 = (fTemp12 + (fRec21 + fTemp13));
			fRec16[0] = (fRec17[0] + fRec16[1]);
			fRec14[0] = fRec16[0];
			float fRec15 = fRec18;
			fRec13[0] = (fRec15 - (((fTemp6 * fRec13[2]) + (2.0f * (fRec13[1] * fTemp7))) / fTemp8));
			float fTemp17 = ((((fRec13[2] + (fRec13[0] + (2.0f * fRec13[1]))) * fTemp1) / fTemp9) + (0.245735466f * (fRec12[0] * (0.0f - ((fTemp10 * fRec13[1]) + ((fRec13[0] + fRec13[2]) / fTemp8))))));
			float fTemp18 = (fConst11 * fRec27[1]);
			float fTemp19 = (fConst12 * fRec30[1]);
			float fTemp20 = (fConst14 * float(input21[i]));
			float fTemp21 = (fConst15 * fRec33[1]);
			float fTemp22 = (fConst16 * fRec36[1]);
			fRec38[0] = (fTemp20 + (fTemp21 + (fRec38[1] + fTemp22)));
			fRec36[0] = fRec38[0];
			float fRec37 = ((fTemp22 + fTemp21) + fTemp20);
			fRec35[0] = (fRec36[0] + fRec35[1]);
			fRec33[0] = fRec35[0];
			float fRec34 = fRec37;
			fRec32[0] = (fTemp18 + (fTemp19 + (fRec34 + fRec32[1])));
			fRec30[0] = fRec32[0];
			float fRec31 = (fTemp18 + (fRec34 + fTemp19));
			fRec29[0] = (fRec30[0] + fRec29[1]);
			fRec27[0] = fRec29[0];
			float fRec28 = fRec31;
			fRec26[0] = (fRec28 - (((fTemp6 * fRec26[2]) + (2.0f * (fRec26[1] * fTemp7))) / fTemp8));
			float fTemp23 = ((((fRec26[2] + (fRec26[0] + (2.0f * fRec26[1]))) * fTemp1) / fTemp9) + (0.245735466f * (fRec12[0] * (0.0f - ((fTemp10 * fRec26[1]) + ((fRec26[0] + fRec26[2]) / fTemp8))))));
			float fTemp24 = (fConst11 * fRec40[1]);
			float fTemp25 = (fConst12 * fRec43[1]);
			float fTemp26 = (fConst14 * float(input20[i]));
			float fTemp27 = (fConst15 * fRec46[1]);
			float fTemp28 = (fConst16 * fRec49[1]);
			fRec51[0] = (fTemp26 + (fTemp27 + (fRec51[1] + fTemp28)));
			fRec49[0] = fRec51[0];
			float fRec50 = ((fTemp28 + fTemp27) + fTemp26);
			fRec48[0] = (fRec49[0] + fRec48[1]);
			fRec46[0] = fRec48[0];
			float fRec47 = fRec50;
			fRec45[0] = (fTemp24 + (fTemp25 + (fRec47 + fRec45[1])));
			fRec43[0] = fRec45[0];
			float fRec44 = (fTemp24 + (fRec47 + fTemp25));
			fRec42[0] = (fRec43[0] + fRec42[1]);
			fRec40[0] = fRec42[0];
			float fRec41 = fRec44;
			fRec39[0] = (fRec41 - (((fTemp6 * fRec39[2]) + (2.0f * (fRec39[1] * fTemp7))) / fTemp8));
			float fTemp29 = ((((fRec39[2] + (fRec39[0] + (2.0f * fRec39[1]))) * fTemp1) / fTemp9) + (0.245735466f * (fRec12[0] * (0.0f - ((fTemp10 * fRec39[1]) + ((fRec39[0] + fRec39[2]) / fTemp8))))));
			float fTemp30 = (fConst18 * float(input6[i]));
			float fTemp31 = (fConst19 * fRec53[1]);
			float fTemp32 = (fConst20 * fRec56[1]);
			fRec58[0] = (fTemp30 + (fTemp31 + (fRec58[1] + fTemp32)));
			fRec56[0] = fRec58[0];
			float fRec57 = ((fTemp32 + fTemp31) + fTemp30);
			fRec55[0] = (fRec56[0] + fRec55[1]);
			fRec53[0] = fRec55[0];
			float fRec54 = fRec57;
			fRec52[0] = (fRec54 - (((fRec52[2] * fTemp6) + (2.0f * (fRec52[1] * fTemp7))) / fTemp8));
			float fTemp33 = ((((fRec52[2] + (fRec52[0] + (2.0f * fRec52[1]))) * fTemp1) / fTemp9) + (0.731742859f * (fRec12[0] * (0.0f - ((fTemp10 * fRec52[1]) + ((fRec52[0] + fRec52[2]) / fTemp8))))));
			float fTemp34 = (fConst22 * float(input3[i]));
			float fTemp35 = (fConst23 * fRec60[1]);
			fRec62[0] = (fTemp34 + (fRec62[1] + fTemp35));
			fRec60[0] = fRec62[0];
			float fRec61 = (fTemp35 + fTemp34);
			fRec59[0] = (fRec61 - (((fRec59[2] * fTemp6) + (2.0f * (fRec59[1] * fTemp7))) / fTemp8));
			float fTemp36 = ((((fRec59[2] + ((2.0f * fRec59[1]) + fRec59[0])) * fTemp1) / fTemp9) + (0.906179845f * (fRec12[0] * (0.0f - ((fTemp10 * fRec59[1]) + ((fRec59[2] + fRec59[0]) / fTemp8))))));
			fRec63[0] = (float(input0[i]) - (((fRec63[2] * fTemp6) + (2.0f * (fRec63[1] * fTemp7))) / fTemp8));
			float fTemp37 = (((((fRec63[0] + (2.0f * fRec63[1])) + fRec63[2]) * fTemp1) / fTemp9) + (fRec12[0] * (0.0f - ((fTemp10 * fRec63[1]) + ((fRec63[0] + fRec63[2]) / fTemp8)))));
			float fTemp38 = (fConst22 * float(input2[i]));
			float fTemp39 = (fConst23 * fRec65[1]);
			fRec67[0] = (fTemp38 + (fRec67[1] + fTemp39));
			fRec65[0] = fRec67[0];
			float fRec66 = (fTemp39 + fTemp38);
			fRec64[0] = (fRec66 - (((fRec64[2] * fTemp6) + (2.0f * (fRec64[1] * fTemp7))) / fTemp8));
			float fTemp40 = ((((fRec64[2] + ((2.0f * fRec64[1]) + fRec64[0])) * fTemp1) / fTemp9) + (0.906179845f * (fRec12[0] * (0.0f - ((fTemp10 * fRec64[1]) + ((fRec64[2] + fRec64[0]) / fTemp8))))));
			float fTemp41 = (fConst18 * float(input4[i]));
			float fTemp42 = (fConst19 * fRec69[1]);
			float fTemp43 = (fConst20 * fRec72[1]);
			fRec74[0] = (fTemp41 + (fTemp42 + (fRec74[1] + fTemp43)));
			fRec72[0] = fRec74[0];
			float fRec73 = ((fTemp43 + fTemp42) + fTemp41);
			fRec71[0] = (fRec71[1] + fRec72[0]);
			fRec69[0] = fRec71[0];
			float fRec70 = fRec73;
			fRec68[0] = (fRec70 - (((fRec68[2] * fTemp6) + (2.0f * (fRec68[1] * fTemp7))) / fTemp8));
			float fTemp44 = ((((fRec68[2] + ((2.0f * fRec68[1]) + fRec68[0])) * fTemp1) / fTemp9) + (0.731742859f * (fRec12[0] * (0.0f - ((fTemp10 * fRec68[1]) + ((fRec68[2] + fRec68[0]) / fTemp8))))));
			float fTemp45 = (fConst4 * fRec76[1]);
			float fTemp46 = (fConst6 * float(input9[i]));
			float fTemp47 = (fConst8 * fRec79[1]);
			float fTemp48 = (fConst9 * fRec82[1]);
			fRec84[0] = (fTemp46 + (fTemp47 + (fRec84[1] + fTemp48)));
			fRec82[0] = fRec84[0];
			float fRec83 = ((fTemp48 + fTemp47) + fTemp46);
			fRec81[0] = (fRec82[0] + fRec81[1]);
			fRec79[0] = fRec81[0];
			float fRec80 = fRec83;
			fRec78[0] = (fTemp45 + (fRec80 + fRec78[1]));
			fRec76[0] = fRec78[0];
			float fRec77 = (fRec80 + fTemp45);
			fRec75[0] = (fRec77 - (((fTemp6 * fRec75[2]) + (2.0f * (fRec75[1] * fTemp7))) / fTemp8));
			float fTemp49 = ((((fRec75[2] + (fRec75[0] + (2.0f * fRec75[1]))) * fTemp1) / fTemp9) + (0.50103116f * (fRec12[0] * (0.0f - ((fTemp10 * fRec75[1]) + ((fRec75[0] + fRec75[2]) / fTemp8))))));
			float fTemp50 = (fConst4 * fRec86[1]);
			float fTemp51 = (fConst6 * float(input12[i]));
			float fTemp52 = (fConst8 * fRec89[1]);
			float fTemp53 = (fConst9 * fRec92[1]);
			fRec94[0] = (fTemp51 + (fTemp52 + (fRec94[1] + fTemp53)));
			fRec92[0] = fRec94[0];
			float fRec93 = ((fTemp53 + fTemp52) + fTemp51);
			fRec91[0] = (fRec92[0] + fRec91[1]);
			fRec89[0] = fRec91[0];
			float fRec90 = fRec93;
			fRec88[0] = (fTemp50 + (fRec90 + fRec88[1]));
			fRec86[0] = fRec88[0];
			float fRec87 = (fRec90 + fTemp50);
			fRec85[0] = (fRec87 - (((fTemp6 * fRec85[2]) + (2.0f * (fTemp7 * fRec85[1]))) / fTemp8));
			float fTemp54 = (((fTemp1 * (fRec85[2] + (fRec85[0] + (2.0f * fRec85[1])))) / fTemp9) + (0.50103116f * (fRec12[0] * (0.0f - ((fTemp10 * fRec85[1]) + ((fRec85[0] + fRec85[2]) / fTemp8))))));
			float fTemp55 = (fConst18 * float(input7[i]));
			float fTemp56 = (fConst19 * fRec96[1]);
			float fTemp57 = (fConst20 * fRec99[1]);
			fRec101[0] = (fTemp55 + (fTemp56 + (fRec101[1] + fTemp57)));
			fRec99[0] = fRec101[0];
			float fRec100 = ((fTemp57 + fTemp56) + fTemp55);
			fRec98[0] = (fRec99[0] + fRec98[1]);
			fRec96[0] = fRec98[0];
			float fRec97 = fRec100;
			fRec95[0] = (fRec97 - (((fTemp6 * fRec95[2]) + (2.0f * (fTemp7 * fRec95[1]))) / fTemp8));
			float fTemp58 = (((fTemp1 * (fRec95[2] + (fRec95[0] + (2.0f * fRec95[1])))) / fTemp9) + (0.731742859f * (fRec12[0] * (0.0f - ((fTemp10 * fRec95[1]) + ((fRec95[0] + fRec95[2]) / fTemp8))))));
			float fTemp59 = (fConst4 * fRec103[1]);
			float fTemp60 = (fConst6 * float(input13[i]));
			float fTemp61 = (fConst8 * fRec106[1]);
			float fTemp62 = (fConst9 * fRec109[1]);
			fRec111[0] = (fTemp60 + (fTemp61 + (fRec111[1] + fTemp62)));
			fRec109[0] = fRec111[0];
			float fRec110 = ((fTemp62 + fTemp61) + fTemp60);
			fRec108[0] = (fRec109[0] + fRec108[1]);
			fRec106[0] = fRec108[0];
			float fRec107 = fRec110;
			fRec105[0] = (fTemp59 + (fRec107 + fRec105[1]));
			fRec103[0] = fRec105[0];
			float fRec104 = (fRec107 + fTemp59);
			fRec102[0] = (fRec104 - (((fTemp6 * fRec102[2]) + (2.0f * (fTemp7 * fRec102[1]))) / fTemp8));
			float fTemp63 = ((0.50103116f * (fRec12[0] * (0.0f - (((fRec102[0] + fRec102[2]) / fTemp8) + (fTemp10 * fRec102[1]))))) + ((fTemp1 * (fRec102[2] + (fRec102[0] + (2.0f * fRec102[1])))) / fTemp9));
			float fTemp64 = (fConst4 * fRec113[1]);
			float fTemp65 = (fConst6 * float(input15[i]));
			float fTemp66 = (fConst8 * fRec116[1]);
			float fTemp67 = (fConst9 * fRec119[1]);
			fRec121[0] = (fTemp65 + (fTemp66 + (fRec121[1] + fTemp67)));
			fRec119[0] = fRec121[0];
			float fRec120 = ((fTemp67 + fTemp66) + fTemp65);
			fRec118[0] = (fRec119[0] + fRec118[1]);
			fRec116[0] = fRec118[0];
			float fRec117 = fRec120;
			fRec115[0] = (fTemp64 + (fRec117 + fRec115[1]));
			fRec113[0] = fRec115[0];
			float fRec114 = (fRec117 + fTemp64);
			fRec112[0] = (fRec114 - (((fTemp6 * fRec112[2]) + (2.0f * (fTemp7 * fRec112[1]))) / fTemp8));
			float fTemp68 = (((fTemp1 * (fRec112[2] + (fRec112[0] + (2.0f * fRec112[1])))) / fTemp9) + (0.50103116f * (fRec12[0] * (0.0f - ((fTemp10 * fRec112[1]) + ((fRec112[0] + fRec112[2]) / fTemp8))))));
			float fTemp69 = (fConst11 * fRec123[1]);
			float fTemp70 = (fConst12 * fRec126[1]);
			float fTemp71 = (fConst14 * float(input16[i]));
			float fTemp72 = (fConst15 * fRec129[1]);
			float fTemp73 = (fConst16 * fRec132[1]);
			fRec134[0] = (fTemp71 + (fTemp72 + (fRec134[1] + fTemp73)));
			fRec132[0] = fRec134[0];
			float fRec133 = ((fTemp73 + fTemp72) + fTemp71);
			fRec131[0] = (fRec132[0] + fRec131[1]);
			fRec129[0] = fRec131[0];
			float fRec130 = fRec133;
			fRec128[0] = (fTemp69 + (fTemp70 + (fRec130 + fRec128[1])));
			fRec126[0] = fRec128[0];
			float fRec127 = (fTemp69 + (fRec130 + fTemp70));
			fRec125[0] = (fRec126[0] + fRec125[1]);
			fRec123[0] = fRec125[0];
			float fRec124 = fRec127;
			fRec122[0] = (fRec124 - (((fTemp6 * fRec122[2]) + (2.0f * (fTemp7 * fRec122[1]))) / fTemp8));
			float fTemp74 = (((fTemp1 * (fRec122[2] + (fRec122[0] + (2.0f * fRec122[1])))) / fTemp9) + (0.245735466f * (fRec12[0] * (0.0f - ((fTemp10 * fRec122[1]) + ((fRec122[0] + fRec122[2]) / fTemp8))))));
			float fTemp75 = (fConst11 * fRec136[1]);
			float fTemp76 = (fConst12 * fRec139[1]);
			float fTemp77 = (fConst14 * float(input17[i]));
			float fTemp78 = (fConst15 * fRec142[1]);
			float fTemp79 = (fConst16 * fRec145[1]);
			fRec147[0] = (fTemp77 + (fTemp78 + (fRec147[1] + fTemp79)));
			fRec145[0] = fRec147[0];
			float fRec146 = ((fTemp79 + fTemp78) + fTemp77);
			fRec144[0] = (fRec145[0] + fRec144[1]);
			fRec142[0] = fRec144[0];
			float fRec143 = fRec146;
			fRec141[0] = (fTemp75 + (fTemp76 + (fRec143 + fRec141[1])));
			fRec139[0] = fRec141[0];
			float fRec140 = (fTemp75 + (fRec143 + fTemp76));
			fRec138[0] = (fRec139[0] + fRec138[1]);
			fRec136[0] = fRec138[0];
			float fRec137 = fRec140;
			fRec135[0] = (fRec137 - (((fTemp6 * fRec135[2]) + (2.0f * (fTemp7 * fRec135[1]))) / fTemp8));
			float fTemp80 = (((fTemp1 * (fRec135[2] + (fRec135[0] + (2.0f * fRec135[1])))) / fTemp9) + (0.245735466f * (fRec12[0] * (0.0f - ((fTemp10 * fRec135[1]) + ((fRec135[0] + fRec135[2]) / fTemp8))))));
			float fTemp81 = (fConst11 * fRec149[1]);
			float fTemp82 = (fConst12 * fRec152[1]);
			float fTemp83 = (fConst14 * float(input18[i]));
			float fTemp84 = (fConst15 * fRec155[1]);
			float fTemp85 = (fConst16 * fRec158[1]);
			fRec160[0] = (fTemp83 + (fTemp84 + (fRec160[1] + fTemp85)));
			fRec158[0] = fRec160[0];
			float fRec159 = ((fTemp85 + fTemp84) + fTemp83);
			fRec157[0] = (fRec158[0] + fRec157[1]);
			fRec155[0] = fRec157[0];
			float fRec156 = fRec159;
			fRec154[0] = (fTemp81 + (fTemp82 + (fRec156 + fRec154[1])));
			fRec152[0] = fRec154[0];
			float fRec153 = (fTemp81 + (fRec156 + fTemp82));
			fRec151[0] = (fRec152[0] + fRec151[1]);
			fRec149[0] = fRec151[0];
			float fRec150 = fRec153;
			fRec148[0] = (fRec150 - (((fTemp6 * fRec148[2]) + (2.0f * (fTemp7 * fRec148[1]))) / fTemp8));
			float fTemp86 = (((fTemp1 * (fRec148[2] + (fRec148[0] + (2.0f * fRec148[1])))) / fTemp9) + (0.245735466f * (fRec12[0] * (0.0f - ((fTemp10 * fRec148[1]) + ((fRec148[0] + fRec148[2]) / fTemp8))))));
			float fTemp87 = (fConst11 * fRec162[1]);
			float fTemp88 = (fConst12 * fRec165[1]);
			float fTemp89 = (fConst14 * float(input24[i]));
			float fTemp90 = (fConst15 * fRec168[1]);
			float fTemp91 = (fConst16 * fRec171[1]);
			fRec173[0] = (fTemp89 + (fTemp90 + (fRec173[1] + fTemp91)));
			fRec171[0] = fRec173[0];
			float fRec172 = ((fTemp91 + fTemp90) + fTemp89);
			fRec170[0] = (fRec171[0] + fRec170[1]);
			fRec168[0] = fRec170[0];
			float fRec169 = fRec172;
			fRec167[0] = (fTemp87 + (fTemp88 + (fRec169 + fRec167[1])));
			fRec165[0] = fRec167[0];
			float fRec166 = (fTemp87 + (fRec169 + fTemp88));
			fRec164[0] = (fRec165[0] + fRec164[1]);
			fRec162[0] = fRec164[0];
			float fRec163 = fRec166;
			fRec161[0] = (fRec163 - (((fTemp6 * fRec161[2]) + (2.0f * (fRec161[1] * fTemp7))) / fTemp8));
			float fTemp92 = ((((fRec161[2] + (fRec161[0] + (2.0f * fRec161[1]))) * fTemp1) / fTemp9) + (0.245735466f * (fRec12[0] * (0.0f - ((fTemp10 * fRec161[1]) + ((fRec161[0] + fRec161[2]) / fTemp8))))));
			float fTemp93 = (fConst11 * fRec175[1]);
			float fTemp94 = (fConst12 * fRec178[1]);
			float fTemp95 = (fConst14 * float(input22[i]));
			float fTemp96 = (fConst15 * fRec181[1]);
			float fTemp97 = (fConst16 * fRec184[1]);
			fRec186[0] = (fTemp95 + (fTemp96 + (fRec186[1] + fTemp97)));
			fRec184[0] = fRec186[0];
			float fRec185 = ((fTemp97 + fTemp96) + fTemp95);
			fRec183[0] = (fRec184[0] + fRec183[1]);
			fRec181[0] = fRec183[0];
			float fRec182 = fRec185;
			fRec180[0] = (fTemp93 + (fTemp94 + (fRec182 + fRec180[1])));
			fRec178[0] = fRec180[0];
			float fRec179 = (fTemp93 + (fRec182 + fTemp94));
			fRec177[0] = (fRec178[0] + fRec177[1]);
			fRec175[0] = fRec177[0];
			float fRec176 = fRec179;
			fRec174[0] = (fRec176 - (((fTemp6 * fRec174[2]) + (2.0f * (fRec174[1] * fTemp7))) / fTemp8));
			float fTemp98 = ((((fRec174[2] + (fRec174[0] + (2.0f * fRec174[1]))) * fTemp1) / fTemp9) + (0.245735466f * (fRec12[0] * (0.0f - ((fTemp10 * fRec174[1]) + ((fRec174[0] + fRec174[2]) / fTemp8))))));
			float fTemp99 = (fConst18 * float(input8[i]));
			float fTemp100 = (fConst19 * fRec188[1]);
			float fTemp101 = (fConst20 * fRec191[1]);
			fRec193[0] = (fTemp99 + (fTemp100 + (fRec193[1] + fTemp101)));
			fRec191[0] = fRec193[0];
			float fRec192 = ((fTemp101 + fTemp100) + fTemp99);
			fRec190[0] = (fRec191[0] + fRec190[1]);
			fRec188[0] = fRec190[0];
			float fRec189 = fRec192;
			fRec187[0] = (fRec189 - (((2.0f * (fTemp7 * fRec187[1])) + (fTemp6 * fRec187[2])) / fTemp8));
			float fTemp102 = (((((fRec187[0] + (2.0f * fRec187[1])) + fRec187[2]) * fTemp1) / fTemp9) + (0.731742859f * (fRec12[0] * (0.0f - ((fTemp10 * fRec187[1]) + ((fRec187[0] + fRec187[2]) / fTemp8))))));
			float fTemp103 = (fConst22 * float(input1[i]));
			float fTemp104 = (fConst23 * fRec195[1]);
			fRec197[0] = (fTemp103 + (fRec197[1] + fTemp104));
			fRec195[0] = fRec197[0];
			float fRec196 = (fTemp104 + fTemp103);
			fRec194[0] = (fRec196 - (((fRec194[2] * fTemp6) + (2.0f * (fRec194[1] * fTemp7))) / fTemp8));
			float fTemp105 = ((((fRec194[2] + (fRec194[0] + (2.0f * fRec194[1]))) * fTemp1) / fTemp9) + (0.906179845f * (fRec12[0] * (0.0f - ((fTemp10 * fRec194[1]) + ((fRec194[0] + fRec194[2]) / fTemp8))))));
			float fTemp106 = (fConst18 * float(input5[i]));
			float fTemp107 = (fConst19 * fRec199[1]);
			float fTemp108 = (fConst20 * fRec202[1]);
			fRec204[0] = (fTemp106 + (fTemp107 + (fRec204[1] + fTemp108)));
			fRec202[0] = fRec204[0];
			float fRec203 = ((fTemp108 + fTemp107) + fTemp106);
			fRec201[0] = (fRec201[1] + fRec202[0]);
			fRec199[0] = fRec201[0];
			float fRec200 = fRec203;
			fRec198[0] = (fRec200 - (((fRec198[2] * fTemp6) + (2.0f * (fRec198[1] * fTemp7))) / fTemp8));
			float fTemp109 = ((((fRec198[2] + ((2.0f * fRec198[1]) + fRec198[0])) * fTemp1) / fTemp9) + (0.731742859f * (fRec12[0] * (0.0f - ((fTemp10 * fRec198[1]) + ((fRec198[2] + fRec198[0]) / fTemp8))))));
			float fTemp110 = (fConst11 * fRec206[1]);
			float fTemp111 = (fConst12 * fRec209[1]);
			float fTemp112 = (fConst14 * float(input19[i]));
			float fTemp113 = (fConst15 * fRec212[1]);
			float fTemp114 = (fConst16 * fRec215[1]);
			fRec217[0] = (fTemp112 + (fTemp113 + (fRec217[1] + fTemp114)));
			fRec215[0] = fRec217[0];
			float fRec216 = ((fTemp114 + fTemp113) + fTemp112);
			fRec214[0] = (fRec215[0] + fRec214[1]);
			fRec212[0] = fRec214[0];
			float fRec213 = fRec216;
			fRec211[0] = (fTemp110 + (fTemp111 + (fRec213 + fRec211[1])));
			fRec209[0] = fRec211[0];
			float fRec210 = (fTemp110 + (fRec213 + fTemp111));
			fRec208[0] = (fRec209[0] + fRec208[1]);
			fRec206[0] = fRec208[0];
			float fRec207 = fRec210;
			fRec205[0] = (fRec207 - (((fTemp6 * fRec205[2]) + (2.0f * (fRec205[1] * fTemp7))) / fTemp8));
			float fTemp115 = ((((fRec205[2] + (fRec205[0] + (2.0f * fRec205[1]))) * fTemp1) / fTemp9) + (0.245735466f * (fRec12[0] * (0.0f - ((fTemp10 * fRec205[1]) + ((fRec205[0] + fRec205[2]) / fTemp8))))));
			float fTemp116 = (fConst4 * fRec219[1]);
			float fTemp117 = (fConst6 * float(input11[i]));
			float fTemp118 = (fConst8 * fRec222[1]);
			float fTemp119 = (fConst9 * fRec225[1]);
			fRec227[0] = (fTemp117 + (fTemp118 + (fRec227[1] + fTemp119)));
			fRec225[0] = fRec227[0];
			float fRec226 = ((fTemp119 + fTemp118) + fTemp117);
			fRec224[0] = (fRec225[0] + fRec224[1]);
			fRec222[0] = fRec224[0];
			float fRec223 = fRec226;
			fRec221[0] = (fTemp116 + (fRec223 + fRec221[1]));
			fRec219[0] = fRec221[0];
			float fRec220 = (fRec223 + fTemp116);
			fRec218[0] = (fRec220 - (((fTemp6 * fRec218[2]) + (2.0f * (fTemp7 * fRec218[1]))) / fTemp8));
			float fTemp120 = (((fTemp1 * (fRec218[2] + (fRec218[0] + (2.0f * fRec218[1])))) / fTemp9) + (0.50103116f * (fRec12[0] * (0.0f - ((fTemp10 * fRec218[1]) + ((fRec218[0] + fRec218[2]) / fTemp8))))));
			float fTemp121 = (fConst4 * fRec229[1]);
			float fTemp122 = (fConst6 * float(input14[i]));
			float fTemp123 = (fConst8 * fRec232[1]);
			float fTemp124 = (fConst9 * fRec235[1]);
			fRec237[0] = (fTemp122 + (fTemp123 + (fRec237[1] + fTemp124)));
			fRec235[0] = fRec237[0];
			float fRec236 = ((fTemp124 + fTemp123) + fTemp122);
			fRec234[0] = (fRec235[0] + fRec234[1]);
			fRec232[0] = fRec234[0];
			float fRec233 = fRec236;
			fRec231[0] = (fTemp121 + (fRec233 + fRec231[1]));
			fRec229[0] = fRec231[0];
			float fRec230 = (fRec233 + fTemp121);
			fRec228[0] = (fRec230 - (((fTemp6 * fRec228[2]) + (2.0f * (fTemp7 * fRec228[1]))) / fTemp8));
			float fTemp125 = (((fTemp1 * (fRec228[2] + (fRec228[0] + (2.0f * fRec228[1])))) / fTemp9) + (0.50103116f * (fRec12[0] * (0.0f - ((fTemp10 * fRec228[1]) + ((fRec228[0] + fRec228[2]) / fTemp8))))));
			output0[i] = FAUSTFLOAT((fRec0[0] * ((((((((((8.52680023e-06f * fTemp11) + ((2.74040008e-06f * fTemp17) + ((4.64979985e-06f * fTemp23) + ((0.0716716722f * fTemp29) + (((0.0872831121f * fTemp33) + (((2.45000001e-06f * fTemp36) + ((0.0473226868f * fTemp37) + (0.0769467279f * fTemp40))) + (3.70409998e-06f * fTemp44))) + (1.65450001e-06f * fTemp49)))))) + (0.0843499899f * fTemp54)) + (4.46969989e-06f * fTemp58)) + (5.3568001e-06f * fTemp63)) + (1.14039995e-06f * fTemp68)) + (5.28700014e-07f * fTemp74)) + (3.90290006e-06f * fTemp80)) + (1.38361002e-05f * fTemp86)) - ((((2.32110006e-06f * fTemp92) + ((5.01880004e-06f * fTemp98) + (((1.50289998e-06f * fTemp102) + ((2.71919998e-06f * fTemp105) + (5.04480022e-06f * fTemp109))) + (5.69839995e-06f * fTemp115)))) + (6.22569996e-06f * fTemp120)) + (3.31060005e-06f * fTemp125)))));
			fVec0[0] = (((0.0264521018f * fTemp86) + ((0.0175191797f * fTemp63) + (((0.00963862892f * fTemp54) + ((0.0541275442f * fTemp120) + ((0.0223340727f * fTemp11) + ((0.010749503f * fTemp23) + ((((((0.0108339777f * fTemp36) + ((0.0463922769f * fTemp40) + ((0.0341214538f * fTemp37) + (0.0301853716f * fTemp105)))) + (0.011978806f * fTemp44)) + (0.0510204248f * fTemp109)) + (0.0338490047f * fTemp33)) + (0.0390786566f * fTemp115)))))) + (0.0176964421f * fTemp58)))) - ((0.00779462699f * fTemp80) + ((0.00370370969f * fTemp74) + ((0.00808212627f * fTemp68) + ((0.026083149f * fTemp125) + ((0.000207974197f * fTemp92) + ((0.0165106263f * fTemp17) + ((0.0332537405f * fTemp98) + (((0.003537582f * fTemp49) + (0.0133990385f * fTemp102)) + (0.0120762754f * fTemp29))))))))));
			output1[i] = FAUSTFLOAT((0.998005569f * (fRec0[0] * fVec0[iConst24])));
			fVec1[0] = (((0.0119946115f * fTemp80) + ((6.08019991e-06f * fTemp74) + ((0.00575517025f * fTemp68) + ((0.0069673867f * fTemp54) + ((0.0435375981f * fTemp120) + ((0.0120080616f * fTemp17) + (((0.00574632082f * fTemp49) + ((0.0352820829f * fTemp33) + (((0.0510143228f * fTemp40) + ((0.038172856f * fTemp37) + (0.0257401336f * fTemp105))) + (0.0426589735f * fTemp109)))) + (0.028853517f * fTemp115)))))))) - ((0.045309592f * fTemp86) + ((5.31510022e-06f * fTemp125) + ((0.0435424335f * fTemp63) + ((0.042658627f * fTemp58) + ((0.0371481292f * fTemp11) + ((0.0018724103f * fTemp92) + ((1.01930004e-06f * fTemp98) + ((0.0288632512f * fTemp23) + (((4.50529978e-06f * fTemp102) + ((0.0257381666f * fTemp36) + (0.0195993222f * fTemp44))) + (0.0168237407f * fTemp29)))))))))));
			output2[i] = FAUSTFLOAT((0.997783959f * (fRec0[0] * fVec1[iConst25])));
			fVec2[0] = (((0.026444735f * fTemp86) + ((0.0036919862f * fTemp74) + ((0.0260652341f * fTemp125) + ((0.00964230858f * fTemp54) + ((0.0223280527f * fTemp11) + (((0.0133855147f * fTemp102) + ((0.0338515826f * fTemp33) + (((0.0341186747f * fTemp37) + (0.0463907383f * fTemp40)) + (0.0119755501f * fTemp44)))) + (0.0332453474f * fTemp98))))))) - ((0.016497789f * fTemp80) + ((0.00352248852f * fTemp68) + ((0.0541319922f * fTemp63) + ((0.0510147251f * fTemp58) + ((0.0175154228f * fTemp120) + ((0.000217761306f * fTemp92) + ((0.00777012017f * fTemp17) + ((0.039093677f * fTemp23) + ((0.0120782666f * fTemp29) + (((0.00807411969f * fTemp49) + (((0.0108332634f * fTemp105) + (0.0301783718f * fTemp36)) + (0.017694287f * fTemp109))) + (0.010745679f * fTemp115))))))))))));
			output3[i] = FAUSTFLOAT((0.998005569f * (fRec0[0] * fVec2[iConst24])));
			fVec3[0] = (((0.0193701088f * fTemp86) + ((0.0122731943f * fTemp80) + ((0.00586031703f * fTemp68) + ((0.0123329759f * fTemp54) + ((0.015175622f * fTemp11) + ((0.00215694727f * fTemp92) + (((0.00585178612f * fTemp49) + ((0.0324303731f * fTemp33) + (((0.0300636496f * fTemp37) + (0.0417693704f * fTemp40)) + (0.00778828422f * fTemp44)))) + (0.0122893974f * fTemp17)))))))) - ((0.00374657242f * fTemp74) + ((0.0262655504f * fTemp125) + ((0.0134790875f * fTemp63) + ((0.012267625f * fTemp58) + ((0.0502912141f * fTemp120) + ((0.0335300677f * fTemp98) + ((0.0104244938f * fTemp23) + ((0.00731574697f * fTemp29) + (((0.0134784635f * fTemp102) + (((0.0265663434f * fTemp105) + (0.0071219611f * fTemp36)) + (0.0457643978f * fTemp109))) + (0.0389057137f * fTemp115)))))))))));
			output4[i] = FAUSTFLOAT((0.998005569f * (fRec0[0] * fVec3[iConst24])));
			fVec4[0] = (((6.30920022e-06f * fTemp74) + ((1.47399999e-06f * fTemp125) + ((0.0435460955f * fTemp63) + ((0.0426651761f * fTemp58) + ((0.00697506964f * fTemp54) + ((2.5027e-06f * fTemp98) + (((4.58500011e-07f * fTemp102) + (((0.0257430729f * fTemp36) + ((0.0381806493f * fTemp37) + (0.0510256663f * fTemp40))) + (0.0352924652f * fTemp33))) + (0.0288615432f * fTemp23)))))))) - ((0.0453090109f * fTemp86) + ((0.0119943367f * fTemp80) + ((0.00575058721f * fTemp68) + ((0.0435448177f * fTemp120) + ((0.037147373f * fTemp11) + ((0.00186739466f * fTemp92) + ((0.0120009482f * fTemp17) + ((0.0168180577f * fTemp29) + (((0.00574612245f * fTemp49) + ((0.0426623151f * fTemp109) + ((0.0257410146f * fTemp105) + (0.019598661f * fTemp44)))) + (0.0288644452f * fTemp115)))))))))));
			output5[i] = FAUSTFLOAT((0.997783959f * (fRec0[0] * fVec4[iConst25])));
			fVec5[0] = (((0.0193752591f * fTemp86) + ((0.0122866696f * fTemp80) + ((0.00374108041f * fTemp74) + ((0.00585071836f * fTemp68) + ((0.0262656976f * fTemp125) + ((0.0502907373f * fTemp63) + (((0.0123249413f * fTemp54) + ((0.0134819802f * fTemp120) + ((0.0151786013f * fTemp11) + ((0.00215586647f * fTemp92) + ((0.0122723347f * fTemp17) + ((0.0335324742f * fTemp98) + ((0.0389056094f * fTemp23) + (((0.00585804973f * fTemp49) + ((0.0134779755f * fTemp102) + ((0.0324229002f * fTemp33) + ((0.0122708166f * fTemp109) + (((0.0265659001f * fTemp36) + ((0.0417632945f * fTemp40) + ((0.0300600771f * fTemp37) + (0.00712388381f * fTemp105)))) + (0.00778933102f * fTemp44)))))) + (0.0104250107f * fTemp115))))))))) + (0.0457637608f * fTemp58)))))))) - (0.00732353423f * fTemp29));
			output6[i] = FAUSTFLOAT((0.998005569f * (fRec0[0] * fVec5[iConst24])));
			fVec6[(IOTA & 31)] = (((((((((0.0200215131f * fTemp40) + ((0.0235311687f * fTemp37) + (0.0338993929f * fTemp105))) + (0.0357975774f * fTemp109)) + (0.00446477858f * fTemp17)) + (0.0215720031f * fTemp92)) + (0.00792338047f * fTemp120)) + (0.00227179355f * fTemp68)) + (0.00192661583f * fTemp74)) - (((((((((((((0.0261735525f * fTemp49) + ((0.0306473803f * fTemp102) + ((0.00514168013f * fTemp33) + ((0.00145658397f * fTemp36) + (0.00219735736f * fTemp44))))) + (0.019657487f * fTemp115)) + (0.0151142906f * fTemp29)) + (0.00029127521f * fTemp23)) + (0.0161167923f * fTemp98)) + (0.00384632917f * fTemp11)) + (0.0215631779f * fTemp54)) + (0.00217522983f * fTemp58)) + (0.00170740404f * fTemp63)) + (0.0365134403f * fTemp125)) + (0.0334232487f * fTemp80)) + (0.00398819847f * fTemp86)));
			output7[i] = FAUSTFLOAT((0.994459808f * (fRec0[0] * fVec6[((IOTA - iConst26) & 31)])));
			fVec7[(IOTA & 31)] = (((0.00235598302f * fTemp86) + ((0.00159388839f * fTemp80) + ((0.0265795738f * fTemp74) + ((0.0393768735f * fTemp68) + ((0.00663555553f * fTemp63) + ((0.0365884043f * fTemp17) + ((0.00257663708f * fTemp98) + (((0.00086466549f * fTemp49) + (((0.0179317892f * fTemp40) + ((0.0306961462f * fTemp37) + (0.0410687849f * fTemp105))) + (0.0297248401f * fTemp109))) + (0.0170072708f * fTemp23))))))))) - ((0.0179478563f * fTemp125) + ((0.0178549215f * fTemp58) + ((0.0266766511f * fTemp54) + ((0.0122424858f * fTemp120) + ((0.0334640145f * fTemp11) + ((0.0163461138f * fTemp92) + ((0.00112161727f * fTemp29) + (((0.0220683031f * fTemp102) + ((0.0192471165f * fTemp33) + ((0.0242131073f * fTemp36) + (0.0396838821f * fTemp44)))) + (0.0292676389f * fTemp115))))))))));
			output8[i] = FAUSTFLOAT((0.994903028f * (fRec0[0] * fVec7[((IOTA - iConst27) & 31)])));
			fVec8[(IOTA & 31)] = (((0.0321489461f * fTemp80) + ((0.0181465168f * fTemp125) + ((0.00707933772f * fTemp63) + (((0.0313078016f * fTemp49) + ((0.0222139508f * fTemp102) + (((0.0157211255f * fTemp40) + ((0.0242891777f * fTemp37) + (0.0165882278f * fTemp105))) + (0.0144822551f * fTemp109)))) + (0.0244781263f * fTemp23))))) - ((0.00389228691f * fTemp86) + ((0.026830906f * fTemp74) + ((0.00749638118f * fTemp68) + ((0.0264675654f * fTemp58) + ((0.0223583821f * fTemp54) + ((0.00146551407f * fTemp120) + ((0.02764659f * fTemp11) + ((0.00488647772f * fTemp92) + ((0.00331561361f * fTemp17) + ((0.00253144396f * fTemp98) + ((0.00482360367f * fTemp29) + (((0.0132820914f * fTemp33) + ((0.0335474797f * fTemp36) + (0.0282651987f * fTemp44))) + (0.0120740347f * fTemp115))))))))))))));
			output9[i] = FAUSTFLOAT((0.994903028f * (fRec0[0] * fVec8[((IOTA - iConst27) & 31)])));
			fVec9[(IOTA & 31)] = (((0.00447567692f * fTemp80) + ((0.0365036465f * fTemp125) + ((0.00170157896f * fTemp120) + ((0.021559542f * fTemp92) + ((0.0161364377f * fTemp98) + ((0.0196390953f * fTemp23) + ((0.000279992702f * fTemp115) + ((0.0022798446f * fTemp49) + ((0.0306251757f * fTemp102) + (((0.0200138334f * fTemp40) + ((0.0235146843f * fTemp37) + (0.00145928503f * fTemp105))) + (0.00217607711f * fTemp109))))))))))) - ((0.00398261799f * fTemp86) + ((0.00193307409f * fTemp74) + ((0.0261559151f * fTemp68) + ((0.00794078596f * fTemp63) + ((0.0357850008f * fTemp58) + ((0.0215517879f * fTemp54) + ((0.00385212409f * fTemp11) + ((0.0334180146f * fTemp17) + ((0.0151245277f * fTemp29) + ((0.00512850937f * fTemp33) + ((0.0338744335f * fTemp36) + (0.00220371434f * fTemp44)))))))))))));
			output10[i] = FAUSTFLOAT((0.994459808f * (fRec0[0] * fVec9[((IOTA - iConst26) & 31)])));
			fVec10[(IOTA & 31)] = (((0.00282653188f * fTemp86) + ((0.0276889466f * fTemp74) + ((0.0201868787f * fTemp125) + ((0.00671209022f * fTemp63) + ((0.00212324364f * fTemp120) + ((0.0264661219f * fTemp11) + ((0.024579538f * fTemp23) + ((0.0119211152f * fTemp115) + ((0.0234601125f * fTemp102) + (((0.0242880136f * fTemp37) + (0.0157179944f * fTemp40)) + (0.0275476705f * fTemp44))))))))))) - ((0.0321472399f * fTemp80) + ((0.00919907447f * fTemp68) + ((0.0270602293f * fTemp58) + ((0.0223621186f * fTemp54) + ((0.00340778288f * fTemp92) + ((0.00647454942f * fTemp17) + ((0.000708782929f * fTemp98) + (((0.0313106589f * fTemp49) + ((0.0132877538f * fTemp33) + (((0.0158460233f * fTemp105) + (0.0339761786f * fTemp36)) + (0.0134490049f * fTemp109)))) + (0.00481808931f * fTemp29))))))))));
			output11[i] = FAUSTFLOAT((0.994903028f * (fRec0[0] * fVec10[((IOTA - iConst27) & 31)])));
			fVec11[(IOTA & 31)] = (((0.0065252129f * fTemp86) + ((0.0422834121f * fTemp68) + ((0.00162406347f * fTemp63) + ((0.00622504856f * fTemp120) + ((0.0420676731f * fTemp11) + ((0.0422692448f * fTemp17) + ((0.000839976419f * fTemp98) + ((0.0161856432f * fTemp23) + ((((0.0363172293f * fTemp37) + (0.0244163014f * fTemp40)) + (0.0446210802f * fTemp44)) + (0.0286090747f * fTemp115)))))))))) - ((0.00447662175f * fTemp80) + ((0.0272365008f * fTemp74) + ((0.0198335573f * fTemp125) + ((0.0242723655f * fTemp58) + ((0.03017544f * fTemp54) + ((0.0179721937f * fTemp92) + (((0.00229224609f * fTemp49) + ((0.0232048221f * fTemp102) + ((((0.0464762598f * fTemp105) + (0.0285261497f * fTemp36)) + (0.0376785174f * fTemp109)) + (0.0170691777f * fTemp33)))) + (0.0076942537f * fTemp29)))))))));
			output12[i] = FAUSTFLOAT((0.994903028f * (fRec0[0] * fVec11[((IOTA - iConst27) & 31)])));
			fVec12[(IOTA & 31)] = (((((0.0017043032f * fTemp63) + ((0.00217205891f * fTemp58) + ((0.0215617754f * fTemp92) + ((0.000286152906f * fTemp23) + ((0.0196337923f * fTemp115) + (((0.00145378441f * fTemp36) + ((0.023518106f * fTemp37) + (0.0200154874f * fTemp40))) + (0.0261591207f * fTemp49))))))) + (0.00191671751f * fTemp74)) + (0.0334158689f * fTemp80)) - (((((0.0215494651f * fTemp54) + ((0.00793956313f * fTemp120) + ((0.00384289213f * fTemp11) + ((0.00446137739f * fTemp17) + ((0.0161336474f * fTemp98) + ((0.0151201384f * fTemp29) + ((0.0306292567f * fTemp102) + ((0.0051292195f * fTemp33) + ((0.0357862934f * fTemp109) + ((0.033879105f * fTemp105) + (0.0021929564f * fTemp44))))))))))) + (0.0365033634f * fTemp125)) + (0.00226526242f * fTemp68)) + (0.00398613233f * fTemp86)));
			output13[i] = FAUSTFLOAT((0.994459808f * (fRec0[0] * fVec12[((IOTA - iConst26) & 31)])));
			fVec13[(IOTA & 31)] = (((0.00235756068f * fTemp86) + ((0.0265826583f * fTemp74) + ((0.017853152f * fTemp58) + ((0.0122532919f * fTemp120) + ((0.00259112311f * fTemp98) + (((0.0242112409f * fTemp36) + ((0.0307002682f * fTemp37) + (0.0179308224f * fTemp40))) + (0.0292704999f * fTemp115))))))) - ((0.00158926437f * fTemp80) + ((0.0393761285f * fTemp68) + ((0.017946979f * fTemp125) + ((0.00663466984f * fTemp63) + ((0.026677642f * fTemp54) + ((0.0334579162f * fTemp11) + ((0.0163302906f * fTemp92) + ((0.0365764908f * fTemp17) + ((0.0170068163f * fTemp23) + (((0.000849328004f * fTemp49) + ((0.0220802464f * fTemp102) + ((0.019253334f * fTemp33) + ((0.0297232959f * fTemp109) + ((0.0410771593f * fTemp105) + (0.0396812446f * fTemp44)))))) + (0.0011163837f * fTemp29))))))))))));
			output14[i] = FAUSTFLOAT((0.994903028f * (fRec0[0] * fVec13[((IOTA - iConst27) & 31)])));
			fVec14[(IOTA & 31)] = (((0.00747441268f * fTemp68) + ((0.018147653f * fTemp125) + ((0.026488414f * fTemp58) + ((0.00146507018f * fTemp120) + ((0.00329523883f * fTemp17) + ((((0.0335719511f * fTemp36) + ((0.0243119132f * fTemp37) + (0.015737446f * fTemp40))) + (0.0222150907f * fTemp102)) + (0.0120821306f * fTemp115))))))) - ((0.0039000127f * fTemp86) + ((0.0321706124f * fTemp80) + ((0.0268342551f * fTemp74) + ((0.00707615074f * fTemp63) + ((0.0223725922f * fTemp54) + ((0.0276765991f * fTemp11) + ((0.00491875922f * fTemp92) + ((0.00252732239f * fTemp98) + ((0.0244832207f * fTemp23) + (((0.0313324779f * fTemp49) + ((0.0132887159f * fTemp33) + ((0.014501893f * fTemp109) + ((0.0166117605f * fTemp105) + (0.0282988027f * fTemp44))))) + (0.0048309518f * fTemp29))))))))))));
			output15[i] = FAUSTFLOAT((0.994903028f * (fRec0[0] * fVec14[((IOTA - iConst27) & 31)])));
			fVec15[(IOTA & 31)] = (((0.0261732973f * fTemp68) + ((0.036511451f * fTemp125) + ((0.00792536978f * fTemp63) + (((((((0.0338919684f * fTemp36) + ((0.0235254578f * fTemp37) + (0.0200174712f * fTemp40))) + (0.0306433197f * fTemp102)) + (0.0161205065f * fTemp98)) + (0.0334273726f * fTemp17)) + (0.0215745289f * fTemp92)) + (0.0357914977f * fTemp58))))) - ((0.00398789532f * fTemp86) + ((0.00448323507f * fTemp80) + (((((((((0.00228837901f * fTemp49) + ((0.00513874181f * fTemp33) + ((0.0021808676f * fTemp109) + ((0.00146376574f * fTemp105) + (0.00221101847f * fTemp44))))) + (0.000280491891f * fTemp115)) + (0.0151117956f * fTemp29)) + (0.0196501091f * fTemp23)) + (0.0038598862f * fTemp11)) + (0.00170406105f * fTemp120)) + (0.0215570088f * fTemp54)) + (0.00194202363f * fTemp74)))));
			output16[i] = FAUSTFLOAT((0.994459808f * (fRec0[0] * fVec15[((IOTA - iConst26) & 31)])));
			fVec16[(IOTA & 31)] = (((0.00651655998f * fTemp86) + ((0.0422659703f * fTemp80) + ((0.0272349734f * fTemp74) + ((0.0198304765f * fTemp125) + ((0.0376720801f * fTemp58) + ((0.0420628637f * fTemp11) + ((0.0422858559f * fTemp49) + ((0.0232067332f * fTemp102) + ((0.0242657457f * fTemp109) + ((0.0446238518f * fTemp44) + ((0.0464785807f * fTemp36) + ((0.0244102012f * fTemp40) + ((0.0363168567f * fTemp37) + (0.0285257287f * fTemp105)))))))))))))) - ((0.0022937879f * fTemp68) + ((0.0062296018f * fTemp63) + ((0.0301687401f * fTemp54) + ((0.00163434329f * fTemp120) + ((0.0179738328f * fTemp92) + ((0.00448196707f * fTemp17) + ((0.000834875915f * fTemp98) + ((0.0285965391f * fTemp23) + ((0.00768448226f * fTemp29) + ((0.0170746874f * fTemp33) + (0.0161899403f * fTemp115))))))))))));
			output17[i] = FAUSTFLOAT((0.994903028f * (fRec0[0] * fVec16[((IOTA - iConst27) & 31)])));
			fVec17[(IOTA & 31)] = (((0.0028538194f * fTemp86) + ((0.013463459f * fTemp58) + ((0.0264879279f * fTemp11) + (((0.0270859804f * fTemp109) + (((0.015851954f * fTemp36) + ((0.0157361906f * fTemp40) + ((0.0242996197f * fTemp37) + (0.0339871384f * fTemp105)))) + (0.0275524594f * fTemp44))) + (0.000692206377f * fTemp98))))) - ((0.00647498108f * fTemp80) + ((0.0276871976f * fTemp74) + ((0.0313103572f * fTemp68) + ((0.020199718f * fTemp125) + ((0.00211028522f * fTemp63) + ((0.0223753918f * fTemp54) + ((0.00668762578f * fTemp120) + ((0.00339932903f * fTemp92) + ((0.032163851f * fTemp17) + ((0.0119247204f * fTemp23) + ((0.00484369416f * fTemp29) + (((0.00920206681f * fTemp49) + ((0.0132781463f * fTemp33) + (0.0234640837f * fTemp102))) + (0.0245817527f * fTemp115))))))))))))));
			output18[i] = FAUSTFLOAT((0.994903028f * (fRec0[0] * fVec17[((IOTA - iConst27) & 31)])));
			fVec18[(IOTA & 63)] = (((0.0287602693f * fTemp80) + ((1.49483003e-05f * fTemp74) + ((1.78635e-05f * fTemp68) + ((0.0332020447f * fTemp125) + ((4.19029993e-06f * fTemp58) + ((0.0116322571f * fTemp54) + ((4.11829978e-06f * fTemp11) + ((0.0607517771f * fTemp92) + ((0.0121668335f * fTemp98) + ((7.65539971e-06f * fTemp23) + ((0.0106905513f * fTemp29) + (((0.0588501506f * fTemp37) + (0.0887992382f * fTemp105)) + (0.00293797976f * fTemp115))))))))))))) - ((6.36850018e-06f * fTemp86) + ((3.69029999e-06f * fTemp63) + ((0.023990212f * fTemp120) + (((0.0742083937f * fTemp49) + ((0.0842990875f * fTemp102) + (((((0.0229201801f * fTemp40) + (1.02769e-05f * fTemp36)) + (1.63954992e-05f * fTemp44)) + (0.0354953706f * fTemp109)) + (0.0305124186f * fTemp33)))) + (2.51770007e-06f * fTemp17))))));
			output19[i] = FAUSTFLOAT((0.990027726f * (fRec0[0] * fVec18[((IOTA - iConst28) & 63)])));
			fVec19[(IOTA & 63)] = (((0.0030837385f * fTemp86) + ((0.0445791222f * fTemp74) + ((0.0603009164f * fTemp68) + ((0.0213693716f * fTemp125) + ((0.00611096714f * fTemp63) + ((0.022038091f * fTemp58) + ((0.0198027026f * fTemp54) + ((0.0370234251f * fTemp11) + ((0.00178489066f * fTemp98) + ((0.00483221514f * fTemp29) + (((0.0475860611f * fTemp37) + (0.061254438f * fTemp105)) + (0.0133260442f * fTemp115)))))))))))) - ((9.05189972e-06f * fTemp80) + ((0.0105896434f * fTemp120) + ((0.0257193707f * fTemp92) + ((0.0376278572f * fTemp17) + (((1.24281996e-05f * fTemp49) + ((0.0336138234f * fTemp102) + ((0.0203053225f * fTemp33) + ((0.0381666757f * fTemp109) + (((0.0270938333f * fTemp40) + (0.0353607573f * fTemp36)) + (0.0582042076f * fTemp44)))))) + (0.0076983762f * fTemp23)))))));
			output20[i] = FAUSTFLOAT((0.990249336f * (fRec0[0] * fVec19[((IOTA - iConst29) & 63)])));
			fVec20[(IOTA & 63)] = ((((((((((((0.0672760159f * fTemp49) + (((0.0532584041f * fTemp37) + (0.042079974f * fTemp105)) + (0.0334427133f * fTemp102))) + (0.00312986341f * fTemp115)) + (0.00777581753f * fTemp29)) + (0.00435806718f * fTemp17)) + (0.0315297917f * fTemp11)) + (0.0156995766f * fTemp54)) + (0.0350273214f * fTemp58)) + (0.0148546649f * fTemp63)) + (0.00735984743f * fTemp68)) + (0.00816389639f * fTemp86)) - (((((((((0.0254155602f * fTemp33) + ((0.0189149324f * fTemp109) + (((0.0250215139f * fTemp40) + (0.067867741f * fTemp36)) + (0.0682127848f * fTemp44)))) + (0.00874696858f * fTemp23)) + (0.00159050908f * fTemp98)) + (0.0356963724f * fTemp92)) + (0.0104994113f * fTemp120)) + (0.0213414878f * fTemp125)) + (0.0441824496f * fTemp74)) + (0.0331903771f * fTemp80)));
			output21[i] = FAUSTFLOAT((0.990249336f * (fRec0[0] * fVec20[((IOTA - iConst29) & 63)])));
			fVec21[(IOTA & 63)] = (((1.73980004e-06f * fTemp86) + ((0.0239910036f * fTemp63) + ((0.0354968123f * fTemp58) + ((0.0116386283f * fTemp54) + ((1.71399995e-06f * fTemp11) + ((0.0607584752f * fTemp92) + ((0.0287690926f * fTemp17) + ((0.0106876018f * fTemp29) + (((1.76210006e-06f * fTemp49) + (((0.0588359646f * fTemp37) + (6.81699987e-07f * fTemp105)) + (0.0842888653f * fTemp102))) + (2.1492001e-06f * fTemp115)))))))))) - ((2.81260009e-06f * fTemp80) + ((9.60200055e-07f * fTemp74) + ((0.0742064863f * fTemp68) + ((0.0332071297f * fTemp125) + ((1.77360005e-06f * fTemp120) + ((0.0121721542f * fTemp98) + (((0.0305128377f * fTemp33) + ((3.29199992e-07f * fTemp109) + (((0.0229164064f * fTemp40) + (0.0887830555f * fTemp36)) + (1.60189995e-06f * fTemp44)))) + (0.0029384573f * fTemp23)))))))));
			output22[i] = FAUSTFLOAT((0.990027726f * (fRec0[0] * fVec21[((IOTA - iConst28) & 63)])));
			fVec22[(IOTA & 63)] = (((0.0331928693f * fTemp80) + ((0.0441909097f * fTemp74) + ((0.00734717352f * fTemp68) + ((0.0148723898f * fTemp63) + ((0.035022445f * fTemp58) + ((0.0157222096f * fTemp54) + ((0.0105118724f * fTemp120) + ((0.00436064741f * fTemp17) + (((0.0334550142f * fTemp102) + ((0.0189076494f * fTemp109) + ((0.0532494672f * fTemp37) + (0.0682108104f * fTemp44)))) + (0.00776369777f * fTemp29)))))))))) - ((0.00817627273f * fTemp86) + ((0.0213441104f * fTemp125) + ((0.0315268077f * fTemp11) + ((0.0356898531f * fTemp92) + ((0.00159648992f * fTemp98) + ((0.00875738822f * fTemp23) + (((0.0672813505f * fTemp49) + ((((0.0420738682f * fTemp105) + (0.0250031725f * fTemp40)) + (0.0678718835f * fTemp36)) + (0.0254423618f * fTemp33))) + (0.0031433471f * fTemp115)))))))));
			output23[i] = FAUSTFLOAT((0.990249336f * (fRec0[0] * fVec22[((IOTA - iConst29) & 63)])));
			fVec23[(IOTA & 63)] = (((6.7400002e-08f * fTemp80) + ((0.0603186153f * fTemp68) + ((0.0213689711f * fTemp125) + ((0.00610464113f * fTemp63) + ((0.0220359024f * fTemp58) + ((0.019768076f * fTemp54) + ((0.010592496f * fTemp120) + ((0.00179317046f * fTemp98) + (((2.35727002e-05f * fTemp49) + ((0.0381643772f * fTemp109) + ((0.0476111248f * fTemp37) + (0.0582197383f * fTemp44)))) + (0.00486049941f * fTemp29)))))))))) - ((0.00307886605f * fTemp86) + ((0.0445941202f * fTemp74) + ((0.0370129459f * fTemp11) + ((0.0257153399f * fTemp92) + ((0.0376170389f * fTemp17) + ((0.00767372129f * fTemp23) + (((0.0336324349f * fTemp102) + ((((0.0612792261f * fTemp105) + (0.0271065626f * fTemp40)) + (0.0353708342f * fTemp36)) + (0.0202950761f * fTemp33))) + (0.013300335f * fTemp115)))))))));
			output24[i] = FAUSTFLOAT((0.990249336f * (fRec0[0] * fVec23[((IOTA - iConst29) & 63)])));
			fVec24[(IOTA & 63)] = (((1.09480004e-06f * fTemp74) + ((0.0331940688f * fTemp125) + ((1.66199996e-07f * fTemp63) + ((2.9099001e-06f * fTemp58) + ((0.011640396f * fTemp54) + ((0.0240037106f * fTemp120) + ((0.0607701056f * fTemp92) + ((0.0121762846f * fTemp98) + (((0.0742251649f * fTemp49) + (((0.0588502251f * fTemp37) + (4.36999983e-08f * fTemp36)) + (0.0354840085f * fTemp109))) + (0.0106761474f * fTemp29)))))))))) - ((2.68980011e-06f * fTemp86) + ((0.0287560839f * fTemp80) + ((6.38699987e-07f * fTemp68) + ((4.66890015e-06f * fTemp11) + ((5.52739994e-06f * fTemp17) + ((6.12999997e-08f * fTemp23) + (((0.0843136683f * fTemp102) + ((0.0305345897f * fTemp33) + ((4.06400005e-07f * fTemp44) + ((0.0888105035f * fTemp105) + (0.0229036063f * fTemp40))))) + (0.00293079647f * fTemp115)))))))));
			output25[i] = FAUSTFLOAT((0.990027726f * (fRec0[0] * fVec24[((IOTA - iConst28) & 63)])));
			fVec25[(IOTA & 63)] = (((0.00309207453f * fTemp86) + ((0.0445668623f * fTemp74) + ((0.0213809013f * fTemp125) + ((0.01976908f * fTemp54) + ((0.0105883619f * fTemp120) + ((0.0370202139f * fTemp11) + ((0.0376309529f * fTemp17) + ((0.00177841587f * fTemp98) + ((0.00767904613f * fTemp23) + ((((0.0476081558f * fTemp37) + (0.0353744999f * fTemp36)) + (0.03817489f * fTemp109)) + (0.00487746019f * fTemp29))))))))))) - ((9.20010007e-06f * fTemp80) + ((0.0603082888f * fTemp68) + ((0.00611473899f * fTemp63) + ((0.022037847f * fTemp58) + ((0.0257394761f * fTemp92) + (((5.4594002e-06f * fTemp49) + ((0.03361056f * fTemp102) + ((0.0202873833f * fTemp33) + (((0.0612672567f * fTemp105) + (0.0271157604f * fTemp40)) + (0.0582216233f * fTemp44))))) + (0.0133118471f * fTemp115))))))));
			output26[i] = FAUSTFLOAT((0.990249336f * (fRec0[0] * fVec25[((IOTA - iConst29) & 63)])));
			fVec26[(IOTA & 63)] = (((0.00817391276f * fTemp86) + ((0.033195816f * fTemp80) + ((0.0157154445f * fTemp54) + ((0.0105084423f * fTemp120) + ((0.0315313675f * fTemp11) + ((0.00874163397f * fTemp23) + (((0.0334601849f * fTemp102) + (((0.0532465205f * fTemp37) + (0.0678689703f * fTemp36)) + (0.0189102255f * fTemp109))) + (0.00776711432f * fTemp29)))))))) - ((0.0442019254f * fTemp74) + ((0.00733538205f * fTemp68) + ((0.0213397481f * fTemp125) + ((0.0148645388f * fTemp63) + ((0.0350250863f * fTemp58) + ((0.0356800146f * fTemp92) + ((0.0043497989f * fTemp17) + ((0.0015972571f * fTemp98) + (((0.0672811568f * fTemp49) + ((0.025433531f * fTemp33) + (((0.04206644f * fTemp105) + (0.0250075795f * fTemp40)) + (0.0682025552f * fTemp44)))) + (0.00314469915f * fTemp115)))))))))));
			output27[i] = FAUSTFLOAT((0.990249336f * (fRec0[0] * fVec26[((IOTA - iConst29) & 63)])));
			fVec27[(IOTA & 63)] = (((3.56860005e-06f * fTemp86) + ((0.07421875f * fTemp68) + ((0.0116383452f * fTemp54) + ((2.25329995e-06f * fTemp120) + ((0.060766831f * fTemp92) + ((0.00293350592f * fTemp23) + ((0.0106785139f * fTemp29) + ((((0.0887977406f * fTemp36) + ((0.0588410161f * fTemp37) + (1.94500004e-07f * fTemp105))) + (0.0843039677f * fTemp102)) + (1.34299995e-07f * fTemp115))))))))) - ((8.39889981e-06f * fTemp80) + ((1.65259996e-06f * fTemp74) + ((0.0331902243f * fTemp125) + ((0.0240087509f * fTemp63) + ((0.0354756378f * fTemp58) + ((7.63729986e-06f * fTemp11) + ((0.0287575163f * fTemp17) + (((1.10400003e-06f * fTemp49) + ((0.0305338241f * fTemp33) + ((4.29940019e-06f * fTemp109) + ((0.0228981767f * fTemp40) + (3.24199988e-07f * fTemp44))))) + (0.0121802175f * fTemp98))))))))));
			output28[i] = FAUSTFLOAT((0.990027726f * (fRec0[0] * fVec27[((IOTA - iConst28) & 63)])));
			fVec28[(IOTA & 63)] = (((0.0445954353f * fTemp74) + ((2.04370008e-05f * fTemp68) + ((0.0197647121f * fTemp54) + ((3.01039995e-06f * fTemp17) + ((0.0132962577f * fTemp23) + ((0.00486262236f * fTemp29) + (((0.0603153482f * fTemp49) + ((0.0336267091f * fTemp102) + (((0.0612718165f * fTemp36) + ((0.0476072319f * fTemp37) + (0.0353668891f * fTemp105))) + (0.0582146868f * fTemp44)))) + (0.00768212462f * fTemp115)))))))) - ((0.00308199762f * fTemp86) + ((0.0376159959f * fTemp80) + ((0.0213693399f * fTemp125) + ((0.0105815213f * fTemp63) + ((0.0381675474f * fTemp58) + ((0.00610750495f * fTemp120) + ((0.0370129235f * fTemp11) + ((0.0257175434f * fTemp92) + (((0.0202854425f * fTemp33) + ((0.0271103531f * fTemp40) + (0.0220347978f * fTemp109))) + (0.00178265246f * fTemp98)))))))))));
			output29[i] = FAUSTFLOAT((0.990249336f * (fRec0[0] * fVec28[((IOTA - iConst29) & 63)])));
			fVec29[(IOTA & 63)] = (((0.00434307195f * fTemp80) + ((0.0213273577f * fTemp125) + ((0.0157040954f * fTemp54) + ((0.0331791602f * fTemp17) + ((0.00159112422f * fTemp98) + ((0.00313719059f * fTemp23) + ((0.00776614109f * fTemp29) + (((((0.042072583f * fTemp36) + ((0.0532519892f * fTemp37) + (0.0678741783f * fTemp105))) + (0.0682138726f * fTemp44)) + (0.00734153436f * fTemp49)) + (0.00873184763f * fTemp115))))))))) - ((0.0081731705f * fTemp86) + ((0.0442137532f * fTemp74) + ((0.0672949478f * fTemp68) + ((0.010509165f * fTemp63) + ((0.0189079586f * fTemp58) + ((0.0148637323f * fTemp120) + ((0.0315223634f * fTemp11) + (((((0.025004046f * fTemp40) + (0.0350133739f * fTemp109)) + (0.0254328717f * fTemp33)) + (0.0334611647f * fTemp102)) + (0.0356942602f * fTemp92))))))))));
			output30[i] = FAUSTFLOAT((0.990249336f * (fRec0[0] * fVec29[((IOTA - iConst29) & 63)])));
			fRec0[1] = fRec0[0];
			fRec1[1] = fRec1[0];
			fRec11[1] = fRec11[0];
			fRec9[1] = fRec9[0];
			fRec8[1] = fRec8[0];
			fRec6[1] = fRec6[0];
			fRec5[1] = fRec5[0];
			fRec3[1] = fRec3[0];
			fRec2[2] = fRec2[1];
			fRec2[1] = fRec2[0];
			fRec12[1] = fRec12[0];
			fRec25[1] = fRec25[0];
			fRec23[1] = fRec23[0];
			fRec22[1] = fRec22[0];
			fRec20[1] = fRec20[0];
			fRec19[1] = fRec19[0];
			fRec17[1] = fRec17[0];
			fRec16[1] = fRec16[0];
			fRec14[1] = fRec14[0];
			fRec13[2] = fRec13[1];
			fRec13[1] = fRec13[0];
			fRec38[1] = fRec38[0];
			fRec36[1] = fRec36[0];
			fRec35[1] = fRec35[0];
			fRec33[1] = fRec33[0];
			fRec32[1] = fRec32[0];
			fRec30[1] = fRec30[0];
			fRec29[1] = fRec29[0];
			fRec27[1] = fRec27[0];
			fRec26[2] = fRec26[1];
			fRec26[1] = fRec26[0];
			fRec51[1] = fRec51[0];
			fRec49[1] = fRec49[0];
			fRec48[1] = fRec48[0];
			fRec46[1] = fRec46[0];
			fRec45[1] = fRec45[0];
			fRec43[1] = fRec43[0];
			fRec42[1] = fRec42[0];
			fRec40[1] = fRec40[0];
			fRec39[2] = fRec39[1];
			fRec39[1] = fRec39[0];
			fRec58[1] = fRec58[0];
			fRec56[1] = fRec56[0];
			fRec55[1] = fRec55[0];
			fRec53[1] = fRec53[0];
			fRec52[2] = fRec52[1];
			fRec52[1] = fRec52[0];
			fRec62[1] = fRec62[0];
			fRec60[1] = fRec60[0];
			fRec59[2] = fRec59[1];
			fRec59[1] = fRec59[0];
			fRec63[2] = fRec63[1];
			fRec63[1] = fRec63[0];
			fRec67[1] = fRec67[0];
			fRec65[1] = fRec65[0];
			fRec64[2] = fRec64[1];
			fRec64[1] = fRec64[0];
			fRec74[1] = fRec74[0];
			fRec72[1] = fRec72[0];
			fRec71[1] = fRec71[0];
			fRec69[1] = fRec69[0];
			fRec68[2] = fRec68[1];
			fRec68[1] = fRec68[0];
			fRec84[1] = fRec84[0];
			fRec82[1] = fRec82[0];
			fRec81[1] = fRec81[0];
			fRec79[1] = fRec79[0];
			fRec78[1] = fRec78[0];
			fRec76[1] = fRec76[0];
			fRec75[2] = fRec75[1];
			fRec75[1] = fRec75[0];
			fRec94[1] = fRec94[0];
			fRec92[1] = fRec92[0];
			fRec91[1] = fRec91[0];
			fRec89[1] = fRec89[0];
			fRec88[1] = fRec88[0];
			fRec86[1] = fRec86[0];
			fRec85[2] = fRec85[1];
			fRec85[1] = fRec85[0];
			fRec101[1] = fRec101[0];
			fRec99[1] = fRec99[0];
			fRec98[1] = fRec98[0];
			fRec96[1] = fRec96[0];
			fRec95[2] = fRec95[1];
			fRec95[1] = fRec95[0];
			fRec111[1] = fRec111[0];
			fRec109[1] = fRec109[0];
			fRec108[1] = fRec108[0];
			fRec106[1] = fRec106[0];
			fRec105[1] = fRec105[0];
			fRec103[1] = fRec103[0];
			fRec102[2] = fRec102[1];
			fRec102[1] = fRec102[0];
			fRec121[1] = fRec121[0];
			fRec119[1] = fRec119[0];
			fRec118[1] = fRec118[0];
			fRec116[1] = fRec116[0];
			fRec115[1] = fRec115[0];
			fRec113[1] = fRec113[0];
			fRec112[2] = fRec112[1];
			fRec112[1] = fRec112[0];
			fRec134[1] = fRec134[0];
			fRec132[1] = fRec132[0];
			fRec131[1] = fRec131[0];
			fRec129[1] = fRec129[0];
			fRec128[1] = fRec128[0];
			fRec126[1] = fRec126[0];
			fRec125[1] = fRec125[0];
			fRec123[1] = fRec123[0];
			fRec122[2] = fRec122[1];
			fRec122[1] = fRec122[0];
			fRec147[1] = fRec147[0];
			fRec145[1] = fRec145[0];
			fRec144[1] = fRec144[0];
			fRec142[1] = fRec142[0];
			fRec141[1] = fRec141[0];
			fRec139[1] = fRec139[0];
			fRec138[1] = fRec138[0];
			fRec136[1] = fRec136[0];
			fRec135[2] = fRec135[1];
			fRec135[1] = fRec135[0];
			fRec160[1] = fRec160[0];
			fRec158[1] = fRec158[0];
			fRec157[1] = fRec157[0];
			fRec155[1] = fRec155[0];
			fRec154[1] = fRec154[0];
			fRec152[1] = fRec152[0];
			fRec151[1] = fRec151[0];
			fRec149[1] = fRec149[0];
			fRec148[2] = fRec148[1];
			fRec148[1] = fRec148[0];
			fRec173[1] = fRec173[0];
			fRec171[1] = fRec171[0];
			fRec170[1] = fRec170[0];
			fRec168[1] = fRec168[0];
			fRec167[1] = fRec167[0];
			fRec165[1] = fRec165[0];
			fRec164[1] = fRec164[0];
			fRec162[1] = fRec162[0];
			fRec161[2] = fRec161[1];
			fRec161[1] = fRec161[0];
			fRec186[1] = fRec186[0];
			fRec184[1] = fRec184[0];
			fRec183[1] = fRec183[0];
			fRec181[1] = fRec181[0];
			fRec180[1] = fRec180[0];
			fRec178[1] = fRec178[0];
			fRec177[1] = fRec177[0];
			fRec175[1] = fRec175[0];
			fRec174[2] = fRec174[1];
			fRec174[1] = fRec174[0];
			fRec193[1] = fRec193[0];
			fRec191[1] = fRec191[0];
			fRec190[1] = fRec190[0];
			fRec188[1] = fRec188[0];
			fRec187[2] = fRec187[1];
			fRec187[1] = fRec187[0];
			fRec197[1] = fRec197[0];
			fRec195[1] = fRec195[0];
			fRec194[2] = fRec194[1];
			fRec194[1] = fRec194[0];
			fRec204[1] = fRec204[0];
			fRec202[1] = fRec202[0];
			fRec201[1] = fRec201[0];
			fRec199[1] = fRec199[0];
			fRec198[2] = fRec198[1];
			fRec198[1] = fRec198[0];
			fRec217[1] = fRec217[0];
			fRec215[1] = fRec215[0];
			fRec214[1] = fRec214[0];
			fRec212[1] = fRec212[0];
			fRec211[1] = fRec211[0];
			fRec209[1] = fRec209[0];
			fRec208[1] = fRec208[0];
			fRec206[1] = fRec206[0];
			fRec205[2] = fRec205[1];
			fRec205[1] = fRec205[0];
			fRec227[1] = fRec227[0];
			fRec225[1] = fRec225[0];
			fRec224[1] = fRec224[0];
			fRec222[1] = fRec222[0];
			fRec221[1] = fRec221[0];
			fRec219[1] = fRec219[0];
			fRec218[2] = fRec218[1];
			fRec218[1] = fRec218[0];
			fRec237[1] = fRec237[0];
			fRec235[1] = fRec235[0];
			fRec234[1] = fRec234[0];
			fRec232[1] = fRec232[0];
			fRec231[1] = fRec231[0];
			fRec229[1] = fRec229[0];
			fRec228[2] = fRec228[1];
			fRec228[1] = fRec228[0];
			for (int j0 = 11; (j0 > 0); j0 = (j0 - 1)) {
				fVec0[j0] = fVec0[(j0 - 1)];
				
			}
			for (int j1 = 12; (j1 > 0); j1 = (j1 - 1)) {
				fVec1[j1] = fVec1[(j1 - 1)];
				
			}
			for (int j2 = 11; (j2 > 0); j2 = (j2 - 1)) {
				fVec2[j2] = fVec2[(j2 - 1)];
				
			}
			for (int j3 = 11; (j3 > 0); j3 = (j3 - 1)) {
				fVec3[j3] = fVec3[(j3 - 1)];
				
			}
			for (int j4 = 12; (j4 > 0); j4 = (j4 - 1)) {
				fVec4[j4] = fVec4[(j4 - 1)];
				
			}
			for (int j5 = 11; (j5 > 0); j5 = (j5 - 1)) {
				fVec5[j5] = fVec5[(j5 - 1)];
				
			}
			IOTA = (IOTA + 1);
			
		}
		
	}

	
};

//----------------------------------------------------------------------------
// SuperCollider/Faust interface
//----------------------------------------------------------------------------

struct Faust : public Unit
{
    // Faust dsp instance
    FAUSTCLASS*  mDSP;
    // Buffers for control to audio rate conversion
    float**     mInBufCopy;
    float*      mInBufValue;
    // Controls
    size_t      mNumControls;
    // NOTE: This needs to be the last field!
    //
    // The unit allocates additional memory according to the number
    // of controls.
    Control     mControls[0];

    int getNumAudioInputs() { return mDSP->getNumInputs(); }
};

// Global state

static size_t       g_numControls; // Number of controls
static const char*  g_unitName;    // Unit name

// Initialize the global state with unit name and sample rate.
void initState(const std::string& name, int sampleRate);

// Return the unit size in bytes, including static fields and controls.
static size_t unitSize();

// Convert a file name to a valid unit name.
static std::string fileNameToUnitName(const std::string& fileName);

// Convert the XML unit name to a valid class name.
static std::string normalizeClassName(const std::string& name);

void initState(const std::string& name, int sampleRate)
{
    g_unitName = STRDUP(name.c_str());

    mydsp* dsp = new FAUSTCLASS;
    ControlCounter* cc = new ControlCounter;

    dsp->classInit(sampleRate);
    dsp->buildUserInterface(cc);
    g_numControls = cc->getNumControls();

    delete dsp;
    delete cc;
}

size_t unitSize()
{
    return sizeof(Faust) + g_numControls * sizeof(Control);
}

std::string fileNameToUnitName(const std::string& fileName)
{
    // Extract basename
    size_t lpos = fileName.rfind('/', fileName.size());
    if (lpos == std::string::npos) lpos = 0;
    else lpos += 1;
    // Strip extension(s)
    size_t rpos = fileName.find('.', lpos);
    // Return substring
    return fileName.substr(lpos, rpos > lpos ? rpos - lpos : 0);
}

// Globals

static InterfaceTable* ft;

// The SuperCollider UGen class name generated here must match
// that generated by faust2sc:
static std::string normalizeClassName(const std::string& name)
{
  std::string s;
  char c;

  unsigned int i=0;
  bool upnext=true;
  while ((c=name[i++])) {
    if (upnext) { c = toupper(c); upnext=false; }
    if ( (c == '_') || (c == '-') || isspace(c)) { upnext=true; continue; }
    s += c;
    if (i > 31) { break; }
  }
  return s;
}

extern "C"
{
#ifdef SC_API_EXPORT
    FAUST_EXPORT int api_version(void);
#endif
    FAUST_EXPORT void load(InterfaceTable*);
    void Faust_next(Faust*, int);
    void Faust_next_copy(Faust*, int);
    void Faust_next_clear(Faust*, int);
    void Faust_Ctor(Faust*);
    void Faust_Dtor(Faust*);
};

inline static void fillBuffer(float* dst, int n, float v)
{
    Fill(n, dst, v);
}

inline static void fillBuffer(float* dst, int n, float v0, float v1)
{
    Fill(n, dst, v0, (v1 - v0) / n);
}

inline static void copyBuffer(float* dst, int n, float* src)
{
    Copy(n, dst, src);
}

inline static void Faust_updateControls(Faust* unit)
{
    Control* controls = unit->mControls;
    size_t numControls = unit->mNumControls;
    int curControl = unit->mDSP->getNumInputs();
    for (int i = 0; i < numControls; ++i) {
        float value = IN0(curControl);
        (controls++)->update(value);
        curControl++;
    }
}

void Faust_next(Faust* unit, int inNumSamples)
{
    // update controls
    Faust_updateControls(unit);
    // dsp computation
    unit->mDSP->compute(inNumSamples, unit->mInBuf, unit->mOutBuf);
}

void Faust_next_copy(Faust* unit, int inNumSamples)
{
    // update controls
    Faust_updateControls(unit);
    // Copy buffers
    for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
        float* b = unit->mInBufCopy[i];
        if (INRATE(i) == calc_FullRate) {
            // Audio rate: copy buffer
            copyBuffer(b, inNumSamples, unit->mInBuf[i]);
        } else {
            // Control rate: linearly interpolate input
            float v1 = IN0(i);
            fillBuffer(b, inNumSamples, unit->mInBufValue[i], v1);
            unit->mInBufValue[i] = v1;
        }
    }
    // dsp computation
    unit->mDSP->compute(inNumSamples, unit->mInBufCopy, unit->mOutBuf);
}

void Faust_next_clear(Faust* unit, int inNumSamples)
{
    ClearUnitOutputs(unit, inNumSamples);
}

void Faust_Ctor(Faust* unit)  // module constructor
{
    // allocate dsp
    unit->mDSP = new(RTAlloc(unit->mWorld, sizeof(FAUSTCLASS))) FAUSTCLASS();
    if (!unit->mDSP) {
        Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
        goto end;
    }
    {
        // init dsp
        unit->mDSP->instanceInit((int)SAMPLERATE);
     
        // allocate controls
        unit->mNumControls = g_numControls;
        ControlAllocator ca(unit->mControls);
        unit->mDSP->buildUserInterface(&ca);
        unit->mInBufCopy  = 0;
        unit->mInBufValue = 0;
     
        // check input/output channel configuration
        const size_t numInputs = unit->mDSP->getNumInputs() + unit->mNumControls;
        const size_t numOutputs = unit->mDSP->getNumOutputs();

        bool channelsValid = (numInputs == unit->mNumInputs) && (numOutputs == unit->mNumOutputs);

        if (channelsValid) {
            bool rateValid = true;
            for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
                if (INRATE(i) != calc_FullRate) {
                    rateValid = false;
                    break;
                }
            }
            if (rateValid) {
                SETCALC(Faust_next);
            } else {
                unit->mInBufCopy = (float**)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*sizeof(float*));
                if (!unit->mInBufCopy) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                // Allocate memory for input buffer copies (numInputs * bufLength)
                // and linear interpolation state (numInputs)
                // = numInputs * (bufLength + 1)
                unit->mInBufValue = (float*)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*sizeof(float));
                if (!unit->mInBufValue) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                // Aquire memory for interpolator state.
                float* mem = (float*)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*BUFLENGTH*sizeof(float));
                if (mem) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
                    // Initialize interpolator.
                    unit->mInBufValue[i] = IN0(i);
                    // Aquire buffer memory.
                    unit->mInBufCopy[i] = mem;
                    mem += BUFLENGTH;
                }
                SETCALC(Faust_next_copy);
            }
    #if !defined(NDEBUG)
            Print("Faust[%s]:\n", g_unitName);
            Print("    Inputs:   %d\n"
                  "    Outputs:  %d\n"
                  "    Callback: %s\n",
                  numInputs, numOutputs,
                  unit->mCalcFunc == (UnitCalcFunc)Faust_next ? "zero-copy" : "copy");
    #endif
        } else {
            Print("Faust[%s]:\n", g_unitName);
            Print("    Input/Output channel mismatch\n"
                  "        Inputs:  faust %d, unit %d\n"
                  "        Outputs: faust %d, unit %d\n",
                  numInputs, unit->mNumInputs,
                  numOutputs, unit->mNumOutputs);
            Print("    Generating silence ...\n");
            SETCALC(Faust_next_clear);
        }
    }
    
end:
    // Fix for https://github.com/grame-cncm/faust/issues/13
    ClearUnitOutputs(unit, 1);
}

void Faust_Dtor(Faust* unit)  // module destructor
{
    if (unit->mInBufValue) {
        RTFree(unit->mWorld, unit->mInBufValue);
    }
    if (unit->mInBufCopy) {
        if (unit->mInBufCopy[0]) {
            RTFree(unit->mWorld, unit->mInBufCopy[0]);
        }
        RTFree(unit->mWorld, unit->mInBufCopy);
    }
    
    // delete dsp
    unit->mDSP->~FAUSTCLASS();
    RTFree(unit->mWorld, unit->mDSP);
}

#ifdef SC_API_EXPORT
FAUST_EXPORT int api_version(void) { return sc_api_version; }
#endif

FAUST_EXPORT void load(InterfaceTable* inTable)
{
    ft = inTable;

    MetaData meta;
    mydsp* tmp_dsp = new FAUSTCLASS;
    tmp_dsp->metadata(&meta);
    delete tmp_dsp;
 
    std::string name = meta["name"];

    if (name.empty()) {
        name = fileNameToUnitName(__FILE__);
    }
  
    name = normalizeClassName(name);

#if !defined(NDEBUG) & defined(SC_API_EXPORT)
    Print("Faust: supercollider.cpp: sc_api_version = %d\n", sc_api_version);
#endif

    if (name.empty()) {
        // Catch empty name
        Print("Faust [supercollider.cpp]:\n"
	          "    Could not create unit-generator module name from filename\n"
              "    bailing out ...\n");
        return;
    }

    if (strncmp(name.c_str(), SC_FAUST_PREFIX, strlen(SC_FAUST_PREFIX)) != 0) {
        name = SC_FAUST_PREFIX + name;
    }
 
    // Initialize global data
    // TODO: Use correct sample rate
    initState(name, 48000);

    // Register ugen
    (*ft->fDefineUnit)(
        (char*)name.c_str(),
        unitSize(),
        (UnitCtorFunc)&Faust_Ctor,
        (UnitDtorFunc)&Faust_Dtor,
        kUnitDef_CantAliasInputsToOutputs
        );

#if !defined(NDEBUG)
    Print("Faust: %s numControls=%d\n", name.c_str(), g_numControls);
#endif // NDEBUG
}

#ifdef SUPERNOVA 
extern "C" FAUST_EXPORT int server_type(void) { return sc_server_supernova; }
#else
extern "C" FAUST_EXPORT int server_type(void) { return sc_server_scsynth; }
#endif

// EOF

#endif
