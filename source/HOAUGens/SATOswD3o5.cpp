/* ------------------------------------------------------------
author: "AmbisonicDecoderToolkit"
copyright: "(c) Aaron J. Heller 2013"
license: "BSD 3-Clause License"
name: "SATOswD3o5"
version: "1.2"
Code generated with Faust 2.5.12 (https://faust.grame.fr)
Compilation options: cpp, -scal -ftz 0
------------------------------------------------------------ */

#ifndef  __mydsp_H__
#define  __mydsp_H__

//-------------------------------------------------------------------
// FAUST architecture file for SuperCollider.
// Copyright (C) 2005-2012 Stefan Kersten.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//-------------------------------------------------------------------

// If other than 'faust2sc --prefix Faust' is used, sed this as well:
#if !defined(SC_FAUST_PREFIX)
#define SC_FAUST_PREFIX "Faust"
#endif

#include <map>
#include <string>
#include <string.h>
#include <SC_PlugIn.h>

/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __dsp__
#define __dsp__

#include <string>

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

class UI;
struct Meta;

/**
 * DSP memory manager.
 */

struct dsp_memory_manager {
    
    virtual ~dsp_memory_manager() {}
    
    virtual void* allocate(size_t size) = 0;
    virtual void destroy(void* ptr) = 0;
    
};

/**
* Signal processor definition.
*/

class dsp {

    public:

        dsp() {}
        virtual ~dsp() {}

        /* Return instance number of audio inputs */
        virtual int getNumInputs() = 0;
    
        /* Return instance number of audio outputs */
        virtual int getNumOutputs() = 0;
    
        /**
         * Trigger the ui_interface parameter with instance specific calls
         * to 'addBtton', 'addVerticalSlider'... in order to build the UI.
         *
         * @param ui_interface - the user interface builder
         */
        virtual void buildUserInterface(UI* ui_interface) = 0;
    
        /* Returns the sample rate currently used by the instance */
        virtual int getSampleRate() = 0;
    
        /**
         * Global init, calls the following methods:
         * - static class 'classInit': static tables initialization
         * - 'instanceInit': constants and instance state initialization
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void init(int samplingRate) = 0;

        /**
         * Init instance state
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void instanceInit(int samplingRate) = 0;

        /**
         * Init instance constant state
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void instanceConstants(int samplingRate) = 0;
    
        /* Init default control parameters values */
        virtual void instanceResetUserInterface() = 0;
    
        /* Init instance state (delay lines...) */
        virtual void instanceClear() = 0;
 
        /**
         * Return a clone of the instance.
         *
         * @return a copy of the instance on success, otherwise a null pointer.
         */
        virtual dsp* clone() = 0;
    
        /**
         * Trigger the Meta* parameter with instance specific calls to 'declare' (key, value) metadata.
         *
         * @param m - the Meta* meta user
         */
        virtual void metadata(Meta* m) = 0;
    
        /**
         * DSP instance computation, to be called with successive in/out audio buffers.
         *
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         *
         */
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) = 0;
    
        /**
         * DSP instance computation: alternative method to be used by subclasses.
         *
         * @param date_usec - the timestamp in microsec given by audio driver.
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         *
         */
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { compute(count, inputs, outputs); }
       
};

/**
 * Generic DSP decorator.
 */

class decorator_dsp : public dsp {

    protected:

        dsp* fDSP;

    public:

        decorator_dsp(dsp* dsp = 0):fDSP(dsp) {}
        virtual ~decorator_dsp() { delete fDSP; }

        virtual int getNumInputs() { return fDSP->getNumInputs(); }
        virtual int getNumOutputs() { return fDSP->getNumOutputs(); }
        virtual void buildUserInterface(UI* ui_interface) { fDSP->buildUserInterface(ui_interface); }
        virtual int getSampleRate() { return fDSP->getSampleRate(); }
        virtual void init(int samplingRate) { fDSP->init(samplingRate); }
        virtual void instanceInit(int samplingRate) { fDSP->instanceInit(samplingRate); }
        virtual void instanceConstants(int samplingRate) { fDSP->instanceConstants(samplingRate); }
        virtual void instanceResetUserInterface() { fDSP->instanceResetUserInterface(); }
        virtual void instanceClear() { fDSP->instanceClear(); }
        virtual decorator_dsp* clone() { return new decorator_dsp(fDSP->clone()); }
        virtual void metadata(Meta* m) { fDSP->metadata(m); }
        // Beware: subclasses usually have to overload the two 'compute' methods
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(count, inputs, outputs); }
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(date_usec, count, inputs, outputs); }
    
};

/**
 * DSP factory class.
 */

class dsp_factory {
    
    protected:
    
        // So that to force sub-classes to use deleteDSPFactory(dsp_factory* factory);
        virtual ~dsp_factory() {}
    
    public:
    
        virtual std::string getName() = 0;
        virtual std::string getSHAKey() = 0;
        virtual std::string getDSPCode() = 0;
    
        virtual dsp* createDSPInstance() = 0;
    
        virtual void setMemoryManager(dsp_memory_manager* manager) = 0;
        virtual dsp_memory_manager* getMemoryManager() = 0;
    
};

/**
 * On Intel set FZ (Flush to Zero) and DAZ (Denormals Are Zero)
 * flags to avoid costly denormals.
 */

#ifdef __SSE__
    #include <xmmintrin.h>
    #ifdef __SSE2__
        #define AVOIDDENORMALS _mm_setcsr(_mm_getcsr() | 0x8040)
    #else
        #define AVOIDDENORMALS _mm_setcsr(_mm_getcsr() | 0x8000)
    #endif
#else
    #define AVOIDDENORMALS
#endif

#endif
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __UI_H__
#define __UI_H__

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

/*******************************************************************************
 * UI : Faust DSP User Interface
 * User Interface as expected by the buildUserInterface() method of a DSP.
 * This abstract class contains only the method that the Faust compiler can
 * generate to describe a DSP user interface.
 ******************************************************************************/

struct Soundfile;

class UI
{

    public:

        UI() {}

        virtual ~UI() {}

        // -- widget's layouts

        virtual void openTabBox(const char* label) = 0;
        virtual void openHorizontalBox(const char* label) = 0;
        virtual void openVerticalBox(const char* label) = 0;
        virtual void closeBox() = 0;

        // -- active widgets

        virtual void addButton(const char* label, FAUSTFLOAT* zone) = 0;
        virtual void addCheckButton(const char* label, FAUSTFLOAT* zone) = 0;
        virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;
        virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;
        virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;

        // -- passive widgets

        virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) = 0;
        virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) = 0;
    
        // -- soundfiles
    
        virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) = 0;

        // -- metadata declarations

        virtual void declare(FAUSTFLOAT*, const char*, const char*) {}
};

#endif
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/
 
#ifndef __misc__
#define __misc__

#include <algorithm>
#include <map>
#include <string.h>
#include <stdlib.h>

/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __meta__
#define __meta__

struct Meta
{
    virtual void declare(const char* key, const char* value) = 0;
    virtual ~Meta() {};
};

#endif

using std::max;
using std::min;

struct XXXX_Meta : std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key]=value; }
};

struct MY_Meta : Meta, std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key]=value; }
};

inline int lsr(int x, int n)	{ return int(((unsigned int)x) >> n); }

inline int int2pow2(int x)		{ int r = 0; while ((1<<r) < x) r++; return r; }

inline long lopt(char* argv[], const char* name, long def)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return atoi(argv[i+1]);
	return def;
}

inline bool isopt(char* argv[], const char* name)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return true;
	return false;
}

inline const char* lopts(char* argv[], const char* name, const char* def)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return argv[i+1];
	return def;
}

#endif


using namespace std;

#if defined(__GNUC__) && __GNUC__ >= 4
    #define FAUST_EXPORT __attribute__((visibility("default")))
#else
    #define FAUST_EXPORT  SC_API_EXPORT
#endif

#ifdef WIN32
    #define STRDUP _strdup
#else
    #define STRDUP strdup
#endif

//----------------------------------------------------------------------------
// Vector intrinsics
//----------------------------------------------------------------------------


//----------------------------------------------------------------------------
// Metadata
//----------------------------------------------------------------------------

class MetaData : public Meta
               , public std::map<std::string, std::string>
{
public:
    void declare(const char* key, const char* value)
    {
        (*this)[key] = value;
    }
};

//----------------------------------------------------------------------------
// Control counter
//----------------------------------------------------------------------------

class ControlCounter : public UI
{
public:
    ControlCounter()
        : mNumControlInputs(0),
          mNumControlOutputs(0)
    {}

    size_t getNumControls() const { return getNumControlInputs(); }
    size_t getNumControlInputs() const { return mNumControlInputs; }
    size_t getNumControlOutputs() const { return mNumControlOutputs; }

    // Layout widgets
    virtual void openTabBox(const char* label) { }
    virtual void openHorizontalBox(const char* label) { }
    virtual void openVerticalBox(const char* label) { }
    virtual void closeBox() { }

    // Active widgets
    virtual void addButton(const char* label, FAUSTFLOAT* zone)
    { addControlInput(); }
    virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
    { addControlInput(); }
    virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }
    virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }
    virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }

    // Passive widgets
    virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
    { addControlOutput(); }
    virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
    { addControlOutput(); }
    
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}

protected:
    void addControlInput() { mNumControlInputs++; }
    void addControlOutput() { mNumControlOutputs++; }

private:
    size_t mNumControlInputs;
    size_t mNumControlOutputs;
};

//----------------------------------------------------------------------------
// UI control
//----------------------------------------------------------------------------

struct Control
{
    typedef void (*UpdateFunction)(Control* self, FAUSTFLOAT value);

    UpdateFunction updateFunction;
    FAUSTFLOAT* zone;
    FAUSTFLOAT min, max;

    inline void update(FAUSTFLOAT value)
    {
        (*updateFunction)(this, value);
    }

    static void simpleUpdate(Control* self, FAUSTFLOAT value)
    {
        *self->zone = value;
    }
    static void boundedUpdate(Control* self, FAUSTFLOAT value)
    {
        *self->zone = sc_clip(value, self->min, self->max);
    }
};

//----------------------------------------------------------------------------
// Control allocator
//----------------------------------------------------------------------------

class ControlAllocator : public UI
{
public:
    ControlAllocator(Control* controls)
        : mControls(controls)
    { }

    // Layout widgets
    virtual void openTabBox(const char* label) { }
    virtual void openHorizontalBox(const char* label) { }
    virtual void openVerticalBox(const char* label) { }
    virtual void closeBox() { }

    // Active widgets
    virtual void addButton(const char* label, FAUSTFLOAT* zone)
    { addSimpleControl(zone); }
    virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
    { addSimpleControl(zone); }
    virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }
    virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }
    virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }

    // Passive widgets
    virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
    virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}

private:
    void addControl(Control::UpdateFunction updateFunction, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT /* step */)
    {
        Control* ctrl        = mControls++;
        ctrl->updateFunction = updateFunction;
        ctrl->zone           = zone;
        ctrl->min            = min;
        ctrl->max            = max;
    }
    void addSimpleControl(FAUSTFLOAT* zone)
    {
        addControl(Control::simpleUpdate, zone, 0.f, 0.f, 0.f);
    }
    void addBoundedControl(FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    {
        addControl(Control::boundedUpdate, zone, min, max, step);
    }

private:
    Control* mControls;
};

//----------------------------------------------------------------------------
// FAUST generated code
//----------------------------------------------------------------------------

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif 

#include <cmath>
#include <math.h>

float mydsp_faustpower2_f(float value) {
	return (value * value);
	
}

#ifndef FAUSTCLASS 
#define FAUSTCLASS mydsp
#endif
#ifdef __APPLE__ 
#define exp10f __exp10f
#define exp10 __exp10
#endif

class mydsp : public dsp {
	
 private:
	
	int fSamplingFreq;
	int iConst0;
	float fConst1;
	float fConst2;
	float fConst3;
	float fConst4;
	float fConst5;
	float fConst6;
	float fConst7;
	float fConst8;
	float fConst9;
	float fConst10;
	float fConst11;
	float fRec15[2];
	float fRec13[2];
	float fRec12[2];
	float fRec10[2];
	float fRec9[2];
	float fRec7[2];
	float fRec6[2];
	float fRec4[2];
	float fRec3[2];
	float fRec1[2];
	float fConst12;
	FAUSTFLOAT fHslider0;
	float fRec16[2];
	float fRec0[3];
	FAUSTFLOAT fHslider1;
	float fRec17[2];
	float fRec33[2];
	float fRec31[2];
	float fRec30[2];
	float fRec28[2];
	float fRec27[2];
	float fRec25[2];
	float fRec24[2];
	float fRec22[2];
	float fRec21[2];
	float fRec19[2];
	float fRec18[3];
	float fRec49[2];
	float fRec47[2];
	float fRec46[2];
	float fRec44[2];
	float fRec43[2];
	float fRec41[2];
	float fRec40[2];
	float fRec38[2];
	float fRec37[2];
	float fRec35[2];
	float fRec34[3];
	float fRec65[2];
	float fRec63[2];
	float fRec62[2];
	float fRec60[2];
	float fRec59[2];
	float fRec57[2];
	float fRec56[2];
	float fRec54[2];
	float fRec53[2];
	float fRec51[2];
	float fRec50[3];
	float fRec81[2];
	float fRec79[2];
	float fRec78[2];
	float fRec76[2];
	float fRec75[2];
	float fRec73[2];
	float fRec72[2];
	float fRec70[2];
	float fRec69[2];
	float fRec67[2];
	float fRec66[3];
	float fConst13;
	float fConst14;
	float fConst15;
	float fConst16;
	float fConst17;
	float fRec94[2];
	float fRec92[2];
	float fRec91[2];
	float fRec89[2];
	float fConst18;
	float fConst19;
	float fRec88[2];
	float fRec86[2];
	float fRec85[2];
	float fRec83[2];
	float fRec82[3];
	float fRec107[2];
	float fRec105[2];
	float fRec104[2];
	float fRec102[2];
	float fRec101[2];
	float fRec99[2];
	float fRec98[2];
	float fRec96[2];
	float fRec95[3];
	float fRec120[2];
	float fRec118[2];
	float fRec117[2];
	float fRec115[2];
	float fRec114[2];
	float fRec112[2];
	float fRec111[2];
	float fRec109[2];
	float fRec108[3];
	float fRec133[2];
	float fRec131[2];
	float fRec130[2];
	float fRec128[2];
	float fRec127[2];
	float fRec125[2];
	float fRec124[2];
	float fRec122[2];
	float fRec121[3];
	float fConst20;
	float fConst21;
	float fConst22;
	float fConst23;
	float fConst24;
	float fRec143[2];
	float fRec141[2];
	float fRec140[2];
	float fRec138[2];
	float fConst25;
	float fRec137[2];
	float fRec135[2];
	float fRec134[3];
	float fRec153[2];
	float fRec151[2];
	float fRec150[2];
	float fRec148[2];
	float fRec147[2];
	float fRec145[2];
	float fRec144[3];
	float fRec163[2];
	float fRec161[2];
	float fRec160[2];
	float fRec158[2];
	float fRec157[2];
	float fRec155[2];
	float fRec154[3];
	float fRec173[2];
	float fRec171[2];
	float fRec170[2];
	float fRec168[2];
	float fRec167[2];
	float fRec165[2];
	float fRec164[3];
	float fRec186[2];
	float fRec184[2];
	float fRec183[2];
	float fRec181[2];
	float fRec180[2];
	float fRec178[2];
	float fRec177[2];
	float fRec175[2];
	float fRec174[3];
	float fRec202[2];
	float fRec200[2];
	float fRec199[2];
	float fRec197[2];
	float fRec196[2];
	float fRec194[2];
	float fRec193[2];
	float fRec191[2];
	float fRec190[2];
	float fRec188[2];
	float fRec187[3];
	float fConst26;
	float fConst27;
	float fConst28;
	float fConst29;
	float fRec209[2];
	float fRec207[2];
	float fRec206[2];
	float fRec204[2];
	float fRec203[3];
	float fRec219[2];
	float fRec217[2];
	float fRec216[2];
	float fRec214[2];
	float fRec213[2];
	float fRec211[2];
	float fRec210[3];
	float fRec232[2];
	float fRec230[2];
	float fRec229[2];
	float fRec227[2];
	float fRec226[2];
	float fRec224[2];
	float fRec223[2];
	float fRec221[2];
	float fRec220[3];
	float fRec233[3];
	float fConst30;
	float fConst31;
	float fConst32;
	float fRec237[2];
	float fRec235[2];
	float fRec234[3];
	float fRec241[2];
	float fRec239[2];
	float fRec238[3];
	float fRec248[2];
	float fRec246[2];
	float fRec245[2];
	float fRec243[2];
	float fRec242[3];
	float fRec255[2];
	float fRec253[2];
	float fRec252[2];
	float fRec250[2];
	float fRec249[3];
	float fRec271[2];
	float fRec269[2];
	float fRec268[2];
	float fRec266[2];
	float fRec265[2];
	float fRec263[2];
	float fRec262[2];
	float fRec260[2];
	float fRec259[2];
	float fRec257[2];
	float fRec256[3];
	float fRec287[2];
	float fRec285[2];
	float fRec284[2];
	float fRec282[2];
	float fRec281[2];
	float fRec279[2];
	float fRec278[2];
	float fRec276[2];
	float fRec275[2];
	float fRec273[2];
	float fRec272[3];
	float fRec303[2];
	float fRec301[2];
	float fRec300[2];
	float fRec298[2];
	float fRec297[2];
	float fRec295[2];
	float fRec294[2];
	float fRec292[2];
	float fRec291[2];
	float fRec289[2];
	float fRec288[3];
	float fRec316[2];
	float fRec314[2];
	float fRec313[2];
	float fRec311[2];
	float fRec310[2];
	float fRec308[2];
	float fRec307[2];
	float fRec305[2];
	float fRec304[3];
	float fRec329[2];
	float fRec327[2];
	float fRec326[2];
	float fRec324[2];
	float fRec323[2];
	float fRec321[2];
	float fRec320[2];
	float fRec318[2];
	float fRec317[3];
	float fRec342[2];
	float fRec340[2];
	float fRec339[2];
	float fRec337[2];
	float fRec336[2];
	float fRec334[2];
	float fRec333[2];
	float fRec331[2];
	float fRec330[3];
	float fRec349[2];
	float fRec347[2];
	float fRec346[2];
	float fRec344[2];
	float fRec343[3];
	float fRec359[2];
	float fRec357[2];
	float fRec356[2];
	float fRec354[2];
	float fRec353[2];
	float fRec351[2];
	float fRec350[3];
	float fRec369[2];
	float fRec367[2];
	float fRec366[2];
	float fRec364[2];
	float fRec363[2];
	float fRec361[2];
	float fRec360[3];
	float fRec385[2];
	float fRec383[2];
	float fRec382[2];
	float fRec380[2];
	float fRec379[2];
	float fRec377[2];
	float fRec376[2];
	float fRec374[2];
	float fRec373[2];
	float fRec371[2];
	float fRec370[3];
	float fRec401[2];
	float fRec399[2];
	float fRec398[2];
	float fRec396[2];
	float fRec395[2];
	float fRec393[2];
	float fRec392[2];
	float fRec390[2];
	float fRec389[2];
	float fRec387[2];
	float fRec386[3];
	float fRec405[2];
	float fRec403[2];
	float fRec402[3];
	float fRec412[2];
	float fRec410[2];
	float fRec409[2];
	float fRec407[2];
	float fRec406[3];
	FAUSTFLOAT fCheckbox0;
	FAUSTFLOAT fHslider2;
	float fRec413[2];
	float fVec0[12];
	int iConst33;
	float fVec1[13];
	int iConst34;
	float fVec2[12];
	float fVec3[12];
	float fVec4[13];
	float fVec5[12];
	int IOTA;
	float fVec6[32];
	int iConst35;
	float fVec7[32];
	int iConst36;
	float fVec8[32];
	float fVec9[32];
	float fVec10[32];
	float fVec11[32];
	float fVec12[32];
	float fVec13[32];
	float fVec14[32];
	float fVec15[32];
	float fVec16[32];
	float fVec17[32];
	float fVec18[64];
	int iConst37;
	float fVec19[64];
	int iConst38;
	float fVec20[64];
	float fVec21[64];
	float fVec22[64];
	float fVec23[64];
	float fVec24[64];
	float fVec25[64];
	float fVec26[64];
	float fVec27[64];
	float fVec28[64];
	float fVec29[64];
	
 public:
	
	void metadata(Meta* m) { 
		m->declare("author", "AmbisonicDecoderToolkit");
		m->declare("copyright", "(c) Aaron J. Heller 2013");
		m->declare("license", "BSD 3-Clause License");
		m->declare("name", "SATOswD3o5");
		m->declare("version", "1.2");
	}

	virtual int getNumInputs() {
		return 36;
		
	}
	virtual int getNumOutputs() {
		return 31;
		
	}
	virtual int getInputRate(int channel) {
		int rate;
		switch (channel) {
			case 0: {
				rate = 1;
				break;
			}
			case 1: {
				rate = 1;
				break;
			}
			case 2: {
				rate = 1;
				break;
			}
			case 3: {
				rate = 1;
				break;
			}
			case 4: {
				rate = 1;
				break;
			}
			case 5: {
				rate = 1;
				break;
			}
			case 6: {
				rate = 1;
				break;
			}
			case 7: {
				rate = 1;
				break;
			}
			case 8: {
				rate = 1;
				break;
			}
			case 9: {
				rate = 1;
				break;
			}
			case 10: {
				rate = 1;
				break;
			}
			case 11: {
				rate = 1;
				break;
			}
			case 12: {
				rate = 1;
				break;
			}
			case 13: {
				rate = 1;
				break;
			}
			case 14: {
				rate = 1;
				break;
			}
			case 15: {
				rate = 1;
				break;
			}
			case 16: {
				rate = 1;
				break;
			}
			case 17: {
				rate = 1;
				break;
			}
			case 18: {
				rate = 1;
				break;
			}
			case 19: {
				rate = 1;
				break;
			}
			case 20: {
				rate = 1;
				break;
			}
			case 21: {
				rate = 1;
				break;
			}
			case 22: {
				rate = 1;
				break;
			}
			case 23: {
				rate = 1;
				break;
			}
			case 24: {
				rate = 1;
				break;
			}
			case 25: {
				rate = 1;
				break;
			}
			case 26: {
				rate = 1;
				break;
			}
			case 27: {
				rate = 1;
				break;
			}
			case 28: {
				rate = 1;
				break;
			}
			case 29: {
				rate = 1;
				break;
			}
			case 30: {
				rate = 1;
				break;
			}
			case 31: {
				rate = 1;
				break;
			}
			case 32: {
				rate = 1;
				break;
			}
			case 33: {
				rate = 1;
				break;
			}
			case 34: {
				rate = 1;
				break;
			}
			case 35: {
				rate = 1;
				break;
			}
			default: {
				rate = -1;
				break;
			}
			
		}
		return rate;
		
	}
	virtual int getOutputRate(int channel) {
		int rate;
		switch (channel) {
			case 0: {
				rate = 1;
				break;
			}
			case 1: {
				rate = 1;
				break;
			}
			case 2: {
				rate = 1;
				break;
			}
			case 3: {
				rate = 1;
				break;
			}
			case 4: {
				rate = 1;
				break;
			}
			case 5: {
				rate = 1;
				break;
			}
			case 6: {
				rate = 1;
				break;
			}
			case 7: {
				rate = 1;
				break;
			}
			case 8: {
				rate = 1;
				break;
			}
			case 9: {
				rate = 1;
				break;
			}
			case 10: {
				rate = 1;
				break;
			}
			case 11: {
				rate = 1;
				break;
			}
			case 12: {
				rate = 1;
				break;
			}
			case 13: {
				rate = 1;
				break;
			}
			case 14: {
				rate = 1;
				break;
			}
			case 15: {
				rate = 1;
				break;
			}
			case 16: {
				rate = 1;
				break;
			}
			case 17: {
				rate = 1;
				break;
			}
			case 18: {
				rate = 1;
				break;
			}
			case 19: {
				rate = 1;
				break;
			}
			case 20: {
				rate = 1;
				break;
			}
			case 21: {
				rate = 1;
				break;
			}
			case 22: {
				rate = 1;
				break;
			}
			case 23: {
				rate = 1;
				break;
			}
			case 24: {
				rate = 1;
				break;
			}
			case 25: {
				rate = 1;
				break;
			}
			case 26: {
				rate = 1;
				break;
			}
			case 27: {
				rate = 1;
				break;
			}
			case 28: {
				rate = 1;
				break;
			}
			case 29: {
				rate = 1;
				break;
			}
			case 30: {
				rate = 1;
				break;
			}
			default: {
				rate = -1;
				break;
			}
			
		}
		return rate;
		
	}
	
	static void classInit(int samplingFreq) {
		
	}
	
	virtual void instanceConstants(int samplingFreq) {
		fSamplingFreq = samplingFreq;
		iConst0 = min(192000, max(1, fSamplingFreq));
		fConst1 = float(iConst0);
		fConst2 = ((69.7764511f / fConst1) + 1.0f);
		fConst3 = (0.0f - (139.552902f / (fConst1 * fConst2)));
		fConst4 = mydsp_faustpower2_f(fConst1);
		fConst5 = ((((5225.26074f / fConst1) + 128.272217f) / fConst1) + 1.0f);
		fConst6 = (0.0f - (20901.043f / (fConst4 * fConst5)));
		fConst7 = (0.0f - (((20901.043f / fConst1) + 256.544434f) / (fConst1 * fConst5)));
		fConst8 = ((((6647.16162f / fConst1) + 88.9603271f) / fConst1) + 1.0f);
		fConst9 = (1.0f / ((fConst2 * fConst5) * fConst8));
		fConst10 = (0.0f - (26588.6465f / (fConst4 * fConst8)));
		fConst11 = (0.0f - (((26588.6465f / fConst1) + 177.920654f) / (fConst1 * fConst8)));
		fConst12 = (3.14159274f / float(iConst0));
		fConst13 = ((((3346.26953f / fConst1) + 110.831802f) / fConst1) + 1.0f);
		fConst14 = ((((4205.76904f / fConst1) + 80.5075302f) / fConst1) + 1.0f);
		fConst15 = (1.0f / (fConst13 * fConst14));
		fConst16 = (0.0f - (16823.0762f / (fConst4 * fConst14)));
		fConst17 = (0.0f - (((16823.0762f / fConst1) + 161.01506f) / (fConst1 * fConst14)));
		fConst18 = (0.0f - (13385.0781f / (fConst4 * fConst13)));
		fConst19 = (0.0f - (((13385.0781f / fConst1) + 221.663605f) / (fConst1 * fConst13)));
		fConst20 = ((44.4325409f / fConst1) + 1.0f);
		fConst21 = ((((2364.84619f / fConst1) + 70.3710632f) / fConst1) + 1.0f);
		fConst22 = (1.0f / (fConst20 * fConst21));
		fConst23 = (0.0f - (9459.38477f / (fConst4 * fConst21)));
		fConst24 = (0.0f - (((9459.38477f / fConst1) + 140.742126f) / (fConst1 * fConst21)));
		fConst25 = (0.0f - (88.8650818f / (fConst1 * fConst20)));
		fConst26 = ((((1098.32227f / fConst1) + 57.4018021f) / fConst1) + 1.0f);
		fConst27 = (1.0f / fConst26);
		fConst28 = (0.0f - (4393.28906f / (fConst4 * fConst26)));
		fConst29 = (0.0f - (((4393.28906f / fConst1) + 114.803604f) / (fConst1 * fConst26)));
		fConst30 = ((19.133934f / fConst1) + 1.0f);
		fConst31 = (1.0f / fConst30);
		fConst32 = (0.0f - (38.267868f / (fConst1 * fConst30)));
		iConst33 = int(((5.24453171e-05f * float(iConst0)) + 0.5f));
		iConst34 = int(((5.8272577e-05f * float(iConst0)) + 0.5f));
		iConst35 = int(((0.000145681435f * float(iConst0)) + 0.5f));
		iConst36 = int(((0.000134026923f * float(iConst0)) + 0.5f));
		iConst37 = int(((0.000262226589f * float(iConst0)) + 0.5f));
		iConst38 = int(((0.000256399333f * float(iConst0)) + 0.5f));
		
	}
	
	virtual void instanceResetUserInterface() {
		fHslider0 = FAUSTFLOAT(400.0f);
		fHslider1 = FAUSTFLOAT(0.0f);
		fCheckbox0 = FAUSTFLOAT(0.0f);
		fHslider2 = FAUSTFLOAT(-10.0f);
		
	}
	
	virtual void instanceClear() {
		for (int l0 = 0; (l0 < 2); l0 = (l0 + 1)) {
			fRec15[l0] = 0.0f;
			
		}
		for (int l1 = 0; (l1 < 2); l1 = (l1 + 1)) {
			fRec13[l1] = 0.0f;
			
		}
		for (int l2 = 0; (l2 < 2); l2 = (l2 + 1)) {
			fRec12[l2] = 0.0f;
			
		}
		for (int l3 = 0; (l3 < 2); l3 = (l3 + 1)) {
			fRec10[l3] = 0.0f;
			
		}
		for (int l4 = 0; (l4 < 2); l4 = (l4 + 1)) {
			fRec9[l4] = 0.0f;
			
		}
		for (int l5 = 0; (l5 < 2); l5 = (l5 + 1)) {
			fRec7[l5] = 0.0f;
			
		}
		for (int l6 = 0; (l6 < 2); l6 = (l6 + 1)) {
			fRec6[l6] = 0.0f;
			
		}
		for (int l7 = 0; (l7 < 2); l7 = (l7 + 1)) {
			fRec4[l7] = 0.0f;
			
		}
		for (int l8 = 0; (l8 < 2); l8 = (l8 + 1)) {
			fRec3[l8] = 0.0f;
			
		}
		for (int l9 = 0; (l9 < 2); l9 = (l9 + 1)) {
			fRec1[l9] = 0.0f;
			
		}
		for (int l10 = 0; (l10 < 2); l10 = (l10 + 1)) {
			fRec16[l10] = 0.0f;
			
		}
		for (int l11 = 0; (l11 < 3); l11 = (l11 + 1)) {
			fRec0[l11] = 0.0f;
			
		}
		for (int l12 = 0; (l12 < 2); l12 = (l12 + 1)) {
			fRec17[l12] = 0.0f;
			
		}
		for (int l13 = 0; (l13 < 2); l13 = (l13 + 1)) {
			fRec33[l13] = 0.0f;
			
		}
		for (int l14 = 0; (l14 < 2); l14 = (l14 + 1)) {
			fRec31[l14] = 0.0f;
			
		}
		for (int l15 = 0; (l15 < 2); l15 = (l15 + 1)) {
			fRec30[l15] = 0.0f;
			
		}
		for (int l16 = 0; (l16 < 2); l16 = (l16 + 1)) {
			fRec28[l16] = 0.0f;
			
		}
		for (int l17 = 0; (l17 < 2); l17 = (l17 + 1)) {
			fRec27[l17] = 0.0f;
			
		}
		for (int l18 = 0; (l18 < 2); l18 = (l18 + 1)) {
			fRec25[l18] = 0.0f;
			
		}
		for (int l19 = 0; (l19 < 2); l19 = (l19 + 1)) {
			fRec24[l19] = 0.0f;
			
		}
		for (int l20 = 0; (l20 < 2); l20 = (l20 + 1)) {
			fRec22[l20] = 0.0f;
			
		}
		for (int l21 = 0; (l21 < 2); l21 = (l21 + 1)) {
			fRec21[l21] = 0.0f;
			
		}
		for (int l22 = 0; (l22 < 2); l22 = (l22 + 1)) {
			fRec19[l22] = 0.0f;
			
		}
		for (int l23 = 0; (l23 < 3); l23 = (l23 + 1)) {
			fRec18[l23] = 0.0f;
			
		}
		for (int l24 = 0; (l24 < 2); l24 = (l24 + 1)) {
			fRec49[l24] = 0.0f;
			
		}
		for (int l25 = 0; (l25 < 2); l25 = (l25 + 1)) {
			fRec47[l25] = 0.0f;
			
		}
		for (int l26 = 0; (l26 < 2); l26 = (l26 + 1)) {
			fRec46[l26] = 0.0f;
			
		}
		for (int l27 = 0; (l27 < 2); l27 = (l27 + 1)) {
			fRec44[l27] = 0.0f;
			
		}
		for (int l28 = 0; (l28 < 2); l28 = (l28 + 1)) {
			fRec43[l28] = 0.0f;
			
		}
		for (int l29 = 0; (l29 < 2); l29 = (l29 + 1)) {
			fRec41[l29] = 0.0f;
			
		}
		for (int l30 = 0; (l30 < 2); l30 = (l30 + 1)) {
			fRec40[l30] = 0.0f;
			
		}
		for (int l31 = 0; (l31 < 2); l31 = (l31 + 1)) {
			fRec38[l31] = 0.0f;
			
		}
		for (int l32 = 0; (l32 < 2); l32 = (l32 + 1)) {
			fRec37[l32] = 0.0f;
			
		}
		for (int l33 = 0; (l33 < 2); l33 = (l33 + 1)) {
			fRec35[l33] = 0.0f;
			
		}
		for (int l34 = 0; (l34 < 3); l34 = (l34 + 1)) {
			fRec34[l34] = 0.0f;
			
		}
		for (int l35 = 0; (l35 < 2); l35 = (l35 + 1)) {
			fRec65[l35] = 0.0f;
			
		}
		for (int l36 = 0; (l36 < 2); l36 = (l36 + 1)) {
			fRec63[l36] = 0.0f;
			
		}
		for (int l37 = 0; (l37 < 2); l37 = (l37 + 1)) {
			fRec62[l37] = 0.0f;
			
		}
		for (int l38 = 0; (l38 < 2); l38 = (l38 + 1)) {
			fRec60[l38] = 0.0f;
			
		}
		for (int l39 = 0; (l39 < 2); l39 = (l39 + 1)) {
			fRec59[l39] = 0.0f;
			
		}
		for (int l40 = 0; (l40 < 2); l40 = (l40 + 1)) {
			fRec57[l40] = 0.0f;
			
		}
		for (int l41 = 0; (l41 < 2); l41 = (l41 + 1)) {
			fRec56[l41] = 0.0f;
			
		}
		for (int l42 = 0; (l42 < 2); l42 = (l42 + 1)) {
			fRec54[l42] = 0.0f;
			
		}
		for (int l43 = 0; (l43 < 2); l43 = (l43 + 1)) {
			fRec53[l43] = 0.0f;
			
		}
		for (int l44 = 0; (l44 < 2); l44 = (l44 + 1)) {
			fRec51[l44] = 0.0f;
			
		}
		for (int l45 = 0; (l45 < 3); l45 = (l45 + 1)) {
			fRec50[l45] = 0.0f;
			
		}
		for (int l46 = 0; (l46 < 2); l46 = (l46 + 1)) {
			fRec81[l46] = 0.0f;
			
		}
		for (int l47 = 0; (l47 < 2); l47 = (l47 + 1)) {
			fRec79[l47] = 0.0f;
			
		}
		for (int l48 = 0; (l48 < 2); l48 = (l48 + 1)) {
			fRec78[l48] = 0.0f;
			
		}
		for (int l49 = 0; (l49 < 2); l49 = (l49 + 1)) {
			fRec76[l49] = 0.0f;
			
		}
		for (int l50 = 0; (l50 < 2); l50 = (l50 + 1)) {
			fRec75[l50] = 0.0f;
			
		}
		for (int l51 = 0; (l51 < 2); l51 = (l51 + 1)) {
			fRec73[l51] = 0.0f;
			
		}
		for (int l52 = 0; (l52 < 2); l52 = (l52 + 1)) {
			fRec72[l52] = 0.0f;
			
		}
		for (int l53 = 0; (l53 < 2); l53 = (l53 + 1)) {
			fRec70[l53] = 0.0f;
			
		}
		for (int l54 = 0; (l54 < 2); l54 = (l54 + 1)) {
			fRec69[l54] = 0.0f;
			
		}
		for (int l55 = 0; (l55 < 2); l55 = (l55 + 1)) {
			fRec67[l55] = 0.0f;
			
		}
		for (int l56 = 0; (l56 < 3); l56 = (l56 + 1)) {
			fRec66[l56] = 0.0f;
			
		}
		for (int l57 = 0; (l57 < 2); l57 = (l57 + 1)) {
			fRec94[l57] = 0.0f;
			
		}
		for (int l58 = 0; (l58 < 2); l58 = (l58 + 1)) {
			fRec92[l58] = 0.0f;
			
		}
		for (int l59 = 0; (l59 < 2); l59 = (l59 + 1)) {
			fRec91[l59] = 0.0f;
			
		}
		for (int l60 = 0; (l60 < 2); l60 = (l60 + 1)) {
			fRec89[l60] = 0.0f;
			
		}
		for (int l61 = 0; (l61 < 2); l61 = (l61 + 1)) {
			fRec88[l61] = 0.0f;
			
		}
		for (int l62 = 0; (l62 < 2); l62 = (l62 + 1)) {
			fRec86[l62] = 0.0f;
			
		}
		for (int l63 = 0; (l63 < 2); l63 = (l63 + 1)) {
			fRec85[l63] = 0.0f;
			
		}
		for (int l64 = 0; (l64 < 2); l64 = (l64 + 1)) {
			fRec83[l64] = 0.0f;
			
		}
		for (int l65 = 0; (l65 < 3); l65 = (l65 + 1)) {
			fRec82[l65] = 0.0f;
			
		}
		for (int l66 = 0; (l66 < 2); l66 = (l66 + 1)) {
			fRec107[l66] = 0.0f;
			
		}
		for (int l67 = 0; (l67 < 2); l67 = (l67 + 1)) {
			fRec105[l67] = 0.0f;
			
		}
		for (int l68 = 0; (l68 < 2); l68 = (l68 + 1)) {
			fRec104[l68] = 0.0f;
			
		}
		for (int l69 = 0; (l69 < 2); l69 = (l69 + 1)) {
			fRec102[l69] = 0.0f;
			
		}
		for (int l70 = 0; (l70 < 2); l70 = (l70 + 1)) {
			fRec101[l70] = 0.0f;
			
		}
		for (int l71 = 0; (l71 < 2); l71 = (l71 + 1)) {
			fRec99[l71] = 0.0f;
			
		}
		for (int l72 = 0; (l72 < 2); l72 = (l72 + 1)) {
			fRec98[l72] = 0.0f;
			
		}
		for (int l73 = 0; (l73 < 2); l73 = (l73 + 1)) {
			fRec96[l73] = 0.0f;
			
		}
		for (int l74 = 0; (l74 < 3); l74 = (l74 + 1)) {
			fRec95[l74] = 0.0f;
			
		}
		for (int l75 = 0; (l75 < 2); l75 = (l75 + 1)) {
			fRec120[l75] = 0.0f;
			
		}
		for (int l76 = 0; (l76 < 2); l76 = (l76 + 1)) {
			fRec118[l76] = 0.0f;
			
		}
		for (int l77 = 0; (l77 < 2); l77 = (l77 + 1)) {
			fRec117[l77] = 0.0f;
			
		}
		for (int l78 = 0; (l78 < 2); l78 = (l78 + 1)) {
			fRec115[l78] = 0.0f;
			
		}
		for (int l79 = 0; (l79 < 2); l79 = (l79 + 1)) {
			fRec114[l79] = 0.0f;
			
		}
		for (int l80 = 0; (l80 < 2); l80 = (l80 + 1)) {
			fRec112[l80] = 0.0f;
			
		}
		for (int l81 = 0; (l81 < 2); l81 = (l81 + 1)) {
			fRec111[l81] = 0.0f;
			
		}
		for (int l82 = 0; (l82 < 2); l82 = (l82 + 1)) {
			fRec109[l82] = 0.0f;
			
		}
		for (int l83 = 0; (l83 < 3); l83 = (l83 + 1)) {
			fRec108[l83] = 0.0f;
			
		}
		for (int l84 = 0; (l84 < 2); l84 = (l84 + 1)) {
			fRec133[l84] = 0.0f;
			
		}
		for (int l85 = 0; (l85 < 2); l85 = (l85 + 1)) {
			fRec131[l85] = 0.0f;
			
		}
		for (int l86 = 0; (l86 < 2); l86 = (l86 + 1)) {
			fRec130[l86] = 0.0f;
			
		}
		for (int l87 = 0; (l87 < 2); l87 = (l87 + 1)) {
			fRec128[l87] = 0.0f;
			
		}
		for (int l88 = 0; (l88 < 2); l88 = (l88 + 1)) {
			fRec127[l88] = 0.0f;
			
		}
		for (int l89 = 0; (l89 < 2); l89 = (l89 + 1)) {
			fRec125[l89] = 0.0f;
			
		}
		for (int l90 = 0; (l90 < 2); l90 = (l90 + 1)) {
			fRec124[l90] = 0.0f;
			
		}
		for (int l91 = 0; (l91 < 2); l91 = (l91 + 1)) {
			fRec122[l91] = 0.0f;
			
		}
		for (int l92 = 0; (l92 < 3); l92 = (l92 + 1)) {
			fRec121[l92] = 0.0f;
			
		}
		for (int l93 = 0; (l93 < 2); l93 = (l93 + 1)) {
			fRec143[l93] = 0.0f;
			
		}
		for (int l94 = 0; (l94 < 2); l94 = (l94 + 1)) {
			fRec141[l94] = 0.0f;
			
		}
		for (int l95 = 0; (l95 < 2); l95 = (l95 + 1)) {
			fRec140[l95] = 0.0f;
			
		}
		for (int l96 = 0; (l96 < 2); l96 = (l96 + 1)) {
			fRec138[l96] = 0.0f;
			
		}
		for (int l97 = 0; (l97 < 2); l97 = (l97 + 1)) {
			fRec137[l97] = 0.0f;
			
		}
		for (int l98 = 0; (l98 < 2); l98 = (l98 + 1)) {
			fRec135[l98] = 0.0f;
			
		}
		for (int l99 = 0; (l99 < 3); l99 = (l99 + 1)) {
			fRec134[l99] = 0.0f;
			
		}
		for (int l100 = 0; (l100 < 2); l100 = (l100 + 1)) {
			fRec153[l100] = 0.0f;
			
		}
		for (int l101 = 0; (l101 < 2); l101 = (l101 + 1)) {
			fRec151[l101] = 0.0f;
			
		}
		for (int l102 = 0; (l102 < 2); l102 = (l102 + 1)) {
			fRec150[l102] = 0.0f;
			
		}
		for (int l103 = 0; (l103 < 2); l103 = (l103 + 1)) {
			fRec148[l103] = 0.0f;
			
		}
		for (int l104 = 0; (l104 < 2); l104 = (l104 + 1)) {
			fRec147[l104] = 0.0f;
			
		}
		for (int l105 = 0; (l105 < 2); l105 = (l105 + 1)) {
			fRec145[l105] = 0.0f;
			
		}
		for (int l106 = 0; (l106 < 3); l106 = (l106 + 1)) {
			fRec144[l106] = 0.0f;
			
		}
		for (int l107 = 0; (l107 < 2); l107 = (l107 + 1)) {
			fRec163[l107] = 0.0f;
			
		}
		for (int l108 = 0; (l108 < 2); l108 = (l108 + 1)) {
			fRec161[l108] = 0.0f;
			
		}
		for (int l109 = 0; (l109 < 2); l109 = (l109 + 1)) {
			fRec160[l109] = 0.0f;
			
		}
		for (int l110 = 0; (l110 < 2); l110 = (l110 + 1)) {
			fRec158[l110] = 0.0f;
			
		}
		for (int l111 = 0; (l111 < 2); l111 = (l111 + 1)) {
			fRec157[l111] = 0.0f;
			
		}
		for (int l112 = 0; (l112 < 2); l112 = (l112 + 1)) {
			fRec155[l112] = 0.0f;
			
		}
		for (int l113 = 0; (l113 < 3); l113 = (l113 + 1)) {
			fRec154[l113] = 0.0f;
			
		}
		for (int l114 = 0; (l114 < 2); l114 = (l114 + 1)) {
			fRec173[l114] = 0.0f;
			
		}
		for (int l115 = 0; (l115 < 2); l115 = (l115 + 1)) {
			fRec171[l115] = 0.0f;
			
		}
		for (int l116 = 0; (l116 < 2); l116 = (l116 + 1)) {
			fRec170[l116] = 0.0f;
			
		}
		for (int l117 = 0; (l117 < 2); l117 = (l117 + 1)) {
			fRec168[l117] = 0.0f;
			
		}
		for (int l118 = 0; (l118 < 2); l118 = (l118 + 1)) {
			fRec167[l118] = 0.0f;
			
		}
		for (int l119 = 0; (l119 < 2); l119 = (l119 + 1)) {
			fRec165[l119] = 0.0f;
			
		}
		for (int l120 = 0; (l120 < 3); l120 = (l120 + 1)) {
			fRec164[l120] = 0.0f;
			
		}
		for (int l121 = 0; (l121 < 2); l121 = (l121 + 1)) {
			fRec186[l121] = 0.0f;
			
		}
		for (int l122 = 0; (l122 < 2); l122 = (l122 + 1)) {
			fRec184[l122] = 0.0f;
			
		}
		for (int l123 = 0; (l123 < 2); l123 = (l123 + 1)) {
			fRec183[l123] = 0.0f;
			
		}
		for (int l124 = 0; (l124 < 2); l124 = (l124 + 1)) {
			fRec181[l124] = 0.0f;
			
		}
		for (int l125 = 0; (l125 < 2); l125 = (l125 + 1)) {
			fRec180[l125] = 0.0f;
			
		}
		for (int l126 = 0; (l126 < 2); l126 = (l126 + 1)) {
			fRec178[l126] = 0.0f;
			
		}
		for (int l127 = 0; (l127 < 2); l127 = (l127 + 1)) {
			fRec177[l127] = 0.0f;
			
		}
		for (int l128 = 0; (l128 < 2); l128 = (l128 + 1)) {
			fRec175[l128] = 0.0f;
			
		}
		for (int l129 = 0; (l129 < 3); l129 = (l129 + 1)) {
			fRec174[l129] = 0.0f;
			
		}
		for (int l130 = 0; (l130 < 2); l130 = (l130 + 1)) {
			fRec202[l130] = 0.0f;
			
		}
		for (int l131 = 0; (l131 < 2); l131 = (l131 + 1)) {
			fRec200[l131] = 0.0f;
			
		}
		for (int l132 = 0; (l132 < 2); l132 = (l132 + 1)) {
			fRec199[l132] = 0.0f;
			
		}
		for (int l133 = 0; (l133 < 2); l133 = (l133 + 1)) {
			fRec197[l133] = 0.0f;
			
		}
		for (int l134 = 0; (l134 < 2); l134 = (l134 + 1)) {
			fRec196[l134] = 0.0f;
			
		}
		for (int l135 = 0; (l135 < 2); l135 = (l135 + 1)) {
			fRec194[l135] = 0.0f;
			
		}
		for (int l136 = 0; (l136 < 2); l136 = (l136 + 1)) {
			fRec193[l136] = 0.0f;
			
		}
		for (int l137 = 0; (l137 < 2); l137 = (l137 + 1)) {
			fRec191[l137] = 0.0f;
			
		}
		for (int l138 = 0; (l138 < 2); l138 = (l138 + 1)) {
			fRec190[l138] = 0.0f;
			
		}
		for (int l139 = 0; (l139 < 2); l139 = (l139 + 1)) {
			fRec188[l139] = 0.0f;
			
		}
		for (int l140 = 0; (l140 < 3); l140 = (l140 + 1)) {
			fRec187[l140] = 0.0f;
			
		}
		for (int l141 = 0; (l141 < 2); l141 = (l141 + 1)) {
			fRec209[l141] = 0.0f;
			
		}
		for (int l142 = 0; (l142 < 2); l142 = (l142 + 1)) {
			fRec207[l142] = 0.0f;
			
		}
		for (int l143 = 0; (l143 < 2); l143 = (l143 + 1)) {
			fRec206[l143] = 0.0f;
			
		}
		for (int l144 = 0; (l144 < 2); l144 = (l144 + 1)) {
			fRec204[l144] = 0.0f;
			
		}
		for (int l145 = 0; (l145 < 3); l145 = (l145 + 1)) {
			fRec203[l145] = 0.0f;
			
		}
		for (int l146 = 0; (l146 < 2); l146 = (l146 + 1)) {
			fRec219[l146] = 0.0f;
			
		}
		for (int l147 = 0; (l147 < 2); l147 = (l147 + 1)) {
			fRec217[l147] = 0.0f;
			
		}
		for (int l148 = 0; (l148 < 2); l148 = (l148 + 1)) {
			fRec216[l148] = 0.0f;
			
		}
		for (int l149 = 0; (l149 < 2); l149 = (l149 + 1)) {
			fRec214[l149] = 0.0f;
			
		}
		for (int l150 = 0; (l150 < 2); l150 = (l150 + 1)) {
			fRec213[l150] = 0.0f;
			
		}
		for (int l151 = 0; (l151 < 2); l151 = (l151 + 1)) {
			fRec211[l151] = 0.0f;
			
		}
		for (int l152 = 0; (l152 < 3); l152 = (l152 + 1)) {
			fRec210[l152] = 0.0f;
			
		}
		for (int l153 = 0; (l153 < 2); l153 = (l153 + 1)) {
			fRec232[l153] = 0.0f;
			
		}
		for (int l154 = 0; (l154 < 2); l154 = (l154 + 1)) {
			fRec230[l154] = 0.0f;
			
		}
		for (int l155 = 0; (l155 < 2); l155 = (l155 + 1)) {
			fRec229[l155] = 0.0f;
			
		}
		for (int l156 = 0; (l156 < 2); l156 = (l156 + 1)) {
			fRec227[l156] = 0.0f;
			
		}
		for (int l157 = 0; (l157 < 2); l157 = (l157 + 1)) {
			fRec226[l157] = 0.0f;
			
		}
		for (int l158 = 0; (l158 < 2); l158 = (l158 + 1)) {
			fRec224[l158] = 0.0f;
			
		}
		for (int l159 = 0; (l159 < 2); l159 = (l159 + 1)) {
			fRec223[l159] = 0.0f;
			
		}
		for (int l160 = 0; (l160 < 2); l160 = (l160 + 1)) {
			fRec221[l160] = 0.0f;
			
		}
		for (int l161 = 0; (l161 < 3); l161 = (l161 + 1)) {
			fRec220[l161] = 0.0f;
			
		}
		for (int l162 = 0; (l162 < 3); l162 = (l162 + 1)) {
			fRec233[l162] = 0.0f;
			
		}
		for (int l163 = 0; (l163 < 2); l163 = (l163 + 1)) {
			fRec237[l163] = 0.0f;
			
		}
		for (int l164 = 0; (l164 < 2); l164 = (l164 + 1)) {
			fRec235[l164] = 0.0f;
			
		}
		for (int l165 = 0; (l165 < 3); l165 = (l165 + 1)) {
			fRec234[l165] = 0.0f;
			
		}
		for (int l166 = 0; (l166 < 2); l166 = (l166 + 1)) {
			fRec241[l166] = 0.0f;
			
		}
		for (int l167 = 0; (l167 < 2); l167 = (l167 + 1)) {
			fRec239[l167] = 0.0f;
			
		}
		for (int l168 = 0; (l168 < 3); l168 = (l168 + 1)) {
			fRec238[l168] = 0.0f;
			
		}
		for (int l169 = 0; (l169 < 2); l169 = (l169 + 1)) {
			fRec248[l169] = 0.0f;
			
		}
		for (int l170 = 0; (l170 < 2); l170 = (l170 + 1)) {
			fRec246[l170] = 0.0f;
			
		}
		for (int l171 = 0; (l171 < 2); l171 = (l171 + 1)) {
			fRec245[l171] = 0.0f;
			
		}
		for (int l172 = 0; (l172 < 2); l172 = (l172 + 1)) {
			fRec243[l172] = 0.0f;
			
		}
		for (int l173 = 0; (l173 < 3); l173 = (l173 + 1)) {
			fRec242[l173] = 0.0f;
			
		}
		for (int l174 = 0; (l174 < 2); l174 = (l174 + 1)) {
			fRec255[l174] = 0.0f;
			
		}
		for (int l175 = 0; (l175 < 2); l175 = (l175 + 1)) {
			fRec253[l175] = 0.0f;
			
		}
		for (int l176 = 0; (l176 < 2); l176 = (l176 + 1)) {
			fRec252[l176] = 0.0f;
			
		}
		for (int l177 = 0; (l177 < 2); l177 = (l177 + 1)) {
			fRec250[l177] = 0.0f;
			
		}
		for (int l178 = 0; (l178 < 3); l178 = (l178 + 1)) {
			fRec249[l178] = 0.0f;
			
		}
		for (int l179 = 0; (l179 < 2); l179 = (l179 + 1)) {
			fRec271[l179] = 0.0f;
			
		}
		for (int l180 = 0; (l180 < 2); l180 = (l180 + 1)) {
			fRec269[l180] = 0.0f;
			
		}
		for (int l181 = 0; (l181 < 2); l181 = (l181 + 1)) {
			fRec268[l181] = 0.0f;
			
		}
		for (int l182 = 0; (l182 < 2); l182 = (l182 + 1)) {
			fRec266[l182] = 0.0f;
			
		}
		for (int l183 = 0; (l183 < 2); l183 = (l183 + 1)) {
			fRec265[l183] = 0.0f;
			
		}
		for (int l184 = 0; (l184 < 2); l184 = (l184 + 1)) {
			fRec263[l184] = 0.0f;
			
		}
		for (int l185 = 0; (l185 < 2); l185 = (l185 + 1)) {
			fRec262[l185] = 0.0f;
			
		}
		for (int l186 = 0; (l186 < 2); l186 = (l186 + 1)) {
			fRec260[l186] = 0.0f;
			
		}
		for (int l187 = 0; (l187 < 2); l187 = (l187 + 1)) {
			fRec259[l187] = 0.0f;
			
		}
		for (int l188 = 0; (l188 < 2); l188 = (l188 + 1)) {
			fRec257[l188] = 0.0f;
			
		}
		for (int l189 = 0; (l189 < 3); l189 = (l189 + 1)) {
			fRec256[l189] = 0.0f;
			
		}
		for (int l190 = 0; (l190 < 2); l190 = (l190 + 1)) {
			fRec287[l190] = 0.0f;
			
		}
		for (int l191 = 0; (l191 < 2); l191 = (l191 + 1)) {
			fRec285[l191] = 0.0f;
			
		}
		for (int l192 = 0; (l192 < 2); l192 = (l192 + 1)) {
			fRec284[l192] = 0.0f;
			
		}
		for (int l193 = 0; (l193 < 2); l193 = (l193 + 1)) {
			fRec282[l193] = 0.0f;
			
		}
		for (int l194 = 0; (l194 < 2); l194 = (l194 + 1)) {
			fRec281[l194] = 0.0f;
			
		}
		for (int l195 = 0; (l195 < 2); l195 = (l195 + 1)) {
			fRec279[l195] = 0.0f;
			
		}
		for (int l196 = 0; (l196 < 2); l196 = (l196 + 1)) {
			fRec278[l196] = 0.0f;
			
		}
		for (int l197 = 0; (l197 < 2); l197 = (l197 + 1)) {
			fRec276[l197] = 0.0f;
			
		}
		for (int l198 = 0; (l198 < 2); l198 = (l198 + 1)) {
			fRec275[l198] = 0.0f;
			
		}
		for (int l199 = 0; (l199 < 2); l199 = (l199 + 1)) {
			fRec273[l199] = 0.0f;
			
		}
		for (int l200 = 0; (l200 < 3); l200 = (l200 + 1)) {
			fRec272[l200] = 0.0f;
			
		}
		for (int l201 = 0; (l201 < 2); l201 = (l201 + 1)) {
			fRec303[l201] = 0.0f;
			
		}
		for (int l202 = 0; (l202 < 2); l202 = (l202 + 1)) {
			fRec301[l202] = 0.0f;
			
		}
		for (int l203 = 0; (l203 < 2); l203 = (l203 + 1)) {
			fRec300[l203] = 0.0f;
			
		}
		for (int l204 = 0; (l204 < 2); l204 = (l204 + 1)) {
			fRec298[l204] = 0.0f;
			
		}
		for (int l205 = 0; (l205 < 2); l205 = (l205 + 1)) {
			fRec297[l205] = 0.0f;
			
		}
		for (int l206 = 0; (l206 < 2); l206 = (l206 + 1)) {
			fRec295[l206] = 0.0f;
			
		}
		for (int l207 = 0; (l207 < 2); l207 = (l207 + 1)) {
			fRec294[l207] = 0.0f;
			
		}
		for (int l208 = 0; (l208 < 2); l208 = (l208 + 1)) {
			fRec292[l208] = 0.0f;
			
		}
		for (int l209 = 0; (l209 < 2); l209 = (l209 + 1)) {
			fRec291[l209] = 0.0f;
			
		}
		for (int l210 = 0; (l210 < 2); l210 = (l210 + 1)) {
			fRec289[l210] = 0.0f;
			
		}
		for (int l211 = 0; (l211 < 3); l211 = (l211 + 1)) {
			fRec288[l211] = 0.0f;
			
		}
		for (int l212 = 0; (l212 < 2); l212 = (l212 + 1)) {
			fRec316[l212] = 0.0f;
			
		}
		for (int l213 = 0; (l213 < 2); l213 = (l213 + 1)) {
			fRec314[l213] = 0.0f;
			
		}
		for (int l214 = 0; (l214 < 2); l214 = (l214 + 1)) {
			fRec313[l214] = 0.0f;
			
		}
		for (int l215 = 0; (l215 < 2); l215 = (l215 + 1)) {
			fRec311[l215] = 0.0f;
			
		}
		for (int l216 = 0; (l216 < 2); l216 = (l216 + 1)) {
			fRec310[l216] = 0.0f;
			
		}
		for (int l217 = 0; (l217 < 2); l217 = (l217 + 1)) {
			fRec308[l217] = 0.0f;
			
		}
		for (int l218 = 0; (l218 < 2); l218 = (l218 + 1)) {
			fRec307[l218] = 0.0f;
			
		}
		for (int l219 = 0; (l219 < 2); l219 = (l219 + 1)) {
			fRec305[l219] = 0.0f;
			
		}
		for (int l220 = 0; (l220 < 3); l220 = (l220 + 1)) {
			fRec304[l220] = 0.0f;
			
		}
		for (int l221 = 0; (l221 < 2); l221 = (l221 + 1)) {
			fRec329[l221] = 0.0f;
			
		}
		for (int l222 = 0; (l222 < 2); l222 = (l222 + 1)) {
			fRec327[l222] = 0.0f;
			
		}
		for (int l223 = 0; (l223 < 2); l223 = (l223 + 1)) {
			fRec326[l223] = 0.0f;
			
		}
		for (int l224 = 0; (l224 < 2); l224 = (l224 + 1)) {
			fRec324[l224] = 0.0f;
			
		}
		for (int l225 = 0; (l225 < 2); l225 = (l225 + 1)) {
			fRec323[l225] = 0.0f;
			
		}
		for (int l226 = 0; (l226 < 2); l226 = (l226 + 1)) {
			fRec321[l226] = 0.0f;
			
		}
		for (int l227 = 0; (l227 < 2); l227 = (l227 + 1)) {
			fRec320[l227] = 0.0f;
			
		}
		for (int l228 = 0; (l228 < 2); l228 = (l228 + 1)) {
			fRec318[l228] = 0.0f;
			
		}
		for (int l229 = 0; (l229 < 3); l229 = (l229 + 1)) {
			fRec317[l229] = 0.0f;
			
		}
		for (int l230 = 0; (l230 < 2); l230 = (l230 + 1)) {
			fRec342[l230] = 0.0f;
			
		}
		for (int l231 = 0; (l231 < 2); l231 = (l231 + 1)) {
			fRec340[l231] = 0.0f;
			
		}
		for (int l232 = 0; (l232 < 2); l232 = (l232 + 1)) {
			fRec339[l232] = 0.0f;
			
		}
		for (int l233 = 0; (l233 < 2); l233 = (l233 + 1)) {
			fRec337[l233] = 0.0f;
			
		}
		for (int l234 = 0; (l234 < 2); l234 = (l234 + 1)) {
			fRec336[l234] = 0.0f;
			
		}
		for (int l235 = 0; (l235 < 2); l235 = (l235 + 1)) {
			fRec334[l235] = 0.0f;
			
		}
		for (int l236 = 0; (l236 < 2); l236 = (l236 + 1)) {
			fRec333[l236] = 0.0f;
			
		}
		for (int l237 = 0; (l237 < 2); l237 = (l237 + 1)) {
			fRec331[l237] = 0.0f;
			
		}
		for (int l238 = 0; (l238 < 3); l238 = (l238 + 1)) {
			fRec330[l238] = 0.0f;
			
		}
		for (int l239 = 0; (l239 < 2); l239 = (l239 + 1)) {
			fRec349[l239] = 0.0f;
			
		}
		for (int l240 = 0; (l240 < 2); l240 = (l240 + 1)) {
			fRec347[l240] = 0.0f;
			
		}
		for (int l241 = 0; (l241 < 2); l241 = (l241 + 1)) {
			fRec346[l241] = 0.0f;
			
		}
		for (int l242 = 0; (l242 < 2); l242 = (l242 + 1)) {
			fRec344[l242] = 0.0f;
			
		}
		for (int l243 = 0; (l243 < 3); l243 = (l243 + 1)) {
			fRec343[l243] = 0.0f;
			
		}
		for (int l244 = 0; (l244 < 2); l244 = (l244 + 1)) {
			fRec359[l244] = 0.0f;
			
		}
		for (int l245 = 0; (l245 < 2); l245 = (l245 + 1)) {
			fRec357[l245] = 0.0f;
			
		}
		for (int l246 = 0; (l246 < 2); l246 = (l246 + 1)) {
			fRec356[l246] = 0.0f;
			
		}
		for (int l247 = 0; (l247 < 2); l247 = (l247 + 1)) {
			fRec354[l247] = 0.0f;
			
		}
		for (int l248 = 0; (l248 < 2); l248 = (l248 + 1)) {
			fRec353[l248] = 0.0f;
			
		}
		for (int l249 = 0; (l249 < 2); l249 = (l249 + 1)) {
			fRec351[l249] = 0.0f;
			
		}
		for (int l250 = 0; (l250 < 3); l250 = (l250 + 1)) {
			fRec350[l250] = 0.0f;
			
		}
		for (int l251 = 0; (l251 < 2); l251 = (l251 + 1)) {
			fRec369[l251] = 0.0f;
			
		}
		for (int l252 = 0; (l252 < 2); l252 = (l252 + 1)) {
			fRec367[l252] = 0.0f;
			
		}
		for (int l253 = 0; (l253 < 2); l253 = (l253 + 1)) {
			fRec366[l253] = 0.0f;
			
		}
		for (int l254 = 0; (l254 < 2); l254 = (l254 + 1)) {
			fRec364[l254] = 0.0f;
			
		}
		for (int l255 = 0; (l255 < 2); l255 = (l255 + 1)) {
			fRec363[l255] = 0.0f;
			
		}
		for (int l256 = 0; (l256 < 2); l256 = (l256 + 1)) {
			fRec361[l256] = 0.0f;
			
		}
		for (int l257 = 0; (l257 < 3); l257 = (l257 + 1)) {
			fRec360[l257] = 0.0f;
			
		}
		for (int l258 = 0; (l258 < 2); l258 = (l258 + 1)) {
			fRec385[l258] = 0.0f;
			
		}
		for (int l259 = 0; (l259 < 2); l259 = (l259 + 1)) {
			fRec383[l259] = 0.0f;
			
		}
		for (int l260 = 0; (l260 < 2); l260 = (l260 + 1)) {
			fRec382[l260] = 0.0f;
			
		}
		for (int l261 = 0; (l261 < 2); l261 = (l261 + 1)) {
			fRec380[l261] = 0.0f;
			
		}
		for (int l262 = 0; (l262 < 2); l262 = (l262 + 1)) {
			fRec379[l262] = 0.0f;
			
		}
		for (int l263 = 0; (l263 < 2); l263 = (l263 + 1)) {
			fRec377[l263] = 0.0f;
			
		}
		for (int l264 = 0; (l264 < 2); l264 = (l264 + 1)) {
			fRec376[l264] = 0.0f;
			
		}
		for (int l265 = 0; (l265 < 2); l265 = (l265 + 1)) {
			fRec374[l265] = 0.0f;
			
		}
		for (int l266 = 0; (l266 < 2); l266 = (l266 + 1)) {
			fRec373[l266] = 0.0f;
			
		}
		for (int l267 = 0; (l267 < 2); l267 = (l267 + 1)) {
			fRec371[l267] = 0.0f;
			
		}
		for (int l268 = 0; (l268 < 3); l268 = (l268 + 1)) {
			fRec370[l268] = 0.0f;
			
		}
		for (int l269 = 0; (l269 < 2); l269 = (l269 + 1)) {
			fRec401[l269] = 0.0f;
			
		}
		for (int l270 = 0; (l270 < 2); l270 = (l270 + 1)) {
			fRec399[l270] = 0.0f;
			
		}
		for (int l271 = 0; (l271 < 2); l271 = (l271 + 1)) {
			fRec398[l271] = 0.0f;
			
		}
		for (int l272 = 0; (l272 < 2); l272 = (l272 + 1)) {
			fRec396[l272] = 0.0f;
			
		}
		for (int l273 = 0; (l273 < 2); l273 = (l273 + 1)) {
			fRec395[l273] = 0.0f;
			
		}
		for (int l274 = 0; (l274 < 2); l274 = (l274 + 1)) {
			fRec393[l274] = 0.0f;
			
		}
		for (int l275 = 0; (l275 < 2); l275 = (l275 + 1)) {
			fRec392[l275] = 0.0f;
			
		}
		for (int l276 = 0; (l276 < 2); l276 = (l276 + 1)) {
			fRec390[l276] = 0.0f;
			
		}
		for (int l277 = 0; (l277 < 2); l277 = (l277 + 1)) {
			fRec389[l277] = 0.0f;
			
		}
		for (int l278 = 0; (l278 < 2); l278 = (l278 + 1)) {
			fRec387[l278] = 0.0f;
			
		}
		for (int l279 = 0; (l279 < 3); l279 = (l279 + 1)) {
			fRec386[l279] = 0.0f;
			
		}
		for (int l280 = 0; (l280 < 2); l280 = (l280 + 1)) {
			fRec405[l280] = 0.0f;
			
		}
		for (int l281 = 0; (l281 < 2); l281 = (l281 + 1)) {
			fRec403[l281] = 0.0f;
			
		}
		for (int l282 = 0; (l282 < 3); l282 = (l282 + 1)) {
			fRec402[l282] = 0.0f;
			
		}
		for (int l283 = 0; (l283 < 2); l283 = (l283 + 1)) {
			fRec412[l283] = 0.0f;
			
		}
		for (int l284 = 0; (l284 < 2); l284 = (l284 + 1)) {
			fRec410[l284] = 0.0f;
			
		}
		for (int l285 = 0; (l285 < 2); l285 = (l285 + 1)) {
			fRec409[l285] = 0.0f;
			
		}
		for (int l286 = 0; (l286 < 2); l286 = (l286 + 1)) {
			fRec407[l286] = 0.0f;
			
		}
		for (int l287 = 0; (l287 < 3); l287 = (l287 + 1)) {
			fRec406[l287] = 0.0f;
			
		}
		for (int l288 = 0; (l288 < 2); l288 = (l288 + 1)) {
			fRec413[l288] = 0.0f;
			
		}
		for (int l289 = 0; (l289 < 12); l289 = (l289 + 1)) {
			fVec0[l289] = 0.0f;
			
		}
		for (int l290 = 0; (l290 < 13); l290 = (l290 + 1)) {
			fVec1[l290] = 0.0f;
			
		}
		for (int l291 = 0; (l291 < 12); l291 = (l291 + 1)) {
			fVec2[l291] = 0.0f;
			
		}
		for (int l292 = 0; (l292 < 12); l292 = (l292 + 1)) {
			fVec3[l292] = 0.0f;
			
		}
		for (int l293 = 0; (l293 < 13); l293 = (l293 + 1)) {
			fVec4[l293] = 0.0f;
			
		}
		for (int l294 = 0; (l294 < 12); l294 = (l294 + 1)) {
			fVec5[l294] = 0.0f;
			
		}
		IOTA = 0;
		for (int l295 = 0; (l295 < 32); l295 = (l295 + 1)) {
			fVec6[l295] = 0.0f;
			
		}
		for (int l296 = 0; (l296 < 32); l296 = (l296 + 1)) {
			fVec7[l296] = 0.0f;
			
		}
		for (int l297 = 0; (l297 < 32); l297 = (l297 + 1)) {
			fVec8[l297] = 0.0f;
			
		}
		for (int l298 = 0; (l298 < 32); l298 = (l298 + 1)) {
			fVec9[l298] = 0.0f;
			
		}
		for (int l299 = 0; (l299 < 32); l299 = (l299 + 1)) {
			fVec10[l299] = 0.0f;
			
		}
		for (int l300 = 0; (l300 < 32); l300 = (l300 + 1)) {
			fVec11[l300] = 0.0f;
			
		}
		for (int l301 = 0; (l301 < 32); l301 = (l301 + 1)) {
			fVec12[l301] = 0.0f;
			
		}
		for (int l302 = 0; (l302 < 32); l302 = (l302 + 1)) {
			fVec13[l302] = 0.0f;
			
		}
		for (int l303 = 0; (l303 < 32); l303 = (l303 + 1)) {
			fVec14[l303] = 0.0f;
			
		}
		for (int l304 = 0; (l304 < 32); l304 = (l304 + 1)) {
			fVec15[l304] = 0.0f;
			
		}
		for (int l305 = 0; (l305 < 32); l305 = (l305 + 1)) {
			fVec16[l305] = 0.0f;
			
		}
		for (int l306 = 0; (l306 < 32); l306 = (l306 + 1)) {
			fVec17[l306] = 0.0f;
			
		}
		for (int l307 = 0; (l307 < 64); l307 = (l307 + 1)) {
			fVec18[l307] = 0.0f;
			
		}
		for (int l308 = 0; (l308 < 64); l308 = (l308 + 1)) {
			fVec19[l308] = 0.0f;
			
		}
		for (int l309 = 0; (l309 < 64); l309 = (l309 + 1)) {
			fVec20[l309] = 0.0f;
			
		}
		for (int l310 = 0; (l310 < 64); l310 = (l310 + 1)) {
			fVec21[l310] = 0.0f;
			
		}
		for (int l311 = 0; (l311 < 64); l311 = (l311 + 1)) {
			fVec22[l311] = 0.0f;
			
		}
		for (int l312 = 0; (l312 < 64); l312 = (l312 + 1)) {
			fVec23[l312] = 0.0f;
			
		}
		for (int l313 = 0; (l313 < 64); l313 = (l313 + 1)) {
			fVec24[l313] = 0.0f;
			
		}
		for (int l314 = 0; (l314 < 64); l314 = (l314 + 1)) {
			fVec25[l314] = 0.0f;
			
		}
		for (int l315 = 0; (l315 < 64); l315 = (l315 + 1)) {
			fVec26[l315] = 0.0f;
			
		}
		for (int l316 = 0; (l316 < 64); l316 = (l316 + 1)) {
			fVec27[l316] = 0.0f;
			
		}
		for (int l317 = 0; (l317 < 64); l317 = (l317 + 1)) {
			fVec28[l317] = 0.0f;
			
		}
		for (int l318 = 0; (l318 < 64); l318 = (l318 + 1)) {
			fVec29[l318] = 0.0f;
			
		}
		
	}
	
	virtual void init(int samplingFreq) {
		classInit(samplingFreq);
		instanceInit(samplingFreq);
	}
	virtual void instanceInit(int samplingFreq) {
		instanceConstants(samplingFreq);
		instanceResetUserInterface();
		instanceClear();
	}
	
	virtual mydsp* clone() {
		return new mydsp();
	}
	virtual int getSampleRate() {
		return fSamplingFreq;
		
	}
	
	virtual void buildUserInterface(UI* ui_interface) {
		ui_interface->openVerticalBox("SATOswD3o5");
		ui_interface->declare(&fHslider2, "unit", "dB");
		ui_interface->addHorizontalSlider("gain", &fHslider2, -10.0f, -30.0f, 10.0f, 1.0f);
		ui_interface->declare(&fHslider1, "unit", "dB");
		ui_interface->addHorizontalSlider("lf/hf", &fHslider1, 0.0f, -3.0f, 3.0f, 0.100000001f);
		ui_interface->addCheckButton("mute", &fCheckbox0);
		ui_interface->declare(&fHslider0, "unit", "Hz");
		ui_interface->addHorizontalSlider("xover", &fHslider0, 400.0f, 200.0f, 800.0f, 20.0f);
		ui_interface->closeBox();
		
	}
	
	virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) {
		FAUSTFLOAT* input0 = inputs[0];
		FAUSTFLOAT* input1 = inputs[1];
		FAUSTFLOAT* input2 = inputs[2];
		FAUSTFLOAT* input3 = inputs[3];
		FAUSTFLOAT* input4 = inputs[4];
		FAUSTFLOAT* input5 = inputs[5];
		FAUSTFLOAT* input6 = inputs[6];
		FAUSTFLOAT* input7 = inputs[7];
		FAUSTFLOAT* input8 = inputs[8];
		FAUSTFLOAT* input9 = inputs[9];
		FAUSTFLOAT* input10 = inputs[10];
		FAUSTFLOAT* input11 = inputs[11];
		FAUSTFLOAT* input12 = inputs[12];
		FAUSTFLOAT* input13 = inputs[13];
		FAUSTFLOAT* input14 = inputs[14];
		FAUSTFLOAT* input15 = inputs[15];
		FAUSTFLOAT* input16 = inputs[16];
		FAUSTFLOAT* input17 = inputs[17];
		FAUSTFLOAT* input18 = inputs[18];
		FAUSTFLOAT* input19 = inputs[19];
		FAUSTFLOAT* input20 = inputs[20];
		FAUSTFLOAT* input21 = inputs[21];
		FAUSTFLOAT* input22 = inputs[22];
		FAUSTFLOAT* input23 = inputs[23];
		FAUSTFLOAT* input24 = inputs[24];
		FAUSTFLOAT* input25 = inputs[25];
		FAUSTFLOAT* input26 = inputs[26];
		FAUSTFLOAT* input27 = inputs[27];
		FAUSTFLOAT* input28 = inputs[28];
		FAUSTFLOAT* input29 = inputs[29];
		FAUSTFLOAT* input30 = inputs[30];
		FAUSTFLOAT* input31 = inputs[31];
		FAUSTFLOAT* input32 = inputs[32];
		FAUSTFLOAT* input33 = inputs[33];
		FAUSTFLOAT* input34 = inputs[34];
		FAUSTFLOAT* input35 = inputs[35];
		FAUSTFLOAT* output0 = outputs[0];
		FAUSTFLOAT* output1 = outputs[1];
		FAUSTFLOAT* output2 = outputs[2];
		FAUSTFLOAT* output3 = outputs[3];
		FAUSTFLOAT* output4 = outputs[4];
		FAUSTFLOAT* output5 = outputs[5];
		FAUSTFLOAT* output6 = outputs[6];
		FAUSTFLOAT* output7 = outputs[7];
		FAUSTFLOAT* output8 = outputs[8];
		FAUSTFLOAT* output9 = outputs[9];
		FAUSTFLOAT* output10 = outputs[10];
		FAUSTFLOAT* output11 = outputs[11];
		FAUSTFLOAT* output12 = outputs[12];
		FAUSTFLOAT* output13 = outputs[13];
		FAUSTFLOAT* output14 = outputs[14];
		FAUSTFLOAT* output15 = outputs[15];
		FAUSTFLOAT* output16 = outputs[16];
		FAUSTFLOAT* output17 = outputs[17];
		FAUSTFLOAT* output18 = outputs[18];
		FAUSTFLOAT* output19 = outputs[19];
		FAUSTFLOAT* output20 = outputs[20];
		FAUSTFLOAT* output21 = outputs[21];
		FAUSTFLOAT* output22 = outputs[22];
		FAUSTFLOAT* output23 = outputs[23];
		FAUSTFLOAT* output24 = outputs[24];
		FAUSTFLOAT* output25 = outputs[25];
		FAUSTFLOAT* output26 = outputs[26];
		FAUSTFLOAT* output27 = outputs[27];
		FAUSTFLOAT* output28 = outputs[28];
		FAUSTFLOAT* output29 = outputs[29];
		FAUSTFLOAT* output30 = outputs[30];
		float fSlow0 = (0.00100000005f * float(fHslider0));
		float fSlow1 = (0.00100000005f * powf(10.0f, (0.0500000007f * float(fHslider1))));
		float fSlow2 = (0.00100000005f * (float((float(fCheckbox0) < 0.5f)) * powf(10.0f, (0.0500000007f * float(fHslider2)))));
		for (int i = 0; (i < count); i = (i + 1)) {
			float fTemp0 = (fConst3 * fRec1[1]);
			float fTemp1 = (fConst6 * fRec4[1]);
			float fTemp2 = (fConst7 * fRec7[1]);
			float fTemp3 = (fConst9 * float(input33[i]));
			float fTemp4 = (fConst10 * fRec10[1]);
			float fTemp5 = (fConst11 * fRec13[1]);
			fRec15[0] = (fTemp3 + (fTemp4 + (fRec15[1] + fTemp5)));
			fRec13[0] = fRec15[0];
			float fRec14 = ((fTemp5 + fTemp4) + fTemp3);
			fRec12[0] = (fRec13[0] + fRec12[1]);
			fRec10[0] = fRec12[0];
			float fRec11 = fRec14;
			fRec9[0] = (fTemp1 + (fTemp2 + (fRec11 + fRec9[1])));
			fRec7[0] = fRec9[0];
			float fRec8 = (fTemp1 + (fRec11 + fTemp2));
			fRec6[0] = (fRec7[0] + fRec6[1]);
			fRec4[0] = fRec6[0];
			float fRec5 = fRec8;
			fRec3[0] = (fTemp0 + (fRec5 + fRec3[1]));
			fRec1[0] = fRec3[0];
			float fRec2 = (fRec5 + fTemp0);
			fRec16[0] = (fSlow0 + (0.999000013f * fRec16[1]));
			float fTemp6 = tanf((fConst12 * fRec16[0]));
			float fTemp7 = ((fTemp6 * (fTemp6 + -2.0f)) + 1.0f);
			float fTemp8 = mydsp_faustpower2_f(fTemp6);
			float fTemp9 = (fTemp8 + -1.0f);
			float fTemp10 = ((fTemp6 * (fTemp6 + 2.0f)) + 1.0f);
			fRec0[0] = (fRec2 - (((fRec0[2] * fTemp7) + (2.0f * (fRec0[1] * fTemp9))) / fTemp10));
			fRec17[0] = (fSlow1 + (0.999000013f * fRec17[1]));
			float fTemp11 = (fRec17[0] * fTemp10);
			float fTemp12 = (0.0f - (2.0f / fTemp10));
			float fTemp13 = ((((fRec0[2] + (fRec0[0] + (2.0f * fRec0[1]))) * fTemp8) / fTemp11) + (0.205712318f * (fRec17[0] * (0.0f - ((fRec0[1] * fTemp12) + ((fRec0[0] + fRec0[2]) / fTemp10))))));
			float fTemp14 = (fConst3 * fRec19[1]);
			float fTemp15 = (fConst6 * fRec22[1]);
			float fTemp16 = (fConst7 * fRec25[1]);
			float fTemp17 = (fConst9 * float(input31[i]));
			float fTemp18 = (fConst10 * fRec28[1]);
			float fTemp19 = (fConst11 * fRec31[1]);
			fRec33[0] = (fTemp17 + (fTemp18 + (fRec33[1] + fTemp19)));
			fRec31[0] = fRec33[0];
			float fRec32 = ((fTemp19 + fTemp18) + fTemp17);
			fRec30[0] = (fRec31[0] + fRec30[1]);
			fRec28[0] = fRec30[0];
			float fRec29 = fRec32;
			fRec27[0] = (fTemp15 + (fTemp16 + (fRec29 + fRec27[1])));
			fRec25[0] = fRec27[0];
			float fRec26 = (fTemp15 + (fRec29 + fTemp16));
			fRec24[0] = (fRec25[0] + fRec24[1]);
			fRec22[0] = fRec24[0];
			float fRec23 = fRec26;
			fRec21[0] = (fTemp14 + (fRec23 + fRec21[1]));
			fRec19[0] = fRec21[0];
			float fRec20 = (fRec23 + fTemp14);
			fRec18[0] = (fRec20 - (((fRec18[2] * fTemp7) + (2.0f * (fRec18[1] * fTemp9))) / fTemp10));
			float fTemp20 = ((((fRec18[2] + (fRec18[0] + (2.0f * fRec18[1]))) * fTemp8) / fTemp11) + (0.205712318f * (fRec17[0] * (0.0f - ((fRec18[1] * fTemp12) + ((fRec18[0] + fRec18[2]) / fTemp10))))));
			float fTemp21 = (fConst3 * fRec35[1]);
			float fTemp22 = (fConst6 * fRec38[1]);
			float fTemp23 = (fConst7 * fRec41[1]);
			float fTemp24 = (fConst9 * float(input30[i]));
			float fTemp25 = (fConst10 * fRec44[1]);
			float fTemp26 = (fConst11 * fRec47[1]);
			fRec49[0] = (fTemp24 + (fTemp25 + (fRec49[1] + fTemp26)));
			fRec47[0] = fRec49[0];
			float fRec48 = ((fTemp26 + fTemp25) + fTemp24);
			fRec46[0] = (fRec47[0] + fRec46[1]);
			fRec44[0] = fRec46[0];
			float fRec45 = fRec48;
			fRec43[0] = (fTemp22 + (fTemp23 + (fRec45 + fRec43[1])));
			fRec41[0] = fRec43[0];
			float fRec42 = (fTemp22 + (fRec45 + fTemp23));
			fRec40[0] = (fRec41[0] + fRec40[1]);
			fRec38[0] = fRec40[0];
			float fRec39 = fRec42;
			fRec37[0] = (fTemp21 + (fRec39 + fRec37[1]));
			fRec35[0] = fRec37[0];
			float fRec36 = (fRec39 + fTemp21);
			fRec34[0] = (fRec36 - (((fRec34[2] * fTemp7) + (2.0f * (fRec34[1] * fTemp9))) / fTemp10));
			float fTemp27 = ((((fRec34[2] + (fRec34[0] + (2.0f * fRec34[1]))) * fTemp8) / fTemp11) + (0.205712318f * (fRec17[0] * (0.0f - ((fRec34[1] * fTemp12) + ((fRec34[0] + fRec34[2]) / fTemp10))))));
			float fTemp28 = (fConst3 * fRec51[1]);
			float fTemp29 = (fConst6 * fRec54[1]);
			float fTemp30 = (fConst7 * fRec57[1]);
			float fTemp31 = (fConst9 * float(input28[i]));
			float fTemp32 = (fConst10 * fRec60[1]);
			float fTemp33 = (fConst11 * fRec63[1]);
			fRec65[0] = (fTemp31 + (fTemp32 + (fRec65[1] + fTemp33)));
			fRec63[0] = fRec65[0];
			float fRec64 = ((fTemp33 + fTemp32) + fTemp31);
			fRec62[0] = (fRec63[0] + fRec62[1]);
			fRec60[0] = fRec62[0];
			float fRec61 = fRec64;
			fRec59[0] = (fTemp29 + (fTemp30 + (fRec61 + fRec59[1])));
			fRec57[0] = fRec59[0];
			float fRec58 = (fTemp29 + (fRec61 + fTemp30));
			fRec56[0] = (fRec57[0] + fRec56[1]);
			fRec54[0] = fRec56[0];
			float fRec55 = fRec58;
			fRec53[0] = (fTemp28 + (fRec55 + fRec53[1]));
			fRec51[0] = fRec53[0];
			float fRec52 = (fRec55 + fTemp28);
			fRec50[0] = (fRec52 - (((fRec50[2] * fTemp7) + (2.0f * (fRec50[1] * fTemp9))) / fTemp10));
			float fTemp34 = ((((fRec50[2] + (fRec50[0] + (2.0f * fRec50[1]))) * fTemp8) / fTemp11) + (0.205712318f * (fRec17[0] * (0.0f - ((fRec50[1] * fTemp12) + ((fRec50[0] + fRec50[2]) / fTemp10))))));
			float fTemp35 = (fConst3 * fRec67[1]);
			float fTemp36 = (fConst6 * fRec70[1]);
			float fTemp37 = (fConst7 * fRec73[1]);
			float fTemp38 = (fConst9 * float(input27[i]));
			float fTemp39 = (fConst10 * fRec76[1]);
			float fTemp40 = (fConst11 * fRec79[1]);
			fRec81[0] = (fTemp38 + (fTemp39 + (fRec81[1] + fTemp40)));
			fRec79[0] = fRec81[0];
			float fRec80 = ((fTemp40 + fTemp39) + fTemp38);
			fRec78[0] = (fRec79[0] + fRec78[1]);
			fRec76[0] = fRec78[0];
			float fRec77 = fRec80;
			fRec75[0] = (fTemp36 + (fTemp37 + (fRec77 + fRec75[1])));
			fRec73[0] = fRec75[0];
			float fRec74 = (fTemp36 + (fRec77 + fTemp37));
			fRec72[0] = (fRec73[0] + fRec72[1]);
			fRec70[0] = fRec72[0];
			float fRec71 = fRec74;
			fRec69[0] = (fTemp35 + (fRec69[1] + fRec71));
			fRec67[0] = fRec69[0];
			float fRec68 = (fRec71 + fTemp35);
			fRec66[0] = (fRec68 - (((fRec66[2] * fTemp7) + (2.0f * (fRec66[1] * fTemp9))) / fTemp10));
			float fTemp41 = ((0.205712318f * (fRec17[0] * (0.0f - (((fRec66[0] + fRec66[2]) / fTemp10) + (fRec66[1] * fTemp12))))) + (((fRec66[2] + (fRec66[0] + (2.0f * fRec66[1]))) * fTemp8) / fTemp11));
			float fTemp42 = (fConst15 * float(input20[i]));
			float fTemp43 = (fConst16 * fRec89[1]);
			float fTemp44 = (fConst17 * fRec92[1]);
			fRec94[0] = (fTemp42 + (fTemp43 + (fRec94[1] + fTemp44)));
			fRec92[0] = fRec94[0];
			float fRec93 = ((fTemp44 + fTemp43) + fTemp42);
			fRec91[0] = (fRec92[0] + fRec91[1]);
			fRec89[0] = fRec91[0];
			float fRec90 = fRec93;
			float fTemp45 = (fConst18 * fRec83[1]);
			float fTemp46 = (fConst19 * fRec86[1]);
			fRec88[0] = (fRec90 + (fTemp45 + (fRec88[1] + fTemp46)));
			fRec86[0] = fRec88[0];
			float fRec87 = ((fTemp46 + fTemp45) + fRec90);
			fRec85[0] = (fRec85[1] + fRec86[0]);
			fRec83[0] = fRec85[0];
			float fRec84 = fRec87;
			fRec82[0] = (fRec84 - (((fRec82[2] * fTemp7) + (2.0f * (fRec82[1] * fTemp9))) / fTemp10));
			float fTemp47 = (((((fRec82[0] + (2.0f * fRec82[1])) + fRec82[2]) * fTemp8) / fTemp11) + (0.422004998f * (fRec17[0] * (0.0f - ((fRec82[1] * fTemp12) + ((fRec82[0] + fRec82[2]) / fTemp10))))));
			float fTemp48 = (fConst18 * fRec96[1]);
			float fTemp49 = (fConst19 * fRec99[1]);
			float fTemp50 = (fConst15 * float(input18[i]));
			float fTemp51 = (fConst16 * fRec102[1]);
			float fTemp52 = (fConst17 * fRec105[1]);
			fRec107[0] = (fTemp50 + (fTemp51 + (fRec107[1] + fTemp52)));
			fRec105[0] = fRec107[0];
			float fRec106 = ((fTemp52 + fTemp51) + fTemp50);
			fRec104[0] = (fRec105[0] + fRec104[1]);
			fRec102[0] = fRec104[0];
			float fRec103 = fRec106;
			fRec101[0] = (fTemp48 + (fTemp49 + (fRec103 + fRec101[1])));
			fRec99[0] = fRec101[0];
			float fRec100 = (fTemp48 + (fRec103 + fTemp49));
			fRec98[0] = (fRec99[0] + fRec98[1]);
			fRec96[0] = fRec98[0];
			float fRec97 = fRec100;
			fRec95[0] = (fRec97 - (((fRec95[2] * fTemp7) + (2.0f * (fRec95[1] * fTemp9))) / fTemp10));
			float fTemp53 = ((((fRec95[2] + (fRec95[0] + (2.0f * fRec95[1]))) * fTemp8) / fTemp11) + (0.422004998f * (fRec17[0] * (0.0f - ((fRec95[1] * fTemp12) + ((fRec95[0] + fRec95[2]) / fTemp10))))));
			float fTemp54 = (fConst18 * fRec109[1]);
			float fTemp55 = (fConst19 * fRec112[1]);
			float fTemp56 = (fConst15 * float(input17[i]));
			float fTemp57 = (fConst16 * fRec115[1]);
			float fTemp58 = (fConst17 * fRec118[1]);
			fRec120[0] = (fTemp56 + (fTemp57 + (fRec120[1] + fTemp58)));
			fRec118[0] = fRec120[0];
			float fRec119 = ((fTemp58 + fTemp57) + fTemp56);
			fRec117[0] = (fRec118[0] + fRec117[1]);
			fRec115[0] = fRec117[0];
			float fRec116 = fRec119;
			fRec114[0] = (fTemp54 + (fTemp55 + (fRec116 + fRec114[1])));
			fRec112[0] = fRec114[0];
			float fRec113 = (fTemp54 + (fRec116 + fTemp55));
			fRec111[0] = (fRec112[0] + fRec111[1]);
			fRec109[0] = fRec111[0];
			float fRec110 = fRec113;
			fRec108[0] = (fRec110 - (((fRec108[2] * fTemp7) + (2.0f * (fRec108[1] * fTemp9))) / fTemp10));
			float fTemp59 = ((((fRec108[2] + (fRec108[0] + (2.0f * fRec108[1]))) * fTemp8) / fTemp11) + (0.422004998f * (fRec17[0] * (0.0f - ((fRec108[1] * fTemp12) + ((fRec108[0] + fRec108[2]) / fTemp10))))));
			float fTemp60 = (fConst18 * fRec122[1]);
			float fTemp61 = (fConst19 * fRec125[1]);
			float fTemp62 = (fConst15 * float(input16[i]));
			float fTemp63 = (fConst16 * fRec128[1]);
			float fTemp64 = (fConst17 * fRec131[1]);
			fRec133[0] = (fTemp62 + (fTemp63 + (fRec133[1] + fTemp64)));
			fRec131[0] = fRec133[0];
			float fRec132 = ((fTemp64 + fTemp63) + fTemp62);
			fRec130[0] = (fRec131[0] + fRec130[1]);
			fRec128[0] = fRec130[0];
			float fRec129 = fRec132;
			fRec127[0] = (fTemp60 + (fTemp61 + (fRec129 + fRec127[1])));
			fRec125[0] = fRec127[0];
			float fRec126 = (fTemp60 + (fRec129 + fTemp61));
			fRec124[0] = (fRec125[0] + fRec124[1]);
			fRec122[0] = fRec124[0];
			float fRec123 = fRec126;
			fRec121[0] = (fRec123 - (((fRec121[2] * fTemp7) + (2.0f * (fRec121[1] * fTemp9))) / fTemp10));
			float fTemp65 = ((((fRec121[2] + (fRec121[0] + (2.0f * fRec121[1]))) * fTemp8) / fTemp11) + (0.422004998f * (fRec17[0] * (0.0f - ((fRec121[1] * fTemp12) + ((fRec121[0] + fRec121[2]) / fTemp10))))));
			float fTemp66 = (fConst22 * float(input15[i]));
			float fTemp67 = (fConst23 * fRec138[1]);
			float fTemp68 = (fConst24 * fRec141[1]);
			fRec143[0] = (fTemp66 + (fTemp67 + (fRec143[1] + fTemp68)));
			fRec141[0] = fRec143[0];
			float fRec142 = ((fTemp68 + fTemp67) + fTemp66);
			fRec140[0] = (fRec140[1] + fRec141[0]);
			fRec138[0] = fRec140[0];
			float fRec139 = fRec142;
			float fTemp69 = (fConst25 * fRec135[1]);
			fRec137[0] = (fRec139 + (fRec137[1] + fTemp69));
			fRec135[0] = fRec137[0];
			float fRec136 = (fTemp69 + fRec139);
			fRec134[0] = (fRec136 - (((fRec134[2] * fTemp7) + (2.0f * (fRec134[1] * fTemp9))) / fTemp10));
			float fTemp70 = ((((fRec134[2] + (fRec134[0] + (2.0f * fRec134[1]))) * fTemp8) / fTemp11) + (0.628249943f * (fRec17[0] * (0.0f - ((fRec134[1] * fTemp12) + ((fRec134[0] + fRec134[2]) / fTemp10))))));
			float fTemp71 = (fConst25 * fRec145[1]);
			float fTemp72 = (fConst22 * float(input10[i]));
			float fTemp73 = (fConst23 * fRec148[1]);
			float fTemp74 = (fConst24 * fRec151[1]);
			fRec153[0] = (fTemp72 + (fTemp73 + (fRec153[1] + fTemp74)));
			fRec151[0] = fRec153[0];
			float fRec152 = ((fTemp74 + fTemp73) + fTemp72);
			fRec150[0] = (fRec151[0] + fRec150[1]);
			fRec148[0] = fRec150[0];
			float fRec149 = fRec152;
			fRec147[0] = (fTemp71 + (fRec149 + fRec147[1]));
			fRec145[0] = fRec147[0];
			float fRec146 = (fRec149 + fTemp71);
			fRec144[0] = (fRec146 - (((fRec144[2] * fTemp7) + (2.0f * (fRec144[1] * fTemp9))) / fTemp10));
			float fTemp75 = ((((fRec144[2] + (fRec144[0] + (2.0f * fRec144[1]))) * fTemp8) / fTemp11) + (0.628249943f * (fRec17[0] * (0.0f - ((fRec144[1] * fTemp12) + ((fRec144[0] + fRec144[2]) / fTemp10))))));
			float fTemp76 = (fConst25 * fRec155[1]);
			float fTemp77 = (fConst22 * float(input13[i]));
			float fTemp78 = (fConst23 * fRec158[1]);
			float fTemp79 = (fConst24 * fRec161[1]);
			fRec163[0] = (fTemp77 + (fTemp78 + (fRec163[1] + fTemp79)));
			fRec161[0] = fRec163[0];
			float fRec162 = ((fTemp79 + fTemp78) + fTemp77);
			fRec160[0] = (fRec161[0] + fRec160[1]);
			fRec158[0] = fRec160[0];
			float fRec159 = fRec162;
			fRec157[0] = (fTemp76 + (fRec159 + fRec157[1]));
			fRec155[0] = fRec157[0];
			float fRec156 = (fRec159 + fTemp76);
			fRec154[0] = (fRec156 - (((fRec154[2] * fTemp7) + (2.0f * (fRec154[1] * fTemp9))) / fTemp10));
			float fTemp80 = ((((fRec154[2] + (fRec154[0] + (2.0f * fRec154[1]))) * fTemp8) / fTemp11) + (0.628249943f * (fRec17[0] * (0.0f - ((fRec154[1] * fTemp12) + ((fRec154[0] + fRec154[2]) / fTemp10))))));
			float fTemp81 = (fConst25 * fRec165[1]);
			float fTemp82 = (fConst22 * float(input12[i]));
			float fTemp83 = (fConst23 * fRec168[1]);
			float fTemp84 = (fConst24 * fRec171[1]);
			fRec173[0] = (fTemp82 + (fTemp83 + (fRec173[1] + fTemp84)));
			fRec171[0] = fRec173[0];
			float fRec172 = ((fTemp84 + fTemp83) + fTemp82);
			fRec170[0] = (fRec171[0] + fRec170[1]);
			fRec168[0] = fRec170[0];
			float fRec169 = fRec172;
			fRec167[0] = (fTemp81 + (fRec169 + fRec167[1]));
			fRec165[0] = fRec167[0];
			float fRec166 = (fRec169 + fTemp81);
			fRec164[0] = (fRec166 - (((fRec164[2] * fTemp7) + (2.0f * (fRec164[1] * fTemp9))) / fTemp10));
			float fTemp85 = ((((fRec164[2] + (fRec164[0] + (2.0f * fRec164[1]))) * fTemp8) / fTemp11) + (0.628249943f * (fRec17[0] * (0.0f - ((fRec164[1] * fTemp12) + ((fRec164[0] + fRec164[2]) / fTemp10))))));
			float fTemp86 = (fConst18 * fRec175[1]);
			float fTemp87 = (fConst19 * fRec178[1]);
			float fTemp88 = (fConst15 * float(input23[i]));
			float fTemp89 = (fConst16 * fRec181[1]);
			float fTemp90 = (fConst17 * fRec184[1]);
			fRec186[0] = (fTemp88 + (fTemp89 + (fRec186[1] + fTemp90)));
			fRec184[0] = fRec186[0];
			float fRec185 = ((fTemp90 + fTemp89) + fTemp88);
			fRec183[0] = (fRec183[1] + fRec184[0]);
			fRec181[0] = fRec183[0];
			float fRec182 = fRec185;
			fRec180[0] = ((fTemp86 + (fRec180[1] + fTemp87)) + fRec182);
			fRec178[0] = fRec180[0];
			float fRec179 = ((fTemp87 + fTemp86) + fRec182);
			fRec177[0] = (fRec177[1] + fRec178[0]);
			fRec175[0] = fRec177[0];
			float fRec176 = fRec179;
			fRec174[0] = (fRec176 - (((fRec174[2] * fTemp7) + (2.0f * (fRec174[1] * fTemp9))) / fTemp10));
			float fTemp91 = ((((fRec174[2] + ((2.0f * fRec174[1]) + fRec174[0])) * fTemp8) / fTemp11) + (0.422004998f * ((0.0f - ((fRec174[1] * fTemp12) + ((fRec174[2] + fRec174[0]) / fTemp10))) * fRec17[0])));
			float fTemp92 = (fConst3 * fRec188[1]);
			float fTemp93 = (fConst6 * fRec191[1]);
			float fTemp94 = (fConst7 * fRec194[1]);
			float fTemp95 = (fConst9 * float(input26[i]));
			float fTemp96 = (fConst10 * fRec197[1]);
			float fTemp97 = (fConst11 * fRec200[1]);
			fRec202[0] = (fTemp95 + (fTemp96 + (fRec202[1] + fTemp97)));
			fRec200[0] = fRec202[0];
			float fRec201 = ((fTemp97 + fTemp96) + fTemp95);
			fRec199[0] = (fRec200[0] + fRec199[1]);
			fRec197[0] = fRec199[0];
			float fRec198 = fRec201;
			fRec196[0] = (fTemp93 + (fTemp94 + (fRec198 + fRec196[1])));
			fRec194[0] = fRec196[0];
			float fRec195 = (fTemp93 + (fRec198 + fTemp94));
			fRec193[0] = (fRec194[0] + fRec193[1]);
			fRec191[0] = fRec193[0];
			float fRec192 = fRec195;
			fRec190[0] = (fTemp92 + (fRec192 + fRec190[1]));
			fRec188[0] = fRec190[0];
			float fRec189 = (fRec192 + fTemp92);
			fRec187[0] = (fRec189 - (((fRec187[2] * fTemp7) + (2.0f * (fRec187[1] * fTemp9))) / fTemp10));
			float fTemp98 = ((0.205712318f * ((0.0f - ((fRec187[1] * fTemp12) + ((fRec187[0] + fRec187[2]) / fTemp10))) * fRec17[0])) + (((fRec187[2] + (fRec187[0] + (2.0f * fRec187[1]))) * fTemp8) / fTemp11));
			float fTemp99 = (fConst27 * float(input7[i]));
			float fTemp100 = (fConst28 * fRec204[1]);
			float fTemp101 = (fConst29 * fRec207[1]);
			fRec209[0] = (fTemp99 + (fTemp100 + (fRec209[1] + fTemp101)));
			fRec207[0] = fRec209[0];
			float fRec208 = ((fTemp101 + fTemp100) + fTemp99);
			fRec206[0] = (fRec207[0] + fRec206[1]);
			fRec204[0] = fRec206[0];
			float fRec205 = fRec208;
			fRec203[0] = (fRec205 - (((fTemp7 * fRec203[2]) + (2.0f * (fTemp9 * fRec203[1]))) / fTemp10));
			float fTemp102 = ((0.804249108f * (fRec17[0] * (0.0f - (((fRec203[0] + fRec203[2]) / fTemp10) + (fTemp12 * fRec203[1]))))) + ((fTemp8 * (fRec203[2] + (fRec203[0] + (2.0f * fRec203[1])))) / fTemp11));
			float fTemp103 = (((fConst24 * fRec217[1]) + (fConst23 * fRec214[1])) + (fConst22 * float(input9[i])));
			fRec219[0] = (fTemp103 + fRec219[1]);
			fRec217[0] = fRec219[0];
			float fRec218 = fTemp103;
			fRec216[0] = (fRec217[0] + fRec216[1]);
			fRec214[0] = fRec216[0];
			float fRec215 = fRec218;
			float fTemp104 = (fConst25 * fRec211[1]);
			fRec213[0] = ((fRec215 + fRec213[1]) + fTemp104);
			fRec211[0] = fRec213[0];
			float fRec212 = (fRec215 + fTemp104);
			fRec210[0] = (fRec212 - (((fRec210[2] * fTemp7) + (2.0f * (fRec210[1] * fTemp9))) / fTemp10));
			float fTemp105 = ((((fRec210[2] + (fRec210[0] + (2.0f * fRec210[1]))) * fTemp8) / fTemp11) + (0.628249943f * (fRec17[0] * (0.0f - ((fRec210[1] * fTemp12) + ((fRec210[0] + fRec210[2]) / fTemp10))))));
			float fTemp106 = ((fConst16 * fRec227[1]) + (fConst15 * float(input21[i])));
			float fTemp107 = (fConst17 * fRec230[1]);
			fRec232[0] = ((fTemp106 + fRec232[1]) + fTemp107);
			fRec230[0] = fRec232[0];
			float fRec231 = (fTemp106 + fTemp107);
			fRec229[0] = (fRec230[0] + fRec229[1]);
			fRec227[0] = fRec229[0];
			float fRec228 = fRec231;
			float fTemp108 = (fConst19 * fRec224[1]);
			float fTemp109 = (fConst18 * fRec221[1]);
			fRec226[0] = (((fRec228 + fRec226[1]) + fTemp108) + fTemp109);
			fRec224[0] = fRec226[0];
			float fRec225 = ((fRec228 + fTemp108) + fTemp109);
			fRec223[0] = (fRec224[0] + fRec223[1]);
			fRec221[0] = fRec223[0];
			float fRec222 = fRec225;
			fRec220[0] = (fRec222 - (((fRec220[2] * fTemp7) + (2.0f * (fRec220[1] * fTemp9))) / fTemp10));
			float fTemp110 = ((((fRec220[2] + (fRec220[0] + (2.0f * fRec220[1]))) * fTemp8) / fTemp11) + (0.422004998f * (fRec17[0] * (0.0f - ((fRec220[1] * fTemp12) + ((fRec220[0] + fRec220[2]) / fTemp10))))));
			fRec233[0] = (float(input0[i]) - (((fRec233[2] * fTemp7) + (2.0f * (fRec233[1] * fTemp9))) / fTemp10));
			float fTemp111 = (((fTemp8 * (fRec233[2] + (fRec233[0] + (2.0f * fRec233[1])))) / fTemp11) + (fRec17[0] * (0.0f - ((fRec233[1] * fTemp12) + ((fRec233[0] + fRec233[2]) / fTemp10)))));
			float fTemp112 = (fConst31 * float(input2[i]));
			float fTemp113 = (fConst32 * fRec235[1]);
			fRec237[0] = (fTemp112 + (fRec237[1] + fTemp113));
			fRec235[0] = fRec237[0];
			float fRec236 = (fTemp113 + fTemp112);
			fRec234[0] = (fRec236 - (((fTemp7 * fRec234[2]) + (2.0f * (fTemp9 * fRec234[1]))) / fTemp10));
			float fTemp114 = (((fTemp8 * (fRec234[2] + (fRec234[0] + (2.0f * fRec234[1])))) / fTemp11) + (0.932469487f * (fRec17[0] * (0.0f - ((fTemp12 * fRec234[1]) + ((fRec234[0] + fRec234[2]) / fTemp10))))));
			float fTemp115 = (fConst31 * float(input3[i]));
			float fTemp116 = (fConst32 * fRec239[1]);
			fRec241[0] = (fTemp115 + (fRec241[1] + fTemp116));
			fRec239[0] = fRec241[0];
			float fRec240 = (fTemp116 + fTemp115);
			fRec238[0] = (fRec240 - (((fTemp7 * fRec238[2]) + (2.0f * (fTemp9 * fRec238[1]))) / fTemp10));
			float fTemp117 = (((fTemp8 * (fRec238[2] + (fRec238[0] + (2.0f * fRec238[1])))) / fTemp11) + (0.932469487f * (fRec17[0] * (0.0f - ((fTemp12 * fRec238[1]) + ((fRec238[0] + fRec238[2]) / fTemp10))))));
			float fTemp118 = (fConst27 * float(input4[i]));
			float fTemp119 = (fConst28 * fRec243[1]);
			float fTemp120 = (fConst29 * fRec246[1]);
			fRec248[0] = (fTemp118 + (fTemp119 + (fRec248[1] + fTemp120)));
			fRec246[0] = fRec248[0];
			float fRec247 = ((fTemp120 + fTemp119) + fTemp118);
			fRec245[0] = (fRec246[0] + fRec245[1]);
			fRec243[0] = fRec245[0];
			float fRec244 = fRec247;
			fRec242[0] = (fRec244 - (((fTemp7 * fRec242[2]) + (2.0f * (fTemp9 * fRec242[1]))) / fTemp10));
			float fTemp121 = (((fTemp8 * (fRec242[2] + (fRec242[0] + (2.0f * fRec242[1])))) / fTemp11) + (0.804249108f * (fRec17[0] * (0.0f - ((fTemp12 * fRec242[1]) + ((fRec242[0] + fRec242[2]) / fTemp10))))));
			float fTemp122 = (fConst27 * float(input6[i]));
			float fTemp123 = (fConst28 * fRec250[1]);
			float fTemp124 = (fConst29 * fRec253[1]);
			fRec255[0] = (fTemp122 + (fTemp123 + (fRec255[1] + fTemp124)));
			fRec253[0] = fRec255[0];
			float fRec254 = ((fTemp124 + fTemp123) + fTemp122);
			fRec252[0] = (fRec253[0] + fRec252[1]);
			fRec250[0] = fRec252[0];
			float fRec251 = fRec254;
			fRec249[0] = (fRec251 - (((fTemp7 * fRec249[2]) + (2.0f * (fTemp9 * fRec249[1]))) / fTemp10));
			float fTemp125 = (((fTemp8 * (fRec249[2] + (fRec249[0] + (2.0f * fRec249[1])))) / fTemp11) + (0.804249108f * (fRec17[0] * (0.0f - ((fTemp12 * fRec249[1]) + ((fRec249[0] + fRec249[2]) / fTemp10))))));
			float fTemp126 = (fConst3 * fRec257[1]);
			float fTemp127 = (fConst6 * fRec260[1]);
			float fTemp128 = (fConst7 * fRec263[1]);
			float fTemp129 = (fConst9 * float(input35[i]));
			float fTemp130 = (fConst10 * fRec266[1]);
			float fTemp131 = (fConst11 * fRec269[1]);
			fRec271[0] = (fTemp129 + (fTemp130 + (fRec271[1] + fTemp131)));
			fRec269[0] = fRec271[0];
			float fRec270 = ((fTemp131 + fTemp130) + fTemp129);
			fRec268[0] = (fRec269[0] + fRec268[1]);
			fRec266[0] = fRec268[0];
			float fRec267 = fRec270;
			fRec265[0] = (fTemp127 + (fTemp128 + (fRec267 + fRec265[1])));
			fRec263[0] = fRec265[0];
			float fRec264 = (fTemp127 + (fRec267 + fTemp128));
			fRec262[0] = (fRec263[0] + fRec262[1]);
			fRec260[0] = fRec262[0];
			float fRec261 = fRec264;
			fRec259[0] = (fTemp126 + (fRec261 + fRec259[1]));
			fRec257[0] = fRec259[0];
			float fRec258 = (fRec261 + fTemp126);
			fRec256[0] = (fRec258 - (((fRec256[2] * fTemp7) + (2.0f * (fRec256[1] * fTemp9))) / fTemp10));
			float fTemp132 = ((((fRec256[2] + (fRec256[0] + (2.0f * fRec256[1]))) * fTemp8) / fTemp11) + (0.205712318f * (fRec17[0] * (0.0f - ((fRec256[1] * fTemp12) + ((fRec256[0] + fRec256[2]) / fTemp10))))));
			float fTemp133 = (fConst3 * fRec273[1]);
			float fTemp134 = (fConst6 * fRec276[1]);
			float fTemp135 = (fConst7 * fRec279[1]);
			float fTemp136 = (fConst9 * float(input34[i]));
			float fTemp137 = (fConst10 * fRec282[1]);
			float fTemp138 = (fConst11 * fRec285[1]);
			fRec287[0] = (fTemp136 + (fTemp137 + (fRec287[1] + fTemp138)));
			fRec285[0] = fRec287[0];
			float fRec286 = ((fTemp138 + fTemp137) + fTemp136);
			fRec284[0] = (fRec285[0] + fRec284[1]);
			fRec282[0] = fRec284[0];
			float fRec283 = fRec286;
			fRec281[0] = (fTemp134 + (fTemp135 + (fRec283 + fRec281[1])));
			fRec279[0] = fRec281[0];
			float fRec280 = (fTemp134 + (fRec283 + fTemp135));
			fRec278[0] = (fRec279[0] + fRec278[1]);
			fRec276[0] = fRec278[0];
			float fRec277 = fRec280;
			fRec275[0] = (fTemp133 + (fRec277 + fRec275[1]));
			fRec273[0] = fRec275[0];
			float fRec274 = (fRec277 + fTemp133);
			fRec272[0] = (fRec274 - (((fRec272[2] * fTemp7) + (2.0f * (fRec272[1] * fTemp9))) / fTemp10));
			float fTemp139 = ((((fRec272[2] + (fRec272[0] + (2.0f * fRec272[1]))) * fTemp8) / fTemp11) + (0.205712318f * (fRec17[0] * (0.0f - ((fRec272[1] * fTemp12) + ((fRec272[0] + fRec272[2]) / fTemp10))))));
			float fTemp140 = (fConst3 * fRec289[1]);
			float fTemp141 = (fConst6 * fRec292[1]);
			float fTemp142 = (fConst7 * fRec295[1]);
			float fTemp143 = (fConst9 * float(input32[i]));
			float fTemp144 = (fConst10 * fRec298[1]);
			float fTemp145 = (fConst11 * fRec301[1]);
			fRec303[0] = (fTemp143 + (fTemp144 + (fRec303[1] + fTemp145)));
			fRec301[0] = fRec303[0];
			float fRec302 = ((fTemp145 + fTemp144) + fTemp143);
			fRec300[0] = (fRec301[0] + fRec300[1]);
			fRec298[0] = fRec300[0];
			float fRec299 = fRec302;
			fRec297[0] = (fTemp141 + (fTemp142 + (fRec299 + fRec297[1])));
			fRec295[0] = fRec297[0];
			float fRec296 = (fTemp141 + (fRec299 + fTemp142));
			fRec294[0] = (fRec295[0] + fRec294[1]);
			fRec292[0] = fRec294[0];
			float fRec293 = fRec296;
			fRec291[0] = (fTemp140 + (fRec293 + fRec291[1]));
			fRec289[0] = fRec291[0];
			float fRec290 = (fRec293 + fTemp140);
			fRec288[0] = (fRec290 - (((fRec288[2] * fTemp7) + (2.0f * (fRec288[1] * fTemp9))) / fTemp10));
			float fTemp146 = ((((fRec288[2] + (fRec288[0] + (2.0f * fRec288[1]))) * fTemp8) / fTemp11) + (0.205712318f * (fRec17[0] * (0.0f - ((fRec288[1] * fTemp12) + ((fRec288[0] + fRec288[2]) / fTemp10))))));
			float fTemp147 = (fConst18 * fRec305[1]);
			float fTemp148 = (fConst19 * fRec308[1]);
			float fTemp149 = (fConst15 * float(input19[i]));
			float fTemp150 = (fConst16 * fRec311[1]);
			float fTemp151 = (fConst17 * fRec314[1]);
			fRec316[0] = (fTemp149 + (fTemp150 + (fRec316[1] + fTemp151)));
			fRec314[0] = fRec316[0];
			float fRec315 = ((fTemp151 + fTemp150) + fTemp149);
			fRec313[0] = (fRec314[0] + fRec313[1]);
			fRec311[0] = fRec313[0];
			float fRec312 = fRec315;
			fRec310[0] = (fTemp147 + (fTemp148 + (fRec312 + fRec310[1])));
			fRec308[0] = fRec310[0];
			float fRec309 = (fTemp147 + (fRec312 + fTemp148));
			fRec307[0] = (fRec308[0] + fRec307[1]);
			fRec305[0] = fRec307[0];
			float fRec306 = fRec309;
			fRec304[0] = (fRec306 - (((fRec304[2] * fTemp7) + (2.0f * (fRec304[1] * fTemp9))) / fTemp10));
			float fTemp152 = ((0.422004998f * ((0.0f - (((fRec304[0] + fRec304[2]) / fTemp10) + (fRec304[1] * fTemp12))) * fRec17[0])) + (((fRec304[2] + (fRec304[0] + (2.0f * fRec304[1]))) * fTemp8) / fTemp11));
			float fTemp153 = (fConst18 * fRec318[1]);
			float fTemp154 = (fConst19 * fRec321[1]);
			float fTemp155 = (fConst15 * float(input22[i]));
			float fTemp156 = (fConst16 * fRec324[1]);
			float fTemp157 = (fConst17 * fRec327[1]);
			fRec329[0] = (fTemp155 + (fTemp156 + (fRec329[1] + fTemp157)));
			fRec327[0] = fRec329[0];
			float fRec328 = ((fTemp157 + fTemp156) + fTemp155);
			fRec326[0] = (fRec327[0] + fRec326[1]);
			fRec324[0] = fRec326[0];
			float fRec325 = fRec328;
			fRec323[0] = (fTemp153 + (fTemp154 + (fRec325 + fRec323[1])));
			fRec321[0] = fRec323[0];
			float fRec322 = (fTemp153 + (fRec325 + fTemp154));
			fRec320[0] = (fRec321[0] + fRec320[1]);
			fRec318[0] = fRec320[0];
			float fRec319 = fRec322;
			fRec317[0] = (fRec319 - (((fRec317[2] * fTemp7) + (2.0f * (fRec317[1] * fTemp9))) / fTemp10));
			float fTemp158 = ((((fRec317[2] + (fRec317[0] + (2.0f * fRec317[1]))) * fTemp8) / fTemp11) + (0.422004998f * ((0.0f - ((fRec317[1] * fTemp12) + ((fRec317[0] + fRec317[2]) / fTemp10))) * fRec17[0])));
			float fTemp159 = (fConst18 * fRec331[1]);
			float fTemp160 = (fConst19 * fRec334[1]);
			float fTemp161 = (fConst15 * float(input24[i]));
			float fTemp162 = (fConst16 * fRec337[1]);
			float fTemp163 = (fConst17 * fRec340[1]);
			fRec342[0] = (fTemp161 + (fTemp162 + (fRec342[1] + fTemp163)));
			fRec340[0] = fRec342[0];
			float fRec341 = ((fTemp163 + fTemp162) + fTemp161);
			fRec339[0] = (fRec339[1] + fRec340[0]);
			fRec337[0] = fRec339[0];
			float fRec338 = fRec341;
			fRec336[0] = ((fTemp159 + (fRec336[1] + fTemp160)) + fRec338);
			fRec334[0] = fRec336[0];
			float fRec335 = ((fTemp160 + fTemp159) + fRec338);
			fRec333[0] = (fRec333[1] + fRec334[0]);
			fRec331[0] = fRec333[0];
			float fRec332 = fRec335;
			fRec330[0] = (fRec332 - (((fRec330[2] * fTemp7) + (2.0f * (fRec330[1] * fTemp9))) / fTemp10));
			float fTemp164 = ((((fRec330[2] + ((2.0f * fRec330[1]) + fRec330[0])) * fTemp8) / fTemp11) + (0.422004998f * ((0.0f - ((fRec330[1] * fTemp12) + ((fRec330[2] + fRec330[0]) / fTemp10))) * fRec17[0])));
			float fTemp165 = (fConst27 * float(input8[i]));
			float fTemp166 = (fConst28 * fRec344[1]);
			float fTemp167 = (fConst29 * fRec347[1]);
			fRec349[0] = (fTemp165 + (fTemp166 + (fRec349[1] + fTemp167)));
			fRec347[0] = fRec349[0];
			float fRec348 = ((fTemp167 + fTemp166) + fTemp165);
			fRec346[0] = (fRec347[0] + fRec346[1]);
			fRec344[0] = fRec346[0];
			float fRec345 = fRec348;
			fRec343[0] = (fRec345 - (((fRec343[2] * fTemp7) + (2.0f * (fRec343[1] * fTemp9))) / fTemp10));
			float fTemp168 = ((0.804249108f * ((0.0f - (((fRec343[0] + fRec343[2]) / fTemp10) + (fRec343[1] * fTemp12))) * fRec17[0])) + (((fRec343[2] + (fRec343[0] + (2.0f * fRec343[1]))) * fTemp8) / fTemp11));
			float fTemp169 = (fConst25 * fRec351[1]);
			float fTemp170 = (fConst22 * float(input11[i]));
			float fTemp171 = (fConst23 * fRec354[1]);
			float fTemp172 = (fConst24 * fRec357[1]);
			fRec359[0] = (fTemp170 + (fTemp171 + (fRec359[1] + fTemp172)));
			fRec357[0] = fRec359[0];
			float fRec358 = ((fTemp172 + fTemp171) + fTemp170);
			fRec356[0] = (fRec357[0] + fRec356[1]);
			fRec354[0] = fRec356[0];
			float fRec355 = fRec358;
			fRec353[0] = (fTemp169 + (fRec355 + fRec353[1]));
			fRec351[0] = fRec353[0];
			float fRec352 = (fRec355 + fTemp169);
			fRec350[0] = (fRec352 - (((fRec350[2] * fTemp7) + (2.0f * (fRec350[1] * fTemp9))) / fTemp10));
			float fTemp173 = ((((fRec350[2] + (fRec350[0] + (2.0f * fRec350[1]))) * fTemp8) / fTemp11) + (0.628249943f * (fRec17[0] * (0.0f - ((fRec350[1] * fTemp12) + ((fRec350[0] + fRec350[2]) / fTemp10))))));
			float fTemp174 = (fConst25 * fRec361[1]);
			float fTemp175 = (fConst22 * float(input14[i]));
			float fTemp176 = (fConst23 * fRec364[1]);
			float fTemp177 = (fConst24 * fRec367[1]);
			fRec369[0] = (fTemp175 + (fTemp176 + (fRec369[1] + fTemp177)));
			fRec367[0] = fRec369[0];
			float fRec368 = ((fTemp177 + fTemp176) + fTemp175);
			fRec366[0] = (fRec367[0] + fRec366[1]);
			fRec364[0] = fRec366[0];
			float fRec365 = fRec368;
			fRec363[0] = (fTemp174 + (fRec365 + fRec363[1]));
			fRec361[0] = fRec363[0];
			float fRec362 = (fRec365 + fTemp174);
			fRec360[0] = (fRec362 - (((fRec360[2] * fTemp7) + (2.0f * (fRec360[1] * fTemp9))) / fTemp10));
			float fTemp178 = ((((fRec360[2] + (fRec360[0] + (2.0f * fRec360[1]))) * fTemp8) / fTemp11) + (0.628249943f * (fRec17[0] * (0.0f - ((fRec360[1] * fTemp12) + ((fRec360[0] + fRec360[2]) / fTemp10))))));
			float fTemp179 = (fConst3 * fRec371[1]);
			float fTemp180 = (fConst6 * fRec374[1]);
			float fTemp181 = (fConst7 * fRec377[1]);
			float fTemp182 = (fConst9 * float(input29[i]));
			float fTemp183 = (fConst10 * fRec380[1]);
			float fTemp184 = (fConst11 * fRec383[1]);
			fRec385[0] = (fTemp182 + (fTemp183 + (fRec385[1] + fTemp184)));
			fRec383[0] = fRec385[0];
			float fRec384 = ((fTemp184 + fTemp183) + fTemp182);
			fRec382[0] = (fRec383[0] + fRec382[1]);
			fRec380[0] = fRec382[0];
			float fRec381 = fRec384;
			fRec379[0] = (fTemp180 + (fTemp181 + (fRec381 + fRec379[1])));
			fRec377[0] = fRec379[0];
			float fRec378 = (fTemp180 + (fRec381 + fTemp181));
			fRec376[0] = (fRec377[0] + fRec376[1]);
			fRec374[0] = fRec376[0];
			float fRec375 = fRec378;
			fRec373[0] = (fTemp179 + (fRec375 + fRec373[1]));
			fRec371[0] = fRec373[0];
			float fRec372 = (fRec375 + fTemp179);
			fRec370[0] = (fRec372 - (((fRec370[2] * fTemp7) + (2.0f * (fRec370[1] * fTemp9))) / fTemp10));
			float fTemp185 = ((((fRec370[2] + (fRec370[0] + (2.0f * fRec370[1]))) * fTemp8) / fTemp11) + (0.205712318f * (fRec17[0] * (0.0f - ((fRec370[1] * fTemp12) + ((fRec370[0] + fRec370[2]) / fTemp10))))));
			float fTemp186 = (fConst3 * fRec387[1]);
			float fTemp187 = (fConst9 * float(input25[i]));
			float fTemp188 = (fConst10 * fRec396[1]);
			float fTemp189 = (fConst11 * fRec399[1]);
			fRec401[0] = (fTemp187 + (fTemp188 + (fRec401[1] + fTemp189)));
			fRec399[0] = fRec401[0];
			float fRec400 = ((fTemp189 + fTemp188) + fTemp187);
			fRec398[0] = (fRec398[1] + fRec399[0]);
			fRec396[0] = fRec398[0];
			float fRec397 = fRec400;
			float fTemp190 = (fConst6 * fRec390[1]);
			float fTemp191 = (fConst7 * fRec393[1]);
			fRec395[0] = (fRec397 + (fTemp190 + (fRec395[1] + fTemp191)));
			fRec393[0] = fRec395[0];
			float fRec394 = ((fTemp191 + fTemp190) + fRec397);
			fRec392[0] = (fRec392[1] + fRec393[0]);
			fRec390[0] = fRec392[0];
			float fRec391 = fRec394;
			fRec389[0] = ((fTemp186 + fRec389[1]) + fRec391);
			fRec387[0] = fRec389[0];
			float fRec388 = (fTemp186 + fRec391);
			fRec386[0] = (fRec388 - (((fRec386[2] * fTemp7) + (2.0f * (fRec386[1] * fTemp9))) / fTemp10));
			float fTemp192 = ((((fRec386[2] + (fRec386[0] + (2.0f * fRec386[1]))) * fTemp8) / fTemp11) + (0.205712318f * (fRec17[0] * (0.0f - ((fRec386[1] * fTemp12) + ((fRec386[2] + fRec386[0]) / fTemp10))))));
			float fTemp193 = (fConst31 * float(input1[i]));
			float fTemp194 = (fConst32 * fRec403[1]);
			fRec405[0] = (fTemp193 + (fRec405[1] + fTemp194));
			fRec403[0] = fRec405[0];
			float fRec404 = (fTemp194 + fTemp193);
			fRec402[0] = (fRec404 - (((fTemp7 * fRec402[2]) + (2.0f * (fTemp9 * fRec402[1]))) / fTemp10));
			float fTemp195 = (((fTemp8 * (fRec402[2] + (fRec402[0] + (2.0f * fRec402[1])))) / fTemp11) + (0.932469487f * (fRec17[0] * (0.0f - ((fTemp12 * fRec402[1]) + ((fRec402[0] + fRec402[2]) / fTemp10))))));
			float fTemp196 = (fConst27 * float(input5[i]));
			float fTemp197 = (fConst28 * fRec407[1]);
			float fTemp198 = (fConst29 * fRec410[1]);
			fRec412[0] = (fTemp196 + (fTemp197 + (fRec412[1] + fTemp198)));
			fRec410[0] = fRec412[0];
			float fRec411 = ((fTemp198 + fTemp197) + fTemp196);
			fRec409[0] = (fRec410[0] + fRec409[1]);
			fRec407[0] = fRec409[0];
			float fRec408 = fRec411;
			fRec406[0] = (fRec408 - (((fTemp7 * fRec406[2]) + (2.0f * (fTemp9 * fRec406[1]))) / fTemp10));
			float fTemp199 = (((fTemp8 * (fRec406[2] + (fRec406[0] + (2.0f * fRec406[1])))) / fTemp11) + (0.804249108f * (fRec17[0] * (0.0f - ((fTemp12 * fRec406[1]) + ((fRec406[0] + fRec406[2]) / fTemp10))))));
			fRec413[0] = (fSlow2 + (0.999000013f * fRec413[1]));
			output0[i] = FAUSTFLOAT(((((((((((((4.52230006e-06f * fTemp13) + ((2.54430006e-06f * fTemp20) + ((0.0531107374f * fTemp27) + ((1.82253007e-05f * fTemp34) + ((6.25749999e-06f * fTemp41) + ((0.0716716722f * fTemp47) + ((1.38361002e-05f * fTemp53) + ((3.90290006e-06f * fTemp59) + ((5.28700014e-07f * fTemp65) + ((1.14039995e-06f * fTemp70) + ((8.52680023e-06f * fTemp75) + ((5.3568001e-06f * fTemp80) + ((0.0843499899f * fTemp85) + ((2.74040008e-06f * fTemp91) + (1.4835e-06f * fTemp98))))))))))))))) + (4.46969989e-06f * fTemp102)) + (1.65450001e-06f * fTemp105)) + (4.64979985e-06f * fTemp110)) + (0.0473226868f * fTemp111)) + (0.0769467279f * fTemp114)) + (2.45000001e-06f * fTemp117)) + (3.70409998e-06f * fTemp121)) + (0.0872831121f * fTemp125)) - (((((3.1319999e-07f * fTemp132) + ((6.32769979e-06f * fTemp139) + ((5.95040001e-06f * fTemp146) + (((((((5.69839995e-06f * fTemp152) + (5.01880004e-06f * fTemp158)) + (2.32110006e-06f * fTemp164)) + (1.50289998e-06f * fTemp168)) + (6.22569996e-06f * fTemp173)) + (3.31060005e-06f * fTemp178)) + (3.56660007e-06f * fTemp185))))) + (2.08499998e-07f * fTemp192)) + (2.71919998e-06f * fTemp195)) + (5.04480022e-06f * fTemp199))) * fRec413[0]));
			fVec0[0] = (((0.0338490047f * fTemp125) + ((0.0510204248f * fTemp199) + ((0.011978806f * fTemp121) + ((0.0108339777f * fTemp117) + ((0.0463922769f * fTemp114) + ((0.0301853716f * fTemp195) + ((0.0341214538f * fTemp111) + ((0.010749503f * fTemp110) + ((0.0176964421f * fTemp102) + ((0.000998056959f * fTemp132) + ((0.00174845045f * fTemp20) + ((0.0154429218f * fTemp185) + (((0.0264521018f * fTemp53) + ((0.0223340727f * fTemp75) + ((0.0175191797f * fTemp80) + ((0.00963862892f * fTemp85) + ((0.0541275442f * fTemp173) + (0.0390786566f * fTemp152)))))) + (0.0216981731f * fTemp34)))))))))))))) - ((0.000595821824f * fTemp192) + ((0.003537582f * fTemp105) + ((0.000240352296f * fTemp139) + ((0.0217951722f * fTemp13) + ((0.0312371645f * fTemp146) + ((0.0217270087f * fTemp27) + ((0.01154532f * fTemp41) + ((0.0120762754f * fTemp47) + ((0.00779462699f * fTemp59) + ((0.00370370969f * fTemp65) + ((0.00808212627f * fTemp70) + ((0.026083149f * fTemp178) + ((0.0133990385f * fTemp168) + (((0.000207974197f * fTemp164) + ((0.0332537405f * fTemp158) + (0.0165106263f * fTemp91))) + (0.00805463362f * fTemp98))))))))))))))));
			output1[i] = FAUSTFLOAT((0.998005569f * (fVec0[iConst33] * fRec413[0])));
			fVec1[0] = (((0.0352820829f * fTemp125) + ((0.0426589735f * fTemp199) + ((0.0510143228f * fTemp114) + ((0.0257401336f * fTemp195) + ((0.038172856f * fTemp111) + ((0.0004702312f * fTemp192) + ((0.00574632082f * fTemp105) + ((0.0164162293f * fTemp13) + ((5.56380019e-06f * fTemp146) + ((0.00804641936f * fTemp185) + ((0.016406158f * fTemp41) + ((0.0119946115f * fTemp59) + ((6.08019991e-06f * fTemp65) + ((0.00575517025f * fTemp70) + ((0.0069673867f * fTemp85) + ((0.0435375981f * fTemp173) + (((0.028853517f * fTemp152) + (0.0120080616f * fTemp91)) + (1.02270997e-05f * fTemp98)))))))))))))))))) - ((0.0195993222f * fTemp121) + ((0.0257381666f * fTemp117) + ((0.0288632512f * fTemp110) + ((0.042658627f * fTemp102) + ((0.000477006601f * fTemp132) + ((0.00422280096f * fTemp139) + ((0.00805632677f * fTemp20) + ((0.0252503026f * fTemp27) + (((0.0168237407f * fTemp47) + ((0.045309592f * fTemp53) + ((0.0371481292f * fTemp75) + ((5.31510022e-06f * fTemp178) + ((0.0435424335f * fTemp80) + ((4.50529978e-06f * fTemp168) + ((1.01930004e-06f * fTemp158) + (0.0018724103f * fTemp164)))))))) + (0.039396286f * fTemp34)))))))))));
			output2[i] = FAUSTFLOAT((0.997783959f * (fVec1[iConst34] * fRec413[0])));
			fVec2[0] = (((0.0338515826f * fTemp125) + ((0.0119755501f * fTemp121) + ((0.0463907383f * fTemp114) + ((0.0341186747f * fTemp111) + ((0.000597332488f * fTemp132) + ((0.0312463343f * fTemp146) + ((0.0216912702f * fTemp34) + ((0.026444735f * fTemp53) + ((0.0036919862f * fTemp65) + ((0.0223280527f * fTemp75) + ((0.0260652341f * fTemp178) + ((0.00964230858f * fTemp85) + ((0.0133855147f * fTemp168) + ((0.0332453474f * fTemp158) + (0.00803524163f * fTemp98))))))))))))))) - ((0.017694287f * fTemp199) + ((0.0301783718f * fTemp117) + ((0.0108332634f * fTemp195) + ((0.000986961881f * fTemp192) + ((0.039093677f * fTemp110) + ((0.00807411969f * fTemp105) + ((0.0510147251f * fTemp102) + ((0.000258992513f * fTemp139) + ((0.0115242945f * fTemp13) + ((0.015458216f * fTemp20) + ((0.0217389856f * fTemp27) + ((0.00174763799f * fTemp185) + ((0.0217839554f * fTemp41) + ((0.0120782666f * fTemp47) + ((0.016497789f * fTemp59) + ((0.00352248852f * fTemp70) + ((0.0541319922f * fTemp80) + ((0.0175154228f * fTemp173) + ((0.000217761306f * fTemp164) + ((0.010745679f * fTemp152) + (0.00777012017f * fTemp91))))))))))))))))))))));
			output3[i] = FAUSTFLOAT((0.998005569f * (fVec2[iConst33] * fRec413[0])));
			fVec3[0] = (((0.0324303731f * fTemp125) + ((0.00778828422f * fTemp121) + ((0.0417693704f * fTemp114) + ((0.0300636496f * fTemp111) + ((0.00585178612f * fTemp105) + ((0.00472355168f * fTemp139) + ((0.0169155933f * fTemp13) + ((0.0182135906f * fTemp34) + ((0.016896585f * fTemp41) + ((0.0193701088f * fTemp53) + ((0.0122731943f * fTemp59) + ((0.00586031703f * fTemp70) + ((0.015175622f * fTemp75) + ((0.0123329759f * fTemp85) + ((0.0122893974f * fTemp91) + (0.00215694727f * fTemp164)))))))))))))))) - ((0.0457643978f * fTemp199) + ((0.0071219611f * fTemp117) + ((0.0265663434f * fTemp195) + ((0.000587312097f * fTemp192) + ((0.0104244938f * fTemp110) + ((0.012267625f * fTemp102) + ((0.00220757467f * fTemp132) + ((0.031535048f * fTemp146) + ((0.00501411827f * fTemp20) + ((0.0182146393f * fTemp27) + ((0.0187291298f * fTemp185) + ((0.00731574697f * fTemp47) + ((0.00374657242f * fTemp65) + ((0.0262655504f * fTemp178) + ((0.0134790875f * fTemp80) + ((0.0502912141f * fTemp173) + ((0.0134784635f * fTemp168) + (((0.0389057137f * fTemp152) + (0.0335300677f * fTemp158)) + (0.00820294023f * fTemp98))))))))))))))))))));
			output4[i] = FAUSTFLOAT((0.998005569f * (fVec3[iConst33] * fRec413[0])));
			fVec4[0] = (((0.0352924652f * fTemp125) + ((0.0257430729f * fTemp117) + ((0.0510256663f * fTemp114) + ((0.0381806493f * fTemp111) + ((0.0288615432f * fTemp110) + ((0.0426651761f * fTemp102) + ((0.000482511212f * fTemp132) + ((2.14379997e-06f * fTemp146) + ((0.00805105548f * fTemp20) + ((6.30920022e-06f * fTemp65) + ((1.47399999e-06f * fTemp178) + ((0.0435460955f * fTemp80) + ((0.00697506964f * fTemp85) + ((4.58500011e-07f * fTemp168) + ((2.5027e-06f * fTemp158) + (1.11559002e-05f * fTemp98)))))))))))))))) - ((0.0426623151f * fTemp199) + ((0.019598661f * fTemp121) + ((0.0257410146f * fTemp195) + ((0.000472368614f * fTemp192) + ((0.00574612245f * fTemp105) + ((0.00421383139f * fTemp139) + ((0.0164106749f * fTemp13) + ((0.0252451636f * fTemp27) + ((0.00805942249f * fTemp185) + ((0.0393948145f * fTemp34) + ((0.0164061505f * fTemp41) + ((0.0168180577f * fTemp47) + ((0.0453090109f * fTemp53) + ((0.0119943367f * fTemp59) + ((0.00575058721f * fTemp70) + ((0.037147373f * fTemp75) + ((0.0435448177f * fTemp173) + ((0.00186739466f * fTemp164) + ((0.0288644452f * fTemp152) + (0.0120009482f * fTemp91)))))))))))))))))))));
			output5[i] = FAUSTFLOAT((0.997783959f * (fVec4[iConst34] * fRec413[0])));
			fVec5[0] = (((0.0324229002f * fTemp125) + ((0.0122708166f * fTemp199) + ((0.00778933102f * fTemp121) + ((0.0265659001f * fTemp117) + ((0.0417632945f * fTemp114) + ((0.00712388381f * fTemp195) + ((0.0300600771f * fTemp111) + ((0.00220045797f * fTemp192) + ((0.0389056094f * fTemp110) + ((0.00585804973f * fTemp105) + ((0.0457637608f * fTemp102) + ((0.000587024202f * fTemp132) + ((0.00472197169f * fTemp139) + ((0.0168983955f * fTemp13) + ((0.0315406211f * fTemp146) + ((0.0187293608f * fTemp20) + ((0.00501084421f * fTemp185) + ((0.0182192959f * fTemp34) + ((0.0169155393f * fTemp41) + ((0.0193752591f * fTemp53) + ((0.0122866696f * fTemp59) + ((0.00374108041f * fTemp65) + ((0.00585071836f * fTemp70) + ((0.0151786013f * fTemp75) + ((0.0262656976f * fTemp178) + ((0.0502907373f * fTemp80) + ((0.0123249413f * fTemp85) + ((0.0134819802f * fTemp173) + ((0.0134779755f * fTemp168) + (((0.00215586647f * fTemp164) + ((0.0122723347f * fTemp91) + ((0.0104250107f * fTemp152) + (0.0335324742f * fTemp158)))) + (0.00819314457f * fTemp98))))))))))))))))))))))))))))))) - ((0.00732353423f * fTemp47) + (0.0182215255f * fTemp27)));
			output6[i] = FAUSTFLOAT((0.998005569f * (fRec413[0] * fVec5[iConst33])));
			fVec6[(IOTA & 31)] = ((((((((0.0285131726f * fTemp139) + ((0.00546691241f * fTemp13) + ((((((((0.00414358731f * fTemp98) + ((0.00446477858f * fTemp91) + (0.0215720031f * fTemp164))) + (0.00792338047f * fTemp173)) + (0.00227179355f * fTemp70)) + (0.00192661583f * fTemp65)) + (0.00213664793f * fTemp27)) + (0.00117794785f * fTemp20)) + (0.00959271751f * fTemp146)))) + (0.0173593424f * fTemp192)) + (0.0235311687f * fTemp111)) + (0.0338993929f * fTemp195)) + (0.0200215131f * fTemp114)) + (0.0357975774f * fTemp199)) - ((((((((0.0014047022f * fTemp132) + ((((0.0188453943f * fTemp41) + ((0.0151142906f * fTemp47) + ((0.00398819847f * fTemp53) + ((0.0334232487f * fTemp59) + ((0.00384632917f * fTemp75) + ((0.0365134403f * fTemp178) + ((0.00170740404f * fTemp80) + ((0.0215631779f * fTemp85) + ((0.0306473803f * fTemp168) + ((0.019657487f * fTemp152) + (0.0161167923f * fTemp158))))))))))) + (0.00225917948f * fTemp34)) + (0.0227040742f * fTemp185))) + (0.00217522983f * fTemp102)) + (0.0261735525f * fTemp105)) + (0.00029127521f * fTemp110)) + (0.00145658397f * fTemp117)) + (0.00219735736f * fTemp121)) + (0.00514168013f * fTemp125)));
			output7[i] = FAUSTFLOAT((0.994459808f * (fVec6[((IOTA - iConst35) & 31)] * fRec413[0])));
			fVec7[(IOTA & 31)] = (((0.0297248401f * fTemp199) + ((0.0179317892f * fTemp114) + ((0.0410687849f * fTemp195) + ((0.0306961462f * fTemp111) + ((0.0170072708f * fTemp110) + ((0.00086466549f * fTemp105) + ((0.00437316718f * fTemp13) + ((0.0135330204f * fTemp146) + ((0.00534791779f * fTemp20) + ((0.0174028352f * fTemp27) + ((0.0227951612f * fTemp34) + ((0.00172672782f * fTemp41) + ((0.00235598302f * fTemp53) + ((0.00159388839f * fTemp59) + ((0.0265795738f * fTemp65) + ((0.0393768735f * fTemp70) + ((0.00663555553f * fTemp80) + (((0.00257663708f * fTemp158) + (0.0365884043f * fTemp91)) + (0.0266535003f * fTemp98))))))))))))))))))) - ((0.0192471165f * fTemp125) + ((0.0396838821f * fTemp121) + ((0.0242131073f * fTemp117) + ((0.0195742436f * fTemp192) + ((0.0178549215f * fTemp102) + ((0.0104047451f * fTemp132) + ((0.0174311772f * fTemp139) + (((0.00112161727f * fTemp47) + ((0.0334640145f * fTemp75) + ((0.0179478563f * fTemp178) + ((0.0266766511f * fTemp85) + ((0.0122424858f * fTemp173) + ((0.0220683031f * fTemp168) + ((0.0292676389f * fTemp152) + (0.0163461138f * fTemp164)))))))) + (0.00833275821f * fTemp185))))))))));
			output8[i] = FAUSTFLOAT((0.994903028f * (fRec413[0] * fVec7[((IOTA - iConst36) & 31)])));
			fVec8[(IOTA & 31)] = (((0.0144822551f * fTemp199) + ((0.0157211255f * fTemp114) + ((0.0165882278f * fTemp195) + ((0.0242891777f * fTemp111) + ((0.0183997601f * fTemp192) + ((0.0244781263f * fTemp110) + ((0.0313078016f * fTemp105) + ((0.0119747641f * fTemp132) + ((0.00529950997f * fTemp13) + ((0.0109387366f * fTemp20) + ((0.0123654082f * fTemp27) + ((0.0155919129f * fTemp34) + (((0.0321489461f * fTemp59) + ((0.0181465168f * fTemp178) + ((0.0222139508f * fTemp168) + (0.00707933772f * fTemp80)))) + (0.00814937521f * fTemp41)))))))))))))) - ((0.0132820914f * fTemp125) + ((0.0282651987f * fTemp121) + ((0.0335474797f * fTemp117) + ((0.0264675654f * fTemp102) + ((0.0103184702f * fTemp139) + ((0.0137239741f * fTemp146) + ((0.00785059109f * fTemp185) + ((0.00482360367f * fTemp47) + ((0.00389228691f * fTemp53) + ((0.026830906f * fTemp65) + ((0.00749638118f * fTemp70) + ((0.02764659f * fTemp75) + ((0.0223583821f * fTemp85) + ((0.00146551407f * fTemp173) + (((0.00488647772f * fTemp164) + ((0.00331561361f * fTemp91) + ((0.0120740347f * fTemp152) + (0.00253144396f * fTemp158)))) + (0.0271207727f * fTemp98)))))))))))))))));
			output9[i] = FAUSTFLOAT((0.994903028f * (fRec413[0] * fVec8[((IOTA - iConst36) & 31)])));
			fVec9[(IOTA & 31)] = (((0.00217607711f * fTemp199) + ((0.0200138334f * fTemp114) + ((0.00145928503f * fTemp195) + ((0.0235146843f * fTemp111) + ((0.00140652945f * fTemp192) + ((0.0196390953f * fTemp110) + ((0.0022798446f * fTemp105) + ((0.0285128579f * fTemp139) + ((0.0227165874f * fTemp20) + ((0.00212069228f * fTemp27) + ((0.00546672894f * fTemp41) + ((0.00447567692f * fTemp59) + ((0.0365036465f * fTemp178) + ((0.00170157896f * fTemp173) + ((0.0306251757f * fTemp168) + (((0.000279992702f * fTemp152) + (0.0161364377f * fTemp158)) + (0.021559542f * fTemp164))))))))))))))))) - ((0.00512850937f * fTemp125) + ((0.00220371434f * fTemp121) + ((0.0338744335f * fTemp117) + ((0.0357850008f * fTemp102) + ((0.0173518918f * fTemp132) + ((0.0188677032f * fTemp13) + ((0.00957015716f * fTemp146) + ((0.00118609506f * fTemp185) + ((0.00223922147f * fTemp34) + ((0.0151245277f * fTemp47) + ((0.00398261799f * fTemp53) + ((0.00193307409f * fTemp65) + ((0.0261559151f * fTemp70) + ((0.00385212409f * fTemp75) + ((0.00794078596f * fTemp80) + ((0.0215517879f * fTemp85) + ((0.0334180146f * fTemp91) + (0.00415598322f * fTemp98)))))))))))))))))));
			output10[i] = FAUSTFLOAT((0.994459808f * (fRec413[0] * fVec9[((IOTA - iConst35) & 31)])));
			fVec10[(IOTA & 31)] = (((((((0.011208104f * fTemp132) + ((0.00184356305f * fTemp13) + ((0.011404559f * fTemp20) + ((0.012376925f * fTemp27) + ((0.0070381891f * fTemp185) + (((((((((0.0288711097f * fTemp98) + (0.0119211152f * fTemp152)) + (0.0234601125f * fTemp168)) + (0.00212324364f * fTemp173)) + (0.00671209022f * fTemp80)) + (0.0201868787f * fTemp178)) + (0.0264661219f * fTemp75)) + (0.0276889466f * fTemp65)) + (0.00282653188f * fTemp53))))))) + (0.024579538f * fTemp110)) + (0.0242880136f * fTemp111)) + (0.0157179944f * fTemp114)) + (0.0275476705f * fTemp121)) - (((((((((0.00728266919f * fTemp139) + ((0.0131626977f * fTemp146) + ((0.015932085f * fTemp34) + ((0.00813732855f * fTemp41) + ((0.00481808931f * fTemp47) + ((0.0321472399f * fTemp59) + ((0.00919907447f * fTemp70) + ((0.0223621186f * fTemp85) + ((0.00340778288f * fTemp164) + ((0.000708782929f * fTemp158) + (0.00647454942f * fTemp91))))))))))) + (0.0270602293f * fTemp102)) + (0.0313106589f * fTemp105)) + (0.0197360385f * fTemp192)) + (0.0158460233f * fTemp195)) + (0.0339761786f * fTemp117)) + (0.0134490049f * fTemp199)) + (0.0132877538f * fTemp125)));
			output11[i] = FAUSTFLOAT((0.994903028f * (fVec10[((IOTA - iConst36) & 31)] * fRec413[0])));
			fVec11[(IOTA & 31)] = (((0.0446210802f * fTemp121) + ((0.0244163014f * fTemp114) + ((0.0363172293f * fTemp111) + ((0.020203568f * fTemp192) + ((0.0161856432f * fTemp110) + ((0.0112802722f * fTemp13) + ((0.012892969f * fTemp146) + ((0.00886349287f * fTemp20) + ((0.0122114895f * fTemp27) + (((0.0065252129f * fTemp53) + ((0.0422834121f * fTemp70) + ((0.0420676731f * fTemp75) + ((0.00162406347f * fTemp80) + ((0.00622504856f * fTemp173) + ((0.0422692448f * fTemp91) + ((0.0286090747f * fTemp152) + (0.000839976419f * fTemp158)))))))) + (0.0129822018f * fTemp185))))))))))) - ((0.0170691777f * fTemp125) + ((0.0376785174f * fTemp199) + ((0.0285261497f * fTemp117) + ((0.0464762598f * fTemp195) + ((0.00229224609f * fTemp105) + ((0.0242723655f * fTemp102) + ((0.0100245094f * fTemp132) + ((0.0209935866f * fTemp139) + ((0.0178152882f * fTemp34) + ((0.00547089428f * fTemp41) + ((0.0076942537f * fTemp47) + ((0.00447662175f * fTemp59) + ((0.0272365008f * fTemp65) + ((0.0198335573f * fTemp178) + ((0.03017544f * fTemp85) + ((0.0232048221f * fTemp168) + ((0.0179721937f * fTemp164) + (0.0280587375f * fTemp98)))))))))))))))))));
			output12[i] = FAUSTFLOAT((0.994903028f * (fRec413[0] * fVec11[((IOTA - iConst36) & 31)])));
			fVec12[(IOTA & 31)] = (((0.00145378441f * fTemp117) + ((0.0200154874f * fTemp114) + ((0.023518106f * fTemp111) + ((0.000286152906f * fTemp110) + ((0.0261591207f * fTemp105) + ((0.00217205891f * fTemp102) + ((0.00139083737f * fTemp132) + ((0.0285091251f * fTemp139) + ((0.00956483372f * fTemp146) + ((0.00211883266f * fTemp27) + ((0.0227058958f * fTemp185) + ((0.0188623257f * fTemp41) + ((0.0334158689f * fTemp59) + ((0.00191671751f * fTemp65) + ((0.0017043032f * fTemp80) + (((0.0196337923f * fTemp152) + (0.0215617754f * fTemp164)) + (0.00413765432f * fTemp98))))))))))))))))) - ((0.0051292195f * fTemp125) + ((0.0357862934f * fTemp199) + ((0.0021929564f * fTemp121) + ((0.033879105f * fTemp195) + ((0.0173531547f * fTemp192) + ((0.00546936458f * fTemp13) + ((0.00118461554f * fTemp20) + (((0.0151201384f * fTemp47) + ((0.00398613233f * fTemp53) + ((0.00226526242f * fTemp70) + ((0.00384289213f * fTemp75) + ((0.0365033634f * fTemp178) + ((0.0215494651f * fTemp85) + ((0.00793956313f * fTemp173) + ((0.0306292567f * fTemp168) + ((0.0161336474f * fTemp158) + (0.00446137739f * fTemp91)))))))))) + (0.00225188211f * fTemp34))))))))));
			output13[i] = FAUSTFLOAT((0.994459808f * (fRec413[0] * fVec12[((IOTA - iConst35) & 31)])));
			fVec13[(IOTA & 31)] = (((0.0242112409f * fTemp117) + ((0.0179308224f * fTemp114) + ((0.0307002682f * fTemp111) + ((0.0195616595f * fTemp192) + ((0.017853152f * fTemp102) + ((0.010410063f * fTemp132) + ((0.013537406f * fTemp146) + ((0.0174069628f * fTemp27) + ((0.00832489599f * fTemp185) + ((0.0227923412f * fTemp34) + ((0.00235756068f * fTemp53) + ((0.0265826583f * fTemp65) + ((0.0122532919f * fTemp173) + (((0.0292704999f * fTemp152) + (0.00259112311f * fTemp158)) + (0.0266404133f * fTemp98))))))))))))))) - ((0.019253334f * fTemp125) + ((0.0297232959f * fTemp199) + ((0.0396812446f * fTemp121) + ((0.0410771593f * fTemp195) + ((0.0170068163f * fTemp110) + ((0.000849328004f * fTemp105) + ((0.0174153913f * fTemp139) + ((0.00436354242f * fTemp13) + ((0.00535270339f * fTemp20) + ((0.00174093153f * fTemp41) + ((0.0011163837f * fTemp47) + ((0.00158926437f * fTemp59) + ((0.0393761285f * fTemp70) + ((0.0334579162f * fTemp75) + ((0.017946979f * fTemp178) + ((0.00663466984f * fTemp80) + ((0.026677642f * fTemp85) + ((0.0220802464f * fTemp168) + ((0.0365764908f * fTemp91) + (0.0163302906f * fTemp164)))))))))))))))))))));
			output14[i] = FAUSTFLOAT((0.994903028f * (fRec413[0] * fVec13[((IOTA - iConst36) & 31)])));
			fVec14[(IOTA & 31)] = (((0.0335719511f * fTemp117) + ((0.015737446f * fTemp114) + ((0.0243119132f * fTemp111) + ((0.026488414f * fTemp102) + ((0.0123618348f * fTemp27) + ((0.00785494596f * fTemp185) + (((0.00747441268f * fTemp70) + ((0.018147653f * fTemp178) + ((0.00146507018f * fTemp173) + ((0.0222150907f * fTemp168) + ((0.0120821306f * fTemp152) + (0.00329523883f * fTemp91)))))) + (0.0155934114f * fTemp34)))))))) - ((0.0132887159f * fTemp125) + ((0.014501893f * fTemp199) + ((0.0282988027f * fTemp121) + ((0.0166117605f * fTemp195) + ((0.0183816366f * fTemp192) + ((0.0244832207f * fTemp110) + ((0.0313324779f * fTemp105) + ((0.0120006716f * fTemp132) + ((0.0103475647f * fTemp139) + ((0.00530138332f * fTemp13) + ((0.0137180863f * fTemp146) + ((0.0109438049f * fTemp20) + ((0.00815873407f * fTemp41) + ((0.0048309518f * fTemp47) + ((0.0039000127f * fTemp53) + ((0.0321706124f * fTemp59) + ((0.0268342551f * fTemp65) + ((0.0276765991f * fTemp75) + ((0.00707615074f * fTemp80) + ((0.0223725922f * fTemp85) + ((0.027121352f * fTemp98) + ((0.00252732239f * fTemp158) + (0.00491875922f * fTemp164))))))))))))))))))))))));
			output15[i] = FAUSTFLOAT((0.994903028f * (fRec413[0] * fVec14[((IOTA - iConst36) & 31)])));
			fVec15[(IOTA & 31)] = (((0.0338919684f * fTemp117) + ((0.0200174712f * fTemp114) + ((0.0235254578f * fTemp111) + ((0.0357914977f * fTemp102) + ((0.0173627231f * fTemp132) + ((0.0285231974f * fTemp139) + ((0.0188536644f * fTemp13) + ((0.00213367119f * fTemp27) + (((0.0261732973f * fTemp70) + ((0.036511451f * fTemp178) + ((0.00792536978f * fTemp80) + ((0.0306433197f * fTemp168) + ((0.0215745289f * fTemp164) + ((0.0161205065f * fTemp158) + (0.0334273726f * fTemp91))))))) + (0.00118738925f * fTemp185)))))))))) - ((0.00513874181f * fTemp125) + ((0.0021808676f * fTemp199) + ((0.00221101847f * fTemp121) + ((0.00146376574f * fTemp195) + ((0.00141605898f * fTemp192) + ((0.0196501091f * fTemp110) + ((0.00228837901f * fTemp105) + ((0.00958617963f * fTemp146) + ((0.0226991177f * fTemp20) + ((0.00224306225f * fTemp34) + ((0.00547177717f * fTemp41) + ((0.0151117956f * fTemp47) + ((0.00398789532f * fTemp53) + ((0.00448323507f * fTemp59) + ((0.00194202363f * fTemp65) + ((0.0038598862f * fTemp75) + ((0.0215570088f * fTemp85) + ((0.00170406105f * fTemp173) + ((0.000280491891f * fTemp152) + (0.00416057417f * fTemp98)))))))))))))))))))));
			output16[i] = FAUSTFLOAT((0.994459808f * (fRec413[0] * fVec15[((IOTA - iConst35) & 31)])));
			fVec16[(IOTA & 31)] = (((((((((0.0100212144f * fTemp192) + ((((0.0122074094f * fTemp27) + ((0.0112793911f * fTemp41) + ((0.00651655998f * fTemp53) + ((0.0422659703f * fTemp59) + ((0.0272349734f * fTemp65) + (((0.0198304765f * fTemp178) + ((0.0280524567f * fTemp98) + (0.0232067332f * fTemp168))) + (0.0420628637f * fTemp75))))))) + (0.0376720801f * fTemp102)) + (0.0422858559f * fTemp105))) + (0.0363168567f * fTemp111)) + (0.0285257287f * fTemp195)) + (0.0244102012f * fTemp114)) + (0.0464785807f * fTemp117)) + (0.0446238518f * fTemp121)) + (0.0242657457f * fTemp199)) - (((0.0285965391f * fTemp110) + ((0.0202007722f * fTemp132) + ((0.0209988132f * fTemp139) + ((0.00546630099f * fTemp13) + ((0.0128700649f * fTemp146) + ((0.0129682645f * fTemp20) + ((0.00885935035f * fTemp185) + (((0.00768448226f * fTemp47) + ((0.0022937879f * fTemp70) + ((0.0062296018f * fTemp80) + ((0.0301687401f * fTemp85) + ((0.00163434329f * fTemp173) + ((0.0179738328f * fTemp164) + ((0.00448196707f * fTemp91) + ((0.0161899403f * fTemp152) + (0.000834875915f * fTemp158))))))))) + (0.0178153962f * fTemp34))))))))) + (0.0170746874f * fTemp125)));
			output17[i] = FAUSTFLOAT((0.994903028f * (fRec413[0] * fVec16[((IOTA - iConst36) & 31)])));
			fVec17[(IOTA & 31)] = (((0.0270859804f * fTemp199) + ((0.0275524594f * fTemp121) + ((0.015851954f * fTemp117) + ((0.0157361906f * fTemp114) + ((0.0339871384f * fTemp195) + ((0.0242996197f * fTemp111) + ((0.013463459f * fTemp102) + ((0.0197406523f * fTemp132) + ((0.0131553551f * fTemp146) + ((0.0123677095f * fTemp27) + ((0.00184490927f * fTemp41) + ((0.0028538194f * fTemp53) + ((0.0264879279f * fTemp75) + (0.000692206377f * fTemp158)))))))))))))) - ((0.0132781463f * fTemp125) + ((0.0111942757f * fTemp192) + ((0.0119247204f * fTemp110) + ((0.00920206681f * fTemp105) + ((0.00728431996f * fTemp139) + ((0.00816502608f * fTemp13) + ((0.00705549028f * fTemp20) + ((0.0114308484f * fTemp185) + ((0.0159261543f * fTemp34) + ((0.00484369416f * fTemp47) + ((0.00647498108f * fTemp59) + ((0.0276871976f * fTemp65) + ((0.0313103572f * fTemp70) + ((0.020199718f * fTemp178) + ((0.00211028522f * fTemp80) + ((0.0223753918f * fTemp85) + ((0.00668762578f * fTemp173) + ((0.0234640837f * fTemp168) + (((0.00339932903f * fTemp164) + ((0.0245817527f * fTemp152) + (0.032163851f * fTemp91))) + (0.0288776979f * fTemp98)))))))))))))))))))));
			output18[i] = FAUSTFLOAT((0.994903028f * (fRec413[0] * fVec17[((IOTA - iConst36) & 31)])));
			fVec18[(IOTA & 63)] = (((0.0887992382f * fTemp195) + ((0.0588501506f * fTemp111) + ((0.0457598157f * fTemp192) + ((7.65539971e-06f * fTemp110) + ((4.19029993e-06f * fTemp102) + ((6.24550012e-06f * fTemp13) + ((0.00535080861f * fTemp146) + ((0.00354070589f * fTemp27) + ((0.0124843847f * fTemp185) + ((1.2601e-05f * fTemp34) + ((0.00445492007f * fTemp41) + ((0.0106905513f * fTemp47) + ((0.0287602693f * fTemp59) + ((1.49483003e-05f * fTemp65) + ((1.78635e-05f * fTemp70) + ((4.11829978e-06f * fTemp75) + ((0.0332020447f * fTemp178) + ((0.0116322571f * fTemp85) + (((0.00293797976f * fTemp152) + (0.0121668335f * fTemp158)) + (0.0607517771f * fTemp164)))))))))))))))))))) - ((0.0305124186f * fTemp125) + ((0.0354953706f * fTemp199) + ((1.63954992e-05f * fTemp121) + ((1.02769e-05f * fTemp117) + ((0.0229201801f * fTemp114) + ((0.0742083937f * fTemp105) + ((8.72629971e-06f * fTemp132) + ((0.0236099698f * fTemp139) + ((3.33880007e-06f * fTemp20) + ((6.36850018e-06f * fTemp53) + ((3.69029999e-06f * fTemp80) + ((0.023990212f * fTemp173) + ((0.0842990875f * fTemp168) + ((2.51770007e-06f * fTemp91) + (1.52550001e-06f * fTemp98))))))))))))))));
			output19[i] = FAUSTFLOAT((0.990027726f * (fRec413[0] * fVec18[((IOTA - iConst37) & 63)])));
			fVec19[(IOTA & 63)] = (((((((((4.96599978e-06f * fTemp41) + ((0.00483221514f * fTemp47) + ((0.0030837385f * fTemp53) + ((0.0445791222f * fTemp65) + ((0.0603009164f * fTemp70) + ((0.0370234251f * fTemp75) + ((0.0213693716f * fTemp178) + ((0.00611096714f * fTemp80) + ((0.0198027026f * fTemp85) + ((0.0133260442f * fTemp152) + (0.00178489066f * fTemp158))))))))))) + (0.00657013385f * fTemp185)) + (0.000866458693f * fTemp13)) + (0.0152068175f * fTemp139)) + (0.022038091f * fTemp102)) + (0.0475860611f * fTemp111)) + (0.061254438f * fTemp195)) - (((((((((((((((((((0.0376278572f * fTemp91) + (0.0257193707f * fTemp164)) + (0.0263141189f * fTemp98)) + (0.0336138234f * fTemp168)) + (0.0105896434f * fTemp173)) + (9.05189972e-06f * fTemp59)) + (0.00540810451f * fTemp34)) + (0.00572323892f * fTemp27)) + (0.00379112712f * fTemp20)) + (0.00311136642f * fTemp146)) + (0.0208957531f * fTemp132)) + (1.24281996e-05f * fTemp105)) + (0.0076983762f * fTemp110)) + (0.0361554585f * fTemp192)) + (0.0270938333f * fTemp114)) + (0.0353607573f * fTemp117)) + (0.0582042076f * fTemp121)) + (0.0381666757f * fTemp199)) + (0.0203053225f * fTemp125)));
			output20[i] = FAUSTFLOAT((0.990249336f * (fVec19[((IOTA - iConst38) & 63)] * fRec413[0])));
			fVec20[(IOTA & 63)] = (((((0.0137594929f * fTemp192) + ((((0.0425689816f * fTemp132) + ((((((0.00777581753f * fTemp47) + ((0.00816389639f * fTemp53) + ((((((((0.00312986341f * fTemp152) + (0.00435806718f * fTemp91)) + (0.0262395646f * fTemp98)) + (0.0334427133f * fTemp168)) + (0.0156995766f * fTemp85)) + (0.0148546649f * fTemp80)) + (0.0315297917f * fTemp75)) + (0.00735984743f * fTemp70)))) + (0.00130308536f * fTemp34)) + (0.00569017557f * fTemp185)) + (0.00309976772f * fTemp146)) + (0.00854121614f * fTemp139))) + (0.0350273214f * fTemp102)) + (0.0672760159f * fTemp105))) + (0.0532584041f * fTemp111)) + (0.042079974f * fTemp195)) - (((((((0.00874696858f * fTemp110) + ((((((0.0331903771f * fTemp59) + ((((0.0104994113f * fTemp173) + ((0.00159050908f * fTemp158) + (0.0356963724f * fTemp164))) + (0.0213414878f * fTemp178)) + (0.0441824496f * fTemp65))) + (0.00179365021f * fTemp41)) + (0.00108762819f * fTemp27)) + (0.00831346959f * fTemp20)) + (0.00309742033f * fTemp13))) + (0.0250215139f * fTemp114)) + (0.067867741f * fTemp117)) + (0.0682127848f * fTemp121)) + (0.0189149324f * fTemp199)) + (0.0254155602f * fTemp125)));
			output21[i] = FAUSTFLOAT((0.990249336f * (fVec20[((IOTA - iConst38) & 63)] * fRec413[0])));
			fVec21[(IOTA & 63)] = (((6.81699987e-07f * fTemp195) + ((0.0588359646f * fTemp111) + ((1.76210006e-06f * fTemp105) + ((0.0354968123f * fTemp102) + ((0.00446532993f * fTemp13) + ((0.00355171692f * fTemp27) + ((0.0106876018f * fTemp47) + ((1.73980004e-06f * fTemp53) + ((1.71399995e-06f * fTemp75) + ((0.0239910036f * fTemp80) + ((0.0116386283f * fTemp85) + ((0.0842888653f * fTemp168) + (((0.0607584752f * fTemp164) + ((2.1492001e-06f * fTemp152) + (0.0287690926f * fTemp91))) + (3.41529994e-06f * fTemp98)))))))))))))) - ((0.0305128377f * fTemp125) + ((3.29199992e-07f * fTemp199) + ((1.60189995e-06f * fTemp121) + ((0.0887830555f * fTemp117) + ((0.0229164064f * fTemp114) + ((6.79100026e-07f * fTemp192) + ((0.0029384573f * fTemp110) + ((0.0457731485f * fTemp132) + ((0.0236222036f * fTemp139) + ((0.00535361329f * fTemp146) + ((0.0124895824f * fTemp20) + ((1.75000002e-06f * fTemp185) + ((1.96220003e-06f * fTemp34) + ((1.18850005e-06f * fTemp41) + ((2.81260009e-06f * fTemp59) + ((9.60200055e-07f * fTemp65) + ((0.0742064863f * fTemp70) + ((0.0332071297f * fTemp178) + ((1.77360005e-06f * fTemp173) + (0.0121721542f * fTemp158)))))))))))))))))))));
			output22[i] = FAUSTFLOAT((0.990027726f * (fVec21[((IOTA - iConst37) & 63)] * fRec413[0])));
			fVec22[(IOTA & 63)] = (((0.0189076494f * fTemp199) + ((0.0682108104f * fTemp121) + ((0.0532494672f * fTemp111) + ((0.035022445f * fTemp102) + ((0.0425682254f * fTemp132) + ((0.00854252279f * fTemp139) + ((0.00309614278f * fTemp146) + ((0.00180044828f * fTemp41) + ((0.00776369777f * fTemp47) + ((0.0331928693f * fTemp59) + ((0.0441909097f * fTemp65) + ((0.00734717352f * fTemp70) + ((0.0148723898f * fTemp80) + ((0.0157222096f * fTemp85) + ((0.0105118724f * fTemp173) + ((0.0334550142f * fTemp168) + (0.00436064741f * fTemp91))))))))))))))))) - ((0.0254423618f * fTemp125) + ((0.0678718835f * fTemp117) + ((0.0250031725f * fTemp114) + ((0.0420738682f * fTemp195) + ((0.0137631269f * fTemp192) + ((0.00875738822f * fTemp110) + ((0.0672813505f * fTemp105) + ((0.00309736817f * fTemp13) + ((0.00831518043f * fTemp20) + ((0.00106891245f * fTemp27) + ((0.00568165537f * fTemp185) + ((0.00129178935f * fTemp34) + ((0.00817627273f * fTemp53) + ((0.0315268077f * fTemp75) + ((0.0213441104f * fTemp178) + ((((0.0031433471f * fTemp152) + (0.00159648992f * fTemp158)) + (0.0356898531f * fTemp164)) + (0.0262443777f * fTemp98))))))))))))))))));
			output23[i] = FAUSTFLOAT((0.990249336f * (fRec413[0] * fVec22[((IOTA - iConst38) & 63)])));
			fVec23[(IOTA & 63)] = ((((((((((((0.00486049941f * fTemp47) + (((((((0.010592496f * fTemp173) + ((0.00179317046f * fTemp158) + (0.0263115503f * fTemp98))) + (0.019768076f * fTemp85)) + (0.00610464113f * fTemp80)) + (0.0213689711f * fTemp178)) + (0.0603186153f * fTemp70)) + (6.7400002e-08f * fTemp59))) + (0.00537661789f * fTemp34)) + (0.000869698881f * fTemp13)) + (0.0151921809f * fTemp139)) + (0.0220359024f * fTemp102)) + (2.35727002e-05f * fTemp105)) + (0.0361542478f * fTemp192)) + (0.0476111248f * fTemp111)) + (0.0582197383f * fTemp121)) + (0.0381643772f * fTemp199)) - ((((((0.00767372129f * fTemp110) + ((((0.00380352279f * fTemp20) + ((0.00573021499f * fTemp27) + ((((0.00307886605f * fTemp53) + ((0.0445941202f * fTemp65) + ((0.0370129459f * fTemp75) + (((0.0257153399f * fTemp164) + ((0.0376170389f * fTemp91) + (0.013300335f * fTemp152))) + (0.0336324349f * fTemp168))))) + (1.48975996e-05f * fTemp41)) + (0.00658963574f * fTemp185)))) + (0.00310948142f * fTemp146)) + (0.0209040754f * fTemp132))) + (0.0612792261f * fTemp195)) + (0.0271065626f * fTemp114)) + (0.0353708342f * fTemp117)) + (0.0202950761f * fTemp125)));
			output24[i] = FAUSTFLOAT((0.990249336f * (fVec23[((IOTA - iConst38) & 63)] * fRec413[0])));
			fVec24[(IOTA & 63)] = (((0.0354840085f * fTemp199) + (((0.0588502251f * fTemp111) + ((0.0742251649f * fTemp105) + ((2.9099001e-06f * fTemp102) + ((1.88959996e-06f * fTemp132) + ((0.00536218751f * fTemp146) + ((0.00356969144f * fTemp27) + ((1.09539997e-06f * fTemp34) + ((0.0106761474f * fTemp47) + ((1.09480004e-06f * fTemp65) + ((0.0331940688f * fTemp178) + ((1.66199996e-07f * fTemp80) + ((0.011640396f * fTemp85) + ((0.0240037106f * fTemp173) + (((0.0121762846f * fTemp158) + (0.0607701056f * fTemp164)) + (4.80959989e-06f * fTemp98))))))))))))))) + (4.36999983e-08f * fTemp117))) - ((0.0305345897f * fTemp125) + ((4.06400005e-07f * fTemp121) + ((0.0229036063f * fTemp114) + ((0.0888105035f * fTemp195) + ((0.0457786024f * fTemp192) + ((6.12999997e-08f * fTemp110) + ((0.0236104764f * fTemp139) + ((4.01270017e-06f * fTemp13) + ((4.65419998e-06f * fTemp20) + ((0.0124783926f * fTemp185) + ((0.00446484191f * fTemp41) + ((2.68980011e-06f * fTemp53) + ((0.0287560839f * fTemp59) + ((6.38699987e-07f * fTemp70) + ((4.66890015e-06f * fTemp75) + ((0.0843136683f * fTemp168) + ((0.00293079647f * fTemp152) + (5.52739994e-06f * fTemp91)))))))))))))))))));
			output25[i] = FAUSTFLOAT((0.990027726f * (fRec413[0] * fVec24[((IOTA - iConst37) & 63)])));
			fVec25[(IOTA & 63)] = (((0.03817489f * fTemp199) + ((0.0353744999f * fTemp117) + ((0.0476081558f * fTemp111) + ((0.0361613147f * fTemp192) + ((0.00767904613f * fTemp110) + ((0.0208674725f * fTemp132) + ((0.0151894204f * fTemp139) + ((0.00381907681f * fTemp20) + (((0.00487746019f * fTemp47) + ((0.00309207453f * fTemp53) + ((0.0445668623f * fTemp65) + ((0.0370202139f * fTemp75) + ((0.0213809013f * fTemp178) + ((0.01976908f * fTemp85) + ((0.0105883619f * fTemp173) + ((0.00177841587f * fTemp158) + (0.0376309529f * fTemp91))))))))) + (9.27529982e-06f * fTemp41)))))))))) - ((0.0202873833f * fTemp125) + ((0.0582216233f * fTemp121) + ((0.0271157604f * fTemp114) + ((0.0612672567f * fTemp195) + ((5.4594002e-06f * fTemp105) + ((0.022037847f * fTemp102) + ((0.000867019524f * fTemp13) + ((0.00311885797f * fTemp146) + ((0.00575290015f * fTemp27) + ((0.00660147425f * fTemp185) + ((0.0053880997f * fTemp34) + ((9.20010007e-06f * fTemp59) + ((0.0603082888f * fTemp70) + ((0.00611473899f * fTemp80) + ((0.03361056f * fTemp168) + (((0.0133118471f * fTemp152) + (0.0257394761f * fTemp164)) + (0.0263291374f * fTemp98))))))))))))))))));
			output26[i] = FAUSTFLOAT((0.990249336f * (fRec413[0] * fVec25[((IOTA - iConst38) & 63)])));
			fVec26[(IOTA & 63)] = (((0.0189102255f * fTemp199) + ((0.0678689703f * fTemp117) + ((0.0532465205f * fTemp111) + ((0.00874163397f * fTemp110) + ((0.00855735317f * fTemp139) + ((0.00309352577f * fTemp13) + ((0.00308140111f * fTemp146) + ((0.00832901802f * fTemp20) + ((0.00129376946f * fTemp34) + ((0.00180271978f * fTemp41) + ((0.00776711432f * fTemp47) + ((0.00817391276f * fTemp53) + ((0.033195816f * fTemp59) + ((0.0315313675f * fTemp75) + ((0.0157154445f * fTemp85) + ((0.0105084423f * fTemp173) + ((0.0334601849f * fTemp168) + (0.0262424536f * fTemp98)))))))))))))))))) - ((0.025433531f * fTemp125) + ((0.0682025552f * fTemp121) + ((0.0250075795f * fTemp114) + ((0.04206644f * fTemp195) + ((0.0137799941f * fTemp192) + ((0.0672811568f * fTemp105) + ((0.0350250863f * fTemp102) + ((0.0425677262f * fTemp132) + ((0.00106344f * fTemp27) + (((0.0442019254f * fTemp65) + ((0.00733538205f * fTemp70) + ((0.0213397481f * fTemp178) + ((0.0148645388f * fTemp80) + ((0.0356800146f * fTemp164) + ((0.0043497989f * fTemp91) + ((0.00314469915f * fTemp152) + (0.0015972571f * fTemp158)))))))) + (0.00568107935f * fTemp185))))))))))));
			output27[i] = FAUSTFLOAT((0.990249336f * (fRec413[0] * fVec26[((IOTA - iConst38) & 63)])));
			fVec27[(IOTA & 63)] = (((0.0887977406f * fTemp117) + ((1.94500004e-07f * fTemp195) + ((0.0588410161f * fTemp111) + ((0.00293350592f * fTemp110) + ((0.0457778834f * fTemp132) + ((0.0124778012f * fTemp20) + ((0.00357354642f * fTemp27) + ((1.87640001e-06f * fTemp185) + ((2.38300004e-06f * fTemp41) + ((0.0106785139f * fTemp47) + ((3.56860005e-06f * fTemp53) + ((0.07421875f * fTemp70) + ((0.0116383452f * fTemp85) + ((2.25329995e-06f * fTemp173) + ((0.0843039677f * fTemp168) + ((1.34299995e-07f * fTemp152) + (0.060766831f * fTemp164))))))))))))))))) - ((0.0305338241f * fTemp125) + ((4.29940019e-06f * fTemp199) + ((3.24199988e-07f * fTemp121) + ((0.0228981767f * fTemp114) + ((1.91670006e-06f * fTemp192) + ((1.10400003e-06f * fTemp105) + ((0.0354756378f * fTemp102) + ((0.0236169193f * fTemp139) + ((0.00446616486f * fTemp13) + ((0.00535984151f * fTemp146) + ((3.11849999e-06f * fTemp34) + ((8.39889981e-06f * fTemp59) + ((1.65259996e-06f * fTemp65) + ((7.63729986e-06f * fTemp75) + ((0.0331902243f * fTemp178) + ((0.0240087509f * fTemp80) + (((0.0121802175f * fTemp158) + (0.0287575163f * fTemp91)) + (6.97909991e-06f * fTemp98)))))))))))))))))));
			output28[i] = FAUSTFLOAT((0.990027726f * (fRec413[0] * fVec27[((IOTA - iConst37) & 63)])));
			fVec28[(IOTA & 63)] = (((0.0582146868f * fTemp121) + ((0.0612718165f * fTemp117) + ((0.0353668891f * fTemp195) + ((0.0476072319f * fTemp111) + ((0.0209110584f * fTemp192) + ((0.0132962577f * fTemp110) + ((0.0603153482f * fTemp105) + ((0.0151955448f * fTemp139) + ((0.00309903664f * fTemp146) + ((0.00659041433f * fTemp20) + ((0.00380104012f * fTemp185) + ((0.00538988737f * fTemp34) + ((0.000869022275f * fTemp41) + ((0.00486262236f * fTemp47) + ((0.0445954353f * fTemp65) + ((2.04370008e-05f * fTemp70) + ((0.0197647121f * fTemp85) + ((0.0336267091f * fTemp168) + ((0.00768212462f * fTemp152) + (3.01039995e-06f * fTemp91)))))))))))))))))))) - ((0.0202854425f * fTemp125) + ((0.0220347978f * fTemp199) + ((0.0271103531f * fTemp114) + ((0.0381675474f * fTemp102) + ((0.0361598507f * fTemp132) + ((5.17070021e-06f * fTemp13) + ((0.00574313244f * fTemp27) + ((0.00308199762f * fTemp53) + ((0.0376159959f * fTemp59) + ((0.0370129235f * fTemp75) + ((0.0213693399f * fTemp178) + ((0.0105815213f * fTemp80) + ((0.00610750495f * fTemp173) + ((0.0263063274f * fTemp98) + ((0.00178265246f * fTemp158) + (0.0257175434f * fTemp164)))))))))))))))));
			output29[i] = FAUSTFLOAT((0.990249336f * (fRec413[0] * fVec28[((IOTA - iConst38) & 63)])));
			fVec29[(IOTA & 63)] = (((0.0682138726f * fTemp121) + ((0.042072583f * fTemp117) + ((0.0678741783f * fTemp195) + ((0.0532519892f * fTemp111) + ((0.00313719059f * fTemp110) + ((0.00734153436f * fTemp105) + ((0.0137856426f * fTemp132) + ((0.00855355989f * fTemp139) + ((0.00179522741f * fTemp13) + ((0.00568517065f * fTemp20) + ((0.00831737742f * fTemp185) + ((0.00776614109f * fTemp47) + ((0.00434307195f * fTemp59) + ((0.0213273577f * fTemp178) + ((0.0157040954f * fTemp85) + (((0.0331791602f * fTemp91) + ((0.00873184763f * fTemp152) + (0.00159112422f * fTemp158))) + (0.0262230765f * fTemp98))))))))))))))))) - ((0.0254328717f * fTemp125) + ((0.0350133739f * fTemp199) + ((0.025004046f * fTemp114) + ((0.0425876118f * fTemp192) + ((0.0189079586f * fTemp102) + ((0.00307896291f * fTemp146) + ((0.00105785835f * fTemp27) + ((0.00130646105f * fTemp34) + ((0.00310442969f * fTemp41) + ((0.0081731705f * fTemp53) + ((0.0442137532f * fTemp65) + ((0.0672949478f * fTemp70) + ((0.0315223634f * fTemp75) + ((0.010509165f * fTemp80) + ((0.0148637323f * fTemp173) + ((0.0334611647f * fTemp168) + (0.0356942602f * fTemp164))))))))))))))))));
			output30[i] = FAUSTFLOAT((0.990249336f * (fRec413[0] * fVec29[((IOTA - iConst38) & 63)])));
			fRec15[1] = fRec15[0];
			fRec13[1] = fRec13[0];
			fRec12[1] = fRec12[0];
			fRec10[1] = fRec10[0];
			fRec9[1] = fRec9[0];
			fRec7[1] = fRec7[0];
			fRec6[1] = fRec6[0];
			fRec4[1] = fRec4[0];
			fRec3[1] = fRec3[0];
			fRec1[1] = fRec1[0];
			fRec16[1] = fRec16[0];
			fRec0[2] = fRec0[1];
			fRec0[1] = fRec0[0];
			fRec17[1] = fRec17[0];
			fRec33[1] = fRec33[0];
			fRec31[1] = fRec31[0];
			fRec30[1] = fRec30[0];
			fRec28[1] = fRec28[0];
			fRec27[1] = fRec27[0];
			fRec25[1] = fRec25[0];
			fRec24[1] = fRec24[0];
			fRec22[1] = fRec22[0];
			fRec21[1] = fRec21[0];
			fRec19[1] = fRec19[0];
			fRec18[2] = fRec18[1];
			fRec18[1] = fRec18[0];
			fRec49[1] = fRec49[0];
			fRec47[1] = fRec47[0];
			fRec46[1] = fRec46[0];
			fRec44[1] = fRec44[0];
			fRec43[1] = fRec43[0];
			fRec41[1] = fRec41[0];
			fRec40[1] = fRec40[0];
			fRec38[1] = fRec38[0];
			fRec37[1] = fRec37[0];
			fRec35[1] = fRec35[0];
			fRec34[2] = fRec34[1];
			fRec34[1] = fRec34[0];
			fRec65[1] = fRec65[0];
			fRec63[1] = fRec63[0];
			fRec62[1] = fRec62[0];
			fRec60[1] = fRec60[0];
			fRec59[1] = fRec59[0];
			fRec57[1] = fRec57[0];
			fRec56[1] = fRec56[0];
			fRec54[1] = fRec54[0];
			fRec53[1] = fRec53[0];
			fRec51[1] = fRec51[0];
			fRec50[2] = fRec50[1];
			fRec50[1] = fRec50[0];
			fRec81[1] = fRec81[0];
			fRec79[1] = fRec79[0];
			fRec78[1] = fRec78[0];
			fRec76[1] = fRec76[0];
			fRec75[1] = fRec75[0];
			fRec73[1] = fRec73[0];
			fRec72[1] = fRec72[0];
			fRec70[1] = fRec70[0];
			fRec69[1] = fRec69[0];
			fRec67[1] = fRec67[0];
			fRec66[2] = fRec66[1];
			fRec66[1] = fRec66[0];
			fRec94[1] = fRec94[0];
			fRec92[1] = fRec92[0];
			fRec91[1] = fRec91[0];
			fRec89[1] = fRec89[0];
			fRec88[1] = fRec88[0];
			fRec86[1] = fRec86[0];
			fRec85[1] = fRec85[0];
			fRec83[1] = fRec83[0];
			fRec82[2] = fRec82[1];
			fRec82[1] = fRec82[0];
			fRec107[1] = fRec107[0];
			fRec105[1] = fRec105[0];
			fRec104[1] = fRec104[0];
			fRec102[1] = fRec102[0];
			fRec101[1] = fRec101[0];
			fRec99[1] = fRec99[0];
			fRec98[1] = fRec98[0];
			fRec96[1] = fRec96[0];
			fRec95[2] = fRec95[1];
			fRec95[1] = fRec95[0];
			fRec120[1] = fRec120[0];
			fRec118[1] = fRec118[0];
			fRec117[1] = fRec117[0];
			fRec115[1] = fRec115[0];
			fRec114[1] = fRec114[0];
			fRec112[1] = fRec112[0];
			fRec111[1] = fRec111[0];
			fRec109[1] = fRec109[0];
			fRec108[2] = fRec108[1];
			fRec108[1] = fRec108[0];
			fRec133[1] = fRec133[0];
			fRec131[1] = fRec131[0];
			fRec130[1] = fRec130[0];
			fRec128[1] = fRec128[0];
			fRec127[1] = fRec127[0];
			fRec125[1] = fRec125[0];
			fRec124[1] = fRec124[0];
			fRec122[1] = fRec122[0];
			fRec121[2] = fRec121[1];
			fRec121[1] = fRec121[0];
			fRec143[1] = fRec143[0];
			fRec141[1] = fRec141[0];
			fRec140[1] = fRec140[0];
			fRec138[1] = fRec138[0];
			fRec137[1] = fRec137[0];
			fRec135[1] = fRec135[0];
			fRec134[2] = fRec134[1];
			fRec134[1] = fRec134[0];
			fRec153[1] = fRec153[0];
			fRec151[1] = fRec151[0];
			fRec150[1] = fRec150[0];
			fRec148[1] = fRec148[0];
			fRec147[1] = fRec147[0];
			fRec145[1] = fRec145[0];
			fRec144[2] = fRec144[1];
			fRec144[1] = fRec144[0];
			fRec163[1] = fRec163[0];
			fRec161[1] = fRec161[0];
			fRec160[1] = fRec160[0];
			fRec158[1] = fRec158[0];
			fRec157[1] = fRec157[0];
			fRec155[1] = fRec155[0];
			fRec154[2] = fRec154[1];
			fRec154[1] = fRec154[0];
			fRec173[1] = fRec173[0];
			fRec171[1] = fRec171[0];
			fRec170[1] = fRec170[0];
			fRec168[1] = fRec168[0];
			fRec167[1] = fRec167[0];
			fRec165[1] = fRec165[0];
			fRec164[2] = fRec164[1];
			fRec164[1] = fRec164[0];
			fRec186[1] = fRec186[0];
			fRec184[1] = fRec184[0];
			fRec183[1] = fRec183[0];
			fRec181[1] = fRec181[0];
			fRec180[1] = fRec180[0];
			fRec178[1] = fRec178[0];
			fRec177[1] = fRec177[0];
			fRec175[1] = fRec175[0];
			fRec174[2] = fRec174[1];
			fRec174[1] = fRec174[0];
			fRec202[1] = fRec202[0];
			fRec200[1] = fRec200[0];
			fRec199[1] = fRec199[0];
			fRec197[1] = fRec197[0];
			fRec196[1] = fRec196[0];
			fRec194[1] = fRec194[0];
			fRec193[1] = fRec193[0];
			fRec191[1] = fRec191[0];
			fRec190[1] = fRec190[0];
			fRec188[1] = fRec188[0];
			fRec187[2] = fRec187[1];
			fRec187[1] = fRec187[0];
			fRec209[1] = fRec209[0];
			fRec207[1] = fRec207[0];
			fRec206[1] = fRec206[0];
			fRec204[1] = fRec204[0];
			fRec203[2] = fRec203[1];
			fRec203[1] = fRec203[0];
			fRec219[1] = fRec219[0];
			fRec217[1] = fRec217[0];
			fRec216[1] = fRec216[0];
			fRec214[1] = fRec214[0];
			fRec213[1] = fRec213[0];
			fRec211[1] = fRec211[0];
			fRec210[2] = fRec210[1];
			fRec210[1] = fRec210[0];
			fRec232[1] = fRec232[0];
			fRec230[1] = fRec230[0];
			fRec229[1] = fRec229[0];
			fRec227[1] = fRec227[0];
			fRec226[1] = fRec226[0];
			fRec224[1] = fRec224[0];
			fRec223[1] = fRec223[0];
			fRec221[1] = fRec221[0];
			fRec220[2] = fRec220[1];
			fRec220[1] = fRec220[0];
			fRec233[2] = fRec233[1];
			fRec233[1] = fRec233[0];
			fRec237[1] = fRec237[0];
			fRec235[1] = fRec235[0];
			fRec234[2] = fRec234[1];
			fRec234[1] = fRec234[0];
			fRec241[1] = fRec241[0];
			fRec239[1] = fRec239[0];
			fRec238[2] = fRec238[1];
			fRec238[1] = fRec238[0];
			fRec248[1] = fRec248[0];
			fRec246[1] = fRec246[0];
			fRec245[1] = fRec245[0];
			fRec243[1] = fRec243[0];
			fRec242[2] = fRec242[1];
			fRec242[1] = fRec242[0];
			fRec255[1] = fRec255[0];
			fRec253[1] = fRec253[0];
			fRec252[1] = fRec252[0];
			fRec250[1] = fRec250[0];
			fRec249[2] = fRec249[1];
			fRec249[1] = fRec249[0];
			fRec271[1] = fRec271[0];
			fRec269[1] = fRec269[0];
			fRec268[1] = fRec268[0];
			fRec266[1] = fRec266[0];
			fRec265[1] = fRec265[0];
			fRec263[1] = fRec263[0];
			fRec262[1] = fRec262[0];
			fRec260[1] = fRec260[0];
			fRec259[1] = fRec259[0];
			fRec257[1] = fRec257[0];
			fRec256[2] = fRec256[1];
			fRec256[1] = fRec256[0];
			fRec287[1] = fRec287[0];
			fRec285[1] = fRec285[0];
			fRec284[1] = fRec284[0];
			fRec282[1] = fRec282[0];
			fRec281[1] = fRec281[0];
			fRec279[1] = fRec279[0];
			fRec278[1] = fRec278[0];
			fRec276[1] = fRec276[0];
			fRec275[1] = fRec275[0];
			fRec273[1] = fRec273[0];
			fRec272[2] = fRec272[1];
			fRec272[1] = fRec272[0];
			fRec303[1] = fRec303[0];
			fRec301[1] = fRec301[0];
			fRec300[1] = fRec300[0];
			fRec298[1] = fRec298[0];
			fRec297[1] = fRec297[0];
			fRec295[1] = fRec295[0];
			fRec294[1] = fRec294[0];
			fRec292[1] = fRec292[0];
			fRec291[1] = fRec291[0];
			fRec289[1] = fRec289[0];
			fRec288[2] = fRec288[1];
			fRec288[1] = fRec288[0];
			fRec316[1] = fRec316[0];
			fRec314[1] = fRec314[0];
			fRec313[1] = fRec313[0];
			fRec311[1] = fRec311[0];
			fRec310[1] = fRec310[0];
			fRec308[1] = fRec308[0];
			fRec307[1] = fRec307[0];
			fRec305[1] = fRec305[0];
			fRec304[2] = fRec304[1];
			fRec304[1] = fRec304[0];
			fRec329[1] = fRec329[0];
			fRec327[1] = fRec327[0];
			fRec326[1] = fRec326[0];
			fRec324[1] = fRec324[0];
			fRec323[1] = fRec323[0];
			fRec321[1] = fRec321[0];
			fRec320[1] = fRec320[0];
			fRec318[1] = fRec318[0];
			fRec317[2] = fRec317[1];
			fRec317[1] = fRec317[0];
			fRec342[1] = fRec342[0];
			fRec340[1] = fRec340[0];
			fRec339[1] = fRec339[0];
			fRec337[1] = fRec337[0];
			fRec336[1] = fRec336[0];
			fRec334[1] = fRec334[0];
			fRec333[1] = fRec333[0];
			fRec331[1] = fRec331[0];
			fRec330[2] = fRec330[1];
			fRec330[1] = fRec330[0];
			fRec349[1] = fRec349[0];
			fRec347[1] = fRec347[0];
			fRec346[1] = fRec346[0];
			fRec344[1] = fRec344[0];
			fRec343[2] = fRec343[1];
			fRec343[1] = fRec343[0];
			fRec359[1] = fRec359[0];
			fRec357[1] = fRec357[0];
			fRec356[1] = fRec356[0];
			fRec354[1] = fRec354[0];
			fRec353[1] = fRec353[0];
			fRec351[1] = fRec351[0];
			fRec350[2] = fRec350[1];
			fRec350[1] = fRec350[0];
			fRec369[1] = fRec369[0];
			fRec367[1] = fRec367[0];
			fRec366[1] = fRec366[0];
			fRec364[1] = fRec364[0];
			fRec363[1] = fRec363[0];
			fRec361[1] = fRec361[0];
			fRec360[2] = fRec360[1];
			fRec360[1] = fRec360[0];
			fRec385[1] = fRec385[0];
			fRec383[1] = fRec383[0];
			fRec382[1] = fRec382[0];
			fRec380[1] = fRec380[0];
			fRec379[1] = fRec379[0];
			fRec377[1] = fRec377[0];
			fRec376[1] = fRec376[0];
			fRec374[1] = fRec374[0];
			fRec373[1] = fRec373[0];
			fRec371[1] = fRec371[0];
			fRec370[2] = fRec370[1];
			fRec370[1] = fRec370[0];
			fRec401[1] = fRec401[0];
			fRec399[1] = fRec399[0];
			fRec398[1] = fRec398[0];
			fRec396[1] = fRec396[0];
			fRec395[1] = fRec395[0];
			fRec393[1] = fRec393[0];
			fRec392[1] = fRec392[0];
			fRec390[1] = fRec390[0];
			fRec389[1] = fRec389[0];
			fRec387[1] = fRec387[0];
			fRec386[2] = fRec386[1];
			fRec386[1] = fRec386[0];
			fRec405[1] = fRec405[0];
			fRec403[1] = fRec403[0];
			fRec402[2] = fRec402[1];
			fRec402[1] = fRec402[0];
			fRec412[1] = fRec412[0];
			fRec410[1] = fRec410[0];
			fRec409[1] = fRec409[0];
			fRec407[1] = fRec407[0];
			fRec406[2] = fRec406[1];
			fRec406[1] = fRec406[0];
			fRec413[1] = fRec413[0];
			for (int j0 = 11; (j0 > 0); j0 = (j0 - 1)) {
				fVec0[j0] = fVec0[(j0 - 1)];
				
			}
			for (int j1 = 12; (j1 > 0); j1 = (j1 - 1)) {
				fVec1[j1] = fVec1[(j1 - 1)];
				
			}
			for (int j2 = 11; (j2 > 0); j2 = (j2 - 1)) {
				fVec2[j2] = fVec2[(j2 - 1)];
				
			}
			for (int j3 = 11; (j3 > 0); j3 = (j3 - 1)) {
				fVec3[j3] = fVec3[(j3 - 1)];
				
			}
			for (int j4 = 12; (j4 > 0); j4 = (j4 - 1)) {
				fVec4[j4] = fVec4[(j4 - 1)];
				
			}
			for (int j5 = 11; (j5 > 0); j5 = (j5 - 1)) {
				fVec5[j5] = fVec5[(j5 - 1)];
				
			}
			IOTA = (IOTA + 1);
			
		}
		
	}

	
};

//----------------------------------------------------------------------------
// SuperCollider/Faust interface
//----------------------------------------------------------------------------

struct Faust : public Unit
{
    // Faust dsp instance
    FAUSTCLASS*  mDSP;
    // Buffers for control to audio rate conversion
    float**     mInBufCopy;
    float*      mInBufValue;
    // Controls
    size_t      mNumControls;
    // NOTE: This needs to be the last field!
    //
    // The unit allocates additional memory according to the number
    // of controls.
    Control     mControls[0];

    int getNumAudioInputs() { return mDSP->getNumInputs(); }
};

// Global state

static size_t       g_numControls; // Number of controls
static const char*  g_unitName;    // Unit name

// Initialize the global state with unit name and sample rate.
void initState(const std::string& name, int sampleRate);

// Return the unit size in bytes, including static fields and controls.
static size_t unitSize();

// Convert a file name to a valid unit name.
static std::string fileNameToUnitName(const std::string& fileName);

// Convert the XML unit name to a valid class name.
static std::string normalizeClassName(const std::string& name);

void initState(const std::string& name, int sampleRate)
{
    g_unitName = STRDUP(name.c_str());

    mydsp* dsp = new FAUSTCLASS;
    ControlCounter* cc = new ControlCounter;

    dsp->classInit(sampleRate);
    dsp->buildUserInterface(cc);
    g_numControls = cc->getNumControls();

    delete dsp;
    delete cc;
}

size_t unitSize()
{
    return sizeof(Faust) + g_numControls * sizeof(Control);
}

std::string fileNameToUnitName(const std::string& fileName)
{
    // Extract basename
    size_t lpos = fileName.rfind('/', fileName.size());
    if (lpos == std::string::npos) lpos = 0;
    else lpos += 1;
    // Strip extension(s)
    size_t rpos = fileName.find('.', lpos);
    // Return substring
    return fileName.substr(lpos, rpos > lpos ? rpos - lpos : 0);
}

// Globals

static InterfaceTable* ft;

// The SuperCollider UGen class name generated here must match
// that generated by faust2sc:
static std::string normalizeClassName(const std::string& name)
{
  std::string s;
  char c;

  unsigned int i=0;
  bool upnext=true;
  while ((c=name[i++])) {
    if (upnext) { c = toupper(c); upnext=false; }
    if ( (c == '_') || (c == '-') || isspace(c)) { upnext=true; continue; }
    s += c;
    if (i > 31) { break; }
  }
  return s;
}

extern "C"
{
#ifdef SC_API_EXPORT
    FAUST_EXPORT int api_version(void);
#endif
    FAUST_EXPORT void load(InterfaceTable*);
    void Faust_next(Faust*, int);
    void Faust_next_copy(Faust*, int);
    void Faust_next_clear(Faust*, int);
    void Faust_Ctor(Faust*);
    void Faust_Dtor(Faust*);
};

inline static void fillBuffer(float* dst, int n, float v)
{
    Fill(n, dst, v);
}

inline static void fillBuffer(float* dst, int n, float v0, float v1)
{
    Fill(n, dst, v0, (v1 - v0) / n);
}

inline static void copyBuffer(float* dst, int n, float* src)
{
    Copy(n, dst, src);
}

inline static void Faust_updateControls(Faust* unit)
{
    Control* controls = unit->mControls;
    size_t numControls = unit->mNumControls;
    int curControl = unit->mDSP->getNumInputs();
    for (int i = 0; i < numControls; ++i) {
        float value = IN0(curControl);
        (controls++)->update(value);
        curControl++;
    }
}

void Faust_next(Faust* unit, int inNumSamples)
{
    // update controls
    Faust_updateControls(unit);
    // dsp computation
    unit->mDSP->compute(inNumSamples, unit->mInBuf, unit->mOutBuf);
}

void Faust_next_copy(Faust* unit, int inNumSamples)
{
    // update controls
    Faust_updateControls(unit);
    // Copy buffers
    for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
        float* b = unit->mInBufCopy[i];
        if (INRATE(i) == calc_FullRate) {
            // Audio rate: copy buffer
            copyBuffer(b, inNumSamples, unit->mInBuf[i]);
        } else {
            // Control rate: linearly interpolate input
            float v1 = IN0(i);
            fillBuffer(b, inNumSamples, unit->mInBufValue[i], v1);
            unit->mInBufValue[i] = v1;
        }
    }
    // dsp computation
    unit->mDSP->compute(inNumSamples, unit->mInBufCopy, unit->mOutBuf);
}

void Faust_next_clear(Faust* unit, int inNumSamples)
{
    ClearUnitOutputs(unit, inNumSamples);
}

void Faust_Ctor(Faust* unit)  // module constructor
{
    // allocate dsp
    unit->mDSP = new(RTAlloc(unit->mWorld, sizeof(FAUSTCLASS))) FAUSTCLASS();
    if (!unit->mDSP) {
        Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
        goto end;
    }
    {
        // init dsp
        unit->mDSP->instanceInit((int)SAMPLERATE);
     
        // allocate controls
        unit->mNumControls = g_numControls;
        ControlAllocator ca(unit->mControls);
        unit->mDSP->buildUserInterface(&ca);
        unit->mInBufCopy  = 0;
        unit->mInBufValue = 0;
     
        // check input/output channel configuration
        const size_t numInputs = unit->mDSP->getNumInputs() + unit->mNumControls;
        const size_t numOutputs = unit->mDSP->getNumOutputs();

        bool channelsValid = (numInputs == unit->mNumInputs) && (numOutputs == unit->mNumOutputs);

        if (channelsValid) {
            bool rateValid = true;
            for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
                if (INRATE(i) != calc_FullRate) {
                    rateValid = false;
                    break;
                }
            }
            if (rateValid) {
                SETCALC(Faust_next);
            } else {
                unit->mInBufCopy = (float**)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*sizeof(float*));
                if (!unit->mInBufCopy) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                // Allocate memory for input buffer copies (numInputs * bufLength)
                // and linear interpolation state (numInputs)
                // = numInputs * (bufLength + 1)
                unit->mInBufValue = (float*)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*sizeof(float));
                if (!unit->mInBufValue) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                // Aquire memory for interpolator state.
                float* mem = (float*)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*BUFLENGTH*sizeof(float));
                if (mem) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
                    // Initialize interpolator.
                    unit->mInBufValue[i] = IN0(i);
                    // Aquire buffer memory.
                    unit->mInBufCopy[i] = mem;
                    mem += BUFLENGTH;
                }
                SETCALC(Faust_next_copy);
            }
    #if !defined(NDEBUG)
            Print("Faust[%s]:\n", g_unitName);
            Print("    Inputs:   %d\n"
                  "    Outputs:  %d\n"
                  "    Callback: %s\n",
                  numInputs, numOutputs,
                  unit->mCalcFunc == (UnitCalcFunc)Faust_next ? "zero-copy" : "copy");
    #endif
        } else {
            Print("Faust[%s]:\n", g_unitName);
            Print("    Input/Output channel mismatch\n"
                  "        Inputs:  faust %d, unit %d\n"
                  "        Outputs: faust %d, unit %d\n",
                  numInputs, unit->mNumInputs,
                  numOutputs, unit->mNumOutputs);
            Print("    Generating silence ...\n");
            SETCALC(Faust_next_clear);
        }
    }
    
end:
    // Fix for https://github.com/grame-cncm/faust/issues/13
    ClearUnitOutputs(unit, 1);
}

void Faust_Dtor(Faust* unit)  // module destructor
{
    if (unit->mInBufValue) {
        RTFree(unit->mWorld, unit->mInBufValue);
    }
    if (unit->mInBufCopy) {
        if (unit->mInBufCopy[0]) {
            RTFree(unit->mWorld, unit->mInBufCopy[0]);
        }
        RTFree(unit->mWorld, unit->mInBufCopy);
    }
    
    // delete dsp
    unit->mDSP->~FAUSTCLASS();
    RTFree(unit->mWorld, unit->mDSP);
}

#ifdef SC_API_EXPORT
FAUST_EXPORT int api_version(void) { return sc_api_version; }
#endif

FAUST_EXPORT void load(InterfaceTable* inTable)
{
    ft = inTable;

    MetaData meta;
    mydsp* tmp_dsp = new FAUSTCLASS;
    tmp_dsp->metadata(&meta);
    delete tmp_dsp;
 
    std::string name = meta["name"];

    if (name.empty()) {
        name = fileNameToUnitName(__FILE__);
    }
  
    name = normalizeClassName(name);

#if !defined(NDEBUG) & defined(SC_API_EXPORT)
    Print("Faust: supercollider.cpp: sc_api_version = %d\n", sc_api_version);
#endif

    if (name.empty()) {
        // Catch empty name
        Print("Faust [supercollider.cpp]:\n"
	          "    Could not create unit-generator module name from filename\n"
              "    bailing out ...\n");
        return;
    }

    if (strncmp(name.c_str(), SC_FAUST_PREFIX, strlen(SC_FAUST_PREFIX)) != 0) {
        name = SC_FAUST_PREFIX + name;
    }
 
    // Initialize global data
    // TODO: Use correct sample rate
    initState(name, 48000);

    // Register ugen
    (*ft->fDefineUnit)(
        (char*)name.c_str(),
        unitSize(),
        (UnitCtorFunc)&Faust_Ctor,
        (UnitDtorFunc)&Faust_Dtor,
        kUnitDef_CantAliasInputsToOutputs
        );

#if !defined(NDEBUG)
    Print("Faust: %s numControls=%d\n", name.c_str(), g_numControls);
#endif // NDEBUG
}

#ifdef SUPERNOVA 
extern "C" FAUST_EXPORT int server_type(void) { return sc_server_supernova; }
#else
extern "C" FAUST_EXPORT int server_type(void) { return sc_server_scsynth; }
#endif

// EOF

#endif
