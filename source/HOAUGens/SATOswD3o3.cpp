/* ------------------------------------------------------------
author: "AmbisonicDecoderToolkit"
copyright: "(c) Aaron J. Heller 2013"
license: "BSD 3-Clause License"
name: "SATOswD3o3"
version: "1.2"
Code generated with Faust 2.5.12 (https://faust.grame.fr)
Compilation options: cpp, -scal -ftz 0
------------------------------------------------------------ */

#ifndef  __mydsp_H__
#define  __mydsp_H__

//-------------------------------------------------------------------
// FAUST architecture file for SuperCollider.
// Copyright (C) 2005-2012 Stefan Kersten.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//-------------------------------------------------------------------

// If other than 'faust2sc --prefix Faust' is used, sed this as well:
#if !defined(SC_FAUST_PREFIX)
#define SC_FAUST_PREFIX "Faust"
#endif

#include <map>
#include <string>
#include <string.h>
#include <SC_PlugIn.h>

/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __dsp__
#define __dsp__

#include <string>

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

class UI;
struct Meta;

/**
 * DSP memory manager.
 */

struct dsp_memory_manager {
    
    virtual ~dsp_memory_manager() {}
    
    virtual void* allocate(size_t size) = 0;
    virtual void destroy(void* ptr) = 0;
    
};

/**
* Signal processor definition.
*/

class dsp {

    public:

        dsp() {}
        virtual ~dsp() {}

        /* Return instance number of audio inputs */
        virtual int getNumInputs() = 0;
    
        /* Return instance number of audio outputs */
        virtual int getNumOutputs() = 0;
    
        /**
         * Trigger the ui_interface parameter with instance specific calls
         * to 'addBtton', 'addVerticalSlider'... in order to build the UI.
         *
         * @param ui_interface - the user interface builder
         */
        virtual void buildUserInterface(UI* ui_interface) = 0;
    
        /* Returns the sample rate currently used by the instance */
        virtual int getSampleRate() = 0;
    
        /**
         * Global init, calls the following methods:
         * - static class 'classInit': static tables initialization
         * - 'instanceInit': constants and instance state initialization
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void init(int samplingRate) = 0;

        /**
         * Init instance state
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void instanceInit(int samplingRate) = 0;

        /**
         * Init instance constant state
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void instanceConstants(int samplingRate) = 0;
    
        /* Init default control parameters values */
        virtual void instanceResetUserInterface() = 0;
    
        /* Init instance state (delay lines...) */
        virtual void instanceClear() = 0;
 
        /**
         * Return a clone of the instance.
         *
         * @return a copy of the instance on success, otherwise a null pointer.
         */
        virtual dsp* clone() = 0;
    
        /**
         * Trigger the Meta* parameter with instance specific calls to 'declare' (key, value) metadata.
         *
         * @param m - the Meta* meta user
         */
        virtual void metadata(Meta* m) = 0;
    
        /**
         * DSP instance computation, to be called with successive in/out audio buffers.
         *
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         *
         */
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) = 0;
    
        /**
         * DSP instance computation: alternative method to be used by subclasses.
         *
         * @param date_usec - the timestamp in microsec given by audio driver.
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         *
         */
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { compute(count, inputs, outputs); }
       
};

/**
 * Generic DSP decorator.
 */

class decorator_dsp : public dsp {

    protected:

        dsp* fDSP;

    public:

        decorator_dsp(dsp* dsp = 0):fDSP(dsp) {}
        virtual ~decorator_dsp() { delete fDSP; }

        virtual int getNumInputs() { return fDSP->getNumInputs(); }
        virtual int getNumOutputs() { return fDSP->getNumOutputs(); }
        virtual void buildUserInterface(UI* ui_interface) { fDSP->buildUserInterface(ui_interface); }
        virtual int getSampleRate() { return fDSP->getSampleRate(); }
        virtual void init(int samplingRate) { fDSP->init(samplingRate); }
        virtual void instanceInit(int samplingRate) { fDSP->instanceInit(samplingRate); }
        virtual void instanceConstants(int samplingRate) { fDSP->instanceConstants(samplingRate); }
        virtual void instanceResetUserInterface() { fDSP->instanceResetUserInterface(); }
        virtual void instanceClear() { fDSP->instanceClear(); }
        virtual decorator_dsp* clone() { return new decorator_dsp(fDSP->clone()); }
        virtual void metadata(Meta* m) { fDSP->metadata(m); }
        // Beware: subclasses usually have to overload the two 'compute' methods
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(count, inputs, outputs); }
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(date_usec, count, inputs, outputs); }
    
};

/**
 * DSP factory class.
 */

class dsp_factory {
    
    protected:
    
        // So that to force sub-classes to use deleteDSPFactory(dsp_factory* factory);
        virtual ~dsp_factory() {}
    
    public:
    
        virtual std::string getName() = 0;
        virtual std::string getSHAKey() = 0;
        virtual std::string getDSPCode() = 0;
    
        virtual dsp* createDSPInstance() = 0;
    
        virtual void setMemoryManager(dsp_memory_manager* manager) = 0;
        virtual dsp_memory_manager* getMemoryManager() = 0;
    
};

/**
 * On Intel set FZ (Flush to Zero) and DAZ (Denormals Are Zero)
 * flags to avoid costly denormals.
 */

#ifdef __SSE__
    #include <xmmintrin.h>
    #ifdef __SSE2__
        #define AVOIDDENORMALS _mm_setcsr(_mm_getcsr() | 0x8040)
    #else
        #define AVOIDDENORMALS _mm_setcsr(_mm_getcsr() | 0x8000)
    #endif
#else
    #define AVOIDDENORMALS
#endif

#endif
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __UI_H__
#define __UI_H__

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

/*******************************************************************************
 * UI : Faust DSP User Interface
 * User Interface as expected by the buildUserInterface() method of a DSP.
 * This abstract class contains only the method that the Faust compiler can
 * generate to describe a DSP user interface.
 ******************************************************************************/

struct Soundfile;

class UI
{

    public:

        UI() {}

        virtual ~UI() {}

        // -- widget's layouts

        virtual void openTabBox(const char* label) = 0;
        virtual void openHorizontalBox(const char* label) = 0;
        virtual void openVerticalBox(const char* label) = 0;
        virtual void closeBox() = 0;

        // -- active widgets

        virtual void addButton(const char* label, FAUSTFLOAT* zone) = 0;
        virtual void addCheckButton(const char* label, FAUSTFLOAT* zone) = 0;
        virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;
        virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;
        virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;

        // -- passive widgets

        virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) = 0;
        virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) = 0;
    
        // -- soundfiles
    
        virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) = 0;

        // -- metadata declarations

        virtual void declare(FAUSTFLOAT*, const char*, const char*) {}
};

#endif
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/
 
#ifndef __misc__
#define __misc__

#include <algorithm>
#include <map>
#include <string.h>
#include <stdlib.h>

/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __meta__
#define __meta__

struct Meta
{
    virtual void declare(const char* key, const char* value) = 0;
    virtual ~Meta() {};
};

#endif

using std::max;
using std::min;

struct XXXX_Meta : std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key]=value; }
};

struct MY_Meta : Meta, std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key]=value; }
};

inline int lsr(int x, int n)	{ return int(((unsigned int)x) >> n); }

inline int int2pow2(int x)		{ int r = 0; while ((1<<r) < x) r++; return r; }

inline long lopt(char* argv[], const char* name, long def)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return atoi(argv[i+1]);
	return def;
}

inline bool isopt(char* argv[], const char* name)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return true;
	return false;
}

inline const char* lopts(char* argv[], const char* name, const char* def)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return argv[i+1];
	return def;
}

#endif


using namespace std;

#if defined(__GNUC__) && __GNUC__ >= 4
    #define FAUST_EXPORT __attribute__((visibility("default")))
#else
    #define FAUST_EXPORT  SC_API_EXPORT
#endif

#ifdef WIN32
    #define STRDUP _strdup
#else
    #define STRDUP strdup
#endif

//----------------------------------------------------------------------------
// Vector intrinsics
//----------------------------------------------------------------------------


//----------------------------------------------------------------------------
// Metadata
//----------------------------------------------------------------------------

class MetaData : public Meta
               , public std::map<std::string, std::string>
{
public:
    void declare(const char* key, const char* value)
    {
        (*this)[key] = value;
    }
};

//----------------------------------------------------------------------------
// Control counter
//----------------------------------------------------------------------------

class ControlCounter : public UI
{
public:
    ControlCounter()
        : mNumControlInputs(0),
          mNumControlOutputs(0)
    {}

    size_t getNumControls() const { return getNumControlInputs(); }
    size_t getNumControlInputs() const { return mNumControlInputs; }
    size_t getNumControlOutputs() const { return mNumControlOutputs; }

    // Layout widgets
    virtual void openTabBox(const char* label) { }
    virtual void openHorizontalBox(const char* label) { }
    virtual void openVerticalBox(const char* label) { }
    virtual void closeBox() { }

    // Active widgets
    virtual void addButton(const char* label, FAUSTFLOAT* zone)
    { addControlInput(); }
    virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
    { addControlInput(); }
    virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }
    virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }
    virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }

    // Passive widgets
    virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
    { addControlOutput(); }
    virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
    { addControlOutput(); }
    
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}

protected:
    void addControlInput() { mNumControlInputs++; }
    void addControlOutput() { mNumControlOutputs++; }

private:
    size_t mNumControlInputs;
    size_t mNumControlOutputs;
};

//----------------------------------------------------------------------------
// UI control
//----------------------------------------------------------------------------

struct Control
{
    typedef void (*UpdateFunction)(Control* self, FAUSTFLOAT value);

    UpdateFunction updateFunction;
    FAUSTFLOAT* zone;
    FAUSTFLOAT min, max;

    inline void update(FAUSTFLOAT value)
    {
        (*updateFunction)(this, value);
    }

    static void simpleUpdate(Control* self, FAUSTFLOAT value)
    {
        *self->zone = value;
    }
    static void boundedUpdate(Control* self, FAUSTFLOAT value)
    {
        *self->zone = sc_clip(value, self->min, self->max);
    }
};

//----------------------------------------------------------------------------
// Control allocator
//----------------------------------------------------------------------------

class ControlAllocator : public UI
{
public:
    ControlAllocator(Control* controls)
        : mControls(controls)
    { }

    // Layout widgets
    virtual void openTabBox(const char* label) { }
    virtual void openHorizontalBox(const char* label) { }
    virtual void openVerticalBox(const char* label) { }
    virtual void closeBox() { }

    // Active widgets
    virtual void addButton(const char* label, FAUSTFLOAT* zone)
    { addSimpleControl(zone); }
    virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
    { addSimpleControl(zone); }
    virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }
    virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }
    virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }

    // Passive widgets
    virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
    virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}

private:
    void addControl(Control::UpdateFunction updateFunction, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT /* step */)
    {
        Control* ctrl        = mControls++;
        ctrl->updateFunction = updateFunction;
        ctrl->zone           = zone;
        ctrl->min            = min;
        ctrl->max            = max;
    }
    void addSimpleControl(FAUSTFLOAT* zone)
    {
        addControl(Control::simpleUpdate, zone, 0.f, 0.f, 0.f);
    }
    void addBoundedControl(FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    {
        addControl(Control::boundedUpdate, zone, min, max, step);
    }

private:
    Control* mControls;
};

//----------------------------------------------------------------------------
// FAUST generated code
//----------------------------------------------------------------------------

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif 

#include <cmath>
#include <math.h>

float mydsp_faustpower2_f(float value) {
	return (value * value);
	
}

#ifndef FAUSTCLASS 
#define FAUSTCLASS mydsp
#endif
#ifdef __APPLE__ 
#define exp10f __exp10f
#define exp10 __exp10
#endif

class mydsp : public dsp {
	
 private:
	
	int fSamplingFreq;
	int iConst0;
	float fConst1;
	FAUSTFLOAT fHslider0;
	float fRec0[2];
	float fConst2;
	float fConst3;
	float fConst4;
	float fConst5;
	float fConst6;
	float fConst7;
	float fConst8;
	float fConst9;
	float fRec10[2];
	float fRec8[2];
	float fRec7[2];
	float fRec5[2];
	float fRec4[2];
	float fRec2[2];
	float fRec1[3];
	FAUSTFLOAT fHslider1;
	float fRec11[2];
	float fRec21[2];
	float fRec19[2];
	float fRec18[2];
	float fRec16[2];
	float fRec15[2];
	float fRec13[2];
	float fRec12[3];
	float fRec31[2];
	float fRec29[2];
	float fRec28[2];
	float fRec26[2];
	float fRec25[2];
	float fRec23[2];
	float fRec22[3];
	float fRec41[2];
	float fRec39[2];
	float fRec38[2];
	float fRec36[2];
	float fRec35[2];
	float fRec33[2];
	float fRec32[3];
	float fConst10;
	float fConst11;
	float fConst12;
	float fConst13;
	float fRec48[2];
	float fRec46[2];
	float fRec45[2];
	float fRec43[2];
	float fRec42[3];
	float fRec55[2];
	float fRec53[2];
	float fRec52[2];
	float fRec50[2];
	float fRec49[3];
	float fConst14;
	float fConst15;
	float fConst16;
	float fRec59[2];
	float fRec57[2];
	float fRec56[3];
	float fRec60[3];
	float fRec64[2];
	float fRec62[2];
	float fRec61[3];
	float fRec71[2];
	float fRec69[2];
	float fRec68[2];
	float fRec66[2];
	float fRec65[3];
	float fRec81[2];
	float fRec79[2];
	float fRec78[2];
	float fRec76[2];
	float fRec75[2];
	float fRec73[2];
	float fRec72[3];
	float fRec91[2];
	float fRec89[2];
	float fRec88[2];
	float fRec86[2];
	float fRec85[2];
	float fRec83[2];
	float fRec82[3];
	float fRec98[2];
	float fRec96[2];
	float fRec95[2];
	float fRec93[2];
	float fRec92[3];
	float fRec102[2];
	float fRec100[2];
	float fRec99[3];
	float fRec109[2];
	float fRec107[2];
	float fRec106[2];
	float fRec104[2];
	float fRec103[3];
	float fRec119[2];
	float fRec117[2];
	float fRec116[2];
	float fRec114[2];
	float fRec113[2];
	float fRec111[2];
	float fRec110[3];
	FAUSTFLOAT fHslider2;
	FAUSTFLOAT fCheckbox0;
	float fRec120[2];
	float fVec0[12];
	int iConst17;
	float fVec1[13];
	int iConst18;
	float fVec2[12];
	float fVec3[12];
	float fVec4[13];
	float fVec5[12];
	int IOTA;
	float fVec6[32];
	int iConst19;
	float fVec7[32];
	int iConst20;
	float fVec8[32];
	float fVec9[32];
	float fVec10[32];
	float fVec11[32];
	float fVec12[32];
	float fVec13[32];
	float fVec14[32];
	float fVec15[32];
	float fVec16[32];
	float fVec17[32];
	float fVec18[64];
	int iConst21;
	float fVec19[64];
	int iConst22;
	float fVec20[64];
	float fVec21[64];
	float fVec22[64];
	float fVec23[64];
	float fVec24[64];
	float fVec25[64];
	float fVec26[64];
	float fVec27[64];
	float fVec28[64];
	float fVec29[64];
	
 public:
	
	void metadata(Meta* m) { 
		m->declare("author", "AmbisonicDecoderToolkit");
		m->declare("copyright", "(c) Aaron J. Heller 2013");
		m->declare("license", "BSD 3-Clause License");
		m->declare("name", "SATOswD3o3");
		m->declare("version", "1.2");
	}

	virtual int getNumInputs() {
		return 16;
		
	}
	virtual int getNumOutputs() {
		return 31;
		
	}
	virtual int getInputRate(int channel) {
		int rate;
		switch (channel) {
			case 0: {
				rate = 1;
				break;
			}
			case 1: {
				rate = 1;
				break;
			}
			case 2: {
				rate = 1;
				break;
			}
			case 3: {
				rate = 1;
				break;
			}
			case 4: {
				rate = 1;
				break;
			}
			case 5: {
				rate = 1;
				break;
			}
			case 6: {
				rate = 1;
				break;
			}
			case 7: {
				rate = 1;
				break;
			}
			case 8: {
				rate = 1;
				break;
			}
			case 9: {
				rate = 1;
				break;
			}
			case 10: {
				rate = 1;
				break;
			}
			case 11: {
				rate = 1;
				break;
			}
			case 12: {
				rate = 1;
				break;
			}
			case 13: {
				rate = 1;
				break;
			}
			case 14: {
				rate = 1;
				break;
			}
			case 15: {
				rate = 1;
				break;
			}
			default: {
				rate = -1;
				break;
			}
			
		}
		return rate;
		
	}
	virtual int getOutputRate(int channel) {
		int rate;
		switch (channel) {
			case 0: {
				rate = 1;
				break;
			}
			case 1: {
				rate = 1;
				break;
			}
			case 2: {
				rate = 1;
				break;
			}
			case 3: {
				rate = 1;
				break;
			}
			case 4: {
				rate = 1;
				break;
			}
			case 5: {
				rate = 1;
				break;
			}
			case 6: {
				rate = 1;
				break;
			}
			case 7: {
				rate = 1;
				break;
			}
			case 8: {
				rate = 1;
				break;
			}
			case 9: {
				rate = 1;
				break;
			}
			case 10: {
				rate = 1;
				break;
			}
			case 11: {
				rate = 1;
				break;
			}
			case 12: {
				rate = 1;
				break;
			}
			case 13: {
				rate = 1;
				break;
			}
			case 14: {
				rate = 1;
				break;
			}
			case 15: {
				rate = 1;
				break;
			}
			case 16: {
				rate = 1;
				break;
			}
			case 17: {
				rate = 1;
				break;
			}
			case 18: {
				rate = 1;
				break;
			}
			case 19: {
				rate = 1;
				break;
			}
			case 20: {
				rate = 1;
				break;
			}
			case 21: {
				rate = 1;
				break;
			}
			case 22: {
				rate = 1;
				break;
			}
			case 23: {
				rate = 1;
				break;
			}
			case 24: {
				rate = 1;
				break;
			}
			case 25: {
				rate = 1;
				break;
			}
			case 26: {
				rate = 1;
				break;
			}
			case 27: {
				rate = 1;
				break;
			}
			case 28: {
				rate = 1;
				break;
			}
			case 29: {
				rate = 1;
				break;
			}
			case 30: {
				rate = 1;
				break;
			}
			default: {
				rate = -1;
				break;
			}
			
		}
		return rate;
		
	}
	
	static void classInit(int samplingFreq) {
		
	}
	
	virtual void instanceConstants(int samplingFreq) {
		fSamplingFreq = samplingFreq;
		iConst0 = min(192000, max(1, fSamplingFreq));
		fConst1 = (3.14159274f / float(iConst0));
		fConst2 = float(iConst0);
		fConst3 = ((44.4325409f / fConst2) + 1.0f);
		fConst4 = (0.0f - (88.8650818f / (fConst2 * fConst3)));
		fConst5 = ((((2364.84619f / fConst2) + 70.3710632f) / fConst2) + 1.0f);
		fConst6 = (1.0f / (fConst3 * fConst5));
		fConst7 = mydsp_faustpower2_f(fConst2);
		fConst8 = (0.0f - (9459.38477f / (fConst7 * fConst5)));
		fConst9 = (0.0f - (((9459.38477f / fConst2) + 140.742126f) / (fConst2 * fConst5)));
		fConst10 = ((((1098.32227f / fConst2) + 57.4018021f) / fConst2) + 1.0f);
		fConst11 = (1.0f / fConst10);
		fConst12 = (0.0f - (4393.28906f / (fConst7 * fConst10)));
		fConst13 = (0.0f - (((4393.28906f / fConst2) + 114.803604f) / (fConst2 * fConst10)));
		fConst14 = ((19.133934f / fConst2) + 1.0f);
		fConst15 = (1.0f / fConst14);
		fConst16 = (0.0f - (38.267868f / (fConst2 * fConst14)));
		iConst17 = int(((5.24453171e-05f * float(iConst0)) + 0.5f));
		iConst18 = int(((5.8272577e-05f * float(iConst0)) + 0.5f));
		iConst19 = int(((0.000145681435f * float(iConst0)) + 0.5f));
		iConst20 = int(((0.000134026923f * float(iConst0)) + 0.5f));
		iConst21 = int(((0.000262226589f * float(iConst0)) + 0.5f));
		iConst22 = int(((0.000256399333f * float(iConst0)) + 0.5f));
		
	}
	
	virtual void instanceResetUserInterface() {
		fHslider0 = FAUSTFLOAT(400.0f);
		fHslider1 = FAUSTFLOAT(0.0f);
		fHslider2 = FAUSTFLOAT(-10.0f);
		fCheckbox0 = FAUSTFLOAT(0.0f);
		
	}
	
	virtual void instanceClear() {
		for (int l0 = 0; (l0 < 2); l0 = (l0 + 1)) {
			fRec0[l0] = 0.0f;
			
		}
		for (int l1 = 0; (l1 < 2); l1 = (l1 + 1)) {
			fRec10[l1] = 0.0f;
			
		}
		for (int l2 = 0; (l2 < 2); l2 = (l2 + 1)) {
			fRec8[l2] = 0.0f;
			
		}
		for (int l3 = 0; (l3 < 2); l3 = (l3 + 1)) {
			fRec7[l3] = 0.0f;
			
		}
		for (int l4 = 0; (l4 < 2); l4 = (l4 + 1)) {
			fRec5[l4] = 0.0f;
			
		}
		for (int l5 = 0; (l5 < 2); l5 = (l5 + 1)) {
			fRec4[l5] = 0.0f;
			
		}
		for (int l6 = 0; (l6 < 2); l6 = (l6 + 1)) {
			fRec2[l6] = 0.0f;
			
		}
		for (int l7 = 0; (l7 < 3); l7 = (l7 + 1)) {
			fRec1[l7] = 0.0f;
			
		}
		for (int l8 = 0; (l8 < 2); l8 = (l8 + 1)) {
			fRec11[l8] = 0.0f;
			
		}
		for (int l9 = 0; (l9 < 2); l9 = (l9 + 1)) {
			fRec21[l9] = 0.0f;
			
		}
		for (int l10 = 0; (l10 < 2); l10 = (l10 + 1)) {
			fRec19[l10] = 0.0f;
			
		}
		for (int l11 = 0; (l11 < 2); l11 = (l11 + 1)) {
			fRec18[l11] = 0.0f;
			
		}
		for (int l12 = 0; (l12 < 2); l12 = (l12 + 1)) {
			fRec16[l12] = 0.0f;
			
		}
		for (int l13 = 0; (l13 < 2); l13 = (l13 + 1)) {
			fRec15[l13] = 0.0f;
			
		}
		for (int l14 = 0; (l14 < 2); l14 = (l14 + 1)) {
			fRec13[l14] = 0.0f;
			
		}
		for (int l15 = 0; (l15 < 3); l15 = (l15 + 1)) {
			fRec12[l15] = 0.0f;
			
		}
		for (int l16 = 0; (l16 < 2); l16 = (l16 + 1)) {
			fRec31[l16] = 0.0f;
			
		}
		for (int l17 = 0; (l17 < 2); l17 = (l17 + 1)) {
			fRec29[l17] = 0.0f;
			
		}
		for (int l18 = 0; (l18 < 2); l18 = (l18 + 1)) {
			fRec28[l18] = 0.0f;
			
		}
		for (int l19 = 0; (l19 < 2); l19 = (l19 + 1)) {
			fRec26[l19] = 0.0f;
			
		}
		for (int l20 = 0; (l20 < 2); l20 = (l20 + 1)) {
			fRec25[l20] = 0.0f;
			
		}
		for (int l21 = 0; (l21 < 2); l21 = (l21 + 1)) {
			fRec23[l21] = 0.0f;
			
		}
		for (int l22 = 0; (l22 < 3); l22 = (l22 + 1)) {
			fRec22[l22] = 0.0f;
			
		}
		for (int l23 = 0; (l23 < 2); l23 = (l23 + 1)) {
			fRec41[l23] = 0.0f;
			
		}
		for (int l24 = 0; (l24 < 2); l24 = (l24 + 1)) {
			fRec39[l24] = 0.0f;
			
		}
		for (int l25 = 0; (l25 < 2); l25 = (l25 + 1)) {
			fRec38[l25] = 0.0f;
			
		}
		for (int l26 = 0; (l26 < 2); l26 = (l26 + 1)) {
			fRec36[l26] = 0.0f;
			
		}
		for (int l27 = 0; (l27 < 2); l27 = (l27 + 1)) {
			fRec35[l27] = 0.0f;
			
		}
		for (int l28 = 0; (l28 < 2); l28 = (l28 + 1)) {
			fRec33[l28] = 0.0f;
			
		}
		for (int l29 = 0; (l29 < 3); l29 = (l29 + 1)) {
			fRec32[l29] = 0.0f;
			
		}
		for (int l30 = 0; (l30 < 2); l30 = (l30 + 1)) {
			fRec48[l30] = 0.0f;
			
		}
		for (int l31 = 0; (l31 < 2); l31 = (l31 + 1)) {
			fRec46[l31] = 0.0f;
			
		}
		for (int l32 = 0; (l32 < 2); l32 = (l32 + 1)) {
			fRec45[l32] = 0.0f;
			
		}
		for (int l33 = 0; (l33 < 2); l33 = (l33 + 1)) {
			fRec43[l33] = 0.0f;
			
		}
		for (int l34 = 0; (l34 < 3); l34 = (l34 + 1)) {
			fRec42[l34] = 0.0f;
			
		}
		for (int l35 = 0; (l35 < 2); l35 = (l35 + 1)) {
			fRec55[l35] = 0.0f;
			
		}
		for (int l36 = 0; (l36 < 2); l36 = (l36 + 1)) {
			fRec53[l36] = 0.0f;
			
		}
		for (int l37 = 0; (l37 < 2); l37 = (l37 + 1)) {
			fRec52[l37] = 0.0f;
			
		}
		for (int l38 = 0; (l38 < 2); l38 = (l38 + 1)) {
			fRec50[l38] = 0.0f;
			
		}
		for (int l39 = 0; (l39 < 3); l39 = (l39 + 1)) {
			fRec49[l39] = 0.0f;
			
		}
		for (int l40 = 0; (l40 < 2); l40 = (l40 + 1)) {
			fRec59[l40] = 0.0f;
			
		}
		for (int l41 = 0; (l41 < 2); l41 = (l41 + 1)) {
			fRec57[l41] = 0.0f;
			
		}
		for (int l42 = 0; (l42 < 3); l42 = (l42 + 1)) {
			fRec56[l42] = 0.0f;
			
		}
		for (int l43 = 0; (l43 < 3); l43 = (l43 + 1)) {
			fRec60[l43] = 0.0f;
			
		}
		for (int l44 = 0; (l44 < 2); l44 = (l44 + 1)) {
			fRec64[l44] = 0.0f;
			
		}
		for (int l45 = 0; (l45 < 2); l45 = (l45 + 1)) {
			fRec62[l45] = 0.0f;
			
		}
		for (int l46 = 0; (l46 < 3); l46 = (l46 + 1)) {
			fRec61[l46] = 0.0f;
			
		}
		for (int l47 = 0; (l47 < 2); l47 = (l47 + 1)) {
			fRec71[l47] = 0.0f;
			
		}
		for (int l48 = 0; (l48 < 2); l48 = (l48 + 1)) {
			fRec69[l48] = 0.0f;
			
		}
		for (int l49 = 0; (l49 < 2); l49 = (l49 + 1)) {
			fRec68[l49] = 0.0f;
			
		}
		for (int l50 = 0; (l50 < 2); l50 = (l50 + 1)) {
			fRec66[l50] = 0.0f;
			
		}
		for (int l51 = 0; (l51 < 3); l51 = (l51 + 1)) {
			fRec65[l51] = 0.0f;
			
		}
		for (int l52 = 0; (l52 < 2); l52 = (l52 + 1)) {
			fRec81[l52] = 0.0f;
			
		}
		for (int l53 = 0; (l53 < 2); l53 = (l53 + 1)) {
			fRec79[l53] = 0.0f;
			
		}
		for (int l54 = 0; (l54 < 2); l54 = (l54 + 1)) {
			fRec78[l54] = 0.0f;
			
		}
		for (int l55 = 0; (l55 < 2); l55 = (l55 + 1)) {
			fRec76[l55] = 0.0f;
			
		}
		for (int l56 = 0; (l56 < 2); l56 = (l56 + 1)) {
			fRec75[l56] = 0.0f;
			
		}
		for (int l57 = 0; (l57 < 2); l57 = (l57 + 1)) {
			fRec73[l57] = 0.0f;
			
		}
		for (int l58 = 0; (l58 < 3); l58 = (l58 + 1)) {
			fRec72[l58] = 0.0f;
			
		}
		for (int l59 = 0; (l59 < 2); l59 = (l59 + 1)) {
			fRec91[l59] = 0.0f;
			
		}
		for (int l60 = 0; (l60 < 2); l60 = (l60 + 1)) {
			fRec89[l60] = 0.0f;
			
		}
		for (int l61 = 0; (l61 < 2); l61 = (l61 + 1)) {
			fRec88[l61] = 0.0f;
			
		}
		for (int l62 = 0; (l62 < 2); l62 = (l62 + 1)) {
			fRec86[l62] = 0.0f;
			
		}
		for (int l63 = 0; (l63 < 2); l63 = (l63 + 1)) {
			fRec85[l63] = 0.0f;
			
		}
		for (int l64 = 0; (l64 < 2); l64 = (l64 + 1)) {
			fRec83[l64] = 0.0f;
			
		}
		for (int l65 = 0; (l65 < 3); l65 = (l65 + 1)) {
			fRec82[l65] = 0.0f;
			
		}
		for (int l66 = 0; (l66 < 2); l66 = (l66 + 1)) {
			fRec98[l66] = 0.0f;
			
		}
		for (int l67 = 0; (l67 < 2); l67 = (l67 + 1)) {
			fRec96[l67] = 0.0f;
			
		}
		for (int l68 = 0; (l68 < 2); l68 = (l68 + 1)) {
			fRec95[l68] = 0.0f;
			
		}
		for (int l69 = 0; (l69 < 2); l69 = (l69 + 1)) {
			fRec93[l69] = 0.0f;
			
		}
		for (int l70 = 0; (l70 < 3); l70 = (l70 + 1)) {
			fRec92[l70] = 0.0f;
			
		}
		for (int l71 = 0; (l71 < 2); l71 = (l71 + 1)) {
			fRec102[l71] = 0.0f;
			
		}
		for (int l72 = 0; (l72 < 2); l72 = (l72 + 1)) {
			fRec100[l72] = 0.0f;
			
		}
		for (int l73 = 0; (l73 < 3); l73 = (l73 + 1)) {
			fRec99[l73] = 0.0f;
			
		}
		for (int l74 = 0; (l74 < 2); l74 = (l74 + 1)) {
			fRec109[l74] = 0.0f;
			
		}
		for (int l75 = 0; (l75 < 2); l75 = (l75 + 1)) {
			fRec107[l75] = 0.0f;
			
		}
		for (int l76 = 0; (l76 < 2); l76 = (l76 + 1)) {
			fRec106[l76] = 0.0f;
			
		}
		for (int l77 = 0; (l77 < 2); l77 = (l77 + 1)) {
			fRec104[l77] = 0.0f;
			
		}
		for (int l78 = 0; (l78 < 3); l78 = (l78 + 1)) {
			fRec103[l78] = 0.0f;
			
		}
		for (int l79 = 0; (l79 < 2); l79 = (l79 + 1)) {
			fRec119[l79] = 0.0f;
			
		}
		for (int l80 = 0; (l80 < 2); l80 = (l80 + 1)) {
			fRec117[l80] = 0.0f;
			
		}
		for (int l81 = 0; (l81 < 2); l81 = (l81 + 1)) {
			fRec116[l81] = 0.0f;
			
		}
		for (int l82 = 0; (l82 < 2); l82 = (l82 + 1)) {
			fRec114[l82] = 0.0f;
			
		}
		for (int l83 = 0; (l83 < 2); l83 = (l83 + 1)) {
			fRec113[l83] = 0.0f;
			
		}
		for (int l84 = 0; (l84 < 2); l84 = (l84 + 1)) {
			fRec111[l84] = 0.0f;
			
		}
		for (int l85 = 0; (l85 < 3); l85 = (l85 + 1)) {
			fRec110[l85] = 0.0f;
			
		}
		for (int l86 = 0; (l86 < 2); l86 = (l86 + 1)) {
			fRec120[l86] = 0.0f;
			
		}
		for (int l87 = 0; (l87 < 12); l87 = (l87 + 1)) {
			fVec0[l87] = 0.0f;
			
		}
		for (int l88 = 0; (l88 < 13); l88 = (l88 + 1)) {
			fVec1[l88] = 0.0f;
			
		}
		for (int l89 = 0; (l89 < 12); l89 = (l89 + 1)) {
			fVec2[l89] = 0.0f;
			
		}
		for (int l90 = 0; (l90 < 12); l90 = (l90 + 1)) {
			fVec3[l90] = 0.0f;
			
		}
		for (int l91 = 0; (l91 < 13); l91 = (l91 + 1)) {
			fVec4[l91] = 0.0f;
			
		}
		for (int l92 = 0; (l92 < 12); l92 = (l92 + 1)) {
			fVec5[l92] = 0.0f;
			
		}
		IOTA = 0;
		for (int l93 = 0; (l93 < 32); l93 = (l93 + 1)) {
			fVec6[l93] = 0.0f;
			
		}
		for (int l94 = 0; (l94 < 32); l94 = (l94 + 1)) {
			fVec7[l94] = 0.0f;
			
		}
		for (int l95 = 0; (l95 < 32); l95 = (l95 + 1)) {
			fVec8[l95] = 0.0f;
			
		}
		for (int l96 = 0; (l96 < 32); l96 = (l96 + 1)) {
			fVec9[l96] = 0.0f;
			
		}
		for (int l97 = 0; (l97 < 32); l97 = (l97 + 1)) {
			fVec10[l97] = 0.0f;
			
		}
		for (int l98 = 0; (l98 < 32); l98 = (l98 + 1)) {
			fVec11[l98] = 0.0f;
			
		}
		for (int l99 = 0; (l99 < 32); l99 = (l99 + 1)) {
			fVec12[l99] = 0.0f;
			
		}
		for (int l100 = 0; (l100 < 32); l100 = (l100 + 1)) {
			fVec13[l100] = 0.0f;
			
		}
		for (int l101 = 0; (l101 < 32); l101 = (l101 + 1)) {
			fVec14[l101] = 0.0f;
			
		}
		for (int l102 = 0; (l102 < 32); l102 = (l102 + 1)) {
			fVec15[l102] = 0.0f;
			
		}
		for (int l103 = 0; (l103 < 32); l103 = (l103 + 1)) {
			fVec16[l103] = 0.0f;
			
		}
		for (int l104 = 0; (l104 < 32); l104 = (l104 + 1)) {
			fVec17[l104] = 0.0f;
			
		}
		for (int l105 = 0; (l105 < 64); l105 = (l105 + 1)) {
			fVec18[l105] = 0.0f;
			
		}
		for (int l106 = 0; (l106 < 64); l106 = (l106 + 1)) {
			fVec19[l106] = 0.0f;
			
		}
		for (int l107 = 0; (l107 < 64); l107 = (l107 + 1)) {
			fVec20[l107] = 0.0f;
			
		}
		for (int l108 = 0; (l108 < 64); l108 = (l108 + 1)) {
			fVec21[l108] = 0.0f;
			
		}
		for (int l109 = 0; (l109 < 64); l109 = (l109 + 1)) {
			fVec22[l109] = 0.0f;
			
		}
		for (int l110 = 0; (l110 < 64); l110 = (l110 + 1)) {
			fVec23[l110] = 0.0f;
			
		}
		for (int l111 = 0; (l111 < 64); l111 = (l111 + 1)) {
			fVec24[l111] = 0.0f;
			
		}
		for (int l112 = 0; (l112 < 64); l112 = (l112 + 1)) {
			fVec25[l112] = 0.0f;
			
		}
		for (int l113 = 0; (l113 < 64); l113 = (l113 + 1)) {
			fVec26[l113] = 0.0f;
			
		}
		for (int l114 = 0; (l114 < 64); l114 = (l114 + 1)) {
			fVec27[l114] = 0.0f;
			
		}
		for (int l115 = 0; (l115 < 64); l115 = (l115 + 1)) {
			fVec28[l115] = 0.0f;
			
		}
		for (int l116 = 0; (l116 < 64); l116 = (l116 + 1)) {
			fVec29[l116] = 0.0f;
			
		}
		
	}
	
	virtual void init(int samplingFreq) {
		classInit(samplingFreq);
		instanceInit(samplingFreq);
	}
	virtual void instanceInit(int samplingFreq) {
		instanceConstants(samplingFreq);
		instanceResetUserInterface();
		instanceClear();
	}
	
	virtual mydsp* clone() {
		return new mydsp();
	}
	virtual int getSampleRate() {
		return fSamplingFreq;
		
	}
	
	virtual void buildUserInterface(UI* ui_interface) {
		ui_interface->openVerticalBox("SATOswD3o3");
		ui_interface->declare(&fHslider2, "unit", "dB");
		ui_interface->addHorizontalSlider("gain", &fHslider2, -10.0f, -30.0f, 10.0f, 1.0f);
		ui_interface->declare(&fHslider1, "unit", "dB");
		ui_interface->addHorizontalSlider("lf/hf", &fHslider1, 0.0f, -3.0f, 3.0f, 0.100000001f);
		ui_interface->addCheckButton("mute", &fCheckbox0);
		ui_interface->declare(&fHslider0, "unit", "Hz");
		ui_interface->addHorizontalSlider("xover", &fHslider0, 400.0f, 200.0f, 800.0f, 20.0f);
		ui_interface->closeBox();
		
	}
	
	virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) {
		FAUSTFLOAT* input0 = inputs[0];
		FAUSTFLOAT* input1 = inputs[1];
		FAUSTFLOAT* input2 = inputs[2];
		FAUSTFLOAT* input3 = inputs[3];
		FAUSTFLOAT* input4 = inputs[4];
		FAUSTFLOAT* input5 = inputs[5];
		FAUSTFLOAT* input6 = inputs[6];
		FAUSTFLOAT* input7 = inputs[7];
		FAUSTFLOAT* input8 = inputs[8];
		FAUSTFLOAT* input9 = inputs[9];
		FAUSTFLOAT* input10 = inputs[10];
		FAUSTFLOAT* input11 = inputs[11];
		FAUSTFLOAT* input12 = inputs[12];
		FAUSTFLOAT* input13 = inputs[13];
		FAUSTFLOAT* input14 = inputs[14];
		FAUSTFLOAT* input15 = inputs[15];
		FAUSTFLOAT* output0 = outputs[0];
		FAUSTFLOAT* output1 = outputs[1];
		FAUSTFLOAT* output2 = outputs[2];
		FAUSTFLOAT* output3 = outputs[3];
		FAUSTFLOAT* output4 = outputs[4];
		FAUSTFLOAT* output5 = outputs[5];
		FAUSTFLOAT* output6 = outputs[6];
		FAUSTFLOAT* output7 = outputs[7];
		FAUSTFLOAT* output8 = outputs[8];
		FAUSTFLOAT* output9 = outputs[9];
		FAUSTFLOAT* output10 = outputs[10];
		FAUSTFLOAT* output11 = outputs[11];
		FAUSTFLOAT* output12 = outputs[12];
		FAUSTFLOAT* output13 = outputs[13];
		FAUSTFLOAT* output14 = outputs[14];
		FAUSTFLOAT* output15 = outputs[15];
		FAUSTFLOAT* output16 = outputs[16];
		FAUSTFLOAT* output17 = outputs[17];
		FAUSTFLOAT* output18 = outputs[18];
		FAUSTFLOAT* output19 = outputs[19];
		FAUSTFLOAT* output20 = outputs[20];
		FAUSTFLOAT* output21 = outputs[21];
		FAUSTFLOAT* output22 = outputs[22];
		FAUSTFLOAT* output23 = outputs[23];
		FAUSTFLOAT* output24 = outputs[24];
		FAUSTFLOAT* output25 = outputs[25];
		FAUSTFLOAT* output26 = outputs[26];
		FAUSTFLOAT* output27 = outputs[27];
		FAUSTFLOAT* output28 = outputs[28];
		FAUSTFLOAT* output29 = outputs[29];
		FAUSTFLOAT* output30 = outputs[30];
		float fSlow0 = (0.00100000005f * float(fHslider0));
		float fSlow1 = (0.00100000005f * powf(10.0f, (0.0500000007f * float(fHslider1))));
		float fSlow2 = (0.00100000005f * (powf(10.0f, (0.0500000007f * float(fHslider2))) * float((float(fCheckbox0) < 0.5f))));
		for (int i = 0; (i < count); i = (i + 1)) {
			fRec0[0] = (fSlow0 + (0.999000013f * fRec0[1]));
			float fTemp0 = tanf((fConst1 * fRec0[0]));
			float fTemp1 = mydsp_faustpower2_f(fTemp0);
			float fTemp2 = (fConst4 * fRec2[1]);
			float fTemp3 = (fConst6 * float(input15[i]));
			float fTemp4 = (fConst8 * fRec5[1]);
			float fTemp5 = (fConst9 * fRec8[1]);
			fRec10[0] = (fTemp3 + (fTemp4 + (fRec10[1] + fTemp5)));
			fRec8[0] = fRec10[0];
			float fRec9 = ((fTemp5 + fTemp4) + fTemp3);
			fRec7[0] = (fRec7[1] + fRec8[0]);
			fRec5[0] = fRec7[0];
			float fRec6 = fRec9;
			fRec4[0] = ((fRec4[1] + fTemp2) + fRec6);
			fRec2[0] = fRec4[0];
			float fRec3 = (fTemp2 + fRec6);
			float fTemp6 = ((fTemp0 * (fTemp0 + -2.0f)) + 1.0f);
			float fTemp7 = (fTemp1 + -1.0f);
			float fTemp8 = ((fTemp0 * (fTemp0 + 2.0f)) + 1.0f);
			fRec1[0] = (fRec3 - (((fTemp6 * fRec1[2]) + (2.0f * (fTemp7 * fRec1[1]))) / fTemp8));
			fRec11[0] = (fSlow1 + (0.999000013f * fRec11[1]));
			float fTemp9 = (fRec11[0] * fTemp8);
			float fTemp10 = (0.0f - (2.0f / fTemp8));
			float fTemp11 = (((fTemp1 * (fRec1[2] + ((2.0f * fRec1[1]) + fRec1[0]))) / fTemp9) + (0.304746985f * (fRec11[0] * (0.0f - ((fTemp10 * fRec1[1]) + ((fRec1[2] + fRec1[0]) / fTemp8))))));
			float fTemp12 = (fConst4 * fRec13[1]);
			float fTemp13 = (fConst6 * float(input13[i]));
			float fTemp14 = (fConst8 * fRec16[1]);
			float fTemp15 = (fConst9 * fRec19[1]);
			fRec21[0] = (fTemp13 + (fTemp14 + (fRec21[1] + fTemp15)));
			fRec19[0] = fRec21[0];
			float fRec20 = ((fTemp15 + fTemp14) + fTemp13);
			fRec18[0] = (fRec18[1] + fRec19[0]);
			fRec16[0] = fRec18[0];
			float fRec17 = fRec20;
			fRec15[0] = ((fRec15[1] + fTemp12) + fRec17);
			fRec13[0] = fRec15[0];
			float fRec14 = (fTemp12 + fRec17);
			fRec12[0] = (fRec14 - (((fTemp6 * fRec12[2]) + (2.0f * (fTemp7 * fRec12[1]))) / fTemp8));
			float fTemp16 = (((fTemp1 * (fRec12[2] + ((2.0f * fRec12[1]) + fRec12[0]))) / fTemp9) + (0.304746985f * (fRec11[0] * (0.0f - ((fTemp10 * fRec12[1]) + ((fRec12[2] + fRec12[0]) / fTemp8))))));
			float fTemp17 = (fConst4 * fRec23[1]);
			float fTemp18 = (fConst6 * float(input12[i]));
			float fTemp19 = (fConst8 * fRec26[1]);
			float fTemp20 = (fConst9 * fRec29[1]);
			fRec31[0] = (fTemp18 + (fTemp19 + (fRec31[1] + fTemp20)));
			fRec29[0] = fRec31[0];
			float fRec30 = ((fTemp20 + fTemp19) + fTemp18);
			fRec28[0] = (fRec28[1] + fRec29[0]);
			fRec26[0] = fRec28[0];
			float fRec27 = fRec30;
			fRec25[0] = ((fRec25[1] + fTemp17) + fRec27);
			fRec23[0] = fRec25[0];
			float fRec24 = (fTemp17 + fRec27);
			fRec22[0] = (fRec24 - (((fTemp6 * fRec22[2]) + (2.0f * (fTemp7 * fRec22[1]))) / fTemp8));
			float fTemp21 = (((fTemp1 * (fRec22[2] + ((2.0f * fRec22[1]) + fRec22[0]))) / fTemp9) + (0.304746985f * (fRec11[0] * (0.0f - ((fTemp10 * fRec22[1]) + ((fRec22[2] + fRec22[0]) / fTemp8))))));
			float fTemp22 = (fConst4 * fRec33[1]);
			float fTemp23 = (fConst6 * float(input10[i]));
			float fTemp24 = (fConst8 * fRec36[1]);
			float fTemp25 = (fConst9 * fRec39[1]);
			fRec41[0] = (fTemp23 + (fTemp24 + (fRec41[1] + fTemp25)));
			fRec39[0] = fRec41[0];
			float fRec40 = ((fTemp25 + fTemp24) + fTemp23);
			fRec38[0] = (fRec38[1] + fRec39[0]);
			fRec36[0] = fRec38[0];
			float fRec37 = fRec40;
			fRec35[0] = ((fRec35[1] + fTemp22) + fRec37);
			fRec33[0] = fRec35[0];
			float fRec34 = (fTemp22 + fRec37);
			fRec32[0] = (fRec34 - (((fTemp6 * fRec32[2]) + (2.0f * (fTemp7 * fRec32[1]))) / fTemp8));
			float fTemp26 = (((fTemp1 * (fRec32[2] + ((2.0f * fRec32[1]) + fRec32[0]))) / fTemp9) + (0.304746985f * (fRec11[0] * (0.0f - ((fTemp10 * fRec32[1]) + ((fRec32[2] + fRec32[0]) / fTemp8))))));
			float fTemp27 = (fConst11 * float(input7[i]));
			float fTemp28 = (fConst12 * fRec43[1]);
			float fTemp29 = (fConst13 * fRec46[1]);
			fRec48[0] = (fTemp27 + (fTemp28 + (fRec48[1] + fTemp29)));
			fRec46[0] = fRec48[0];
			float fRec47 = ((fTemp29 + fTemp28) + fTemp27);
			fRec45[0] = (fRec46[0] + fRec45[1]);
			fRec43[0] = fRec45[0];
			float fRec44 = fRec47;
			fRec42[0] = (fRec44 - (((fTemp6 * fRec42[2]) + (2.0f * (fTemp7 * fRec42[1]))) / fTemp8));
			float fTemp30 = (((fTemp1 * (fRec42[2] + (fRec42[0] + (2.0f * fRec42[1])))) / fTemp9) + (0.612333596f * (fRec11[0] * (0.0f - ((fTemp10 * fRec42[1]) + ((fRec42[0] + fRec42[2]) / fTemp8))))));
			float fTemp31 = (fConst11 * float(input6[i]));
			float fTemp32 = (fConst12 * fRec50[1]);
			float fTemp33 = (fConst13 * fRec53[1]);
			fRec55[0] = (fTemp31 + (fTemp32 + (fRec55[1] + fTemp33)));
			fRec53[0] = fRec55[0];
			float fRec54 = ((fTemp33 + fTemp32) + fTemp31);
			fRec52[0] = (fRec53[0] + fRec52[1]);
			fRec50[0] = fRec52[0];
			float fRec51 = fRec54;
			fRec49[0] = (fRec51 - (((fTemp6 * fRec49[2]) + (2.0f * (fTemp7 * fRec49[1]))) / fTemp8));
			float fTemp34 = (((fTemp1 * (fRec49[2] + (fRec49[0] + (2.0f * fRec49[1])))) / fTemp9) + (0.612333596f * (fRec11[0] * (0.0f - ((fTemp10 * fRec49[1]) + ((fRec49[0] + fRec49[2]) / fTemp8))))));
			float fTemp35 = (fConst15 * float(input3[i]));
			float fTemp36 = (fConst16 * fRec57[1]);
			fRec59[0] = (fTemp35 + (fRec59[1] + fTemp36));
			fRec57[0] = fRec59[0];
			float fRec58 = (fTemp36 + fTemp35);
			fRec56[0] = (fRec58 - (((fTemp6 * fRec56[2]) + (2.0f * (fTemp7 * fRec56[1]))) / fTemp8));
			float fTemp37 = (((fTemp1 * (fRec56[2] + (fRec56[0] + (2.0f * fRec56[1])))) / fTemp9) + (0.861136317f * (fRec11[0] * (0.0f - ((fTemp10 * fRec56[1]) + ((fRec56[0] + fRec56[2]) / fTemp8))))));
			fRec60[0] = (float(input0[i]) - (((fRec60[2] * fTemp6) + (2.0f * (fRec60[1] * fTemp7))) / fTemp8));
			float fTemp38 = (((fTemp1 * (fRec60[2] + (fRec60[0] + (2.0f * fRec60[1])))) / fTemp9) + (fRec11[0] * (0.0f - ((fRec60[1] * fTemp10) + ((fRec60[0] + fRec60[2]) / fTemp8)))));
			float fTemp39 = (fConst15 * float(input2[i]));
			float fTemp40 = (fConst16 * fRec62[1]);
			fRec64[0] = (fTemp39 + (fRec64[1] + fTemp40));
			fRec62[0] = fRec64[0];
			float fRec63 = (fTemp40 + fTemp39);
			fRec61[0] = (fRec63 - (((fTemp6 * fRec61[2]) + (2.0f * (fTemp7 * fRec61[1]))) / fTemp8));
			float fTemp41 = (((fTemp1 * (fRec61[2] + (fRec61[0] + (2.0f * fRec61[1])))) / fTemp9) + (0.861136317f * (fRec11[0] * (0.0f - ((fTemp10 * fRec61[1]) + ((fRec61[0] + fRec61[2]) / fTemp8))))));
			float fTemp42 = (fConst11 * float(input4[i]));
			float fTemp43 = (fConst12 * fRec66[1]);
			float fTemp44 = (fConst13 * fRec69[1]);
			fRec71[0] = (fTemp42 + (fTemp43 + (fRec71[1] + fTemp44)));
			fRec69[0] = fRec71[0];
			float fRec70 = ((fTemp44 + fTemp43) + fTemp42);
			fRec68[0] = (fRec69[0] + fRec68[1]);
			fRec66[0] = fRec68[0];
			float fRec67 = fRec70;
			fRec65[0] = (fRec67 - (((fTemp6 * fRec65[2]) + (2.0f * (fTemp7 * fRec65[1]))) / fTemp8));
			float fTemp45 = (((fTemp1 * (fRec65[2] + (fRec65[0] + (2.0f * fRec65[1])))) / fTemp9) + (0.612333596f * (fRec11[0] * (0.0f - ((fTemp10 * fRec65[1]) + ((fRec65[0] + fRec65[2]) / fTemp8))))));
			float fTemp46 = (fConst4 * fRec73[1]);
			float fTemp47 = (fConst6 * float(input9[i]));
			float fTemp48 = (fConst8 * fRec76[1]);
			float fTemp49 = (fConst9 * fRec79[1]);
			fRec81[0] = (fTemp47 + (fTemp48 + (fRec81[1] + fTemp49)));
			fRec79[0] = fRec81[0];
			float fRec80 = ((fTemp49 + fTemp48) + fTemp47);
			fRec78[0] = (fRec78[1] + fRec79[0]);
			fRec76[0] = fRec78[0];
			float fRec77 = fRec80;
			fRec75[0] = ((fRec75[1] + fTemp46) + fRec77);
			fRec73[0] = fRec75[0];
			float fRec74 = (fTemp46 + fRec77);
			fRec72[0] = (fRec74 - (((fTemp6 * fRec72[2]) + (2.0f * (fTemp7 * fRec72[1]))) / fTemp8));
			float fTemp50 = (((fTemp1 * (fRec72[2] + ((2.0f * fRec72[1]) + fRec72[0]))) / fTemp9) + (0.304746985f * (fRec11[0] * (0.0f - ((fTemp10 * fRec72[1]) + ((fRec72[2] + fRec72[0]) / fTemp8))))));
			float fTemp51 = (fConst4 * fRec83[1]);
			float fTemp52 = (fConst6 * float(input14[i]));
			float fTemp53 = (fConst8 * fRec86[1]);
			float fTemp54 = (fConst9 * fRec89[1]);
			fRec91[0] = (fTemp52 + (fTemp53 + (fRec91[1] + fTemp54)));
			fRec89[0] = fRec91[0];
			float fRec90 = ((fTemp54 + fTemp53) + fTemp52);
			fRec88[0] = (fRec88[1] + fRec89[0]);
			fRec86[0] = fRec88[0];
			float fRec87 = fRec90;
			fRec85[0] = ((fRec85[1] + fTemp51) + fRec87);
			fRec83[0] = fRec85[0];
			float fRec84 = (fTemp51 + fRec87);
			fRec82[0] = (fRec84 - (((fTemp6 * fRec82[2]) + (2.0f * (fTemp7 * fRec82[1]))) / fTemp8));
			float fTemp55 = (((fTemp1 * (fRec82[2] + ((2.0f * fRec82[1]) + fRec82[0]))) / fTemp9) + (0.304746985f * (fRec11[0] * (0.0f - ((fTemp10 * fRec82[1]) + ((fRec82[2] + fRec82[0]) / fTemp8))))));
			float fTemp56 = (fConst11 * float(input8[i]));
			float fTemp57 = (fConst12 * fRec93[1]);
			float fTemp58 = (fConst13 * fRec96[1]);
			fRec98[0] = (fTemp56 + (fTemp57 + (fRec98[1] + fTemp58)));
			fRec96[0] = fRec98[0];
			float fRec97 = ((fTemp58 + fTemp57) + fTemp56);
			fRec95[0] = (fRec96[0] + fRec95[1]);
			fRec93[0] = fRec95[0];
			float fRec94 = fRec97;
			fRec92[0] = (fRec94 - (((fTemp6 * fRec92[2]) + (2.0f * (fTemp7 * fRec92[1]))) / fTemp8));
			float fTemp59 = (((fTemp1 * (fRec92[2] + (fRec92[0] + (2.0f * fRec92[1])))) / fTemp9) + (0.612333596f * (fRec11[0] * (0.0f - ((fTemp10 * fRec92[1]) + ((fRec92[0] + fRec92[2]) / fTemp8))))));
			float fTemp60 = (fConst15 * float(input1[i]));
			float fTemp61 = (fConst16 * fRec100[1]);
			fRec102[0] = (fTemp60 + (fRec102[1] + fTemp61));
			fRec100[0] = fRec102[0];
			float fRec101 = (fTemp61 + fTemp60);
			fRec99[0] = (fRec101 - (((fTemp6 * fRec99[2]) + (2.0f * (fTemp7 * fRec99[1]))) / fTemp8));
			float fTemp62 = (((fTemp1 * (fRec99[2] + (fRec99[0] + (2.0f * fRec99[1])))) / fTemp9) + (0.861136317f * (fRec11[0] * (0.0f - ((fTemp10 * fRec99[1]) + ((fRec99[0] + fRec99[2]) / fTemp8))))));
			float fTemp63 = (fConst11 * float(input5[i]));
			float fTemp64 = (fConst12 * fRec104[1]);
			float fTemp65 = (fConst13 * fRec107[1]);
			fRec109[0] = (fTemp63 + (fTemp64 + (fRec109[1] + fTemp65)));
			fRec107[0] = fRec109[0];
			float fRec108 = ((fTemp65 + fTemp64) + fTemp63);
			fRec106[0] = (fRec107[0] + fRec106[1]);
			fRec104[0] = fRec106[0];
			float fRec105 = fRec108;
			fRec103[0] = (fRec105 - (((fTemp6 * fRec103[2]) + (2.0f * (fTemp7 * fRec103[1]))) / fTemp8));
			float fTemp66 = (((fTemp1 * (fRec103[2] + (fRec103[0] + (2.0f * fRec103[1])))) / fTemp9) + (0.612333596f * (fRec11[0] * (0.0f - ((fTemp10 * fRec103[1]) + ((fRec103[0] + fRec103[2]) / fTemp8))))));
			float fTemp67 = (fConst4 * fRec111[1]);
			float fTemp68 = (fConst6 * float(input11[i]));
			float fTemp69 = (fConst8 * fRec114[1]);
			float fTemp70 = (fConst9 * fRec117[1]);
			fRec119[0] = (fTemp68 + (fTemp69 + (fRec119[1] + fTemp70)));
			fRec117[0] = fRec119[0];
			float fRec118 = ((fTemp70 + fTemp69) + fTemp68);
			fRec116[0] = (fRec116[1] + fRec117[0]);
			fRec114[0] = fRec116[0];
			float fRec115 = fRec118;
			fRec113[0] = ((fRec113[1] + fTemp67) + fRec115);
			fRec111[0] = fRec113[0];
			float fRec112 = (fTemp67 + fRec115);
			fRec110[0] = (fRec112 - (((fTemp6 * fRec110[2]) + (2.0f * (fTemp7 * fRec110[1]))) / fTemp8));
			float fTemp71 = (((fTemp1 * (fRec110[2] + ((2.0f * fRec110[1]) + fRec110[0]))) / fTemp9) + (0.304746985f * (fRec11[0] * (0.0f - ((fTemp10 * fRec110[1]) + ((fRec110[2] + fRec110[0]) / fTemp8))))));
			fRec120[0] = (fSlow2 + (0.999000013f * fRec120[1]));
			output0[i] = FAUSTFLOAT(((((1.14039995e-06f * fTemp11) + ((5.3568001e-06f * fTemp16) + ((0.0843499899f * fTemp21) + ((8.52680023e-06f * fTemp26) + (((4.46969989e-06f * fTemp30) + ((0.0872831121f * fTemp34) + (((2.45000001e-06f * fTemp37) + ((0.0473226868f * fTemp38) + (0.0769467279f * fTemp41))) + (3.70409998e-06f * fTemp45)))) + (1.65450001e-06f * fTemp50)))))) - ((3.31060005e-06f * fTemp55) + (((1.50289998e-06f * fTemp59) + ((2.71919998e-06f * fTemp62) + (5.04480022e-06f * fTemp66))) + (6.22569996e-06f * fTemp71)))) * fRec120[0]));
			fVec0[0] = (((0.0175191797f * fTemp16) + ((0.00963862892f * fTemp21) + ((0.0541275442f * fTemp71) + (((0.0176964421f * fTemp30) + ((0.0338490047f * fTemp34) + ((0.0510204248f * fTemp66) + (((0.0108339777f * fTemp37) + ((0.0463922769f * fTemp41) + ((0.0341214538f * fTemp38) + (0.0301853716f * fTemp62)))) + (0.011978806f * fTemp45))))) + (0.0223340727f * fTemp26))))) - ((0.00808212627f * fTemp11) + ((0.026083149f * fTemp55) + ((0.0133990385f * fTemp59) + (0.003537582f * fTemp50)))));
			output1[i] = FAUSTFLOAT((0.998005569f * (fRec120[0] * fVec0[iConst17])));
			fVec1[0] = (((0.00575517025f * fTemp11) + ((0.0069673867f * fTemp21) + ((0.0435375981f * fTemp71) + ((0.00574632082f * fTemp50) + ((0.0352820829f * fTemp34) + (((0.0510143228f * fTemp41) + ((0.038172856f * fTemp38) + (0.0257401336f * fTemp62))) + (0.0426589735f * fTemp66))))))) - ((5.31510022e-06f * fTemp55) + ((0.0435424335f * fTemp16) + (((4.50529978e-06f * fTemp59) + ((0.042658627f * fTemp30) + ((0.0257381666f * fTemp37) + (0.0195993222f * fTemp45)))) + (0.0371481292f * fTemp26)))));
			output2[i] = FAUSTFLOAT((0.997783959f * (fRec120[0] * fVec1[iConst18])));
			fVec2[0] = (((0.0260652341f * fTemp55) + ((0.00964230858f * fTemp21) + (((0.0133855147f * fTemp59) + ((0.0338515826f * fTemp34) + (((0.0341186747f * fTemp38) + (0.0463907383f * fTemp41)) + (0.0119755501f * fTemp45)))) + (0.0223280527f * fTemp26)))) - ((0.00352248852f * fTemp11) + ((0.0541319922f * fTemp16) + ((0.0175154228f * fTemp71) + ((0.00807411969f * fTemp50) + ((0.0510147251f * fTemp30) + (((0.0108332634f * fTemp62) + (0.0301783718f * fTemp37)) + (0.017694287f * fTemp66))))))));
			output3[i] = FAUSTFLOAT((0.998005569f * (fRec120[0] * fVec2[iConst17])));
			fVec3[0] = (((0.00586031703f * fTemp11) + ((0.0123329759f * fTemp21) + ((0.015175622f * fTemp26) + ((0.00585178612f * fTemp50) + ((0.0324303731f * fTemp34) + (((0.0300636496f * fTemp38) + (0.0417693704f * fTemp41)) + (0.00778828422f * fTemp45))))))) - ((0.0262655504f * fTemp55) + ((0.0134790875f * fTemp16) + (((0.0134784635f * fTemp59) + ((0.012267625f * fTemp30) + (((0.0265663434f * fTemp62) + (0.0071219611f * fTemp37)) + (0.0457643978f * fTemp66)))) + (0.0502912141f * fTemp71)))));
			output4[i] = FAUSTFLOAT((0.998005569f * (fRec120[0] * fVec3[iConst17])));
			fVec4[0] = (((1.47399999e-06f * fTemp55) + ((0.0435460955f * fTemp16) + (((4.58500011e-07f * fTemp59) + ((0.0426651761f * fTemp30) + (((0.0257430729f * fTemp37) + ((0.0381806493f * fTemp38) + (0.0510256663f * fTemp41))) + (0.0352924652f * fTemp34)))) + (0.00697506964f * fTemp21)))) - ((0.00575058721f * fTemp11) + ((0.0435448177f * fTemp71) + ((0.037147373f * fTemp26) + ((0.00574612245f * fTemp50) + ((0.0426623151f * fTemp66) + ((0.0257410146f * fTemp62) + (0.019598661f * fTemp45))))))));
			output5[i] = FAUSTFLOAT((0.997783959f * (fRec120[0] * fVec4[iConst18])));
			fVec5[0] = ((0.00585071836f * fTemp11) + ((0.0262656976f * fTemp55) + ((0.0502907373f * fTemp16) + ((0.0123249413f * fTemp21) + ((0.0134819802f * fTemp71) + ((0.0151786013f * fTemp26) + ((0.00585804973f * fTemp50) + ((0.0134779755f * fTemp59) + ((0.0457637608f * fTemp30) + ((0.0324229002f * fTemp34) + ((0.0122708166f * fTemp66) + (((0.0265659001f * fTemp37) + ((0.0417632945f * fTemp41) + ((0.0300600771f * fTemp38) + (0.00712388381f * fTemp62)))) + (0.00778933102f * fTemp45)))))))))))));
			output6[i] = FAUSTFLOAT((0.998005569f * (fRec120[0] * fVec5[iConst17])));
			fVec6[(IOTA & 31)] = (((0.00227179355f * fTemp11) + ((((0.0200215131f * fTemp41) + ((0.0235311687f * fTemp38) + (0.0338993929f * fTemp62))) + (0.0357975774f * fTemp66)) + (0.00792338047f * fTemp71))) - ((0.0365134403f * fTemp55) + ((0.00170740404f * fTemp16) + ((0.0215631779f * fTemp21) + ((0.00384632917f * fTemp26) + ((0.0261735525f * fTemp50) + ((0.0306473803f * fTemp59) + ((0.00217522983f * fTemp30) + ((0.00514168013f * fTemp34) + ((0.00145658397f * fTemp37) + (0.00219735736f * fTemp45)))))))))));
			output7[i] = FAUSTFLOAT((0.994459808f * (fRec120[0] * fVec6[((IOTA - iConst19) & 31)])));
			fVec7[(IOTA & 31)] = (((0.0393768735f * fTemp11) + ((0.00663555553f * fTemp16) + ((0.00086466549f * fTemp50) + (((0.0179317892f * fTemp41) + ((0.0306961462f * fTemp38) + (0.0410687849f * fTemp62))) + (0.0297248401f * fTemp66))))) - ((0.0179478563f * fTemp55) + ((0.0266766511f * fTemp21) + ((0.0122424858f * fTemp71) + (((0.0220683031f * fTemp59) + ((0.0178549215f * fTemp30) + ((0.0192471165f * fTemp34) + ((0.0242131073f * fTemp37) + (0.0396838821f * fTemp45))))) + (0.0334640145f * fTemp26))))));
			output8[i] = FAUSTFLOAT((0.994903028f * (fRec120[0] * fVec7[((IOTA - iConst20) & 31)])));
			fVec8[(IOTA & 31)] = (((0.0181465168f * fTemp55) + ((0.00707933772f * fTemp16) + ((0.0313078016f * fTemp50) + ((0.0222139508f * fTemp59) + (((0.0157211255f * fTemp41) + ((0.0242891777f * fTemp38) + (0.0165882278f * fTemp62))) + (0.0144822551f * fTemp66)))))) - ((0.00749638118f * fTemp11) + ((0.0223583821f * fTemp21) + ((0.00146551407f * fTemp71) + (((0.0264675654f * fTemp30) + ((0.0132820914f * fTemp34) + ((0.0335474797f * fTemp37) + (0.0282651987f * fTemp45)))) + (0.02764659f * fTemp26))))));
			output9[i] = FAUSTFLOAT((0.994903028f * (fRec120[0] * fVec8[((IOTA - iConst20) & 31)])));
			fVec9[(IOTA & 31)] = (((0.0365036465f * fTemp55) + ((0.00170157896f * fTemp71) + ((0.0022798446f * fTemp50) + ((0.0306251757f * fTemp59) + (((0.0200138334f * fTemp41) + ((0.0235146843f * fTemp38) + (0.00145928503f * fTemp62))) + (0.00217607711f * fTemp66)))))) - ((0.0261559151f * fTemp11) + ((0.00794078596f * fTemp16) + ((0.0215517879f * fTemp21) + (((0.0357850008f * fTemp30) + ((0.00512850937f * fTemp34) + ((0.0338744335f * fTemp37) + (0.00220371434f * fTemp45)))) + (0.00385212409f * fTemp26))))));
			output10[i] = FAUSTFLOAT((0.994459808f * (fRec120[0] * fVec9[((IOTA - iConst19) & 31)])));
			fVec10[(IOTA & 31)] = (((0.0201868787f * fTemp55) + ((0.00671209022f * fTemp16) + ((0.00212324364f * fTemp71) + (((0.0234601125f * fTemp59) + (((0.0242880136f * fTemp38) + (0.0157179944f * fTemp41)) + (0.0275476705f * fTemp45))) + (0.0264661219f * fTemp26))))) - ((0.00919907447f * fTemp11) + ((0.0223621186f * fTemp21) + ((0.0313106589f * fTemp50) + ((0.0270602293f * fTemp30) + ((0.0132877538f * fTemp34) + (((0.0158460233f * fTemp62) + (0.0339761786f * fTemp37)) + (0.0134490049f * fTemp66))))))));
			output11[i] = FAUSTFLOAT((0.994903028f * (fVec10[((IOTA - iConst20) & 31)] * fRec120[0])));
			fVec11[(IOTA & 31)] = (((0.0422834121f * fTemp11) + ((0.00162406347f * fTemp16) + ((0.00622504856f * fTemp71) + ((((0.0363172293f * fTemp38) + (0.0244163014f * fTemp41)) + (0.0446210802f * fTemp45)) + (0.0420676731f * fTemp26))))) - ((0.0198335573f * fTemp55) + ((0.03017544f * fTemp21) + ((0.00229224609f * fTemp50) + ((0.0232048221f * fTemp59) + ((0.0242723655f * fTemp30) + ((0.0170691777f * fTemp34) + (((0.0464762598f * fTemp62) + (0.0285261497f * fTemp37)) + (0.0376785174f * fTemp66)))))))));
			output12[i] = FAUSTFLOAT((0.994903028f * (fVec11[((IOTA - iConst20) & 31)] * fRec120[0])));
			fVec12[(IOTA & 31)] = (((0.0017043032f * fTemp16) + ((0.0261591207f * fTemp50) + (((0.00145378441f * fTemp37) + ((0.023518106f * fTemp38) + (0.0200154874f * fTemp41))) + (0.00217205891f * fTemp30)))) - ((0.00226526242f * fTemp11) + ((0.0365033634f * fTemp55) + ((0.0215494651f * fTemp21) + ((0.00793956313f * fTemp71) + (((0.0306292567f * fTemp59) + ((0.0051292195f * fTemp34) + ((0.0357862934f * fTemp66) + ((0.033879105f * fTemp62) + (0.0021929564f * fTemp45))))) + (0.00384289213f * fTemp26)))))));
			output13[i] = FAUSTFLOAT((0.994459808f * (fVec12[((IOTA - iConst19) & 31)] * fRec120[0])));
			fVec13[(IOTA & 31)] = (((((0.0242112409f * fTemp37) + ((0.0307002682f * fTemp38) + (0.0179308224f * fTemp41))) + (0.017853152f * fTemp30)) + (0.0122532919f * fTemp71)) - ((0.0393761285f * fTemp11) + ((0.017946979f * fTemp55) + ((0.00663466984f * fTemp16) + ((0.026677642f * fTemp21) + ((0.0334579162f * fTemp26) + ((0.000849328004f * fTemp50) + ((0.0220802464f * fTemp59) + ((0.019253334f * fTemp34) + ((0.0297232959f * fTemp66) + ((0.0410771593f * fTemp62) + (0.0396812446f * fTemp45))))))))))));
			output14[i] = FAUSTFLOAT((0.994903028f * (fVec13[((IOTA - iConst20) & 31)] * fRec120[0])));
			fVec14[(IOTA & 31)] = (((0.00747441268f * fTemp11) + ((0.018147653f * fTemp55) + (((0.0222150907f * fTemp59) + (((0.0335719511f * fTemp37) + ((0.0243119132f * fTemp38) + (0.015737446f * fTemp41))) + (0.026488414f * fTemp30))) + (0.00146507018f * fTemp71)))) - ((0.00707615074f * fTemp16) + ((0.0223725922f * fTemp21) + ((0.0276765991f * fTemp26) + ((0.0313324779f * fTemp50) + ((0.0132887159f * fTemp34) + ((0.014501893f * fTemp66) + ((0.0166117605f * fTemp62) + (0.0282988027f * fTemp45)))))))));
			output15[i] = FAUSTFLOAT((0.994903028f * (fVec14[((IOTA - iConst20) & 31)] * fRec120[0])));
			fVec15[(IOTA & 31)] = (((0.0261732973f * fTemp11) + ((0.036511451f * fTemp55) + (((0.0306433197f * fTemp59) + (((0.0338919684f * fTemp37) + ((0.0235254578f * fTemp38) + (0.0200174712f * fTemp41))) + (0.0357914977f * fTemp30))) + (0.00792536978f * fTemp16)))) - ((0.0215570088f * fTemp21) + ((0.00170406105f * fTemp71) + ((0.0038598862f * fTemp26) + ((0.00228837901f * fTemp50) + ((0.00513874181f * fTemp34) + ((0.0021808676f * fTemp66) + ((0.00146376574f * fTemp62) + (0.00221101847f * fTemp45)))))))));
			output16[i] = FAUSTFLOAT((0.994459808f * (fVec15[((IOTA - iConst19) & 31)] * fRec120[0])));
			fVec16[(IOTA & 31)] = (((0.0198304765f * fTemp55) + ((0.0420628637f * fTemp26) + ((0.0422858559f * fTemp50) + ((0.0232067332f * fTemp59) + ((0.0376720801f * fTemp30) + ((0.0242657457f * fTemp66) + (((0.0464785807f * fTemp37) + ((0.0244102012f * fTemp41) + ((0.0363168567f * fTemp38) + (0.0285257287f * fTemp62)))) + (0.0446238518f * fTemp45)))))))) - ((0.0022937879f * fTemp11) + ((0.0062296018f * fTemp16) + ((0.0301687401f * fTemp21) + ((0.0170746874f * fTemp34) + (0.00163434329f * fTemp71))))));
			output17[i] = FAUSTFLOAT((0.994903028f * (fRec120[0] * fVec16[((IOTA - iConst20) & 31)])));
			fVec17[(IOTA & 31)] = ((((0.013463459f * fTemp30) + ((0.0270859804f * fTemp66) + (((0.015851954f * fTemp37) + ((0.0157361906f * fTemp41) + ((0.0242996197f * fTemp38) + (0.0339871384f * fTemp62)))) + (0.0275524594f * fTemp45)))) + (0.0264879279f * fTemp26)) - ((0.0313103572f * fTemp11) + ((0.020199718f * fTemp55) + ((0.00211028522f * fTemp16) + ((0.0223753918f * fTemp21) + ((0.00668762578f * fTemp71) + ((0.00920206681f * fTemp50) + ((0.0132781463f * fTemp34) + (0.0234640837f * fTemp59)))))))));
			output18[i] = FAUSTFLOAT((0.994903028f * (fRec120[0] * fVec17[((IOTA - iConst20) & 31)])));
			fVec18[(IOTA & 63)] = (((1.78635e-05f * fTemp11) + ((0.0332020447f * fTemp55) + ((0.0116322571f * fTemp21) + ((((0.0588501506f * fTemp38) + (0.0887992382f * fTemp62)) + (4.19029993e-06f * fTemp30)) + (4.11829978e-06f * fTemp26))))) - ((3.69029999e-06f * fTemp16) + ((0.023990212f * fTemp71) + ((0.0742083937f * fTemp50) + ((0.0842990875f * fTemp59) + ((0.0305124186f * fTemp34) + ((0.0354953706f * fTemp66) + (((0.0229201801f * fTemp41) + (1.02769e-05f * fTemp37)) + (1.63954992e-05f * fTemp45)))))))));
			output19[i] = FAUSTFLOAT((0.990027726f * (fRec120[0] * fVec18[((IOTA - iConst21) & 63)])));
			fVec19[(IOTA & 63)] = (((0.0603009164f * fTemp11) + ((0.0213693716f * fTemp55) + ((0.00611096714f * fTemp16) + ((0.0198027026f * fTemp21) + ((((0.0475860611f * fTemp38) + (0.061254438f * fTemp62)) + (0.022038091f * fTemp30)) + (0.0370234251f * fTemp26)))))) - ((0.0105896434f * fTemp71) + ((1.24281996e-05f * fTemp50) + ((0.0336138234f * fTemp59) + ((0.0203053225f * fTemp34) + ((0.0381666757f * fTemp66) + (((0.0270938333f * fTemp41) + (0.0353607573f * fTemp37)) + (0.0582042076f * fTemp45))))))));
			output20[i] = FAUSTFLOAT((0.990249336f * (fRec120[0] * fVec19[((IOTA - iConst22) & 63)])));
			fVec20[(IOTA & 63)] = (((0.00735984743f * fTemp11) + ((0.0148546649f * fTemp16) + ((0.0156995766f * fTemp21) + ((0.0315297917f * fTemp26) + ((0.0672760159f * fTemp50) + ((0.0334427133f * fTemp59) + (((0.0532584041f * fTemp38) + (0.042079974f * fTemp62)) + (0.0350273214f * fTemp30)))))))) - ((0.0213414878f * fTemp55) + (((0.0254155602f * fTemp34) + ((0.0189149324f * fTemp66) + (((0.0250215139f * fTemp41) + (0.067867741f * fTemp37)) + (0.0682127848f * fTemp45)))) + (0.0104994113f * fTemp71))));
			output21[i] = FAUSTFLOAT((0.990249336f * (fRec120[0] * fVec20[((IOTA - iConst22) & 63)])));
			fVec21[(IOTA & 63)] = (((0.0239910036f * fTemp16) + ((0.0116386283f * fTemp21) + ((1.71399995e-06f * fTemp26) + ((1.76210006e-06f * fTemp50) + ((0.0842888653f * fTemp59) + (((0.0588359646f * fTemp38) + (6.81699987e-07f * fTemp62)) + (0.0354968123f * fTemp30))))))) - ((0.0742064863f * fTemp11) + ((0.0332071297f * fTemp55) + (((0.0305128377f * fTemp34) + ((3.29199992e-07f * fTemp66) + (((0.0229164064f * fTemp41) + (0.0887830555f * fTemp37)) + (1.60189995e-06f * fTemp45)))) + (1.77360005e-06f * fTemp71)))));
			output22[i] = FAUSTFLOAT((0.990027726f * (fRec120[0] * fVec21[((IOTA - iConst21) & 63)])));
			fVec22[(IOTA & 63)] = (((0.00734717352f * fTemp11) + ((0.0148723898f * fTemp16) + ((0.0157222096f * fTemp21) + (((0.0334550142f * fTemp59) + ((0.035022445f * fTemp30) + ((0.0189076494f * fTemp66) + ((0.0532494672f * fTemp38) + (0.0682108104f * fTemp45))))) + (0.0105118724f * fTemp71))))) - ((0.0213441104f * fTemp55) + ((0.0315268077f * fTemp26) + ((0.0672813505f * fTemp50) + ((((0.0420738682f * fTemp62) + (0.0250031725f * fTemp41)) + (0.0678718835f * fTemp37)) + (0.0254423618f * fTemp34))))));
			output23[i] = FAUSTFLOAT((0.990249336f * (fRec120[0] * fVec22[((IOTA - iConst22) & 63)])));
			fVec23[(IOTA & 63)] = (((0.0603186153f * fTemp11) + ((0.0213689711f * fTemp55) + ((0.00610464113f * fTemp16) + ((0.019768076f * fTemp21) + ((0.010592496f * fTemp71) + ((2.35727002e-05f * fTemp50) + ((0.0220359024f * fTemp30) + ((0.0381643772f * fTemp66) + ((0.0476111248f * fTemp38) + (0.0582197383f * fTemp45)))))))))) - (((0.0336324349f * fTemp59) + ((((0.0612792261f * fTemp62) + (0.0271065626f * fTemp41)) + (0.0353708342f * fTemp37)) + (0.0202950761f * fTemp34))) + (0.0370129459f * fTemp26)));
			output24[i] = FAUSTFLOAT((0.990249336f * (fRec120[0] * fVec23[((IOTA - iConst22) & 63)])));
			fVec24[(IOTA & 63)] = (((0.0331940688f * fTemp55) + ((1.66199996e-07f * fTemp16) + ((0.011640396f * fTemp21) + ((0.0240037106f * fTemp71) + ((0.0742251649f * fTemp50) + ((2.9099001e-06f * fTemp30) + (((0.0588502251f * fTemp38) + (4.36999983e-08f * fTemp37)) + (0.0354840085f * fTemp66)))))))) - ((6.38699987e-07f * fTemp11) + (((0.0843136683f * fTemp59) + ((0.0305345897f * fTemp34) + (((0.0888105035f * fTemp62) + (0.0229036063f * fTemp41)) + (4.06400005e-07f * fTemp45)))) + (4.66890015e-06f * fTemp26))));
			output25[i] = FAUSTFLOAT((0.990027726f * (fRec120[0] * fVec24[((IOTA - iConst21) & 63)])));
			fVec25[(IOTA & 63)] = (((0.0213809013f * fTemp55) + ((0.01976908f * fTemp21) + ((0.0105883619f * fTemp71) + ((((0.0476081558f * fTemp38) + (0.0353744999f * fTemp37)) + (0.03817489f * fTemp66)) + (0.0370202139f * fTemp26))))) - ((0.0603082888f * fTemp11) + ((0.00611473899f * fTemp16) + ((5.4594002e-06f * fTemp50) + ((((((0.0612672567f * fTemp62) + (0.0271157604f * fTemp41)) + (0.0582216233f * fTemp45)) + (0.0202873833f * fTemp34)) + (0.022037847f * fTemp30)) + (0.03361056f * fTemp59))))));
			output26[i] = FAUSTFLOAT((0.990249336f * (fRec120[0] * fVec25[((IOTA - iConst22) & 63)])));
			fVec26[(IOTA & 63)] = (((0.0157154445f * fTemp21) + ((0.0105084423f * fTemp71) + (((0.0334601849f * fTemp59) + (((0.0532465205f * fTemp38) + (0.0678689703f * fTemp37)) + (0.0189102255f * fTemp66))) + (0.0315313675f * fTemp26)))) - ((0.00733538205f * fTemp11) + ((0.0213397481f * fTemp55) + ((0.0148645388f * fTemp16) + ((0.0672811568f * fTemp50) + ((0.0350250863f * fTemp30) + ((0.025433531f * fTemp34) + (((0.04206644f * fTemp62) + (0.0250075795f * fTemp41)) + (0.0682025552f * fTemp45)))))))));
			output27[i] = FAUSTFLOAT((0.990249336f * (fRec120[0] * fVec26[((IOTA - iConst22) & 63)])));
			fVec27[(IOTA & 63)] = (((0.07421875f * fTemp11) + ((0.0116383452f * fTemp21) + ((((0.0887977406f * fTemp37) + ((0.0588410161f * fTemp38) + (1.94500004e-07f * fTemp62))) + (0.0843039677f * fTemp59)) + (2.25329995e-06f * fTemp71)))) - ((0.0331902243f * fTemp55) + ((0.0240087509f * fTemp16) + ((7.63729986e-06f * fTemp26) + ((1.10400003e-06f * fTemp50) + ((0.0354756378f * fTemp30) + ((0.0305338241f * fTemp34) + ((4.29940019e-06f * fTemp66) + ((0.0228981767f * fTemp41) + (3.24199988e-07f * fTemp45))))))))));
			output28[i] = FAUSTFLOAT((0.990027726f * (fRec120[0] * fVec27[((IOTA - iConst21) & 63)])));
			fVec28[(IOTA & 63)] = (((2.04370008e-05f * fTemp11) + ((0.0197647121f * fTemp21) + ((0.0603153482f * fTemp50) + ((0.0336267091f * fTemp59) + (((0.0612718165f * fTemp37) + ((0.0476072319f * fTemp38) + (0.0353668891f * fTemp62))) + (0.0582146868f * fTemp45)))))) - ((0.0213693399f * fTemp55) + ((0.0105815213f * fTemp16) + ((0.00610750495f * fTemp71) + (((0.0381675474f * fTemp30) + ((0.0202854425f * fTemp34) + ((0.0271103531f * fTemp41) + (0.0220347978f * fTemp66)))) + (0.0370129235f * fTemp26))))));
			output29[i] = FAUSTFLOAT((0.990249336f * (fRec120[0] * fVec28[((IOTA - iConst22) & 63)])));
			fVec29[(IOTA & 63)] = (((0.0213273577f * fTemp55) + ((0.0157040954f * fTemp21) + ((0.00734153436f * fTemp50) + (((0.042072583f * fTemp37) + ((0.0532519892f * fTemp38) + (0.0678741783f * fTemp62))) + (0.0682138726f * fTemp45))))) - ((0.0672949478f * fTemp11) + ((0.010509165f * fTemp16) + ((0.0148637323f * fTemp71) + (((0.0334611647f * fTemp59) + ((0.0189079586f * fTemp30) + ((0.0254328717f * fTemp34) + ((0.025004046f * fTemp41) + (0.0350133739f * fTemp66))))) + (0.0315223634f * fTemp26))))));
			output30[i] = FAUSTFLOAT((0.990249336f * (fRec120[0] * fVec29[((IOTA - iConst22) & 63)])));
			fRec0[1] = fRec0[0];
			fRec10[1] = fRec10[0];
			fRec8[1] = fRec8[0];
			fRec7[1] = fRec7[0];
			fRec5[1] = fRec5[0];
			fRec4[1] = fRec4[0];
			fRec2[1] = fRec2[0];
			fRec1[2] = fRec1[1];
			fRec1[1] = fRec1[0];
			fRec11[1] = fRec11[0];
			fRec21[1] = fRec21[0];
			fRec19[1] = fRec19[0];
			fRec18[1] = fRec18[0];
			fRec16[1] = fRec16[0];
			fRec15[1] = fRec15[0];
			fRec13[1] = fRec13[0];
			fRec12[2] = fRec12[1];
			fRec12[1] = fRec12[0];
			fRec31[1] = fRec31[0];
			fRec29[1] = fRec29[0];
			fRec28[1] = fRec28[0];
			fRec26[1] = fRec26[0];
			fRec25[1] = fRec25[0];
			fRec23[1] = fRec23[0];
			fRec22[2] = fRec22[1];
			fRec22[1] = fRec22[0];
			fRec41[1] = fRec41[0];
			fRec39[1] = fRec39[0];
			fRec38[1] = fRec38[0];
			fRec36[1] = fRec36[0];
			fRec35[1] = fRec35[0];
			fRec33[1] = fRec33[0];
			fRec32[2] = fRec32[1];
			fRec32[1] = fRec32[0];
			fRec48[1] = fRec48[0];
			fRec46[1] = fRec46[0];
			fRec45[1] = fRec45[0];
			fRec43[1] = fRec43[0];
			fRec42[2] = fRec42[1];
			fRec42[1] = fRec42[0];
			fRec55[1] = fRec55[0];
			fRec53[1] = fRec53[0];
			fRec52[1] = fRec52[0];
			fRec50[1] = fRec50[0];
			fRec49[2] = fRec49[1];
			fRec49[1] = fRec49[0];
			fRec59[1] = fRec59[0];
			fRec57[1] = fRec57[0];
			fRec56[2] = fRec56[1];
			fRec56[1] = fRec56[0];
			fRec60[2] = fRec60[1];
			fRec60[1] = fRec60[0];
			fRec64[1] = fRec64[0];
			fRec62[1] = fRec62[0];
			fRec61[2] = fRec61[1];
			fRec61[1] = fRec61[0];
			fRec71[1] = fRec71[0];
			fRec69[1] = fRec69[0];
			fRec68[1] = fRec68[0];
			fRec66[1] = fRec66[0];
			fRec65[2] = fRec65[1];
			fRec65[1] = fRec65[0];
			fRec81[1] = fRec81[0];
			fRec79[1] = fRec79[0];
			fRec78[1] = fRec78[0];
			fRec76[1] = fRec76[0];
			fRec75[1] = fRec75[0];
			fRec73[1] = fRec73[0];
			fRec72[2] = fRec72[1];
			fRec72[1] = fRec72[0];
			fRec91[1] = fRec91[0];
			fRec89[1] = fRec89[0];
			fRec88[1] = fRec88[0];
			fRec86[1] = fRec86[0];
			fRec85[1] = fRec85[0];
			fRec83[1] = fRec83[0];
			fRec82[2] = fRec82[1];
			fRec82[1] = fRec82[0];
			fRec98[1] = fRec98[0];
			fRec96[1] = fRec96[0];
			fRec95[1] = fRec95[0];
			fRec93[1] = fRec93[0];
			fRec92[2] = fRec92[1];
			fRec92[1] = fRec92[0];
			fRec102[1] = fRec102[0];
			fRec100[1] = fRec100[0];
			fRec99[2] = fRec99[1];
			fRec99[1] = fRec99[0];
			fRec109[1] = fRec109[0];
			fRec107[1] = fRec107[0];
			fRec106[1] = fRec106[0];
			fRec104[1] = fRec104[0];
			fRec103[2] = fRec103[1];
			fRec103[1] = fRec103[0];
			fRec119[1] = fRec119[0];
			fRec117[1] = fRec117[0];
			fRec116[1] = fRec116[0];
			fRec114[1] = fRec114[0];
			fRec113[1] = fRec113[0];
			fRec111[1] = fRec111[0];
			fRec110[2] = fRec110[1];
			fRec110[1] = fRec110[0];
			fRec120[1] = fRec120[0];
			for (int j0 = 11; (j0 > 0); j0 = (j0 - 1)) {
				fVec0[j0] = fVec0[(j0 - 1)];
				
			}
			for (int j1 = 12; (j1 > 0); j1 = (j1 - 1)) {
				fVec1[j1] = fVec1[(j1 - 1)];
				
			}
			for (int j2 = 11; (j2 > 0); j2 = (j2 - 1)) {
				fVec2[j2] = fVec2[(j2 - 1)];
				
			}
			for (int j3 = 11; (j3 > 0); j3 = (j3 - 1)) {
				fVec3[j3] = fVec3[(j3 - 1)];
				
			}
			for (int j4 = 12; (j4 > 0); j4 = (j4 - 1)) {
				fVec4[j4] = fVec4[(j4 - 1)];
				
			}
			for (int j5 = 11; (j5 > 0); j5 = (j5 - 1)) {
				fVec5[j5] = fVec5[(j5 - 1)];
				
			}
			IOTA = (IOTA + 1);
			
		}
		
	}

	
};

//----------------------------------------------------------------------------
// SuperCollider/Faust interface
//----------------------------------------------------------------------------

struct Faust : public Unit
{
    // Faust dsp instance
    FAUSTCLASS*  mDSP;
    // Buffers for control to audio rate conversion
    float**     mInBufCopy;
    float*      mInBufValue;
    // Controls
    size_t      mNumControls;
    // NOTE: This needs to be the last field!
    //
    // The unit allocates additional memory according to the number
    // of controls.
    Control     mControls[0];

    int getNumAudioInputs() { return mDSP->getNumInputs(); }
};

// Global state

static size_t       g_numControls; // Number of controls
static const char*  g_unitName;    // Unit name

// Initialize the global state with unit name and sample rate.
void initState(const std::string& name, int sampleRate);

// Return the unit size in bytes, including static fields and controls.
static size_t unitSize();

// Convert a file name to a valid unit name.
static std::string fileNameToUnitName(const std::string& fileName);

// Convert the XML unit name to a valid class name.
static std::string normalizeClassName(const std::string& name);

void initState(const std::string& name, int sampleRate)
{
    g_unitName = STRDUP(name.c_str());

    mydsp* dsp = new FAUSTCLASS;
    ControlCounter* cc = new ControlCounter;

    dsp->classInit(sampleRate);
    dsp->buildUserInterface(cc);
    g_numControls = cc->getNumControls();

    delete dsp;
    delete cc;
}

size_t unitSize()
{
    return sizeof(Faust) + g_numControls * sizeof(Control);
}

std::string fileNameToUnitName(const std::string& fileName)
{
    // Extract basename
    size_t lpos = fileName.rfind('/', fileName.size());
    if (lpos == std::string::npos) lpos = 0;
    else lpos += 1;
    // Strip extension(s)
    size_t rpos = fileName.find('.', lpos);
    // Return substring
    return fileName.substr(lpos, rpos > lpos ? rpos - lpos : 0);
}

// Globals

static InterfaceTable* ft;

// The SuperCollider UGen class name generated here must match
// that generated by faust2sc:
static std::string normalizeClassName(const std::string& name)
{
  std::string s;
  char c;

  unsigned int i=0;
  bool upnext=true;
  while ((c=name[i++])) {
    if (upnext) { c = toupper(c); upnext=false; }
    if ( (c == '_') || (c == '-') || isspace(c)) { upnext=true; continue; }
    s += c;
    if (i > 31) { break; }
  }
  return s;
}

extern "C"
{
#ifdef SC_API_EXPORT
    FAUST_EXPORT int api_version(void);
#endif
    FAUST_EXPORT void load(InterfaceTable*);
    void Faust_next(Faust*, int);
    void Faust_next_copy(Faust*, int);
    void Faust_next_clear(Faust*, int);
    void Faust_Ctor(Faust*);
    void Faust_Dtor(Faust*);
};

inline static void fillBuffer(float* dst, int n, float v)
{
    Fill(n, dst, v);
}

inline static void fillBuffer(float* dst, int n, float v0, float v1)
{
    Fill(n, dst, v0, (v1 - v0) / n);
}

inline static void copyBuffer(float* dst, int n, float* src)
{
    Copy(n, dst, src);
}

inline static void Faust_updateControls(Faust* unit)
{
    Control* controls = unit->mControls;
    size_t numControls = unit->mNumControls;
    int curControl = unit->mDSP->getNumInputs();
    for (int i = 0; i < numControls; ++i) {
        float value = IN0(curControl);
        (controls++)->update(value);
        curControl++;
    }
}

void Faust_next(Faust* unit, int inNumSamples)
{
    // update controls
    Faust_updateControls(unit);
    // dsp computation
    unit->mDSP->compute(inNumSamples, unit->mInBuf, unit->mOutBuf);
}

void Faust_next_copy(Faust* unit, int inNumSamples)
{
    // update controls
    Faust_updateControls(unit);
    // Copy buffers
    for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
        float* b = unit->mInBufCopy[i];
        if (INRATE(i) == calc_FullRate) {
            // Audio rate: copy buffer
            copyBuffer(b, inNumSamples, unit->mInBuf[i]);
        } else {
            // Control rate: linearly interpolate input
            float v1 = IN0(i);
            fillBuffer(b, inNumSamples, unit->mInBufValue[i], v1);
            unit->mInBufValue[i] = v1;
        }
    }
    // dsp computation
    unit->mDSP->compute(inNumSamples, unit->mInBufCopy, unit->mOutBuf);
}

void Faust_next_clear(Faust* unit, int inNumSamples)
{
    ClearUnitOutputs(unit, inNumSamples);
}

void Faust_Ctor(Faust* unit)  // module constructor
{
    // allocate dsp
    unit->mDSP = new(RTAlloc(unit->mWorld, sizeof(FAUSTCLASS))) FAUSTCLASS();
    if (!unit->mDSP) {
        Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
        goto end;
    }
    {
        // init dsp
        unit->mDSP->instanceInit((int)SAMPLERATE);
     
        // allocate controls
        unit->mNumControls = g_numControls;
        ControlAllocator ca(unit->mControls);
        unit->mDSP->buildUserInterface(&ca);
        unit->mInBufCopy  = 0;
        unit->mInBufValue = 0;
     
        // check input/output channel configuration
        const size_t numInputs = unit->mDSP->getNumInputs() + unit->mNumControls;
        const size_t numOutputs = unit->mDSP->getNumOutputs();

        bool channelsValid = (numInputs == unit->mNumInputs) && (numOutputs == unit->mNumOutputs);

        if (channelsValid) {
            bool rateValid = true;
            for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
                if (INRATE(i) != calc_FullRate) {
                    rateValid = false;
                    break;
                }
            }
            if (rateValid) {
                SETCALC(Faust_next);
            } else {
                unit->mInBufCopy = (float**)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*sizeof(float*));
                if (!unit->mInBufCopy) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                // Allocate memory for input buffer copies (numInputs * bufLength)
                // and linear interpolation state (numInputs)
                // = numInputs * (bufLength + 1)
                unit->mInBufValue = (float*)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*sizeof(float));
                if (!unit->mInBufValue) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                // Aquire memory for interpolator state.
                float* mem = (float*)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*BUFLENGTH*sizeof(float));
                if (mem) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
                    // Initialize interpolator.
                    unit->mInBufValue[i] = IN0(i);
                    // Aquire buffer memory.
                    unit->mInBufCopy[i] = mem;
                    mem += BUFLENGTH;
                }
                SETCALC(Faust_next_copy);
            }
    #if !defined(NDEBUG)
            Print("Faust[%s]:\n", g_unitName);
            Print("    Inputs:   %d\n"
                  "    Outputs:  %d\n"
                  "    Callback: %s\n",
                  numInputs, numOutputs,
                  unit->mCalcFunc == (UnitCalcFunc)Faust_next ? "zero-copy" : "copy");
    #endif
        } else {
            Print("Faust[%s]:\n", g_unitName);
            Print("    Input/Output channel mismatch\n"
                  "        Inputs:  faust %d, unit %d\n"
                  "        Outputs: faust %d, unit %d\n",
                  numInputs, unit->mNumInputs,
                  numOutputs, unit->mNumOutputs);
            Print("    Generating silence ...\n");
            SETCALC(Faust_next_clear);
        }
    }
    
end:
    // Fix for https://github.com/grame-cncm/faust/issues/13
    ClearUnitOutputs(unit, 1);
}

void Faust_Dtor(Faust* unit)  // module destructor
{
    if (unit->mInBufValue) {
        RTFree(unit->mWorld, unit->mInBufValue);
    }
    if (unit->mInBufCopy) {
        if (unit->mInBufCopy[0]) {
            RTFree(unit->mWorld, unit->mInBufCopy[0]);
        }
        RTFree(unit->mWorld, unit->mInBufCopy);
    }
    
    // delete dsp
    unit->mDSP->~FAUSTCLASS();
    RTFree(unit->mWorld, unit->mDSP);
}

#ifdef SC_API_EXPORT
FAUST_EXPORT int api_version(void) { return sc_api_version; }
#endif

FAUST_EXPORT void load(InterfaceTable* inTable)
{
    ft = inTable;

    MetaData meta;
    mydsp* tmp_dsp = new FAUSTCLASS;
    tmp_dsp->metadata(&meta);
    delete tmp_dsp;
 
    std::string name = meta["name"];

    if (name.empty()) {
        name = fileNameToUnitName(__FILE__);
    }
  
    name = normalizeClassName(name);

#if !defined(NDEBUG) & defined(SC_API_EXPORT)
    Print("Faust: supercollider.cpp: sc_api_version = %d\n", sc_api_version);
#endif

    if (name.empty()) {
        // Catch empty name
        Print("Faust [supercollider.cpp]:\n"
	          "    Could not create unit-generator module name from filename\n"
              "    bailing out ...\n");
        return;
    }

    if (strncmp(name.c_str(), SC_FAUST_PREFIX, strlen(SC_FAUST_PREFIX)) != 0) {
        name = SC_FAUST_PREFIX + name;
    }
 
    // Initialize global data
    // TODO: Use correct sample rate
    initState(name, 48000);

    // Register ugen
    (*ft->fDefineUnit)(
        (char*)name.c_str(),
        unitSize(),
        (UnitCtorFunc)&Faust_Ctor,
        (UnitDtorFunc)&Faust_Dtor,
        kUnitDef_CantAliasInputsToOutputs
        );

#if !defined(NDEBUG)
    Print("Faust: %s numControls=%d\n", name.c_str(), g_numControls);
#endif // NDEBUG
}

#ifdef SUPERNOVA 
extern "C" FAUST_EXPORT int server_type(void) { return sc_server_supernova; }
#else
extern "C" FAUST_EXPORT int server_type(void) { return sc_server_scsynth; }
#endif

// EOF

#endif
