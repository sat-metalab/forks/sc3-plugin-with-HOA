/* ------------------------------------------------------------
author: "AmbisonicDecoderToolkit"
copyright: "(c) Aaron J. Heller 2013"
license: "BSD 3-Clause License"
name: "SATOswDo3"
version: "1.2"
Code generated with Faust 2.5.12 (https://faust.grame.fr)
Compilation options: cpp, -scal -ftz 0
------------------------------------------------------------ */

#ifndef  __mydsp_H__
#define  __mydsp_H__

//-------------------------------------------------------------------
// FAUST architecture file for SuperCollider.
// Copyright (C) 2005-2012 Stefan Kersten.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//-------------------------------------------------------------------

// If other than 'faust2sc --prefix Faust' is used, sed this as well:
#if !defined(SC_FAUST_PREFIX)
#define SC_FAUST_PREFIX "Faust"
#endif

#include <map>
#include <string>
#include <string.h>
#include <SC_PlugIn.h>

/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __dsp__
#define __dsp__

#include <string>

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

class UI;
struct Meta;

/**
 * DSP memory manager.
 */

struct dsp_memory_manager {
    
    virtual ~dsp_memory_manager() {}
    
    virtual void* allocate(size_t size) = 0;
    virtual void destroy(void* ptr) = 0;
    
};

/**
* Signal processor definition.
*/

class dsp {

    public:

        dsp() {}
        virtual ~dsp() {}

        /* Return instance number of audio inputs */
        virtual int getNumInputs() = 0;
    
        /* Return instance number of audio outputs */
        virtual int getNumOutputs() = 0;
    
        /**
         * Trigger the ui_interface parameter with instance specific calls
         * to 'addBtton', 'addVerticalSlider'... in order to build the UI.
         *
         * @param ui_interface - the user interface builder
         */
        virtual void buildUserInterface(UI* ui_interface) = 0;
    
        /* Returns the sample rate currently used by the instance */
        virtual int getSampleRate() = 0;
    
        /**
         * Global init, calls the following methods:
         * - static class 'classInit': static tables initialization
         * - 'instanceInit': constants and instance state initialization
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void init(int samplingRate) = 0;

        /**
         * Init instance state
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void instanceInit(int samplingRate) = 0;

        /**
         * Init instance constant state
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void instanceConstants(int samplingRate) = 0;
    
        /* Init default control parameters values */
        virtual void instanceResetUserInterface() = 0;
    
        /* Init instance state (delay lines...) */
        virtual void instanceClear() = 0;
 
        /**
         * Return a clone of the instance.
         *
         * @return a copy of the instance on success, otherwise a null pointer.
         */
        virtual dsp* clone() = 0;
    
        /**
         * Trigger the Meta* parameter with instance specific calls to 'declare' (key, value) metadata.
         *
         * @param m - the Meta* meta user
         */
        virtual void metadata(Meta* m) = 0;
    
        /**
         * DSP instance computation, to be called with successive in/out audio buffers.
         *
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         *
         */
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) = 0;
    
        /**
         * DSP instance computation: alternative method to be used by subclasses.
         *
         * @param date_usec - the timestamp in microsec given by audio driver.
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         *
         */
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { compute(count, inputs, outputs); }
       
};

/**
 * Generic DSP decorator.
 */

class decorator_dsp : public dsp {

    protected:

        dsp* fDSP;

    public:

        decorator_dsp(dsp* dsp = 0):fDSP(dsp) {}
        virtual ~decorator_dsp() { delete fDSP; }

        virtual int getNumInputs() { return fDSP->getNumInputs(); }
        virtual int getNumOutputs() { return fDSP->getNumOutputs(); }
        virtual void buildUserInterface(UI* ui_interface) { fDSP->buildUserInterface(ui_interface); }
        virtual int getSampleRate() { return fDSP->getSampleRate(); }
        virtual void init(int samplingRate) { fDSP->init(samplingRate); }
        virtual void instanceInit(int samplingRate) { fDSP->instanceInit(samplingRate); }
        virtual void instanceConstants(int samplingRate) { fDSP->instanceConstants(samplingRate); }
        virtual void instanceResetUserInterface() { fDSP->instanceResetUserInterface(); }
        virtual void instanceClear() { fDSP->instanceClear(); }
        virtual decorator_dsp* clone() { return new decorator_dsp(fDSP->clone()); }
        virtual void metadata(Meta* m) { fDSP->metadata(m); }
        // Beware: subclasses usually have to overload the two 'compute' methods
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(count, inputs, outputs); }
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(date_usec, count, inputs, outputs); }
    
};

/**
 * DSP factory class.
 */

class dsp_factory {
    
    protected:
    
        // So that to force sub-classes to use deleteDSPFactory(dsp_factory* factory);
        virtual ~dsp_factory() {}
    
    public:
    
        virtual std::string getName() = 0;
        virtual std::string getSHAKey() = 0;
        virtual std::string getDSPCode() = 0;
    
        virtual dsp* createDSPInstance() = 0;
    
        virtual void setMemoryManager(dsp_memory_manager* manager) = 0;
        virtual dsp_memory_manager* getMemoryManager() = 0;
    
};

/**
 * On Intel set FZ (Flush to Zero) and DAZ (Denormals Are Zero)
 * flags to avoid costly denormals.
 */

#ifdef __SSE__
    #include <xmmintrin.h>
    #ifdef __SSE2__
        #define AVOIDDENORMALS _mm_setcsr(_mm_getcsr() | 0x8040)
    #else
        #define AVOIDDENORMALS _mm_setcsr(_mm_getcsr() | 0x8000)
    #endif
#else
    #define AVOIDDENORMALS
#endif

#endif
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __UI_H__
#define __UI_H__

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

/*******************************************************************************
 * UI : Faust DSP User Interface
 * User Interface as expected by the buildUserInterface() method of a DSP.
 * This abstract class contains only the method that the Faust compiler can
 * generate to describe a DSP user interface.
 ******************************************************************************/

struct Soundfile;

class UI
{

    public:

        UI() {}

        virtual ~UI() {}

        // -- widget's layouts

        virtual void openTabBox(const char* label) = 0;
        virtual void openHorizontalBox(const char* label) = 0;
        virtual void openVerticalBox(const char* label) = 0;
        virtual void closeBox() = 0;

        // -- active widgets

        virtual void addButton(const char* label, FAUSTFLOAT* zone) = 0;
        virtual void addCheckButton(const char* label, FAUSTFLOAT* zone) = 0;
        virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;
        virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;
        virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;

        // -- passive widgets

        virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) = 0;
        virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) = 0;
    
        // -- soundfiles
    
        virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) = 0;

        // -- metadata declarations

        virtual void declare(FAUSTFLOAT*, const char*, const char*) {}
};

#endif
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/
 
#ifndef __misc__
#define __misc__

#include <algorithm>
#include <map>
#include <string.h>
#include <stdlib.h>

/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __meta__
#define __meta__

struct Meta
{
    virtual void declare(const char* key, const char* value) = 0;
    virtual ~Meta() {};
};

#endif

using std::max;
using std::min;

struct XXXX_Meta : std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key]=value; }
};

struct MY_Meta : Meta, std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key]=value; }
};

inline int lsr(int x, int n)	{ return int(((unsigned int)x) >> n); }

inline int int2pow2(int x)		{ int r = 0; while ((1<<r) < x) r++; return r; }

inline long lopt(char* argv[], const char* name, long def)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return atoi(argv[i+1]);
	return def;
}

inline bool isopt(char* argv[], const char* name)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return true;
	return false;
}

inline const char* lopts(char* argv[], const char* name, const char* def)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return argv[i+1];
	return def;
}

#endif


using namespace std;

#if defined(__GNUC__) && __GNUC__ >= 4
    #define FAUST_EXPORT __attribute__((visibility("default")))
#else
    #define FAUST_EXPORT  SC_API_EXPORT
#endif

#ifdef WIN32
    #define STRDUP _strdup
#else
    #define STRDUP strdup
#endif

//----------------------------------------------------------------------------
// Vector intrinsics
//----------------------------------------------------------------------------


//----------------------------------------------------------------------------
// Metadata
//----------------------------------------------------------------------------

class MetaData : public Meta
               , public std::map<std::string, std::string>
{
public:
    void declare(const char* key, const char* value)
    {
        (*this)[key] = value;
    }
};

//----------------------------------------------------------------------------
// Control counter
//----------------------------------------------------------------------------

class ControlCounter : public UI
{
public:
    ControlCounter()
        : mNumControlInputs(0),
          mNumControlOutputs(0)
    {}

    size_t getNumControls() const { return getNumControlInputs(); }
    size_t getNumControlInputs() const { return mNumControlInputs; }
    size_t getNumControlOutputs() const { return mNumControlOutputs; }

    // Layout widgets
    virtual void openTabBox(const char* label) { }
    virtual void openHorizontalBox(const char* label) { }
    virtual void openVerticalBox(const char* label) { }
    virtual void closeBox() { }

    // Active widgets
    virtual void addButton(const char* label, FAUSTFLOAT* zone)
    { addControlInput(); }
    virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
    { addControlInput(); }
    virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }
    virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }
    virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }

    // Passive widgets
    virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
    { addControlOutput(); }
    virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
    { addControlOutput(); }
    
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}

protected:
    void addControlInput() { mNumControlInputs++; }
    void addControlOutput() { mNumControlOutputs++; }

private:
    size_t mNumControlInputs;
    size_t mNumControlOutputs;
};

//----------------------------------------------------------------------------
// UI control
//----------------------------------------------------------------------------

struct Control
{
    typedef void (*UpdateFunction)(Control* self, FAUSTFLOAT value);

    UpdateFunction updateFunction;
    FAUSTFLOAT* zone;
    FAUSTFLOAT min, max;

    inline void update(FAUSTFLOAT value)
    {
        (*updateFunction)(this, value);
    }

    static void simpleUpdate(Control* self, FAUSTFLOAT value)
    {
        *self->zone = value;
    }
    static void boundedUpdate(Control* self, FAUSTFLOAT value)
    {
        *self->zone = sc_clip(value, self->min, self->max);
    }
};

//----------------------------------------------------------------------------
// Control allocator
//----------------------------------------------------------------------------

class ControlAllocator : public UI
{
public:
    ControlAllocator(Control* controls)
        : mControls(controls)
    { }

    // Layout widgets
    virtual void openTabBox(const char* label) { }
    virtual void openHorizontalBox(const char* label) { }
    virtual void openVerticalBox(const char* label) { }
    virtual void closeBox() { }

    // Active widgets
    virtual void addButton(const char* label, FAUSTFLOAT* zone)
    { addSimpleControl(zone); }
    virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
    { addSimpleControl(zone); }
    virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }
    virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }
    virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }

    // Passive widgets
    virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
    virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}

private:
    void addControl(Control::UpdateFunction updateFunction, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT /* step */)
    {
        Control* ctrl        = mControls++;
        ctrl->updateFunction = updateFunction;
        ctrl->zone           = zone;
        ctrl->min            = min;
        ctrl->max            = max;
    }
    void addSimpleControl(FAUSTFLOAT* zone)
    {
        addControl(Control::simpleUpdate, zone, 0.f, 0.f, 0.f);
    }
    void addBoundedControl(FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    {
        addControl(Control::boundedUpdate, zone, min, max, step);
    }

private:
    Control* mControls;
};

//----------------------------------------------------------------------------
// FAUST generated code
//----------------------------------------------------------------------------

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif 

#include <cmath>
#include <math.h>

float mydsp_faustpower2_f(float value) {
	return (value * value);
	
}

#ifndef FAUSTCLASS 
#define FAUSTCLASS mydsp
#endif
#ifdef __APPLE__ 
#define exp10f __exp10f
#define exp10 __exp10
#endif

class mydsp : public dsp {
	
 private:
	
	FAUSTFLOAT fCheckbox0;
	FAUSTFLOAT fHslider0;
	float fRec0[2];
	int fSamplingFreq;
	int iConst0;
	float fConst1;
	FAUSTFLOAT fHslider1;
	float fRec1[2];
	float fRec2[3];
	FAUSTFLOAT fHslider2;
	float fRec3[2];
	float fConst2;
	float fConst3;
	float fConst4;
	float fConst5;
	float fConst6;
	float fRec13[3];
	float fRec14[3];
	float fRec15[3];
	float fRec16[3];
	float fRec17[3];
	float fRec18[3];
	float fRec19[3];
	float fConst7;
	float fConst8;
	float fConst9;
	float fRec12[2];
	float fRec10[2];
	float fRec9[2];
	float fRec7[2];
	float fRec6[2];
	float fRec4[2];
	float fConst10;
	float fConst11;
	float fRec23[3];
	float fRec24[3];
	float fRec25[3];
	float fConst12;
	float fRec22[2];
	float fRec20[2];
	float fConst13;
	float fConst14;
	float fRec32[3];
	float fRec33[3];
	float fRec34[3];
	float fRec35[3];
	float fRec36[3];
	float fConst15;
	float fConst16;
	float fRec31[2];
	float fRec29[2];
	float fRec28[2];
	float fRec26[2];
	int IOTA;
	float fVec0[2048];
	int iConst17;
	float fConst18;
	float fConst19;
	float fConst20;
	float fConst21;
	float fConst22;
	float fRec45[2];
	float fRec43[2];
	float fRec42[2];
	float fRec40[2];
	float fConst23;
	float fRec39[2];
	float fRec37[2];
	float fConst24;
	float fConst25;
	float fConst26;
	float fRec48[2];
	float fRec46[2];
	float fConst27;
	float fConst28;
	float fConst29;
	float fConst30;
	float fRec54[2];
	float fRec52[2];
	float fRec51[2];
	float fRec49[2];
	float fVec1[1024];
	int iConst31;
	float fConst32;
	float fConst33;
	float fConst34;
	float fConst35;
	float fConst36;
	float fConst37;
	float fRec63[2];
	float fRec61[2];
	float fRec60[2];
	float fRec58[2];
	float fRec57[2];
	float fRec55[2];
	float fConst38;
	float fConst39;
	float fConst40;
	float fRec66[2];
	float fRec64[2];
	float fConst41;
	float fConst42;
	float fConst43;
	float fConst44;
	float fRec72[2];
	float fRec70[2];
	float fRec69[2];
	float fRec67[2];
	float fVec2[1024];
	int iConst45;
	float fRec75[2];
	float fRec73[2];
	float fRec81[2];
	float fRec79[2];
	float fRec78[2];
	float fRec76[2];
	float fRec90[2];
	float fRec88[2];
	float fRec87[2];
	float fRec85[2];
	float fRec84[2];
	float fRec82[2];
	float fVec3[1024];
	float fRec93[2];
	float fRec91[2];
	float fRec99[2];
	float fRec97[2];
	float fRec96[2];
	float fRec94[2];
	float fRec108[2];
	float fRec106[2];
	float fRec105[2];
	float fRec103[2];
	float fRec102[2];
	float fRec100[2];
	float fVec4[1024];
	float fRec111[2];
	float fRec109[2];
	float fRec117[2];
	float fRec115[2];
	float fRec114[2];
	float fRec112[2];
	float fRec126[2];
	float fRec124[2];
	float fRec123[2];
	float fRec121[2];
	float fRec120[2];
	float fRec118[2];
	float fVec5[1024];
	float fRec135[2];
	float fRec133[2];
	float fRec132[2];
	float fRec130[2];
	float fRec129[2];
	float fRec127[2];
	float fRec138[2];
	float fRec136[2];
	float fRec144[2];
	float fRec142[2];
	float fRec141[2];
	float fRec139[2];
	float fVec6[1024];
	float fConst46;
	float fConst47;
	float fConst48;
	float fConst49;
	float fConst50;
	float fConst51;
	float fRec153[2];
	float fRec151[2];
	float fRec150[2];
	float fRec148[2];
	float fRec147[2];
	float fRec145[2];
	float fConst52;
	float fConst53;
	float fConst54;
	float fRec156[2];
	float fRec154[2];
	float fConst55;
	float fConst56;
	float fConst57;
	float fConst58;
	float fRec162[2];
	float fRec160[2];
	float fRec159[2];
	float fRec157[2];
	float fVec7[1024];
	int iConst59;
	float fConst60;
	float fConst61;
	float fConst62;
	float fConst63;
	float fConst64;
	float fConst65;
	float fRec171[2];
	float fRec169[2];
	float fRec168[2];
	float fRec166[2];
	float fRec165[2];
	float fRec163[2];
	float fConst66;
	float fConst67;
	float fConst68;
	float fRec174[2];
	float fRec172[2];
	float fConst69;
	float fConst70;
	float fConst71;
	float fConst72;
	float fRec180[2];
	float fRec178[2];
	float fRec177[2];
	float fRec175[2];
	float fVec8[1024];
	int iConst73;
	float fRec189[2];
	float fRec187[2];
	float fRec186[2];
	float fRec184[2];
	float fRec183[2];
	float fRec181[2];
	float fRec192[2];
	float fRec190[2];
	float fRec198[2];
	float fRec196[2];
	float fRec195[2];
	float fRec193[2];
	float fVec9[1024];
	float fRec207[2];
	float fRec205[2];
	float fRec204[2];
	float fRec202[2];
	float fRec201[2];
	float fRec199[2];
	float fRec210[2];
	float fRec208[2];
	float fRec216[2];
	float fRec214[2];
	float fRec213[2];
	float fRec211[2];
	float fVec10[1024];
	float fRec225[2];
	float fRec223[2];
	float fRec222[2];
	float fRec220[2];
	float fRec219[2];
	float fRec217[2];
	float fRec228[2];
	float fRec226[2];
	float fRec234[2];
	float fRec232[2];
	float fRec231[2];
	float fRec229[2];
	float fVec11[1024];
	float fRec243[2];
	float fRec241[2];
	float fRec240[2];
	float fRec238[2];
	float fRec237[2];
	float fRec235[2];
	float fRec246[2];
	float fRec244[2];
	float fRec252[2];
	float fRec250[2];
	float fRec249[2];
	float fRec247[2];
	float fVec12[1024];
	float fRec261[2];
	float fRec259[2];
	float fRec258[2];
	float fRec256[2];
	float fRec255[2];
	float fRec253[2];
	float fRec264[2];
	float fRec262[2];
	float fRec270[2];
	float fRec268[2];
	float fRec267[2];
	float fRec265[2];
	float fVec13[1024];
	float fRec279[2];
	float fRec277[2];
	float fRec276[2];
	float fRec274[2];
	float fRec273[2];
	float fRec271[2];
	float fRec282[2];
	float fRec280[2];
	float fRec288[2];
	float fRec286[2];
	float fRec285[2];
	float fRec283[2];
	float fVec14[1024];
	float fRec297[2];
	float fRec295[2];
	float fRec294[2];
	float fRec292[2];
	float fRec291[2];
	float fRec289[2];
	float fRec300[2];
	float fRec298[2];
	float fRec306[2];
	float fRec304[2];
	float fRec303[2];
	float fRec301[2];
	float fVec15[1024];
	float fRec315[2];
	float fRec313[2];
	float fRec312[2];
	float fRec310[2];
	float fRec309[2];
	float fRec307[2];
	float fRec318[2];
	float fRec316[2];
	float fRec324[2];
	float fRec322[2];
	float fRec321[2];
	float fRec319[2];
	float fVec16[1024];
	float fRec333[2];
	float fRec331[2];
	float fRec330[2];
	float fRec328[2];
	float fRec327[2];
	float fRec325[2];
	float fRec336[2];
	float fRec334[2];
	float fRec342[2];
	float fRec340[2];
	float fRec339[2];
	float fRec337[2];
	float fVec17[1024];
	float fRec351[2];
	float fRec349[2];
	float fRec348[2];
	float fRec346[2];
	float fRec345[2];
	float fRec343[2];
	float fRec354[2];
	float fRec352[2];
	float fRec360[2];
	float fRec358[2];
	float fRec357[2];
	float fRec355[2];
	float fVec18[1024];
	float fConst74;
	float fConst75;
	float fConst76;
	float fRec363[2];
	float fRec361[2];
	float fConst77;
	float fConst78;
	float fConst79;
	float fConst80;
	float fRec369[2];
	float fRec367[2];
	float fRec366[2];
	float fRec364[2];
	float fConst81;
	float fConst82;
	float fConst83;
	float fConst84;
	float fConst85;
	float fConst86;
	float fRec378[2];
	float fRec376[2];
	float fRec375[2];
	float fRec373[2];
	float fRec372[2];
	float fRec370[2];
	float fVec19[2];
	int iConst87;
	float fConst88;
	float fConst89;
	float fConst90;
	float fConst91;
	float fConst92;
	float fConst93;
	float fRec387[2];
	float fRec385[2];
	float fRec384[2];
	float fRec382[2];
	float fRec381[2];
	float fRec379[2];
	float fConst94;
	float fConst95;
	float fConst96;
	float fRec390[2];
	float fRec388[2];
	float fConst97;
	float fConst98;
	float fConst99;
	float fConst100;
	float fRec396[2];
	float fRec394[2];
	float fRec393[2];
	float fRec391[2];
	float fRec405[2];
	float fRec403[2];
	float fRec402[2];
	float fRec400[2];
	float fRec399[2];
	float fRec397[2];
	float fRec411[2];
	float fRec409[2];
	float fRec408[2];
	float fRec406[2];
	float fRec414[2];
	float fRec412[2];
	float fRec417[2];
	float fRec415[2];
	float fRec423[2];
	float fRec421[2];
	float fRec420[2];
	float fRec418[2];
	float fRec432[2];
	float fRec430[2];
	float fRec429[2];
	float fRec427[2];
	float fRec426[2];
	float fRec424[2];
	float fVec20[2];
	float fRec441[2];
	float fRec439[2];
	float fRec438[2];
	float fRec436[2];
	float fRec435[2];
	float fRec433[2];
	float fRec444[2];
	float fRec442[2];
	float fRec450[2];
	float fRec448[2];
	float fRec447[2];
	float fRec445[2];
	float fRec459[2];
	float fRec457[2];
	float fRec456[2];
	float fRec454[2];
	float fRec453[2];
	float fRec451[2];
	float fRec462[2];
	float fRec460[2];
	float fRec468[2];
	float fRec466[2];
	float fRec465[2];
	float fRec463[2];
	float fRec477[2];
	float fRec475[2];
	float fRec474[2];
	float fRec472[2];
	float fRec471[2];
	float fRec469[2];
	float fRec480[2];
	float fRec478[2];
	float fRec486[2];
	float fRec484[2];
	float fRec483[2];
	float fRec481[2];
	float fVec21[2];
	float fRec489[2];
	float fRec487[2];
	float fRec495[2];
	float fRec493[2];
	float fRec492[2];
	float fRec490[2];
	float fRec504[2];
	float fRec502[2];
	float fRec501[2];
	float fRec499[2];
	float fRec498[2];
	float fRec496[2];
	float fRec507[2];
	float fRec505[2];
	float fRec513[2];
	float fRec511[2];
	float fRec510[2];
	float fRec508[2];
	float fRec522[2];
	float fRec520[2];
	float fRec519[2];
	float fRec517[2];
	float fRec516[2];
	float fRec514[2];
	float fRec525[2];
	float fRec523[2];
	float fRec531[2];
	float fRec529[2];
	float fRec528[2];
	float fRec526[2];
	float fRec540[2];
	float fRec538[2];
	float fRec537[2];
	float fRec535[2];
	float fRec534[2];
	float fRec532[2];
	float fVec22[2];
	float fRec543[2];
	float fRec541[2];
	float fRec549[2];
	float fRec547[2];
	float fRec546[2];
	float fRec544[2];
	float fRec558[2];
	float fRec556[2];
	float fRec555[2];
	float fRec553[2];
	float fRec552[2];
	float fRec550[2];
	float fRec561[2];
	float fRec559[2];
	float fRec567[2];
	float fRec565[2];
	float fRec564[2];
	float fRec562[2];
	float fRec576[2];
	float fRec574[2];
	float fRec573[2];
	float fRec571[2];
	float fRec570[2];
	float fRec568[2];
	
 public:
	
	void metadata(Meta* m) { 
		m->declare("author", "AmbisonicDecoderToolkit");
		m->declare("copyright", "(c) Aaron J. Heller 2013");
		m->declare("license", "BSD 3-Clause License");
		m->declare("name", "SATOswDo3");
		m->declare("version", "1.2");
	}

	virtual int getNumInputs() {
		return 16;
		
	}
	virtual int getNumOutputs() {
		return 31;
		
	}
	virtual int getInputRate(int channel) {
		int rate;
		switch (channel) {
			case 0: {
				rate = 1;
				break;
			}
			case 1: {
				rate = 1;
				break;
			}
			case 2: {
				rate = 1;
				break;
			}
			case 3: {
				rate = 1;
				break;
			}
			case 4: {
				rate = 1;
				break;
			}
			case 5: {
				rate = 1;
				break;
			}
			case 6: {
				rate = 1;
				break;
			}
			case 7: {
				rate = 1;
				break;
			}
			case 8: {
				rate = 1;
				break;
			}
			case 9: {
				rate = 1;
				break;
			}
			case 10: {
				rate = 1;
				break;
			}
			case 11: {
				rate = 1;
				break;
			}
			case 12: {
				rate = 1;
				break;
			}
			case 13: {
				rate = 1;
				break;
			}
			case 14: {
				rate = 1;
				break;
			}
			case 15: {
				rate = 1;
				break;
			}
			default: {
				rate = -1;
				break;
			}
			
		}
		return rate;
		
	}
	virtual int getOutputRate(int channel) {
		int rate;
		switch (channel) {
			case 0: {
				rate = 1;
				break;
			}
			case 1: {
				rate = 1;
				break;
			}
			case 2: {
				rate = 1;
				break;
			}
			case 3: {
				rate = 1;
				break;
			}
			case 4: {
				rate = 1;
				break;
			}
			case 5: {
				rate = 1;
				break;
			}
			case 6: {
				rate = 1;
				break;
			}
			case 7: {
				rate = 1;
				break;
			}
			case 8: {
				rate = 1;
				break;
			}
			case 9: {
				rate = 1;
				break;
			}
			case 10: {
				rate = 1;
				break;
			}
			case 11: {
				rate = 1;
				break;
			}
			case 12: {
				rate = 1;
				break;
			}
			case 13: {
				rate = 1;
				break;
			}
			case 14: {
				rate = 1;
				break;
			}
			case 15: {
				rate = 1;
				break;
			}
			case 16: {
				rate = 1;
				break;
			}
			case 17: {
				rate = 1;
				break;
			}
			case 18: {
				rate = 1;
				break;
			}
			case 19: {
				rate = 1;
				break;
			}
			case 20: {
				rate = 1;
				break;
			}
			case 21: {
				rate = 1;
				break;
			}
			case 22: {
				rate = 1;
				break;
			}
			case 23: {
				rate = 1;
				break;
			}
			case 24: {
				rate = 1;
				break;
			}
			case 25: {
				rate = 1;
				break;
			}
			case 26: {
				rate = 1;
				break;
			}
			case 27: {
				rate = 1;
				break;
			}
			case 28: {
				rate = 1;
				break;
			}
			case 29: {
				rate = 1;
				break;
			}
			case 30: {
				rate = 1;
				break;
			}
			default: {
				rate = -1;
				break;
			}
			
		}
		return rate;
		
	}
	
	static void classInit(int samplingFreq) {
		
	}
	
	virtual void instanceConstants(int samplingFreq) {
		fSamplingFreq = samplingFreq;
		iConst0 = min(192000, max(1, fSamplingFreq));
		fConst1 = (3.14159274f / float(iConst0));
		fConst2 = float(iConst0);
		fConst3 = ((56.7265472f / fConst2) + 1.0f);
		fConst4 = (0.0f - (113.453094f / (fConst2 * fConst3)));
		fConst5 = ((((3854.54663f / fConst2) + 89.84198f) / fConst2) + 1.0f);
		fConst6 = (1.0f / (fConst3 * fConst5));
		fConst7 = mydsp_faustpower2_f(fConst2);
		fConst8 = (0.0f - (15418.1865f / (fConst7 * fConst5)));
		fConst9 = (0.0f - (((15418.1865f / fConst2) + 179.68396f) / (fConst2 * fConst5)));
		fConst10 = ((24.4280872f / fConst2) + 1.0f);
		fConst11 = (1.0f / fConst10);
		fConst12 = (0.0f - (48.8561745f / (fConst2 * fConst10)));
		fConst13 = ((((1790.19434f / fConst2) + 73.2842636f) / fConst2) + 1.0f);
		fConst14 = (1.0f / fConst13);
		fConst15 = (0.0f - (7160.77734f / (fConst7 * fConst13)));
		fConst16 = (0.0f - (((7160.77734f / fConst2) + 146.568527f) / (fConst2 * fConst13)));
		iConst17 = int(((0.00671591423f * float(iConst0)) + 0.5f));
		fConst18 = ((((3329.17993f / fConst2) + 83.4951553f) / fConst2) + 1.0f);
		fConst19 = (0.0f - (13316.7197f / (fConst7 * fConst18)));
		fConst20 = (0.0f - (((13316.7197f / fConst2) + 166.990311f) / (fConst2 * fConst18)));
		fConst21 = ((52.7191391f / fConst2) + 1.0f);
		fConst22 = (1.0f / (fConst21 * fConst18));
		fConst23 = (0.0f - (105.438278f / (fConst2 * fConst21)));
		fConst24 = ((22.702383f / fConst2) + 1.0f);
		fConst25 = (1.0f / fConst24);
		fConst26 = (0.0f - (45.4047661f / (fConst2 * fConst24)));
		fConst27 = ((((1546.19458f / fConst2) + 68.1071472f) / fConst2) + 1.0f);
		fConst28 = (1.0f / fConst27);
		fConst29 = (0.0f - (6184.77832f / (fConst7 * fConst27)));
		fConst30 = (0.0f - (((6184.77832f / fConst2) + 136.214294f) / (fConst2 * fConst27)));
		iConst31 = int(((0.0051600365f * float(iConst0)) + 0.5f));
		fConst32 = ((52.7330933f / fConst2) + 1.0f);
		fConst33 = (0.0f - (105.466187f / (fConst2 * fConst32)));
		fConst34 = ((((3330.94238f / fConst2) + 83.5172501f) / fConst2) + 1.0f);
		fConst35 = (1.0f / (fConst32 * fConst34));
		fConst36 = (0.0f - (13323.7695f / (fConst7 * fConst34)));
		fConst37 = (0.0f - (((13323.7695f / fConst2) + 167.0345f) / (fConst2 * fConst34)));
		fConst38 = ((22.7083912f / fConst2) + 1.0f);
		fConst39 = (1.0f / fConst38);
		fConst40 = (0.0f - (45.4167824f / (fConst2 * fConst38)));
		fConst41 = ((((1547.01306f / fConst2) + 68.1251755f) / fConst2) + 1.0f);
		fConst42 = (1.0f / fConst41);
		fConst43 = (0.0f - (6188.05225f / (fConst7 * fConst41)));
		fConst44 = (0.0f - (((6188.05225f / fConst2) + 136.250351f) / (fConst2 * fConst41)));
		iConst45 = int(((0.00516586378f * float(iConst0)) + 0.5f));
		fConst46 = ((48.5565948f / fConst2) + 1.0f);
		fConst47 = (0.0f - (97.1131897f / (fConst2 * fConst46)));
		fConst48 = ((((2824.21069f / fConst2) + 76.902626f) / fConst2) + 1.0f);
		fConst49 = (1.0f / (fConst46 * fConst48));
		fConst50 = (0.0f - (11296.8428f / (fConst7 * fConst48)));
		fConst51 = (0.0f - (((11296.8428f / fConst2) + 153.805252f) / (fConst2 * fConst48)));
		fConst52 = ((20.9098701f / fConst2) + 1.0f);
		fConst53 = (1.0f / fConst52);
		fConst54 = (0.0f - (41.8197403f / (fConst2 * fConst52)));
		fConst55 = ((((1311.66809f / fConst2) + 62.7296143f) / fConst2) + 1.0f);
		fConst56 = (1.0f / fConst55);
		fConst57 = (0.0f - (5246.67236f / (fConst7 * fConst55)));
		fConst58 = (0.0f - (((5246.67236f / fConst2) + 125.459229f) / (fConst2 * fConst55)));
		iConst59 = int(((0.00327200512f * float(iConst0)) + 0.5f));
		fConst60 = ((48.5270309f / fConst2) + 1.0f);
		fConst61 = (0.0f - (97.0540619f / (fConst2 * fConst60)));
		fConst62 = ((((2820.77246f / fConst2) + 76.8558044f) / fConst2) + 1.0f);
		fConst63 = (1.0f / (fConst60 * fConst62));
		fConst64 = (0.0f - (11283.0898f / (fConst7 * fConst62)));
		fConst65 = (0.0f - (((11283.0898f / fConst2) + 153.711609f) / (fConst2 * fConst62)));
		fConst66 = ((20.8971405f / fConst2) + 1.0f);
		fConst67 = (1.0f / fConst66);
		fConst68 = (0.0f - (41.794281f / (fConst2 * fConst66)));
		fConst69 = ((((1310.07129f / fConst2) + 62.6914177f) / fConst2) + 1.0f);
		fConst70 = (1.0f / fConst69);
		fConst71 = (0.0f - (5240.28516f / (fConst7 * fConst69)));
		fConst72 = (0.0f - (((5240.28516f / fConst2) + 125.382835f) / (fConst2 * fConst69)));
		iConst73 = int(((0.00325743691f * float(iConst0)) + 0.5f));
		fConst74 = ((18.3950386f / fConst2) + 1.0f);
		fConst75 = (1.0f / fConst74);
		fConst76 = (0.0f - (36.7900772f / (fConst2 * fConst74)));
		fConst77 = ((((1015.13226f / fConst2) + 55.1851158f) / fConst2) + 1.0f);
		fConst78 = (1.0f / fConst77);
		fConst79 = (0.0f - (4060.52905f / (fConst7 * fConst77)));
		fConst80 = (0.0f - (((4060.52905f / fConst2) + 110.370232f) / (fConst2 * fConst77)));
		fConst81 = ((42.7166901f / fConst2) + 1.0f);
		fConst82 = (0.0f - (85.4333801f / (fConst2 * fConst81)));
		fConst83 = ((((2185.72632f / fConst2) + 67.6535416f) / fConst2) + 1.0f);
		fConst84 = (1.0f / (fConst81 * fConst83));
		fConst85 = (0.0f - (8742.90527f / (fConst7 * fConst83)));
		fConst86 = (0.0f - (((8742.90527f / fConst2) + 135.307083f) / (fConst2 * fConst83)));
		iConst87 = int(((2.9136288e-06f * float(iConst0)) + 0.5f));
		fConst88 = ((42.7121086f / fConst2) + 1.0f);
		fConst89 = (0.0f - (85.4242172f / (fConst2 * fConst88)));
		fConst90 = ((((2185.25781f / fConst2) + 67.646286f) / fConst2) + 1.0f);
		fConst91 = (1.0f / (fConst88 * fConst90));
		fConst92 = (0.0f - (8741.03125f / (fConst7 * fConst90)));
		fConst93 = (0.0f - (((8741.03125f / fConst2) + 135.292572f) / (fConst2 * fConst90)));
		fConst94 = ((18.3930664f / fConst2) + 1.0f);
		fConst95 = (1.0f / fConst94);
		fConst96 = (0.0f - (36.7861328f / (fConst2 * fConst94)));
		fConst97 = ((((1014.91467f / fConst2) + 55.1791992f) / fConst2) + 1.0f);
		fConst98 = (1.0f / fConst97);
		fConst99 = (0.0f - (4059.65869f / (fConst7 * fConst97)));
		fConst100 = (0.0f - (((4059.65869f / fConst2) + 110.358398f) / (fConst2 * fConst97)));
		
	}
	
	virtual void instanceResetUserInterface() {
		fCheckbox0 = FAUSTFLOAT(0.0f);
		fHslider0 = FAUSTFLOAT(-10.0f);
		fHslider1 = FAUSTFLOAT(400.0f);
		fHslider2 = FAUSTFLOAT(0.0f);
		
	}
	
	virtual void instanceClear() {
		for (int l0 = 0; (l0 < 2); l0 = (l0 + 1)) {
			fRec0[l0] = 0.0f;
			
		}
		for (int l1 = 0; (l1 < 2); l1 = (l1 + 1)) {
			fRec1[l1] = 0.0f;
			
		}
		for (int l2 = 0; (l2 < 3); l2 = (l2 + 1)) {
			fRec2[l2] = 0.0f;
			
		}
		for (int l3 = 0; (l3 < 2); l3 = (l3 + 1)) {
			fRec3[l3] = 0.0f;
			
		}
		for (int l4 = 0; (l4 < 3); l4 = (l4 + 1)) {
			fRec13[l4] = 0.0f;
			
		}
		for (int l5 = 0; (l5 < 3); l5 = (l5 + 1)) {
			fRec14[l5] = 0.0f;
			
		}
		for (int l6 = 0; (l6 < 3); l6 = (l6 + 1)) {
			fRec15[l6] = 0.0f;
			
		}
		for (int l7 = 0; (l7 < 3); l7 = (l7 + 1)) {
			fRec16[l7] = 0.0f;
			
		}
		for (int l8 = 0; (l8 < 3); l8 = (l8 + 1)) {
			fRec17[l8] = 0.0f;
			
		}
		for (int l9 = 0; (l9 < 3); l9 = (l9 + 1)) {
			fRec18[l9] = 0.0f;
			
		}
		for (int l10 = 0; (l10 < 3); l10 = (l10 + 1)) {
			fRec19[l10] = 0.0f;
			
		}
		for (int l11 = 0; (l11 < 2); l11 = (l11 + 1)) {
			fRec12[l11] = 0.0f;
			
		}
		for (int l12 = 0; (l12 < 2); l12 = (l12 + 1)) {
			fRec10[l12] = 0.0f;
			
		}
		for (int l13 = 0; (l13 < 2); l13 = (l13 + 1)) {
			fRec9[l13] = 0.0f;
			
		}
		for (int l14 = 0; (l14 < 2); l14 = (l14 + 1)) {
			fRec7[l14] = 0.0f;
			
		}
		for (int l15 = 0; (l15 < 2); l15 = (l15 + 1)) {
			fRec6[l15] = 0.0f;
			
		}
		for (int l16 = 0; (l16 < 2); l16 = (l16 + 1)) {
			fRec4[l16] = 0.0f;
			
		}
		for (int l17 = 0; (l17 < 3); l17 = (l17 + 1)) {
			fRec23[l17] = 0.0f;
			
		}
		for (int l18 = 0; (l18 < 3); l18 = (l18 + 1)) {
			fRec24[l18] = 0.0f;
			
		}
		for (int l19 = 0; (l19 < 3); l19 = (l19 + 1)) {
			fRec25[l19] = 0.0f;
			
		}
		for (int l20 = 0; (l20 < 2); l20 = (l20 + 1)) {
			fRec22[l20] = 0.0f;
			
		}
		for (int l21 = 0; (l21 < 2); l21 = (l21 + 1)) {
			fRec20[l21] = 0.0f;
			
		}
		for (int l22 = 0; (l22 < 3); l22 = (l22 + 1)) {
			fRec32[l22] = 0.0f;
			
		}
		for (int l23 = 0; (l23 < 3); l23 = (l23 + 1)) {
			fRec33[l23] = 0.0f;
			
		}
		for (int l24 = 0; (l24 < 3); l24 = (l24 + 1)) {
			fRec34[l24] = 0.0f;
			
		}
		for (int l25 = 0; (l25 < 3); l25 = (l25 + 1)) {
			fRec35[l25] = 0.0f;
			
		}
		for (int l26 = 0; (l26 < 3); l26 = (l26 + 1)) {
			fRec36[l26] = 0.0f;
			
		}
		for (int l27 = 0; (l27 < 2); l27 = (l27 + 1)) {
			fRec31[l27] = 0.0f;
			
		}
		for (int l28 = 0; (l28 < 2); l28 = (l28 + 1)) {
			fRec29[l28] = 0.0f;
			
		}
		for (int l29 = 0; (l29 < 2); l29 = (l29 + 1)) {
			fRec28[l29] = 0.0f;
			
		}
		for (int l30 = 0; (l30 < 2); l30 = (l30 + 1)) {
			fRec26[l30] = 0.0f;
			
		}
		IOTA = 0;
		for (int l31 = 0; (l31 < 2048); l31 = (l31 + 1)) {
			fVec0[l31] = 0.0f;
			
		}
		for (int l32 = 0; (l32 < 2); l32 = (l32 + 1)) {
			fRec45[l32] = 0.0f;
			
		}
		for (int l33 = 0; (l33 < 2); l33 = (l33 + 1)) {
			fRec43[l33] = 0.0f;
			
		}
		for (int l34 = 0; (l34 < 2); l34 = (l34 + 1)) {
			fRec42[l34] = 0.0f;
			
		}
		for (int l35 = 0; (l35 < 2); l35 = (l35 + 1)) {
			fRec40[l35] = 0.0f;
			
		}
		for (int l36 = 0; (l36 < 2); l36 = (l36 + 1)) {
			fRec39[l36] = 0.0f;
			
		}
		for (int l37 = 0; (l37 < 2); l37 = (l37 + 1)) {
			fRec37[l37] = 0.0f;
			
		}
		for (int l38 = 0; (l38 < 2); l38 = (l38 + 1)) {
			fRec48[l38] = 0.0f;
			
		}
		for (int l39 = 0; (l39 < 2); l39 = (l39 + 1)) {
			fRec46[l39] = 0.0f;
			
		}
		for (int l40 = 0; (l40 < 2); l40 = (l40 + 1)) {
			fRec54[l40] = 0.0f;
			
		}
		for (int l41 = 0; (l41 < 2); l41 = (l41 + 1)) {
			fRec52[l41] = 0.0f;
			
		}
		for (int l42 = 0; (l42 < 2); l42 = (l42 + 1)) {
			fRec51[l42] = 0.0f;
			
		}
		for (int l43 = 0; (l43 < 2); l43 = (l43 + 1)) {
			fRec49[l43] = 0.0f;
			
		}
		for (int l44 = 0; (l44 < 1024); l44 = (l44 + 1)) {
			fVec1[l44] = 0.0f;
			
		}
		for (int l45 = 0; (l45 < 2); l45 = (l45 + 1)) {
			fRec63[l45] = 0.0f;
			
		}
		for (int l46 = 0; (l46 < 2); l46 = (l46 + 1)) {
			fRec61[l46] = 0.0f;
			
		}
		for (int l47 = 0; (l47 < 2); l47 = (l47 + 1)) {
			fRec60[l47] = 0.0f;
			
		}
		for (int l48 = 0; (l48 < 2); l48 = (l48 + 1)) {
			fRec58[l48] = 0.0f;
			
		}
		for (int l49 = 0; (l49 < 2); l49 = (l49 + 1)) {
			fRec57[l49] = 0.0f;
			
		}
		for (int l50 = 0; (l50 < 2); l50 = (l50 + 1)) {
			fRec55[l50] = 0.0f;
			
		}
		for (int l51 = 0; (l51 < 2); l51 = (l51 + 1)) {
			fRec66[l51] = 0.0f;
			
		}
		for (int l52 = 0; (l52 < 2); l52 = (l52 + 1)) {
			fRec64[l52] = 0.0f;
			
		}
		for (int l53 = 0; (l53 < 2); l53 = (l53 + 1)) {
			fRec72[l53] = 0.0f;
			
		}
		for (int l54 = 0; (l54 < 2); l54 = (l54 + 1)) {
			fRec70[l54] = 0.0f;
			
		}
		for (int l55 = 0; (l55 < 2); l55 = (l55 + 1)) {
			fRec69[l55] = 0.0f;
			
		}
		for (int l56 = 0; (l56 < 2); l56 = (l56 + 1)) {
			fRec67[l56] = 0.0f;
			
		}
		for (int l57 = 0; (l57 < 1024); l57 = (l57 + 1)) {
			fVec2[l57] = 0.0f;
			
		}
		for (int l58 = 0; (l58 < 2); l58 = (l58 + 1)) {
			fRec75[l58] = 0.0f;
			
		}
		for (int l59 = 0; (l59 < 2); l59 = (l59 + 1)) {
			fRec73[l59] = 0.0f;
			
		}
		for (int l60 = 0; (l60 < 2); l60 = (l60 + 1)) {
			fRec81[l60] = 0.0f;
			
		}
		for (int l61 = 0; (l61 < 2); l61 = (l61 + 1)) {
			fRec79[l61] = 0.0f;
			
		}
		for (int l62 = 0; (l62 < 2); l62 = (l62 + 1)) {
			fRec78[l62] = 0.0f;
			
		}
		for (int l63 = 0; (l63 < 2); l63 = (l63 + 1)) {
			fRec76[l63] = 0.0f;
			
		}
		for (int l64 = 0; (l64 < 2); l64 = (l64 + 1)) {
			fRec90[l64] = 0.0f;
			
		}
		for (int l65 = 0; (l65 < 2); l65 = (l65 + 1)) {
			fRec88[l65] = 0.0f;
			
		}
		for (int l66 = 0; (l66 < 2); l66 = (l66 + 1)) {
			fRec87[l66] = 0.0f;
			
		}
		for (int l67 = 0; (l67 < 2); l67 = (l67 + 1)) {
			fRec85[l67] = 0.0f;
			
		}
		for (int l68 = 0; (l68 < 2); l68 = (l68 + 1)) {
			fRec84[l68] = 0.0f;
			
		}
		for (int l69 = 0; (l69 < 2); l69 = (l69 + 1)) {
			fRec82[l69] = 0.0f;
			
		}
		for (int l70 = 0; (l70 < 1024); l70 = (l70 + 1)) {
			fVec3[l70] = 0.0f;
			
		}
		for (int l71 = 0; (l71 < 2); l71 = (l71 + 1)) {
			fRec93[l71] = 0.0f;
			
		}
		for (int l72 = 0; (l72 < 2); l72 = (l72 + 1)) {
			fRec91[l72] = 0.0f;
			
		}
		for (int l73 = 0; (l73 < 2); l73 = (l73 + 1)) {
			fRec99[l73] = 0.0f;
			
		}
		for (int l74 = 0; (l74 < 2); l74 = (l74 + 1)) {
			fRec97[l74] = 0.0f;
			
		}
		for (int l75 = 0; (l75 < 2); l75 = (l75 + 1)) {
			fRec96[l75] = 0.0f;
			
		}
		for (int l76 = 0; (l76 < 2); l76 = (l76 + 1)) {
			fRec94[l76] = 0.0f;
			
		}
		for (int l77 = 0; (l77 < 2); l77 = (l77 + 1)) {
			fRec108[l77] = 0.0f;
			
		}
		for (int l78 = 0; (l78 < 2); l78 = (l78 + 1)) {
			fRec106[l78] = 0.0f;
			
		}
		for (int l79 = 0; (l79 < 2); l79 = (l79 + 1)) {
			fRec105[l79] = 0.0f;
			
		}
		for (int l80 = 0; (l80 < 2); l80 = (l80 + 1)) {
			fRec103[l80] = 0.0f;
			
		}
		for (int l81 = 0; (l81 < 2); l81 = (l81 + 1)) {
			fRec102[l81] = 0.0f;
			
		}
		for (int l82 = 0; (l82 < 2); l82 = (l82 + 1)) {
			fRec100[l82] = 0.0f;
			
		}
		for (int l83 = 0; (l83 < 1024); l83 = (l83 + 1)) {
			fVec4[l83] = 0.0f;
			
		}
		for (int l84 = 0; (l84 < 2); l84 = (l84 + 1)) {
			fRec111[l84] = 0.0f;
			
		}
		for (int l85 = 0; (l85 < 2); l85 = (l85 + 1)) {
			fRec109[l85] = 0.0f;
			
		}
		for (int l86 = 0; (l86 < 2); l86 = (l86 + 1)) {
			fRec117[l86] = 0.0f;
			
		}
		for (int l87 = 0; (l87 < 2); l87 = (l87 + 1)) {
			fRec115[l87] = 0.0f;
			
		}
		for (int l88 = 0; (l88 < 2); l88 = (l88 + 1)) {
			fRec114[l88] = 0.0f;
			
		}
		for (int l89 = 0; (l89 < 2); l89 = (l89 + 1)) {
			fRec112[l89] = 0.0f;
			
		}
		for (int l90 = 0; (l90 < 2); l90 = (l90 + 1)) {
			fRec126[l90] = 0.0f;
			
		}
		for (int l91 = 0; (l91 < 2); l91 = (l91 + 1)) {
			fRec124[l91] = 0.0f;
			
		}
		for (int l92 = 0; (l92 < 2); l92 = (l92 + 1)) {
			fRec123[l92] = 0.0f;
			
		}
		for (int l93 = 0; (l93 < 2); l93 = (l93 + 1)) {
			fRec121[l93] = 0.0f;
			
		}
		for (int l94 = 0; (l94 < 2); l94 = (l94 + 1)) {
			fRec120[l94] = 0.0f;
			
		}
		for (int l95 = 0; (l95 < 2); l95 = (l95 + 1)) {
			fRec118[l95] = 0.0f;
			
		}
		for (int l96 = 0; (l96 < 1024); l96 = (l96 + 1)) {
			fVec5[l96] = 0.0f;
			
		}
		for (int l97 = 0; (l97 < 2); l97 = (l97 + 1)) {
			fRec135[l97] = 0.0f;
			
		}
		for (int l98 = 0; (l98 < 2); l98 = (l98 + 1)) {
			fRec133[l98] = 0.0f;
			
		}
		for (int l99 = 0; (l99 < 2); l99 = (l99 + 1)) {
			fRec132[l99] = 0.0f;
			
		}
		for (int l100 = 0; (l100 < 2); l100 = (l100 + 1)) {
			fRec130[l100] = 0.0f;
			
		}
		for (int l101 = 0; (l101 < 2); l101 = (l101 + 1)) {
			fRec129[l101] = 0.0f;
			
		}
		for (int l102 = 0; (l102 < 2); l102 = (l102 + 1)) {
			fRec127[l102] = 0.0f;
			
		}
		for (int l103 = 0; (l103 < 2); l103 = (l103 + 1)) {
			fRec138[l103] = 0.0f;
			
		}
		for (int l104 = 0; (l104 < 2); l104 = (l104 + 1)) {
			fRec136[l104] = 0.0f;
			
		}
		for (int l105 = 0; (l105 < 2); l105 = (l105 + 1)) {
			fRec144[l105] = 0.0f;
			
		}
		for (int l106 = 0; (l106 < 2); l106 = (l106 + 1)) {
			fRec142[l106] = 0.0f;
			
		}
		for (int l107 = 0; (l107 < 2); l107 = (l107 + 1)) {
			fRec141[l107] = 0.0f;
			
		}
		for (int l108 = 0; (l108 < 2); l108 = (l108 + 1)) {
			fRec139[l108] = 0.0f;
			
		}
		for (int l109 = 0; (l109 < 1024); l109 = (l109 + 1)) {
			fVec6[l109] = 0.0f;
			
		}
		for (int l110 = 0; (l110 < 2); l110 = (l110 + 1)) {
			fRec153[l110] = 0.0f;
			
		}
		for (int l111 = 0; (l111 < 2); l111 = (l111 + 1)) {
			fRec151[l111] = 0.0f;
			
		}
		for (int l112 = 0; (l112 < 2); l112 = (l112 + 1)) {
			fRec150[l112] = 0.0f;
			
		}
		for (int l113 = 0; (l113 < 2); l113 = (l113 + 1)) {
			fRec148[l113] = 0.0f;
			
		}
		for (int l114 = 0; (l114 < 2); l114 = (l114 + 1)) {
			fRec147[l114] = 0.0f;
			
		}
		for (int l115 = 0; (l115 < 2); l115 = (l115 + 1)) {
			fRec145[l115] = 0.0f;
			
		}
		for (int l116 = 0; (l116 < 2); l116 = (l116 + 1)) {
			fRec156[l116] = 0.0f;
			
		}
		for (int l117 = 0; (l117 < 2); l117 = (l117 + 1)) {
			fRec154[l117] = 0.0f;
			
		}
		for (int l118 = 0; (l118 < 2); l118 = (l118 + 1)) {
			fRec162[l118] = 0.0f;
			
		}
		for (int l119 = 0; (l119 < 2); l119 = (l119 + 1)) {
			fRec160[l119] = 0.0f;
			
		}
		for (int l120 = 0; (l120 < 2); l120 = (l120 + 1)) {
			fRec159[l120] = 0.0f;
			
		}
		for (int l121 = 0; (l121 < 2); l121 = (l121 + 1)) {
			fRec157[l121] = 0.0f;
			
		}
		for (int l122 = 0; (l122 < 1024); l122 = (l122 + 1)) {
			fVec7[l122] = 0.0f;
			
		}
		for (int l123 = 0; (l123 < 2); l123 = (l123 + 1)) {
			fRec171[l123] = 0.0f;
			
		}
		for (int l124 = 0; (l124 < 2); l124 = (l124 + 1)) {
			fRec169[l124] = 0.0f;
			
		}
		for (int l125 = 0; (l125 < 2); l125 = (l125 + 1)) {
			fRec168[l125] = 0.0f;
			
		}
		for (int l126 = 0; (l126 < 2); l126 = (l126 + 1)) {
			fRec166[l126] = 0.0f;
			
		}
		for (int l127 = 0; (l127 < 2); l127 = (l127 + 1)) {
			fRec165[l127] = 0.0f;
			
		}
		for (int l128 = 0; (l128 < 2); l128 = (l128 + 1)) {
			fRec163[l128] = 0.0f;
			
		}
		for (int l129 = 0; (l129 < 2); l129 = (l129 + 1)) {
			fRec174[l129] = 0.0f;
			
		}
		for (int l130 = 0; (l130 < 2); l130 = (l130 + 1)) {
			fRec172[l130] = 0.0f;
			
		}
		for (int l131 = 0; (l131 < 2); l131 = (l131 + 1)) {
			fRec180[l131] = 0.0f;
			
		}
		for (int l132 = 0; (l132 < 2); l132 = (l132 + 1)) {
			fRec178[l132] = 0.0f;
			
		}
		for (int l133 = 0; (l133 < 2); l133 = (l133 + 1)) {
			fRec177[l133] = 0.0f;
			
		}
		for (int l134 = 0; (l134 < 2); l134 = (l134 + 1)) {
			fRec175[l134] = 0.0f;
			
		}
		for (int l135 = 0; (l135 < 1024); l135 = (l135 + 1)) {
			fVec8[l135] = 0.0f;
			
		}
		for (int l136 = 0; (l136 < 2); l136 = (l136 + 1)) {
			fRec189[l136] = 0.0f;
			
		}
		for (int l137 = 0; (l137 < 2); l137 = (l137 + 1)) {
			fRec187[l137] = 0.0f;
			
		}
		for (int l138 = 0; (l138 < 2); l138 = (l138 + 1)) {
			fRec186[l138] = 0.0f;
			
		}
		for (int l139 = 0; (l139 < 2); l139 = (l139 + 1)) {
			fRec184[l139] = 0.0f;
			
		}
		for (int l140 = 0; (l140 < 2); l140 = (l140 + 1)) {
			fRec183[l140] = 0.0f;
			
		}
		for (int l141 = 0; (l141 < 2); l141 = (l141 + 1)) {
			fRec181[l141] = 0.0f;
			
		}
		for (int l142 = 0; (l142 < 2); l142 = (l142 + 1)) {
			fRec192[l142] = 0.0f;
			
		}
		for (int l143 = 0; (l143 < 2); l143 = (l143 + 1)) {
			fRec190[l143] = 0.0f;
			
		}
		for (int l144 = 0; (l144 < 2); l144 = (l144 + 1)) {
			fRec198[l144] = 0.0f;
			
		}
		for (int l145 = 0; (l145 < 2); l145 = (l145 + 1)) {
			fRec196[l145] = 0.0f;
			
		}
		for (int l146 = 0; (l146 < 2); l146 = (l146 + 1)) {
			fRec195[l146] = 0.0f;
			
		}
		for (int l147 = 0; (l147 < 2); l147 = (l147 + 1)) {
			fRec193[l147] = 0.0f;
			
		}
		for (int l148 = 0; (l148 < 1024); l148 = (l148 + 1)) {
			fVec9[l148] = 0.0f;
			
		}
		for (int l149 = 0; (l149 < 2); l149 = (l149 + 1)) {
			fRec207[l149] = 0.0f;
			
		}
		for (int l150 = 0; (l150 < 2); l150 = (l150 + 1)) {
			fRec205[l150] = 0.0f;
			
		}
		for (int l151 = 0; (l151 < 2); l151 = (l151 + 1)) {
			fRec204[l151] = 0.0f;
			
		}
		for (int l152 = 0; (l152 < 2); l152 = (l152 + 1)) {
			fRec202[l152] = 0.0f;
			
		}
		for (int l153 = 0; (l153 < 2); l153 = (l153 + 1)) {
			fRec201[l153] = 0.0f;
			
		}
		for (int l154 = 0; (l154 < 2); l154 = (l154 + 1)) {
			fRec199[l154] = 0.0f;
			
		}
		for (int l155 = 0; (l155 < 2); l155 = (l155 + 1)) {
			fRec210[l155] = 0.0f;
			
		}
		for (int l156 = 0; (l156 < 2); l156 = (l156 + 1)) {
			fRec208[l156] = 0.0f;
			
		}
		for (int l157 = 0; (l157 < 2); l157 = (l157 + 1)) {
			fRec216[l157] = 0.0f;
			
		}
		for (int l158 = 0; (l158 < 2); l158 = (l158 + 1)) {
			fRec214[l158] = 0.0f;
			
		}
		for (int l159 = 0; (l159 < 2); l159 = (l159 + 1)) {
			fRec213[l159] = 0.0f;
			
		}
		for (int l160 = 0; (l160 < 2); l160 = (l160 + 1)) {
			fRec211[l160] = 0.0f;
			
		}
		for (int l161 = 0; (l161 < 1024); l161 = (l161 + 1)) {
			fVec10[l161] = 0.0f;
			
		}
		for (int l162 = 0; (l162 < 2); l162 = (l162 + 1)) {
			fRec225[l162] = 0.0f;
			
		}
		for (int l163 = 0; (l163 < 2); l163 = (l163 + 1)) {
			fRec223[l163] = 0.0f;
			
		}
		for (int l164 = 0; (l164 < 2); l164 = (l164 + 1)) {
			fRec222[l164] = 0.0f;
			
		}
		for (int l165 = 0; (l165 < 2); l165 = (l165 + 1)) {
			fRec220[l165] = 0.0f;
			
		}
		for (int l166 = 0; (l166 < 2); l166 = (l166 + 1)) {
			fRec219[l166] = 0.0f;
			
		}
		for (int l167 = 0; (l167 < 2); l167 = (l167 + 1)) {
			fRec217[l167] = 0.0f;
			
		}
		for (int l168 = 0; (l168 < 2); l168 = (l168 + 1)) {
			fRec228[l168] = 0.0f;
			
		}
		for (int l169 = 0; (l169 < 2); l169 = (l169 + 1)) {
			fRec226[l169] = 0.0f;
			
		}
		for (int l170 = 0; (l170 < 2); l170 = (l170 + 1)) {
			fRec234[l170] = 0.0f;
			
		}
		for (int l171 = 0; (l171 < 2); l171 = (l171 + 1)) {
			fRec232[l171] = 0.0f;
			
		}
		for (int l172 = 0; (l172 < 2); l172 = (l172 + 1)) {
			fRec231[l172] = 0.0f;
			
		}
		for (int l173 = 0; (l173 < 2); l173 = (l173 + 1)) {
			fRec229[l173] = 0.0f;
			
		}
		for (int l174 = 0; (l174 < 1024); l174 = (l174 + 1)) {
			fVec11[l174] = 0.0f;
			
		}
		for (int l175 = 0; (l175 < 2); l175 = (l175 + 1)) {
			fRec243[l175] = 0.0f;
			
		}
		for (int l176 = 0; (l176 < 2); l176 = (l176 + 1)) {
			fRec241[l176] = 0.0f;
			
		}
		for (int l177 = 0; (l177 < 2); l177 = (l177 + 1)) {
			fRec240[l177] = 0.0f;
			
		}
		for (int l178 = 0; (l178 < 2); l178 = (l178 + 1)) {
			fRec238[l178] = 0.0f;
			
		}
		for (int l179 = 0; (l179 < 2); l179 = (l179 + 1)) {
			fRec237[l179] = 0.0f;
			
		}
		for (int l180 = 0; (l180 < 2); l180 = (l180 + 1)) {
			fRec235[l180] = 0.0f;
			
		}
		for (int l181 = 0; (l181 < 2); l181 = (l181 + 1)) {
			fRec246[l181] = 0.0f;
			
		}
		for (int l182 = 0; (l182 < 2); l182 = (l182 + 1)) {
			fRec244[l182] = 0.0f;
			
		}
		for (int l183 = 0; (l183 < 2); l183 = (l183 + 1)) {
			fRec252[l183] = 0.0f;
			
		}
		for (int l184 = 0; (l184 < 2); l184 = (l184 + 1)) {
			fRec250[l184] = 0.0f;
			
		}
		for (int l185 = 0; (l185 < 2); l185 = (l185 + 1)) {
			fRec249[l185] = 0.0f;
			
		}
		for (int l186 = 0; (l186 < 2); l186 = (l186 + 1)) {
			fRec247[l186] = 0.0f;
			
		}
		for (int l187 = 0; (l187 < 1024); l187 = (l187 + 1)) {
			fVec12[l187] = 0.0f;
			
		}
		for (int l188 = 0; (l188 < 2); l188 = (l188 + 1)) {
			fRec261[l188] = 0.0f;
			
		}
		for (int l189 = 0; (l189 < 2); l189 = (l189 + 1)) {
			fRec259[l189] = 0.0f;
			
		}
		for (int l190 = 0; (l190 < 2); l190 = (l190 + 1)) {
			fRec258[l190] = 0.0f;
			
		}
		for (int l191 = 0; (l191 < 2); l191 = (l191 + 1)) {
			fRec256[l191] = 0.0f;
			
		}
		for (int l192 = 0; (l192 < 2); l192 = (l192 + 1)) {
			fRec255[l192] = 0.0f;
			
		}
		for (int l193 = 0; (l193 < 2); l193 = (l193 + 1)) {
			fRec253[l193] = 0.0f;
			
		}
		for (int l194 = 0; (l194 < 2); l194 = (l194 + 1)) {
			fRec264[l194] = 0.0f;
			
		}
		for (int l195 = 0; (l195 < 2); l195 = (l195 + 1)) {
			fRec262[l195] = 0.0f;
			
		}
		for (int l196 = 0; (l196 < 2); l196 = (l196 + 1)) {
			fRec270[l196] = 0.0f;
			
		}
		for (int l197 = 0; (l197 < 2); l197 = (l197 + 1)) {
			fRec268[l197] = 0.0f;
			
		}
		for (int l198 = 0; (l198 < 2); l198 = (l198 + 1)) {
			fRec267[l198] = 0.0f;
			
		}
		for (int l199 = 0; (l199 < 2); l199 = (l199 + 1)) {
			fRec265[l199] = 0.0f;
			
		}
		for (int l200 = 0; (l200 < 1024); l200 = (l200 + 1)) {
			fVec13[l200] = 0.0f;
			
		}
		for (int l201 = 0; (l201 < 2); l201 = (l201 + 1)) {
			fRec279[l201] = 0.0f;
			
		}
		for (int l202 = 0; (l202 < 2); l202 = (l202 + 1)) {
			fRec277[l202] = 0.0f;
			
		}
		for (int l203 = 0; (l203 < 2); l203 = (l203 + 1)) {
			fRec276[l203] = 0.0f;
			
		}
		for (int l204 = 0; (l204 < 2); l204 = (l204 + 1)) {
			fRec274[l204] = 0.0f;
			
		}
		for (int l205 = 0; (l205 < 2); l205 = (l205 + 1)) {
			fRec273[l205] = 0.0f;
			
		}
		for (int l206 = 0; (l206 < 2); l206 = (l206 + 1)) {
			fRec271[l206] = 0.0f;
			
		}
		for (int l207 = 0; (l207 < 2); l207 = (l207 + 1)) {
			fRec282[l207] = 0.0f;
			
		}
		for (int l208 = 0; (l208 < 2); l208 = (l208 + 1)) {
			fRec280[l208] = 0.0f;
			
		}
		for (int l209 = 0; (l209 < 2); l209 = (l209 + 1)) {
			fRec288[l209] = 0.0f;
			
		}
		for (int l210 = 0; (l210 < 2); l210 = (l210 + 1)) {
			fRec286[l210] = 0.0f;
			
		}
		for (int l211 = 0; (l211 < 2); l211 = (l211 + 1)) {
			fRec285[l211] = 0.0f;
			
		}
		for (int l212 = 0; (l212 < 2); l212 = (l212 + 1)) {
			fRec283[l212] = 0.0f;
			
		}
		for (int l213 = 0; (l213 < 1024); l213 = (l213 + 1)) {
			fVec14[l213] = 0.0f;
			
		}
		for (int l214 = 0; (l214 < 2); l214 = (l214 + 1)) {
			fRec297[l214] = 0.0f;
			
		}
		for (int l215 = 0; (l215 < 2); l215 = (l215 + 1)) {
			fRec295[l215] = 0.0f;
			
		}
		for (int l216 = 0; (l216 < 2); l216 = (l216 + 1)) {
			fRec294[l216] = 0.0f;
			
		}
		for (int l217 = 0; (l217 < 2); l217 = (l217 + 1)) {
			fRec292[l217] = 0.0f;
			
		}
		for (int l218 = 0; (l218 < 2); l218 = (l218 + 1)) {
			fRec291[l218] = 0.0f;
			
		}
		for (int l219 = 0; (l219 < 2); l219 = (l219 + 1)) {
			fRec289[l219] = 0.0f;
			
		}
		for (int l220 = 0; (l220 < 2); l220 = (l220 + 1)) {
			fRec300[l220] = 0.0f;
			
		}
		for (int l221 = 0; (l221 < 2); l221 = (l221 + 1)) {
			fRec298[l221] = 0.0f;
			
		}
		for (int l222 = 0; (l222 < 2); l222 = (l222 + 1)) {
			fRec306[l222] = 0.0f;
			
		}
		for (int l223 = 0; (l223 < 2); l223 = (l223 + 1)) {
			fRec304[l223] = 0.0f;
			
		}
		for (int l224 = 0; (l224 < 2); l224 = (l224 + 1)) {
			fRec303[l224] = 0.0f;
			
		}
		for (int l225 = 0; (l225 < 2); l225 = (l225 + 1)) {
			fRec301[l225] = 0.0f;
			
		}
		for (int l226 = 0; (l226 < 1024); l226 = (l226 + 1)) {
			fVec15[l226] = 0.0f;
			
		}
		for (int l227 = 0; (l227 < 2); l227 = (l227 + 1)) {
			fRec315[l227] = 0.0f;
			
		}
		for (int l228 = 0; (l228 < 2); l228 = (l228 + 1)) {
			fRec313[l228] = 0.0f;
			
		}
		for (int l229 = 0; (l229 < 2); l229 = (l229 + 1)) {
			fRec312[l229] = 0.0f;
			
		}
		for (int l230 = 0; (l230 < 2); l230 = (l230 + 1)) {
			fRec310[l230] = 0.0f;
			
		}
		for (int l231 = 0; (l231 < 2); l231 = (l231 + 1)) {
			fRec309[l231] = 0.0f;
			
		}
		for (int l232 = 0; (l232 < 2); l232 = (l232 + 1)) {
			fRec307[l232] = 0.0f;
			
		}
		for (int l233 = 0; (l233 < 2); l233 = (l233 + 1)) {
			fRec318[l233] = 0.0f;
			
		}
		for (int l234 = 0; (l234 < 2); l234 = (l234 + 1)) {
			fRec316[l234] = 0.0f;
			
		}
		for (int l235 = 0; (l235 < 2); l235 = (l235 + 1)) {
			fRec324[l235] = 0.0f;
			
		}
		for (int l236 = 0; (l236 < 2); l236 = (l236 + 1)) {
			fRec322[l236] = 0.0f;
			
		}
		for (int l237 = 0; (l237 < 2); l237 = (l237 + 1)) {
			fRec321[l237] = 0.0f;
			
		}
		for (int l238 = 0; (l238 < 2); l238 = (l238 + 1)) {
			fRec319[l238] = 0.0f;
			
		}
		for (int l239 = 0; (l239 < 1024); l239 = (l239 + 1)) {
			fVec16[l239] = 0.0f;
			
		}
		for (int l240 = 0; (l240 < 2); l240 = (l240 + 1)) {
			fRec333[l240] = 0.0f;
			
		}
		for (int l241 = 0; (l241 < 2); l241 = (l241 + 1)) {
			fRec331[l241] = 0.0f;
			
		}
		for (int l242 = 0; (l242 < 2); l242 = (l242 + 1)) {
			fRec330[l242] = 0.0f;
			
		}
		for (int l243 = 0; (l243 < 2); l243 = (l243 + 1)) {
			fRec328[l243] = 0.0f;
			
		}
		for (int l244 = 0; (l244 < 2); l244 = (l244 + 1)) {
			fRec327[l244] = 0.0f;
			
		}
		for (int l245 = 0; (l245 < 2); l245 = (l245 + 1)) {
			fRec325[l245] = 0.0f;
			
		}
		for (int l246 = 0; (l246 < 2); l246 = (l246 + 1)) {
			fRec336[l246] = 0.0f;
			
		}
		for (int l247 = 0; (l247 < 2); l247 = (l247 + 1)) {
			fRec334[l247] = 0.0f;
			
		}
		for (int l248 = 0; (l248 < 2); l248 = (l248 + 1)) {
			fRec342[l248] = 0.0f;
			
		}
		for (int l249 = 0; (l249 < 2); l249 = (l249 + 1)) {
			fRec340[l249] = 0.0f;
			
		}
		for (int l250 = 0; (l250 < 2); l250 = (l250 + 1)) {
			fRec339[l250] = 0.0f;
			
		}
		for (int l251 = 0; (l251 < 2); l251 = (l251 + 1)) {
			fRec337[l251] = 0.0f;
			
		}
		for (int l252 = 0; (l252 < 1024); l252 = (l252 + 1)) {
			fVec17[l252] = 0.0f;
			
		}
		for (int l253 = 0; (l253 < 2); l253 = (l253 + 1)) {
			fRec351[l253] = 0.0f;
			
		}
		for (int l254 = 0; (l254 < 2); l254 = (l254 + 1)) {
			fRec349[l254] = 0.0f;
			
		}
		for (int l255 = 0; (l255 < 2); l255 = (l255 + 1)) {
			fRec348[l255] = 0.0f;
			
		}
		for (int l256 = 0; (l256 < 2); l256 = (l256 + 1)) {
			fRec346[l256] = 0.0f;
			
		}
		for (int l257 = 0; (l257 < 2); l257 = (l257 + 1)) {
			fRec345[l257] = 0.0f;
			
		}
		for (int l258 = 0; (l258 < 2); l258 = (l258 + 1)) {
			fRec343[l258] = 0.0f;
			
		}
		for (int l259 = 0; (l259 < 2); l259 = (l259 + 1)) {
			fRec354[l259] = 0.0f;
			
		}
		for (int l260 = 0; (l260 < 2); l260 = (l260 + 1)) {
			fRec352[l260] = 0.0f;
			
		}
		for (int l261 = 0; (l261 < 2); l261 = (l261 + 1)) {
			fRec360[l261] = 0.0f;
			
		}
		for (int l262 = 0; (l262 < 2); l262 = (l262 + 1)) {
			fRec358[l262] = 0.0f;
			
		}
		for (int l263 = 0; (l263 < 2); l263 = (l263 + 1)) {
			fRec357[l263] = 0.0f;
			
		}
		for (int l264 = 0; (l264 < 2); l264 = (l264 + 1)) {
			fRec355[l264] = 0.0f;
			
		}
		for (int l265 = 0; (l265 < 1024); l265 = (l265 + 1)) {
			fVec18[l265] = 0.0f;
			
		}
		for (int l266 = 0; (l266 < 2); l266 = (l266 + 1)) {
			fRec363[l266] = 0.0f;
			
		}
		for (int l267 = 0; (l267 < 2); l267 = (l267 + 1)) {
			fRec361[l267] = 0.0f;
			
		}
		for (int l268 = 0; (l268 < 2); l268 = (l268 + 1)) {
			fRec369[l268] = 0.0f;
			
		}
		for (int l269 = 0; (l269 < 2); l269 = (l269 + 1)) {
			fRec367[l269] = 0.0f;
			
		}
		for (int l270 = 0; (l270 < 2); l270 = (l270 + 1)) {
			fRec366[l270] = 0.0f;
			
		}
		for (int l271 = 0; (l271 < 2); l271 = (l271 + 1)) {
			fRec364[l271] = 0.0f;
			
		}
		for (int l272 = 0; (l272 < 2); l272 = (l272 + 1)) {
			fRec378[l272] = 0.0f;
			
		}
		for (int l273 = 0; (l273 < 2); l273 = (l273 + 1)) {
			fRec376[l273] = 0.0f;
			
		}
		for (int l274 = 0; (l274 < 2); l274 = (l274 + 1)) {
			fRec375[l274] = 0.0f;
			
		}
		for (int l275 = 0; (l275 < 2); l275 = (l275 + 1)) {
			fRec373[l275] = 0.0f;
			
		}
		for (int l276 = 0; (l276 < 2); l276 = (l276 + 1)) {
			fRec372[l276] = 0.0f;
			
		}
		for (int l277 = 0; (l277 < 2); l277 = (l277 + 1)) {
			fRec370[l277] = 0.0f;
			
		}
		for (int l278 = 0; (l278 < 2); l278 = (l278 + 1)) {
			fVec19[l278] = 0.0f;
			
		}
		for (int l279 = 0; (l279 < 2); l279 = (l279 + 1)) {
			fRec387[l279] = 0.0f;
			
		}
		for (int l280 = 0; (l280 < 2); l280 = (l280 + 1)) {
			fRec385[l280] = 0.0f;
			
		}
		for (int l281 = 0; (l281 < 2); l281 = (l281 + 1)) {
			fRec384[l281] = 0.0f;
			
		}
		for (int l282 = 0; (l282 < 2); l282 = (l282 + 1)) {
			fRec382[l282] = 0.0f;
			
		}
		for (int l283 = 0; (l283 < 2); l283 = (l283 + 1)) {
			fRec381[l283] = 0.0f;
			
		}
		for (int l284 = 0; (l284 < 2); l284 = (l284 + 1)) {
			fRec379[l284] = 0.0f;
			
		}
		for (int l285 = 0; (l285 < 2); l285 = (l285 + 1)) {
			fRec390[l285] = 0.0f;
			
		}
		for (int l286 = 0; (l286 < 2); l286 = (l286 + 1)) {
			fRec388[l286] = 0.0f;
			
		}
		for (int l287 = 0; (l287 < 2); l287 = (l287 + 1)) {
			fRec396[l287] = 0.0f;
			
		}
		for (int l288 = 0; (l288 < 2); l288 = (l288 + 1)) {
			fRec394[l288] = 0.0f;
			
		}
		for (int l289 = 0; (l289 < 2); l289 = (l289 + 1)) {
			fRec393[l289] = 0.0f;
			
		}
		for (int l290 = 0; (l290 < 2); l290 = (l290 + 1)) {
			fRec391[l290] = 0.0f;
			
		}
		for (int l291 = 0; (l291 < 2); l291 = (l291 + 1)) {
			fRec405[l291] = 0.0f;
			
		}
		for (int l292 = 0; (l292 < 2); l292 = (l292 + 1)) {
			fRec403[l292] = 0.0f;
			
		}
		for (int l293 = 0; (l293 < 2); l293 = (l293 + 1)) {
			fRec402[l293] = 0.0f;
			
		}
		for (int l294 = 0; (l294 < 2); l294 = (l294 + 1)) {
			fRec400[l294] = 0.0f;
			
		}
		for (int l295 = 0; (l295 < 2); l295 = (l295 + 1)) {
			fRec399[l295] = 0.0f;
			
		}
		for (int l296 = 0; (l296 < 2); l296 = (l296 + 1)) {
			fRec397[l296] = 0.0f;
			
		}
		for (int l297 = 0; (l297 < 2); l297 = (l297 + 1)) {
			fRec411[l297] = 0.0f;
			
		}
		for (int l298 = 0; (l298 < 2); l298 = (l298 + 1)) {
			fRec409[l298] = 0.0f;
			
		}
		for (int l299 = 0; (l299 < 2); l299 = (l299 + 1)) {
			fRec408[l299] = 0.0f;
			
		}
		for (int l300 = 0; (l300 < 2); l300 = (l300 + 1)) {
			fRec406[l300] = 0.0f;
			
		}
		for (int l301 = 0; (l301 < 2); l301 = (l301 + 1)) {
			fRec414[l301] = 0.0f;
			
		}
		for (int l302 = 0; (l302 < 2); l302 = (l302 + 1)) {
			fRec412[l302] = 0.0f;
			
		}
		for (int l303 = 0; (l303 < 2); l303 = (l303 + 1)) {
			fRec417[l303] = 0.0f;
			
		}
		for (int l304 = 0; (l304 < 2); l304 = (l304 + 1)) {
			fRec415[l304] = 0.0f;
			
		}
		for (int l305 = 0; (l305 < 2); l305 = (l305 + 1)) {
			fRec423[l305] = 0.0f;
			
		}
		for (int l306 = 0; (l306 < 2); l306 = (l306 + 1)) {
			fRec421[l306] = 0.0f;
			
		}
		for (int l307 = 0; (l307 < 2); l307 = (l307 + 1)) {
			fRec420[l307] = 0.0f;
			
		}
		for (int l308 = 0; (l308 < 2); l308 = (l308 + 1)) {
			fRec418[l308] = 0.0f;
			
		}
		for (int l309 = 0; (l309 < 2); l309 = (l309 + 1)) {
			fRec432[l309] = 0.0f;
			
		}
		for (int l310 = 0; (l310 < 2); l310 = (l310 + 1)) {
			fRec430[l310] = 0.0f;
			
		}
		for (int l311 = 0; (l311 < 2); l311 = (l311 + 1)) {
			fRec429[l311] = 0.0f;
			
		}
		for (int l312 = 0; (l312 < 2); l312 = (l312 + 1)) {
			fRec427[l312] = 0.0f;
			
		}
		for (int l313 = 0; (l313 < 2); l313 = (l313 + 1)) {
			fRec426[l313] = 0.0f;
			
		}
		for (int l314 = 0; (l314 < 2); l314 = (l314 + 1)) {
			fRec424[l314] = 0.0f;
			
		}
		for (int l315 = 0; (l315 < 2); l315 = (l315 + 1)) {
			fVec20[l315] = 0.0f;
			
		}
		for (int l316 = 0; (l316 < 2); l316 = (l316 + 1)) {
			fRec441[l316] = 0.0f;
			
		}
		for (int l317 = 0; (l317 < 2); l317 = (l317 + 1)) {
			fRec439[l317] = 0.0f;
			
		}
		for (int l318 = 0; (l318 < 2); l318 = (l318 + 1)) {
			fRec438[l318] = 0.0f;
			
		}
		for (int l319 = 0; (l319 < 2); l319 = (l319 + 1)) {
			fRec436[l319] = 0.0f;
			
		}
		for (int l320 = 0; (l320 < 2); l320 = (l320 + 1)) {
			fRec435[l320] = 0.0f;
			
		}
		for (int l321 = 0; (l321 < 2); l321 = (l321 + 1)) {
			fRec433[l321] = 0.0f;
			
		}
		for (int l322 = 0; (l322 < 2); l322 = (l322 + 1)) {
			fRec444[l322] = 0.0f;
			
		}
		for (int l323 = 0; (l323 < 2); l323 = (l323 + 1)) {
			fRec442[l323] = 0.0f;
			
		}
		for (int l324 = 0; (l324 < 2); l324 = (l324 + 1)) {
			fRec450[l324] = 0.0f;
			
		}
		for (int l325 = 0; (l325 < 2); l325 = (l325 + 1)) {
			fRec448[l325] = 0.0f;
			
		}
		for (int l326 = 0; (l326 < 2); l326 = (l326 + 1)) {
			fRec447[l326] = 0.0f;
			
		}
		for (int l327 = 0; (l327 < 2); l327 = (l327 + 1)) {
			fRec445[l327] = 0.0f;
			
		}
		for (int l328 = 0; (l328 < 2); l328 = (l328 + 1)) {
			fRec459[l328] = 0.0f;
			
		}
		for (int l329 = 0; (l329 < 2); l329 = (l329 + 1)) {
			fRec457[l329] = 0.0f;
			
		}
		for (int l330 = 0; (l330 < 2); l330 = (l330 + 1)) {
			fRec456[l330] = 0.0f;
			
		}
		for (int l331 = 0; (l331 < 2); l331 = (l331 + 1)) {
			fRec454[l331] = 0.0f;
			
		}
		for (int l332 = 0; (l332 < 2); l332 = (l332 + 1)) {
			fRec453[l332] = 0.0f;
			
		}
		for (int l333 = 0; (l333 < 2); l333 = (l333 + 1)) {
			fRec451[l333] = 0.0f;
			
		}
		for (int l334 = 0; (l334 < 2); l334 = (l334 + 1)) {
			fRec462[l334] = 0.0f;
			
		}
		for (int l335 = 0; (l335 < 2); l335 = (l335 + 1)) {
			fRec460[l335] = 0.0f;
			
		}
		for (int l336 = 0; (l336 < 2); l336 = (l336 + 1)) {
			fRec468[l336] = 0.0f;
			
		}
		for (int l337 = 0; (l337 < 2); l337 = (l337 + 1)) {
			fRec466[l337] = 0.0f;
			
		}
		for (int l338 = 0; (l338 < 2); l338 = (l338 + 1)) {
			fRec465[l338] = 0.0f;
			
		}
		for (int l339 = 0; (l339 < 2); l339 = (l339 + 1)) {
			fRec463[l339] = 0.0f;
			
		}
		for (int l340 = 0; (l340 < 2); l340 = (l340 + 1)) {
			fRec477[l340] = 0.0f;
			
		}
		for (int l341 = 0; (l341 < 2); l341 = (l341 + 1)) {
			fRec475[l341] = 0.0f;
			
		}
		for (int l342 = 0; (l342 < 2); l342 = (l342 + 1)) {
			fRec474[l342] = 0.0f;
			
		}
		for (int l343 = 0; (l343 < 2); l343 = (l343 + 1)) {
			fRec472[l343] = 0.0f;
			
		}
		for (int l344 = 0; (l344 < 2); l344 = (l344 + 1)) {
			fRec471[l344] = 0.0f;
			
		}
		for (int l345 = 0; (l345 < 2); l345 = (l345 + 1)) {
			fRec469[l345] = 0.0f;
			
		}
		for (int l346 = 0; (l346 < 2); l346 = (l346 + 1)) {
			fRec480[l346] = 0.0f;
			
		}
		for (int l347 = 0; (l347 < 2); l347 = (l347 + 1)) {
			fRec478[l347] = 0.0f;
			
		}
		for (int l348 = 0; (l348 < 2); l348 = (l348 + 1)) {
			fRec486[l348] = 0.0f;
			
		}
		for (int l349 = 0; (l349 < 2); l349 = (l349 + 1)) {
			fRec484[l349] = 0.0f;
			
		}
		for (int l350 = 0; (l350 < 2); l350 = (l350 + 1)) {
			fRec483[l350] = 0.0f;
			
		}
		for (int l351 = 0; (l351 < 2); l351 = (l351 + 1)) {
			fRec481[l351] = 0.0f;
			
		}
		for (int l352 = 0; (l352 < 2); l352 = (l352 + 1)) {
			fVec21[l352] = 0.0f;
			
		}
		for (int l353 = 0; (l353 < 2); l353 = (l353 + 1)) {
			fRec489[l353] = 0.0f;
			
		}
		for (int l354 = 0; (l354 < 2); l354 = (l354 + 1)) {
			fRec487[l354] = 0.0f;
			
		}
		for (int l355 = 0; (l355 < 2); l355 = (l355 + 1)) {
			fRec495[l355] = 0.0f;
			
		}
		for (int l356 = 0; (l356 < 2); l356 = (l356 + 1)) {
			fRec493[l356] = 0.0f;
			
		}
		for (int l357 = 0; (l357 < 2); l357 = (l357 + 1)) {
			fRec492[l357] = 0.0f;
			
		}
		for (int l358 = 0; (l358 < 2); l358 = (l358 + 1)) {
			fRec490[l358] = 0.0f;
			
		}
		for (int l359 = 0; (l359 < 2); l359 = (l359 + 1)) {
			fRec504[l359] = 0.0f;
			
		}
		for (int l360 = 0; (l360 < 2); l360 = (l360 + 1)) {
			fRec502[l360] = 0.0f;
			
		}
		for (int l361 = 0; (l361 < 2); l361 = (l361 + 1)) {
			fRec501[l361] = 0.0f;
			
		}
		for (int l362 = 0; (l362 < 2); l362 = (l362 + 1)) {
			fRec499[l362] = 0.0f;
			
		}
		for (int l363 = 0; (l363 < 2); l363 = (l363 + 1)) {
			fRec498[l363] = 0.0f;
			
		}
		for (int l364 = 0; (l364 < 2); l364 = (l364 + 1)) {
			fRec496[l364] = 0.0f;
			
		}
		for (int l365 = 0; (l365 < 2); l365 = (l365 + 1)) {
			fRec507[l365] = 0.0f;
			
		}
		for (int l366 = 0; (l366 < 2); l366 = (l366 + 1)) {
			fRec505[l366] = 0.0f;
			
		}
		for (int l367 = 0; (l367 < 2); l367 = (l367 + 1)) {
			fRec513[l367] = 0.0f;
			
		}
		for (int l368 = 0; (l368 < 2); l368 = (l368 + 1)) {
			fRec511[l368] = 0.0f;
			
		}
		for (int l369 = 0; (l369 < 2); l369 = (l369 + 1)) {
			fRec510[l369] = 0.0f;
			
		}
		for (int l370 = 0; (l370 < 2); l370 = (l370 + 1)) {
			fRec508[l370] = 0.0f;
			
		}
		for (int l371 = 0; (l371 < 2); l371 = (l371 + 1)) {
			fRec522[l371] = 0.0f;
			
		}
		for (int l372 = 0; (l372 < 2); l372 = (l372 + 1)) {
			fRec520[l372] = 0.0f;
			
		}
		for (int l373 = 0; (l373 < 2); l373 = (l373 + 1)) {
			fRec519[l373] = 0.0f;
			
		}
		for (int l374 = 0; (l374 < 2); l374 = (l374 + 1)) {
			fRec517[l374] = 0.0f;
			
		}
		for (int l375 = 0; (l375 < 2); l375 = (l375 + 1)) {
			fRec516[l375] = 0.0f;
			
		}
		for (int l376 = 0; (l376 < 2); l376 = (l376 + 1)) {
			fRec514[l376] = 0.0f;
			
		}
		for (int l377 = 0; (l377 < 2); l377 = (l377 + 1)) {
			fRec525[l377] = 0.0f;
			
		}
		for (int l378 = 0; (l378 < 2); l378 = (l378 + 1)) {
			fRec523[l378] = 0.0f;
			
		}
		for (int l379 = 0; (l379 < 2); l379 = (l379 + 1)) {
			fRec531[l379] = 0.0f;
			
		}
		for (int l380 = 0; (l380 < 2); l380 = (l380 + 1)) {
			fRec529[l380] = 0.0f;
			
		}
		for (int l381 = 0; (l381 < 2); l381 = (l381 + 1)) {
			fRec528[l381] = 0.0f;
			
		}
		for (int l382 = 0; (l382 < 2); l382 = (l382 + 1)) {
			fRec526[l382] = 0.0f;
			
		}
		for (int l383 = 0; (l383 < 2); l383 = (l383 + 1)) {
			fRec540[l383] = 0.0f;
			
		}
		for (int l384 = 0; (l384 < 2); l384 = (l384 + 1)) {
			fRec538[l384] = 0.0f;
			
		}
		for (int l385 = 0; (l385 < 2); l385 = (l385 + 1)) {
			fRec537[l385] = 0.0f;
			
		}
		for (int l386 = 0; (l386 < 2); l386 = (l386 + 1)) {
			fRec535[l386] = 0.0f;
			
		}
		for (int l387 = 0; (l387 < 2); l387 = (l387 + 1)) {
			fRec534[l387] = 0.0f;
			
		}
		for (int l388 = 0; (l388 < 2); l388 = (l388 + 1)) {
			fRec532[l388] = 0.0f;
			
		}
		for (int l389 = 0; (l389 < 2); l389 = (l389 + 1)) {
			fVec22[l389] = 0.0f;
			
		}
		for (int l390 = 0; (l390 < 2); l390 = (l390 + 1)) {
			fRec543[l390] = 0.0f;
			
		}
		for (int l391 = 0; (l391 < 2); l391 = (l391 + 1)) {
			fRec541[l391] = 0.0f;
			
		}
		for (int l392 = 0; (l392 < 2); l392 = (l392 + 1)) {
			fRec549[l392] = 0.0f;
			
		}
		for (int l393 = 0; (l393 < 2); l393 = (l393 + 1)) {
			fRec547[l393] = 0.0f;
			
		}
		for (int l394 = 0; (l394 < 2); l394 = (l394 + 1)) {
			fRec546[l394] = 0.0f;
			
		}
		for (int l395 = 0; (l395 < 2); l395 = (l395 + 1)) {
			fRec544[l395] = 0.0f;
			
		}
		for (int l396 = 0; (l396 < 2); l396 = (l396 + 1)) {
			fRec558[l396] = 0.0f;
			
		}
		for (int l397 = 0; (l397 < 2); l397 = (l397 + 1)) {
			fRec556[l397] = 0.0f;
			
		}
		for (int l398 = 0; (l398 < 2); l398 = (l398 + 1)) {
			fRec555[l398] = 0.0f;
			
		}
		for (int l399 = 0; (l399 < 2); l399 = (l399 + 1)) {
			fRec553[l399] = 0.0f;
			
		}
		for (int l400 = 0; (l400 < 2); l400 = (l400 + 1)) {
			fRec552[l400] = 0.0f;
			
		}
		for (int l401 = 0; (l401 < 2); l401 = (l401 + 1)) {
			fRec550[l401] = 0.0f;
			
		}
		for (int l402 = 0; (l402 < 2); l402 = (l402 + 1)) {
			fRec561[l402] = 0.0f;
			
		}
		for (int l403 = 0; (l403 < 2); l403 = (l403 + 1)) {
			fRec559[l403] = 0.0f;
			
		}
		for (int l404 = 0; (l404 < 2); l404 = (l404 + 1)) {
			fRec567[l404] = 0.0f;
			
		}
		for (int l405 = 0; (l405 < 2); l405 = (l405 + 1)) {
			fRec565[l405] = 0.0f;
			
		}
		for (int l406 = 0; (l406 < 2); l406 = (l406 + 1)) {
			fRec564[l406] = 0.0f;
			
		}
		for (int l407 = 0; (l407 < 2); l407 = (l407 + 1)) {
			fRec562[l407] = 0.0f;
			
		}
		for (int l408 = 0; (l408 < 2); l408 = (l408 + 1)) {
			fRec576[l408] = 0.0f;
			
		}
		for (int l409 = 0; (l409 < 2); l409 = (l409 + 1)) {
			fRec574[l409] = 0.0f;
			
		}
		for (int l410 = 0; (l410 < 2); l410 = (l410 + 1)) {
			fRec573[l410] = 0.0f;
			
		}
		for (int l411 = 0; (l411 < 2); l411 = (l411 + 1)) {
			fRec571[l411] = 0.0f;
			
		}
		for (int l412 = 0; (l412 < 2); l412 = (l412 + 1)) {
			fRec570[l412] = 0.0f;
			
		}
		for (int l413 = 0; (l413 < 2); l413 = (l413 + 1)) {
			fRec568[l413] = 0.0f;
			
		}
		
	}
	
	virtual void init(int samplingFreq) {
		classInit(samplingFreq);
		instanceInit(samplingFreq);
	}
	virtual void instanceInit(int samplingFreq) {
		instanceConstants(samplingFreq);
		instanceResetUserInterface();
		instanceClear();
	}
	
	virtual mydsp* clone() {
		return new mydsp();
	}
	virtual int getSampleRate() {
		return fSamplingFreq;
		
	}
	
	virtual void buildUserInterface(UI* ui_interface) {
		ui_interface->openVerticalBox("SATOswDo3");
		ui_interface->declare(&fHslider0, "unit", "dB");
		ui_interface->addHorizontalSlider("gain", &fHslider0, -10.0f, -30.0f, 10.0f, 1.0f);
		ui_interface->declare(&fHslider2, "unit", "dB");
		ui_interface->addHorizontalSlider("lf/hf", &fHslider2, 0.0f, -3.0f, 3.0f, 0.100000001f);
		ui_interface->addCheckButton("mute", &fCheckbox0);
		ui_interface->declare(&fHslider1, "unit", "Hz");
		ui_interface->addHorizontalSlider("xover", &fHslider1, 400.0f, 200.0f, 800.0f, 20.0f);
		ui_interface->closeBox();
		
	}
	
	virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) {
		FAUSTFLOAT* input0 = inputs[0];
		FAUSTFLOAT* input1 = inputs[1];
		FAUSTFLOAT* input2 = inputs[2];
		FAUSTFLOAT* input3 = inputs[3];
		FAUSTFLOAT* input4 = inputs[4];
		FAUSTFLOAT* input5 = inputs[5];
		FAUSTFLOAT* input6 = inputs[6];
		FAUSTFLOAT* input7 = inputs[7];
		FAUSTFLOAT* input8 = inputs[8];
		FAUSTFLOAT* input9 = inputs[9];
		FAUSTFLOAT* input10 = inputs[10];
		FAUSTFLOAT* input11 = inputs[11];
		FAUSTFLOAT* input12 = inputs[12];
		FAUSTFLOAT* input13 = inputs[13];
		FAUSTFLOAT* input14 = inputs[14];
		FAUSTFLOAT* input15 = inputs[15];
		FAUSTFLOAT* output0 = outputs[0];
		FAUSTFLOAT* output1 = outputs[1];
		FAUSTFLOAT* output2 = outputs[2];
		FAUSTFLOAT* output3 = outputs[3];
		FAUSTFLOAT* output4 = outputs[4];
		FAUSTFLOAT* output5 = outputs[5];
		FAUSTFLOAT* output6 = outputs[6];
		FAUSTFLOAT* output7 = outputs[7];
		FAUSTFLOAT* output8 = outputs[8];
		FAUSTFLOAT* output9 = outputs[9];
		FAUSTFLOAT* output10 = outputs[10];
		FAUSTFLOAT* output11 = outputs[11];
		FAUSTFLOAT* output12 = outputs[12];
		FAUSTFLOAT* output13 = outputs[13];
		FAUSTFLOAT* output14 = outputs[14];
		FAUSTFLOAT* output15 = outputs[15];
		FAUSTFLOAT* output16 = outputs[16];
		FAUSTFLOAT* output17 = outputs[17];
		FAUSTFLOAT* output18 = outputs[18];
		FAUSTFLOAT* output19 = outputs[19];
		FAUSTFLOAT* output20 = outputs[20];
		FAUSTFLOAT* output21 = outputs[21];
		FAUSTFLOAT* output22 = outputs[22];
		FAUSTFLOAT* output23 = outputs[23];
		FAUSTFLOAT* output24 = outputs[24];
		FAUSTFLOAT* output25 = outputs[25];
		FAUSTFLOAT* output26 = outputs[26];
		FAUSTFLOAT* output27 = outputs[27];
		FAUSTFLOAT* output28 = outputs[28];
		FAUSTFLOAT* output29 = outputs[29];
		FAUSTFLOAT* output30 = outputs[30];
		float fSlow0 = (0.00100000005f * (float((float(fCheckbox0) < 0.5f)) * powf(10.0f, (0.0500000007f * float(fHslider0)))));
		float fSlow1 = (0.00100000005f * float(fHslider1));
		float fSlow2 = (0.00100000005f * powf(10.0f, (0.0500000007f * float(fHslider2))));
		for (int i = 0; (i < count); i = (i + 1)) {
			fRec0[0] = (fSlow0 + (0.999000013f * fRec0[1]));
			fRec1[0] = (fSlow1 + (0.999000013f * fRec1[1]));
			float fTemp0 = tanf((fConst1 * fRec1[0]));
			float fTemp1 = mydsp_faustpower2_f(fTemp0);
			float fTemp2 = ((fTemp0 * (fTemp0 + -2.0f)) + 1.0f);
			float fTemp3 = (fTemp1 + -1.0f);
			float fTemp4 = ((fTemp0 * (fTemp0 + 2.0f)) + 1.0f);
			fRec2[0] = (float(input0[i]) - (((fRec2[2] * fTemp2) + (2.0f * (fRec2[1] * fTemp3))) / fTemp4));
			fRec3[0] = (fSlow2 + (0.999000013f * fRec3[1]));
			float fTemp5 = (fRec3[0] * fTemp4);
			float fTemp6 = (0.0f - (2.0f / fTemp4));
			float fTemp7 = (((fTemp1 * (fRec2[2] + (fRec2[0] + (2.0f * fRec2[1])))) / fTemp5) + (fRec3[0] * (0.0f - ((fRec2[1] * fTemp6) + ((fRec2[0] + fRec2[2]) / fTemp4)))));
			float fTemp8 = (fConst4 * fRec4[1]);
			fRec13[0] = (float(input10[i]) - (((fTemp2 * fRec13[2]) + (2.0f * (fTemp3 * fRec13[1]))) / fTemp4));
			float fTemp9 = (((fTemp1 * (fRec13[2] + (fRec13[0] + (2.0f * fRec13[1])))) / fTemp5) + (0.304746985f * (fRec3[0] * (0.0f - ((fTemp6 * fRec13[1]) + ((fRec13[0] + fRec13[2]) / fTemp4))))));
			fRec14[0] = (float(input12[i]) - (((fTemp2 * fRec14[2]) + (2.0f * (fTemp3 * fRec14[1]))) / fTemp4));
			float fTemp10 = (((fTemp1 * (fRec14[2] + (fRec14[0] + (2.0f * fRec14[1])))) / fTemp5) + (0.304746985f * (fRec3[0] * (0.0f - ((fTemp6 * fRec14[1]) + ((fRec14[0] + fRec14[2]) / fTemp4))))));
			fRec15[0] = (float(input13[i]) - (((fTemp2 * fRec15[2]) + (2.0f * (fTemp3 * fRec15[1]))) / fTemp4));
			float fTemp11 = (((fTemp1 * (fRec15[2] + (fRec15[0] + (2.0f * fRec15[1])))) / fTemp5) + (0.304746985f * (fRec3[0] * (0.0f - ((fTemp6 * fRec15[1]) + ((fRec15[0] + fRec15[2]) / fTemp4))))));
			fRec16[0] = (float(input14[i]) - (((fTemp2 * fRec16[2]) + (2.0f * (fTemp3 * fRec16[1]))) / fTemp4));
			float fTemp12 = (((fTemp1 * (fRec16[2] + (fRec16[0] + (2.0f * fRec16[1])))) / fTemp5) + (0.304746985f * (fRec3[0] * (0.0f - ((fTemp6 * fRec16[1]) + ((fRec16[0] + fRec16[2]) / fTemp4))))));
			fRec17[0] = (float(input9[i]) - (((fTemp2 * fRec17[2]) + (2.0f * (fTemp3 * fRec17[1]))) / fTemp4));
			float fTemp13 = (((fTemp1 * (fRec17[2] + (fRec17[0] + (2.0f * fRec17[1])))) / fTemp5) + (0.304746985f * (fRec3[0] * (0.0f - ((fTemp6 * fRec17[1]) + ((fRec17[0] + fRec17[2]) / fTemp4))))));
			fRec18[0] = (float(input11[i]) - (((fTemp2 * fRec18[2]) + (2.0f * (fTemp3 * fRec18[1]))) / fTemp4));
			float fTemp14 = (((fTemp1 * (fRec18[2] + (fRec18[0] + (2.0f * fRec18[1])))) / fTemp5) + (0.304746985f * (fRec3[0] * (0.0f - ((fTemp6 * fRec18[1]) + ((fRec18[0] + fRec18[2]) / fTemp4))))));
			fRec19[0] = (float(input15[i]) - (((fTemp2 * fRec19[2]) + (2.0f * (fTemp3 * fRec19[1]))) / fTemp4));
			float fTemp15 = (((fTemp1 * (fRec19[2] + (fRec19[0] + (2.0f * fRec19[1])))) / fTemp5) + (0.304746985f * (fRec3[0] * (0.0f - ((fTemp6 * fRec19[1]) + ((fRec19[0] + fRec19[2]) / fTemp4))))));
			float fTemp16 = (fConst6 * (((((2.55382001e-05f * fTemp9) + (0.102538198f * fTemp10)) + (3.89539991e-06f * fTemp11)) + (5.84349982e-06f * fTemp12)) - (((1.65499998e-07f * fTemp13) + (1.23203999e-05f * fTemp14)) + (1.21899996e-06f * fTemp15))));
			float fTemp17 = (fConst8 * fRec7[1]);
			float fTemp18 = (fConst9 * fRec10[1]);
			fRec12[0] = (fTemp16 + (fTemp17 + (fRec12[1] + fTemp18)));
			fRec10[0] = fRec12[0];
			float fRec11 = ((fTemp18 + fTemp17) + fTemp16);
			fRec9[0] = (fRec10[0] + fRec9[1]);
			fRec7[0] = fRec9[0];
			float fRec8 = fRec11;
			fRec6[0] = (fTemp8 + (fRec8 + fRec6[1]));
			fRec4[0] = fRec6[0];
			float fRec5 = (fRec8 + fTemp8);
			fRec23[0] = (float(input2[i]) - (((fTemp2 * fRec23[2]) + (2.0f * (fTemp3 * fRec23[1]))) / fTemp4));
			float fTemp19 = (((fTemp1 * (fRec23[2] + (fRec23[0] + (2.0f * fRec23[1])))) / fTemp5) + (0.861136317f * (fRec3[0] * (0.0f - ((fTemp6 * fRec23[1]) + ((fRec23[0] + fRec23[2]) / fTemp4))))));
			fRec24[0] = (float(input3[i]) - (((fTemp2 * fRec24[2]) + (2.0f * (fTemp3 * fRec24[1]))) / fTemp4));
			float fTemp20 = (((fTemp1 * (fRec24[2] + (fRec24[0] + (2.0f * fRec24[1])))) / fTemp5) + (0.861136317f * (fRec3[0] * (0.0f - ((fTemp6 * fRec24[1]) + ((fRec24[0] + fRec24[2]) / fTemp4))))));
			fRec25[0] = (float(input1[i]) - (((fTemp2 * fRec25[2]) + (2.0f * (fTemp3 * fRec25[1]))) / fTemp4));
			float fTemp21 = (((fTemp1 * (fRec25[2] + (fRec25[0] + (2.0f * fRec25[1])))) / fTemp5) + (0.861136317f * (fRec3[0] * (0.0f - ((fTemp6 * fRec25[1]) + ((fRec25[0] + fRec25[2]) / fTemp4))))));
			float fTemp22 = (fConst11 * (((0.116276413f * fTemp19) + (2.27310011e-06f * fTemp20)) - (8.45809973e-06f * fTemp21)));
			float fTemp23 = (fConst12 * fRec20[1]);
			fRec22[0] = (fTemp22 + (fRec22[1] + fTemp23));
			fRec20[0] = fRec22[0];
			float fRec21 = (fTemp23 + fTemp22);
			fRec32[0] = (float(input4[i]) - (((fTemp2 * fRec32[2]) + (2.0f * (fTemp3 * fRec32[1]))) / fTemp4));
			float fTemp24 = (((fTemp1 * (fRec32[2] + (fRec32[0] + (2.0f * fRec32[1])))) / fTemp5) + (0.612333596f * (fRec3[0] * (0.0f - ((fTemp6 * fRec32[1]) + ((fRec32[0] + fRec32[2]) / fTemp4))))));
			fRec33[0] = (float(input6[i]) - (((fTemp2 * fRec33[2]) + (2.0f * (fTemp3 * fRec33[1]))) / fTemp4));
			float fTemp25 = (((fTemp1 * (fRec33[2] + (fRec33[0] + (2.0f * fRec33[1])))) / fTemp5) + (0.612333596f * (fRec3[0] * (0.0f - ((fTemp6 * fRec33[1]) + ((fRec33[0] + fRec33[2]) / fTemp4))))));
			fRec34[0] = (float(input7[i]) - (((fTemp2 * fRec34[2]) + (2.0f * (fTemp3 * fRec34[1]))) / fTemp4));
			float fTemp26 = (((fTemp1 * (fRec34[2] + (fRec34[0] + (2.0f * fRec34[1])))) / fTemp5) + (0.612333596f * (fRec3[0] * (0.0f - ((fTemp6 * fRec34[1]) + ((fRec34[0] + fRec34[2]) / fTemp4))))));
			fRec35[0] = (float(input8[i]) - (((fTemp2 * fRec35[2]) + (2.0f * (fTemp3 * fRec35[1]))) / fTemp4));
			float fTemp27 = (((fTemp1 * (fRec35[2] + (fRec35[0] + (2.0f * fRec35[1])))) / fTemp5) + (0.612333596f * (fRec3[0] * (0.0f - ((fTemp6 * fRec35[1]) + ((fRec35[0] + fRec35[2]) / fTemp4))))));
			fRec36[0] = (float(input5[i]) - (((fTemp2 * fRec36[2]) + (2.0f * (fTemp3 * fRec36[1]))) / fTemp4));
			float fTemp28 = (((fTemp1 * (fRec36[2] + (fRec36[0] + (2.0f * fRec36[1])))) / fTemp5) + (0.612333596f * (fRec3[0] * (0.0f - ((fTemp6 * fRec36[1]) + ((fRec36[0] + fRec36[2]) / fTemp4))))));
			float fTemp29 = (fConst14 * (((((1.26164996e-05f * fTemp24) + (0.121653795f * fTemp25)) + (3.81099994e-06f * fTemp26)) + (3.20460003e-06f * fTemp27)) - (1.34709999e-05f * fTemp28)));
			float fTemp30 = (fConst15 * fRec26[1]);
			float fTemp31 = (fConst16 * fRec29[1]);
			fRec31[0] = (fTemp29 + (fTemp30 + (fRec31[1] + fTemp31)));
			fRec29[0] = fRec31[0];
			float fRec30 = ((fTemp31 + fTemp30) + fTemp29);
			fRec28[0] = (fRec29[0] + fRec28[1]);
			fRec26[0] = fRec28[0];
			float fRec27 = fRec30;
			fVec0[(IOTA & 2047)] = ((0.074275896f * fTemp7) + (fRec5 + (fRec21 + fRec27)));
			output0[i] = FAUSTFLOAT((0.752947509f * (fRec0[0] * fVec0[((IOTA - iConst17) & 2047)])));
			float fTemp32 = (fConst19 * fRec40[1]);
			float fTemp33 = (fConst20 * fRec43[1]);
			float fTemp34 = (fConst22 * ((((0.0338524766f * fTemp9) + (0.0587121285f * fTemp14)) + (0.0167919695f * fTemp11)) - ((((0.00809382275f * fTemp13) + (0.0106742699f * fTemp10)) + (0.0429489762f * fTemp12)) + (0.0165827759f * fTemp15))));
			fRec45[0] = (fTemp32 + (fTemp33 + (fTemp34 + fRec45[1])));
			fRec43[0] = fRec45[0];
			float fRec44 = ((fTemp34 + fTemp33) + fTemp32);
			fRec42[0] = (fRec43[0] + fRec42[1]);
			fRec40[0] = fRec42[0];
			float fRec41 = fRec44;
			float fTemp35 = (fConst23 * fRec37[1]);
			fRec39[0] = ((fRec41 + fRec39[1]) + fTemp35);
			fRec37[0] = fRec39[0];
			float fRec38 = (fRec41 + fTemp35);
			float fTemp36 = (fConst25 * (((0.0502111912f * fTemp21) + (0.0583267435f * fTemp19)) + (0.0173864271f * fTemp20)));
			float fTemp37 = (fConst26 * fRec46[1]);
			fRec48[0] = (fTemp36 + (fRec48[1] + fTemp37));
			fRec46[0] = fRec48[0];
			float fRec47 = (fTemp37 + fTemp36);
			float fTemp38 = (fConst28 * (((((0.021890806f * fTemp24) + (0.0733522624f * fTemp28)) + (0.0276794825f * fTemp25)) + (0.0240465272f * fTemp26)) - (0.0258579031f * fTemp27)));
			float fTemp39 = (fConst29 * fRec49[1]);
			float fTemp40 = (fConst30 * fRec52[1]);
			fRec54[0] = (fTemp38 + (fTemp39 + (fRec54[1] + fTemp40)));
			fRec52[0] = fRec54[0];
			float fRec53 = ((fTemp40 + fTemp39) + fTemp38);
			fRec51[0] = (fRec52[0] + fRec51[1]);
			fRec49[0] = fRec51[0];
			float fRec50 = fRec53;
			fVec1[(IOTA & 1023)] = ((fRec38 + (fRec47 + fRec50)) + (0.0487409718f * fTemp7));
			output1[i] = FAUSTFLOAT((0.810182214f * (fVec1[((IOTA - iConst31) & 1023)] * fRec0[0])));
			float fTemp41 = (fConst33 * fRec55[1]);
			float fTemp42 = (fConst35 * (((0.0122100972f * fTemp15) + (((0.0122129228f * fTemp13) + (0.0442641154f * fTemp14)) + (6.1572e-06f * fTemp12))) - (((0.0582878366f * fTemp9) + (0.016099954f * fTemp10)) + (0.044274468f * fTemp11))));
			float fTemp43 = (fConst36 * fRec58[1]);
			float fTemp44 = (fConst37 * fRec61[1]);
			fRec63[0] = (fTemp42 + (fTemp43 + (fRec63[1] + fTemp44)));
			fRec61[0] = fRec63[0];
			float fRec62 = ((fTemp44 + fTemp43) + fTemp42);
			fRec60[0] = (fRec60[1] + fRec61[0]);
			fRec58[0] = fRec60[0];
			float fRec59 = fRec62;
			fRec57[0] = ((fRec57[1] + fTemp41) + fRec59);
			fRec55[0] = fRec57[0];
			float fRec56 = (fTemp41 + fRec59);
			float fTemp45 = (fConst39 * (((0.0419778749f * fTemp21) + (0.0627192035f * fTemp19)) - (0.0419842787f * fTemp20)));
			float fTemp46 = (fConst40 * fRec64[1]);
			fRec66[0] = (fTemp45 + (fRec66[1] + fTemp46));
			fRec64[0] = fRec66[0];
			float fRec65 = (fTemp46 + fTemp45);
			float fTemp47 = (fConst42 * ((((0.0594701208f * fTemp28) + (0.0264332425f * fTemp25)) + (4.01599982e-06f * fTemp27)) - ((0.036625877f * fTemp24) + (0.0594800413f * fTemp26))));
			float fTemp48 = (fConst43 * fRec67[1]);
			float fTemp49 = (fConst44 * fRec70[1]);
			fRec72[0] = (fTemp47 + (fTemp48 + (fRec72[1] + fTemp49)));
			fRec70[0] = fRec72[0];
			float fRec71 = ((fTemp49 + fTemp48) + fTemp47);
			fRec69[0] = (fRec69[1] + fRec70[0]);
			fRec67[0] = fRec69[0];
			float fRec68 = fRec71;
			fVec2[(IOTA & 1023)] = ((0.053797558f * fTemp7) + ((fRec56 + fRec65) + fRec68));
			output2[i] = FAUSTFLOAT((0.809967816f * (fVec2[((IOTA - iConst45) & 1023)] * fRec0[0])));
			float fTemp50 = (fConst25 * ((0.0539395995f * fTemp19) - ((0.0120689478f * fTemp21) + (0.0450384244f * fTemp20))));
			float fTemp51 = (fConst26 * fRec73[1]);
			fRec75[0] = (fTemp50 + (fRec75[1] + fTemp51));
			fRec73[0] = fRec75[0];
			float fRec74 = (fTemp51 + fTemp50);
			float fTemp52 = (fConst28 * ((((0.0150248362f * fTemp24) + (0.0289373379f * fTemp25)) + (0.0260208491f * fTemp27)) - ((0.0181305222f * fTemp28) + (0.0676586777f * fTemp26))));
			float fTemp53 = (fConst29 * fRec76[1]);
			float fTemp54 = (fConst30 * fRec79[1]);
			fRec81[0] = (fTemp52 + (fTemp53 + (fRec81[1] + fTemp54)));
			fRec79[0] = fRec81[0];
			float fRec80 = ((fTemp54 + fTemp53) + fTemp52);
			fRec78[0] = (fRec78[1] + fRec79[0]);
			fRec76[0] = fRec78[0];
			float fRec77 = fRec80;
			float fTemp55 = (fConst23 * fRec82[1]);
			float fTemp56 = (fConst22 * (((0.0249754917f * fTemp9) + (0.0432558395f * fTemp12)) - (((((0.0124645401f * fTemp13) + (0.0154083548f * fTemp14)) + (0.00523858657f * fTemp10)) + (0.0574972518f * fTemp11)) + (0.0124602364f * fTemp15))));
			float fTemp57 = (fConst19 * fRec85[1]);
			float fTemp58 = (fConst20 * fRec88[1]);
			fRec90[0] = (fTemp56 + (fTemp57 + (fRec90[1] + fTemp58)));
			fRec88[0] = fRec90[0];
			float fRec89 = ((fTemp58 + fTemp57) + fTemp56);
			fRec87[0] = (fRec87[1] + fRec88[0]);
			fRec85[0] = fRec87[0];
			float fRec86 = fRec89;
			fRec84[0] = ((fRec84[1] + fTemp55) + fRec86);
			fRec82[0] = fRec84[0];
			float fRec83 = (fTemp55 + fRec86);
			fVec3[(IOTA & 1023)] = ((0.0436849333f * fTemp7) + ((fRec74 + fRec77) + fRec83));
			output3[i] = FAUSTFLOAT((0.810182214f * (fVec3[((IOTA - iConst31) & 1023)] * fRec0[0])));
			float fTemp59 = (fConst25 * ((0.0583168715f * fTemp19) - ((0.050208047f * fTemp21) + (0.0173970331f * fTemp20))));
			float fTemp60 = (fConst26 * fRec91[1]);
			fRec93[0] = (fTemp59 + (fRec93[1] + fTemp60));
			fRec91[0] = fRec93[0];
			float fRec92 = (fTemp60 + fTemp59);
			float fTemp61 = (fConst28 * (((0.0219038911f * fTemp24) + (0.0276680924f * fTemp25)) - (((0.0733433738f * fTemp28) + (0.0240597557f * fTemp26)) + (0.0258559622f * fTemp27))));
			float fTemp62 = (fConst29 * fRec94[1]);
			float fTemp63 = (fConst30 * fRec97[1]);
			fRec99[0] = (fTemp61 + (fTemp62 + (fRec99[1] + fTemp63)));
			fRec97[0] = fRec99[0];
			float fRec98 = ((fTemp63 + fTemp62) + fTemp61);
			fRec96[0] = (fRec96[1] + fRec97[0]);
			fRec94[0] = fRec96[0];
			float fRec95 = fRec98;
			float fTemp64 = (fConst23 * fRec100[1]);
			float fTemp65 = (fConst22 * ((((0.00808941945f * fTemp13) + (0.0338699184f * fTemp9)) + (0.0165906455f * fTemp15)) - ((((0.0586978495f * fTemp14) + (0.0106811747f * fTemp10)) + (0.016799517f * fTemp11)) + (0.0429462641f * fTemp12))));
			float fTemp66 = (fConst19 * fRec103[1]);
			float fTemp67 = (fConst20 * fRec106[1]);
			fRec108[0] = (fTemp65 + (fTemp66 + (fRec108[1] + fTemp67)));
			fRec106[0] = fRec108[0];
			float fRec107 = ((fTemp67 + fTemp66) + fTemp65);
			fRec105[0] = (fRec105[1] + fRec106[0]);
			fRec103[0] = fRec105[0];
			float fRec104 = fRec107;
			fRec102[0] = ((fRec102[1] + fTemp64) + fRec104);
			fRec100[0] = fRec102[0];
			float fRec101 = (fTemp64 + fRec104);
			fVec4[(IOTA & 1023)] = ((0.0487356819f * fTemp7) + ((fRec92 + fRec95) + fRec101));
			output4[i] = FAUSTFLOAT((0.810182214f * (fVec4[((IOTA - iConst31) & 1023)] * fRec0[0])));
			float fTemp68 = (fConst39 * (((0.062723957f * fTemp19) + (0.0419793911f * fTemp20)) - (0.0419737063f * fTemp21)));
			float fTemp69 = (fConst40 * fRec109[1]);
			fRec111[0] = (fTemp68 + (fRec111[1] + fTemp69));
			fRec109[0] = fRec111[0];
			float fRec110 = (fTemp69 + fTemp68);
			float fTemp70 = (fConst42 * ((((0.0264462121f * fTemp25) + (0.059475638f * fTemp26)) + (5.71769988e-06f * fTemp27)) - ((0.0366183743f * fTemp24) + (0.0594708472f * fTemp28))));
			float fTemp71 = (fConst43 * fRec112[1]);
			float fTemp72 = (fConst44 * fRec115[1]);
			fRec117[0] = (fTemp70 + (fTemp71 + (fRec117[1] + fTemp72)));
			fRec115[0] = fRec117[0];
			float fRec116 = ((fTemp72 + fTemp71) + fTemp70);
			fRec114[0] = (fRec114[1] + fRec115[0]);
			fRec112[0] = fRec114[0];
			float fRec113 = fRec116;
			float fTemp73 = (fConst35 * (((0.0442743786f * fTemp11) + (5.98499992e-06f * fTemp12)) - (((((0.0122086443f * fTemp13) + (0.0582810603f * fTemp9)) + (0.0442765392f * fTemp14)) + (0.0160852037f * fTemp10)) + (0.0122040147f * fTemp15))));
			float fTemp74 = (fConst36 * fRec121[1]);
			float fTemp75 = (fConst37 * fRec124[1]);
			fRec126[0] = (fTemp73 + (fTemp74 + (fRec126[1] + fTemp75)));
			fRec124[0] = fRec126[0];
			float fRec125 = ((fTemp75 + fTemp74) + fTemp73);
			fRec123[0] = (fRec123[1] + fRec124[0]);
			fRec121[0] = fRec123[0];
			float fRec122 = fRec125;
			float fTemp76 = (fConst33 * fRec118[1]);
			fRec120[0] = (fRec122 + (fRec120[1] + fTemp76));
			fRec118[0] = fRec120[0];
			float fRec119 = (fTemp76 + fRec122);
			fVec5[(IOTA & 1023)] = ((0.0537970774f * fTemp7) + ((fRec110 + fRec113) + fRec119));
			output5[i] = FAUSTFLOAT((0.809967816f * (fRec0[0] * fVec5[((IOTA - iConst45) & 1023)])));
			float fTemp77 = (fConst23 * fRec127[1]);
			float fTemp78 = (fConst22 * (((0.0124570848f * fTemp15) + ((0.0432575345f * fTemp12) + ((((0.0124552557f * fTemp13) + (0.0249784216f * fTemp9)) + (0.0154068097f * fTemp14)) + (0.0574973635f * fTemp11)))) - (0.0052484558f * fTemp10)));
			float fTemp79 = (fConst19 * fRec130[1]);
			float fTemp80 = (fConst20 * fRec133[1]);
			fRec135[0] = (fTemp78 + (fTemp79 + (fRec135[1] + fTemp80)));
			fRec133[0] = fRec135[0];
			float fRec134 = ((fTemp80 + fTemp79) + fTemp78);
			fRec132[0] = (fRec133[0] + fRec132[1]);
			fRec130[0] = fRec132[0];
			float fRec131 = fRec134;
			fRec129[0] = (fTemp77 + (fRec131 + fRec129[1]));
			fRec127[0] = fRec129[0];
			float fRec128 = (fRec131 + fTemp77);
			float fTemp81 = (fConst25 * (((0.0120678125f * fTemp21) + (0.0539331064f * fTemp19)) + (0.0450365283f * fTemp20)));
			float fTemp82 = (fConst26 * fRec136[1]);
			fRec138[0] = (fTemp81 + (fRec138[1] + fTemp82));
			fRec136[0] = fRec138[0];
			float fRec137 = (fTemp82 + fTemp81);
			float fTemp83 = (fConst28 * (((((0.0150224213f * fTemp24) + (0.0181299727f * fTemp28)) + (0.0289299879f * fTemp25)) + (0.0676582232f * fTemp26)) + (0.0260189939f * fTemp27)));
			float fTemp84 = (fConst29 * fRec139[1]);
			float fTemp85 = (fConst30 * fRec142[1]);
			fRec144[0] = (fTemp83 + (fTemp84 + (fRec144[1] + fTemp85)));
			fRec142[0] = fRec144[0];
			float fRec143 = ((fTemp85 + fTemp84) + fTemp83);
			fRec141[0] = (fRec142[0] + fRec141[1]);
			fRec139[0] = fRec141[0];
			float fRec140 = fRec143;
			fVec6[(IOTA & 1023)] = ((0.0436802469f * fTemp7) + (fRec128 + (fRec137 + fRec140)));
			output6[i] = FAUSTFLOAT((0.810182214f * (fRec0[0] * fVec6[((IOTA - iConst31) & 1023)])));
			float fTemp86 = (fConst47 * fRec145[1]);
			float fTemp87 = (fConst49 * ((0.00467731338f * fTemp15) - ((((((0.0389770418f * fTemp13) + (0.00530663319f * fTemp9)) + (0.0152090061f * fTemp14)) + (0.0237736925f * fTemp10)) + (0.000768534199f * fTemp11)) + (0.0325269736f * fTemp12))));
			float fTemp88 = (fConst50 * fRec148[1]);
			float fTemp89 = (fConst51 * fRec151[1]);
			fRec153[0] = (fTemp87 + (fTemp88 + (fRec153[1] + fTemp89)));
			fRec151[0] = fRec153[0];
			float fRec152 = ((fTemp89 + fTemp88) + fTemp87);
			fRec150[0] = (fRec151[0] + fRec150[1]);
			fRec148[0] = fRec150[0];
			float fRec149 = fRec152;
			fRec147[0] = (fTemp86 + (fRec149 + fRec147[1]));
			fRec145[0] = fRec147[0];
			float fRec146 = (fRec149 + fTemp86);
			float fTemp90 = (fConst53 * (((0.0444157235f * fTemp21) + (0.0155758914f * fTemp19)) - (0.0022798581f * fTemp20)));
			float fTemp91 = (fConst54 * fRec154[1]);
			fRec156[0] = (fTemp90 + (fRec156[1] + fTemp91));
			fRec154[0] = fRec156[0];
			float fRec155 = (fTemp91 + fTemp90);
			float fTemp92 = (fConst56 * ((0.0300963297f * fTemp28) - ((((0.00395526737f * fTemp24) + (0.0187431257f * fTemp25)) + (0.0026183913f * fTemp26)) + (0.0431320332f * fTemp27))));
			float fTemp93 = (fConst57 * fRec157[1]);
			float fTemp94 = (fConst58 * fRec160[1]);
			fRec162[0] = (fTemp92 + (fTemp93 + (fRec162[1] + fTemp94)));
			fRec160[0] = fRec162[0];
			float fRec161 = ((fTemp94 + fTemp93) + fTemp92);
			fRec159[0] = (fRec160[0] + fRec159[1]);
			fRec157[0] = fRec159[0];
			float fRec158 = fRec161;
			fVec7[(IOTA & 1023)] = ((0.0283387434f * fTemp7) + (fRec146 + (fRec155 + fRec158)));
			output7[i] = FAUSTFLOAT((0.879635572f * (fRec0[0] * fVec7[((IOTA - iConst59) & 1023)])));
			float fTemp95 = (fConst61 * fRec163[1]);
			float fTemp96 = (fConst63 * ((((0.00176773127f * fTemp13) + (0.0178551711f * fTemp11)) + (0.0492927805f * fTemp15)) - ((((0.0179782622f * fTemp9) + (0.0309820846f * fTemp14)) + (0.0147790294f * fTemp10)) + (0.00857336447f * fTemp12))));
			float fTemp97 = (fConst64 * fRec166[1]);
			float fTemp98 = (fConst65 * fRec169[1]);
			fRec171[0] = (fTemp96 + (fTemp97 + (fRec171[1] + fTemp98)));
			fRec169[0] = fRec171[0];
			float fRec170 = ((fTemp98 + fTemp97) + fTemp96);
			fRec168[0] = (fRec169[0] + fRec168[1]);
			fRec166[0] = fRec168[0];
			float fRec167 = fRec170;
			fRec165[0] = (fTemp95 + (fRec167 + fRec165[1]));
			fRec163[0] = fRec165[0];
			float fRec164 = (fRec167 + fTemp95);
			float fTemp99 = (fConst67 * (((0.0463183224f * fTemp21) + (0.00839097518f * fTemp19)) - (0.0275335684f * fTemp20)));
			float fTemp100 = (fConst68 * fRec172[1]);
			fRec174[0] = (fTemp99 + (fRec174[1] + fTemp100));
			fRec172[0] = fRec174[0];
			float fRec173 = (fTemp100 + fTemp99);
			float fTemp101 = (fConst70 * ((0.0144425482f * fTemp28) - ((((0.0473840795f * fTemp24) + (0.0301495101f * fTemp25)) + (0.00914137997f * fTemp26)) + (0.0258442741f * fTemp27))));
			float fTemp102 = (fConst71 * fRec175[1]);
			float fTemp103 = (fConst72 * fRec178[1]);
			fRec180[0] = (fTemp101 + (fTemp102 + (fRec180[1] + fTemp103)));
			fRec178[0] = fRec180[0];
			float fRec179 = ((fTemp103 + fTemp102) + fTemp101);
			fRec177[0] = (fRec178[0] + fRec177[1]);
			fRec175[0] = fRec177[0];
			float fRec176 = fRec179;
			fVec8[(IOTA & 1023)] = ((0.0329640992f * fTemp7) + (fRec164 + (fRec173 + fRec176)));
			output8[i] = FAUSTFLOAT((0.880171478f * (fRec0[0] * fVec8[((IOTA - iConst73) & 1023)])));
			float fTemp104 = (fConst61 * fRec181[1]);
			float fTemp105 = (fConst63 * (((0.00871290267f * fTemp12) + ((0.0411527641f * fTemp13) + (0.0246891771f * fTemp11))) - ((((0.0185482372f * fTemp9) + (0.011468906f * fTemp14)) + (0.0152193001f * fTemp10)) + (0.006762248f * fTemp15))));
			float fTemp106 = (fConst64 * fRec184[1]);
			float fTemp107 = (fConst65 * fRec187[1]);
			fRec189[0] = (fTemp105 + (fTemp106 + (fRec189[1] + fTemp107)));
			fRec187[0] = fRec189[0];
			float fRec188 = ((fTemp107 + fTemp106) + fTemp105);
			fRec186[0] = (fRec187[0] + fRec186[1]);
			fRec184[0] = fRec186[0];
			float fRec185 = fRec188;
			fRec183[0] = (fTemp104 + (fRec185 + fRec183[1]));
			fRec181[0] = fRec183[0];
			float fRec182 = (fRec185 + fTemp104);
			float fTemp108 = (fConst67 * (((0.020101551f * fTemp21) + (0.00860498939f * fTemp19)) - (0.0390066057f * fTemp20)));
			float fTemp109 = (fConst68 * fRec190[1]);
			fRec192[0] = (fTemp108 + (fRec192[1] + fTemp109));
			fRec190[0] = fRec192[0];
			float fRec191 = (fTemp109 + fTemp108);
			float fTemp110 = (fConst70 * (((0.00942643546f * fTemp28) + (0.0260256138f * fTemp27)) - (((0.0360338092f * fTemp24) + (0.0236546472f * fTemp25)) + (0.0148056475f * fTemp26))));
			float fTemp111 = (fConst71 * fRec193[1]);
			float fTemp112 = (fConst72 * fRec196[1]);
			fRec198[0] = (fTemp110 + (fTemp111 + (fRec198[1] + fTemp112)));
			fRec196[0] = fRec198[0];
			float fRec197 = ((fTemp112 + fTemp111) + fTemp110);
			fRec195[0] = (fRec196[0] + fRec195[1]);
			fRec193[0] = fRec195[0];
			float fRec194 = fRec197;
			fVec9[(IOTA & 1023)] = ((0.026858544f * fTemp7) + (fRec182 + (fRec191 + fRec194)));
			output9[i] = FAUSTFLOAT((0.880171478f * (fRec0[0] * fVec9[((IOTA - iConst73) & 1023)])));
			float fTemp113 = (fConst47 * fRec199[1]);
			float fTemp114 = (fConst49 * (((0.0325262211f * fTemp12) + (((0.00467047235f * fTemp13) + (0.000772691681f * fTemp14)) + (0.0152002154f * fTemp11))) - (((0.00530857872f * fTemp9) + (0.0237655547f * fTemp10)) + (0.0389713533f * fTemp15))));
			float fTemp115 = (fConst50 * fRec202[1]);
			float fTemp116 = (fConst51 * fRec205[1]);
			fRec207[0] = (fTemp114 + (fTemp115 + (fRec207[1] + fTemp116)));
			fRec205[0] = fRec207[0];
			float fRec206 = ((fTemp116 + fTemp115) + fTemp114);
			fRec204[0] = (fRec205[0] + fRec204[1]);
			fRec202[0] = fRec204[0];
			float fRec203 = fRec206;
			fRec201[0] = (fTemp113 + (fRec203 + fRec201[1]));
			fRec199[0] = fRec201[0];
			float fRec200 = (fRec203 + fTemp113);
			float fTemp117 = (fConst53 * (((0.00227736658f * fTemp21) + (0.0155748995f * fTemp19)) - (0.0444059633f * fTemp20)));
			float fTemp118 = (fConst54 * fRec208[1]);
			fRec210[0] = (fTemp117 + (fRec210[1] + fTemp118));
			fRec208[0] = fRec210[0];
			float fRec209 = (fTemp118 + fTemp117);
			float fTemp119 = (fConst56 * (((0.00261935568f * fTemp28) + (0.0431238264f * fTemp27)) - (((0.00395008409f * fTemp24) + (0.0187353157f * fTemp25)) + (0.0300932992f * fTemp26))));
			float fTemp120 = (fConst57 * fRec211[1]);
			float fTemp121 = (fConst58 * fRec214[1]);
			fRec216[0] = (fTemp119 + (fTemp120 + (fRec216[1] + fTemp121)));
			fRec214[0] = fRec216[0];
			float fRec215 = ((fTemp121 + fTemp120) + fTemp119);
			fRec213[0] = (fRec214[0] + fRec213[1]);
			fRec211[0] = fRec213[0];
			float fRec212 = fRec215;
			fVec10[(IOTA & 1023)] = ((0.0283328258f * fTemp7) + (fRec200 + (fRec209 + fRec212)));
			output10[i] = FAUSTFLOAT((0.879635572f * (fRec0[0] * fVec10[((IOTA - iConst59) & 1023)])));
			float fTemp122 = (fConst61 * fRec217[1]);
			float fTemp123 = (fConst63 * (((0.00468057534f * fTemp15) + ((0.0111199031f * fTemp12) + (((0.0298867859f * fTemp9) + (0.0155930547f * fTemp14)) + (0.0285520405f * fTemp11)))) - ((0.0553105474f * fTemp13) + (0.0229106862f * fTemp10))));
			float fTemp124 = (fConst64 * fRec220[1]);
			float fTemp125 = (fConst65 * fRec223[1]);
			fRec225[0] = (fTemp123 + (fTemp124 + (fRec225[1] + fTemp125)));
			fRec223[0] = fRec225[0];
			float fRec224 = ((fTemp125 + fTemp124) + fTemp123);
			fRec222[0] = (fRec223[0] + fRec222[1]);
			fRec220[0] = fRec222[0];
			float fRec221 = fRec224;
			fRec219[0] = (fTemp122 + (fRec221 + fRec219[1]));
			fRec217[0] = fRec219[0];
			float fRec218 = (fRec221 + fTemp122);
			float fTemp126 = (fConst67 * ((0.0151734343f * fTemp19) - ((0.0342850015f * fTemp21) + (0.0548170209f * fTemp20))));
			float fTemp127 = (fConst68 * fRec226[1]);
			fRec228[0] = (fTemp126 + (fRec228[1] + fTemp127));
			fRec226[0] = fRec228[0];
			float fRec227 = (fTemp127 + fTemp126);
			float fTemp128 = (fConst70 * (((0.0562758073f * fTemp24) + (0.027918499f * fTemp27)) - (((0.0168836657f * fTemp28) + (0.0317739099f * fTemp25)) + (0.0239986237f * fTemp26))));
			float fTemp129 = (fConst71 * fRec229[1]);
			float fTemp130 = (fConst72 * fRec232[1]);
			fRec234[0] = (fTemp128 + (fTemp129 + (fRec234[1] + fTemp130)));
			fRec232[0] = fRec234[0];
			float fRec233 = ((fTemp130 + fTemp129) + fTemp128);
			fRec231[0] = (fRec232[0] + fRec231[1]);
			fRec229[0] = fRec231[0];
			float fRec230 = fRec233;
			fVec11[(IOTA & 1023)] = ((0.0406197943f * fTemp7) + (fRec218 + (fRec227 + fRec230)));
			output11[i] = FAUSTFLOAT((0.880171478f * (fRec0[0] * fVec11[((IOTA - iConst73) & 1023)])));
			float fTemp131 = (fConst61 * fRec235[1]);
			float fTemp132 = (fConst63 * ((((((0.0103097195f * fTemp13) + (0.016962165f * fTemp9)) + (0.0246620625f * fTemp14)) + (0.011517141f * fTemp11)) + (0.0411229059f * fTemp15)) - ((0.0151936384f * fTemp10) + (0.0113901151f * fTemp12))));
			float fTemp133 = (fConst64 * fRec238[1]);
			float fTemp134 = (fConst65 * fRec241[1]);
			fRec243[0] = (fTemp132 + (fTemp133 + (fRec243[1] + fTemp134)));
			fRec241[0] = fRec243[0];
			float fRec242 = ((fTemp134 + fTemp133) + fTemp132);
			fRec240[0] = (fRec241[0] + fRec240[1]);
			fRec238[0] = fRec240[0];
			float fRec239 = fRec242;
			fRec237[0] = (fTemp131 + (fRec239 + fRec237[1]));
			fRec235[0] = fRec237[0];
			float fRec236 = (fRec239 + fTemp131);
			float fTemp135 = (fConst67 * ((0.00858379155f * fTemp19) - ((0.0396553129f * fTemp21) + (0.0188880209f * fTemp20))));
			float fTemp136 = (fConst68 * fRec244[1]);
			fRec246[0] = (fTemp135 + (fRec246[1] + fTemp136));
			fRec244[0] = fRec246[0];
			float fRec245 = (fTemp136 + fTemp135);
			float fTemp137 = (fConst70 * ((0.0346854255f * fTemp24) - ((((0.0154645126f * fTemp28) + (0.0236423165f * fTemp25)) + (0.00820955727f * fTemp26)) + (0.028282484f * fTemp27))));
			float fTemp138 = (fConst71 * fRec247[1]);
			float fTemp139 = (fConst72 * fRec250[1]);
			fRec252[0] = (fTemp137 + (fTemp138 + (fRec252[1] + fTemp139)));
			fRec250[0] = fRec252[0];
			float fRec251 = ((fTemp139 + fTemp138) + fTemp137);
			fRec249[0] = (fRec250[0] + fRec249[1]);
			fRec247[0] = fRec249[0];
			float fRec248 = fRec251;
			fVec12[(IOTA & 1023)] = ((0.0268279463f * fTemp7) + (fRec236 + (fRec245 + fRec248)));
			output12[i] = FAUSTFLOAT((0.880171478f * (fRec0[0] * fVec12[((IOTA - iConst73) & 1023)])));
			float fTemp140 = (fConst47 * fRec253[1]);
			float fTemp141 = (fConst49 * ((((0.0389659628f * fTemp13) + (0.0151934419f * fTemp14)) + (0.000778859772f * fTemp11)) - ((0.00466579571f * fTemp15) + (((0.00531259878f * fTemp9) + (0.0237730574f * fTemp10)) + (0.0325311758f * fTemp12)))));
			float fTemp142 = (fConst50 * fRec256[1]);
			float fTemp143 = (fConst51 * fRec259[1]);
			fRec261[0] = (fTemp141 + (fTemp142 + (fRec261[1] + fTemp143)));
			fRec259[0] = fRec261[0];
			float fRec260 = ((fTemp143 + fTemp142) + fTemp141);
			fRec258[0] = (fRec259[0] + fRec258[1]);
			fRec256[0] = fRec258[0];
			float fRec257 = fRec260;
			fRec255[0] = (fTemp140 + (fRec257 + fRec255[1]));
			fRec253[0] = fRec255[0];
			float fRec254 = (fRec257 + fTemp140);
			float fTemp144 = (fConst53 * (((0.0155842332f * fTemp19) + (0.00227896054f * fTemp20)) - (0.044411663f * fTemp21)));
			float fTemp145 = (fConst54 * fRec262[1]);
			fRec264[0] = (fTemp144 + (fRec264[1] + fTemp145));
			fRec262[0] = fRec264[0];
			float fRec263 = (fTemp145 + fTemp144);
			float fTemp146 = (fConst56 * ((0.0026237031f * fTemp26) - ((((0.00394974416f * fTemp24) + (0.0301061943f * fTemp28)) + (0.0187329222f * fTemp25)) + (0.0431233831f * fTemp27))));
			float fTemp147 = (fConst57 * fRec265[1]);
			float fTemp148 = (fConst58 * fRec268[1]);
			fRec270[0] = (fTemp146 + (fTemp147 + (fRec270[1] + fTemp148)));
			fRec268[0] = fRec270[0];
			float fRec269 = ((fTemp148 + fTemp147) + fTemp146);
			fRec267[0] = (fRec268[0] + fRec267[1]);
			fRec265[0] = fRec267[0];
			float fRec266 = fRec269;
			fVec13[(IOTA & 1023)] = ((0.0283390991f * fTemp7) + (fRec254 + (fRec263 + fRec266)));
			output13[i] = FAUSTFLOAT((0.879635572f * (fRec0[0] * fVec13[((IOTA - iConst59) & 1023)])));
			float fTemp149 = (fConst61 * fRec271[1]);
			float fTemp150 = (fConst63 * ((0.0309881084f * fTemp14) - ((0.0493036173f * fTemp15) + (((((0.0017651082f * fTemp13) + (0.0179697257f * fTemp9)) + (0.0147721153f * fTemp10)) + (0.0178599581f * fTemp11)) + (0.00858830567f * fTemp12)))));
			float fTemp151 = (fConst64 * fRec274[1]);
			float fTemp152 = (fConst65 * fRec277[1]);
			fRec279[0] = (fTemp150 + (fTemp151 + (fRec279[1] + fTemp152)));
			fRec277[0] = fRec279[0];
			float fRec278 = ((fTemp152 + fTemp151) + fTemp150);
			fRec276[0] = (fRec277[0] + fRec276[1]);
			fRec274[0] = fRec276[0];
			float fRec275 = fRec278;
			fRec273[0] = (fTemp149 + (fRec275 + fRec273[1]));
			fRec271[0] = fRec273[0];
			float fRec272 = (fRec275 + fTemp149);
			float fTemp153 = (fConst67 * (((0.00838882104f * fTemp19) + (0.0275408067f * fTemp20)) - (0.0463334993f * fTemp21)));
			float fTemp154 = (fConst68 * fRec280[1]);
			fRec282[0] = (fTemp153 + (fRec282[1] + fTemp154));
			fRec280[0] = fRec282[0];
			float fRec281 = (fTemp154 + fTemp153);
			float fTemp155 = (fConst70 * ((0.0091328593f * fTemp26) - ((((0.0473960564f * fTemp24) + (0.0144431572f * fTemp28)) + (0.0301579721f * fTemp25)) + (0.0258537512f * fTemp27))));
			float fTemp156 = (fConst71 * fRec283[1]);
			float fTemp157 = (fConst72 * fRec286[1]);
			fRec288[0] = (fTemp155 + (fTemp156 + (fRec288[1] + fTemp157)));
			fRec286[0] = fRec288[0];
			float fRec287 = ((fTemp157 + fTemp156) + fTemp155);
			fRec285[0] = (fRec286[0] + fRec285[1]);
			fRec283[0] = fRec285[0];
			float fRec284 = fRec287;
			fVec14[(IOTA & 1023)] = ((0.0329747647f * fTemp7) + (fRec272 + (fRec281 + fRec284)));
			output14[i] = FAUSTFLOAT((0.880171478f * (fRec0[0] * fVec14[((IOTA - iConst73) & 1023)])));
			float fTemp158 = (fConst61 * fRec289[1]);
			float fTemp159 = (fConst63 * (((0.00677051628f * fTemp15) + ((0.0114678685f * fTemp14) + (0.0087232003f * fTemp12))) - ((((0.041143775f * fTemp13) + (0.0185491145f * fTemp9)) + (0.0152238971f * fTemp10)) + (0.0246859211f * fTemp11))));
			float fTemp160 = (fConst64 * fRec292[1]);
			float fTemp161 = (fConst65 * fRec295[1]);
			fRec297[0] = (fTemp159 + (fTemp160 + (fRec297[1] + fTemp161)));
			fRec295[0] = fRec297[0];
			float fRec296 = ((fTemp161 + fTemp160) + fTemp159);
			fRec294[0] = (fRec295[0] + fRec294[1]);
			fRec292[0] = fRec294[0];
			float fRec293 = fRec296;
			fRec291[0] = (fTemp158 + (fRec293 + fRec291[1]));
			fRec289[0] = fRec291[0];
			float fRec290 = (fRec293 + fTemp158);
			float fTemp162 = (fConst67 * (((0.00860586017f * fTemp19) + (0.0390018113f * fTemp20)) - (0.0200940184f * fTemp21)));
			float fTemp163 = (fConst68 * fRec298[1]);
			fRec300[0] = (fTemp162 + (fRec300[1] + fTemp163));
			fRec298[0] = fRec300[0];
			float fRec299 = (fTemp163 + fTemp162);
			float fTemp164 = (fConst70 * (((0.0148103526f * fTemp26) + (0.02602759f * fTemp27)) - (((0.0360228419f * fTemp24) + (0.00942459423f * fTemp28)) + (0.0236514919f * fTemp25))));
			float fTemp165 = (fConst71 * fRec301[1]);
			float fTemp166 = (fConst72 * fRec304[1]);
			fRec306[0] = (fTemp164 + (fTemp165 + (fRec306[1] + fTemp166)));
			fRec304[0] = fRec306[0];
			float fRec305 = ((fTemp166 + fTemp165) + fTemp164);
			fRec303[0] = (fRec304[0] + fRec303[1]);
			fRec301[0] = fRec303[0];
			float fRec302 = fRec305;
			fVec15[(IOTA & 1023)] = ((0.0268536117f * fTemp7) + (fRec290 + (fRec299 + fRec302)));
			output15[i] = FAUSTFLOAT((0.880171478f * (fRec0[0] * fVec15[((IOTA - iConst73) & 1023)])));
			float fTemp167 = (fConst47 * fRec307[1]);
			float fTemp168 = (fConst49 * (((0.0325279534f * fTemp12) + (0.0389627181f * fTemp15)) - (((((0.00466731703f * fTemp13) + (0.00530193001f * fTemp9)) + (0.00077174278f * fTemp14)) + (0.0237659756f * fTemp10)) + (0.0151881436f * fTemp11))));
			float fTemp169 = (fConst50 * fRec310[1]);
			float fTemp170 = (fConst51 * fRec313[1]);
			fRec315[0] = (fTemp168 + (fTemp169 + (fRec315[1] + fTemp170)));
			fRec313[0] = fRec315[0];
			float fRec314 = ((fTemp170 + fTemp169) + fTemp168);
			fRec312[0] = (fRec313[0] + fRec312[1]);
			fRec310[0] = fRec312[0];
			float fRec311 = fRec314;
			fRec309[0] = (fTemp167 + (fRec311 + fRec309[1]));
			fRec307[0] = fRec309[0];
			float fRec308 = (fRec311 + fTemp167);
			float fTemp171 = (fConst53 * (((0.015574404f * fTemp19) + (0.0443907827f * fTemp20)) - (0.0022749207f * fTemp21)));
			float fTemp172 = (fConst54 * fRec316[1]);
			fRec318[0] = (fTemp171 + (fRec318[1] + fTemp172));
			fRec316[0] = fRec318[0];
			float fRec317 = (fTemp172 + fTemp171);
			float fTemp173 = (fConst56 * (((0.030093208f * fTemp26) + (0.0431107245f * fTemp27)) - (((0.00394637231f * fTemp24) + (0.00261598756f * fTemp28)) + (0.018725859f * fTemp25))));
			float fTemp174 = (fConst57 * fRec319[1]);
			float fTemp175 = (fConst58 * fRec322[1]);
			fRec324[0] = (fTemp173 + (fTemp174 + (fRec324[1] + fTemp175)));
			fRec322[0] = fRec324[0];
			float fRec323 = ((fTemp175 + fTemp174) + fTemp173);
			fRec321[0] = (fRec322[0] + fRec321[1]);
			fRec319[0] = fRec321[0];
			float fRec320 = fRec323;
			fVec16[(IOTA & 1023)] = ((0.0283231307f * fTemp7) + (fRec308 + (fRec317 + fRec320)));
			output16[i] = FAUSTFLOAT((0.879635572f * (fRec0[0] * fVec16[((IOTA - iConst59) & 1023)])));
			float fTemp176 = (fConst61 * fRec325[1]);
			float fTemp177 = (fConst63 * ((((0.0553068481f * fTemp13) + (0.0298772734f * fTemp9)) + (0.0111225173f * fTemp12)) - ((((0.0155937243f * fTemp14) + (0.0229095872f * fTemp10)) + (0.0285480842f * fTemp11)) + (0.00467078993f * fTemp15))));
			float fTemp178 = (fConst64 * fRec328[1]);
			float fTemp179 = (fConst65 * fRec331[1]);
			fRec333[0] = (fTemp177 + (fTemp178 + (fRec333[1] + fTemp179)));
			fRec331[0] = fRec333[0];
			float fRec332 = ((fTemp179 + fTemp178) + fTemp177);
			fRec330[0] = (fRec331[0] + fRec330[1]);
			fRec328[0] = fRec330[0];
			float fRec329 = fRec332;
			fRec327[0] = (fTemp176 + (fRec329 + fRec327[1]));
			fRec325[0] = fRec327[0];
			float fRec326 = (fRec329 + fTemp176);
			float fTemp180 = (fConst67 * (((0.0342726484f * fTemp21) + (0.01516994f * fTemp19)) + (0.0548043214f * fTemp20)));
			float fTemp181 = (fConst68 * fRec334[1]);
			fRec336[0] = (fTemp180 + (fRec336[1] + fTemp181));
			fRec334[0] = fRec336[0];
			float fRec335 = (fTemp181 + fTemp180);
			float fTemp182 = (fConst70 * (((((0.0562616065f * fTemp24) + (0.0168774109f * fTemp28)) + (0.0239955988f * fTemp26)) + (0.0279191658f * fTemp27)) - (0.0317676514f * fTemp25)));
			float fTemp183 = (fConst71 * fRec337[1]);
			float fTemp184 = (fConst72 * fRec340[1]);
			fRec342[0] = (fTemp182 + (fTemp183 + (fRec342[1] + fTemp184)));
			fRec340[0] = fRec342[0];
			float fRec341 = ((fTemp184 + fTemp183) + fTemp182);
			fRec339[0] = (fRec340[0] + fRec339[1]);
			fRec337[0] = fRec339[0];
			float fRec338 = fRec341;
			fVec17[(IOTA & 1023)] = ((0.0406075418f * fTemp7) + (fRec326 + (fRec335 + fRec338)));
			output17[i] = FAUSTFLOAT((0.880171478f * (fRec0[0] * fVec17[((IOTA - iConst73) & 1023)])));
			float fTemp185 = (fConst61 * fRec343[1]);
			float fTemp186 = (fConst63 * ((0.0169819035f * fTemp9) - ((0.0411421359f * fTemp15) + (((((0.0102870315f * fTemp13) + (0.0246529412f * fTemp14)) + (0.0152127426f * fTemp10)) + (0.0115211168f * fTemp11)) + (0.0114039099f * fTemp12)))));
			float fTemp187 = (fConst64 * fRec346[1]);
			float fTemp188 = (fConst65 * fRec349[1]);
			fRec351[0] = (fTemp186 + (fTemp187 + (fRec351[1] + fTemp188)));
			fRec349[0] = fRec351[0];
			float fRec350 = ((fTemp188 + fTemp187) + fTemp186);
			fRec348[0] = (fRec349[0] + fRec348[1]);
			fRec346[0] = fRec348[0];
			float fRec347 = fRec350;
			fRec345[0] = (fTemp185 + (fRec347 + fRec345[1]));
			fRec343[0] = fRec345[0];
			float fRec344 = (fRec347 + fTemp185);
			float fTemp189 = (fConst67 * (((0.0396705791f * fTemp21) + (0.00859803706f * fTemp19)) + (0.018905893f * fTemp20)));
			float fTemp190 = (fConst68 * fRec352[1]);
			fRec354[0] = (fTemp189 + (fRec354[1] + fTemp190));
			fRec352[0] = fRec354[0];
			float fRec353 = (fTemp190 + fTemp189);
			float fTemp191 = (fConst70 * ((((0.0347117484f * fTemp24) + (0.0154876364f * fTemp28)) + (0.00822132174f * fTemp26)) - ((0.0236457568f * fTemp25) + (0.0282789245f * fTemp27))));
			float fTemp192 = (fConst71 * fRec355[1]);
			float fTemp193 = (fConst72 * fRec358[1]);
			fRec360[0] = (fTemp191 + (fTemp192 + (fRec360[1] + fTemp193)));
			fRec358[0] = fRec360[0];
			float fRec359 = ((fTemp193 + fTemp192) + fTemp191);
			fRec357[0] = (fRec358[0] + fRec357[1]);
			fRec355[0] = fRec357[0];
			float fRec356 = fRec359;
			fVec18[(IOTA & 1023)] = ((0.026842976f * fTemp7) + (fRec344 + (fRec353 + fRec356)));
			output18[i] = FAUSTFLOAT((0.880171478f * (fRec0[0] * fVec18[((IOTA - iConst73) & 1023)])));
			float fTemp194 = (fConst75 * ((0.0753616467f * fTemp21) - ((0.0311328489f * fTemp19) + (6.40090002e-06f * fTemp20))));
			float fTemp195 = (fConst76 * fRec361[1]);
			fRec363[0] = (fTemp194 + (fRec363[1] + fTemp195));
			fRec361[0] = fRec363[0];
			float fRec362 = (fTemp195 + fTemp194);
			float fTemp196 = (fConst78 * ((8.12150029e-06f * fTemp26) - ((((9.05569959e-06f * fTemp24) + (0.0506631546f * fTemp28)) + (0.0202656481f * fTemp25)) + (0.0699572712f * fTemp27))));
			float fTemp197 = (fConst79 * fRec364[1]);
			float fTemp198 = (fConst80 * fRec367[1]);
			fRec369[0] = (fTemp196 + (fTemp197 + (fRec369[1] + fTemp198)));
			fRec367[0] = fRec369[0];
			float fRec368 = ((fTemp198 + fTemp197) + fTemp196);
			fRec366[0] = (fRec367[0] + fRec366[1]);
			fRec364[0] = fRec366[0];
			float fRec365 = fRec368;
			float fTemp199 = (fConst82 * fRec370[1]);
			float fTemp200 = (fConst84 * (((8.90140018e-06f * fTemp15) + (((1.15727998e-05f * fTemp9) + (0.0226103123f * fTemp10)) + (0.049382329f * fTemp12))) - (((0.0602857322f * fTemp13) + (0.0101948977f * fTemp14)) + (5.91619983e-06f * fTemp11))));
			float fTemp201 = (fConst85 * fRec373[1]);
			float fTemp202 = (fConst86 * fRec376[1]);
			fRec378[0] = (fTemp200 + (fTemp201 + (fRec378[1] + fTemp202)));
			fRec376[0] = fRec378[0];
			float fRec377 = ((fTemp202 + fTemp201) + fTemp200);
			fRec375[0] = (fRec376[0] + fRec375[1]);
			fRec373[0] = fRec375[0];
			float fRec374 = fRec377;
			fRec372[0] = (fTemp199 + (fRec374 + fRec372[1]));
			fRec370[0] = fRec372[0];
			float fRec371 = (fRec374 + fTemp199);
			fVec19[0] = ((0.0513265356f * fTemp7) + (fRec362 + (fRec365 + fRec371)));
			output19[i] = FAUSTFLOAT((0.999892831f * (fRec0[0] * fVec19[iConst87])));
			float fTemp203 = (fConst89 * fRec379[1]);
			float fTemp204 = (fConst91 * (((0.0438517034f * fTemp15) + (((((6.24799986e-06f * fTemp13) + (0.0427996628f * fTemp9)) + (0.00624921452f * fTemp14)) + (0.0225724299f * fTemp10)) + (0.0247061756f * fTemp12))) - (0.00360593479f * fTemp11)));
			float fTemp205 = (fConst92 * fRec382[1]);
			float fTemp206 = (fConst93 * fRec385[1]);
			fRec387[0] = (fTemp204 + (fTemp205 + (fRec387[1] + fTemp206)));
			fRec385[0] = fRec387[0];
			float fRec386 = ((fTemp206 + fTemp205) + fTemp204);
			fRec384[0] = (fRec385[0] + fRec384[1]);
			fRec382[0] = fRec384[0];
			float fRec383 = fRec386;
			fRec381[0] = (fTemp203 + (fRec383 + fRec381[1]));
			fRec379[0] = fRec381[0];
			float fRec380 = (fRec383 + fTemp203);
			float fTemp207 = (fConst95 * ((0.0476964563f * fTemp21) - ((0.0310665164f * fTemp19) + (0.0275408607f * fTemp20))));
			float fTemp208 = (fConst96 * fRec388[1]);
			fRec390[0] = (fTemp207 + (fRec390[1] + fTemp208));
			fRec388[0] = fRec390[0];
			float fRec389 = (fTemp208 + fTemp207);
			float fTemp209 = (fConst98 * ((0.0253001824f * fTemp26) - ((((0.0435637385f * fTemp24) + (0.043817725f * fTemp28)) + (0.00720053958f * fTemp25)) + (0.0251454953f * fTemp27))));
			float fTemp210 = (fConst99 * fRec391[1]);
			float fTemp211 = (fConst100 * fRec394[1]);
			fRec396[0] = (fTemp209 + (fTemp210 + (fRec396[1] + fTemp211)));
			fRec394[0] = fRec396[0];
			float fRec395 = ((fTemp211 + fTemp210) + fTemp209);
			fRec393[0] = (fRec394[0] + fRec393[1]);
			fRec391[0] = fRec393[0];
			float fRec392 = fRec395;
			output20[i] = FAUSTFLOAT((fRec0[0] * (fRec380 + ((0.0389862582f * fTemp7) + (fRec389 + fRec392)))));
			float fTemp212 = (fConst89 * fRec397[1]);
			float fTemp213 = (fConst91 * ((((((0.0520841889f * fTemp13) + (0.0428706668f * fTemp9)) + (0.0225811359f * fTemp10)) + (9.50566027e-05f * fTemp11)) + (0.00860575773f * fTemp15)) - ((0.00281936885f * fTemp14) + (0.0245684087f * fTemp12))));
			float fTemp214 = (fConst92 * fRec400[1]);
			float fTemp215 = (fConst93 * fRec403[1]);
			fRec405[0] = (fTemp213 + (fTemp214 + (fRec405[1] + fTemp215)));
			fRec403[0] = fRec405[0];
			float fRec404 = ((fTemp215 + fTemp214) + fTemp213);
			fRec402[0] = (fRec403[0] + fRec402[1]);
			fRec400[0] = fRec402[0];
			float fRec401 = fRec404;
			fRec399[0] = (fTemp212 + (fRec401 + fRec399[1]));
			fRec397[0] = fRec399[0];
			float fRec398 = (fRec401 + fTemp212);
			float fTemp216 = (fConst98 * (((0.0438276269f * fTemp26) + (0.0249798838f * fTemp27)) - (((0.0550406128f * fTemp24) + (0.0253856331f * fTemp28)) + (0.0137328422f * fTemp25))));
			float fTemp217 = (fConst99 * fRec406[1]);
			float fTemp218 = (fConst100 * fRec409[1]);
			fRec411[0] = (fTemp216 + (fTemp217 + (fRec411[1] + fTemp218)));
			fRec409[0] = fRec411[0];
			float fRec410 = ((fTemp218 + fTemp217) + fTemp216);
			fRec408[0] = (fRec409[0] + fRec408[1]);
			fRec406[0] = fRec408[0];
			float fRec407 = fRec410;
			float fTemp219 = (fConst95 * ((0.035060972f * fTemp21) - ((0.0311201271f * fTemp19) + (0.0551089793f * fTemp20))));
			float fTemp220 = (fConst96 * fRec412[1]);
			fRec414[0] = (fTemp219 + (fRec414[1] + fTemp220));
			fRec412[0] = fRec414[0];
			float fRec413 = (fTemp220 + fTemp219);
			output21[i] = FAUSTFLOAT((fRec0[0] * (fRec398 + (fRec407 + (fRec413 + (0.0451872647f * fTemp7))))));
			float fTemp221 = (fConst75 * ((1.0128e-06f * fTemp21) - ((0.0311235841f * fTemp19) + (0.0753507093f * fTemp20))));
			float fTemp222 = (fConst76 * fRec415[1]);
			fRec417[0] = (fTemp221 + (fRec417[1] + fTemp222));
			fRec415[0] = fRec417[0];
			float fRec416 = (fTemp222 + fTemp221);
			float fTemp223 = (fConst78 * (((0.0506550409f * fTemp26) + (0.0699529573f * fTemp27)) - (((2.42639999e-06f * fTemp24) + (9.29999988e-09f * fTemp28)) + (0.0202659946f * fTemp25))));
			float fTemp224 = (fConst79 * fRec418[1]);
			float fTemp225 = (fConst80 * fRec421[1]);
			fRec423[0] = (fTemp223 + (fTemp224 + (fRec423[1] + fTemp225)));
			fRec421[0] = fRec423[0];
			float fRec422 = ((fTemp225 + fTemp224) + fTemp223);
			fRec420[0] = (fRec421[0] + fRec420[1]);
			fRec418[0] = fRec420[0];
			float fRec419 = fRec422;
			float fTemp226 = (fConst82 * fRec424[1]);
			float fTemp227 = (fConst84 * (((((3.27869998e-06f * fTemp13) + (1.25589997e-06f * fTemp9)) + (0.0226109792f * fTemp10)) + (0.0101913298f * fTemp11)) - ((0.0602908395f * fTemp15) + ((1.91830009e-06f * fTemp14) + (0.0493774936f * fTemp12)))));
			float fTemp228 = (fConst85 * fRec427[1]);
			float fTemp229 = (fConst86 * fRec430[1]);
			fRec432[0] = (fTemp227 + (fTemp228 + (fRec432[1] + fTemp229)));
			fRec430[0] = fRec432[0];
			float fRec431 = ((fTemp229 + fTemp228) + fTemp227);
			fRec429[0] = (fRec430[0] + fRec429[1]);
			fRec427[0] = fRec429[0];
			float fRec428 = fRec431;
			fRec426[0] = (fTemp226 + (fRec428 + fRec426[1]));
			fRec424[0] = fRec426[0];
			float fRec425 = (fRec428 + fTemp226);
			fVec20[0] = (fRec416 + ((0.0513156652f * fTemp7) + (fRec419 + fRec425)));
			output22[i] = FAUSTFLOAT((0.999892831f * (fRec0[0] * fVec20[iConst87])));
			float fTemp230 = (fConst89 * fRec433[1]);
			float fTemp231 = (fConst91 * ((0.0225571264f * fTemp10) - ((1.28254997e-05f * fTemp15) + (((((0.0438632742f * fTemp13) + (0.0427932888f * fTemp9)) + (0.00361614698f * fTemp14)) + (0.00625533238f * fTemp11)) + (0.024712624f * fTemp12)))));
			float fTemp232 = (fConst92 * fRec436[1]);
			float fTemp233 = (fConst93 * fRec439[1]);
			fRec441[0] = (fTemp231 + (fTemp232 + (fRec441[1] + fTemp233)));
			fRec439[0] = fRec441[0];
			float fRec440 = ((fTemp233 + fTemp232) + fTemp231);
			fRec438[0] = (fRec439[0] + fRec438[1]);
			fRec436[0] = fRec438[0];
			float fRec437 = fRec440;
			fRec435[0] = (fTemp230 + (fRec437 + fRec435[1]));
			fRec433[0] = fRec435[0];
			float fRec434 = (fRec437 + fTemp230);
			float fTemp234 = (fConst95 * (0.0f - (((0.0275392588f * fTemp21) + (0.0310740825f * fTemp19)) + (0.047707703f * fTemp20))));
			float fTemp235 = (fConst96 * fRec442[1]);
			fRec444[0] = (fTemp234 + (fRec444[1] + fTemp235));
			fRec442[0] = fRec444[0];
			float fRec443 = (fTemp235 + fTemp234);
			float fTemp236 = (fConst98 * (((((0.04356445f * fTemp24) + (0.0252987836f * fTemp28)) + (0.043821644f * fTemp26)) + (0.0251624361f * fTemp27)) - (0.00719131483f * fTemp25)));
			float fTemp237 = (fConst99 * fRec445[1]);
			float fTemp238 = (fConst100 * fRec448[1]);
			fRec450[0] = (fTemp236 + (fTemp237 + (fRec450[1] + fTemp238)));
			fRec448[0] = fRec450[0];
			float fRec449 = ((fTemp238 + fTemp237) + fTemp236);
			fRec447[0] = (fRec448[0] + fRec447[1]);
			fRec445[0] = fRec447[0];
			float fRec446 = fRec449;
			output23[i] = FAUSTFLOAT((fRec0[0] * ((0.0389952473f * fTemp7) + (fRec434 + (fRec443 + fRec446)))));
			float fTemp239 = (fConst89 * fRec451[1]);
			float fTemp240 = (fConst91 * (((0.0520936586f * fTemp15) + ((0.0245648362f * fTemp12) + (((0.000104623199f * fTemp14) + (0.022579886f * fTemp10)) + (0.00282499567f * fTemp11)))) - ((0.00859174039f * fTemp13) + (0.0428586192f * fTemp9))));
			float fTemp241 = (fConst92 * fRec454[1]);
			float fTemp242 = (fConst93 * fRec457[1]);
			fRec459[0] = (fTemp240 + (fTemp241 + (fRec459[1] + fTemp242)));
			fRec457[0] = fRec459[0];
			float fRec458 = ((fTemp242 + fTemp241) + fTemp240);
			fRec456[0] = (fRec457[0] + fRec456[1]);
			fRec454[0] = fRec456[0];
			float fRec455 = fRec458;
			fRec453[0] = (fTemp239 + (fRec455 + fRec453[1]));
			fRec451[0] = fRec453[0];
			float fRec452 = (fRec455 + fTemp239);
			float fTemp243 = (fConst95 * (0.0f - (((0.0551024303f * fTemp21) + (0.0311034713f * fTemp19)) + (0.0350493789f * fTemp20))));
			float fTemp244 = (fConst96 * fRec460[1]);
			fRec462[0] = (fTemp243 + (fRec462[1] + fTemp244));
			fRec460[0] = fRec462[0];
			float fRec461 = (fTemp244 + fTemp243);
			float fTemp245 = (fConst98 * ((((0.0550333261f * fTemp24) + (0.0438125134f * fTemp28)) + (0.0253732949f * fTemp26)) - ((0.0137405042f * fTemp25) + (0.0249889698f * fTemp27))));
			float fTemp246 = (fConst99 * fRec463[1]);
			float fTemp247 = (fConst100 * fRec466[1]);
			fRec468[0] = (fTemp245 + (fTemp246 + (fRec468[1] + fTemp247)));
			fRec466[0] = fRec468[0];
			float fRec467 = ((fTemp247 + fTemp246) + fTemp245);
			fRec465[0] = (fRec466[0] + fRec465[1]);
			fRec463[0] = fRec465[0];
			float fRec464 = fRec467;
			output24[i] = FAUSTFLOAT((fRec0[0] * ((0.0451742858f * fTemp7) + (fRec452 + (fRec461 + fRec464)))));
			float fTemp248 = (fConst82 * fRec469[1]);
			float fTemp249 = (fConst84 * (((((((0.060286101f * fTemp13) + (9.41999986e-07f * fTemp9)) + (0.0101943975f * fTemp14)) + (0.0226276685f * fTemp10)) + (2.46300004e-07f * fTemp11)) + (0.0493814163f * fTemp12)) + (5.53410018e-06f * fTemp15)));
			float fTemp250 = (fConst85 * fRec472[1]);
			float fTemp251 = (fConst86 * fRec475[1]);
			fRec477[0] = (fTemp249 + (fTemp250 + (fRec477[1] + fTemp251)));
			fRec475[0] = fRec477[0];
			float fRec476 = ((fTemp251 + fTemp250) + fTemp249);
			fRec474[0] = (fRec475[0] + fRec474[1]);
			fRec472[0] = fRec474[0];
			float fRec473 = fRec476;
			fRec471[0] = (fTemp248 + (fRec473 + fRec471[1]));
			fRec469[0] = fRec471[0];
			float fRec470 = (fRec473 + fTemp248);
			float fTemp252 = (fConst75 * (0.0f - (((0.0753505081f * fTemp21) + (0.0311186891f * fTemp19)) + (3.0092001e-06f * fTemp20))));
			float fTemp253 = (fConst76 * fRec478[1]);
			fRec480[0] = (fTemp252 + (fRec480[1] + fTemp253));
			fRec478[0] = fRec480[0];
			float fRec479 = (fTemp253 + fTemp252);
			float fTemp254 = (fConst78 * ((((4.94569986e-06f * fTemp24) + (0.0506584905f * fTemp28)) + (2.134e-07f * fTemp26)) - ((0.020276377f * fTemp25) + (0.0699512139f * fTemp27))));
			float fTemp255 = (fConst79 * fRec481[1]);
			float fTemp256 = (fConst80 * fRec484[1]);
			fRec486[0] = (fTemp254 + (fTemp255 + (fRec486[1] + fTemp256)));
			fRec484[0] = fRec486[0];
			float fRec485 = ((fTemp256 + fTemp255) + fTemp254);
			fRec483[0] = (fRec484[0] + fRec483[1]);
			fRec481[0] = fRec483[0];
			float fRec482 = fRec485;
			fVec21[0] = ((0.051312048f * fTemp7) + (fRec470 + (fRec479 + fRec482)));
			output25[i] = FAUSTFLOAT((0.999892831f * (fRec0[0] * fVec21[iConst87])));
			float fTemp257 = (fConst95 * ((0.0275366008f * fTemp20) - ((0.0477022678f * fTemp21) + (0.0310844164f * fTemp19))));
			float fTemp258 = (fConst96 * fRec487[1]);
			fRec489[0] = (fTemp257 + (fRec489[1] + fTemp258));
			fRec487[0] = fRec489[0];
			float fRec488 = (fTemp258 + fTemp257);
			float fTemp259 = (fConst98 * ((0.0438174382f * fTemp28) - ((((0.0435620397f * fTemp24) + (0.00717064785f * fTemp25)) + (0.025294859f * fTemp26)) + (0.0251593087f * fTemp27))));
			float fTemp260 = (fConst99 * fRec490[1]);
			float fTemp261 = (fConst100 * fRec493[1]);
			fRec495[0] = (fTemp259 + (fTemp260 + (fRec495[1] + fTemp261)));
			fRec493[0] = fRec495[0];
			float fRec494 = ((fTemp261 + fTemp260) + fTemp259);
			fRec492[0] = (fRec493[0] + fRec492[1]);
			fRec490[0] = fRec492[0];
			float fRec491 = fRec494;
			float fTemp262 = (fConst89 * fRec496[1]);
			float fTemp263 = (fConst91 * (((0.0247124508f * fTemp12) + ((((9.34639957e-06f * fTemp13) + (0.0427875109f * fTemp9)) + (0.0225271285f * fTemp10)) + (0.00361575908f * fTemp11))) - ((0.00625694171f * fTemp14) + (0.043863859f * fTemp15))));
			float fTemp264 = (fConst92 * fRec499[1]);
			float fTemp265 = (fConst93 * fRec502[1]);
			fRec504[0] = (fTemp263 + (fTemp264 + (fRec504[1] + fTemp265)));
			fRec502[0] = fRec504[0];
			float fRec503 = ((fTemp265 + fTemp264) + fTemp263);
			fRec501[0] = (fRec502[0] + fRec501[1]);
			fRec499[0] = fRec501[0];
			float fRec500 = fRec503;
			fRec498[0] = (fTemp262 + (fRec500 + fRec498[1]));
			fRec496[0] = fRec498[0];
			float fRec497 = (fRec500 + fTemp262);
			output26[i] = FAUSTFLOAT((fRec0[0] * ((0.0389983803f * fTemp7) + (fRec488 + (fRec491 + fRec497)))));
			float fTemp266 = (fConst95 * ((0.0550909229f * fTemp20) - ((0.0350420922f * fTemp21) + (0.031103991f * fTemp19))));
			float fTemp267 = (fConst96 * fRec505[1]);
			fRec507[0] = (fTemp266 + (fRec507[1] + fTemp267));
			fRec505[0] = fRec507[0];
			float fRec506 = (fTemp267 + fTemp266);
			float fTemp268 = (fConst100 * fRec511[1]);
			float fTemp269 = (fConst99 * fRec508[1]);
			float fTemp270 = (fConst98 * (((0.0253775977f * fTemp28) + (0.0249799546f * fTemp27)) - (((0.0550207198f * fTemp24) + (0.0137372883f * fTemp25)) + (0.0438250452f * fTemp26))));
			fRec513[0] = (((fRec513[1] + fTemp268) + fTemp269) + fTemp270);
			fRec511[0] = fRec513[0];
			float fRec512 = ((fTemp268 + fTemp269) + fTemp270);
			fRec510[0] = (fRec511[0] + fRec510[1]);
			fRec508[0] = fRec510[0];
			float fRec509 = fRec512;
			float fTemp271 = (fConst91 * ((((0.0428688265f * fTemp9) + (0.0028221251f * fTemp14)) + (0.0225989278f * fTemp10)) - ((0.0085950112f * fTemp15) + (((0.0520783328f * fTemp13) + (8.48522977e-05f * fTemp11)) + (0.0245707203f * fTemp12)))));
			float fTemp272 = (fConst92 * fRec517[1]);
			float fTemp273 = (fConst93 * fRec520[1]);
			fRec522[0] = (fTemp271 + (fTemp272 + (fRec522[1] + fTemp273)));
			fRec520[0] = fRec522[0];
			float fRec521 = ((fTemp273 + fTemp272) + fTemp271);
			fRec519[0] = (fRec519[1] + fRec520[0]);
			fRec517[0] = fRec519[0];
			float fRec518 = fRec521;
			float fTemp274 = (fConst89 * fRec514[1]);
			fRec516[0] = (fRec518 + (fRec516[1] + fTemp274));
			fRec514[0] = fRec516[0];
			float fRec515 = (fTemp274 + fRec518);
			output27[i] = FAUSTFLOAT((fRec0[0] * (((fRec506 + fRec509) + fRec515) + (0.0451631024f * fTemp7))));
			float fTemp275 = (fConst75 * (((3.88260014e-06f * fTemp21) + (0.0753510371f * fTemp20)) - (0.0311100706f * fTemp19)));
			float fTemp276 = (fConst76 * fRec523[1]);
			fRec525[0] = (fTemp275 + (fRec525[1] + fTemp276));
			fRec523[0] = fRec525[0];
			float fRec524 = (fTemp276 + fTemp275);
			float fTemp277 = (fConst79 * fRec526[1]);
			float fTemp278 = (fConst80 * fRec529[1]);
			float fTemp279 = (fConst78 * (((0.0699535608f * fTemp27) + (6.40969984e-06f * fTemp24)) - (((0.0202828702f * fTemp25) + (3.11620011e-06f * fTemp28)) + (0.0506437719f * fTemp26))));
			fRec531[0] = ((fTemp277 + (fRec531[1] + fTemp278)) + fTemp279);
			fRec529[0] = fRec531[0];
			float fRec530 = ((fTemp278 + fTemp277) + fTemp279);
			fRec528[0] = (fRec528[1] + fRec529[0]);
			fRec526[0] = fRec528[0];
			float fRec527 = fRec530;
			float fTemp280 = (fConst85 * fRec535[1]);
			float fTemp281 = (fConst86 * fRec538[1]);
			float fTemp282 = (fConst84 * (((((6.92689991e-06f * fTemp13) + (7.53400002e-07f * fTemp14)) + (0.0226198863f * fTemp10)) + (0.0602886081f * fTemp15)) - ((0.0493706539f * fTemp12) + ((5.07279992e-06f * fTemp9) + (0.0102069797f * fTemp11)))));
			fRec540[0] = (fTemp280 + (fTemp281 + (fTemp282 + fRec540[1])));
			fRec538[0] = fRec540[0];
			float fRec539 = ((fTemp282 + fTemp281) + fTemp280);
			fRec537[0] = (fRec537[1] + fRec538[0]);
			fRec535[0] = fRec537[0];
			float fRec536 = fRec539;
			float fTemp283 = (fConst82 * fRec532[1]);
			fRec534[0] = (fRec536 + (fRec534[1] + fTemp283));
			fRec532[0] = fRec534[0];
			float fRec533 = (fTemp283 + fRec536);
			fVec22[0] = ((0.0513112508f * fTemp7) + ((fRec524 + fRec527) + fRec533));
			output28[i] = FAUSTFLOAT((0.999892831f * (fRec0[0] * fVec22[iConst87])));
			float fTemp284 = (fConst95 * (((0.0476932786f * fTemp20) + (0.0275351312f * fTemp21)) - (0.0310895834f * fTemp19)));
			float fTemp285 = (fConst96 * fRec541[1]);
			fRec543[0] = (fTemp284 + (fTemp285 + fRec543[1]));
			fRec541[0] = fRec543[0];
			float fRec542 = (fTemp285 + fTemp284);
			float fTemp286 = (fConst98 * (((0.0251447037f * fTemp27) + (0.0435531959f * fTemp24)) - (((0.0253003445f * fTemp28) + (0.00716599543f * fTemp25)) + (0.043829482f * fTemp26))));
			float fTemp287 = (fConst99 * fRec544[1]);
			float fTemp288 = (fConst100 * fRec547[1]);
			fRec549[0] = (fTemp286 + (fTemp287 + (fRec549[1] + fTemp288)));
			fRec547[0] = fRec549[0];
			float fRec548 = ((fTemp288 + fTemp287) + fTemp286);
			fRec546[0] = (fRec546[1] + fRec547[0]);
			fRec544[0] = fRec546[0];
			float fRec545 = fRec548;
			float fTemp289 = (fConst89 * fRec550[1]);
			float fTemp290 = (fConst91 * (((((0.0438416526f * fTemp13) + (0.00361537864f * fTemp14)) + (0.0225419905f * fTemp10)) + (0.00627214322f * fTemp11)) - ((2.67740006e-06f * fTemp15) + ((0.0427974351f * fTemp9) + (0.024716828f * fTemp12)))));
			float fTemp291 = (fConst92 * fRec553[1]);
			float fTemp292 = (fConst93 * fRec556[1]);
			fRec558[0] = (fTemp290 + (fTemp291 + (fRec558[1] + fTemp292)));
			fRec556[0] = fRec558[0];
			float fRec557 = ((fTemp292 + fTemp291) + fTemp290);
			fRec555[0] = (fRec555[1] + fRec556[0]);
			fRec553[0] = fRec555[0];
			float fRec554 = fRec557;
			fRec552[0] = ((fRec552[1] + fTemp289) + fRec554);
			fRec550[0] = fRec552[0];
			float fRec551 = (fTemp289 + fRec554);
			output29[i] = FAUSTFLOAT(((((fRec542 + fRec545) + fRec551) + (0.0389937758f * fTemp7)) * fRec0[0]));
			float fTemp293 = (fConst95 * (((0.055092264f * fTemp21) + (0.0350449346f * fTemp20)) - (0.0311054997f * fTemp19)));
			float fTemp294 = (fConst96 * fRec559[1]);
			fRec561[0] = (fTemp293 + (fRec561[1] + fTemp294));
			fRec559[0] = fRec561[0];
			float fRec560 = (fTemp294 + fTemp293);
			float fTemp295 = (fConst98 * ((0.0550222285f * fTemp24) - ((((0.0438246243f * fTemp28) + (0.0137371952f * fTemp25)) + (0.0253771991f * fTemp26)) + (0.0249779373f * fTemp27))));
			float fTemp296 = (fConst99 * fRec562[1]);
			float fTemp297 = (fConst100 * fRec565[1]);
			fRec567[0] = (fTemp295 + (fTemp296 + (fRec567[1] + fTemp297)));
			fRec565[0] = fRec567[0];
			float fRec566 = ((fTemp297 + fTemp296) + fTemp295);
			fRec564[0] = (fRec564[1] + fRec565[0]);
			fRec562[0] = fRec564[0];
			float fRec563 = fRec566;
			float fTemp298 = (fConst89 * fRec568[1]);
			float fTemp299 = (fConst91 * ((((0.00859783869f * fTemp13) + (0.0225961804f * fTemp10)) + (0.024573572f * fTemp12)) - ((((0.0428675599f * fTemp9) + (8.81157976e-05f * fTemp14)) + (0.00282131881f * fTemp11)) + (0.0520746484f * fTemp15))));
			float fTemp300 = (fConst92 * fRec571[1]);
			float fTemp301 = (fConst93 * fRec574[1]);
			fRec576[0] = (fTemp299 + (fTemp300 + (fRec576[1] + fTemp301)));
			fRec574[0] = fRec576[0];
			float fRec575 = ((fTemp301 + fTemp300) + fTemp299);
			fRec573[0] = (fRec573[1] + fRec574[0]);
			fRec571[0] = fRec573[0];
			float fRec572 = fRec575;
			fRec570[0] = ((fRec570[1] + fTemp298) + fRec572);
			fRec568[0] = fRec570[0];
			float fRec569 = (fTemp298 + fRec572);
			output30[i] = FAUSTFLOAT((((0.0451662317f * fTemp7) + ((fRec560 + fRec563) + fRec569)) * fRec0[0]));
			fRec0[1] = fRec0[0];
			fRec1[1] = fRec1[0];
			fRec2[2] = fRec2[1];
			fRec2[1] = fRec2[0];
			fRec3[1] = fRec3[0];
			fRec13[2] = fRec13[1];
			fRec13[1] = fRec13[0];
			fRec14[2] = fRec14[1];
			fRec14[1] = fRec14[0];
			fRec15[2] = fRec15[1];
			fRec15[1] = fRec15[0];
			fRec16[2] = fRec16[1];
			fRec16[1] = fRec16[0];
			fRec17[2] = fRec17[1];
			fRec17[1] = fRec17[0];
			fRec18[2] = fRec18[1];
			fRec18[1] = fRec18[0];
			fRec19[2] = fRec19[1];
			fRec19[1] = fRec19[0];
			fRec12[1] = fRec12[0];
			fRec10[1] = fRec10[0];
			fRec9[1] = fRec9[0];
			fRec7[1] = fRec7[0];
			fRec6[1] = fRec6[0];
			fRec4[1] = fRec4[0];
			fRec23[2] = fRec23[1];
			fRec23[1] = fRec23[0];
			fRec24[2] = fRec24[1];
			fRec24[1] = fRec24[0];
			fRec25[2] = fRec25[1];
			fRec25[1] = fRec25[0];
			fRec22[1] = fRec22[0];
			fRec20[1] = fRec20[0];
			fRec32[2] = fRec32[1];
			fRec32[1] = fRec32[0];
			fRec33[2] = fRec33[1];
			fRec33[1] = fRec33[0];
			fRec34[2] = fRec34[1];
			fRec34[1] = fRec34[0];
			fRec35[2] = fRec35[1];
			fRec35[1] = fRec35[0];
			fRec36[2] = fRec36[1];
			fRec36[1] = fRec36[0];
			fRec31[1] = fRec31[0];
			fRec29[1] = fRec29[0];
			fRec28[1] = fRec28[0];
			fRec26[1] = fRec26[0];
			IOTA = (IOTA + 1);
			fRec45[1] = fRec45[0];
			fRec43[1] = fRec43[0];
			fRec42[1] = fRec42[0];
			fRec40[1] = fRec40[0];
			fRec39[1] = fRec39[0];
			fRec37[1] = fRec37[0];
			fRec48[1] = fRec48[0];
			fRec46[1] = fRec46[0];
			fRec54[1] = fRec54[0];
			fRec52[1] = fRec52[0];
			fRec51[1] = fRec51[0];
			fRec49[1] = fRec49[0];
			fRec63[1] = fRec63[0];
			fRec61[1] = fRec61[0];
			fRec60[1] = fRec60[0];
			fRec58[1] = fRec58[0];
			fRec57[1] = fRec57[0];
			fRec55[1] = fRec55[0];
			fRec66[1] = fRec66[0];
			fRec64[1] = fRec64[0];
			fRec72[1] = fRec72[0];
			fRec70[1] = fRec70[0];
			fRec69[1] = fRec69[0];
			fRec67[1] = fRec67[0];
			fRec75[1] = fRec75[0];
			fRec73[1] = fRec73[0];
			fRec81[1] = fRec81[0];
			fRec79[1] = fRec79[0];
			fRec78[1] = fRec78[0];
			fRec76[1] = fRec76[0];
			fRec90[1] = fRec90[0];
			fRec88[1] = fRec88[0];
			fRec87[1] = fRec87[0];
			fRec85[1] = fRec85[0];
			fRec84[1] = fRec84[0];
			fRec82[1] = fRec82[0];
			fRec93[1] = fRec93[0];
			fRec91[1] = fRec91[0];
			fRec99[1] = fRec99[0];
			fRec97[1] = fRec97[0];
			fRec96[1] = fRec96[0];
			fRec94[1] = fRec94[0];
			fRec108[1] = fRec108[0];
			fRec106[1] = fRec106[0];
			fRec105[1] = fRec105[0];
			fRec103[1] = fRec103[0];
			fRec102[1] = fRec102[0];
			fRec100[1] = fRec100[0];
			fRec111[1] = fRec111[0];
			fRec109[1] = fRec109[0];
			fRec117[1] = fRec117[0];
			fRec115[1] = fRec115[0];
			fRec114[1] = fRec114[0];
			fRec112[1] = fRec112[0];
			fRec126[1] = fRec126[0];
			fRec124[1] = fRec124[0];
			fRec123[1] = fRec123[0];
			fRec121[1] = fRec121[0];
			fRec120[1] = fRec120[0];
			fRec118[1] = fRec118[0];
			fRec135[1] = fRec135[0];
			fRec133[1] = fRec133[0];
			fRec132[1] = fRec132[0];
			fRec130[1] = fRec130[0];
			fRec129[1] = fRec129[0];
			fRec127[1] = fRec127[0];
			fRec138[1] = fRec138[0];
			fRec136[1] = fRec136[0];
			fRec144[1] = fRec144[0];
			fRec142[1] = fRec142[0];
			fRec141[1] = fRec141[0];
			fRec139[1] = fRec139[0];
			fRec153[1] = fRec153[0];
			fRec151[1] = fRec151[0];
			fRec150[1] = fRec150[0];
			fRec148[1] = fRec148[0];
			fRec147[1] = fRec147[0];
			fRec145[1] = fRec145[0];
			fRec156[1] = fRec156[0];
			fRec154[1] = fRec154[0];
			fRec162[1] = fRec162[0];
			fRec160[1] = fRec160[0];
			fRec159[1] = fRec159[0];
			fRec157[1] = fRec157[0];
			fRec171[1] = fRec171[0];
			fRec169[1] = fRec169[0];
			fRec168[1] = fRec168[0];
			fRec166[1] = fRec166[0];
			fRec165[1] = fRec165[0];
			fRec163[1] = fRec163[0];
			fRec174[1] = fRec174[0];
			fRec172[1] = fRec172[0];
			fRec180[1] = fRec180[0];
			fRec178[1] = fRec178[0];
			fRec177[1] = fRec177[0];
			fRec175[1] = fRec175[0];
			fRec189[1] = fRec189[0];
			fRec187[1] = fRec187[0];
			fRec186[1] = fRec186[0];
			fRec184[1] = fRec184[0];
			fRec183[1] = fRec183[0];
			fRec181[1] = fRec181[0];
			fRec192[1] = fRec192[0];
			fRec190[1] = fRec190[0];
			fRec198[1] = fRec198[0];
			fRec196[1] = fRec196[0];
			fRec195[1] = fRec195[0];
			fRec193[1] = fRec193[0];
			fRec207[1] = fRec207[0];
			fRec205[1] = fRec205[0];
			fRec204[1] = fRec204[0];
			fRec202[1] = fRec202[0];
			fRec201[1] = fRec201[0];
			fRec199[1] = fRec199[0];
			fRec210[1] = fRec210[0];
			fRec208[1] = fRec208[0];
			fRec216[1] = fRec216[0];
			fRec214[1] = fRec214[0];
			fRec213[1] = fRec213[0];
			fRec211[1] = fRec211[0];
			fRec225[1] = fRec225[0];
			fRec223[1] = fRec223[0];
			fRec222[1] = fRec222[0];
			fRec220[1] = fRec220[0];
			fRec219[1] = fRec219[0];
			fRec217[1] = fRec217[0];
			fRec228[1] = fRec228[0];
			fRec226[1] = fRec226[0];
			fRec234[1] = fRec234[0];
			fRec232[1] = fRec232[0];
			fRec231[1] = fRec231[0];
			fRec229[1] = fRec229[0];
			fRec243[1] = fRec243[0];
			fRec241[1] = fRec241[0];
			fRec240[1] = fRec240[0];
			fRec238[1] = fRec238[0];
			fRec237[1] = fRec237[0];
			fRec235[1] = fRec235[0];
			fRec246[1] = fRec246[0];
			fRec244[1] = fRec244[0];
			fRec252[1] = fRec252[0];
			fRec250[1] = fRec250[0];
			fRec249[1] = fRec249[0];
			fRec247[1] = fRec247[0];
			fRec261[1] = fRec261[0];
			fRec259[1] = fRec259[0];
			fRec258[1] = fRec258[0];
			fRec256[1] = fRec256[0];
			fRec255[1] = fRec255[0];
			fRec253[1] = fRec253[0];
			fRec264[1] = fRec264[0];
			fRec262[1] = fRec262[0];
			fRec270[1] = fRec270[0];
			fRec268[1] = fRec268[0];
			fRec267[1] = fRec267[0];
			fRec265[1] = fRec265[0];
			fRec279[1] = fRec279[0];
			fRec277[1] = fRec277[0];
			fRec276[1] = fRec276[0];
			fRec274[1] = fRec274[0];
			fRec273[1] = fRec273[0];
			fRec271[1] = fRec271[0];
			fRec282[1] = fRec282[0];
			fRec280[1] = fRec280[0];
			fRec288[1] = fRec288[0];
			fRec286[1] = fRec286[0];
			fRec285[1] = fRec285[0];
			fRec283[1] = fRec283[0];
			fRec297[1] = fRec297[0];
			fRec295[1] = fRec295[0];
			fRec294[1] = fRec294[0];
			fRec292[1] = fRec292[0];
			fRec291[1] = fRec291[0];
			fRec289[1] = fRec289[0];
			fRec300[1] = fRec300[0];
			fRec298[1] = fRec298[0];
			fRec306[1] = fRec306[0];
			fRec304[1] = fRec304[0];
			fRec303[1] = fRec303[0];
			fRec301[1] = fRec301[0];
			fRec315[1] = fRec315[0];
			fRec313[1] = fRec313[0];
			fRec312[1] = fRec312[0];
			fRec310[1] = fRec310[0];
			fRec309[1] = fRec309[0];
			fRec307[1] = fRec307[0];
			fRec318[1] = fRec318[0];
			fRec316[1] = fRec316[0];
			fRec324[1] = fRec324[0];
			fRec322[1] = fRec322[0];
			fRec321[1] = fRec321[0];
			fRec319[1] = fRec319[0];
			fRec333[1] = fRec333[0];
			fRec331[1] = fRec331[0];
			fRec330[1] = fRec330[0];
			fRec328[1] = fRec328[0];
			fRec327[1] = fRec327[0];
			fRec325[1] = fRec325[0];
			fRec336[1] = fRec336[0];
			fRec334[1] = fRec334[0];
			fRec342[1] = fRec342[0];
			fRec340[1] = fRec340[0];
			fRec339[1] = fRec339[0];
			fRec337[1] = fRec337[0];
			fRec351[1] = fRec351[0];
			fRec349[1] = fRec349[0];
			fRec348[1] = fRec348[0];
			fRec346[1] = fRec346[0];
			fRec345[1] = fRec345[0];
			fRec343[1] = fRec343[0];
			fRec354[1] = fRec354[0];
			fRec352[1] = fRec352[0];
			fRec360[1] = fRec360[0];
			fRec358[1] = fRec358[0];
			fRec357[1] = fRec357[0];
			fRec355[1] = fRec355[0];
			fRec363[1] = fRec363[0];
			fRec361[1] = fRec361[0];
			fRec369[1] = fRec369[0];
			fRec367[1] = fRec367[0];
			fRec366[1] = fRec366[0];
			fRec364[1] = fRec364[0];
			fRec378[1] = fRec378[0];
			fRec376[1] = fRec376[0];
			fRec375[1] = fRec375[0];
			fRec373[1] = fRec373[0];
			fRec372[1] = fRec372[0];
			fRec370[1] = fRec370[0];
			fVec19[1] = fVec19[0];
			fRec387[1] = fRec387[0];
			fRec385[1] = fRec385[0];
			fRec384[1] = fRec384[0];
			fRec382[1] = fRec382[0];
			fRec381[1] = fRec381[0];
			fRec379[1] = fRec379[0];
			fRec390[1] = fRec390[0];
			fRec388[1] = fRec388[0];
			fRec396[1] = fRec396[0];
			fRec394[1] = fRec394[0];
			fRec393[1] = fRec393[0];
			fRec391[1] = fRec391[0];
			fRec405[1] = fRec405[0];
			fRec403[1] = fRec403[0];
			fRec402[1] = fRec402[0];
			fRec400[1] = fRec400[0];
			fRec399[1] = fRec399[0];
			fRec397[1] = fRec397[0];
			fRec411[1] = fRec411[0];
			fRec409[1] = fRec409[0];
			fRec408[1] = fRec408[0];
			fRec406[1] = fRec406[0];
			fRec414[1] = fRec414[0];
			fRec412[1] = fRec412[0];
			fRec417[1] = fRec417[0];
			fRec415[1] = fRec415[0];
			fRec423[1] = fRec423[0];
			fRec421[1] = fRec421[0];
			fRec420[1] = fRec420[0];
			fRec418[1] = fRec418[0];
			fRec432[1] = fRec432[0];
			fRec430[1] = fRec430[0];
			fRec429[1] = fRec429[0];
			fRec427[1] = fRec427[0];
			fRec426[1] = fRec426[0];
			fRec424[1] = fRec424[0];
			fVec20[1] = fVec20[0];
			fRec441[1] = fRec441[0];
			fRec439[1] = fRec439[0];
			fRec438[1] = fRec438[0];
			fRec436[1] = fRec436[0];
			fRec435[1] = fRec435[0];
			fRec433[1] = fRec433[0];
			fRec444[1] = fRec444[0];
			fRec442[1] = fRec442[0];
			fRec450[1] = fRec450[0];
			fRec448[1] = fRec448[0];
			fRec447[1] = fRec447[0];
			fRec445[1] = fRec445[0];
			fRec459[1] = fRec459[0];
			fRec457[1] = fRec457[0];
			fRec456[1] = fRec456[0];
			fRec454[1] = fRec454[0];
			fRec453[1] = fRec453[0];
			fRec451[1] = fRec451[0];
			fRec462[1] = fRec462[0];
			fRec460[1] = fRec460[0];
			fRec468[1] = fRec468[0];
			fRec466[1] = fRec466[0];
			fRec465[1] = fRec465[0];
			fRec463[1] = fRec463[0];
			fRec477[1] = fRec477[0];
			fRec475[1] = fRec475[0];
			fRec474[1] = fRec474[0];
			fRec472[1] = fRec472[0];
			fRec471[1] = fRec471[0];
			fRec469[1] = fRec469[0];
			fRec480[1] = fRec480[0];
			fRec478[1] = fRec478[0];
			fRec486[1] = fRec486[0];
			fRec484[1] = fRec484[0];
			fRec483[1] = fRec483[0];
			fRec481[1] = fRec481[0];
			fVec21[1] = fVec21[0];
			fRec489[1] = fRec489[0];
			fRec487[1] = fRec487[0];
			fRec495[1] = fRec495[0];
			fRec493[1] = fRec493[0];
			fRec492[1] = fRec492[0];
			fRec490[1] = fRec490[0];
			fRec504[1] = fRec504[0];
			fRec502[1] = fRec502[0];
			fRec501[1] = fRec501[0];
			fRec499[1] = fRec499[0];
			fRec498[1] = fRec498[0];
			fRec496[1] = fRec496[0];
			fRec507[1] = fRec507[0];
			fRec505[1] = fRec505[0];
			fRec513[1] = fRec513[0];
			fRec511[1] = fRec511[0];
			fRec510[1] = fRec510[0];
			fRec508[1] = fRec508[0];
			fRec522[1] = fRec522[0];
			fRec520[1] = fRec520[0];
			fRec519[1] = fRec519[0];
			fRec517[1] = fRec517[0];
			fRec516[1] = fRec516[0];
			fRec514[1] = fRec514[0];
			fRec525[1] = fRec525[0];
			fRec523[1] = fRec523[0];
			fRec531[1] = fRec531[0];
			fRec529[1] = fRec529[0];
			fRec528[1] = fRec528[0];
			fRec526[1] = fRec526[0];
			fRec540[1] = fRec540[0];
			fRec538[1] = fRec538[0];
			fRec537[1] = fRec537[0];
			fRec535[1] = fRec535[0];
			fRec534[1] = fRec534[0];
			fRec532[1] = fRec532[0];
			fVec22[1] = fVec22[0];
			fRec543[1] = fRec543[0];
			fRec541[1] = fRec541[0];
			fRec549[1] = fRec549[0];
			fRec547[1] = fRec547[0];
			fRec546[1] = fRec546[0];
			fRec544[1] = fRec544[0];
			fRec558[1] = fRec558[0];
			fRec556[1] = fRec556[0];
			fRec555[1] = fRec555[0];
			fRec553[1] = fRec553[0];
			fRec552[1] = fRec552[0];
			fRec550[1] = fRec550[0];
			fRec561[1] = fRec561[0];
			fRec559[1] = fRec559[0];
			fRec567[1] = fRec567[0];
			fRec565[1] = fRec565[0];
			fRec564[1] = fRec564[0];
			fRec562[1] = fRec562[0];
			fRec576[1] = fRec576[0];
			fRec574[1] = fRec574[0];
			fRec573[1] = fRec573[0];
			fRec571[1] = fRec571[0];
			fRec570[1] = fRec570[0];
			fRec568[1] = fRec568[0];
			
		}
		
	}

	
};

//----------------------------------------------------------------------------
// SuperCollider/Faust interface
//----------------------------------------------------------------------------

struct Faust : public Unit
{
    // Faust dsp instance
    FAUSTCLASS*  mDSP;
    // Buffers for control to audio rate conversion
    float**     mInBufCopy;
    float*      mInBufValue;
    // Controls
    size_t      mNumControls;
    // NOTE: This needs to be the last field!
    //
    // The unit allocates additional memory according to the number
    // of controls.
    Control     mControls[0];

    int getNumAudioInputs() { return mDSP->getNumInputs(); }
};

// Global state

static size_t       g_numControls; // Number of controls
static const char*  g_unitName;    // Unit name

// Initialize the global state with unit name and sample rate.
void initState(const std::string& name, int sampleRate);

// Return the unit size in bytes, including static fields and controls.
static size_t unitSize();

// Convert a file name to a valid unit name.
static std::string fileNameToUnitName(const std::string& fileName);

// Convert the XML unit name to a valid class name.
static std::string normalizeClassName(const std::string& name);

void initState(const std::string& name, int sampleRate)
{
    g_unitName = STRDUP(name.c_str());

    mydsp* dsp = new FAUSTCLASS;
    ControlCounter* cc = new ControlCounter;

    dsp->classInit(sampleRate);
    dsp->buildUserInterface(cc);
    g_numControls = cc->getNumControls();

    delete dsp;
    delete cc;
}

size_t unitSize()
{
    return sizeof(Faust) + g_numControls * sizeof(Control);
}

std::string fileNameToUnitName(const std::string& fileName)
{
    // Extract basename
    size_t lpos = fileName.rfind('/', fileName.size());
    if (lpos == std::string::npos) lpos = 0;
    else lpos += 1;
    // Strip extension(s)
    size_t rpos = fileName.find('.', lpos);
    // Return substring
    return fileName.substr(lpos, rpos > lpos ? rpos - lpos : 0);
}

// Globals

static InterfaceTable* ft;

// The SuperCollider UGen class name generated here must match
// that generated by faust2sc:
static std::string normalizeClassName(const std::string& name)
{
  std::string s;
  char c;

  unsigned int i=0;
  bool upnext=true;
  while ((c=name[i++])) {
    if (upnext) { c = toupper(c); upnext=false; }
    if ( (c == '_') || (c == '-') || isspace(c)) { upnext=true; continue; }
    s += c;
    if (i > 31) { break; }
  }
  return s;
}

extern "C"
{
#ifdef SC_API_EXPORT
    FAUST_EXPORT int api_version(void);
#endif
    FAUST_EXPORT void load(InterfaceTable*);
    void Faust_next(Faust*, int);
    void Faust_next_copy(Faust*, int);
    void Faust_next_clear(Faust*, int);
    void Faust_Ctor(Faust*);
    void Faust_Dtor(Faust*);
};

inline static void fillBuffer(float* dst, int n, float v)
{
    Fill(n, dst, v);
}

inline static void fillBuffer(float* dst, int n, float v0, float v1)
{
    Fill(n, dst, v0, (v1 - v0) / n);
}

inline static void copyBuffer(float* dst, int n, float* src)
{
    Copy(n, dst, src);
}

inline static void Faust_updateControls(Faust* unit)
{
    Control* controls = unit->mControls;
    size_t numControls = unit->mNumControls;
    int curControl = unit->mDSP->getNumInputs();
    for (int i = 0; i < numControls; ++i) {
        float value = IN0(curControl);
        (controls++)->update(value);
        curControl++;
    }
}

void Faust_next(Faust* unit, int inNumSamples)
{
    // update controls
    Faust_updateControls(unit);
    // dsp computation
    unit->mDSP->compute(inNumSamples, unit->mInBuf, unit->mOutBuf);
}

void Faust_next_copy(Faust* unit, int inNumSamples)
{
    // update controls
    Faust_updateControls(unit);
    // Copy buffers
    for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
        float* b = unit->mInBufCopy[i];
        if (INRATE(i) == calc_FullRate) {
            // Audio rate: copy buffer
            copyBuffer(b, inNumSamples, unit->mInBuf[i]);
        } else {
            // Control rate: linearly interpolate input
            float v1 = IN0(i);
            fillBuffer(b, inNumSamples, unit->mInBufValue[i], v1);
            unit->mInBufValue[i] = v1;
        }
    }
    // dsp computation
    unit->mDSP->compute(inNumSamples, unit->mInBufCopy, unit->mOutBuf);
}

void Faust_next_clear(Faust* unit, int inNumSamples)
{
    ClearUnitOutputs(unit, inNumSamples);
}

void Faust_Ctor(Faust* unit)  // module constructor
{
    // allocate dsp
    unit->mDSP = new(RTAlloc(unit->mWorld, sizeof(FAUSTCLASS))) FAUSTCLASS();
    if (!unit->mDSP) {
        Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
        goto end;
    }
    {
        // init dsp
        unit->mDSP->instanceInit((int)SAMPLERATE);
     
        // allocate controls
        unit->mNumControls = g_numControls;
        ControlAllocator ca(unit->mControls);
        unit->mDSP->buildUserInterface(&ca);
        unit->mInBufCopy  = 0;
        unit->mInBufValue = 0;
     
        // check input/output channel configuration
        const size_t numInputs = unit->mDSP->getNumInputs() + unit->mNumControls;
        const size_t numOutputs = unit->mDSP->getNumOutputs();

        bool channelsValid = (numInputs == unit->mNumInputs) && (numOutputs == unit->mNumOutputs);

        if (channelsValid) {
            bool rateValid = true;
            for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
                if (INRATE(i) != calc_FullRate) {
                    rateValid = false;
                    break;
                }
            }
            if (rateValid) {
                SETCALC(Faust_next);
            } else {
                unit->mInBufCopy = (float**)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*sizeof(float*));
                if (!unit->mInBufCopy) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                // Allocate memory for input buffer copies (numInputs * bufLength)
                // and linear interpolation state (numInputs)
                // = numInputs * (bufLength + 1)
                unit->mInBufValue = (float*)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*sizeof(float));
                if (!unit->mInBufValue) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                // Aquire memory for interpolator state.
                float* mem = (float*)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*BUFLENGTH*sizeof(float));
                if (mem) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
                    // Initialize interpolator.
                    unit->mInBufValue[i] = IN0(i);
                    // Aquire buffer memory.
                    unit->mInBufCopy[i] = mem;
                    mem += BUFLENGTH;
                }
                SETCALC(Faust_next_copy);
            }
    #if !defined(NDEBUG)
            Print("Faust[%s]:\n", g_unitName);
            Print("    Inputs:   %d\n"
                  "    Outputs:  %d\n"
                  "    Callback: %s\n",
                  numInputs, numOutputs,
                  unit->mCalcFunc == (UnitCalcFunc)Faust_next ? "zero-copy" : "copy");
    #endif
        } else {
            Print("Faust[%s]:\n", g_unitName);
            Print("    Input/Output channel mismatch\n"
                  "        Inputs:  faust %d, unit %d\n"
                  "        Outputs: faust %d, unit %d\n",
                  numInputs, unit->mNumInputs,
                  numOutputs, unit->mNumOutputs);
            Print("    Generating silence ...\n");
            SETCALC(Faust_next_clear);
        }
    }
    
end:
    // Fix for https://github.com/grame-cncm/faust/issues/13
    ClearUnitOutputs(unit, 1);
}

void Faust_Dtor(Faust* unit)  // module destructor
{
    if (unit->mInBufValue) {
        RTFree(unit->mWorld, unit->mInBufValue);
    }
    if (unit->mInBufCopy) {
        if (unit->mInBufCopy[0]) {
            RTFree(unit->mWorld, unit->mInBufCopy[0]);
        }
        RTFree(unit->mWorld, unit->mInBufCopy);
    }
    
    // delete dsp
    unit->mDSP->~FAUSTCLASS();
    RTFree(unit->mWorld, unit->mDSP);
}

#ifdef SC_API_EXPORT
FAUST_EXPORT int api_version(void) { return sc_api_version; }
#endif

FAUST_EXPORT void load(InterfaceTable* inTable)
{
    ft = inTable;

    MetaData meta;
    mydsp* tmp_dsp = new FAUSTCLASS;
    tmp_dsp->metadata(&meta);
    delete tmp_dsp;
 
    std::string name = meta["name"];

    if (name.empty()) {
        name = fileNameToUnitName(__FILE__);
    }
  
    name = normalizeClassName(name);

#if !defined(NDEBUG) & defined(SC_API_EXPORT)
    Print("Faust: supercollider.cpp: sc_api_version = %d\n", sc_api_version);
#endif

    if (name.empty()) {
        // Catch empty name
        Print("Faust [supercollider.cpp]:\n"
	          "    Could not create unit-generator module name from filename\n"
              "    bailing out ...\n");
        return;
    }

    if (strncmp(name.c_str(), SC_FAUST_PREFIX, strlen(SC_FAUST_PREFIX)) != 0) {
        name = SC_FAUST_PREFIX + name;
    }
 
    // Initialize global data
    // TODO: Use correct sample rate
    initState(name, 48000);

    // Register ugen
    (*ft->fDefineUnit)(
        (char*)name.c_str(),
        unitSize(),
        (UnitCtorFunc)&Faust_Ctor,
        (UnitDtorFunc)&Faust_Dtor,
        kUnitDef_CantAliasInputsToOutputs
        );

#if !defined(NDEBUG)
    Print("Faust: %s numControls=%d\n", name.c_str(), g_numControls);
#endif // NDEBUG
}

#ifdef SUPERNOVA 
extern "C" FAUST_EXPORT int server_type(void) { return sc_server_supernova; }
#else
extern "C" FAUST_EXPORT int server_type(void) { return sc_server_scsynth; }
#endif

// EOF

#endif
