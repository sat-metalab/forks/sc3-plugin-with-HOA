/* ------------------------------------------------------------
author: "AmbisonicDecoderToolkit"
copyright: "(c) Aaron J. Heller 2013"
license: "BSD 3-Clause License"
name: "SATOswD2o1"
version: "1.2"
Code generated with Faust 2.5.12 (https://faust.grame.fr)
Compilation options: cpp, -scal -ftz 0
------------------------------------------------------------ */

#ifndef  __mydsp_H__
#define  __mydsp_H__

//-------------------------------------------------------------------
// FAUST architecture file for SuperCollider.
// Copyright (C) 2005-2012 Stefan Kersten.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//-------------------------------------------------------------------

// If other than 'faust2sc --prefix Faust' is used, sed this as well:
#if !defined(SC_FAUST_PREFIX)
#define SC_FAUST_PREFIX "Faust"
#endif

#include <map>
#include <string>
#include <string.h>
#include <SC_PlugIn.h>

/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __dsp__
#define __dsp__

#include <string>

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

class UI;
struct Meta;

/**
 * DSP memory manager.
 */

struct dsp_memory_manager {
    
    virtual ~dsp_memory_manager() {}
    
    virtual void* allocate(size_t size) = 0;
    virtual void destroy(void* ptr) = 0;
    
};

/**
* Signal processor definition.
*/

class dsp {

    public:

        dsp() {}
        virtual ~dsp() {}

        /* Return instance number of audio inputs */
        virtual int getNumInputs() = 0;
    
        /* Return instance number of audio outputs */
        virtual int getNumOutputs() = 0;
    
        /**
         * Trigger the ui_interface parameter with instance specific calls
         * to 'addBtton', 'addVerticalSlider'... in order to build the UI.
         *
         * @param ui_interface - the user interface builder
         */
        virtual void buildUserInterface(UI* ui_interface) = 0;
    
        /* Returns the sample rate currently used by the instance */
        virtual int getSampleRate() = 0;
    
        /**
         * Global init, calls the following methods:
         * - static class 'classInit': static tables initialization
         * - 'instanceInit': constants and instance state initialization
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void init(int samplingRate) = 0;

        /**
         * Init instance state
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void instanceInit(int samplingRate) = 0;

        /**
         * Init instance constant state
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void instanceConstants(int samplingRate) = 0;
    
        /* Init default control parameters values */
        virtual void instanceResetUserInterface() = 0;
    
        /* Init instance state (delay lines...) */
        virtual void instanceClear() = 0;
 
        /**
         * Return a clone of the instance.
         *
         * @return a copy of the instance on success, otherwise a null pointer.
         */
        virtual dsp* clone() = 0;
    
        /**
         * Trigger the Meta* parameter with instance specific calls to 'declare' (key, value) metadata.
         *
         * @param m - the Meta* meta user
         */
        virtual void metadata(Meta* m) = 0;
    
        /**
         * DSP instance computation, to be called with successive in/out audio buffers.
         *
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         *
         */
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) = 0;
    
        /**
         * DSP instance computation: alternative method to be used by subclasses.
         *
         * @param date_usec - the timestamp in microsec given by audio driver.
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         *
         */
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { compute(count, inputs, outputs); }
       
};

/**
 * Generic DSP decorator.
 */

class decorator_dsp : public dsp {

    protected:

        dsp* fDSP;

    public:

        decorator_dsp(dsp* dsp = 0):fDSP(dsp) {}
        virtual ~decorator_dsp() { delete fDSP; }

        virtual int getNumInputs() { return fDSP->getNumInputs(); }
        virtual int getNumOutputs() { return fDSP->getNumOutputs(); }
        virtual void buildUserInterface(UI* ui_interface) { fDSP->buildUserInterface(ui_interface); }
        virtual int getSampleRate() { return fDSP->getSampleRate(); }
        virtual void init(int samplingRate) { fDSP->init(samplingRate); }
        virtual void instanceInit(int samplingRate) { fDSP->instanceInit(samplingRate); }
        virtual void instanceConstants(int samplingRate) { fDSP->instanceConstants(samplingRate); }
        virtual void instanceResetUserInterface() { fDSP->instanceResetUserInterface(); }
        virtual void instanceClear() { fDSP->instanceClear(); }
        virtual decorator_dsp* clone() { return new decorator_dsp(fDSP->clone()); }
        virtual void metadata(Meta* m) { fDSP->metadata(m); }
        // Beware: subclasses usually have to overload the two 'compute' methods
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(count, inputs, outputs); }
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(date_usec, count, inputs, outputs); }
    
};

/**
 * DSP factory class.
 */

class dsp_factory {
    
    protected:
    
        // So that to force sub-classes to use deleteDSPFactory(dsp_factory* factory);
        virtual ~dsp_factory() {}
    
    public:
    
        virtual std::string getName() = 0;
        virtual std::string getSHAKey() = 0;
        virtual std::string getDSPCode() = 0;
    
        virtual dsp* createDSPInstance() = 0;
    
        virtual void setMemoryManager(dsp_memory_manager* manager) = 0;
        virtual dsp_memory_manager* getMemoryManager() = 0;
    
};

/**
 * On Intel set FZ (Flush to Zero) and DAZ (Denormals Are Zero)
 * flags to avoid costly denormals.
 */

#ifdef __SSE__
    #include <xmmintrin.h>
    #ifdef __SSE2__
        #define AVOIDDENORMALS _mm_setcsr(_mm_getcsr() | 0x8040)
    #else
        #define AVOIDDENORMALS _mm_setcsr(_mm_getcsr() | 0x8000)
    #endif
#else
    #define AVOIDDENORMALS
#endif

#endif
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __UI_H__
#define __UI_H__

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

/*******************************************************************************
 * UI : Faust DSP User Interface
 * User Interface as expected by the buildUserInterface() method of a DSP.
 * This abstract class contains only the method that the Faust compiler can
 * generate to describe a DSP user interface.
 ******************************************************************************/

struct Soundfile;

class UI
{

    public:

        UI() {}

        virtual ~UI() {}

        // -- widget's layouts

        virtual void openTabBox(const char* label) = 0;
        virtual void openHorizontalBox(const char* label) = 0;
        virtual void openVerticalBox(const char* label) = 0;
        virtual void closeBox() = 0;

        // -- active widgets

        virtual void addButton(const char* label, FAUSTFLOAT* zone) = 0;
        virtual void addCheckButton(const char* label, FAUSTFLOAT* zone) = 0;
        virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;
        virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;
        virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;

        // -- passive widgets

        virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) = 0;
        virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) = 0;
    
        // -- soundfiles
    
        virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) = 0;

        // -- metadata declarations

        virtual void declare(FAUSTFLOAT*, const char*, const char*) {}
};

#endif
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/
 
#ifndef __misc__
#define __misc__

#include <algorithm>
#include <map>
#include <string.h>
#include <stdlib.h>

/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __meta__
#define __meta__

struct Meta
{
    virtual void declare(const char* key, const char* value) = 0;
    virtual ~Meta() {};
};

#endif

using std::max;
using std::min;

struct XXXX_Meta : std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key]=value; }
};

struct MY_Meta : Meta, std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key]=value; }
};

inline int lsr(int x, int n)	{ return int(((unsigned int)x) >> n); }

inline int int2pow2(int x)		{ int r = 0; while ((1<<r) < x) r++; return r; }

inline long lopt(char* argv[], const char* name, long def)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return atoi(argv[i+1]);
	return def;
}

inline bool isopt(char* argv[], const char* name)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return true;
	return false;
}

inline const char* lopts(char* argv[], const char* name, const char* def)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return argv[i+1];
	return def;
}

#endif


using namespace std;

#if defined(__GNUC__) && __GNUC__ >= 4
    #define FAUST_EXPORT __attribute__((visibility("default")))
#else
    #define FAUST_EXPORT  SC_API_EXPORT
#endif

#ifdef WIN32
    #define STRDUP _strdup
#else
    #define STRDUP strdup
#endif

//----------------------------------------------------------------------------
// Vector intrinsics
//----------------------------------------------------------------------------


//----------------------------------------------------------------------------
// Metadata
//----------------------------------------------------------------------------

class MetaData : public Meta
               , public std::map<std::string, std::string>
{
public:
    void declare(const char* key, const char* value)
    {
        (*this)[key] = value;
    }
};

//----------------------------------------------------------------------------
// Control counter
//----------------------------------------------------------------------------

class ControlCounter : public UI
{
public:
    ControlCounter()
        : mNumControlInputs(0),
          mNumControlOutputs(0)
    {}

    size_t getNumControls() const { return getNumControlInputs(); }
    size_t getNumControlInputs() const { return mNumControlInputs; }
    size_t getNumControlOutputs() const { return mNumControlOutputs; }

    // Layout widgets
    virtual void openTabBox(const char* label) { }
    virtual void openHorizontalBox(const char* label) { }
    virtual void openVerticalBox(const char* label) { }
    virtual void closeBox() { }

    // Active widgets
    virtual void addButton(const char* label, FAUSTFLOAT* zone)
    { addControlInput(); }
    virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
    { addControlInput(); }
    virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }
    virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }
    virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }

    // Passive widgets
    virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
    { addControlOutput(); }
    virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
    { addControlOutput(); }
    
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}

protected:
    void addControlInput() { mNumControlInputs++; }
    void addControlOutput() { mNumControlOutputs++; }

private:
    size_t mNumControlInputs;
    size_t mNumControlOutputs;
};

//----------------------------------------------------------------------------
// UI control
//----------------------------------------------------------------------------

struct Control
{
    typedef void (*UpdateFunction)(Control* self, FAUSTFLOAT value);

    UpdateFunction updateFunction;
    FAUSTFLOAT* zone;
    FAUSTFLOAT min, max;

    inline void update(FAUSTFLOAT value)
    {
        (*updateFunction)(this, value);
    }

    static void simpleUpdate(Control* self, FAUSTFLOAT value)
    {
        *self->zone = value;
    }
    static void boundedUpdate(Control* self, FAUSTFLOAT value)
    {
        *self->zone = sc_clip(value, self->min, self->max);
    }
};

//----------------------------------------------------------------------------
// Control allocator
//----------------------------------------------------------------------------

class ControlAllocator : public UI
{
public:
    ControlAllocator(Control* controls)
        : mControls(controls)
    { }

    // Layout widgets
    virtual void openTabBox(const char* label) { }
    virtual void openHorizontalBox(const char* label) { }
    virtual void openVerticalBox(const char* label) { }
    virtual void closeBox() { }

    // Active widgets
    virtual void addButton(const char* label, FAUSTFLOAT* zone)
    { addSimpleControl(zone); }
    virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
    { addSimpleControl(zone); }
    virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }
    virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }
    virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }

    // Passive widgets
    virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
    virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}

private:
    void addControl(Control::UpdateFunction updateFunction, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT /* step */)
    {
        Control* ctrl        = mControls++;
        ctrl->updateFunction = updateFunction;
        ctrl->zone           = zone;
        ctrl->min            = min;
        ctrl->max            = max;
    }
    void addSimpleControl(FAUSTFLOAT* zone)
    {
        addControl(Control::simpleUpdate, zone, 0.f, 0.f, 0.f);
    }
    void addBoundedControl(FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    {
        addControl(Control::boundedUpdate, zone, min, max, step);
    }

private:
    Control* mControls;
};

//----------------------------------------------------------------------------
// FAUST generated code
//----------------------------------------------------------------------------

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif 

#include <cmath>
#include <math.h>

float mydsp_faustpower2_f(float value) {
	return (value * value);
	
}

#ifndef FAUSTCLASS 
#define FAUSTCLASS mydsp
#endif
#ifdef __APPLE__ 
#define exp10f __exp10f
#define exp10 __exp10
#endif

class mydsp : public dsp {
	
 private:
	
	FAUSTFLOAT fCheckbox0;
	FAUSTFLOAT fHslider0;
	float fRec0[2];
	int fSamplingFreq;
	int iConst0;
	float fConst1;
	float fConst2;
	float fConst3;
	float fConst4;
	FAUSTFLOAT fHslider1;
	float fRec4[2];
	float fRec5[3];
	FAUSTFLOAT fHslider2;
	float fRec6[2];
	float fRec7[3];
	float fRec8[3];
	float fConst5;
	float fRec3[2];
	float fRec1[2];
	float fRec9[3];
	int IOTA;
	float fVec0[1024];
	int iConst6;
	float fConst7;
	float fConst8;
	float fConst9;
	float fRec12[2];
	float fRec10[2];
	float fVec1[512];
	int iConst10;
	float fConst11;
	float fConst12;
	float fConst13;
	float fRec15[2];
	float fRec13[2];
	float fVec2[512];
	int iConst14;
	float fRec18[2];
	float fRec16[2];
	float fVec3[512];
	float fRec21[2];
	float fRec19[2];
	float fVec4[512];
	float fRec24[2];
	float fRec22[2];
	float fVec5[512];
	float fRec27[2];
	float fRec25[2];
	float fVec6[512];
	float fConst15;
	float fConst16;
	float fConst17;
	float fRec30[2];
	float fRec28[2];
	float fVec7[512];
	int iConst18;
	float fConst19;
	float fConst20;
	float fConst21;
	float fRec33[2];
	float fRec31[2];
	float fVec8[512];
	int iConst22;
	float fRec36[2];
	float fRec34[2];
	float fVec9[512];
	float fRec39[2];
	float fRec37[2];
	float fVec10[512];
	float fRec42[2];
	float fRec40[2];
	float fVec11[512];
	float fRec45[2];
	float fRec43[2];
	float fVec12[512];
	float fRec48[2];
	float fRec46[2];
	float fVec13[512];
	float fRec51[2];
	float fRec49[2];
	float fVec14[512];
	float fRec54[2];
	float fRec52[2];
	float fVec15[512];
	float fRec57[2];
	float fRec55[2];
	float fVec16[512];
	float fRec60[2];
	float fRec58[2];
	float fVec17[512];
	float fRec63[2];
	float fRec61[2];
	float fVec18[512];
	float fConst23;
	float fConst24;
	float fConst25;
	float fRec66[2];
	float fRec64[2];
	float fVec19[3];
	int iConst26;
	float fConst27;
	float fConst28;
	float fConst29;
	float fRec69[2];
	float fRec67[2];
	float fRec72[2];
	float fRec70[2];
	float fRec75[2];
	float fRec73[2];
	float fVec20[3];
	float fRec78[2];
	float fRec76[2];
	float fRec81[2];
	float fRec79[2];
	float fRec84[2];
	float fRec82[2];
	float fVec21[3];
	float fRec87[2];
	float fRec85[2];
	float fRec90[2];
	float fRec88[2];
	float fRec93[2];
	float fRec91[2];
	float fVec22[3];
	float fRec96[2];
	float fRec94[2];
	float fRec99[2];
	float fRec97[2];
	
 public:
	
	void metadata(Meta* m) { 
		m->declare("author", "AmbisonicDecoderToolkit");
		m->declare("copyright", "(c) Aaron J. Heller 2013");
		m->declare("license", "BSD 3-Clause License");
		m->declare("name", "SATOswD2o1");
		m->declare("version", "1.2");
	}

	virtual int getNumInputs() {
		return 4;
		
	}
	virtual int getNumOutputs() {
		return 31;
		
	}
	virtual int getInputRate(int channel) {
		int rate;
		switch (channel) {
			case 0: {
				rate = 1;
				break;
			}
			case 1: {
				rate = 1;
				break;
			}
			case 2: {
				rate = 1;
				break;
			}
			case 3: {
				rate = 1;
				break;
			}
			default: {
				rate = -1;
				break;
			}
			
		}
		return rate;
		
	}
	virtual int getOutputRate(int channel) {
		int rate;
		switch (channel) {
			case 0: {
				rate = 1;
				break;
			}
			case 1: {
				rate = 1;
				break;
			}
			case 2: {
				rate = 1;
				break;
			}
			case 3: {
				rate = 1;
				break;
			}
			case 4: {
				rate = 1;
				break;
			}
			case 5: {
				rate = 1;
				break;
			}
			case 6: {
				rate = 1;
				break;
			}
			case 7: {
				rate = 1;
				break;
			}
			case 8: {
				rate = 1;
				break;
			}
			case 9: {
				rate = 1;
				break;
			}
			case 10: {
				rate = 1;
				break;
			}
			case 11: {
				rate = 1;
				break;
			}
			case 12: {
				rate = 1;
				break;
			}
			case 13: {
				rate = 1;
				break;
			}
			case 14: {
				rate = 1;
				break;
			}
			case 15: {
				rate = 1;
				break;
			}
			case 16: {
				rate = 1;
				break;
			}
			case 17: {
				rate = 1;
				break;
			}
			case 18: {
				rate = 1;
				break;
			}
			case 19: {
				rate = 1;
				break;
			}
			case 20: {
				rate = 1;
				break;
			}
			case 21: {
				rate = 1;
				break;
			}
			case 22: {
				rate = 1;
				break;
			}
			case 23: {
				rate = 1;
				break;
			}
			case 24: {
				rate = 1;
				break;
			}
			case 25: {
				rate = 1;
				break;
			}
			case 26: {
				rate = 1;
				break;
			}
			case 27: {
				rate = 1;
				break;
			}
			case 28: {
				rate = 1;
				break;
			}
			case 29: {
				rate = 1;
				break;
			}
			case 30: {
				rate = 1;
				break;
			}
			default: {
				rate = -1;
				break;
			}
			
		}
		return rate;
		
	}
	
	static void classInit(int samplingFreq) {
		
	}
	
	virtual void instanceConstants(int samplingFreq) {
		fSamplingFreq = samplingFreq;
		iConst0 = min(192000, max(1, fSamplingFreq));
		fConst1 = float(iConst0);
		fConst2 = ((21.3840885f / fConst1) + 1.0f);
		fConst3 = (1.0f / fConst2);
		fConst4 = (3.14159274f / float(iConst0));
		fConst5 = (0.0f - (42.768177f / (fConst1 * fConst2)));
		iConst6 = int(((0.003076792f * float(iConst0)) + 0.5f));
		fConst7 = ((20.7908058f / fConst1) + 1.0f);
		fConst8 = (1.0f / fConst7);
		fConst9 = (0.0f - (41.5816116f / (fConst1 * fConst7)));
		iConst10 = int(((0.00240957108f * float(iConst0)) + 0.5f));
		fConst11 = ((20.795845f / fConst1) + 1.0f);
		fConst12 = (1.0f / fConst11);
		fConst13 = (0.0f - (41.5916901f / (fConst1 * fConst11)));
		iConst14 = int(((0.00241539837f * float(iConst0)) + 0.5f));
		fConst15 = ((20.0921803f / fConst1) + 1.0f);
		fConst16 = (1.0f / fConst15);
		fConst17 = (0.0f - (40.1843605f / (fConst1 * fConst15)));
		iConst18 = int(((0.00157335959f * float(iConst0)) + 0.5f));
		fConst19 = ((20.0804253f / fConst1) + 1.0f);
		fConst20 = (1.0f / fConst19);
		fConst21 = (0.0f - (40.1608505f / (fConst1 * fConst19)));
		iConst22 = int(((0.00155879138f * float(iConst0)) + 0.5f));
		fConst23 = ((18.9015656f / fConst1) + 1.0f);
		fConst24 = (1.0f / fConst23);
		fConst25 = (0.0f - (37.8031311f / (fConst1 * fConst23)));
		iConst26 = int(((5.82725761e-06f * float(iConst0)) + 0.5f));
		fConst27 = ((18.8974018f / fConst1) + 1.0f);
		fConst28 = (1.0f / fConst27);
		fConst29 = (0.0f - (37.7948036f / (fConst1 * fConst27)));
		
	}
	
	virtual void instanceResetUserInterface() {
		fCheckbox0 = FAUSTFLOAT(0.0f);
		fHslider0 = FAUSTFLOAT(-10.0f);
		fHslider1 = FAUSTFLOAT(400.0f);
		fHslider2 = FAUSTFLOAT(0.0f);
		
	}
	
	virtual void instanceClear() {
		for (int l0 = 0; (l0 < 2); l0 = (l0 + 1)) {
			fRec0[l0] = 0.0f;
			
		}
		for (int l1 = 0; (l1 < 2); l1 = (l1 + 1)) {
			fRec4[l1] = 0.0f;
			
		}
		for (int l2 = 0; (l2 < 3); l2 = (l2 + 1)) {
			fRec5[l2] = 0.0f;
			
		}
		for (int l3 = 0; (l3 < 2); l3 = (l3 + 1)) {
			fRec6[l3] = 0.0f;
			
		}
		for (int l4 = 0; (l4 < 3); l4 = (l4 + 1)) {
			fRec7[l4] = 0.0f;
			
		}
		for (int l5 = 0; (l5 < 3); l5 = (l5 + 1)) {
			fRec8[l5] = 0.0f;
			
		}
		for (int l6 = 0; (l6 < 2); l6 = (l6 + 1)) {
			fRec3[l6] = 0.0f;
			
		}
		for (int l7 = 0; (l7 < 2); l7 = (l7 + 1)) {
			fRec1[l7] = 0.0f;
			
		}
		for (int l8 = 0; (l8 < 3); l8 = (l8 + 1)) {
			fRec9[l8] = 0.0f;
			
		}
		IOTA = 0;
		for (int l9 = 0; (l9 < 1024); l9 = (l9 + 1)) {
			fVec0[l9] = 0.0f;
			
		}
		for (int l10 = 0; (l10 < 2); l10 = (l10 + 1)) {
			fRec12[l10] = 0.0f;
			
		}
		for (int l11 = 0; (l11 < 2); l11 = (l11 + 1)) {
			fRec10[l11] = 0.0f;
			
		}
		for (int l12 = 0; (l12 < 512); l12 = (l12 + 1)) {
			fVec1[l12] = 0.0f;
			
		}
		for (int l13 = 0; (l13 < 2); l13 = (l13 + 1)) {
			fRec15[l13] = 0.0f;
			
		}
		for (int l14 = 0; (l14 < 2); l14 = (l14 + 1)) {
			fRec13[l14] = 0.0f;
			
		}
		for (int l15 = 0; (l15 < 512); l15 = (l15 + 1)) {
			fVec2[l15] = 0.0f;
			
		}
		for (int l16 = 0; (l16 < 2); l16 = (l16 + 1)) {
			fRec18[l16] = 0.0f;
			
		}
		for (int l17 = 0; (l17 < 2); l17 = (l17 + 1)) {
			fRec16[l17] = 0.0f;
			
		}
		for (int l18 = 0; (l18 < 512); l18 = (l18 + 1)) {
			fVec3[l18] = 0.0f;
			
		}
		for (int l19 = 0; (l19 < 2); l19 = (l19 + 1)) {
			fRec21[l19] = 0.0f;
			
		}
		for (int l20 = 0; (l20 < 2); l20 = (l20 + 1)) {
			fRec19[l20] = 0.0f;
			
		}
		for (int l21 = 0; (l21 < 512); l21 = (l21 + 1)) {
			fVec4[l21] = 0.0f;
			
		}
		for (int l22 = 0; (l22 < 2); l22 = (l22 + 1)) {
			fRec24[l22] = 0.0f;
			
		}
		for (int l23 = 0; (l23 < 2); l23 = (l23 + 1)) {
			fRec22[l23] = 0.0f;
			
		}
		for (int l24 = 0; (l24 < 512); l24 = (l24 + 1)) {
			fVec5[l24] = 0.0f;
			
		}
		for (int l25 = 0; (l25 < 2); l25 = (l25 + 1)) {
			fRec27[l25] = 0.0f;
			
		}
		for (int l26 = 0; (l26 < 2); l26 = (l26 + 1)) {
			fRec25[l26] = 0.0f;
			
		}
		for (int l27 = 0; (l27 < 512); l27 = (l27 + 1)) {
			fVec6[l27] = 0.0f;
			
		}
		for (int l28 = 0; (l28 < 2); l28 = (l28 + 1)) {
			fRec30[l28] = 0.0f;
			
		}
		for (int l29 = 0; (l29 < 2); l29 = (l29 + 1)) {
			fRec28[l29] = 0.0f;
			
		}
		for (int l30 = 0; (l30 < 512); l30 = (l30 + 1)) {
			fVec7[l30] = 0.0f;
			
		}
		for (int l31 = 0; (l31 < 2); l31 = (l31 + 1)) {
			fRec33[l31] = 0.0f;
			
		}
		for (int l32 = 0; (l32 < 2); l32 = (l32 + 1)) {
			fRec31[l32] = 0.0f;
			
		}
		for (int l33 = 0; (l33 < 512); l33 = (l33 + 1)) {
			fVec8[l33] = 0.0f;
			
		}
		for (int l34 = 0; (l34 < 2); l34 = (l34 + 1)) {
			fRec36[l34] = 0.0f;
			
		}
		for (int l35 = 0; (l35 < 2); l35 = (l35 + 1)) {
			fRec34[l35] = 0.0f;
			
		}
		for (int l36 = 0; (l36 < 512); l36 = (l36 + 1)) {
			fVec9[l36] = 0.0f;
			
		}
		for (int l37 = 0; (l37 < 2); l37 = (l37 + 1)) {
			fRec39[l37] = 0.0f;
			
		}
		for (int l38 = 0; (l38 < 2); l38 = (l38 + 1)) {
			fRec37[l38] = 0.0f;
			
		}
		for (int l39 = 0; (l39 < 512); l39 = (l39 + 1)) {
			fVec10[l39] = 0.0f;
			
		}
		for (int l40 = 0; (l40 < 2); l40 = (l40 + 1)) {
			fRec42[l40] = 0.0f;
			
		}
		for (int l41 = 0; (l41 < 2); l41 = (l41 + 1)) {
			fRec40[l41] = 0.0f;
			
		}
		for (int l42 = 0; (l42 < 512); l42 = (l42 + 1)) {
			fVec11[l42] = 0.0f;
			
		}
		for (int l43 = 0; (l43 < 2); l43 = (l43 + 1)) {
			fRec45[l43] = 0.0f;
			
		}
		for (int l44 = 0; (l44 < 2); l44 = (l44 + 1)) {
			fRec43[l44] = 0.0f;
			
		}
		for (int l45 = 0; (l45 < 512); l45 = (l45 + 1)) {
			fVec12[l45] = 0.0f;
			
		}
		for (int l46 = 0; (l46 < 2); l46 = (l46 + 1)) {
			fRec48[l46] = 0.0f;
			
		}
		for (int l47 = 0; (l47 < 2); l47 = (l47 + 1)) {
			fRec46[l47] = 0.0f;
			
		}
		for (int l48 = 0; (l48 < 512); l48 = (l48 + 1)) {
			fVec13[l48] = 0.0f;
			
		}
		for (int l49 = 0; (l49 < 2); l49 = (l49 + 1)) {
			fRec51[l49] = 0.0f;
			
		}
		for (int l50 = 0; (l50 < 2); l50 = (l50 + 1)) {
			fRec49[l50] = 0.0f;
			
		}
		for (int l51 = 0; (l51 < 512); l51 = (l51 + 1)) {
			fVec14[l51] = 0.0f;
			
		}
		for (int l52 = 0; (l52 < 2); l52 = (l52 + 1)) {
			fRec54[l52] = 0.0f;
			
		}
		for (int l53 = 0; (l53 < 2); l53 = (l53 + 1)) {
			fRec52[l53] = 0.0f;
			
		}
		for (int l54 = 0; (l54 < 512); l54 = (l54 + 1)) {
			fVec15[l54] = 0.0f;
			
		}
		for (int l55 = 0; (l55 < 2); l55 = (l55 + 1)) {
			fRec57[l55] = 0.0f;
			
		}
		for (int l56 = 0; (l56 < 2); l56 = (l56 + 1)) {
			fRec55[l56] = 0.0f;
			
		}
		for (int l57 = 0; (l57 < 512); l57 = (l57 + 1)) {
			fVec16[l57] = 0.0f;
			
		}
		for (int l58 = 0; (l58 < 2); l58 = (l58 + 1)) {
			fRec60[l58] = 0.0f;
			
		}
		for (int l59 = 0; (l59 < 2); l59 = (l59 + 1)) {
			fRec58[l59] = 0.0f;
			
		}
		for (int l60 = 0; (l60 < 512); l60 = (l60 + 1)) {
			fVec17[l60] = 0.0f;
			
		}
		for (int l61 = 0; (l61 < 2); l61 = (l61 + 1)) {
			fRec63[l61] = 0.0f;
			
		}
		for (int l62 = 0; (l62 < 2); l62 = (l62 + 1)) {
			fRec61[l62] = 0.0f;
			
		}
		for (int l63 = 0; (l63 < 512); l63 = (l63 + 1)) {
			fVec18[l63] = 0.0f;
			
		}
		for (int l64 = 0; (l64 < 2); l64 = (l64 + 1)) {
			fRec66[l64] = 0.0f;
			
		}
		for (int l65 = 0; (l65 < 2); l65 = (l65 + 1)) {
			fRec64[l65] = 0.0f;
			
		}
		for (int l66 = 0; (l66 < 3); l66 = (l66 + 1)) {
			fVec19[l66] = 0.0f;
			
		}
		for (int l67 = 0; (l67 < 2); l67 = (l67 + 1)) {
			fRec69[l67] = 0.0f;
			
		}
		for (int l68 = 0; (l68 < 2); l68 = (l68 + 1)) {
			fRec67[l68] = 0.0f;
			
		}
		for (int l69 = 0; (l69 < 2); l69 = (l69 + 1)) {
			fRec72[l69] = 0.0f;
			
		}
		for (int l70 = 0; (l70 < 2); l70 = (l70 + 1)) {
			fRec70[l70] = 0.0f;
			
		}
		for (int l71 = 0; (l71 < 2); l71 = (l71 + 1)) {
			fRec75[l71] = 0.0f;
			
		}
		for (int l72 = 0; (l72 < 2); l72 = (l72 + 1)) {
			fRec73[l72] = 0.0f;
			
		}
		for (int l73 = 0; (l73 < 3); l73 = (l73 + 1)) {
			fVec20[l73] = 0.0f;
			
		}
		for (int l74 = 0; (l74 < 2); l74 = (l74 + 1)) {
			fRec78[l74] = 0.0f;
			
		}
		for (int l75 = 0; (l75 < 2); l75 = (l75 + 1)) {
			fRec76[l75] = 0.0f;
			
		}
		for (int l76 = 0; (l76 < 2); l76 = (l76 + 1)) {
			fRec81[l76] = 0.0f;
			
		}
		for (int l77 = 0; (l77 < 2); l77 = (l77 + 1)) {
			fRec79[l77] = 0.0f;
			
		}
		for (int l78 = 0; (l78 < 2); l78 = (l78 + 1)) {
			fRec84[l78] = 0.0f;
			
		}
		for (int l79 = 0; (l79 < 2); l79 = (l79 + 1)) {
			fRec82[l79] = 0.0f;
			
		}
		for (int l80 = 0; (l80 < 3); l80 = (l80 + 1)) {
			fVec21[l80] = 0.0f;
			
		}
		for (int l81 = 0; (l81 < 2); l81 = (l81 + 1)) {
			fRec87[l81] = 0.0f;
			
		}
		for (int l82 = 0; (l82 < 2); l82 = (l82 + 1)) {
			fRec85[l82] = 0.0f;
			
		}
		for (int l83 = 0; (l83 < 2); l83 = (l83 + 1)) {
			fRec90[l83] = 0.0f;
			
		}
		for (int l84 = 0; (l84 < 2); l84 = (l84 + 1)) {
			fRec88[l84] = 0.0f;
			
		}
		for (int l85 = 0; (l85 < 2); l85 = (l85 + 1)) {
			fRec93[l85] = 0.0f;
			
		}
		for (int l86 = 0; (l86 < 2); l86 = (l86 + 1)) {
			fRec91[l86] = 0.0f;
			
		}
		for (int l87 = 0; (l87 < 3); l87 = (l87 + 1)) {
			fVec22[l87] = 0.0f;
			
		}
		for (int l88 = 0; (l88 < 2); l88 = (l88 + 1)) {
			fRec96[l88] = 0.0f;
			
		}
		for (int l89 = 0; (l89 < 2); l89 = (l89 + 1)) {
			fRec94[l89] = 0.0f;
			
		}
		for (int l90 = 0; (l90 < 2); l90 = (l90 + 1)) {
			fRec99[l90] = 0.0f;
			
		}
		for (int l91 = 0; (l91 < 2); l91 = (l91 + 1)) {
			fRec97[l91] = 0.0f;
			
		}
		
	}
	
	virtual void init(int samplingFreq) {
		classInit(samplingFreq);
		instanceInit(samplingFreq);
	}
	virtual void instanceInit(int samplingFreq) {
		instanceConstants(samplingFreq);
		instanceResetUserInterface();
		instanceClear();
	}
	
	virtual mydsp* clone() {
		return new mydsp();
	}
	virtual int getSampleRate() {
		return fSamplingFreq;
		
	}
	
	virtual void buildUserInterface(UI* ui_interface) {
		ui_interface->openVerticalBox("SATOswD2o1");
		ui_interface->declare(&fHslider0, "unit", "dB");
		ui_interface->addHorizontalSlider("gain", &fHslider0, -10.0f, -30.0f, 10.0f, 1.0f);
		ui_interface->declare(&fHslider2, "unit", "dB");
		ui_interface->addHorizontalSlider("lf/hf", &fHslider2, 0.0f, -3.0f, 3.0f, 0.100000001f);
		ui_interface->addCheckButton("mute", &fCheckbox0);
		ui_interface->declare(&fHslider1, "unit", "Hz");
		ui_interface->addHorizontalSlider("xover", &fHslider1, 400.0f, 200.0f, 800.0f, 20.0f);
		ui_interface->closeBox();
		
	}
	
	virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) {
		FAUSTFLOAT* input0 = inputs[0];
		FAUSTFLOAT* input1 = inputs[1];
		FAUSTFLOAT* input2 = inputs[2];
		FAUSTFLOAT* input3 = inputs[3];
		FAUSTFLOAT* output0 = outputs[0];
		FAUSTFLOAT* output1 = outputs[1];
		FAUSTFLOAT* output2 = outputs[2];
		FAUSTFLOAT* output3 = outputs[3];
		FAUSTFLOAT* output4 = outputs[4];
		FAUSTFLOAT* output5 = outputs[5];
		FAUSTFLOAT* output6 = outputs[6];
		FAUSTFLOAT* output7 = outputs[7];
		FAUSTFLOAT* output8 = outputs[8];
		FAUSTFLOAT* output9 = outputs[9];
		FAUSTFLOAT* output10 = outputs[10];
		FAUSTFLOAT* output11 = outputs[11];
		FAUSTFLOAT* output12 = outputs[12];
		FAUSTFLOAT* output13 = outputs[13];
		FAUSTFLOAT* output14 = outputs[14];
		FAUSTFLOAT* output15 = outputs[15];
		FAUSTFLOAT* output16 = outputs[16];
		FAUSTFLOAT* output17 = outputs[17];
		FAUSTFLOAT* output18 = outputs[18];
		FAUSTFLOAT* output19 = outputs[19];
		FAUSTFLOAT* output20 = outputs[20];
		FAUSTFLOAT* output21 = outputs[21];
		FAUSTFLOAT* output22 = outputs[22];
		FAUSTFLOAT* output23 = outputs[23];
		FAUSTFLOAT* output24 = outputs[24];
		FAUSTFLOAT* output25 = outputs[25];
		FAUSTFLOAT* output26 = outputs[26];
		FAUSTFLOAT* output27 = outputs[27];
		FAUSTFLOAT* output28 = outputs[28];
		FAUSTFLOAT* output29 = outputs[29];
		FAUSTFLOAT* output30 = outputs[30];
		float fSlow0 = (0.00100000005f * (float((float(fCheckbox0) < 0.5f)) * powf(10.0f, (0.0500000007f * float(fHslider0)))));
		float fSlow1 = (0.00100000005f * float(fHslider1));
		float fSlow2 = (0.00100000005f * powf(10.0f, (0.0500000007f * float(fHslider2))));
		for (int i = 0; (i < count); i = (i + 1)) {
			fRec0[0] = (fSlow0 + (0.999000013f * fRec0[1]));
			fRec4[0] = (fSlow1 + (0.999000013f * fRec4[1]));
			float fTemp0 = tanf((fConst4 * fRec4[0]));
			float fTemp1 = mydsp_faustpower2_f(fTemp0);
			float fTemp2 = ((fTemp0 * (fTemp0 + -2.0f)) + 1.0f);
			float fTemp3 = (fTemp1 + -1.0f);
			float fTemp4 = ((fTemp0 * (fTemp0 + 2.0f)) + 1.0f);
			fRec5[0] = (float(input2[i]) - (((fTemp2 * fRec5[2]) + (2.0f * (fTemp3 * fRec5[1]))) / fTemp4));
			fRec6[0] = (fSlow2 + (0.999000013f * fRec6[1]));
			float fTemp5 = (fRec6[0] * fTemp4);
			float fTemp6 = (0.0f - (2.0f / fTemp4));
			float fTemp7 = (((fTemp1 * (fRec5[2] + (fRec5[0] + (2.0f * fRec5[1])))) / fTemp5) + (0.577350259f * (fRec6[0] * (0.0f - ((fTemp6 * fRec5[1]) + ((fRec5[0] + fRec5[2]) / fTemp4))))));
			fRec7[0] = (float(input3[i]) - (((fTemp2 * fRec7[2]) + (2.0f * (fTemp3 * fRec7[1]))) / fTemp4));
			float fTemp8 = (((fTemp1 * (fRec7[2] + (fRec7[0] + (2.0f * fRec7[1])))) / fTemp5) + (0.577350259f * (fRec6[0] * (0.0f - ((fTemp6 * fRec7[1]) + ((fRec7[0] + fRec7[2]) / fTemp4))))));
			fRec8[0] = (float(input1[i]) - (((fTemp2 * fRec8[2]) + (2.0f * (fTemp3 * fRec8[1]))) / fTemp4));
			float fTemp9 = (((fTemp1 * (fRec8[2] + (fRec8[0] + (2.0f * fRec8[1])))) / fTemp5) + (0.577350259f * (fRec6[0] * (0.0f - ((fTemp6 * fRec8[1]) + ((fRec8[0] + fRec8[2]) / fTemp4))))));
			float fTemp10 = (fConst3 * (((0.0941376761f * fTemp7) + (2.42800013e-07f * fTemp8)) - (4.15470004e-06f * fTemp9)));
			float fTemp11 = (fConst5 * fRec1[1]);
			fRec3[0] = (fTemp10 + (fRec3[1] + fTemp11));
			fRec1[0] = fRec3[0];
			float fRec2 = (fTemp11 + fTemp10);
			fRec9[0] = (float(input0[i]) - (((fRec9[2] * fTemp2) + (2.0f * (fRec9[1] * fTemp3))) / fTemp4));
			float fTemp12 = (((fTemp1 * (fRec9[2] + (fRec9[0] + (2.0f * fRec9[1])))) / fTemp5) + (fRec6[0] * (0.0f - ((fRec9[1] * fTemp6) + ((fRec9[0] + fRec9[2]) / fTemp4)))));
			fVec0[(IOTA & 1023)] = (fRec2 + (0.0588310584f * fTemp12));
			output0[i] = FAUSTFLOAT((0.883713245f * (fRec0[0] * fVec0[((IOTA - iConst6) & 1023)])));
			float fTemp13 = (fConst8 * (((0.0389746763f * fTemp9) + (0.0524411313f * fTemp7)) + (0.0137809403f * fTemp8)));
			float fTemp14 = (fConst9 * fRec10[1]);
			fRec12[0] = (fTemp13 + (fRec12[1] + fTemp14));
			fRec10[0] = fRec12[0];
			float fRec11 = (fTemp14 + fTemp13);
			fVec1[(IOTA & 511)] = (fRec11 + (0.0407811999f * fTemp12));
			output1[i] = FAUSTFLOAT((0.908930719f * (fRec0[0] * fVec1[((IOTA - iConst10) & 511)])));
			float fTemp15 = (fConst12 * (((0.0329679698f * fTemp9) + (0.0571265034f * fTemp7)) - (0.0329778865f * fTemp8)));
			float fTemp16 = (fConst13 * fRec13[1]);
			fRec15[0] = (fTemp15 + (fRec15[1] + fTemp16));
			fRec13[0] = fRec15[0];
			float fRec14 = (fTemp16 + fTemp15);
			fVec2[(IOTA & 511)] = (fRec14 + (0.0453824513f * fTemp12));
			output2[i] = FAUSTFLOAT((0.90871048f * (fRec0[0] * fVec2[((IOTA - iConst14) & 511)])));
			float fTemp17 = (fConst8 * ((0.0524600223f * fTemp7) - ((0.0137928454f * fTemp9) + (0.0389987528f * fTemp8))));
			float fTemp18 = (fConst9 * fRec16[1]);
			fRec18[0] = (fTemp17 + (fRec18[1] + fTemp18));
			fRec16[0] = fRec18[0];
			float fRec17 = (fTemp18 + fTemp17);
			fVec3[(IOTA & 511)] = (fRec17 + (0.0408032164f * fTemp12));
			output3[i] = FAUSTFLOAT((0.908930719f * (fRec0[0] * fVec3[((IOTA - iConst10) & 511)])));
			float fTemp19 = (fConst8 * ((0.0477688611f * fTemp7) - ((0.0345717557f * fTemp9) + (0.00927145034f * fTemp8))));
			float fTemp20 = (fConst9 * fRec19[1]);
			fRec21[0] = (fTemp19 + (fRec21[1] + fTemp20));
			fRec19[0] = fRec21[0];
			float fRec20 = (fTemp20 + fTemp19);
			fVec4[(IOTA & 511)] = (fRec20 + (0.0361875407f * fTemp12));
			output4[i] = FAUSTFLOAT((0.908930719f * (fRec0[0] * fVec4[((IOTA - iConst10) & 511)])));
			float fTemp21 = (fConst12 * (((0.0571384393f * fTemp7) + (0.032984715f * fTemp8)) - (0.0329728834f * fTemp9)));
			float fTemp22 = (fConst13 * fRec22[1]);
			fRec24[0] = (fTemp21 + (fRec24[1] + fTemp22));
			fRec22[0] = fRec24[0];
			float fRec23 = (fTemp22 + fTemp21);
			fVec5[(IOTA & 511)] = (fRec23 + (0.04539131f * fTemp12));
			output5[i] = FAUSTFLOAT((0.90871048f * (fRec0[0] * fVec5[((IOTA - iConst14) & 511)])));
			float fTemp23 = (fConst8 * (((0.00926380046f * fTemp9) + (0.0477699935f * fTemp7)) + (0.0345857032f * fTemp8)));
			float fTemp24 = (fConst9 * fRec25[1]);
			fRec27[0] = (fTemp23 + (fRec27[1] + fTemp24));
			fRec25[0] = fRec27[0];
			float fRec26 = (fTemp24 + fTemp23);
			fVec6[(IOTA & 511)] = (fRec26 + (0.0361924917f * fTemp12));
			output6[i] = FAUSTFLOAT((0.908930719f * (fRec0[0] * fVec6[((IOTA - iConst10) & 511)])));
			float fTemp25 = (fConst16 * (((0.0394506529f * fTemp9) + (0.0185638908f * fTemp7)) - (0.00184053963f * fTemp8)));
			float fTemp26 = (fConst17 * fRec28[1]);
			fRec30[0] = (fTemp25 + (fRec30[1] + fTemp26));
			fRec28[0] = fRec30[0];
			float fRec29 = (fTemp26 + fTemp25);
			fVec7[(IOTA & 511)] = (fRec29 + (0.0261618514f * fTemp12));
			output7[i] = FAUSTFLOAT((0.940535188f * (fRec0[0] * fVec7[((IOTA - iConst18) & 511)])));
			float fTemp27 = ((fConst20 * (((0.0137387449f * fTemp7) + (0.0445195064f * fTemp9)) - (0.0263461135f * fTemp8))) + (fConst21 * fRec31[1]));
			fRec33[0] = (fTemp27 + fRec33[1]);
			fRec31[0] = fRec33[0];
			float fRec32 = fTemp27;
			fVec8[(IOTA & 511)] = (fRec32 + (0.0323272236f * fTemp12));
			output8[i] = FAUSTFLOAT((0.941085756f * (fRec0[0] * fVec8[((IOTA - iConst22) & 511)])));
			float fTemp28 = ((fConst20 * (((0.0186304655f * fTemp9) + (0.0127383061f * fTemp7)) - (0.0369097739f * fTemp8))) + (fConst21 * fRec34[1]));
			fRec36[0] = (fTemp28 + fRec36[1]);
			fRec34[0] = fRec36[0];
			float fRec35 = fTemp28;
			fVec9[(IOTA & 511)] = (fRec35 + (0.0259500761f * fTemp12));
			output9[i] = FAUSTFLOAT((0.941085756f * (fRec0[0] * fVec9[((IOTA - iConst22) & 511)])));
			float fTemp29 = (fConst16 * (((0.00183700956f * fTemp9) + (0.0185472425f * fTemp7)) - (0.0394348688f * fTemp8)));
			float fTemp30 = (fConst17 * fRec37[1]);
			fRec39[0] = (fTemp29 + (fRec39[1] + fTemp30));
			fRec37[0] = fRec39[0];
			float fRec38 = (fTemp30 + fTemp29);
			fVec10[(IOTA & 511)] = (fRec38 + (0.0261483416f * fTemp12));
			output10[i] = FAUSTFLOAT((0.940535188f * (fRec0[0] * fVec10[((IOTA - iConst18) & 511)])));
			float fTemp31 = (fConst20 * ((0.0127308108f * fTemp7) - ((0.0176555794f * fTemp9) + (0.0374553464f * fTemp8))));
			float fTemp32 = (fConst21 * fRec40[1]);
			fRec42[0] = (fTemp31 + (fRec42[1] + fTemp32));
			fRec40[0] = fRec42[0];
			float fRec41 = (fTemp32 + fTemp31);
			fVec11[(IOTA & 511)] = (fRec41 + (0.0259405281f * fTemp12));
			output11[i] = FAUSTFLOAT((0.941085756f * (fRec0[0] * fVec11[((IOTA - iConst22) & 511)])));
			float fTemp33 = (fConst20 * ((0.0205770936f * fTemp7) - ((0.0513798967f * fTemp9) + (0.0317948349f * fTemp8))));
			float fTemp34 = (fConst21 * fRec43[1]);
			fRec45[0] = (fTemp33 + (fRec45[1] + fTemp34));
			fRec43[0] = fRec45[0];
			float fRec44 = (fTemp34 + fTemp33);
			fVec12[(IOTA & 511)] = (fRec44 + (0.0389448367f * fTemp12));
			output12[i] = FAUSTFLOAT((0.941085756f * (fRec0[0] * fVec12[((IOTA - iConst22) & 511)])));
			float fTemp35 = (fConst16 * (((0.0185660571f * fTemp7) + (0.00184788718f * fTemp8)) - (0.0394531265f * fTemp9)));
			float fTemp36 = (fConst17 * fRec46[1]);
			fRec48[0] = (fTemp35 + (fRec48[1] + fTemp36));
			fRec46[0] = fRec48[0];
			float fRec47 = (fTemp36 + fTemp35);
			fVec13[(IOTA & 511)] = (fRec47 + (0.0261641443f * fTemp12));
			output13[i] = FAUSTFLOAT((0.940535188f * (fRec0[0] * fVec13[((IOTA - iConst18) & 511)])));
			float fTemp37 = (fConst20 * (((0.0127271321f * fTemp7) + (0.0186162814f * fTemp8)) - (0.0369095467f * fTemp9)));
			float fTemp38 = (fConst21 * fRec49[1]);
			fRec51[0] = (fTemp37 + (fRec51[1] + fTemp38));
			fRec49[0] = fRec51[0];
			float fRec50 = (fTemp38 + fTemp37);
			fVec14[(IOTA & 511)] = (fRec50 + (0.0259447377f * fTemp12));
			output14[i] = FAUSTFLOAT((0.941085756f * (fRec0[0] * fVec14[((IOTA - iConst22) & 511)])));
			float fTemp39 = (fConst20 * (((0.0137484306f * fTemp7) + (0.0445271879f * fTemp8)) - (0.0263485294f * fTemp9)));
			float fTemp40 = (fConst21 * fRec52[1]);
			fRec54[0] = (fTemp39 + (fRec54[1] + fTemp40));
			fRec52[0] = fRec54[0];
			float fRec53 = (fTemp40 + fTemp39);
			fVec15[(IOTA & 511)] = (fRec53 + (0.0323338211f * fTemp12));
			output15[i] = FAUSTFLOAT((0.941085756f * (fRec0[0] * fVec15[((IOTA - iConst22) & 511)])));
			float fTemp41 = (fConst16 * (((0.0185498931f * fTemp7) + (0.0394535773f * fTemp8)) - (0.00184174115f * fTemp9)));
			float fTemp42 = (fConst17 * fRec55[1]);
			fRec57[0] = (fTemp41 + (fRec57[1] + fTemp42));
			fRec55[0] = fRec57[0];
			float fRec56 = (fTemp42 + fTemp41);
			fVec16[(IOTA & 511)] = (fRec56 + (0.0261601638f * fTemp12));
			output16[i] = FAUSTFLOAT((0.940535188f * (fRec0[0] * fVec16[((IOTA - iConst18) & 511)])));
			float fTemp43 = (fConst20 * (((0.0318024792f * fTemp9) + (0.0205848552f * fTemp7)) + (0.0513721406f * fTemp8)));
			float fTemp44 = (fConst21 * fRec58[1]);
			fRec60[0] = (fTemp43 + (fRec60[1] + fTemp44));
			fRec58[0] = fRec60[0];
			float fRec59 = (fTemp44 + fTemp43);
			fVec17[(IOTA & 511)] = (fRec59 + (0.038944684f * fTemp12));
			output17[i] = FAUSTFLOAT((0.941085756f * (fRec0[0] * fVec17[((IOTA - iConst22) & 511)])));
			float fTemp45 = (fConst20 * (((0.0374534018f * fTemp9) + (0.0127278063f * fTemp7)) + (0.0176651441f * fTemp8)));
			float fTemp46 = (fConst21 * fRec61[1]);
			fRec63[0] = (fTemp45 + (fRec63[1] + fTemp46));
			fRec61[0] = fRec63[0];
			float fRec62 = (fTemp46 + fTemp45);
			fVec18[(IOTA & 511)] = (fRec62 + (0.0259419568f * fTemp12));
			output18[i] = FAUSTFLOAT((0.941085756f * (fRec0[0] * fVec18[((IOTA - iConst22) & 511)])));
			float fTemp47 = (fConst24 * ((0.0827766433f * fTemp9) - ((0.0273921341f * fTemp7) + (7.02950001e-06f * fTemp8))));
			float fTemp48 = (fConst25 * fRec64[1]);
			fRec66[0] = (fTemp47 + (fRec66[1] + fTemp48));
			fRec64[0] = fRec66[0];
			float fRec65 = (fTemp48 + fTemp47);
			fVec19[0] = (fRec65 + (0.055423025f * fTemp12));
			output19[i] = FAUSTFLOAT((0.999779761f * (fRec0[0] * fVec19[iConst26])));
			float fTemp49 = (fConst28 * ((0.0546924546f * fTemp9) - ((0.0296233296f * fTemp7) + (0.0315752216f * fTemp8))));
			float fTemp50 = (fConst29 * fRec67[1]);
			fRec69[0] = (fTemp49 + (fRec69[1] + fTemp50));
			fRec67[0] = fRec69[0];
			float fRec68 = (fTemp50 + fTemp49);
			output20[i] = FAUSTFLOAT((fRec0[0] * (fRec68 + (0.0434079953f * fTemp12))));
			float fTemp51 = (fConst28 * ((0.0388471968f * fTemp9) - ((0.0285362061f * fTemp7) + (0.0618594699f * fTemp8))));
			float fTemp52 = (fConst29 * fRec70[1]);
			fRec72[0] = (fTemp51 + (fRec72[1] + fTemp52));
			fRec70[0] = fRec72[0];
			float fRec71 = (fTemp52 + fTemp51);
			output21[i] = FAUSTFLOAT((fRec0[0] * (fRec71 + (0.0494420342f * fTemp12))));
			float fTemp53 = (fConst24 * ((2.4369001e-06f * fTemp9) - ((0.0273813531f * fTemp7) + (0.082773298f * fTemp8))));
			float fTemp54 = (fConst25 * fRec73[1]);
			fRec75[0] = (fTemp53 + (fRec75[1] + fTemp54));
			fRec73[0] = fRec75[0];
			float fRec74 = (fTemp54 + fTemp53);
			fVec20[0] = (fRec74 + (0.0554167405f * fTemp12));
			output22[i] = FAUSTFLOAT((0.999779761f * (fRec0[0] * fVec20[iConst26])));
			float fTemp55 = (fConst28 * (0.0f - (((0.0388576426f * fTemp9) + (0.0285068844f * fTemp7)) + (0.0618772283f * fTemp8))));
			float fTemp56 = (fConst29 * fRec76[1]);
			fRec78[0] = (fTemp55 + (fRec78[1] + fTemp56));
			fRec76[0] = fRec78[0];
			float fRec77 = (fTemp56 + fTemp55);
			output23[i] = FAUSTFLOAT((fRec0[0] * (fRec77 + (0.0494453609f * fTemp12))));
			float fTemp57 = (fConst28 * (0.0f - (((0.0546927527f * fTemp9) + (0.0296443459f * fTemp7)) + (0.0315747596f * fTemp8))));
			float fTemp58 = (fConst29 * fRec79[1]);
			fRec81[0] = (fTemp57 + (fRec81[1] + fTemp58));
			fRec79[0] = fRec81[0];
			float fRec80 = (fTemp58 + fTemp57);
			output24[i] = FAUSTFLOAT((fRec0[0] * (fRec80 + (0.0434169397f * fTemp12))));
			float fTemp59 = (fConst24 * (0.0f - (((0.0827651322f * fTemp9) + (0.0273843631f * fTemp7)) + (5.92059996e-06f * fTemp8))));
			float fTemp60 = (fConst25 * fRec82[1]);
			fRec84[0] = (fTemp59 + (fRec84[1] + fTemp60));
			fRec82[0] = fRec84[0];
			float fRec83 = (fTemp60 + fTemp59);
			fVec21[0] = (fRec83 + (0.0554073155f * fTemp12));
			output25[i] = FAUSTFLOAT((0.999779761f * (fRec0[0] * fVec21[iConst26])));
			float fTemp61 = (fConst28 * ((0.0388509668f * fTemp8) - ((0.0618637428f * fTemp9) + (0.0285150502f * fTemp7))));
			float fTemp62 = (fConst29 * fRec85[1]);
			fRec87[0] = (fTemp61 + (fRec87[1] + fTemp62));
			fRec85[0] = fRec87[0];
			float fRec86 = (fTemp62 + fTemp61);
			output26[i] = FAUSTFLOAT((fRec0[0] * (fRec86 + (0.0494432151f * fTemp12))));
			float fTemp63 = (fConst28 * ((0.0547024459f * fTemp8) - ((0.0315749347f * fTemp9) + (0.0296466146f * fTemp7))));
			float fTemp64 = (fConst29 * fRec88[1]);
			fRec90[0] = (fTemp63 + (fRec90[1] + fTemp64));
			fRec88[0] = fRec90[0];
			float fRec89 = (fTemp64 + fTemp63);
			output27[i] = FAUSTFLOAT((fRec0[0] * (fRec89 + (0.0434193648f * fTemp12))));
			float fTemp65 = (fConst24 * ((0.0827706978f * fTemp8) - ((2.25269991e-06f * fTemp9) + (0.0273699835f * fTemp7))));
			float fTemp66 = (fConst25 * fRec91[1]);
			fRec93[0] = (fTemp65 + (fRec93[1] + fTemp66));
			fRec91[0] = fRec93[0];
			float fRec92 = (fTemp66 + fTemp65);
			fVec22[0] = (fRec92 + (0.0554104783f * fTemp12));
			output28[i] = FAUSTFLOAT((0.999779761f * (fRec0[0] * fVec22[iConst26])));
			float fTemp67 = (fConst28 * (((0.0315765515f * fTemp9) + (0.0547167361f * fTemp8)) - (0.0296458863f * fTemp7)));
			float fTemp68 = (fConst29 * fRec94[1]);
			fRec96[0] = (fTemp67 + (fRec96[1] + fTemp68));
			fRec94[0] = fRec96[0];
			float fRec95 = (fTemp68 + fTemp67);
			output29[i] = FAUSTFLOAT((fRec0[0] * (fRec95 + (0.0434320159f * fTemp12))));
			float fTemp69 = (fConst28 * (((0.0618486144f * fTemp9) + (0.0388411246f * fTemp8)) - (0.028516816f * fTemp7)));
			float fTemp70 = (fConst29 * fRec97[1]);
			fRec99[0] = (fTemp69 + (fRec99[1] + fTemp70));
			fRec97[0] = fRec99[0];
			float fRec98 = (fTemp70 + fTemp69);
			output30[i] = FAUSTFLOAT((fRec0[0] * (fRec98 + (0.0494268872f * fTemp12))));
			fRec0[1] = fRec0[0];
			fRec4[1] = fRec4[0];
			fRec5[2] = fRec5[1];
			fRec5[1] = fRec5[0];
			fRec6[1] = fRec6[0];
			fRec7[2] = fRec7[1];
			fRec7[1] = fRec7[0];
			fRec8[2] = fRec8[1];
			fRec8[1] = fRec8[0];
			fRec3[1] = fRec3[0];
			fRec1[1] = fRec1[0];
			fRec9[2] = fRec9[1];
			fRec9[1] = fRec9[0];
			IOTA = (IOTA + 1);
			fRec12[1] = fRec12[0];
			fRec10[1] = fRec10[0];
			fRec15[1] = fRec15[0];
			fRec13[1] = fRec13[0];
			fRec18[1] = fRec18[0];
			fRec16[1] = fRec16[0];
			fRec21[1] = fRec21[0];
			fRec19[1] = fRec19[0];
			fRec24[1] = fRec24[0];
			fRec22[1] = fRec22[0];
			fRec27[1] = fRec27[0];
			fRec25[1] = fRec25[0];
			fRec30[1] = fRec30[0];
			fRec28[1] = fRec28[0];
			fRec33[1] = fRec33[0];
			fRec31[1] = fRec31[0];
			fRec36[1] = fRec36[0];
			fRec34[1] = fRec34[0];
			fRec39[1] = fRec39[0];
			fRec37[1] = fRec37[0];
			fRec42[1] = fRec42[0];
			fRec40[1] = fRec40[0];
			fRec45[1] = fRec45[0];
			fRec43[1] = fRec43[0];
			fRec48[1] = fRec48[0];
			fRec46[1] = fRec46[0];
			fRec51[1] = fRec51[0];
			fRec49[1] = fRec49[0];
			fRec54[1] = fRec54[0];
			fRec52[1] = fRec52[0];
			fRec57[1] = fRec57[0];
			fRec55[1] = fRec55[0];
			fRec60[1] = fRec60[0];
			fRec58[1] = fRec58[0];
			fRec63[1] = fRec63[0];
			fRec61[1] = fRec61[0];
			fRec66[1] = fRec66[0];
			fRec64[1] = fRec64[0];
			fVec19[2] = fVec19[1];
			fVec19[1] = fVec19[0];
			fRec69[1] = fRec69[0];
			fRec67[1] = fRec67[0];
			fRec72[1] = fRec72[0];
			fRec70[1] = fRec70[0];
			fRec75[1] = fRec75[0];
			fRec73[1] = fRec73[0];
			fVec20[2] = fVec20[1];
			fVec20[1] = fVec20[0];
			fRec78[1] = fRec78[0];
			fRec76[1] = fRec76[0];
			fRec81[1] = fRec81[0];
			fRec79[1] = fRec79[0];
			fRec84[1] = fRec84[0];
			fRec82[1] = fRec82[0];
			fVec21[2] = fVec21[1];
			fVec21[1] = fVec21[0];
			fRec87[1] = fRec87[0];
			fRec85[1] = fRec85[0];
			fRec90[1] = fRec90[0];
			fRec88[1] = fRec88[0];
			fRec93[1] = fRec93[0];
			fRec91[1] = fRec91[0];
			fVec22[2] = fVec22[1];
			fVec22[1] = fVec22[0];
			fRec96[1] = fRec96[0];
			fRec94[1] = fRec94[0];
			fRec99[1] = fRec99[0];
			fRec97[1] = fRec97[0];
			
		}
		
	}

	
};

//----------------------------------------------------------------------------
// SuperCollider/Faust interface
//----------------------------------------------------------------------------

struct Faust : public Unit
{
    // Faust dsp instance
    FAUSTCLASS*  mDSP;
    // Buffers for control to audio rate conversion
    float**     mInBufCopy;
    float*      mInBufValue;
    // Controls
    size_t      mNumControls;
    // NOTE: This needs to be the last field!
    //
    // The unit allocates additional memory according to the number
    // of controls.
    Control     mControls[0];

    int getNumAudioInputs() { return mDSP->getNumInputs(); }
};

// Global state

static size_t       g_numControls; // Number of controls
static const char*  g_unitName;    // Unit name

// Initialize the global state with unit name and sample rate.
void initState(const std::string& name, int sampleRate);

// Return the unit size in bytes, including static fields and controls.
static size_t unitSize();

// Convert a file name to a valid unit name.
static std::string fileNameToUnitName(const std::string& fileName);

// Convert the XML unit name to a valid class name.
static std::string normalizeClassName(const std::string& name);

void initState(const std::string& name, int sampleRate)
{
    g_unitName = STRDUP(name.c_str());

    mydsp* dsp = new FAUSTCLASS;
    ControlCounter* cc = new ControlCounter;

    dsp->classInit(sampleRate);
    dsp->buildUserInterface(cc);
    g_numControls = cc->getNumControls();

    delete dsp;
    delete cc;
}

size_t unitSize()
{
    return sizeof(Faust) + g_numControls * sizeof(Control);
}

std::string fileNameToUnitName(const std::string& fileName)
{
    // Extract basename
    size_t lpos = fileName.rfind('/', fileName.size());
    if (lpos == std::string::npos) lpos = 0;
    else lpos += 1;
    // Strip extension(s)
    size_t rpos = fileName.find('.', lpos);
    // Return substring
    return fileName.substr(lpos, rpos > lpos ? rpos - lpos : 0);
}

// Globals

static InterfaceTable* ft;

// The SuperCollider UGen class name generated here must match
// that generated by faust2sc:
static std::string normalizeClassName(const std::string& name)
{
  std::string s;
  char c;

  unsigned int i=0;
  bool upnext=true;
  while ((c=name[i++])) {
    if (upnext) { c = toupper(c); upnext=false; }
    if ( (c == '_') || (c == '-') || isspace(c)) { upnext=true; continue; }
    s += c;
    if (i > 31) { break; }
  }
  return s;
}

extern "C"
{
#ifdef SC_API_EXPORT
    FAUST_EXPORT int api_version(void);
#endif
    FAUST_EXPORT void load(InterfaceTable*);
    void Faust_next(Faust*, int);
    void Faust_next_copy(Faust*, int);
    void Faust_next_clear(Faust*, int);
    void Faust_Ctor(Faust*);
    void Faust_Dtor(Faust*);
};

inline static void fillBuffer(float* dst, int n, float v)
{
    Fill(n, dst, v);
}

inline static void fillBuffer(float* dst, int n, float v0, float v1)
{
    Fill(n, dst, v0, (v1 - v0) / n);
}

inline static void copyBuffer(float* dst, int n, float* src)
{
    Copy(n, dst, src);
}

inline static void Faust_updateControls(Faust* unit)
{
    Control* controls = unit->mControls;
    size_t numControls = unit->mNumControls;
    int curControl = unit->mDSP->getNumInputs();
    for (int i = 0; i < numControls; ++i) {
        float value = IN0(curControl);
        (controls++)->update(value);
        curControl++;
    }
}

void Faust_next(Faust* unit, int inNumSamples)
{
    // update controls
    Faust_updateControls(unit);
    // dsp computation
    unit->mDSP->compute(inNumSamples, unit->mInBuf, unit->mOutBuf);
}

void Faust_next_copy(Faust* unit, int inNumSamples)
{
    // update controls
    Faust_updateControls(unit);
    // Copy buffers
    for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
        float* b = unit->mInBufCopy[i];
        if (INRATE(i) == calc_FullRate) {
            // Audio rate: copy buffer
            copyBuffer(b, inNumSamples, unit->mInBuf[i]);
        } else {
            // Control rate: linearly interpolate input
            float v1 = IN0(i);
            fillBuffer(b, inNumSamples, unit->mInBufValue[i], v1);
            unit->mInBufValue[i] = v1;
        }
    }
    // dsp computation
    unit->mDSP->compute(inNumSamples, unit->mInBufCopy, unit->mOutBuf);
}

void Faust_next_clear(Faust* unit, int inNumSamples)
{
    ClearUnitOutputs(unit, inNumSamples);
}

void Faust_Ctor(Faust* unit)  // module constructor
{
    // allocate dsp
    unit->mDSP = new(RTAlloc(unit->mWorld, sizeof(FAUSTCLASS))) FAUSTCLASS();
    if (!unit->mDSP) {
        Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
        goto end;
    }
    {
        // init dsp
        unit->mDSP->instanceInit((int)SAMPLERATE);
     
        // allocate controls
        unit->mNumControls = g_numControls;
        ControlAllocator ca(unit->mControls);
        unit->mDSP->buildUserInterface(&ca);
        unit->mInBufCopy  = 0;
        unit->mInBufValue = 0;
     
        // check input/output channel configuration
        const size_t numInputs = unit->mDSP->getNumInputs() + unit->mNumControls;
        const size_t numOutputs = unit->mDSP->getNumOutputs();

        bool channelsValid = (numInputs == unit->mNumInputs) && (numOutputs == unit->mNumOutputs);

        if (channelsValid) {
            bool rateValid = true;
            for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
                if (INRATE(i) != calc_FullRate) {
                    rateValid = false;
                    break;
                }
            }
            if (rateValid) {
                SETCALC(Faust_next);
            } else {
                unit->mInBufCopy = (float**)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*sizeof(float*));
                if (!unit->mInBufCopy) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                // Allocate memory for input buffer copies (numInputs * bufLength)
                // and linear interpolation state (numInputs)
                // = numInputs * (bufLength + 1)
                unit->mInBufValue = (float*)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*sizeof(float));
                if (!unit->mInBufValue) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                // Aquire memory for interpolator state.
                float* mem = (float*)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*BUFLENGTH*sizeof(float));
                if (mem) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
                    // Initialize interpolator.
                    unit->mInBufValue[i] = IN0(i);
                    // Aquire buffer memory.
                    unit->mInBufCopy[i] = mem;
                    mem += BUFLENGTH;
                }
                SETCALC(Faust_next_copy);
            }
    #if !defined(NDEBUG)
            Print("Faust[%s]:\n", g_unitName);
            Print("    Inputs:   %d\n"
                  "    Outputs:  %d\n"
                  "    Callback: %s\n",
                  numInputs, numOutputs,
                  unit->mCalcFunc == (UnitCalcFunc)Faust_next ? "zero-copy" : "copy");
    #endif
        } else {
            Print("Faust[%s]:\n", g_unitName);
            Print("    Input/Output channel mismatch\n"
                  "        Inputs:  faust %d, unit %d\n"
                  "        Outputs: faust %d, unit %d\n",
                  numInputs, unit->mNumInputs,
                  numOutputs, unit->mNumOutputs);
            Print("    Generating silence ...\n");
            SETCALC(Faust_next_clear);
        }
    }
    
end:
    // Fix for https://github.com/grame-cncm/faust/issues/13
    ClearUnitOutputs(unit, 1);
}

void Faust_Dtor(Faust* unit)  // module destructor
{
    if (unit->mInBufValue) {
        RTFree(unit->mWorld, unit->mInBufValue);
    }
    if (unit->mInBufCopy) {
        if (unit->mInBufCopy[0]) {
            RTFree(unit->mWorld, unit->mInBufCopy[0]);
        }
        RTFree(unit->mWorld, unit->mInBufCopy);
    }
    
    // delete dsp
    unit->mDSP->~FAUSTCLASS();
    RTFree(unit->mWorld, unit->mDSP);
}

#ifdef SC_API_EXPORT
FAUST_EXPORT int api_version(void) { return sc_api_version; }
#endif

FAUST_EXPORT void load(InterfaceTable* inTable)
{
    ft = inTable;

    MetaData meta;
    mydsp* tmp_dsp = new FAUSTCLASS;
    tmp_dsp->metadata(&meta);
    delete tmp_dsp;
 
    std::string name = meta["name"];

    if (name.empty()) {
        name = fileNameToUnitName(__FILE__);
    }
  
    name = normalizeClassName(name);

#if !defined(NDEBUG) & defined(SC_API_EXPORT)
    Print("Faust: supercollider.cpp: sc_api_version = %d\n", sc_api_version);
#endif

    if (name.empty()) {
        // Catch empty name
        Print("Faust [supercollider.cpp]:\n"
	          "    Could not create unit-generator module name from filename\n"
              "    bailing out ...\n");
        return;
    }

    if (strncmp(name.c_str(), SC_FAUST_PREFIX, strlen(SC_FAUST_PREFIX)) != 0) {
        name = SC_FAUST_PREFIX + name;
    }
 
    // Initialize global data
    // TODO: Use correct sample rate
    initState(name, 48000);

    // Register ugen
    (*ft->fDefineUnit)(
        (char*)name.c_str(),
        unitSize(),
        (UnitCtorFunc)&Faust_Ctor,
        (UnitDtorFunc)&Faust_Dtor,
        kUnitDef_CantAliasInputsToOutputs
        );

#if !defined(NDEBUG)
    Print("Faust: %s numControls=%d\n", name.c_str(), g_numControls);
#endif // NDEBUG
}

#ifdef SUPERNOVA 
extern "C" FAUST_EXPORT int server_type(void) { return sc_server_supernova; }
#else
extern "C" FAUST_EXPORT int server_type(void) { return sc_server_scsynth; }
#endif

// EOF

#endif
