/* ------------------------------------------------------------
author: "AmbisonicDecoderToolkit"
copyright: "(c) Aaron J. Heller 2013"
license: "BSD 3-Clause License"
name: "SATOswDo5"
version: "1.2"
Code generated with Faust 2.5.12 (https://faust.grame.fr)
Compilation options: cpp, -scal -ftz 0
------------------------------------------------------------ */

#ifndef  __mydsp_H__
#define  __mydsp_H__

//-------------------------------------------------------------------
// FAUST architecture file for SuperCollider.
// Copyright (C) 2005-2012 Stefan Kersten.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//-------------------------------------------------------------------

// If other than 'faust2sc --prefix Faust' is used, sed this as well:
#if !defined(SC_FAUST_PREFIX)
#define SC_FAUST_PREFIX "Faust"
#endif

#include <map>
#include <string>
#include <string.h>
#include <SC_PlugIn.h>

/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __dsp__
#define __dsp__

#include <string>

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

class UI;
struct Meta;

/**
 * DSP memory manager.
 */

struct dsp_memory_manager {
    
    virtual ~dsp_memory_manager() {}
    
    virtual void* allocate(size_t size) = 0;
    virtual void destroy(void* ptr) = 0;
    
};

/**
* Signal processor definition.
*/

class dsp {

    public:

        dsp() {}
        virtual ~dsp() {}

        /* Return instance number of audio inputs */
        virtual int getNumInputs() = 0;
    
        /* Return instance number of audio outputs */
        virtual int getNumOutputs() = 0;
    
        /**
         * Trigger the ui_interface parameter with instance specific calls
         * to 'addBtton', 'addVerticalSlider'... in order to build the UI.
         *
         * @param ui_interface - the user interface builder
         */
        virtual void buildUserInterface(UI* ui_interface) = 0;
    
        /* Returns the sample rate currently used by the instance */
        virtual int getSampleRate() = 0;
    
        /**
         * Global init, calls the following methods:
         * - static class 'classInit': static tables initialization
         * - 'instanceInit': constants and instance state initialization
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void init(int samplingRate) = 0;

        /**
         * Init instance state
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void instanceInit(int samplingRate) = 0;

        /**
         * Init instance constant state
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void instanceConstants(int samplingRate) = 0;
    
        /* Init default control parameters values */
        virtual void instanceResetUserInterface() = 0;
    
        /* Init instance state (delay lines...) */
        virtual void instanceClear() = 0;
 
        /**
         * Return a clone of the instance.
         *
         * @return a copy of the instance on success, otherwise a null pointer.
         */
        virtual dsp* clone() = 0;
    
        /**
         * Trigger the Meta* parameter with instance specific calls to 'declare' (key, value) metadata.
         *
         * @param m - the Meta* meta user
         */
        virtual void metadata(Meta* m) = 0;
    
        /**
         * DSP instance computation, to be called with successive in/out audio buffers.
         *
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         *
         */
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) = 0;
    
        /**
         * DSP instance computation: alternative method to be used by subclasses.
         *
         * @param date_usec - the timestamp in microsec given by audio driver.
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         *
         */
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { compute(count, inputs, outputs); }
       
};

/**
 * Generic DSP decorator.
 */

class decorator_dsp : public dsp {

    protected:

        dsp* fDSP;

    public:

        decorator_dsp(dsp* dsp = 0):fDSP(dsp) {}
        virtual ~decorator_dsp() { delete fDSP; }

        virtual int getNumInputs() { return fDSP->getNumInputs(); }
        virtual int getNumOutputs() { return fDSP->getNumOutputs(); }
        virtual void buildUserInterface(UI* ui_interface) { fDSP->buildUserInterface(ui_interface); }
        virtual int getSampleRate() { return fDSP->getSampleRate(); }
        virtual void init(int samplingRate) { fDSP->init(samplingRate); }
        virtual void instanceInit(int samplingRate) { fDSP->instanceInit(samplingRate); }
        virtual void instanceConstants(int samplingRate) { fDSP->instanceConstants(samplingRate); }
        virtual void instanceResetUserInterface() { fDSP->instanceResetUserInterface(); }
        virtual void instanceClear() { fDSP->instanceClear(); }
        virtual decorator_dsp* clone() { return new decorator_dsp(fDSP->clone()); }
        virtual void metadata(Meta* m) { fDSP->metadata(m); }
        // Beware: subclasses usually have to overload the two 'compute' methods
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(count, inputs, outputs); }
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(date_usec, count, inputs, outputs); }
    
};

/**
 * DSP factory class.
 */

class dsp_factory {
    
    protected:
    
        // So that to force sub-classes to use deleteDSPFactory(dsp_factory* factory);
        virtual ~dsp_factory() {}
    
    public:
    
        virtual std::string getName() = 0;
        virtual std::string getSHAKey() = 0;
        virtual std::string getDSPCode() = 0;
    
        virtual dsp* createDSPInstance() = 0;
    
        virtual void setMemoryManager(dsp_memory_manager* manager) = 0;
        virtual dsp_memory_manager* getMemoryManager() = 0;
    
};

/**
 * On Intel set FZ (Flush to Zero) and DAZ (Denormals Are Zero)
 * flags to avoid costly denormals.
 */

#ifdef __SSE__
    #include <xmmintrin.h>
    #ifdef __SSE2__
        #define AVOIDDENORMALS _mm_setcsr(_mm_getcsr() | 0x8040)
    #else
        #define AVOIDDENORMALS _mm_setcsr(_mm_getcsr() | 0x8000)
    #endif
#else
    #define AVOIDDENORMALS
#endif

#endif
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __UI_H__
#define __UI_H__

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

/*******************************************************************************
 * UI : Faust DSP User Interface
 * User Interface as expected by the buildUserInterface() method of a DSP.
 * This abstract class contains only the method that the Faust compiler can
 * generate to describe a DSP user interface.
 ******************************************************************************/

struct Soundfile;

class UI
{

    public:

        UI() {}

        virtual ~UI() {}

        // -- widget's layouts

        virtual void openTabBox(const char* label) = 0;
        virtual void openHorizontalBox(const char* label) = 0;
        virtual void openVerticalBox(const char* label) = 0;
        virtual void closeBox() = 0;

        // -- active widgets

        virtual void addButton(const char* label, FAUSTFLOAT* zone) = 0;
        virtual void addCheckButton(const char* label, FAUSTFLOAT* zone) = 0;
        virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;
        virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;
        virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;

        // -- passive widgets

        virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) = 0;
        virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) = 0;
    
        // -- soundfiles
    
        virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) = 0;

        // -- metadata declarations

        virtual void declare(FAUSTFLOAT*, const char*, const char*) {}
};

#endif
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/
 
#ifndef __misc__
#define __misc__

#include <algorithm>
#include <map>
#include <string.h>
#include <stdlib.h>

/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __meta__
#define __meta__

struct Meta
{
    virtual void declare(const char* key, const char* value) = 0;
    virtual ~Meta() {};
};

#endif

using std::max;
using std::min;

struct XXXX_Meta : std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key]=value; }
};

struct MY_Meta : Meta, std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key]=value; }
};

inline int lsr(int x, int n)	{ return int(((unsigned int)x) >> n); }

inline int int2pow2(int x)		{ int r = 0; while ((1<<r) < x) r++; return r; }

inline long lopt(char* argv[], const char* name, long def)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return atoi(argv[i+1]);
	return def;
}

inline bool isopt(char* argv[], const char* name)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return true;
	return false;
}

inline const char* lopts(char* argv[], const char* name, const char* def)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return argv[i+1];
	return def;
}

#endif


using namespace std;

#if defined(__GNUC__) && __GNUC__ >= 4
    #define FAUST_EXPORT __attribute__((visibility("default")))
#else
    #define FAUST_EXPORT  SC_API_EXPORT
#endif

#ifdef WIN32
    #define STRDUP _strdup
#else
    #define STRDUP strdup
#endif

//----------------------------------------------------------------------------
// Vector intrinsics
//----------------------------------------------------------------------------


//----------------------------------------------------------------------------
// Metadata
//----------------------------------------------------------------------------

class MetaData : public Meta
               , public std::map<std::string, std::string>
{
public:
    void declare(const char* key, const char* value)
    {
        (*this)[key] = value;
    }
};

//----------------------------------------------------------------------------
// Control counter
//----------------------------------------------------------------------------

class ControlCounter : public UI
{
public:
    ControlCounter()
        : mNumControlInputs(0),
          mNumControlOutputs(0)
    {}

    size_t getNumControls() const { return getNumControlInputs(); }
    size_t getNumControlInputs() const { return mNumControlInputs; }
    size_t getNumControlOutputs() const { return mNumControlOutputs; }

    // Layout widgets
    virtual void openTabBox(const char* label) { }
    virtual void openHorizontalBox(const char* label) { }
    virtual void openVerticalBox(const char* label) { }
    virtual void closeBox() { }

    // Active widgets
    virtual void addButton(const char* label, FAUSTFLOAT* zone)
    { addControlInput(); }
    virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
    { addControlInput(); }
    virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }
    virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }
    virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }

    // Passive widgets
    virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
    { addControlOutput(); }
    virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
    { addControlOutput(); }
    
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}

protected:
    void addControlInput() { mNumControlInputs++; }
    void addControlOutput() { mNumControlOutputs++; }

private:
    size_t mNumControlInputs;
    size_t mNumControlOutputs;
};

//----------------------------------------------------------------------------
// UI control
//----------------------------------------------------------------------------

struct Control
{
    typedef void (*UpdateFunction)(Control* self, FAUSTFLOAT value);

    UpdateFunction updateFunction;
    FAUSTFLOAT* zone;
    FAUSTFLOAT min, max;

    inline void update(FAUSTFLOAT value)
    {
        (*updateFunction)(this, value);
    }

    static void simpleUpdate(Control* self, FAUSTFLOAT value)
    {
        *self->zone = value;
    }
    static void boundedUpdate(Control* self, FAUSTFLOAT value)
    {
        *self->zone = sc_clip(value, self->min, self->max);
    }
};

//----------------------------------------------------------------------------
// Control allocator
//----------------------------------------------------------------------------

class ControlAllocator : public UI
{
public:
    ControlAllocator(Control* controls)
        : mControls(controls)
    { }

    // Layout widgets
    virtual void openTabBox(const char* label) { }
    virtual void openHorizontalBox(const char* label) { }
    virtual void openVerticalBox(const char* label) { }
    virtual void closeBox() { }

    // Active widgets
    virtual void addButton(const char* label, FAUSTFLOAT* zone)
    { addSimpleControl(zone); }
    virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
    { addSimpleControl(zone); }
    virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }
    virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }
    virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }

    // Passive widgets
    virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
    virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}

private:
    void addControl(Control::UpdateFunction updateFunction, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT /* step */)
    {
        Control* ctrl        = mControls++;
        ctrl->updateFunction = updateFunction;
        ctrl->zone           = zone;
        ctrl->min            = min;
        ctrl->max            = max;
    }
    void addSimpleControl(FAUSTFLOAT* zone)
    {
        addControl(Control::simpleUpdate, zone, 0.f, 0.f, 0.f);
    }
    void addBoundedControl(FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    {
        addControl(Control::boundedUpdate, zone, min, max, step);
    }

private:
    Control* mControls;
};

//----------------------------------------------------------------------------
// FAUST generated code
//----------------------------------------------------------------------------

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif 

#include <cmath>
#include <math.h>

float mydsp_faustpower2_f(float value) {
	return (value * value);
	
}

#ifndef FAUSTCLASS 
#define FAUSTCLASS mydsp
#endif
#ifdef __APPLE__ 
#define exp10f __exp10f
#define exp10 __exp10
#endif

class mydsp : public dsp {
	
 private:
	
	FAUSTFLOAT fCheckbox0;
	FAUSTFLOAT fHslider0;
	float fRec0[2];
	int fSamplingFreq;
	int iConst0;
	float fConst1;
	float fConst2;
	float fConst3;
	float fConst4;
	float fConst5;
	float fConst6;
	float fConst7;
	float fConst8;
	float fConst9;
	float fConst10;
	FAUSTFLOAT fHslider1;
	float fRec17[2];
	float fRec16[3];
	FAUSTFLOAT fHslider2;
	float fRec18[2];
	float fRec19[3];
	float fRec20[3];
	float fRec21[3];
	float fRec22[3];
	float fRec23[3];
	float fRec24[3];
	float fRec25[3];
	float fRec26[3];
	float fRec27[3];
	float fRec28[3];
	float fConst11;
	float fConst12;
	float fRec15[2];
	float fRec13[2];
	float fRec12[2];
	float fRec10[2];
	float fRec9[2];
	float fRec7[2];
	float fRec6[2];
	float fRec4[2];
	float fRec3[2];
	float fRec1[2];
	float fConst13;
	float fConst14;
	float fConst15;
	float fConst16;
	float fConst17;
	float fRec41[3];
	float fRec42[3];
	float fRec43[3];
	float fRec44[3];
	float fRec45[3];
	float fRec46[3];
	float fRec47[3];
	float fRec48[3];
	float fRec49[3];
	float fConst18;
	float fConst19;
	float fRec40[2];
	float fRec38[2];
	float fRec37[2];
	float fRec35[2];
	float fRec34[2];
	float fRec32[2];
	float fRec31[2];
	float fRec29[2];
	float fConst20;
	float fConst21;
	float fConst22;
	float fRec59[3];
	float fRec60[3];
	float fRec61[3];
	float fRec62[3];
	float fRec63[3];
	float fRec64[3];
	float fRec65[3];
	float fConst23;
	float fConst24;
	float fRec58[2];
	float fRec56[2];
	float fRec55[2];
	float fRec53[2];
	float fConst25;
	float fRec52[2];
	float fRec50[2];
	float fConst26;
	float fConst27;
	float fRec69[3];
	float fRec70[3];
	float fRec71[3];
	float fConst28;
	float fRec68[2];
	float fRec66[2];
	float fConst29;
	float fConst30;
	float fRec78[3];
	float fRec79[3];
	float fRec80[3];
	float fRec81[3];
	float fRec82[3];
	float fConst31;
	float fConst32;
	float fRec77[2];
	float fRec75[2];
	float fRec74[2];
	float fRec72[2];
	float fRec83[3];
	int IOTA;
	float fVec0[2048];
	int iConst33;
	float fConst34;
	float fConst35;
	float fConst36;
	float fConst37;
	float fConst38;
	float fConst39;
	float fConst40;
	float fConst41;
	float fConst42;
	float fRec98[2];
	float fRec96[2];
	float fRec95[2];
	float fRec93[2];
	float fRec92[2];
	float fRec90[2];
	float fRec89[2];
	float fRec87[2];
	float fRec86[2];
	float fRec84[2];
	float fConst43;
	float fConst44;
	float fConst45;
	float fConst46;
	float fConst47;
	float fRec110[2];
	float fRec108[2];
	float fRec107[2];
	float fRec105[2];
	float fConst48;
	float fConst49;
	float fRec104[2];
	float fRec102[2];
	float fRec101[2];
	float fRec99[2];
	float fConst50;
	float fConst51;
	float fConst52;
	float fConst53;
	float fConst54;
	float fConst55;
	float fRec119[2];
	float fRec117[2];
	float fRec116[2];
	float fRec114[2];
	float fRec113[2];
	float fRec111[2];
	float fConst56;
	float fConst57;
	float fConst58;
	float fRec122[2];
	float fRec120[2];
	float fConst59;
	float fConst60;
	float fConst61;
	float fConst62;
	float fRec128[2];
	float fRec126[2];
	float fRec125[2];
	float fRec123[2];
	float fVec1[1024];
	int iConst63;
	float fConst64;
	float fConst65;
	float fConst66;
	float fConst67;
	float fConst68;
	float fConst69;
	float fRec137[2];
	float fRec135[2];
	float fRec134[2];
	float fRec132[2];
	float fRec131[2];
	float fRec129[2];
	float fConst70;
	float fConst71;
	float fConst72;
	float fConst73;
	float fRec143[2];
	float fRec141[2];
	float fRec140[2];
	float fRec138[2];
	float fConst74;
	float fConst75;
	float fConst76;
	float fRec146[2];
	float fRec144[2];
	float fConst77;
	float fConst78;
	float fConst79;
	float fConst80;
	float fConst81;
	float fConst82;
	float fConst83;
	float fRec158[2];
	float fRec156[2];
	float fRec155[2];
	float fRec153[2];
	float fRec152[2];
	float fRec150[2];
	float fRec149[2];
	float fRec147[2];
	float fConst84;
	float fConst85;
	float fConst86;
	float fConst87;
	float fConst88;
	float fConst89;
	float fConst90;
	float fConst91;
	float fConst92;
	float fRec173[2];
	float fRec171[2];
	float fRec170[2];
	float fRec168[2];
	float fRec167[2];
	float fRec165[2];
	float fRec164[2];
	float fRec162[2];
	float fRec161[2];
	float fRec159[2];
	float fVec2[1024];
	int iConst93;
	float fRec188[2];
	float fRec186[2];
	float fRec185[2];
	float fRec183[2];
	float fRec182[2];
	float fRec180[2];
	float fRec179[2];
	float fRec177[2];
	float fRec176[2];
	float fRec174[2];
	float fRec200[2];
	float fRec198[2];
	float fRec197[2];
	float fRec195[2];
	float fRec194[2];
	float fRec192[2];
	float fRec191[2];
	float fRec189[2];
	float fRec209[2];
	float fRec207[2];
	float fRec206[2];
	float fRec204[2];
	float fRec203[2];
	float fRec201[2];
	float fRec212[2];
	float fRec210[2];
	float fRec218[2];
	float fRec216[2];
	float fRec215[2];
	float fRec213[2];
	float fVec3[1024];
	float fRec233[2];
	float fRec231[2];
	float fRec230[2];
	float fRec228[2];
	float fRec227[2];
	float fRec225[2];
	float fRec224[2];
	float fRec222[2];
	float fRec221[2];
	float fRec219[2];
	float fRec245[2];
	float fRec243[2];
	float fRec242[2];
	float fRec240[2];
	float fRec239[2];
	float fRec237[2];
	float fRec236[2];
	float fRec234[2];
	float fRec254[2];
	float fRec252[2];
	float fRec251[2];
	float fRec249[2];
	float fRec248[2];
	float fRec246[2];
	float fRec257[2];
	float fRec255[2];
	float fRec263[2];
	float fRec261[2];
	float fRec260[2];
	float fRec258[2];
	float fVec4[1024];
	float fRec278[2];
	float fRec276[2];
	float fRec275[2];
	float fRec273[2];
	float fRec272[2];
	float fRec270[2];
	float fRec269[2];
	float fRec267[2];
	float fRec266[2];
	float fRec264[2];
	float fRec290[2];
	float fRec288[2];
	float fRec287[2];
	float fRec285[2];
	float fRec284[2];
	float fRec282[2];
	float fRec281[2];
	float fRec279[2];
	float fRec299[2];
	float fRec297[2];
	float fRec296[2];
	float fRec294[2];
	float fRec293[2];
	float fRec291[2];
	float fRec302[2];
	float fRec300[2];
	float fRec308[2];
	float fRec306[2];
	float fRec305[2];
	float fRec303[2];
	float fVec5[1024];
	float fRec320[2];
	float fRec318[2];
	float fRec317[2];
	float fRec315[2];
	float fRec314[2];
	float fRec312[2];
	float fRec311[2];
	float fRec309[2];
	float fRec329[2];
	float fRec327[2];
	float fRec326[2];
	float fRec324[2];
	float fRec323[2];
	float fRec321[2];
	float fRec332[2];
	float fRec330[2];
	float fRec338[2];
	float fRec336[2];
	float fRec335[2];
	float fRec333[2];
	float fRec353[2];
	float fRec351[2];
	float fRec350[2];
	float fRec348[2];
	float fRec347[2];
	float fRec345[2];
	float fRec344[2];
	float fRec342[2];
	float fRec341[2];
	float fRec339[2];
	float fVec6[1024];
	float fConst94;
	float fConst95;
	float fConst96;
	float fRec356[2];
	float fRec354[2];
	float fConst97;
	float fConst98;
	float fConst99;
	float fConst100;
	float fRec362[2];
	float fRec360[2];
	float fRec359[2];
	float fRec357[2];
	float fConst101;
	float fConst102;
	float fConst103;
	float fConst104;
	float fConst105;
	float fConst106;
	float fRec371[2];
	float fRec369[2];
	float fRec368[2];
	float fRec366[2];
	float fRec365[2];
	float fRec363[2];
	float fConst107;
	float fConst108;
	float fConst109;
	float fConst110;
	float fConst111;
	float fConst112;
	float fConst113;
	float fRec383[2];
	float fRec381[2];
	float fRec380[2];
	float fRec378[2];
	float fRec377[2];
	float fRec375[2];
	float fRec374[2];
	float fRec372[2];
	float fConst114;
	float fConst115;
	float fConst116;
	float fConst117;
	float fConst118;
	float fConst119;
	float fConst120;
	float fConst121;
	float fConst122;
	float fRec398[2];
	float fRec396[2];
	float fRec395[2];
	float fRec393[2];
	float fRec392[2];
	float fRec390[2];
	float fRec389[2];
	float fRec387[2];
	float fRec386[2];
	float fRec384[2];
	float fVec7[1024];
	int iConst123;
	float fConst124;
	float fConst125;
	float fConst126;
	float fConst127;
	float fConst128;
	float fConst129;
	float fRec407[2];
	float fRec405[2];
	float fRec404[2];
	float fRec402[2];
	float fRec401[2];
	float fRec399[2];
	float fConst130;
	float fConst131;
	float fConst132;
	float fConst133;
	float fConst134;
	float fConst135;
	float fConst136;
	float fRec419[2];
	float fRec417[2];
	float fRec416[2];
	float fRec414[2];
	float fRec413[2];
	float fRec411[2];
	float fRec410[2];
	float fRec408[2];
	float fConst137;
	float fConst138;
	float fConst139;
	float fRec422[2];
	float fRec420[2];
	float fConst140;
	float fConst141;
	float fConst142;
	float fConst143;
	float fRec428[2];
	float fRec426[2];
	float fRec425[2];
	float fRec423[2];
	float fConst144;
	float fConst145;
	float fConst146;
	float fConst147;
	float fConst148;
	float fConst149;
	float fConst150;
	float fConst151;
	float fConst152;
	float fRec443[2];
	float fRec441[2];
	float fRec440[2];
	float fRec438[2];
	float fRec437[2];
	float fRec435[2];
	float fRec434[2];
	float fRec432[2];
	float fRec431[2];
	float fRec429[2];
	float fVec8[1024];
	int iConst153;
	float fRec446[2];
	float fRec444[2];
	float fRec452[2];
	float fRec450[2];
	float fRec449[2];
	float fRec447[2];
	float fRec461[2];
	float fRec459[2];
	float fRec458[2];
	float fRec456[2];
	float fRec455[2];
	float fRec453[2];
	float fRec473[2];
	float fRec471[2];
	float fRec470[2];
	float fRec468[2];
	float fRec467[2];
	float fRec465[2];
	float fRec464[2];
	float fRec462[2];
	float fRec488[2];
	float fRec486[2];
	float fRec485[2];
	float fRec483[2];
	float fRec482[2];
	float fRec480[2];
	float fRec479[2];
	float fRec477[2];
	float fRec476[2];
	float fRec474[2];
	float fVec9[1024];
	float fRec491[2];
	float fRec489[2];
	float fRec497[2];
	float fRec495[2];
	float fRec494[2];
	float fRec492[2];
	float fRec506[2];
	float fRec504[2];
	float fRec503[2];
	float fRec501[2];
	float fRec500[2];
	float fRec498[2];
	float fRec518[2];
	float fRec516[2];
	float fRec515[2];
	float fRec513[2];
	float fRec512[2];
	float fRec510[2];
	float fRec509[2];
	float fRec507[2];
	float fRec533[2];
	float fRec531[2];
	float fRec530[2];
	float fRec528[2];
	float fRec527[2];
	float fRec525[2];
	float fRec524[2];
	float fRec522[2];
	float fRec521[2];
	float fRec519[2];
	float fVec10[1024];
	float fRec548[2];
	float fRec546[2];
	float fRec545[2];
	float fRec543[2];
	float fRec542[2];
	float fRec540[2];
	float fRec539[2];
	float fRec537[2];
	float fRec536[2];
	float fRec534[2];
	float fRec560[2];
	float fRec558[2];
	float fRec557[2];
	float fRec555[2];
	float fRec554[2];
	float fRec552[2];
	float fRec551[2];
	float fRec549[2];
	float fRec569[2];
	float fRec567[2];
	float fRec566[2];
	float fRec564[2];
	float fRec563[2];
	float fRec561[2];
	float fRec572[2];
	float fRec570[2];
	float fRec578[2];
	float fRec576[2];
	float fRec575[2];
	float fRec573[2];
	float fVec11[1024];
	float fRec593[2];
	float fRec591[2];
	float fRec590[2];
	float fRec588[2];
	float fRec587[2];
	float fRec585[2];
	float fRec584[2];
	float fRec582[2];
	float fRec581[2];
	float fRec579[2];
	float fRec605[2];
	float fRec603[2];
	float fRec602[2];
	float fRec600[2];
	float fRec599[2];
	float fRec597[2];
	float fRec596[2];
	float fRec594[2];
	float fRec614[2];
	float fRec612[2];
	float fRec611[2];
	float fRec609[2];
	float fRec608[2];
	float fRec606[2];
	float fRec617[2];
	float fRec615[2];
	float fRec623[2];
	float fRec621[2];
	float fRec620[2];
	float fRec618[2];
	float fVec12[1024];
	float fRec638[2];
	float fRec636[2];
	float fRec635[2];
	float fRec633[2];
	float fRec632[2];
	float fRec630[2];
	float fRec629[2];
	float fRec627[2];
	float fRec626[2];
	float fRec624[2];
	float fRec650[2];
	float fRec648[2];
	float fRec647[2];
	float fRec645[2];
	float fRec644[2];
	float fRec642[2];
	float fRec641[2];
	float fRec639[2];
	float fRec659[2];
	float fRec657[2];
	float fRec656[2];
	float fRec654[2];
	float fRec653[2];
	float fRec651[2];
	float fRec662[2];
	float fRec660[2];
	float fRec668[2];
	float fRec666[2];
	float fRec665[2];
	float fRec663[2];
	float fVec13[1024];
	float fRec683[2];
	float fRec681[2];
	float fRec680[2];
	float fRec678[2];
	float fRec677[2];
	float fRec675[2];
	float fRec674[2];
	float fRec672[2];
	float fRec671[2];
	float fRec669[2];
	float fRec695[2];
	float fRec693[2];
	float fRec692[2];
	float fRec690[2];
	float fRec689[2];
	float fRec687[2];
	float fRec686[2];
	float fRec684[2];
	float fRec704[2];
	float fRec702[2];
	float fRec701[2];
	float fRec699[2];
	float fRec698[2];
	float fRec696[2];
	float fRec707[2];
	float fRec705[2];
	float fRec713[2];
	float fRec711[2];
	float fRec710[2];
	float fRec708[2];
	float fVec14[1024];
	float fRec728[2];
	float fRec726[2];
	float fRec725[2];
	float fRec723[2];
	float fRec722[2];
	float fRec720[2];
	float fRec719[2];
	float fRec717[2];
	float fRec716[2];
	float fRec714[2];
	float fRec740[2];
	float fRec738[2];
	float fRec737[2];
	float fRec735[2];
	float fRec734[2];
	float fRec732[2];
	float fRec731[2];
	float fRec729[2];
	float fRec749[2];
	float fRec747[2];
	float fRec746[2];
	float fRec744[2];
	float fRec743[2];
	float fRec741[2];
	float fRec752[2];
	float fRec750[2];
	float fRec758[2];
	float fRec756[2];
	float fRec755[2];
	float fRec753[2];
	float fVec15[1024];
	float fRec773[2];
	float fRec771[2];
	float fRec770[2];
	float fRec768[2];
	float fRec767[2];
	float fRec765[2];
	float fRec764[2];
	float fRec762[2];
	float fRec761[2];
	float fRec759[2];
	float fRec785[2];
	float fRec783[2];
	float fRec782[2];
	float fRec780[2];
	float fRec779[2];
	float fRec777[2];
	float fRec776[2];
	float fRec774[2];
	float fRec794[2];
	float fRec792[2];
	float fRec791[2];
	float fRec789[2];
	float fRec788[2];
	float fRec786[2];
	float fRec797[2];
	float fRec795[2];
	float fRec803[2];
	float fRec801[2];
	float fRec800[2];
	float fRec798[2];
	float fVec16[1024];
	float fRec818[2];
	float fRec816[2];
	float fRec815[2];
	float fRec813[2];
	float fRec812[2];
	float fRec810[2];
	float fRec809[2];
	float fRec807[2];
	float fRec806[2];
	float fRec804[2];
	float fRec830[2];
	float fRec828[2];
	float fRec827[2];
	float fRec825[2];
	float fRec824[2];
	float fRec822[2];
	float fRec821[2];
	float fRec819[2];
	float fRec839[2];
	float fRec837[2];
	float fRec836[2];
	float fRec834[2];
	float fRec833[2];
	float fRec831[2];
	float fRec842[2];
	float fRec840[2];
	float fRec848[2];
	float fRec846[2];
	float fRec845[2];
	float fRec843[2];
	float fVec17[1024];
	float fRec863[2];
	float fRec861[2];
	float fRec860[2];
	float fRec858[2];
	float fRec857[2];
	float fRec855[2];
	float fRec854[2];
	float fRec852[2];
	float fRec851[2];
	float fRec849[2];
	float fRec875[2];
	float fRec873[2];
	float fRec872[2];
	float fRec870[2];
	float fRec869[2];
	float fRec867[2];
	float fRec866[2];
	float fRec864[2];
	float fRec884[2];
	float fRec882[2];
	float fRec881[2];
	float fRec879[2];
	float fRec878[2];
	float fRec876[2];
	float fRec887[2];
	float fRec885[2];
	float fRec893[2];
	float fRec891[2];
	float fRec890[2];
	float fRec888[2];
	float fVec18[1024];
	float fConst154;
	float fConst155;
	float fConst156;
	float fConst157;
	float fConst158;
	float fConst159;
	float fConst160;
	float fRec905[2];
	float fRec903[2];
	float fRec902[2];
	float fRec900[2];
	float fRec899[2];
	float fRec897[2];
	float fRec896[2];
	float fRec894[2];
	float fConst161;
	float fConst162;
	float fConst163;
	float fConst164;
	float fConst165;
	float fConst166;
	float fRec914[2];
	float fRec912[2];
	float fRec911[2];
	float fRec909[2];
	float fRec908[2];
	float fRec906[2];
	float fConst167;
	float fConst168;
	float fConst169;
	float fConst170;
	float fRec920[2];
	float fRec918[2];
	float fRec917[2];
	float fRec915[2];
	float fConst171;
	float fConst172;
	float fConst173;
	float fConst174;
	float fConst175;
	float fConst176;
	float fConst177;
	float fConst178;
	float fConst179;
	float fRec935[2];
	float fRec933[2];
	float fRec932[2];
	float fRec930[2];
	float fRec929[2];
	float fRec927[2];
	float fRec926[2];
	float fRec924[2];
	float fRec923[2];
	float fRec921[2];
	float fConst180;
	float fConst181;
	float fConst182;
	float fRec938[2];
	float fRec936[2];
	float fVec19[2];
	int iConst183;
	float fConst184;
	float fConst185;
	float fConst186;
	float fConst187;
	float fConst188;
	float fConst189;
	float fConst190;
	float fConst191;
	float fConst192;
	float fRec953[2];
	float fRec951[2];
	float fRec950[2];
	float fRec948[2];
	float fRec947[2];
	float fRec945[2];
	float fRec944[2];
	float fRec942[2];
	float fRec941[2];
	float fRec939[2];
	float fConst193;
	float fConst194;
	float fConst195;
	float fConst196;
	float fConst197;
	float fConst198;
	float fConst199;
	float fRec965[2];
	float fRec963[2];
	float fRec962[2];
	float fRec960[2];
	float fRec959[2];
	float fRec957[2];
	float fRec956[2];
	float fRec954[2];
	float fConst200;
	float fConst201;
	float fConst202;
	float fConst203;
	float fConst204;
	float fConst205;
	float fRec974[2];
	float fRec972[2];
	float fRec971[2];
	float fRec969[2];
	float fRec968[2];
	float fRec966[2];
	float fConst206;
	float fConst207;
	float fConst208;
	float fRec977[2];
	float fRec975[2];
	float fConst209;
	float fConst210;
	float fConst211;
	float fConst212;
	float fRec983[2];
	float fRec981[2];
	float fRec980[2];
	float fRec978[2];
	float fRec998[2];
	float fRec996[2];
	float fRec995[2];
	float fRec993[2];
	float fRec992[2];
	float fRec990[2];
	float fRec989[2];
	float fRec987[2];
	float fRec986[2];
	float fRec984[2];
	float fRec1010[2];
	float fRec1008[2];
	float fRec1007[2];
	float fRec1005[2];
	float fRec1004[2];
	float fRec1002[2];
	float fRec1001[2];
	float fRec999[2];
	float fRec1019[2];
	float fRec1017[2];
	float fRec1016[2];
	float fRec1014[2];
	float fRec1013[2];
	float fRec1011[2];
	float fRec1022[2];
	float fRec1020[2];
	float fRec1028[2];
	float fRec1026[2];
	float fRec1025[2];
	float fRec1023[2];
	float fRec1034[2];
	float fRec1032[2];
	float fRec1031[2];
	float fRec1029[2];
	float fRec1037[2];
	float fRec1035[2];
	float fRec1052[2];
	float fRec1050[2];
	float fRec1049[2];
	float fRec1047[2];
	float fRec1046[2];
	float fRec1044[2];
	float fRec1043[2];
	float fRec1041[2];
	float fRec1040[2];
	float fRec1038[2];
	float fRec1061[2];
	float fRec1059[2];
	float fRec1058[2];
	float fRec1056[2];
	float fRec1055[2];
	float fRec1053[2];
	float fRec1073[2];
	float fRec1071[2];
	float fRec1070[2];
	float fRec1068[2];
	float fRec1067[2];
	float fRec1065[2];
	float fRec1064[2];
	float fRec1062[2];
	float fVec20[2];
	float fRec1088[2];
	float fRec1086[2];
	float fRec1085[2];
	float fRec1083[2];
	float fRec1082[2];
	float fRec1080[2];
	float fRec1079[2];
	float fRec1077[2];
	float fRec1076[2];
	float fRec1074[2];
	float fRec1100[2];
	float fRec1098[2];
	float fRec1097[2];
	float fRec1095[2];
	float fRec1094[2];
	float fRec1092[2];
	float fRec1091[2];
	float fRec1089[2];
	float fRec1109[2];
	float fRec1107[2];
	float fRec1106[2];
	float fRec1104[2];
	float fRec1103[2];
	float fRec1101[2];
	float fRec1112[2];
	float fRec1110[2];
	float fRec1118[2];
	float fRec1116[2];
	float fRec1115[2];
	float fRec1113[2];
	float fRec1133[2];
	float fRec1131[2];
	float fRec1130[2];
	float fRec1128[2];
	float fRec1127[2];
	float fRec1125[2];
	float fRec1124[2];
	float fRec1122[2];
	float fRec1121[2];
	float fRec1119[2];
	float fRec1145[2];
	float fRec1143[2];
	float fRec1142[2];
	float fRec1140[2];
	float fRec1139[2];
	float fRec1137[2];
	float fRec1136[2];
	float fRec1134[2];
	float fRec1154[2];
	float fRec1152[2];
	float fRec1151[2];
	float fRec1149[2];
	float fRec1148[2];
	float fRec1146[2];
	float fRec1157[2];
	float fRec1155[2];
	float fRec1163[2];
	float fRec1161[2];
	float fRec1160[2];
	float fRec1158[2];
	float fRec1178[2];
	float fRec1176[2];
	float fRec1175[2];
	float fRec1173[2];
	float fRec1172[2];
	float fRec1170[2];
	float fRec1169[2];
	float fRec1167[2];
	float fRec1166[2];
	float fRec1164[2];
	float fRec1190[2];
	float fRec1188[2];
	float fRec1187[2];
	float fRec1185[2];
	float fRec1184[2];
	float fRec1182[2];
	float fRec1181[2];
	float fRec1179[2];
	float fRec1199[2];
	float fRec1197[2];
	float fRec1196[2];
	float fRec1194[2];
	float fRec1193[2];
	float fRec1191[2];
	float fRec1202[2];
	float fRec1200[2];
	float fRec1208[2];
	float fRec1206[2];
	float fRec1205[2];
	float fRec1203[2];
	float fVec21[2];
	float fRec1223[2];
	float fRec1221[2];
	float fRec1220[2];
	float fRec1218[2];
	float fRec1217[2];
	float fRec1215[2];
	float fRec1214[2];
	float fRec1212[2];
	float fRec1211[2];
	float fRec1209[2];
	float fRec1235[2];
	float fRec1233[2];
	float fRec1232[2];
	float fRec1230[2];
	float fRec1229[2];
	float fRec1227[2];
	float fRec1226[2];
	float fRec1224[2];
	float fRec1244[2];
	float fRec1242[2];
	float fRec1241[2];
	float fRec1239[2];
	float fRec1238[2];
	float fRec1236[2];
	float fRec1247[2];
	float fRec1245[2];
	float fRec1253[2];
	float fRec1251[2];
	float fRec1250[2];
	float fRec1248[2];
	float fRec1268[2];
	float fRec1266[2];
	float fRec1265[2];
	float fRec1263[2];
	float fRec1262[2];
	float fRec1260[2];
	float fRec1259[2];
	float fRec1257[2];
	float fRec1256[2];
	float fRec1254[2];
	float fRec1280[2];
	float fRec1278[2];
	float fRec1277[2];
	float fRec1275[2];
	float fRec1274[2];
	float fRec1272[2];
	float fRec1271[2];
	float fRec1269[2];
	float fRec1289[2];
	float fRec1287[2];
	float fRec1286[2];
	float fRec1284[2];
	float fRec1283[2];
	float fRec1281[2];
	float fRec1292[2];
	float fRec1290[2];
	float fRec1298[2];
	float fRec1296[2];
	float fRec1295[2];
	float fRec1293[2];
	float fRec1313[2];
	float fRec1311[2];
	float fRec1310[2];
	float fRec1308[2];
	float fRec1307[2];
	float fRec1305[2];
	float fRec1304[2];
	float fRec1302[2];
	float fRec1301[2];
	float fRec1299[2];
	float fRec1325[2];
	float fRec1323[2];
	float fRec1322[2];
	float fRec1320[2];
	float fRec1319[2];
	float fRec1317[2];
	float fRec1316[2];
	float fRec1314[2];
	float fRec1334[2];
	float fRec1332[2];
	float fRec1331[2];
	float fRec1329[2];
	float fRec1328[2];
	float fRec1326[2];
	float fRec1337[2];
	float fRec1335[2];
	float fRec1343[2];
	float fRec1341[2];
	float fRec1340[2];
	float fRec1338[2];
	float fVec22[2];
	float fRec1358[2];
	float fRec1356[2];
	float fRec1355[2];
	float fRec1353[2];
	float fRec1352[2];
	float fRec1350[2];
	float fRec1349[2];
	float fRec1347[2];
	float fRec1346[2];
	float fRec1344[2];
	float fRec1370[2];
	float fRec1368[2];
	float fRec1367[2];
	float fRec1365[2];
	float fRec1364[2];
	float fRec1362[2];
	float fRec1361[2];
	float fRec1359[2];
	float fRec1379[2];
	float fRec1377[2];
	float fRec1376[2];
	float fRec1374[2];
	float fRec1373[2];
	float fRec1371[2];
	float fRec1382[2];
	float fRec1380[2];
	float fRec1388[2];
	float fRec1386[2];
	float fRec1385[2];
	float fRec1383[2];
	float fRec1391[2];
	float fRec1389[2];
	float fRec1397[2];
	float fRec1395[2];
	float fRec1394[2];
	float fRec1392[2];
	float fRec1406[2];
	float fRec1404[2];
	float fRec1403[2];
	float fRec1401[2];
	float fRec1400[2];
	float fRec1398[2];
	float fRec1418[2];
	float fRec1416[2];
	float fRec1415[2];
	float fRec1413[2];
	float fRec1412[2];
	float fRec1410[2];
	float fRec1409[2];
	float fRec1407[2];
	float fRec1433[2];
	float fRec1431[2];
	float fRec1430[2];
	float fRec1428[2];
	float fRec1427[2];
	float fRec1425[2];
	float fRec1424[2];
	float fRec1422[2];
	float fRec1421[2];
	float fRec1419[2];
	
 public:
	
	void metadata(Meta* m) { 
		m->declare("author", "AmbisonicDecoderToolkit");
		m->declare("copyright", "(c) Aaron J. Heller 2013");
		m->declare("license", "BSD 3-Clause License");
		m->declare("name", "SATOswDo5");
		m->declare("version", "1.2");
	}

	virtual int getNumInputs() {
		return 36;
		
	}
	virtual int getNumOutputs() {
		return 31;
		
	}
	virtual int getInputRate(int channel) {
		int rate;
		switch (channel) {
			case 0: {
				rate = 1;
				break;
			}
			case 1: {
				rate = 1;
				break;
			}
			case 2: {
				rate = 1;
				break;
			}
			case 3: {
				rate = 1;
				break;
			}
			case 4: {
				rate = 1;
				break;
			}
			case 5: {
				rate = 1;
				break;
			}
			case 6: {
				rate = 1;
				break;
			}
			case 7: {
				rate = 1;
				break;
			}
			case 8: {
				rate = 1;
				break;
			}
			case 9: {
				rate = 1;
				break;
			}
			case 10: {
				rate = 1;
				break;
			}
			case 11: {
				rate = 1;
				break;
			}
			case 12: {
				rate = 1;
				break;
			}
			case 13: {
				rate = 1;
				break;
			}
			case 14: {
				rate = 1;
				break;
			}
			case 15: {
				rate = 1;
				break;
			}
			case 16: {
				rate = 1;
				break;
			}
			case 17: {
				rate = 1;
				break;
			}
			case 18: {
				rate = 1;
				break;
			}
			case 19: {
				rate = 1;
				break;
			}
			case 20: {
				rate = 1;
				break;
			}
			case 21: {
				rate = 1;
				break;
			}
			case 22: {
				rate = 1;
				break;
			}
			case 23: {
				rate = 1;
				break;
			}
			case 24: {
				rate = 1;
				break;
			}
			case 25: {
				rate = 1;
				break;
			}
			case 26: {
				rate = 1;
				break;
			}
			case 27: {
				rate = 1;
				break;
			}
			case 28: {
				rate = 1;
				break;
			}
			case 29: {
				rate = 1;
				break;
			}
			case 30: {
				rate = 1;
				break;
			}
			case 31: {
				rate = 1;
				break;
			}
			case 32: {
				rate = 1;
				break;
			}
			case 33: {
				rate = 1;
				break;
			}
			case 34: {
				rate = 1;
				break;
			}
			case 35: {
				rate = 1;
				break;
			}
			default: {
				rate = -1;
				break;
			}
			
		}
		return rate;
		
	}
	virtual int getOutputRate(int channel) {
		int rate;
		switch (channel) {
			case 0: {
				rate = 1;
				break;
			}
			case 1: {
				rate = 1;
				break;
			}
			case 2: {
				rate = 1;
				break;
			}
			case 3: {
				rate = 1;
				break;
			}
			case 4: {
				rate = 1;
				break;
			}
			case 5: {
				rate = 1;
				break;
			}
			case 6: {
				rate = 1;
				break;
			}
			case 7: {
				rate = 1;
				break;
			}
			case 8: {
				rate = 1;
				break;
			}
			case 9: {
				rate = 1;
				break;
			}
			case 10: {
				rate = 1;
				break;
			}
			case 11: {
				rate = 1;
				break;
			}
			case 12: {
				rate = 1;
				break;
			}
			case 13: {
				rate = 1;
				break;
			}
			case 14: {
				rate = 1;
				break;
			}
			case 15: {
				rate = 1;
				break;
			}
			case 16: {
				rate = 1;
				break;
			}
			case 17: {
				rate = 1;
				break;
			}
			case 18: {
				rate = 1;
				break;
			}
			case 19: {
				rate = 1;
				break;
			}
			case 20: {
				rate = 1;
				break;
			}
			case 21: {
				rate = 1;
				break;
			}
			case 22: {
				rate = 1;
				break;
			}
			case 23: {
				rate = 1;
				break;
			}
			case 24: {
				rate = 1;
				break;
			}
			case 25: {
				rate = 1;
				break;
			}
			case 26: {
				rate = 1;
				break;
			}
			case 27: {
				rate = 1;
				break;
			}
			case 28: {
				rate = 1;
				break;
			}
			case 29: {
				rate = 1;
				break;
			}
			case 30: {
				rate = 1;
				break;
			}
			default: {
				rate = -1;
				break;
			}
			
		}
		return rate;
		
	}
	
	static void classInit(int samplingFreq) {
		
	}
	
	virtual void instanceConstants(int samplingFreq) {
		fSamplingFreq = samplingFreq;
		iConst0 = min(192000, max(1, fSamplingFreq));
		fConst1 = float(iConst0);
		fConst2 = ((89.0828476f / fConst1) + 1.0f);
		fConst3 = (0.0f - (178.165695f / (fConst1 * fConst2)));
		fConst4 = mydsp_faustpower2_f(fConst1);
		fConst5 = ((((8516.83789f / fConst1) + 163.763763f) / fConst1) + 1.0f);
		fConst6 = (0.0f - (34067.3516f / (fConst4 * fConst5)));
		fConst7 = (0.0f - (((34067.3516f / fConst1) + 327.527527f) / (fConst1 * fConst5)));
		fConst8 = ((((10834.4443f / fConst1) + 113.574692f) / fConst1) + 1.0f);
		fConst9 = (1.0f / ((fConst2 * fConst5) * fConst8));
		fConst10 = (3.14159274f / float(iConst0));
		fConst11 = (0.0f - (43337.7773f / (fConst4 * fConst8)));
		fConst12 = (0.0f - (((43337.7773f / fConst1) + 227.149384f) / (fConst1 * fConst8)));
		fConst13 = ((((5454.20361f / fConst1) + 141.497772f) / fConst1) + 1.0f);
		fConst14 = (0.0f - (21816.8145f / (fConst4 * fConst13)));
		fConst15 = (0.0f - (((21816.8145f / fConst1) + 282.995544f) / (fConst1 * fConst13)));
		fConst16 = ((((6855.13184f / fConst1) + 102.783104f) / fConst1) + 1.0f);
		fConst17 = (1.0f / (fConst13 * fConst16));
		fConst18 = (0.0f - (27420.5273f / (fConst4 * fConst16)));
		fConst19 = (0.0f - (((27420.5273f / fConst1) + 205.566208f) / (fConst1 * fConst16)));
		fConst20 = ((56.7265472f / fConst1) + 1.0f);
		fConst21 = ((((3854.54663f / fConst1) + 89.84198f) / fConst1) + 1.0f);
		fConst22 = (1.0f / (fConst20 * fConst21));
		fConst23 = (0.0f - (15418.1865f / (fConst4 * fConst21)));
		fConst24 = (0.0f - (((15418.1865f / fConst1) + 179.68396f) / (fConst1 * fConst21)));
		fConst25 = (0.0f - (113.453094f / (fConst1 * fConst20)));
		fConst26 = ((24.4280872f / fConst1) + 1.0f);
		fConst27 = (1.0f / fConst26);
		fConst28 = (0.0f - (48.8561745f / (fConst1 * fConst26)));
		fConst29 = ((((1790.19434f / fConst1) + 73.2842636f) / fConst1) + 1.0f);
		fConst30 = (1.0f / fConst29);
		fConst31 = (0.0f - (7160.77734f / (fConst4 * fConst29)));
		fConst32 = (0.0f - (((7160.77734f / fConst1) + 146.568527f) / (fConst1 * fConst29)));
		iConst33 = int(((0.00671591423f * float(iConst0)) + 0.5f));
		fConst34 = ((82.7896576f / fConst1) + 1.0f);
		fConst35 = (0.0f - (165.579315f / (fConst1 * fConst34)));
		fConst36 = ((((7356.01074f / fConst1) + 152.194794f) / fConst1) + 1.0f);
		fConst37 = (0.0f - (29424.043f / (fConst4 * fConst36)));
		fConst38 = (0.0f - (((29424.043f / fConst1) + 304.389587f) / (fConst1 * fConst36)));
		fConst39 = ((((9357.73145f / fConst1) + 105.551292f) / fConst1) + 1.0f);
		fConst40 = (1.0f / ((fConst34 * fConst36) * fConst39));
		fConst41 = (0.0f - (37430.9258f / (fConst4 * fConst39)));
		fConst42 = (0.0f - (((37430.9258f / fConst1) + 211.102585f) / (fConst1 * fConst39)));
		fConst43 = ((((4710.80664f / fConst1) + 131.50177f) / fConst1) + 1.0f);
		fConst44 = ((((5920.7915f / fConst1) + 95.5220642f) / fConst1) + 1.0f);
		fConst45 = (1.0f / (fConst43 * fConst44));
		fConst46 = (0.0f - (((23683.166f / fConst1) + 191.044128f) / (fConst1 * fConst44)));
		fConst47 = (0.0f - (23683.166f / (fConst4 * fConst44)));
		fConst48 = (0.0f - (((18843.2266f / fConst1) + 263.00354f) / (fConst1 * fConst43)));
		fConst49 = (0.0f - (18843.2266f / (fConst4 * fConst43)));
		fConst50 = ((52.7191391f / fConst1) + 1.0f);
		fConst51 = (0.0f - (105.438278f / (fConst1 * fConst50)));
		fConst52 = ((((3329.17993f / fConst1) + 83.4951553f) / fConst1) + 1.0f);
		fConst53 = (1.0f / (fConst50 * fConst52));
		fConst54 = (0.0f - (13316.7197f / (fConst4 * fConst52)));
		fConst55 = (0.0f - (((13316.7197f / fConst1) + 166.990311f) / (fConst1 * fConst52)));
		fConst56 = ((22.702383f / fConst1) + 1.0f);
		fConst57 = (1.0f / fConst56);
		fConst58 = (0.0f - (45.4047661f / (fConst1 * fConst56)));
		fConst59 = ((((1546.19458f / fConst1) + 68.1071472f) / fConst1) + 1.0f);
		fConst60 = (1.0f / fConst59);
		fConst61 = (0.0f - (6184.77832f / (fConst4 * fConst59)));
		fConst62 = (0.0f - (((6184.77832f / fConst1) + 136.214294f) / (fConst1 * fConst59)));
		iConst63 = int(((0.0051600365f * float(iConst0)) + 0.5f));
		fConst64 = ((52.7330933f / fConst1) + 1.0f);
		fConst65 = (0.0f - (105.466187f / (fConst1 * fConst64)));
		fConst66 = ((((3330.94238f / fConst1) + 83.5172501f) / fConst1) + 1.0f);
		fConst67 = (1.0f / (fConst64 * fConst66));
		fConst68 = (0.0f - (13323.7695f / (fConst4 * fConst66)));
		fConst69 = (0.0f - (((13323.7695f / fConst1) + 167.0345f) / (fConst1 * fConst66)));
		fConst70 = ((((1547.01306f / fConst1) + 68.1251755f) / fConst1) + 1.0f);
		fConst71 = (1.0f / fConst70);
		fConst72 = (0.0f - (6188.05225f / (fConst70 * fConst4)));
		fConst73 = (0.0f - (((6188.05225f / fConst1) + 136.250351f) / (fConst70 * fConst1)));
		fConst74 = ((22.7083912f / fConst1) + 1.0f);
		fConst75 = (1.0f / fConst74);
		fConst76 = (0.0f - (45.4167824f / (fConst1 * fConst74)));
		fConst77 = ((((4713.30078f / fConst1) + 131.53656f) / fConst1) + 1.0f);
		fConst78 = (0.0f - (18853.2031f / (fConst4 * fConst77)));
		fConst79 = (0.0f - (((18853.2031f / fConst1) + 263.07312f) / (fConst1 * fConst77)));
		fConst80 = ((((5923.92578f / fConst1) + 95.547348f) / fConst1) + 1.0f);
		fConst81 = (1.0f / (fConst77 * fConst80));
		fConst82 = (0.0f - (23695.7031f / (fConst4 * fConst80)));
		fConst83 = (0.0f - (((23695.7031f / fConst1) + 191.094696f) / (fConst1 * fConst80)));
		fConst84 = ((82.8115692f / fConst1) + 1.0f);
		fConst85 = (0.0f - (165.623138f / (fConst1 * fConst84)));
		fConst86 = ((((7359.90479f / fConst1) + 152.235077f) / fConst1) + 1.0f);
		fConst87 = (0.0f - (29439.6191f / (fConst4 * fConst86)));
		fConst88 = (0.0f - (((29439.6191f / fConst1) + 304.470154f) / (fConst1 * fConst86)));
		fConst89 = ((((9362.68555f / fConst1) + 105.579224f) / fConst1) + 1.0f);
		fConst90 = (1.0f / ((fConst84 * fConst86) * fConst89));
		fConst91 = (0.0f - (37450.7422f / (fConst4 * fConst89)));
		fConst92 = (0.0f - (((37450.7422f / fConst1) + 211.158447f) / (fConst1 * fConst89)));
		iConst93 = int(((0.00516586378f * float(iConst0)) + 0.5f));
		fConst94 = ((20.9098701f / fConst1) + 1.0f);
		fConst95 = (1.0f / fConst94);
		fConst96 = (0.0f - (41.8197403f / (fConst1 * fConst94)));
		fConst97 = ((((1311.66809f / fConst1) + 62.7296143f) / fConst1) + 1.0f);
		fConst98 = (1.0f / fConst97);
		fConst99 = (0.0f - (5246.67236f / (fConst4 * fConst97)));
		fConst100 = (0.0f - (((5246.67236f / fConst1) + 125.459229f) / (fConst1 * fConst97)));
		fConst101 = ((48.5565948f / fConst1) + 1.0f);
		fConst102 = (0.0f - (97.1131897f / (fConst101 * fConst1)));
		fConst103 = ((((2824.21069f / fConst1) + 76.902626f) / fConst1) + 1.0f);
		fConst104 = (1.0f / (fConst101 * fConst103));
		fConst105 = (0.0f - (11296.8428f / (fConst103 * fConst4)));
		fConst106 = (0.0f - (((11296.8428f / fConst1) + 153.805252f) / (fConst103 * fConst1)));
		fConst107 = ((((3996.27271f / fConst1) + 121.118782f) / fConst1) + 1.0f);
		fConst108 = (0.0f - (15985.0908f / (fConst4 * fConst107)));
		fConst109 = (0.0f - (((15985.0908f / fConst1) + 242.237564f) / (fConst1 * fConst107)));
		fConst110 = ((((5022.72705f / fConst1) + 87.9799271f) / fConst1) + 1.0f);
		fConst111 = (1.0f / (fConst107 * fConst110));
		fConst112 = (0.0f - (20090.9082f / (fConst4 * fConst110)));
		fConst113 = (0.0f - (((20090.9082f / fConst1) + 175.959854f) / (fConst1 * fConst110)));
		fConst114 = ((76.2528305f / fConst1) + 1.0f);
		fConst115 = (0.0f - (152.505661f / (fConst1 * fConst114)));
		fConst116 = ((((6240.25244f / fConst1) + 140.177948f) / fConst1) + 1.0f);
		fConst117 = (0.0f - (24961.0098f / (fConst4 * fConst116)));
		fConst118 = (0.0f - (((24961.0098f / fConst1) + 280.355896f) / (fConst1 * fConst116)));
		fConst119 = ((((7938.35352f / fConst1) + 97.2172775f) / fConst1) + 1.0f);
		fConst120 = (1.0f / ((fConst114 * fConst116) * fConst119));
		fConst121 = (0.0f - (31753.4141f / (fConst4 * fConst119)));
		fConst122 = (0.0f - (((31753.4141f / fConst1) + 194.434555f) / (fConst1 * fConst119)));
		iConst123 = int(((0.00327200512f * float(iConst0)) + 0.5f));
		fConst124 = ((48.5270309f / fConst1) + 1.0f);
		fConst125 = (0.0f - (97.0540619f / (fConst1 * fConst124)));
		fConst126 = ((((2820.77246f / fConst1) + 76.8558044f) / fConst1) + 1.0f);
		fConst127 = (1.0f / (fConst124 * fConst126));
		fConst128 = (0.0f - (11283.0898f / (fConst4 * fConst126)));
		fConst129 = (0.0f - (((11283.0898f / fConst1) + 153.711609f) / (fConst1 * fConst126)));
		fConst130 = ((((3991.40796f / fConst1) + 121.045036f) / fConst1) + 1.0f);
		fConst131 = (0.0f - (15965.6318f / (fConst4 * fConst130)));
		fConst132 = (0.0f - (((15965.6318f / fConst1) + 242.090073f) / (fConst1 * fConst130)));
		fConst133 = ((((5016.61279f / fConst1) + 87.9263611f) / fConst1) + 1.0f);
		fConst134 = (1.0f / (fConst130 * fConst133));
		fConst135 = (0.0f - (20066.4512f / (fConst4 * fConst133)));
		fConst136 = (0.0f - (((20066.4512f / fConst1) + 175.852722f) / (fConst1 * fConst133)));
		fConst137 = ((20.8971405f / fConst1) + 1.0f);
		fConst138 = (1.0f / fConst137);
		fConst139 = (0.0f - (41.794281f / (fConst1 * fConst137)));
		fConst140 = ((((1310.07129f / fConst1) + 62.6914177f) / fConst1) + 1.0f);
		fConst141 = (1.0f / fConst140);
		fConst142 = (0.0f - (5240.28516f / (fConst4 * fConst140)));
		fConst143 = (0.0f - (((5240.28516f / fConst1) + 125.382835f) / (fConst1 * fConst140)));
		fConst144 = ((76.2064056f / fConst1) + 1.0f);
		fConst145 = (0.0f - (152.412811f / (fConst1 * fConst144)));
		fConst146 = ((((6232.65576f / fConst1) + 140.092606f) / fConst1) + 1.0f);
		fConst147 = (0.0f - (24930.623f / (fConst4 * fConst146)));
		fConst148 = (0.0f - (((24930.623f / fConst1) + 280.185211f) / (fConst1 * fConst146)));
		fConst149 = ((((7928.68945f / fConst1) + 97.1580887f) / fConst1) + 1.0f);
		fConst150 = (1.0f / ((fConst144 * fConst146) * fConst149));
		fConst151 = (0.0f - (31714.7578f / (fConst4 * fConst149)));
		fConst152 = (0.0f - (((31714.7578f / fConst1) + 194.316177f) / (fConst1 * fConst149)));
		iConst153 = int(((0.00325743691f * float(iConst0)) + 0.5f));
		fConst154 = ((((3092.81396f / fConst1) + 106.551811f) / fConst1) + 1.0f);
		fConst155 = (0.0f - (12371.2559f / (fConst4 * fConst154)));
		fConst156 = (0.0f - (((12371.2559f / fConst1) + 213.103622f) / (fConst1 * fConst154)));
		fConst157 = ((((3887.2124f / fConst1) + 77.3985748f) / fConst1) + 1.0f);
		fConst158 = (1.0f / (fConst154 * fConst157));
		fConst159 = (0.0f - (15548.8496f / (fConst4 * fConst157)));
		fConst160 = (0.0f - (((15548.8496f / fConst1) + 154.79715f) / (fConst1 * fConst157)));
		fConst161 = ((42.7166901f / fConst1) + 1.0f);
		fConst162 = (0.0f - (85.4333801f / (fConst1 * fConst161)));
		fConst163 = ((((2185.72632f / fConst1) + 67.6535416f) / fConst1) + 1.0f);
		fConst164 = (1.0f / (fConst161 * fConst163));
		fConst165 = (0.0f - (8742.90527f / (fConst4 * fConst163)));
		fConst166 = (0.0f - (((8742.90527f / fConst1) + 135.307083f) / (fConst1 * fConst163)));
		fConst167 = ((((1015.13226f / fConst1) + 55.1851158f) / fConst1) + 1.0f);
		fConst168 = (1.0f / fConst167);
		fConst169 = (0.0f - (4060.52905f / (fConst4 * fConst167)));
		fConst170 = (0.0f - (((4060.52905f / fConst1) + 110.370232f) / (fConst1 * fConst167)));
		fConst171 = ((67.0818939f / fConst1) + 1.0f);
		fConst172 = (0.0f - (134.163788f / (fConst1 * fConst171)));
		fConst173 = ((((4829.48535f / fConst1) + 123.318733f) / fConst1) + 1.0f);
		fConst174 = (0.0f - (19317.9414f / (fConst4 * fConst173)));
		fConst175 = (0.0f - (((19317.9414f / fConst1) + 246.637466f) / (fConst1 * fConst173)));
		fConst176 = ((((6143.6875f / fConst1) + 85.5249481f) / fConst1) + 1.0f);
		fConst177 = (1.0f / ((fConst171 * fConst173) * fConst176));
		fConst178 = (0.0f - (24574.75f / (fConst4 * fConst176)));
		fConst179 = (0.0f - (((24574.75f / fConst1) + 171.049896f) / (fConst1 * fConst176)));
		fConst180 = ((18.3950386f / fConst1) + 1.0f);
		fConst181 = (1.0f / fConst180);
		fConst182 = (0.0f - (36.7900772f / (fConst1 * fConst180)));
		iConst183 = int(((2.9136288e-06f * float(iConst0)) + 0.5f));
		fConst184 = ((67.074707f / fConst1) + 1.0f);
		fConst185 = (0.0f - (134.149414f / (fConst1 * fConst184)));
		fConst186 = ((((4828.4502f / fConst1) + 123.305511f) / fConst1) + 1.0f);
		fConst187 = (0.0f - (19313.8008f / (fConst4 * fConst186)));
		fConst188 = (0.0f - (((19313.8008f / fConst1) + 246.611023f) / (fConst1 * fConst186)));
		fConst189 = ((((6142.37061f / fConst1) + 85.5157776f) / fConst1) + 1.0f);
		fConst190 = (1.0f / ((fConst184 * fConst186) * fConst189));
		fConst191 = (0.0f - (24569.4824f / (fConst4 * fConst189)));
		fConst192 = (0.0f - (((24569.4824f / fConst1) + 171.031555f) / (fConst1 * fConst189)));
		fConst193 = ((((3092.15112f / fConst1) + 106.54039f) / fConst1) + 1.0f);
		fConst194 = (0.0f - (12368.6045f / (fConst4 * fConst193)));
		fConst195 = (0.0f - (((12368.6045f / fConst1) + 213.08078f) / (fConst1 * fConst193)));
		fConst196 = ((((3886.37915f / fConst1) + 77.390274f) / fConst1) + 1.0f);
		fConst197 = (1.0f / (fConst193 * fConst196));
		fConst198 = (0.0f - (15545.5166f / (fConst4 * fConst196)));
		fConst199 = (0.0f - (((15545.5166f / fConst1) + 154.780548f) / (fConst1 * fConst196)));
		fConst200 = ((42.7121086f / fConst1) + 1.0f);
		fConst201 = (0.0f - (85.4242172f / (fConst1 * fConst200)));
		fConst202 = ((((2185.25781f / fConst1) + 67.646286f) / fConst1) + 1.0f);
		fConst203 = (1.0f / (fConst200 * fConst202));
		fConst204 = (0.0f - (8741.03125f / (fConst4 * fConst202)));
		fConst205 = (0.0f - (((8741.03125f / fConst1) + 135.292572f) / (fConst1 * fConst202)));
		fConst206 = ((18.3930664f / fConst1) + 1.0f);
		fConst207 = (1.0f / fConst206);
		fConst208 = (0.0f - (36.7861328f / (fConst1 * fConst206)));
		fConst209 = ((((1014.91467f / fConst1) + 55.1791992f) / fConst1) + 1.0f);
		fConst210 = (1.0f / fConst209);
		fConst211 = (0.0f - (4059.65869f / (fConst4 * fConst209)));
		fConst212 = (0.0f - (((4059.65869f / fConst1) + 110.358398f) / (fConst1 * fConst209)));
		
	}
	
	virtual void instanceResetUserInterface() {
		fCheckbox0 = FAUSTFLOAT(0.0f);
		fHslider0 = FAUSTFLOAT(-10.0f);
		fHslider1 = FAUSTFLOAT(400.0f);
		fHslider2 = FAUSTFLOAT(0.0f);
		
	}
	
	virtual void instanceClear() {
		for (int l0 = 0; (l0 < 2); l0 = (l0 + 1)) {
			fRec0[l0] = 0.0f;
			
		}
		for (int l1 = 0; (l1 < 2); l1 = (l1 + 1)) {
			fRec17[l1] = 0.0f;
			
		}
		for (int l2 = 0; (l2 < 3); l2 = (l2 + 1)) {
			fRec16[l2] = 0.0f;
			
		}
		for (int l3 = 0; (l3 < 2); l3 = (l3 + 1)) {
			fRec18[l3] = 0.0f;
			
		}
		for (int l4 = 0; (l4 < 3); l4 = (l4 + 1)) {
			fRec19[l4] = 0.0f;
			
		}
		for (int l5 = 0; (l5 < 3); l5 = (l5 + 1)) {
			fRec20[l5] = 0.0f;
			
		}
		for (int l6 = 0; (l6 < 3); l6 = (l6 + 1)) {
			fRec21[l6] = 0.0f;
			
		}
		for (int l7 = 0; (l7 < 3); l7 = (l7 + 1)) {
			fRec22[l7] = 0.0f;
			
		}
		for (int l8 = 0; (l8 < 3); l8 = (l8 + 1)) {
			fRec23[l8] = 0.0f;
			
		}
		for (int l9 = 0; (l9 < 3); l9 = (l9 + 1)) {
			fRec24[l9] = 0.0f;
			
		}
		for (int l10 = 0; (l10 < 3); l10 = (l10 + 1)) {
			fRec25[l10] = 0.0f;
			
		}
		for (int l11 = 0; (l11 < 3); l11 = (l11 + 1)) {
			fRec26[l11] = 0.0f;
			
		}
		for (int l12 = 0; (l12 < 3); l12 = (l12 + 1)) {
			fRec27[l12] = 0.0f;
			
		}
		for (int l13 = 0; (l13 < 3); l13 = (l13 + 1)) {
			fRec28[l13] = 0.0f;
			
		}
		for (int l14 = 0; (l14 < 2); l14 = (l14 + 1)) {
			fRec15[l14] = 0.0f;
			
		}
		for (int l15 = 0; (l15 < 2); l15 = (l15 + 1)) {
			fRec13[l15] = 0.0f;
			
		}
		for (int l16 = 0; (l16 < 2); l16 = (l16 + 1)) {
			fRec12[l16] = 0.0f;
			
		}
		for (int l17 = 0; (l17 < 2); l17 = (l17 + 1)) {
			fRec10[l17] = 0.0f;
			
		}
		for (int l18 = 0; (l18 < 2); l18 = (l18 + 1)) {
			fRec9[l18] = 0.0f;
			
		}
		for (int l19 = 0; (l19 < 2); l19 = (l19 + 1)) {
			fRec7[l19] = 0.0f;
			
		}
		for (int l20 = 0; (l20 < 2); l20 = (l20 + 1)) {
			fRec6[l20] = 0.0f;
			
		}
		for (int l21 = 0; (l21 < 2); l21 = (l21 + 1)) {
			fRec4[l21] = 0.0f;
			
		}
		for (int l22 = 0; (l22 < 2); l22 = (l22 + 1)) {
			fRec3[l22] = 0.0f;
			
		}
		for (int l23 = 0; (l23 < 2); l23 = (l23 + 1)) {
			fRec1[l23] = 0.0f;
			
		}
		for (int l24 = 0; (l24 < 3); l24 = (l24 + 1)) {
			fRec41[l24] = 0.0f;
			
		}
		for (int l25 = 0; (l25 < 3); l25 = (l25 + 1)) {
			fRec42[l25] = 0.0f;
			
		}
		for (int l26 = 0; (l26 < 3); l26 = (l26 + 1)) {
			fRec43[l26] = 0.0f;
			
		}
		for (int l27 = 0; (l27 < 3); l27 = (l27 + 1)) {
			fRec44[l27] = 0.0f;
			
		}
		for (int l28 = 0; (l28 < 3); l28 = (l28 + 1)) {
			fRec45[l28] = 0.0f;
			
		}
		for (int l29 = 0; (l29 < 3); l29 = (l29 + 1)) {
			fRec46[l29] = 0.0f;
			
		}
		for (int l30 = 0; (l30 < 3); l30 = (l30 + 1)) {
			fRec47[l30] = 0.0f;
			
		}
		for (int l31 = 0; (l31 < 3); l31 = (l31 + 1)) {
			fRec48[l31] = 0.0f;
			
		}
		for (int l32 = 0; (l32 < 3); l32 = (l32 + 1)) {
			fRec49[l32] = 0.0f;
			
		}
		for (int l33 = 0; (l33 < 2); l33 = (l33 + 1)) {
			fRec40[l33] = 0.0f;
			
		}
		for (int l34 = 0; (l34 < 2); l34 = (l34 + 1)) {
			fRec38[l34] = 0.0f;
			
		}
		for (int l35 = 0; (l35 < 2); l35 = (l35 + 1)) {
			fRec37[l35] = 0.0f;
			
		}
		for (int l36 = 0; (l36 < 2); l36 = (l36 + 1)) {
			fRec35[l36] = 0.0f;
			
		}
		for (int l37 = 0; (l37 < 2); l37 = (l37 + 1)) {
			fRec34[l37] = 0.0f;
			
		}
		for (int l38 = 0; (l38 < 2); l38 = (l38 + 1)) {
			fRec32[l38] = 0.0f;
			
		}
		for (int l39 = 0; (l39 < 2); l39 = (l39 + 1)) {
			fRec31[l39] = 0.0f;
			
		}
		for (int l40 = 0; (l40 < 2); l40 = (l40 + 1)) {
			fRec29[l40] = 0.0f;
			
		}
		for (int l41 = 0; (l41 < 3); l41 = (l41 + 1)) {
			fRec59[l41] = 0.0f;
			
		}
		for (int l42 = 0; (l42 < 3); l42 = (l42 + 1)) {
			fRec60[l42] = 0.0f;
			
		}
		for (int l43 = 0; (l43 < 3); l43 = (l43 + 1)) {
			fRec61[l43] = 0.0f;
			
		}
		for (int l44 = 0; (l44 < 3); l44 = (l44 + 1)) {
			fRec62[l44] = 0.0f;
			
		}
		for (int l45 = 0; (l45 < 3); l45 = (l45 + 1)) {
			fRec63[l45] = 0.0f;
			
		}
		for (int l46 = 0; (l46 < 3); l46 = (l46 + 1)) {
			fRec64[l46] = 0.0f;
			
		}
		for (int l47 = 0; (l47 < 3); l47 = (l47 + 1)) {
			fRec65[l47] = 0.0f;
			
		}
		for (int l48 = 0; (l48 < 2); l48 = (l48 + 1)) {
			fRec58[l48] = 0.0f;
			
		}
		for (int l49 = 0; (l49 < 2); l49 = (l49 + 1)) {
			fRec56[l49] = 0.0f;
			
		}
		for (int l50 = 0; (l50 < 2); l50 = (l50 + 1)) {
			fRec55[l50] = 0.0f;
			
		}
		for (int l51 = 0; (l51 < 2); l51 = (l51 + 1)) {
			fRec53[l51] = 0.0f;
			
		}
		for (int l52 = 0; (l52 < 2); l52 = (l52 + 1)) {
			fRec52[l52] = 0.0f;
			
		}
		for (int l53 = 0; (l53 < 2); l53 = (l53 + 1)) {
			fRec50[l53] = 0.0f;
			
		}
		for (int l54 = 0; (l54 < 3); l54 = (l54 + 1)) {
			fRec69[l54] = 0.0f;
			
		}
		for (int l55 = 0; (l55 < 3); l55 = (l55 + 1)) {
			fRec70[l55] = 0.0f;
			
		}
		for (int l56 = 0; (l56 < 3); l56 = (l56 + 1)) {
			fRec71[l56] = 0.0f;
			
		}
		for (int l57 = 0; (l57 < 2); l57 = (l57 + 1)) {
			fRec68[l57] = 0.0f;
			
		}
		for (int l58 = 0; (l58 < 2); l58 = (l58 + 1)) {
			fRec66[l58] = 0.0f;
			
		}
		for (int l59 = 0; (l59 < 3); l59 = (l59 + 1)) {
			fRec78[l59] = 0.0f;
			
		}
		for (int l60 = 0; (l60 < 3); l60 = (l60 + 1)) {
			fRec79[l60] = 0.0f;
			
		}
		for (int l61 = 0; (l61 < 3); l61 = (l61 + 1)) {
			fRec80[l61] = 0.0f;
			
		}
		for (int l62 = 0; (l62 < 3); l62 = (l62 + 1)) {
			fRec81[l62] = 0.0f;
			
		}
		for (int l63 = 0; (l63 < 3); l63 = (l63 + 1)) {
			fRec82[l63] = 0.0f;
			
		}
		for (int l64 = 0; (l64 < 2); l64 = (l64 + 1)) {
			fRec77[l64] = 0.0f;
			
		}
		for (int l65 = 0; (l65 < 2); l65 = (l65 + 1)) {
			fRec75[l65] = 0.0f;
			
		}
		for (int l66 = 0; (l66 < 2); l66 = (l66 + 1)) {
			fRec74[l66] = 0.0f;
			
		}
		for (int l67 = 0; (l67 < 2); l67 = (l67 + 1)) {
			fRec72[l67] = 0.0f;
			
		}
		for (int l68 = 0; (l68 < 3); l68 = (l68 + 1)) {
			fRec83[l68] = 0.0f;
			
		}
		IOTA = 0;
		for (int l69 = 0; (l69 < 2048); l69 = (l69 + 1)) {
			fVec0[l69] = 0.0f;
			
		}
		for (int l70 = 0; (l70 < 2); l70 = (l70 + 1)) {
			fRec98[l70] = 0.0f;
			
		}
		for (int l71 = 0; (l71 < 2); l71 = (l71 + 1)) {
			fRec96[l71] = 0.0f;
			
		}
		for (int l72 = 0; (l72 < 2); l72 = (l72 + 1)) {
			fRec95[l72] = 0.0f;
			
		}
		for (int l73 = 0; (l73 < 2); l73 = (l73 + 1)) {
			fRec93[l73] = 0.0f;
			
		}
		for (int l74 = 0; (l74 < 2); l74 = (l74 + 1)) {
			fRec92[l74] = 0.0f;
			
		}
		for (int l75 = 0; (l75 < 2); l75 = (l75 + 1)) {
			fRec90[l75] = 0.0f;
			
		}
		for (int l76 = 0; (l76 < 2); l76 = (l76 + 1)) {
			fRec89[l76] = 0.0f;
			
		}
		for (int l77 = 0; (l77 < 2); l77 = (l77 + 1)) {
			fRec87[l77] = 0.0f;
			
		}
		for (int l78 = 0; (l78 < 2); l78 = (l78 + 1)) {
			fRec86[l78] = 0.0f;
			
		}
		for (int l79 = 0; (l79 < 2); l79 = (l79 + 1)) {
			fRec84[l79] = 0.0f;
			
		}
		for (int l80 = 0; (l80 < 2); l80 = (l80 + 1)) {
			fRec110[l80] = 0.0f;
			
		}
		for (int l81 = 0; (l81 < 2); l81 = (l81 + 1)) {
			fRec108[l81] = 0.0f;
			
		}
		for (int l82 = 0; (l82 < 2); l82 = (l82 + 1)) {
			fRec107[l82] = 0.0f;
			
		}
		for (int l83 = 0; (l83 < 2); l83 = (l83 + 1)) {
			fRec105[l83] = 0.0f;
			
		}
		for (int l84 = 0; (l84 < 2); l84 = (l84 + 1)) {
			fRec104[l84] = 0.0f;
			
		}
		for (int l85 = 0; (l85 < 2); l85 = (l85 + 1)) {
			fRec102[l85] = 0.0f;
			
		}
		for (int l86 = 0; (l86 < 2); l86 = (l86 + 1)) {
			fRec101[l86] = 0.0f;
			
		}
		for (int l87 = 0; (l87 < 2); l87 = (l87 + 1)) {
			fRec99[l87] = 0.0f;
			
		}
		for (int l88 = 0; (l88 < 2); l88 = (l88 + 1)) {
			fRec119[l88] = 0.0f;
			
		}
		for (int l89 = 0; (l89 < 2); l89 = (l89 + 1)) {
			fRec117[l89] = 0.0f;
			
		}
		for (int l90 = 0; (l90 < 2); l90 = (l90 + 1)) {
			fRec116[l90] = 0.0f;
			
		}
		for (int l91 = 0; (l91 < 2); l91 = (l91 + 1)) {
			fRec114[l91] = 0.0f;
			
		}
		for (int l92 = 0; (l92 < 2); l92 = (l92 + 1)) {
			fRec113[l92] = 0.0f;
			
		}
		for (int l93 = 0; (l93 < 2); l93 = (l93 + 1)) {
			fRec111[l93] = 0.0f;
			
		}
		for (int l94 = 0; (l94 < 2); l94 = (l94 + 1)) {
			fRec122[l94] = 0.0f;
			
		}
		for (int l95 = 0; (l95 < 2); l95 = (l95 + 1)) {
			fRec120[l95] = 0.0f;
			
		}
		for (int l96 = 0; (l96 < 2); l96 = (l96 + 1)) {
			fRec128[l96] = 0.0f;
			
		}
		for (int l97 = 0; (l97 < 2); l97 = (l97 + 1)) {
			fRec126[l97] = 0.0f;
			
		}
		for (int l98 = 0; (l98 < 2); l98 = (l98 + 1)) {
			fRec125[l98] = 0.0f;
			
		}
		for (int l99 = 0; (l99 < 2); l99 = (l99 + 1)) {
			fRec123[l99] = 0.0f;
			
		}
		for (int l100 = 0; (l100 < 1024); l100 = (l100 + 1)) {
			fVec1[l100] = 0.0f;
			
		}
		for (int l101 = 0; (l101 < 2); l101 = (l101 + 1)) {
			fRec137[l101] = 0.0f;
			
		}
		for (int l102 = 0; (l102 < 2); l102 = (l102 + 1)) {
			fRec135[l102] = 0.0f;
			
		}
		for (int l103 = 0; (l103 < 2); l103 = (l103 + 1)) {
			fRec134[l103] = 0.0f;
			
		}
		for (int l104 = 0; (l104 < 2); l104 = (l104 + 1)) {
			fRec132[l104] = 0.0f;
			
		}
		for (int l105 = 0; (l105 < 2); l105 = (l105 + 1)) {
			fRec131[l105] = 0.0f;
			
		}
		for (int l106 = 0; (l106 < 2); l106 = (l106 + 1)) {
			fRec129[l106] = 0.0f;
			
		}
		for (int l107 = 0; (l107 < 2); l107 = (l107 + 1)) {
			fRec143[l107] = 0.0f;
			
		}
		for (int l108 = 0; (l108 < 2); l108 = (l108 + 1)) {
			fRec141[l108] = 0.0f;
			
		}
		for (int l109 = 0; (l109 < 2); l109 = (l109 + 1)) {
			fRec140[l109] = 0.0f;
			
		}
		for (int l110 = 0; (l110 < 2); l110 = (l110 + 1)) {
			fRec138[l110] = 0.0f;
			
		}
		for (int l111 = 0; (l111 < 2); l111 = (l111 + 1)) {
			fRec146[l111] = 0.0f;
			
		}
		for (int l112 = 0; (l112 < 2); l112 = (l112 + 1)) {
			fRec144[l112] = 0.0f;
			
		}
		for (int l113 = 0; (l113 < 2); l113 = (l113 + 1)) {
			fRec158[l113] = 0.0f;
			
		}
		for (int l114 = 0; (l114 < 2); l114 = (l114 + 1)) {
			fRec156[l114] = 0.0f;
			
		}
		for (int l115 = 0; (l115 < 2); l115 = (l115 + 1)) {
			fRec155[l115] = 0.0f;
			
		}
		for (int l116 = 0; (l116 < 2); l116 = (l116 + 1)) {
			fRec153[l116] = 0.0f;
			
		}
		for (int l117 = 0; (l117 < 2); l117 = (l117 + 1)) {
			fRec152[l117] = 0.0f;
			
		}
		for (int l118 = 0; (l118 < 2); l118 = (l118 + 1)) {
			fRec150[l118] = 0.0f;
			
		}
		for (int l119 = 0; (l119 < 2); l119 = (l119 + 1)) {
			fRec149[l119] = 0.0f;
			
		}
		for (int l120 = 0; (l120 < 2); l120 = (l120 + 1)) {
			fRec147[l120] = 0.0f;
			
		}
		for (int l121 = 0; (l121 < 2); l121 = (l121 + 1)) {
			fRec173[l121] = 0.0f;
			
		}
		for (int l122 = 0; (l122 < 2); l122 = (l122 + 1)) {
			fRec171[l122] = 0.0f;
			
		}
		for (int l123 = 0; (l123 < 2); l123 = (l123 + 1)) {
			fRec170[l123] = 0.0f;
			
		}
		for (int l124 = 0; (l124 < 2); l124 = (l124 + 1)) {
			fRec168[l124] = 0.0f;
			
		}
		for (int l125 = 0; (l125 < 2); l125 = (l125 + 1)) {
			fRec167[l125] = 0.0f;
			
		}
		for (int l126 = 0; (l126 < 2); l126 = (l126 + 1)) {
			fRec165[l126] = 0.0f;
			
		}
		for (int l127 = 0; (l127 < 2); l127 = (l127 + 1)) {
			fRec164[l127] = 0.0f;
			
		}
		for (int l128 = 0; (l128 < 2); l128 = (l128 + 1)) {
			fRec162[l128] = 0.0f;
			
		}
		for (int l129 = 0; (l129 < 2); l129 = (l129 + 1)) {
			fRec161[l129] = 0.0f;
			
		}
		for (int l130 = 0; (l130 < 2); l130 = (l130 + 1)) {
			fRec159[l130] = 0.0f;
			
		}
		for (int l131 = 0; (l131 < 1024); l131 = (l131 + 1)) {
			fVec2[l131] = 0.0f;
			
		}
		for (int l132 = 0; (l132 < 2); l132 = (l132 + 1)) {
			fRec188[l132] = 0.0f;
			
		}
		for (int l133 = 0; (l133 < 2); l133 = (l133 + 1)) {
			fRec186[l133] = 0.0f;
			
		}
		for (int l134 = 0; (l134 < 2); l134 = (l134 + 1)) {
			fRec185[l134] = 0.0f;
			
		}
		for (int l135 = 0; (l135 < 2); l135 = (l135 + 1)) {
			fRec183[l135] = 0.0f;
			
		}
		for (int l136 = 0; (l136 < 2); l136 = (l136 + 1)) {
			fRec182[l136] = 0.0f;
			
		}
		for (int l137 = 0; (l137 < 2); l137 = (l137 + 1)) {
			fRec180[l137] = 0.0f;
			
		}
		for (int l138 = 0; (l138 < 2); l138 = (l138 + 1)) {
			fRec179[l138] = 0.0f;
			
		}
		for (int l139 = 0; (l139 < 2); l139 = (l139 + 1)) {
			fRec177[l139] = 0.0f;
			
		}
		for (int l140 = 0; (l140 < 2); l140 = (l140 + 1)) {
			fRec176[l140] = 0.0f;
			
		}
		for (int l141 = 0; (l141 < 2); l141 = (l141 + 1)) {
			fRec174[l141] = 0.0f;
			
		}
		for (int l142 = 0; (l142 < 2); l142 = (l142 + 1)) {
			fRec200[l142] = 0.0f;
			
		}
		for (int l143 = 0; (l143 < 2); l143 = (l143 + 1)) {
			fRec198[l143] = 0.0f;
			
		}
		for (int l144 = 0; (l144 < 2); l144 = (l144 + 1)) {
			fRec197[l144] = 0.0f;
			
		}
		for (int l145 = 0; (l145 < 2); l145 = (l145 + 1)) {
			fRec195[l145] = 0.0f;
			
		}
		for (int l146 = 0; (l146 < 2); l146 = (l146 + 1)) {
			fRec194[l146] = 0.0f;
			
		}
		for (int l147 = 0; (l147 < 2); l147 = (l147 + 1)) {
			fRec192[l147] = 0.0f;
			
		}
		for (int l148 = 0; (l148 < 2); l148 = (l148 + 1)) {
			fRec191[l148] = 0.0f;
			
		}
		for (int l149 = 0; (l149 < 2); l149 = (l149 + 1)) {
			fRec189[l149] = 0.0f;
			
		}
		for (int l150 = 0; (l150 < 2); l150 = (l150 + 1)) {
			fRec209[l150] = 0.0f;
			
		}
		for (int l151 = 0; (l151 < 2); l151 = (l151 + 1)) {
			fRec207[l151] = 0.0f;
			
		}
		for (int l152 = 0; (l152 < 2); l152 = (l152 + 1)) {
			fRec206[l152] = 0.0f;
			
		}
		for (int l153 = 0; (l153 < 2); l153 = (l153 + 1)) {
			fRec204[l153] = 0.0f;
			
		}
		for (int l154 = 0; (l154 < 2); l154 = (l154 + 1)) {
			fRec203[l154] = 0.0f;
			
		}
		for (int l155 = 0; (l155 < 2); l155 = (l155 + 1)) {
			fRec201[l155] = 0.0f;
			
		}
		for (int l156 = 0; (l156 < 2); l156 = (l156 + 1)) {
			fRec212[l156] = 0.0f;
			
		}
		for (int l157 = 0; (l157 < 2); l157 = (l157 + 1)) {
			fRec210[l157] = 0.0f;
			
		}
		for (int l158 = 0; (l158 < 2); l158 = (l158 + 1)) {
			fRec218[l158] = 0.0f;
			
		}
		for (int l159 = 0; (l159 < 2); l159 = (l159 + 1)) {
			fRec216[l159] = 0.0f;
			
		}
		for (int l160 = 0; (l160 < 2); l160 = (l160 + 1)) {
			fRec215[l160] = 0.0f;
			
		}
		for (int l161 = 0; (l161 < 2); l161 = (l161 + 1)) {
			fRec213[l161] = 0.0f;
			
		}
		for (int l162 = 0; (l162 < 1024); l162 = (l162 + 1)) {
			fVec3[l162] = 0.0f;
			
		}
		for (int l163 = 0; (l163 < 2); l163 = (l163 + 1)) {
			fRec233[l163] = 0.0f;
			
		}
		for (int l164 = 0; (l164 < 2); l164 = (l164 + 1)) {
			fRec231[l164] = 0.0f;
			
		}
		for (int l165 = 0; (l165 < 2); l165 = (l165 + 1)) {
			fRec230[l165] = 0.0f;
			
		}
		for (int l166 = 0; (l166 < 2); l166 = (l166 + 1)) {
			fRec228[l166] = 0.0f;
			
		}
		for (int l167 = 0; (l167 < 2); l167 = (l167 + 1)) {
			fRec227[l167] = 0.0f;
			
		}
		for (int l168 = 0; (l168 < 2); l168 = (l168 + 1)) {
			fRec225[l168] = 0.0f;
			
		}
		for (int l169 = 0; (l169 < 2); l169 = (l169 + 1)) {
			fRec224[l169] = 0.0f;
			
		}
		for (int l170 = 0; (l170 < 2); l170 = (l170 + 1)) {
			fRec222[l170] = 0.0f;
			
		}
		for (int l171 = 0; (l171 < 2); l171 = (l171 + 1)) {
			fRec221[l171] = 0.0f;
			
		}
		for (int l172 = 0; (l172 < 2); l172 = (l172 + 1)) {
			fRec219[l172] = 0.0f;
			
		}
		for (int l173 = 0; (l173 < 2); l173 = (l173 + 1)) {
			fRec245[l173] = 0.0f;
			
		}
		for (int l174 = 0; (l174 < 2); l174 = (l174 + 1)) {
			fRec243[l174] = 0.0f;
			
		}
		for (int l175 = 0; (l175 < 2); l175 = (l175 + 1)) {
			fRec242[l175] = 0.0f;
			
		}
		for (int l176 = 0; (l176 < 2); l176 = (l176 + 1)) {
			fRec240[l176] = 0.0f;
			
		}
		for (int l177 = 0; (l177 < 2); l177 = (l177 + 1)) {
			fRec239[l177] = 0.0f;
			
		}
		for (int l178 = 0; (l178 < 2); l178 = (l178 + 1)) {
			fRec237[l178] = 0.0f;
			
		}
		for (int l179 = 0; (l179 < 2); l179 = (l179 + 1)) {
			fRec236[l179] = 0.0f;
			
		}
		for (int l180 = 0; (l180 < 2); l180 = (l180 + 1)) {
			fRec234[l180] = 0.0f;
			
		}
		for (int l181 = 0; (l181 < 2); l181 = (l181 + 1)) {
			fRec254[l181] = 0.0f;
			
		}
		for (int l182 = 0; (l182 < 2); l182 = (l182 + 1)) {
			fRec252[l182] = 0.0f;
			
		}
		for (int l183 = 0; (l183 < 2); l183 = (l183 + 1)) {
			fRec251[l183] = 0.0f;
			
		}
		for (int l184 = 0; (l184 < 2); l184 = (l184 + 1)) {
			fRec249[l184] = 0.0f;
			
		}
		for (int l185 = 0; (l185 < 2); l185 = (l185 + 1)) {
			fRec248[l185] = 0.0f;
			
		}
		for (int l186 = 0; (l186 < 2); l186 = (l186 + 1)) {
			fRec246[l186] = 0.0f;
			
		}
		for (int l187 = 0; (l187 < 2); l187 = (l187 + 1)) {
			fRec257[l187] = 0.0f;
			
		}
		for (int l188 = 0; (l188 < 2); l188 = (l188 + 1)) {
			fRec255[l188] = 0.0f;
			
		}
		for (int l189 = 0; (l189 < 2); l189 = (l189 + 1)) {
			fRec263[l189] = 0.0f;
			
		}
		for (int l190 = 0; (l190 < 2); l190 = (l190 + 1)) {
			fRec261[l190] = 0.0f;
			
		}
		for (int l191 = 0; (l191 < 2); l191 = (l191 + 1)) {
			fRec260[l191] = 0.0f;
			
		}
		for (int l192 = 0; (l192 < 2); l192 = (l192 + 1)) {
			fRec258[l192] = 0.0f;
			
		}
		for (int l193 = 0; (l193 < 1024); l193 = (l193 + 1)) {
			fVec4[l193] = 0.0f;
			
		}
		for (int l194 = 0; (l194 < 2); l194 = (l194 + 1)) {
			fRec278[l194] = 0.0f;
			
		}
		for (int l195 = 0; (l195 < 2); l195 = (l195 + 1)) {
			fRec276[l195] = 0.0f;
			
		}
		for (int l196 = 0; (l196 < 2); l196 = (l196 + 1)) {
			fRec275[l196] = 0.0f;
			
		}
		for (int l197 = 0; (l197 < 2); l197 = (l197 + 1)) {
			fRec273[l197] = 0.0f;
			
		}
		for (int l198 = 0; (l198 < 2); l198 = (l198 + 1)) {
			fRec272[l198] = 0.0f;
			
		}
		for (int l199 = 0; (l199 < 2); l199 = (l199 + 1)) {
			fRec270[l199] = 0.0f;
			
		}
		for (int l200 = 0; (l200 < 2); l200 = (l200 + 1)) {
			fRec269[l200] = 0.0f;
			
		}
		for (int l201 = 0; (l201 < 2); l201 = (l201 + 1)) {
			fRec267[l201] = 0.0f;
			
		}
		for (int l202 = 0; (l202 < 2); l202 = (l202 + 1)) {
			fRec266[l202] = 0.0f;
			
		}
		for (int l203 = 0; (l203 < 2); l203 = (l203 + 1)) {
			fRec264[l203] = 0.0f;
			
		}
		for (int l204 = 0; (l204 < 2); l204 = (l204 + 1)) {
			fRec290[l204] = 0.0f;
			
		}
		for (int l205 = 0; (l205 < 2); l205 = (l205 + 1)) {
			fRec288[l205] = 0.0f;
			
		}
		for (int l206 = 0; (l206 < 2); l206 = (l206 + 1)) {
			fRec287[l206] = 0.0f;
			
		}
		for (int l207 = 0; (l207 < 2); l207 = (l207 + 1)) {
			fRec285[l207] = 0.0f;
			
		}
		for (int l208 = 0; (l208 < 2); l208 = (l208 + 1)) {
			fRec284[l208] = 0.0f;
			
		}
		for (int l209 = 0; (l209 < 2); l209 = (l209 + 1)) {
			fRec282[l209] = 0.0f;
			
		}
		for (int l210 = 0; (l210 < 2); l210 = (l210 + 1)) {
			fRec281[l210] = 0.0f;
			
		}
		for (int l211 = 0; (l211 < 2); l211 = (l211 + 1)) {
			fRec279[l211] = 0.0f;
			
		}
		for (int l212 = 0; (l212 < 2); l212 = (l212 + 1)) {
			fRec299[l212] = 0.0f;
			
		}
		for (int l213 = 0; (l213 < 2); l213 = (l213 + 1)) {
			fRec297[l213] = 0.0f;
			
		}
		for (int l214 = 0; (l214 < 2); l214 = (l214 + 1)) {
			fRec296[l214] = 0.0f;
			
		}
		for (int l215 = 0; (l215 < 2); l215 = (l215 + 1)) {
			fRec294[l215] = 0.0f;
			
		}
		for (int l216 = 0; (l216 < 2); l216 = (l216 + 1)) {
			fRec293[l216] = 0.0f;
			
		}
		for (int l217 = 0; (l217 < 2); l217 = (l217 + 1)) {
			fRec291[l217] = 0.0f;
			
		}
		for (int l218 = 0; (l218 < 2); l218 = (l218 + 1)) {
			fRec302[l218] = 0.0f;
			
		}
		for (int l219 = 0; (l219 < 2); l219 = (l219 + 1)) {
			fRec300[l219] = 0.0f;
			
		}
		for (int l220 = 0; (l220 < 2); l220 = (l220 + 1)) {
			fRec308[l220] = 0.0f;
			
		}
		for (int l221 = 0; (l221 < 2); l221 = (l221 + 1)) {
			fRec306[l221] = 0.0f;
			
		}
		for (int l222 = 0; (l222 < 2); l222 = (l222 + 1)) {
			fRec305[l222] = 0.0f;
			
		}
		for (int l223 = 0; (l223 < 2); l223 = (l223 + 1)) {
			fRec303[l223] = 0.0f;
			
		}
		for (int l224 = 0; (l224 < 1024); l224 = (l224 + 1)) {
			fVec5[l224] = 0.0f;
			
		}
		for (int l225 = 0; (l225 < 2); l225 = (l225 + 1)) {
			fRec320[l225] = 0.0f;
			
		}
		for (int l226 = 0; (l226 < 2); l226 = (l226 + 1)) {
			fRec318[l226] = 0.0f;
			
		}
		for (int l227 = 0; (l227 < 2); l227 = (l227 + 1)) {
			fRec317[l227] = 0.0f;
			
		}
		for (int l228 = 0; (l228 < 2); l228 = (l228 + 1)) {
			fRec315[l228] = 0.0f;
			
		}
		for (int l229 = 0; (l229 < 2); l229 = (l229 + 1)) {
			fRec314[l229] = 0.0f;
			
		}
		for (int l230 = 0; (l230 < 2); l230 = (l230 + 1)) {
			fRec312[l230] = 0.0f;
			
		}
		for (int l231 = 0; (l231 < 2); l231 = (l231 + 1)) {
			fRec311[l231] = 0.0f;
			
		}
		for (int l232 = 0; (l232 < 2); l232 = (l232 + 1)) {
			fRec309[l232] = 0.0f;
			
		}
		for (int l233 = 0; (l233 < 2); l233 = (l233 + 1)) {
			fRec329[l233] = 0.0f;
			
		}
		for (int l234 = 0; (l234 < 2); l234 = (l234 + 1)) {
			fRec327[l234] = 0.0f;
			
		}
		for (int l235 = 0; (l235 < 2); l235 = (l235 + 1)) {
			fRec326[l235] = 0.0f;
			
		}
		for (int l236 = 0; (l236 < 2); l236 = (l236 + 1)) {
			fRec324[l236] = 0.0f;
			
		}
		for (int l237 = 0; (l237 < 2); l237 = (l237 + 1)) {
			fRec323[l237] = 0.0f;
			
		}
		for (int l238 = 0; (l238 < 2); l238 = (l238 + 1)) {
			fRec321[l238] = 0.0f;
			
		}
		for (int l239 = 0; (l239 < 2); l239 = (l239 + 1)) {
			fRec332[l239] = 0.0f;
			
		}
		for (int l240 = 0; (l240 < 2); l240 = (l240 + 1)) {
			fRec330[l240] = 0.0f;
			
		}
		for (int l241 = 0; (l241 < 2); l241 = (l241 + 1)) {
			fRec338[l241] = 0.0f;
			
		}
		for (int l242 = 0; (l242 < 2); l242 = (l242 + 1)) {
			fRec336[l242] = 0.0f;
			
		}
		for (int l243 = 0; (l243 < 2); l243 = (l243 + 1)) {
			fRec335[l243] = 0.0f;
			
		}
		for (int l244 = 0; (l244 < 2); l244 = (l244 + 1)) {
			fRec333[l244] = 0.0f;
			
		}
		for (int l245 = 0; (l245 < 2); l245 = (l245 + 1)) {
			fRec353[l245] = 0.0f;
			
		}
		for (int l246 = 0; (l246 < 2); l246 = (l246 + 1)) {
			fRec351[l246] = 0.0f;
			
		}
		for (int l247 = 0; (l247 < 2); l247 = (l247 + 1)) {
			fRec350[l247] = 0.0f;
			
		}
		for (int l248 = 0; (l248 < 2); l248 = (l248 + 1)) {
			fRec348[l248] = 0.0f;
			
		}
		for (int l249 = 0; (l249 < 2); l249 = (l249 + 1)) {
			fRec347[l249] = 0.0f;
			
		}
		for (int l250 = 0; (l250 < 2); l250 = (l250 + 1)) {
			fRec345[l250] = 0.0f;
			
		}
		for (int l251 = 0; (l251 < 2); l251 = (l251 + 1)) {
			fRec344[l251] = 0.0f;
			
		}
		for (int l252 = 0; (l252 < 2); l252 = (l252 + 1)) {
			fRec342[l252] = 0.0f;
			
		}
		for (int l253 = 0; (l253 < 2); l253 = (l253 + 1)) {
			fRec341[l253] = 0.0f;
			
		}
		for (int l254 = 0; (l254 < 2); l254 = (l254 + 1)) {
			fRec339[l254] = 0.0f;
			
		}
		for (int l255 = 0; (l255 < 1024); l255 = (l255 + 1)) {
			fVec6[l255] = 0.0f;
			
		}
		for (int l256 = 0; (l256 < 2); l256 = (l256 + 1)) {
			fRec356[l256] = 0.0f;
			
		}
		for (int l257 = 0; (l257 < 2); l257 = (l257 + 1)) {
			fRec354[l257] = 0.0f;
			
		}
		for (int l258 = 0; (l258 < 2); l258 = (l258 + 1)) {
			fRec362[l258] = 0.0f;
			
		}
		for (int l259 = 0; (l259 < 2); l259 = (l259 + 1)) {
			fRec360[l259] = 0.0f;
			
		}
		for (int l260 = 0; (l260 < 2); l260 = (l260 + 1)) {
			fRec359[l260] = 0.0f;
			
		}
		for (int l261 = 0; (l261 < 2); l261 = (l261 + 1)) {
			fRec357[l261] = 0.0f;
			
		}
		for (int l262 = 0; (l262 < 2); l262 = (l262 + 1)) {
			fRec371[l262] = 0.0f;
			
		}
		for (int l263 = 0; (l263 < 2); l263 = (l263 + 1)) {
			fRec369[l263] = 0.0f;
			
		}
		for (int l264 = 0; (l264 < 2); l264 = (l264 + 1)) {
			fRec368[l264] = 0.0f;
			
		}
		for (int l265 = 0; (l265 < 2); l265 = (l265 + 1)) {
			fRec366[l265] = 0.0f;
			
		}
		for (int l266 = 0; (l266 < 2); l266 = (l266 + 1)) {
			fRec365[l266] = 0.0f;
			
		}
		for (int l267 = 0; (l267 < 2); l267 = (l267 + 1)) {
			fRec363[l267] = 0.0f;
			
		}
		for (int l268 = 0; (l268 < 2); l268 = (l268 + 1)) {
			fRec383[l268] = 0.0f;
			
		}
		for (int l269 = 0; (l269 < 2); l269 = (l269 + 1)) {
			fRec381[l269] = 0.0f;
			
		}
		for (int l270 = 0; (l270 < 2); l270 = (l270 + 1)) {
			fRec380[l270] = 0.0f;
			
		}
		for (int l271 = 0; (l271 < 2); l271 = (l271 + 1)) {
			fRec378[l271] = 0.0f;
			
		}
		for (int l272 = 0; (l272 < 2); l272 = (l272 + 1)) {
			fRec377[l272] = 0.0f;
			
		}
		for (int l273 = 0; (l273 < 2); l273 = (l273 + 1)) {
			fRec375[l273] = 0.0f;
			
		}
		for (int l274 = 0; (l274 < 2); l274 = (l274 + 1)) {
			fRec374[l274] = 0.0f;
			
		}
		for (int l275 = 0; (l275 < 2); l275 = (l275 + 1)) {
			fRec372[l275] = 0.0f;
			
		}
		for (int l276 = 0; (l276 < 2); l276 = (l276 + 1)) {
			fRec398[l276] = 0.0f;
			
		}
		for (int l277 = 0; (l277 < 2); l277 = (l277 + 1)) {
			fRec396[l277] = 0.0f;
			
		}
		for (int l278 = 0; (l278 < 2); l278 = (l278 + 1)) {
			fRec395[l278] = 0.0f;
			
		}
		for (int l279 = 0; (l279 < 2); l279 = (l279 + 1)) {
			fRec393[l279] = 0.0f;
			
		}
		for (int l280 = 0; (l280 < 2); l280 = (l280 + 1)) {
			fRec392[l280] = 0.0f;
			
		}
		for (int l281 = 0; (l281 < 2); l281 = (l281 + 1)) {
			fRec390[l281] = 0.0f;
			
		}
		for (int l282 = 0; (l282 < 2); l282 = (l282 + 1)) {
			fRec389[l282] = 0.0f;
			
		}
		for (int l283 = 0; (l283 < 2); l283 = (l283 + 1)) {
			fRec387[l283] = 0.0f;
			
		}
		for (int l284 = 0; (l284 < 2); l284 = (l284 + 1)) {
			fRec386[l284] = 0.0f;
			
		}
		for (int l285 = 0; (l285 < 2); l285 = (l285 + 1)) {
			fRec384[l285] = 0.0f;
			
		}
		for (int l286 = 0; (l286 < 1024); l286 = (l286 + 1)) {
			fVec7[l286] = 0.0f;
			
		}
		for (int l287 = 0; (l287 < 2); l287 = (l287 + 1)) {
			fRec407[l287] = 0.0f;
			
		}
		for (int l288 = 0; (l288 < 2); l288 = (l288 + 1)) {
			fRec405[l288] = 0.0f;
			
		}
		for (int l289 = 0; (l289 < 2); l289 = (l289 + 1)) {
			fRec404[l289] = 0.0f;
			
		}
		for (int l290 = 0; (l290 < 2); l290 = (l290 + 1)) {
			fRec402[l290] = 0.0f;
			
		}
		for (int l291 = 0; (l291 < 2); l291 = (l291 + 1)) {
			fRec401[l291] = 0.0f;
			
		}
		for (int l292 = 0; (l292 < 2); l292 = (l292 + 1)) {
			fRec399[l292] = 0.0f;
			
		}
		for (int l293 = 0; (l293 < 2); l293 = (l293 + 1)) {
			fRec419[l293] = 0.0f;
			
		}
		for (int l294 = 0; (l294 < 2); l294 = (l294 + 1)) {
			fRec417[l294] = 0.0f;
			
		}
		for (int l295 = 0; (l295 < 2); l295 = (l295 + 1)) {
			fRec416[l295] = 0.0f;
			
		}
		for (int l296 = 0; (l296 < 2); l296 = (l296 + 1)) {
			fRec414[l296] = 0.0f;
			
		}
		for (int l297 = 0; (l297 < 2); l297 = (l297 + 1)) {
			fRec413[l297] = 0.0f;
			
		}
		for (int l298 = 0; (l298 < 2); l298 = (l298 + 1)) {
			fRec411[l298] = 0.0f;
			
		}
		for (int l299 = 0; (l299 < 2); l299 = (l299 + 1)) {
			fRec410[l299] = 0.0f;
			
		}
		for (int l300 = 0; (l300 < 2); l300 = (l300 + 1)) {
			fRec408[l300] = 0.0f;
			
		}
		for (int l301 = 0; (l301 < 2); l301 = (l301 + 1)) {
			fRec422[l301] = 0.0f;
			
		}
		for (int l302 = 0; (l302 < 2); l302 = (l302 + 1)) {
			fRec420[l302] = 0.0f;
			
		}
		for (int l303 = 0; (l303 < 2); l303 = (l303 + 1)) {
			fRec428[l303] = 0.0f;
			
		}
		for (int l304 = 0; (l304 < 2); l304 = (l304 + 1)) {
			fRec426[l304] = 0.0f;
			
		}
		for (int l305 = 0; (l305 < 2); l305 = (l305 + 1)) {
			fRec425[l305] = 0.0f;
			
		}
		for (int l306 = 0; (l306 < 2); l306 = (l306 + 1)) {
			fRec423[l306] = 0.0f;
			
		}
		for (int l307 = 0; (l307 < 2); l307 = (l307 + 1)) {
			fRec443[l307] = 0.0f;
			
		}
		for (int l308 = 0; (l308 < 2); l308 = (l308 + 1)) {
			fRec441[l308] = 0.0f;
			
		}
		for (int l309 = 0; (l309 < 2); l309 = (l309 + 1)) {
			fRec440[l309] = 0.0f;
			
		}
		for (int l310 = 0; (l310 < 2); l310 = (l310 + 1)) {
			fRec438[l310] = 0.0f;
			
		}
		for (int l311 = 0; (l311 < 2); l311 = (l311 + 1)) {
			fRec437[l311] = 0.0f;
			
		}
		for (int l312 = 0; (l312 < 2); l312 = (l312 + 1)) {
			fRec435[l312] = 0.0f;
			
		}
		for (int l313 = 0; (l313 < 2); l313 = (l313 + 1)) {
			fRec434[l313] = 0.0f;
			
		}
		for (int l314 = 0; (l314 < 2); l314 = (l314 + 1)) {
			fRec432[l314] = 0.0f;
			
		}
		for (int l315 = 0; (l315 < 2); l315 = (l315 + 1)) {
			fRec431[l315] = 0.0f;
			
		}
		for (int l316 = 0; (l316 < 2); l316 = (l316 + 1)) {
			fRec429[l316] = 0.0f;
			
		}
		for (int l317 = 0; (l317 < 1024); l317 = (l317 + 1)) {
			fVec8[l317] = 0.0f;
			
		}
		for (int l318 = 0; (l318 < 2); l318 = (l318 + 1)) {
			fRec446[l318] = 0.0f;
			
		}
		for (int l319 = 0; (l319 < 2); l319 = (l319 + 1)) {
			fRec444[l319] = 0.0f;
			
		}
		for (int l320 = 0; (l320 < 2); l320 = (l320 + 1)) {
			fRec452[l320] = 0.0f;
			
		}
		for (int l321 = 0; (l321 < 2); l321 = (l321 + 1)) {
			fRec450[l321] = 0.0f;
			
		}
		for (int l322 = 0; (l322 < 2); l322 = (l322 + 1)) {
			fRec449[l322] = 0.0f;
			
		}
		for (int l323 = 0; (l323 < 2); l323 = (l323 + 1)) {
			fRec447[l323] = 0.0f;
			
		}
		for (int l324 = 0; (l324 < 2); l324 = (l324 + 1)) {
			fRec461[l324] = 0.0f;
			
		}
		for (int l325 = 0; (l325 < 2); l325 = (l325 + 1)) {
			fRec459[l325] = 0.0f;
			
		}
		for (int l326 = 0; (l326 < 2); l326 = (l326 + 1)) {
			fRec458[l326] = 0.0f;
			
		}
		for (int l327 = 0; (l327 < 2); l327 = (l327 + 1)) {
			fRec456[l327] = 0.0f;
			
		}
		for (int l328 = 0; (l328 < 2); l328 = (l328 + 1)) {
			fRec455[l328] = 0.0f;
			
		}
		for (int l329 = 0; (l329 < 2); l329 = (l329 + 1)) {
			fRec453[l329] = 0.0f;
			
		}
		for (int l330 = 0; (l330 < 2); l330 = (l330 + 1)) {
			fRec473[l330] = 0.0f;
			
		}
		for (int l331 = 0; (l331 < 2); l331 = (l331 + 1)) {
			fRec471[l331] = 0.0f;
			
		}
		for (int l332 = 0; (l332 < 2); l332 = (l332 + 1)) {
			fRec470[l332] = 0.0f;
			
		}
		for (int l333 = 0; (l333 < 2); l333 = (l333 + 1)) {
			fRec468[l333] = 0.0f;
			
		}
		for (int l334 = 0; (l334 < 2); l334 = (l334 + 1)) {
			fRec467[l334] = 0.0f;
			
		}
		for (int l335 = 0; (l335 < 2); l335 = (l335 + 1)) {
			fRec465[l335] = 0.0f;
			
		}
		for (int l336 = 0; (l336 < 2); l336 = (l336 + 1)) {
			fRec464[l336] = 0.0f;
			
		}
		for (int l337 = 0; (l337 < 2); l337 = (l337 + 1)) {
			fRec462[l337] = 0.0f;
			
		}
		for (int l338 = 0; (l338 < 2); l338 = (l338 + 1)) {
			fRec488[l338] = 0.0f;
			
		}
		for (int l339 = 0; (l339 < 2); l339 = (l339 + 1)) {
			fRec486[l339] = 0.0f;
			
		}
		for (int l340 = 0; (l340 < 2); l340 = (l340 + 1)) {
			fRec485[l340] = 0.0f;
			
		}
		for (int l341 = 0; (l341 < 2); l341 = (l341 + 1)) {
			fRec483[l341] = 0.0f;
			
		}
		for (int l342 = 0; (l342 < 2); l342 = (l342 + 1)) {
			fRec482[l342] = 0.0f;
			
		}
		for (int l343 = 0; (l343 < 2); l343 = (l343 + 1)) {
			fRec480[l343] = 0.0f;
			
		}
		for (int l344 = 0; (l344 < 2); l344 = (l344 + 1)) {
			fRec479[l344] = 0.0f;
			
		}
		for (int l345 = 0; (l345 < 2); l345 = (l345 + 1)) {
			fRec477[l345] = 0.0f;
			
		}
		for (int l346 = 0; (l346 < 2); l346 = (l346 + 1)) {
			fRec476[l346] = 0.0f;
			
		}
		for (int l347 = 0; (l347 < 2); l347 = (l347 + 1)) {
			fRec474[l347] = 0.0f;
			
		}
		for (int l348 = 0; (l348 < 1024); l348 = (l348 + 1)) {
			fVec9[l348] = 0.0f;
			
		}
		for (int l349 = 0; (l349 < 2); l349 = (l349 + 1)) {
			fRec491[l349] = 0.0f;
			
		}
		for (int l350 = 0; (l350 < 2); l350 = (l350 + 1)) {
			fRec489[l350] = 0.0f;
			
		}
		for (int l351 = 0; (l351 < 2); l351 = (l351 + 1)) {
			fRec497[l351] = 0.0f;
			
		}
		for (int l352 = 0; (l352 < 2); l352 = (l352 + 1)) {
			fRec495[l352] = 0.0f;
			
		}
		for (int l353 = 0; (l353 < 2); l353 = (l353 + 1)) {
			fRec494[l353] = 0.0f;
			
		}
		for (int l354 = 0; (l354 < 2); l354 = (l354 + 1)) {
			fRec492[l354] = 0.0f;
			
		}
		for (int l355 = 0; (l355 < 2); l355 = (l355 + 1)) {
			fRec506[l355] = 0.0f;
			
		}
		for (int l356 = 0; (l356 < 2); l356 = (l356 + 1)) {
			fRec504[l356] = 0.0f;
			
		}
		for (int l357 = 0; (l357 < 2); l357 = (l357 + 1)) {
			fRec503[l357] = 0.0f;
			
		}
		for (int l358 = 0; (l358 < 2); l358 = (l358 + 1)) {
			fRec501[l358] = 0.0f;
			
		}
		for (int l359 = 0; (l359 < 2); l359 = (l359 + 1)) {
			fRec500[l359] = 0.0f;
			
		}
		for (int l360 = 0; (l360 < 2); l360 = (l360 + 1)) {
			fRec498[l360] = 0.0f;
			
		}
		for (int l361 = 0; (l361 < 2); l361 = (l361 + 1)) {
			fRec518[l361] = 0.0f;
			
		}
		for (int l362 = 0; (l362 < 2); l362 = (l362 + 1)) {
			fRec516[l362] = 0.0f;
			
		}
		for (int l363 = 0; (l363 < 2); l363 = (l363 + 1)) {
			fRec515[l363] = 0.0f;
			
		}
		for (int l364 = 0; (l364 < 2); l364 = (l364 + 1)) {
			fRec513[l364] = 0.0f;
			
		}
		for (int l365 = 0; (l365 < 2); l365 = (l365 + 1)) {
			fRec512[l365] = 0.0f;
			
		}
		for (int l366 = 0; (l366 < 2); l366 = (l366 + 1)) {
			fRec510[l366] = 0.0f;
			
		}
		for (int l367 = 0; (l367 < 2); l367 = (l367 + 1)) {
			fRec509[l367] = 0.0f;
			
		}
		for (int l368 = 0; (l368 < 2); l368 = (l368 + 1)) {
			fRec507[l368] = 0.0f;
			
		}
		for (int l369 = 0; (l369 < 2); l369 = (l369 + 1)) {
			fRec533[l369] = 0.0f;
			
		}
		for (int l370 = 0; (l370 < 2); l370 = (l370 + 1)) {
			fRec531[l370] = 0.0f;
			
		}
		for (int l371 = 0; (l371 < 2); l371 = (l371 + 1)) {
			fRec530[l371] = 0.0f;
			
		}
		for (int l372 = 0; (l372 < 2); l372 = (l372 + 1)) {
			fRec528[l372] = 0.0f;
			
		}
		for (int l373 = 0; (l373 < 2); l373 = (l373 + 1)) {
			fRec527[l373] = 0.0f;
			
		}
		for (int l374 = 0; (l374 < 2); l374 = (l374 + 1)) {
			fRec525[l374] = 0.0f;
			
		}
		for (int l375 = 0; (l375 < 2); l375 = (l375 + 1)) {
			fRec524[l375] = 0.0f;
			
		}
		for (int l376 = 0; (l376 < 2); l376 = (l376 + 1)) {
			fRec522[l376] = 0.0f;
			
		}
		for (int l377 = 0; (l377 < 2); l377 = (l377 + 1)) {
			fRec521[l377] = 0.0f;
			
		}
		for (int l378 = 0; (l378 < 2); l378 = (l378 + 1)) {
			fRec519[l378] = 0.0f;
			
		}
		for (int l379 = 0; (l379 < 1024); l379 = (l379 + 1)) {
			fVec10[l379] = 0.0f;
			
		}
		for (int l380 = 0; (l380 < 2); l380 = (l380 + 1)) {
			fRec548[l380] = 0.0f;
			
		}
		for (int l381 = 0; (l381 < 2); l381 = (l381 + 1)) {
			fRec546[l381] = 0.0f;
			
		}
		for (int l382 = 0; (l382 < 2); l382 = (l382 + 1)) {
			fRec545[l382] = 0.0f;
			
		}
		for (int l383 = 0; (l383 < 2); l383 = (l383 + 1)) {
			fRec543[l383] = 0.0f;
			
		}
		for (int l384 = 0; (l384 < 2); l384 = (l384 + 1)) {
			fRec542[l384] = 0.0f;
			
		}
		for (int l385 = 0; (l385 < 2); l385 = (l385 + 1)) {
			fRec540[l385] = 0.0f;
			
		}
		for (int l386 = 0; (l386 < 2); l386 = (l386 + 1)) {
			fRec539[l386] = 0.0f;
			
		}
		for (int l387 = 0; (l387 < 2); l387 = (l387 + 1)) {
			fRec537[l387] = 0.0f;
			
		}
		for (int l388 = 0; (l388 < 2); l388 = (l388 + 1)) {
			fRec536[l388] = 0.0f;
			
		}
		for (int l389 = 0; (l389 < 2); l389 = (l389 + 1)) {
			fRec534[l389] = 0.0f;
			
		}
		for (int l390 = 0; (l390 < 2); l390 = (l390 + 1)) {
			fRec560[l390] = 0.0f;
			
		}
		for (int l391 = 0; (l391 < 2); l391 = (l391 + 1)) {
			fRec558[l391] = 0.0f;
			
		}
		for (int l392 = 0; (l392 < 2); l392 = (l392 + 1)) {
			fRec557[l392] = 0.0f;
			
		}
		for (int l393 = 0; (l393 < 2); l393 = (l393 + 1)) {
			fRec555[l393] = 0.0f;
			
		}
		for (int l394 = 0; (l394 < 2); l394 = (l394 + 1)) {
			fRec554[l394] = 0.0f;
			
		}
		for (int l395 = 0; (l395 < 2); l395 = (l395 + 1)) {
			fRec552[l395] = 0.0f;
			
		}
		for (int l396 = 0; (l396 < 2); l396 = (l396 + 1)) {
			fRec551[l396] = 0.0f;
			
		}
		for (int l397 = 0; (l397 < 2); l397 = (l397 + 1)) {
			fRec549[l397] = 0.0f;
			
		}
		for (int l398 = 0; (l398 < 2); l398 = (l398 + 1)) {
			fRec569[l398] = 0.0f;
			
		}
		for (int l399 = 0; (l399 < 2); l399 = (l399 + 1)) {
			fRec567[l399] = 0.0f;
			
		}
		for (int l400 = 0; (l400 < 2); l400 = (l400 + 1)) {
			fRec566[l400] = 0.0f;
			
		}
		for (int l401 = 0; (l401 < 2); l401 = (l401 + 1)) {
			fRec564[l401] = 0.0f;
			
		}
		for (int l402 = 0; (l402 < 2); l402 = (l402 + 1)) {
			fRec563[l402] = 0.0f;
			
		}
		for (int l403 = 0; (l403 < 2); l403 = (l403 + 1)) {
			fRec561[l403] = 0.0f;
			
		}
		for (int l404 = 0; (l404 < 2); l404 = (l404 + 1)) {
			fRec572[l404] = 0.0f;
			
		}
		for (int l405 = 0; (l405 < 2); l405 = (l405 + 1)) {
			fRec570[l405] = 0.0f;
			
		}
		for (int l406 = 0; (l406 < 2); l406 = (l406 + 1)) {
			fRec578[l406] = 0.0f;
			
		}
		for (int l407 = 0; (l407 < 2); l407 = (l407 + 1)) {
			fRec576[l407] = 0.0f;
			
		}
		for (int l408 = 0; (l408 < 2); l408 = (l408 + 1)) {
			fRec575[l408] = 0.0f;
			
		}
		for (int l409 = 0; (l409 < 2); l409 = (l409 + 1)) {
			fRec573[l409] = 0.0f;
			
		}
		for (int l410 = 0; (l410 < 1024); l410 = (l410 + 1)) {
			fVec11[l410] = 0.0f;
			
		}
		for (int l411 = 0; (l411 < 2); l411 = (l411 + 1)) {
			fRec593[l411] = 0.0f;
			
		}
		for (int l412 = 0; (l412 < 2); l412 = (l412 + 1)) {
			fRec591[l412] = 0.0f;
			
		}
		for (int l413 = 0; (l413 < 2); l413 = (l413 + 1)) {
			fRec590[l413] = 0.0f;
			
		}
		for (int l414 = 0; (l414 < 2); l414 = (l414 + 1)) {
			fRec588[l414] = 0.0f;
			
		}
		for (int l415 = 0; (l415 < 2); l415 = (l415 + 1)) {
			fRec587[l415] = 0.0f;
			
		}
		for (int l416 = 0; (l416 < 2); l416 = (l416 + 1)) {
			fRec585[l416] = 0.0f;
			
		}
		for (int l417 = 0; (l417 < 2); l417 = (l417 + 1)) {
			fRec584[l417] = 0.0f;
			
		}
		for (int l418 = 0; (l418 < 2); l418 = (l418 + 1)) {
			fRec582[l418] = 0.0f;
			
		}
		for (int l419 = 0; (l419 < 2); l419 = (l419 + 1)) {
			fRec581[l419] = 0.0f;
			
		}
		for (int l420 = 0; (l420 < 2); l420 = (l420 + 1)) {
			fRec579[l420] = 0.0f;
			
		}
		for (int l421 = 0; (l421 < 2); l421 = (l421 + 1)) {
			fRec605[l421] = 0.0f;
			
		}
		for (int l422 = 0; (l422 < 2); l422 = (l422 + 1)) {
			fRec603[l422] = 0.0f;
			
		}
		for (int l423 = 0; (l423 < 2); l423 = (l423 + 1)) {
			fRec602[l423] = 0.0f;
			
		}
		for (int l424 = 0; (l424 < 2); l424 = (l424 + 1)) {
			fRec600[l424] = 0.0f;
			
		}
		for (int l425 = 0; (l425 < 2); l425 = (l425 + 1)) {
			fRec599[l425] = 0.0f;
			
		}
		for (int l426 = 0; (l426 < 2); l426 = (l426 + 1)) {
			fRec597[l426] = 0.0f;
			
		}
		for (int l427 = 0; (l427 < 2); l427 = (l427 + 1)) {
			fRec596[l427] = 0.0f;
			
		}
		for (int l428 = 0; (l428 < 2); l428 = (l428 + 1)) {
			fRec594[l428] = 0.0f;
			
		}
		for (int l429 = 0; (l429 < 2); l429 = (l429 + 1)) {
			fRec614[l429] = 0.0f;
			
		}
		for (int l430 = 0; (l430 < 2); l430 = (l430 + 1)) {
			fRec612[l430] = 0.0f;
			
		}
		for (int l431 = 0; (l431 < 2); l431 = (l431 + 1)) {
			fRec611[l431] = 0.0f;
			
		}
		for (int l432 = 0; (l432 < 2); l432 = (l432 + 1)) {
			fRec609[l432] = 0.0f;
			
		}
		for (int l433 = 0; (l433 < 2); l433 = (l433 + 1)) {
			fRec608[l433] = 0.0f;
			
		}
		for (int l434 = 0; (l434 < 2); l434 = (l434 + 1)) {
			fRec606[l434] = 0.0f;
			
		}
		for (int l435 = 0; (l435 < 2); l435 = (l435 + 1)) {
			fRec617[l435] = 0.0f;
			
		}
		for (int l436 = 0; (l436 < 2); l436 = (l436 + 1)) {
			fRec615[l436] = 0.0f;
			
		}
		for (int l437 = 0; (l437 < 2); l437 = (l437 + 1)) {
			fRec623[l437] = 0.0f;
			
		}
		for (int l438 = 0; (l438 < 2); l438 = (l438 + 1)) {
			fRec621[l438] = 0.0f;
			
		}
		for (int l439 = 0; (l439 < 2); l439 = (l439 + 1)) {
			fRec620[l439] = 0.0f;
			
		}
		for (int l440 = 0; (l440 < 2); l440 = (l440 + 1)) {
			fRec618[l440] = 0.0f;
			
		}
		for (int l441 = 0; (l441 < 1024); l441 = (l441 + 1)) {
			fVec12[l441] = 0.0f;
			
		}
		for (int l442 = 0; (l442 < 2); l442 = (l442 + 1)) {
			fRec638[l442] = 0.0f;
			
		}
		for (int l443 = 0; (l443 < 2); l443 = (l443 + 1)) {
			fRec636[l443] = 0.0f;
			
		}
		for (int l444 = 0; (l444 < 2); l444 = (l444 + 1)) {
			fRec635[l444] = 0.0f;
			
		}
		for (int l445 = 0; (l445 < 2); l445 = (l445 + 1)) {
			fRec633[l445] = 0.0f;
			
		}
		for (int l446 = 0; (l446 < 2); l446 = (l446 + 1)) {
			fRec632[l446] = 0.0f;
			
		}
		for (int l447 = 0; (l447 < 2); l447 = (l447 + 1)) {
			fRec630[l447] = 0.0f;
			
		}
		for (int l448 = 0; (l448 < 2); l448 = (l448 + 1)) {
			fRec629[l448] = 0.0f;
			
		}
		for (int l449 = 0; (l449 < 2); l449 = (l449 + 1)) {
			fRec627[l449] = 0.0f;
			
		}
		for (int l450 = 0; (l450 < 2); l450 = (l450 + 1)) {
			fRec626[l450] = 0.0f;
			
		}
		for (int l451 = 0; (l451 < 2); l451 = (l451 + 1)) {
			fRec624[l451] = 0.0f;
			
		}
		for (int l452 = 0; (l452 < 2); l452 = (l452 + 1)) {
			fRec650[l452] = 0.0f;
			
		}
		for (int l453 = 0; (l453 < 2); l453 = (l453 + 1)) {
			fRec648[l453] = 0.0f;
			
		}
		for (int l454 = 0; (l454 < 2); l454 = (l454 + 1)) {
			fRec647[l454] = 0.0f;
			
		}
		for (int l455 = 0; (l455 < 2); l455 = (l455 + 1)) {
			fRec645[l455] = 0.0f;
			
		}
		for (int l456 = 0; (l456 < 2); l456 = (l456 + 1)) {
			fRec644[l456] = 0.0f;
			
		}
		for (int l457 = 0; (l457 < 2); l457 = (l457 + 1)) {
			fRec642[l457] = 0.0f;
			
		}
		for (int l458 = 0; (l458 < 2); l458 = (l458 + 1)) {
			fRec641[l458] = 0.0f;
			
		}
		for (int l459 = 0; (l459 < 2); l459 = (l459 + 1)) {
			fRec639[l459] = 0.0f;
			
		}
		for (int l460 = 0; (l460 < 2); l460 = (l460 + 1)) {
			fRec659[l460] = 0.0f;
			
		}
		for (int l461 = 0; (l461 < 2); l461 = (l461 + 1)) {
			fRec657[l461] = 0.0f;
			
		}
		for (int l462 = 0; (l462 < 2); l462 = (l462 + 1)) {
			fRec656[l462] = 0.0f;
			
		}
		for (int l463 = 0; (l463 < 2); l463 = (l463 + 1)) {
			fRec654[l463] = 0.0f;
			
		}
		for (int l464 = 0; (l464 < 2); l464 = (l464 + 1)) {
			fRec653[l464] = 0.0f;
			
		}
		for (int l465 = 0; (l465 < 2); l465 = (l465 + 1)) {
			fRec651[l465] = 0.0f;
			
		}
		for (int l466 = 0; (l466 < 2); l466 = (l466 + 1)) {
			fRec662[l466] = 0.0f;
			
		}
		for (int l467 = 0; (l467 < 2); l467 = (l467 + 1)) {
			fRec660[l467] = 0.0f;
			
		}
		for (int l468 = 0; (l468 < 2); l468 = (l468 + 1)) {
			fRec668[l468] = 0.0f;
			
		}
		for (int l469 = 0; (l469 < 2); l469 = (l469 + 1)) {
			fRec666[l469] = 0.0f;
			
		}
		for (int l470 = 0; (l470 < 2); l470 = (l470 + 1)) {
			fRec665[l470] = 0.0f;
			
		}
		for (int l471 = 0; (l471 < 2); l471 = (l471 + 1)) {
			fRec663[l471] = 0.0f;
			
		}
		for (int l472 = 0; (l472 < 1024); l472 = (l472 + 1)) {
			fVec13[l472] = 0.0f;
			
		}
		for (int l473 = 0; (l473 < 2); l473 = (l473 + 1)) {
			fRec683[l473] = 0.0f;
			
		}
		for (int l474 = 0; (l474 < 2); l474 = (l474 + 1)) {
			fRec681[l474] = 0.0f;
			
		}
		for (int l475 = 0; (l475 < 2); l475 = (l475 + 1)) {
			fRec680[l475] = 0.0f;
			
		}
		for (int l476 = 0; (l476 < 2); l476 = (l476 + 1)) {
			fRec678[l476] = 0.0f;
			
		}
		for (int l477 = 0; (l477 < 2); l477 = (l477 + 1)) {
			fRec677[l477] = 0.0f;
			
		}
		for (int l478 = 0; (l478 < 2); l478 = (l478 + 1)) {
			fRec675[l478] = 0.0f;
			
		}
		for (int l479 = 0; (l479 < 2); l479 = (l479 + 1)) {
			fRec674[l479] = 0.0f;
			
		}
		for (int l480 = 0; (l480 < 2); l480 = (l480 + 1)) {
			fRec672[l480] = 0.0f;
			
		}
		for (int l481 = 0; (l481 < 2); l481 = (l481 + 1)) {
			fRec671[l481] = 0.0f;
			
		}
		for (int l482 = 0; (l482 < 2); l482 = (l482 + 1)) {
			fRec669[l482] = 0.0f;
			
		}
		for (int l483 = 0; (l483 < 2); l483 = (l483 + 1)) {
			fRec695[l483] = 0.0f;
			
		}
		for (int l484 = 0; (l484 < 2); l484 = (l484 + 1)) {
			fRec693[l484] = 0.0f;
			
		}
		for (int l485 = 0; (l485 < 2); l485 = (l485 + 1)) {
			fRec692[l485] = 0.0f;
			
		}
		for (int l486 = 0; (l486 < 2); l486 = (l486 + 1)) {
			fRec690[l486] = 0.0f;
			
		}
		for (int l487 = 0; (l487 < 2); l487 = (l487 + 1)) {
			fRec689[l487] = 0.0f;
			
		}
		for (int l488 = 0; (l488 < 2); l488 = (l488 + 1)) {
			fRec687[l488] = 0.0f;
			
		}
		for (int l489 = 0; (l489 < 2); l489 = (l489 + 1)) {
			fRec686[l489] = 0.0f;
			
		}
		for (int l490 = 0; (l490 < 2); l490 = (l490 + 1)) {
			fRec684[l490] = 0.0f;
			
		}
		for (int l491 = 0; (l491 < 2); l491 = (l491 + 1)) {
			fRec704[l491] = 0.0f;
			
		}
		for (int l492 = 0; (l492 < 2); l492 = (l492 + 1)) {
			fRec702[l492] = 0.0f;
			
		}
		for (int l493 = 0; (l493 < 2); l493 = (l493 + 1)) {
			fRec701[l493] = 0.0f;
			
		}
		for (int l494 = 0; (l494 < 2); l494 = (l494 + 1)) {
			fRec699[l494] = 0.0f;
			
		}
		for (int l495 = 0; (l495 < 2); l495 = (l495 + 1)) {
			fRec698[l495] = 0.0f;
			
		}
		for (int l496 = 0; (l496 < 2); l496 = (l496 + 1)) {
			fRec696[l496] = 0.0f;
			
		}
		for (int l497 = 0; (l497 < 2); l497 = (l497 + 1)) {
			fRec707[l497] = 0.0f;
			
		}
		for (int l498 = 0; (l498 < 2); l498 = (l498 + 1)) {
			fRec705[l498] = 0.0f;
			
		}
		for (int l499 = 0; (l499 < 2); l499 = (l499 + 1)) {
			fRec713[l499] = 0.0f;
			
		}
		for (int l500 = 0; (l500 < 2); l500 = (l500 + 1)) {
			fRec711[l500] = 0.0f;
			
		}
		for (int l501 = 0; (l501 < 2); l501 = (l501 + 1)) {
			fRec710[l501] = 0.0f;
			
		}
		for (int l502 = 0; (l502 < 2); l502 = (l502 + 1)) {
			fRec708[l502] = 0.0f;
			
		}
		for (int l503 = 0; (l503 < 1024); l503 = (l503 + 1)) {
			fVec14[l503] = 0.0f;
			
		}
		for (int l504 = 0; (l504 < 2); l504 = (l504 + 1)) {
			fRec728[l504] = 0.0f;
			
		}
		for (int l505 = 0; (l505 < 2); l505 = (l505 + 1)) {
			fRec726[l505] = 0.0f;
			
		}
		for (int l506 = 0; (l506 < 2); l506 = (l506 + 1)) {
			fRec725[l506] = 0.0f;
			
		}
		for (int l507 = 0; (l507 < 2); l507 = (l507 + 1)) {
			fRec723[l507] = 0.0f;
			
		}
		for (int l508 = 0; (l508 < 2); l508 = (l508 + 1)) {
			fRec722[l508] = 0.0f;
			
		}
		for (int l509 = 0; (l509 < 2); l509 = (l509 + 1)) {
			fRec720[l509] = 0.0f;
			
		}
		for (int l510 = 0; (l510 < 2); l510 = (l510 + 1)) {
			fRec719[l510] = 0.0f;
			
		}
		for (int l511 = 0; (l511 < 2); l511 = (l511 + 1)) {
			fRec717[l511] = 0.0f;
			
		}
		for (int l512 = 0; (l512 < 2); l512 = (l512 + 1)) {
			fRec716[l512] = 0.0f;
			
		}
		for (int l513 = 0; (l513 < 2); l513 = (l513 + 1)) {
			fRec714[l513] = 0.0f;
			
		}
		for (int l514 = 0; (l514 < 2); l514 = (l514 + 1)) {
			fRec740[l514] = 0.0f;
			
		}
		for (int l515 = 0; (l515 < 2); l515 = (l515 + 1)) {
			fRec738[l515] = 0.0f;
			
		}
		for (int l516 = 0; (l516 < 2); l516 = (l516 + 1)) {
			fRec737[l516] = 0.0f;
			
		}
		for (int l517 = 0; (l517 < 2); l517 = (l517 + 1)) {
			fRec735[l517] = 0.0f;
			
		}
		for (int l518 = 0; (l518 < 2); l518 = (l518 + 1)) {
			fRec734[l518] = 0.0f;
			
		}
		for (int l519 = 0; (l519 < 2); l519 = (l519 + 1)) {
			fRec732[l519] = 0.0f;
			
		}
		for (int l520 = 0; (l520 < 2); l520 = (l520 + 1)) {
			fRec731[l520] = 0.0f;
			
		}
		for (int l521 = 0; (l521 < 2); l521 = (l521 + 1)) {
			fRec729[l521] = 0.0f;
			
		}
		for (int l522 = 0; (l522 < 2); l522 = (l522 + 1)) {
			fRec749[l522] = 0.0f;
			
		}
		for (int l523 = 0; (l523 < 2); l523 = (l523 + 1)) {
			fRec747[l523] = 0.0f;
			
		}
		for (int l524 = 0; (l524 < 2); l524 = (l524 + 1)) {
			fRec746[l524] = 0.0f;
			
		}
		for (int l525 = 0; (l525 < 2); l525 = (l525 + 1)) {
			fRec744[l525] = 0.0f;
			
		}
		for (int l526 = 0; (l526 < 2); l526 = (l526 + 1)) {
			fRec743[l526] = 0.0f;
			
		}
		for (int l527 = 0; (l527 < 2); l527 = (l527 + 1)) {
			fRec741[l527] = 0.0f;
			
		}
		for (int l528 = 0; (l528 < 2); l528 = (l528 + 1)) {
			fRec752[l528] = 0.0f;
			
		}
		for (int l529 = 0; (l529 < 2); l529 = (l529 + 1)) {
			fRec750[l529] = 0.0f;
			
		}
		for (int l530 = 0; (l530 < 2); l530 = (l530 + 1)) {
			fRec758[l530] = 0.0f;
			
		}
		for (int l531 = 0; (l531 < 2); l531 = (l531 + 1)) {
			fRec756[l531] = 0.0f;
			
		}
		for (int l532 = 0; (l532 < 2); l532 = (l532 + 1)) {
			fRec755[l532] = 0.0f;
			
		}
		for (int l533 = 0; (l533 < 2); l533 = (l533 + 1)) {
			fRec753[l533] = 0.0f;
			
		}
		for (int l534 = 0; (l534 < 1024); l534 = (l534 + 1)) {
			fVec15[l534] = 0.0f;
			
		}
		for (int l535 = 0; (l535 < 2); l535 = (l535 + 1)) {
			fRec773[l535] = 0.0f;
			
		}
		for (int l536 = 0; (l536 < 2); l536 = (l536 + 1)) {
			fRec771[l536] = 0.0f;
			
		}
		for (int l537 = 0; (l537 < 2); l537 = (l537 + 1)) {
			fRec770[l537] = 0.0f;
			
		}
		for (int l538 = 0; (l538 < 2); l538 = (l538 + 1)) {
			fRec768[l538] = 0.0f;
			
		}
		for (int l539 = 0; (l539 < 2); l539 = (l539 + 1)) {
			fRec767[l539] = 0.0f;
			
		}
		for (int l540 = 0; (l540 < 2); l540 = (l540 + 1)) {
			fRec765[l540] = 0.0f;
			
		}
		for (int l541 = 0; (l541 < 2); l541 = (l541 + 1)) {
			fRec764[l541] = 0.0f;
			
		}
		for (int l542 = 0; (l542 < 2); l542 = (l542 + 1)) {
			fRec762[l542] = 0.0f;
			
		}
		for (int l543 = 0; (l543 < 2); l543 = (l543 + 1)) {
			fRec761[l543] = 0.0f;
			
		}
		for (int l544 = 0; (l544 < 2); l544 = (l544 + 1)) {
			fRec759[l544] = 0.0f;
			
		}
		for (int l545 = 0; (l545 < 2); l545 = (l545 + 1)) {
			fRec785[l545] = 0.0f;
			
		}
		for (int l546 = 0; (l546 < 2); l546 = (l546 + 1)) {
			fRec783[l546] = 0.0f;
			
		}
		for (int l547 = 0; (l547 < 2); l547 = (l547 + 1)) {
			fRec782[l547] = 0.0f;
			
		}
		for (int l548 = 0; (l548 < 2); l548 = (l548 + 1)) {
			fRec780[l548] = 0.0f;
			
		}
		for (int l549 = 0; (l549 < 2); l549 = (l549 + 1)) {
			fRec779[l549] = 0.0f;
			
		}
		for (int l550 = 0; (l550 < 2); l550 = (l550 + 1)) {
			fRec777[l550] = 0.0f;
			
		}
		for (int l551 = 0; (l551 < 2); l551 = (l551 + 1)) {
			fRec776[l551] = 0.0f;
			
		}
		for (int l552 = 0; (l552 < 2); l552 = (l552 + 1)) {
			fRec774[l552] = 0.0f;
			
		}
		for (int l553 = 0; (l553 < 2); l553 = (l553 + 1)) {
			fRec794[l553] = 0.0f;
			
		}
		for (int l554 = 0; (l554 < 2); l554 = (l554 + 1)) {
			fRec792[l554] = 0.0f;
			
		}
		for (int l555 = 0; (l555 < 2); l555 = (l555 + 1)) {
			fRec791[l555] = 0.0f;
			
		}
		for (int l556 = 0; (l556 < 2); l556 = (l556 + 1)) {
			fRec789[l556] = 0.0f;
			
		}
		for (int l557 = 0; (l557 < 2); l557 = (l557 + 1)) {
			fRec788[l557] = 0.0f;
			
		}
		for (int l558 = 0; (l558 < 2); l558 = (l558 + 1)) {
			fRec786[l558] = 0.0f;
			
		}
		for (int l559 = 0; (l559 < 2); l559 = (l559 + 1)) {
			fRec797[l559] = 0.0f;
			
		}
		for (int l560 = 0; (l560 < 2); l560 = (l560 + 1)) {
			fRec795[l560] = 0.0f;
			
		}
		for (int l561 = 0; (l561 < 2); l561 = (l561 + 1)) {
			fRec803[l561] = 0.0f;
			
		}
		for (int l562 = 0; (l562 < 2); l562 = (l562 + 1)) {
			fRec801[l562] = 0.0f;
			
		}
		for (int l563 = 0; (l563 < 2); l563 = (l563 + 1)) {
			fRec800[l563] = 0.0f;
			
		}
		for (int l564 = 0; (l564 < 2); l564 = (l564 + 1)) {
			fRec798[l564] = 0.0f;
			
		}
		for (int l565 = 0; (l565 < 1024); l565 = (l565 + 1)) {
			fVec16[l565] = 0.0f;
			
		}
		for (int l566 = 0; (l566 < 2); l566 = (l566 + 1)) {
			fRec818[l566] = 0.0f;
			
		}
		for (int l567 = 0; (l567 < 2); l567 = (l567 + 1)) {
			fRec816[l567] = 0.0f;
			
		}
		for (int l568 = 0; (l568 < 2); l568 = (l568 + 1)) {
			fRec815[l568] = 0.0f;
			
		}
		for (int l569 = 0; (l569 < 2); l569 = (l569 + 1)) {
			fRec813[l569] = 0.0f;
			
		}
		for (int l570 = 0; (l570 < 2); l570 = (l570 + 1)) {
			fRec812[l570] = 0.0f;
			
		}
		for (int l571 = 0; (l571 < 2); l571 = (l571 + 1)) {
			fRec810[l571] = 0.0f;
			
		}
		for (int l572 = 0; (l572 < 2); l572 = (l572 + 1)) {
			fRec809[l572] = 0.0f;
			
		}
		for (int l573 = 0; (l573 < 2); l573 = (l573 + 1)) {
			fRec807[l573] = 0.0f;
			
		}
		for (int l574 = 0; (l574 < 2); l574 = (l574 + 1)) {
			fRec806[l574] = 0.0f;
			
		}
		for (int l575 = 0; (l575 < 2); l575 = (l575 + 1)) {
			fRec804[l575] = 0.0f;
			
		}
		for (int l576 = 0; (l576 < 2); l576 = (l576 + 1)) {
			fRec830[l576] = 0.0f;
			
		}
		for (int l577 = 0; (l577 < 2); l577 = (l577 + 1)) {
			fRec828[l577] = 0.0f;
			
		}
		for (int l578 = 0; (l578 < 2); l578 = (l578 + 1)) {
			fRec827[l578] = 0.0f;
			
		}
		for (int l579 = 0; (l579 < 2); l579 = (l579 + 1)) {
			fRec825[l579] = 0.0f;
			
		}
		for (int l580 = 0; (l580 < 2); l580 = (l580 + 1)) {
			fRec824[l580] = 0.0f;
			
		}
		for (int l581 = 0; (l581 < 2); l581 = (l581 + 1)) {
			fRec822[l581] = 0.0f;
			
		}
		for (int l582 = 0; (l582 < 2); l582 = (l582 + 1)) {
			fRec821[l582] = 0.0f;
			
		}
		for (int l583 = 0; (l583 < 2); l583 = (l583 + 1)) {
			fRec819[l583] = 0.0f;
			
		}
		for (int l584 = 0; (l584 < 2); l584 = (l584 + 1)) {
			fRec839[l584] = 0.0f;
			
		}
		for (int l585 = 0; (l585 < 2); l585 = (l585 + 1)) {
			fRec837[l585] = 0.0f;
			
		}
		for (int l586 = 0; (l586 < 2); l586 = (l586 + 1)) {
			fRec836[l586] = 0.0f;
			
		}
		for (int l587 = 0; (l587 < 2); l587 = (l587 + 1)) {
			fRec834[l587] = 0.0f;
			
		}
		for (int l588 = 0; (l588 < 2); l588 = (l588 + 1)) {
			fRec833[l588] = 0.0f;
			
		}
		for (int l589 = 0; (l589 < 2); l589 = (l589 + 1)) {
			fRec831[l589] = 0.0f;
			
		}
		for (int l590 = 0; (l590 < 2); l590 = (l590 + 1)) {
			fRec842[l590] = 0.0f;
			
		}
		for (int l591 = 0; (l591 < 2); l591 = (l591 + 1)) {
			fRec840[l591] = 0.0f;
			
		}
		for (int l592 = 0; (l592 < 2); l592 = (l592 + 1)) {
			fRec848[l592] = 0.0f;
			
		}
		for (int l593 = 0; (l593 < 2); l593 = (l593 + 1)) {
			fRec846[l593] = 0.0f;
			
		}
		for (int l594 = 0; (l594 < 2); l594 = (l594 + 1)) {
			fRec845[l594] = 0.0f;
			
		}
		for (int l595 = 0; (l595 < 2); l595 = (l595 + 1)) {
			fRec843[l595] = 0.0f;
			
		}
		for (int l596 = 0; (l596 < 1024); l596 = (l596 + 1)) {
			fVec17[l596] = 0.0f;
			
		}
		for (int l597 = 0; (l597 < 2); l597 = (l597 + 1)) {
			fRec863[l597] = 0.0f;
			
		}
		for (int l598 = 0; (l598 < 2); l598 = (l598 + 1)) {
			fRec861[l598] = 0.0f;
			
		}
		for (int l599 = 0; (l599 < 2); l599 = (l599 + 1)) {
			fRec860[l599] = 0.0f;
			
		}
		for (int l600 = 0; (l600 < 2); l600 = (l600 + 1)) {
			fRec858[l600] = 0.0f;
			
		}
		for (int l601 = 0; (l601 < 2); l601 = (l601 + 1)) {
			fRec857[l601] = 0.0f;
			
		}
		for (int l602 = 0; (l602 < 2); l602 = (l602 + 1)) {
			fRec855[l602] = 0.0f;
			
		}
		for (int l603 = 0; (l603 < 2); l603 = (l603 + 1)) {
			fRec854[l603] = 0.0f;
			
		}
		for (int l604 = 0; (l604 < 2); l604 = (l604 + 1)) {
			fRec852[l604] = 0.0f;
			
		}
		for (int l605 = 0; (l605 < 2); l605 = (l605 + 1)) {
			fRec851[l605] = 0.0f;
			
		}
		for (int l606 = 0; (l606 < 2); l606 = (l606 + 1)) {
			fRec849[l606] = 0.0f;
			
		}
		for (int l607 = 0; (l607 < 2); l607 = (l607 + 1)) {
			fRec875[l607] = 0.0f;
			
		}
		for (int l608 = 0; (l608 < 2); l608 = (l608 + 1)) {
			fRec873[l608] = 0.0f;
			
		}
		for (int l609 = 0; (l609 < 2); l609 = (l609 + 1)) {
			fRec872[l609] = 0.0f;
			
		}
		for (int l610 = 0; (l610 < 2); l610 = (l610 + 1)) {
			fRec870[l610] = 0.0f;
			
		}
		for (int l611 = 0; (l611 < 2); l611 = (l611 + 1)) {
			fRec869[l611] = 0.0f;
			
		}
		for (int l612 = 0; (l612 < 2); l612 = (l612 + 1)) {
			fRec867[l612] = 0.0f;
			
		}
		for (int l613 = 0; (l613 < 2); l613 = (l613 + 1)) {
			fRec866[l613] = 0.0f;
			
		}
		for (int l614 = 0; (l614 < 2); l614 = (l614 + 1)) {
			fRec864[l614] = 0.0f;
			
		}
		for (int l615 = 0; (l615 < 2); l615 = (l615 + 1)) {
			fRec884[l615] = 0.0f;
			
		}
		for (int l616 = 0; (l616 < 2); l616 = (l616 + 1)) {
			fRec882[l616] = 0.0f;
			
		}
		for (int l617 = 0; (l617 < 2); l617 = (l617 + 1)) {
			fRec881[l617] = 0.0f;
			
		}
		for (int l618 = 0; (l618 < 2); l618 = (l618 + 1)) {
			fRec879[l618] = 0.0f;
			
		}
		for (int l619 = 0; (l619 < 2); l619 = (l619 + 1)) {
			fRec878[l619] = 0.0f;
			
		}
		for (int l620 = 0; (l620 < 2); l620 = (l620 + 1)) {
			fRec876[l620] = 0.0f;
			
		}
		for (int l621 = 0; (l621 < 2); l621 = (l621 + 1)) {
			fRec887[l621] = 0.0f;
			
		}
		for (int l622 = 0; (l622 < 2); l622 = (l622 + 1)) {
			fRec885[l622] = 0.0f;
			
		}
		for (int l623 = 0; (l623 < 2); l623 = (l623 + 1)) {
			fRec893[l623] = 0.0f;
			
		}
		for (int l624 = 0; (l624 < 2); l624 = (l624 + 1)) {
			fRec891[l624] = 0.0f;
			
		}
		for (int l625 = 0; (l625 < 2); l625 = (l625 + 1)) {
			fRec890[l625] = 0.0f;
			
		}
		for (int l626 = 0; (l626 < 2); l626 = (l626 + 1)) {
			fRec888[l626] = 0.0f;
			
		}
		for (int l627 = 0; (l627 < 1024); l627 = (l627 + 1)) {
			fVec18[l627] = 0.0f;
			
		}
		for (int l628 = 0; (l628 < 2); l628 = (l628 + 1)) {
			fRec905[l628] = 0.0f;
			
		}
		for (int l629 = 0; (l629 < 2); l629 = (l629 + 1)) {
			fRec903[l629] = 0.0f;
			
		}
		for (int l630 = 0; (l630 < 2); l630 = (l630 + 1)) {
			fRec902[l630] = 0.0f;
			
		}
		for (int l631 = 0; (l631 < 2); l631 = (l631 + 1)) {
			fRec900[l631] = 0.0f;
			
		}
		for (int l632 = 0; (l632 < 2); l632 = (l632 + 1)) {
			fRec899[l632] = 0.0f;
			
		}
		for (int l633 = 0; (l633 < 2); l633 = (l633 + 1)) {
			fRec897[l633] = 0.0f;
			
		}
		for (int l634 = 0; (l634 < 2); l634 = (l634 + 1)) {
			fRec896[l634] = 0.0f;
			
		}
		for (int l635 = 0; (l635 < 2); l635 = (l635 + 1)) {
			fRec894[l635] = 0.0f;
			
		}
		for (int l636 = 0; (l636 < 2); l636 = (l636 + 1)) {
			fRec914[l636] = 0.0f;
			
		}
		for (int l637 = 0; (l637 < 2); l637 = (l637 + 1)) {
			fRec912[l637] = 0.0f;
			
		}
		for (int l638 = 0; (l638 < 2); l638 = (l638 + 1)) {
			fRec911[l638] = 0.0f;
			
		}
		for (int l639 = 0; (l639 < 2); l639 = (l639 + 1)) {
			fRec909[l639] = 0.0f;
			
		}
		for (int l640 = 0; (l640 < 2); l640 = (l640 + 1)) {
			fRec908[l640] = 0.0f;
			
		}
		for (int l641 = 0; (l641 < 2); l641 = (l641 + 1)) {
			fRec906[l641] = 0.0f;
			
		}
		for (int l642 = 0; (l642 < 2); l642 = (l642 + 1)) {
			fRec920[l642] = 0.0f;
			
		}
		for (int l643 = 0; (l643 < 2); l643 = (l643 + 1)) {
			fRec918[l643] = 0.0f;
			
		}
		for (int l644 = 0; (l644 < 2); l644 = (l644 + 1)) {
			fRec917[l644] = 0.0f;
			
		}
		for (int l645 = 0; (l645 < 2); l645 = (l645 + 1)) {
			fRec915[l645] = 0.0f;
			
		}
		for (int l646 = 0; (l646 < 2); l646 = (l646 + 1)) {
			fRec935[l646] = 0.0f;
			
		}
		for (int l647 = 0; (l647 < 2); l647 = (l647 + 1)) {
			fRec933[l647] = 0.0f;
			
		}
		for (int l648 = 0; (l648 < 2); l648 = (l648 + 1)) {
			fRec932[l648] = 0.0f;
			
		}
		for (int l649 = 0; (l649 < 2); l649 = (l649 + 1)) {
			fRec930[l649] = 0.0f;
			
		}
		for (int l650 = 0; (l650 < 2); l650 = (l650 + 1)) {
			fRec929[l650] = 0.0f;
			
		}
		for (int l651 = 0; (l651 < 2); l651 = (l651 + 1)) {
			fRec927[l651] = 0.0f;
			
		}
		for (int l652 = 0; (l652 < 2); l652 = (l652 + 1)) {
			fRec926[l652] = 0.0f;
			
		}
		for (int l653 = 0; (l653 < 2); l653 = (l653 + 1)) {
			fRec924[l653] = 0.0f;
			
		}
		for (int l654 = 0; (l654 < 2); l654 = (l654 + 1)) {
			fRec923[l654] = 0.0f;
			
		}
		for (int l655 = 0; (l655 < 2); l655 = (l655 + 1)) {
			fRec921[l655] = 0.0f;
			
		}
		for (int l656 = 0; (l656 < 2); l656 = (l656 + 1)) {
			fRec938[l656] = 0.0f;
			
		}
		for (int l657 = 0; (l657 < 2); l657 = (l657 + 1)) {
			fRec936[l657] = 0.0f;
			
		}
		for (int l658 = 0; (l658 < 2); l658 = (l658 + 1)) {
			fVec19[l658] = 0.0f;
			
		}
		for (int l659 = 0; (l659 < 2); l659 = (l659 + 1)) {
			fRec953[l659] = 0.0f;
			
		}
		for (int l660 = 0; (l660 < 2); l660 = (l660 + 1)) {
			fRec951[l660] = 0.0f;
			
		}
		for (int l661 = 0; (l661 < 2); l661 = (l661 + 1)) {
			fRec950[l661] = 0.0f;
			
		}
		for (int l662 = 0; (l662 < 2); l662 = (l662 + 1)) {
			fRec948[l662] = 0.0f;
			
		}
		for (int l663 = 0; (l663 < 2); l663 = (l663 + 1)) {
			fRec947[l663] = 0.0f;
			
		}
		for (int l664 = 0; (l664 < 2); l664 = (l664 + 1)) {
			fRec945[l664] = 0.0f;
			
		}
		for (int l665 = 0; (l665 < 2); l665 = (l665 + 1)) {
			fRec944[l665] = 0.0f;
			
		}
		for (int l666 = 0; (l666 < 2); l666 = (l666 + 1)) {
			fRec942[l666] = 0.0f;
			
		}
		for (int l667 = 0; (l667 < 2); l667 = (l667 + 1)) {
			fRec941[l667] = 0.0f;
			
		}
		for (int l668 = 0; (l668 < 2); l668 = (l668 + 1)) {
			fRec939[l668] = 0.0f;
			
		}
		for (int l669 = 0; (l669 < 2); l669 = (l669 + 1)) {
			fRec965[l669] = 0.0f;
			
		}
		for (int l670 = 0; (l670 < 2); l670 = (l670 + 1)) {
			fRec963[l670] = 0.0f;
			
		}
		for (int l671 = 0; (l671 < 2); l671 = (l671 + 1)) {
			fRec962[l671] = 0.0f;
			
		}
		for (int l672 = 0; (l672 < 2); l672 = (l672 + 1)) {
			fRec960[l672] = 0.0f;
			
		}
		for (int l673 = 0; (l673 < 2); l673 = (l673 + 1)) {
			fRec959[l673] = 0.0f;
			
		}
		for (int l674 = 0; (l674 < 2); l674 = (l674 + 1)) {
			fRec957[l674] = 0.0f;
			
		}
		for (int l675 = 0; (l675 < 2); l675 = (l675 + 1)) {
			fRec956[l675] = 0.0f;
			
		}
		for (int l676 = 0; (l676 < 2); l676 = (l676 + 1)) {
			fRec954[l676] = 0.0f;
			
		}
		for (int l677 = 0; (l677 < 2); l677 = (l677 + 1)) {
			fRec974[l677] = 0.0f;
			
		}
		for (int l678 = 0; (l678 < 2); l678 = (l678 + 1)) {
			fRec972[l678] = 0.0f;
			
		}
		for (int l679 = 0; (l679 < 2); l679 = (l679 + 1)) {
			fRec971[l679] = 0.0f;
			
		}
		for (int l680 = 0; (l680 < 2); l680 = (l680 + 1)) {
			fRec969[l680] = 0.0f;
			
		}
		for (int l681 = 0; (l681 < 2); l681 = (l681 + 1)) {
			fRec968[l681] = 0.0f;
			
		}
		for (int l682 = 0; (l682 < 2); l682 = (l682 + 1)) {
			fRec966[l682] = 0.0f;
			
		}
		for (int l683 = 0; (l683 < 2); l683 = (l683 + 1)) {
			fRec977[l683] = 0.0f;
			
		}
		for (int l684 = 0; (l684 < 2); l684 = (l684 + 1)) {
			fRec975[l684] = 0.0f;
			
		}
		for (int l685 = 0; (l685 < 2); l685 = (l685 + 1)) {
			fRec983[l685] = 0.0f;
			
		}
		for (int l686 = 0; (l686 < 2); l686 = (l686 + 1)) {
			fRec981[l686] = 0.0f;
			
		}
		for (int l687 = 0; (l687 < 2); l687 = (l687 + 1)) {
			fRec980[l687] = 0.0f;
			
		}
		for (int l688 = 0; (l688 < 2); l688 = (l688 + 1)) {
			fRec978[l688] = 0.0f;
			
		}
		for (int l689 = 0; (l689 < 2); l689 = (l689 + 1)) {
			fRec998[l689] = 0.0f;
			
		}
		for (int l690 = 0; (l690 < 2); l690 = (l690 + 1)) {
			fRec996[l690] = 0.0f;
			
		}
		for (int l691 = 0; (l691 < 2); l691 = (l691 + 1)) {
			fRec995[l691] = 0.0f;
			
		}
		for (int l692 = 0; (l692 < 2); l692 = (l692 + 1)) {
			fRec993[l692] = 0.0f;
			
		}
		for (int l693 = 0; (l693 < 2); l693 = (l693 + 1)) {
			fRec992[l693] = 0.0f;
			
		}
		for (int l694 = 0; (l694 < 2); l694 = (l694 + 1)) {
			fRec990[l694] = 0.0f;
			
		}
		for (int l695 = 0; (l695 < 2); l695 = (l695 + 1)) {
			fRec989[l695] = 0.0f;
			
		}
		for (int l696 = 0; (l696 < 2); l696 = (l696 + 1)) {
			fRec987[l696] = 0.0f;
			
		}
		for (int l697 = 0; (l697 < 2); l697 = (l697 + 1)) {
			fRec986[l697] = 0.0f;
			
		}
		for (int l698 = 0; (l698 < 2); l698 = (l698 + 1)) {
			fRec984[l698] = 0.0f;
			
		}
		for (int l699 = 0; (l699 < 2); l699 = (l699 + 1)) {
			fRec1010[l699] = 0.0f;
			
		}
		for (int l700 = 0; (l700 < 2); l700 = (l700 + 1)) {
			fRec1008[l700] = 0.0f;
			
		}
		for (int l701 = 0; (l701 < 2); l701 = (l701 + 1)) {
			fRec1007[l701] = 0.0f;
			
		}
		for (int l702 = 0; (l702 < 2); l702 = (l702 + 1)) {
			fRec1005[l702] = 0.0f;
			
		}
		for (int l703 = 0; (l703 < 2); l703 = (l703 + 1)) {
			fRec1004[l703] = 0.0f;
			
		}
		for (int l704 = 0; (l704 < 2); l704 = (l704 + 1)) {
			fRec1002[l704] = 0.0f;
			
		}
		for (int l705 = 0; (l705 < 2); l705 = (l705 + 1)) {
			fRec1001[l705] = 0.0f;
			
		}
		for (int l706 = 0; (l706 < 2); l706 = (l706 + 1)) {
			fRec999[l706] = 0.0f;
			
		}
		for (int l707 = 0; (l707 < 2); l707 = (l707 + 1)) {
			fRec1019[l707] = 0.0f;
			
		}
		for (int l708 = 0; (l708 < 2); l708 = (l708 + 1)) {
			fRec1017[l708] = 0.0f;
			
		}
		for (int l709 = 0; (l709 < 2); l709 = (l709 + 1)) {
			fRec1016[l709] = 0.0f;
			
		}
		for (int l710 = 0; (l710 < 2); l710 = (l710 + 1)) {
			fRec1014[l710] = 0.0f;
			
		}
		for (int l711 = 0; (l711 < 2); l711 = (l711 + 1)) {
			fRec1013[l711] = 0.0f;
			
		}
		for (int l712 = 0; (l712 < 2); l712 = (l712 + 1)) {
			fRec1011[l712] = 0.0f;
			
		}
		for (int l713 = 0; (l713 < 2); l713 = (l713 + 1)) {
			fRec1022[l713] = 0.0f;
			
		}
		for (int l714 = 0; (l714 < 2); l714 = (l714 + 1)) {
			fRec1020[l714] = 0.0f;
			
		}
		for (int l715 = 0; (l715 < 2); l715 = (l715 + 1)) {
			fRec1028[l715] = 0.0f;
			
		}
		for (int l716 = 0; (l716 < 2); l716 = (l716 + 1)) {
			fRec1026[l716] = 0.0f;
			
		}
		for (int l717 = 0; (l717 < 2); l717 = (l717 + 1)) {
			fRec1025[l717] = 0.0f;
			
		}
		for (int l718 = 0; (l718 < 2); l718 = (l718 + 1)) {
			fRec1023[l718] = 0.0f;
			
		}
		for (int l719 = 0; (l719 < 2); l719 = (l719 + 1)) {
			fRec1034[l719] = 0.0f;
			
		}
		for (int l720 = 0; (l720 < 2); l720 = (l720 + 1)) {
			fRec1032[l720] = 0.0f;
			
		}
		for (int l721 = 0; (l721 < 2); l721 = (l721 + 1)) {
			fRec1031[l721] = 0.0f;
			
		}
		for (int l722 = 0; (l722 < 2); l722 = (l722 + 1)) {
			fRec1029[l722] = 0.0f;
			
		}
		for (int l723 = 0; (l723 < 2); l723 = (l723 + 1)) {
			fRec1037[l723] = 0.0f;
			
		}
		for (int l724 = 0; (l724 < 2); l724 = (l724 + 1)) {
			fRec1035[l724] = 0.0f;
			
		}
		for (int l725 = 0; (l725 < 2); l725 = (l725 + 1)) {
			fRec1052[l725] = 0.0f;
			
		}
		for (int l726 = 0; (l726 < 2); l726 = (l726 + 1)) {
			fRec1050[l726] = 0.0f;
			
		}
		for (int l727 = 0; (l727 < 2); l727 = (l727 + 1)) {
			fRec1049[l727] = 0.0f;
			
		}
		for (int l728 = 0; (l728 < 2); l728 = (l728 + 1)) {
			fRec1047[l728] = 0.0f;
			
		}
		for (int l729 = 0; (l729 < 2); l729 = (l729 + 1)) {
			fRec1046[l729] = 0.0f;
			
		}
		for (int l730 = 0; (l730 < 2); l730 = (l730 + 1)) {
			fRec1044[l730] = 0.0f;
			
		}
		for (int l731 = 0; (l731 < 2); l731 = (l731 + 1)) {
			fRec1043[l731] = 0.0f;
			
		}
		for (int l732 = 0; (l732 < 2); l732 = (l732 + 1)) {
			fRec1041[l732] = 0.0f;
			
		}
		for (int l733 = 0; (l733 < 2); l733 = (l733 + 1)) {
			fRec1040[l733] = 0.0f;
			
		}
		for (int l734 = 0; (l734 < 2); l734 = (l734 + 1)) {
			fRec1038[l734] = 0.0f;
			
		}
		for (int l735 = 0; (l735 < 2); l735 = (l735 + 1)) {
			fRec1061[l735] = 0.0f;
			
		}
		for (int l736 = 0; (l736 < 2); l736 = (l736 + 1)) {
			fRec1059[l736] = 0.0f;
			
		}
		for (int l737 = 0; (l737 < 2); l737 = (l737 + 1)) {
			fRec1058[l737] = 0.0f;
			
		}
		for (int l738 = 0; (l738 < 2); l738 = (l738 + 1)) {
			fRec1056[l738] = 0.0f;
			
		}
		for (int l739 = 0; (l739 < 2); l739 = (l739 + 1)) {
			fRec1055[l739] = 0.0f;
			
		}
		for (int l740 = 0; (l740 < 2); l740 = (l740 + 1)) {
			fRec1053[l740] = 0.0f;
			
		}
		for (int l741 = 0; (l741 < 2); l741 = (l741 + 1)) {
			fRec1073[l741] = 0.0f;
			
		}
		for (int l742 = 0; (l742 < 2); l742 = (l742 + 1)) {
			fRec1071[l742] = 0.0f;
			
		}
		for (int l743 = 0; (l743 < 2); l743 = (l743 + 1)) {
			fRec1070[l743] = 0.0f;
			
		}
		for (int l744 = 0; (l744 < 2); l744 = (l744 + 1)) {
			fRec1068[l744] = 0.0f;
			
		}
		for (int l745 = 0; (l745 < 2); l745 = (l745 + 1)) {
			fRec1067[l745] = 0.0f;
			
		}
		for (int l746 = 0; (l746 < 2); l746 = (l746 + 1)) {
			fRec1065[l746] = 0.0f;
			
		}
		for (int l747 = 0; (l747 < 2); l747 = (l747 + 1)) {
			fRec1064[l747] = 0.0f;
			
		}
		for (int l748 = 0; (l748 < 2); l748 = (l748 + 1)) {
			fRec1062[l748] = 0.0f;
			
		}
		for (int l749 = 0; (l749 < 2); l749 = (l749 + 1)) {
			fVec20[l749] = 0.0f;
			
		}
		for (int l750 = 0; (l750 < 2); l750 = (l750 + 1)) {
			fRec1088[l750] = 0.0f;
			
		}
		for (int l751 = 0; (l751 < 2); l751 = (l751 + 1)) {
			fRec1086[l751] = 0.0f;
			
		}
		for (int l752 = 0; (l752 < 2); l752 = (l752 + 1)) {
			fRec1085[l752] = 0.0f;
			
		}
		for (int l753 = 0; (l753 < 2); l753 = (l753 + 1)) {
			fRec1083[l753] = 0.0f;
			
		}
		for (int l754 = 0; (l754 < 2); l754 = (l754 + 1)) {
			fRec1082[l754] = 0.0f;
			
		}
		for (int l755 = 0; (l755 < 2); l755 = (l755 + 1)) {
			fRec1080[l755] = 0.0f;
			
		}
		for (int l756 = 0; (l756 < 2); l756 = (l756 + 1)) {
			fRec1079[l756] = 0.0f;
			
		}
		for (int l757 = 0; (l757 < 2); l757 = (l757 + 1)) {
			fRec1077[l757] = 0.0f;
			
		}
		for (int l758 = 0; (l758 < 2); l758 = (l758 + 1)) {
			fRec1076[l758] = 0.0f;
			
		}
		for (int l759 = 0; (l759 < 2); l759 = (l759 + 1)) {
			fRec1074[l759] = 0.0f;
			
		}
		for (int l760 = 0; (l760 < 2); l760 = (l760 + 1)) {
			fRec1100[l760] = 0.0f;
			
		}
		for (int l761 = 0; (l761 < 2); l761 = (l761 + 1)) {
			fRec1098[l761] = 0.0f;
			
		}
		for (int l762 = 0; (l762 < 2); l762 = (l762 + 1)) {
			fRec1097[l762] = 0.0f;
			
		}
		for (int l763 = 0; (l763 < 2); l763 = (l763 + 1)) {
			fRec1095[l763] = 0.0f;
			
		}
		for (int l764 = 0; (l764 < 2); l764 = (l764 + 1)) {
			fRec1094[l764] = 0.0f;
			
		}
		for (int l765 = 0; (l765 < 2); l765 = (l765 + 1)) {
			fRec1092[l765] = 0.0f;
			
		}
		for (int l766 = 0; (l766 < 2); l766 = (l766 + 1)) {
			fRec1091[l766] = 0.0f;
			
		}
		for (int l767 = 0; (l767 < 2); l767 = (l767 + 1)) {
			fRec1089[l767] = 0.0f;
			
		}
		for (int l768 = 0; (l768 < 2); l768 = (l768 + 1)) {
			fRec1109[l768] = 0.0f;
			
		}
		for (int l769 = 0; (l769 < 2); l769 = (l769 + 1)) {
			fRec1107[l769] = 0.0f;
			
		}
		for (int l770 = 0; (l770 < 2); l770 = (l770 + 1)) {
			fRec1106[l770] = 0.0f;
			
		}
		for (int l771 = 0; (l771 < 2); l771 = (l771 + 1)) {
			fRec1104[l771] = 0.0f;
			
		}
		for (int l772 = 0; (l772 < 2); l772 = (l772 + 1)) {
			fRec1103[l772] = 0.0f;
			
		}
		for (int l773 = 0; (l773 < 2); l773 = (l773 + 1)) {
			fRec1101[l773] = 0.0f;
			
		}
		for (int l774 = 0; (l774 < 2); l774 = (l774 + 1)) {
			fRec1112[l774] = 0.0f;
			
		}
		for (int l775 = 0; (l775 < 2); l775 = (l775 + 1)) {
			fRec1110[l775] = 0.0f;
			
		}
		for (int l776 = 0; (l776 < 2); l776 = (l776 + 1)) {
			fRec1118[l776] = 0.0f;
			
		}
		for (int l777 = 0; (l777 < 2); l777 = (l777 + 1)) {
			fRec1116[l777] = 0.0f;
			
		}
		for (int l778 = 0; (l778 < 2); l778 = (l778 + 1)) {
			fRec1115[l778] = 0.0f;
			
		}
		for (int l779 = 0; (l779 < 2); l779 = (l779 + 1)) {
			fRec1113[l779] = 0.0f;
			
		}
		for (int l780 = 0; (l780 < 2); l780 = (l780 + 1)) {
			fRec1133[l780] = 0.0f;
			
		}
		for (int l781 = 0; (l781 < 2); l781 = (l781 + 1)) {
			fRec1131[l781] = 0.0f;
			
		}
		for (int l782 = 0; (l782 < 2); l782 = (l782 + 1)) {
			fRec1130[l782] = 0.0f;
			
		}
		for (int l783 = 0; (l783 < 2); l783 = (l783 + 1)) {
			fRec1128[l783] = 0.0f;
			
		}
		for (int l784 = 0; (l784 < 2); l784 = (l784 + 1)) {
			fRec1127[l784] = 0.0f;
			
		}
		for (int l785 = 0; (l785 < 2); l785 = (l785 + 1)) {
			fRec1125[l785] = 0.0f;
			
		}
		for (int l786 = 0; (l786 < 2); l786 = (l786 + 1)) {
			fRec1124[l786] = 0.0f;
			
		}
		for (int l787 = 0; (l787 < 2); l787 = (l787 + 1)) {
			fRec1122[l787] = 0.0f;
			
		}
		for (int l788 = 0; (l788 < 2); l788 = (l788 + 1)) {
			fRec1121[l788] = 0.0f;
			
		}
		for (int l789 = 0; (l789 < 2); l789 = (l789 + 1)) {
			fRec1119[l789] = 0.0f;
			
		}
		for (int l790 = 0; (l790 < 2); l790 = (l790 + 1)) {
			fRec1145[l790] = 0.0f;
			
		}
		for (int l791 = 0; (l791 < 2); l791 = (l791 + 1)) {
			fRec1143[l791] = 0.0f;
			
		}
		for (int l792 = 0; (l792 < 2); l792 = (l792 + 1)) {
			fRec1142[l792] = 0.0f;
			
		}
		for (int l793 = 0; (l793 < 2); l793 = (l793 + 1)) {
			fRec1140[l793] = 0.0f;
			
		}
		for (int l794 = 0; (l794 < 2); l794 = (l794 + 1)) {
			fRec1139[l794] = 0.0f;
			
		}
		for (int l795 = 0; (l795 < 2); l795 = (l795 + 1)) {
			fRec1137[l795] = 0.0f;
			
		}
		for (int l796 = 0; (l796 < 2); l796 = (l796 + 1)) {
			fRec1136[l796] = 0.0f;
			
		}
		for (int l797 = 0; (l797 < 2); l797 = (l797 + 1)) {
			fRec1134[l797] = 0.0f;
			
		}
		for (int l798 = 0; (l798 < 2); l798 = (l798 + 1)) {
			fRec1154[l798] = 0.0f;
			
		}
		for (int l799 = 0; (l799 < 2); l799 = (l799 + 1)) {
			fRec1152[l799] = 0.0f;
			
		}
		for (int l800 = 0; (l800 < 2); l800 = (l800 + 1)) {
			fRec1151[l800] = 0.0f;
			
		}
		for (int l801 = 0; (l801 < 2); l801 = (l801 + 1)) {
			fRec1149[l801] = 0.0f;
			
		}
		for (int l802 = 0; (l802 < 2); l802 = (l802 + 1)) {
			fRec1148[l802] = 0.0f;
			
		}
		for (int l803 = 0; (l803 < 2); l803 = (l803 + 1)) {
			fRec1146[l803] = 0.0f;
			
		}
		for (int l804 = 0; (l804 < 2); l804 = (l804 + 1)) {
			fRec1157[l804] = 0.0f;
			
		}
		for (int l805 = 0; (l805 < 2); l805 = (l805 + 1)) {
			fRec1155[l805] = 0.0f;
			
		}
		for (int l806 = 0; (l806 < 2); l806 = (l806 + 1)) {
			fRec1163[l806] = 0.0f;
			
		}
		for (int l807 = 0; (l807 < 2); l807 = (l807 + 1)) {
			fRec1161[l807] = 0.0f;
			
		}
		for (int l808 = 0; (l808 < 2); l808 = (l808 + 1)) {
			fRec1160[l808] = 0.0f;
			
		}
		for (int l809 = 0; (l809 < 2); l809 = (l809 + 1)) {
			fRec1158[l809] = 0.0f;
			
		}
		for (int l810 = 0; (l810 < 2); l810 = (l810 + 1)) {
			fRec1178[l810] = 0.0f;
			
		}
		for (int l811 = 0; (l811 < 2); l811 = (l811 + 1)) {
			fRec1176[l811] = 0.0f;
			
		}
		for (int l812 = 0; (l812 < 2); l812 = (l812 + 1)) {
			fRec1175[l812] = 0.0f;
			
		}
		for (int l813 = 0; (l813 < 2); l813 = (l813 + 1)) {
			fRec1173[l813] = 0.0f;
			
		}
		for (int l814 = 0; (l814 < 2); l814 = (l814 + 1)) {
			fRec1172[l814] = 0.0f;
			
		}
		for (int l815 = 0; (l815 < 2); l815 = (l815 + 1)) {
			fRec1170[l815] = 0.0f;
			
		}
		for (int l816 = 0; (l816 < 2); l816 = (l816 + 1)) {
			fRec1169[l816] = 0.0f;
			
		}
		for (int l817 = 0; (l817 < 2); l817 = (l817 + 1)) {
			fRec1167[l817] = 0.0f;
			
		}
		for (int l818 = 0; (l818 < 2); l818 = (l818 + 1)) {
			fRec1166[l818] = 0.0f;
			
		}
		for (int l819 = 0; (l819 < 2); l819 = (l819 + 1)) {
			fRec1164[l819] = 0.0f;
			
		}
		for (int l820 = 0; (l820 < 2); l820 = (l820 + 1)) {
			fRec1190[l820] = 0.0f;
			
		}
		for (int l821 = 0; (l821 < 2); l821 = (l821 + 1)) {
			fRec1188[l821] = 0.0f;
			
		}
		for (int l822 = 0; (l822 < 2); l822 = (l822 + 1)) {
			fRec1187[l822] = 0.0f;
			
		}
		for (int l823 = 0; (l823 < 2); l823 = (l823 + 1)) {
			fRec1185[l823] = 0.0f;
			
		}
		for (int l824 = 0; (l824 < 2); l824 = (l824 + 1)) {
			fRec1184[l824] = 0.0f;
			
		}
		for (int l825 = 0; (l825 < 2); l825 = (l825 + 1)) {
			fRec1182[l825] = 0.0f;
			
		}
		for (int l826 = 0; (l826 < 2); l826 = (l826 + 1)) {
			fRec1181[l826] = 0.0f;
			
		}
		for (int l827 = 0; (l827 < 2); l827 = (l827 + 1)) {
			fRec1179[l827] = 0.0f;
			
		}
		for (int l828 = 0; (l828 < 2); l828 = (l828 + 1)) {
			fRec1199[l828] = 0.0f;
			
		}
		for (int l829 = 0; (l829 < 2); l829 = (l829 + 1)) {
			fRec1197[l829] = 0.0f;
			
		}
		for (int l830 = 0; (l830 < 2); l830 = (l830 + 1)) {
			fRec1196[l830] = 0.0f;
			
		}
		for (int l831 = 0; (l831 < 2); l831 = (l831 + 1)) {
			fRec1194[l831] = 0.0f;
			
		}
		for (int l832 = 0; (l832 < 2); l832 = (l832 + 1)) {
			fRec1193[l832] = 0.0f;
			
		}
		for (int l833 = 0; (l833 < 2); l833 = (l833 + 1)) {
			fRec1191[l833] = 0.0f;
			
		}
		for (int l834 = 0; (l834 < 2); l834 = (l834 + 1)) {
			fRec1202[l834] = 0.0f;
			
		}
		for (int l835 = 0; (l835 < 2); l835 = (l835 + 1)) {
			fRec1200[l835] = 0.0f;
			
		}
		for (int l836 = 0; (l836 < 2); l836 = (l836 + 1)) {
			fRec1208[l836] = 0.0f;
			
		}
		for (int l837 = 0; (l837 < 2); l837 = (l837 + 1)) {
			fRec1206[l837] = 0.0f;
			
		}
		for (int l838 = 0; (l838 < 2); l838 = (l838 + 1)) {
			fRec1205[l838] = 0.0f;
			
		}
		for (int l839 = 0; (l839 < 2); l839 = (l839 + 1)) {
			fRec1203[l839] = 0.0f;
			
		}
		for (int l840 = 0; (l840 < 2); l840 = (l840 + 1)) {
			fVec21[l840] = 0.0f;
			
		}
		for (int l841 = 0; (l841 < 2); l841 = (l841 + 1)) {
			fRec1223[l841] = 0.0f;
			
		}
		for (int l842 = 0; (l842 < 2); l842 = (l842 + 1)) {
			fRec1221[l842] = 0.0f;
			
		}
		for (int l843 = 0; (l843 < 2); l843 = (l843 + 1)) {
			fRec1220[l843] = 0.0f;
			
		}
		for (int l844 = 0; (l844 < 2); l844 = (l844 + 1)) {
			fRec1218[l844] = 0.0f;
			
		}
		for (int l845 = 0; (l845 < 2); l845 = (l845 + 1)) {
			fRec1217[l845] = 0.0f;
			
		}
		for (int l846 = 0; (l846 < 2); l846 = (l846 + 1)) {
			fRec1215[l846] = 0.0f;
			
		}
		for (int l847 = 0; (l847 < 2); l847 = (l847 + 1)) {
			fRec1214[l847] = 0.0f;
			
		}
		for (int l848 = 0; (l848 < 2); l848 = (l848 + 1)) {
			fRec1212[l848] = 0.0f;
			
		}
		for (int l849 = 0; (l849 < 2); l849 = (l849 + 1)) {
			fRec1211[l849] = 0.0f;
			
		}
		for (int l850 = 0; (l850 < 2); l850 = (l850 + 1)) {
			fRec1209[l850] = 0.0f;
			
		}
		for (int l851 = 0; (l851 < 2); l851 = (l851 + 1)) {
			fRec1235[l851] = 0.0f;
			
		}
		for (int l852 = 0; (l852 < 2); l852 = (l852 + 1)) {
			fRec1233[l852] = 0.0f;
			
		}
		for (int l853 = 0; (l853 < 2); l853 = (l853 + 1)) {
			fRec1232[l853] = 0.0f;
			
		}
		for (int l854 = 0; (l854 < 2); l854 = (l854 + 1)) {
			fRec1230[l854] = 0.0f;
			
		}
		for (int l855 = 0; (l855 < 2); l855 = (l855 + 1)) {
			fRec1229[l855] = 0.0f;
			
		}
		for (int l856 = 0; (l856 < 2); l856 = (l856 + 1)) {
			fRec1227[l856] = 0.0f;
			
		}
		for (int l857 = 0; (l857 < 2); l857 = (l857 + 1)) {
			fRec1226[l857] = 0.0f;
			
		}
		for (int l858 = 0; (l858 < 2); l858 = (l858 + 1)) {
			fRec1224[l858] = 0.0f;
			
		}
		for (int l859 = 0; (l859 < 2); l859 = (l859 + 1)) {
			fRec1244[l859] = 0.0f;
			
		}
		for (int l860 = 0; (l860 < 2); l860 = (l860 + 1)) {
			fRec1242[l860] = 0.0f;
			
		}
		for (int l861 = 0; (l861 < 2); l861 = (l861 + 1)) {
			fRec1241[l861] = 0.0f;
			
		}
		for (int l862 = 0; (l862 < 2); l862 = (l862 + 1)) {
			fRec1239[l862] = 0.0f;
			
		}
		for (int l863 = 0; (l863 < 2); l863 = (l863 + 1)) {
			fRec1238[l863] = 0.0f;
			
		}
		for (int l864 = 0; (l864 < 2); l864 = (l864 + 1)) {
			fRec1236[l864] = 0.0f;
			
		}
		for (int l865 = 0; (l865 < 2); l865 = (l865 + 1)) {
			fRec1247[l865] = 0.0f;
			
		}
		for (int l866 = 0; (l866 < 2); l866 = (l866 + 1)) {
			fRec1245[l866] = 0.0f;
			
		}
		for (int l867 = 0; (l867 < 2); l867 = (l867 + 1)) {
			fRec1253[l867] = 0.0f;
			
		}
		for (int l868 = 0; (l868 < 2); l868 = (l868 + 1)) {
			fRec1251[l868] = 0.0f;
			
		}
		for (int l869 = 0; (l869 < 2); l869 = (l869 + 1)) {
			fRec1250[l869] = 0.0f;
			
		}
		for (int l870 = 0; (l870 < 2); l870 = (l870 + 1)) {
			fRec1248[l870] = 0.0f;
			
		}
		for (int l871 = 0; (l871 < 2); l871 = (l871 + 1)) {
			fRec1268[l871] = 0.0f;
			
		}
		for (int l872 = 0; (l872 < 2); l872 = (l872 + 1)) {
			fRec1266[l872] = 0.0f;
			
		}
		for (int l873 = 0; (l873 < 2); l873 = (l873 + 1)) {
			fRec1265[l873] = 0.0f;
			
		}
		for (int l874 = 0; (l874 < 2); l874 = (l874 + 1)) {
			fRec1263[l874] = 0.0f;
			
		}
		for (int l875 = 0; (l875 < 2); l875 = (l875 + 1)) {
			fRec1262[l875] = 0.0f;
			
		}
		for (int l876 = 0; (l876 < 2); l876 = (l876 + 1)) {
			fRec1260[l876] = 0.0f;
			
		}
		for (int l877 = 0; (l877 < 2); l877 = (l877 + 1)) {
			fRec1259[l877] = 0.0f;
			
		}
		for (int l878 = 0; (l878 < 2); l878 = (l878 + 1)) {
			fRec1257[l878] = 0.0f;
			
		}
		for (int l879 = 0; (l879 < 2); l879 = (l879 + 1)) {
			fRec1256[l879] = 0.0f;
			
		}
		for (int l880 = 0; (l880 < 2); l880 = (l880 + 1)) {
			fRec1254[l880] = 0.0f;
			
		}
		for (int l881 = 0; (l881 < 2); l881 = (l881 + 1)) {
			fRec1280[l881] = 0.0f;
			
		}
		for (int l882 = 0; (l882 < 2); l882 = (l882 + 1)) {
			fRec1278[l882] = 0.0f;
			
		}
		for (int l883 = 0; (l883 < 2); l883 = (l883 + 1)) {
			fRec1277[l883] = 0.0f;
			
		}
		for (int l884 = 0; (l884 < 2); l884 = (l884 + 1)) {
			fRec1275[l884] = 0.0f;
			
		}
		for (int l885 = 0; (l885 < 2); l885 = (l885 + 1)) {
			fRec1274[l885] = 0.0f;
			
		}
		for (int l886 = 0; (l886 < 2); l886 = (l886 + 1)) {
			fRec1272[l886] = 0.0f;
			
		}
		for (int l887 = 0; (l887 < 2); l887 = (l887 + 1)) {
			fRec1271[l887] = 0.0f;
			
		}
		for (int l888 = 0; (l888 < 2); l888 = (l888 + 1)) {
			fRec1269[l888] = 0.0f;
			
		}
		for (int l889 = 0; (l889 < 2); l889 = (l889 + 1)) {
			fRec1289[l889] = 0.0f;
			
		}
		for (int l890 = 0; (l890 < 2); l890 = (l890 + 1)) {
			fRec1287[l890] = 0.0f;
			
		}
		for (int l891 = 0; (l891 < 2); l891 = (l891 + 1)) {
			fRec1286[l891] = 0.0f;
			
		}
		for (int l892 = 0; (l892 < 2); l892 = (l892 + 1)) {
			fRec1284[l892] = 0.0f;
			
		}
		for (int l893 = 0; (l893 < 2); l893 = (l893 + 1)) {
			fRec1283[l893] = 0.0f;
			
		}
		for (int l894 = 0; (l894 < 2); l894 = (l894 + 1)) {
			fRec1281[l894] = 0.0f;
			
		}
		for (int l895 = 0; (l895 < 2); l895 = (l895 + 1)) {
			fRec1292[l895] = 0.0f;
			
		}
		for (int l896 = 0; (l896 < 2); l896 = (l896 + 1)) {
			fRec1290[l896] = 0.0f;
			
		}
		for (int l897 = 0; (l897 < 2); l897 = (l897 + 1)) {
			fRec1298[l897] = 0.0f;
			
		}
		for (int l898 = 0; (l898 < 2); l898 = (l898 + 1)) {
			fRec1296[l898] = 0.0f;
			
		}
		for (int l899 = 0; (l899 < 2); l899 = (l899 + 1)) {
			fRec1295[l899] = 0.0f;
			
		}
		for (int l900 = 0; (l900 < 2); l900 = (l900 + 1)) {
			fRec1293[l900] = 0.0f;
			
		}
		for (int l901 = 0; (l901 < 2); l901 = (l901 + 1)) {
			fRec1313[l901] = 0.0f;
			
		}
		for (int l902 = 0; (l902 < 2); l902 = (l902 + 1)) {
			fRec1311[l902] = 0.0f;
			
		}
		for (int l903 = 0; (l903 < 2); l903 = (l903 + 1)) {
			fRec1310[l903] = 0.0f;
			
		}
		for (int l904 = 0; (l904 < 2); l904 = (l904 + 1)) {
			fRec1308[l904] = 0.0f;
			
		}
		for (int l905 = 0; (l905 < 2); l905 = (l905 + 1)) {
			fRec1307[l905] = 0.0f;
			
		}
		for (int l906 = 0; (l906 < 2); l906 = (l906 + 1)) {
			fRec1305[l906] = 0.0f;
			
		}
		for (int l907 = 0; (l907 < 2); l907 = (l907 + 1)) {
			fRec1304[l907] = 0.0f;
			
		}
		for (int l908 = 0; (l908 < 2); l908 = (l908 + 1)) {
			fRec1302[l908] = 0.0f;
			
		}
		for (int l909 = 0; (l909 < 2); l909 = (l909 + 1)) {
			fRec1301[l909] = 0.0f;
			
		}
		for (int l910 = 0; (l910 < 2); l910 = (l910 + 1)) {
			fRec1299[l910] = 0.0f;
			
		}
		for (int l911 = 0; (l911 < 2); l911 = (l911 + 1)) {
			fRec1325[l911] = 0.0f;
			
		}
		for (int l912 = 0; (l912 < 2); l912 = (l912 + 1)) {
			fRec1323[l912] = 0.0f;
			
		}
		for (int l913 = 0; (l913 < 2); l913 = (l913 + 1)) {
			fRec1322[l913] = 0.0f;
			
		}
		for (int l914 = 0; (l914 < 2); l914 = (l914 + 1)) {
			fRec1320[l914] = 0.0f;
			
		}
		for (int l915 = 0; (l915 < 2); l915 = (l915 + 1)) {
			fRec1319[l915] = 0.0f;
			
		}
		for (int l916 = 0; (l916 < 2); l916 = (l916 + 1)) {
			fRec1317[l916] = 0.0f;
			
		}
		for (int l917 = 0; (l917 < 2); l917 = (l917 + 1)) {
			fRec1316[l917] = 0.0f;
			
		}
		for (int l918 = 0; (l918 < 2); l918 = (l918 + 1)) {
			fRec1314[l918] = 0.0f;
			
		}
		for (int l919 = 0; (l919 < 2); l919 = (l919 + 1)) {
			fRec1334[l919] = 0.0f;
			
		}
		for (int l920 = 0; (l920 < 2); l920 = (l920 + 1)) {
			fRec1332[l920] = 0.0f;
			
		}
		for (int l921 = 0; (l921 < 2); l921 = (l921 + 1)) {
			fRec1331[l921] = 0.0f;
			
		}
		for (int l922 = 0; (l922 < 2); l922 = (l922 + 1)) {
			fRec1329[l922] = 0.0f;
			
		}
		for (int l923 = 0; (l923 < 2); l923 = (l923 + 1)) {
			fRec1328[l923] = 0.0f;
			
		}
		for (int l924 = 0; (l924 < 2); l924 = (l924 + 1)) {
			fRec1326[l924] = 0.0f;
			
		}
		for (int l925 = 0; (l925 < 2); l925 = (l925 + 1)) {
			fRec1337[l925] = 0.0f;
			
		}
		for (int l926 = 0; (l926 < 2); l926 = (l926 + 1)) {
			fRec1335[l926] = 0.0f;
			
		}
		for (int l927 = 0; (l927 < 2); l927 = (l927 + 1)) {
			fRec1343[l927] = 0.0f;
			
		}
		for (int l928 = 0; (l928 < 2); l928 = (l928 + 1)) {
			fRec1341[l928] = 0.0f;
			
		}
		for (int l929 = 0; (l929 < 2); l929 = (l929 + 1)) {
			fRec1340[l929] = 0.0f;
			
		}
		for (int l930 = 0; (l930 < 2); l930 = (l930 + 1)) {
			fRec1338[l930] = 0.0f;
			
		}
		for (int l931 = 0; (l931 < 2); l931 = (l931 + 1)) {
			fVec22[l931] = 0.0f;
			
		}
		for (int l932 = 0; (l932 < 2); l932 = (l932 + 1)) {
			fRec1358[l932] = 0.0f;
			
		}
		for (int l933 = 0; (l933 < 2); l933 = (l933 + 1)) {
			fRec1356[l933] = 0.0f;
			
		}
		for (int l934 = 0; (l934 < 2); l934 = (l934 + 1)) {
			fRec1355[l934] = 0.0f;
			
		}
		for (int l935 = 0; (l935 < 2); l935 = (l935 + 1)) {
			fRec1353[l935] = 0.0f;
			
		}
		for (int l936 = 0; (l936 < 2); l936 = (l936 + 1)) {
			fRec1352[l936] = 0.0f;
			
		}
		for (int l937 = 0; (l937 < 2); l937 = (l937 + 1)) {
			fRec1350[l937] = 0.0f;
			
		}
		for (int l938 = 0; (l938 < 2); l938 = (l938 + 1)) {
			fRec1349[l938] = 0.0f;
			
		}
		for (int l939 = 0; (l939 < 2); l939 = (l939 + 1)) {
			fRec1347[l939] = 0.0f;
			
		}
		for (int l940 = 0; (l940 < 2); l940 = (l940 + 1)) {
			fRec1346[l940] = 0.0f;
			
		}
		for (int l941 = 0; (l941 < 2); l941 = (l941 + 1)) {
			fRec1344[l941] = 0.0f;
			
		}
		for (int l942 = 0; (l942 < 2); l942 = (l942 + 1)) {
			fRec1370[l942] = 0.0f;
			
		}
		for (int l943 = 0; (l943 < 2); l943 = (l943 + 1)) {
			fRec1368[l943] = 0.0f;
			
		}
		for (int l944 = 0; (l944 < 2); l944 = (l944 + 1)) {
			fRec1367[l944] = 0.0f;
			
		}
		for (int l945 = 0; (l945 < 2); l945 = (l945 + 1)) {
			fRec1365[l945] = 0.0f;
			
		}
		for (int l946 = 0; (l946 < 2); l946 = (l946 + 1)) {
			fRec1364[l946] = 0.0f;
			
		}
		for (int l947 = 0; (l947 < 2); l947 = (l947 + 1)) {
			fRec1362[l947] = 0.0f;
			
		}
		for (int l948 = 0; (l948 < 2); l948 = (l948 + 1)) {
			fRec1361[l948] = 0.0f;
			
		}
		for (int l949 = 0; (l949 < 2); l949 = (l949 + 1)) {
			fRec1359[l949] = 0.0f;
			
		}
		for (int l950 = 0; (l950 < 2); l950 = (l950 + 1)) {
			fRec1379[l950] = 0.0f;
			
		}
		for (int l951 = 0; (l951 < 2); l951 = (l951 + 1)) {
			fRec1377[l951] = 0.0f;
			
		}
		for (int l952 = 0; (l952 < 2); l952 = (l952 + 1)) {
			fRec1376[l952] = 0.0f;
			
		}
		for (int l953 = 0; (l953 < 2); l953 = (l953 + 1)) {
			fRec1374[l953] = 0.0f;
			
		}
		for (int l954 = 0; (l954 < 2); l954 = (l954 + 1)) {
			fRec1373[l954] = 0.0f;
			
		}
		for (int l955 = 0; (l955 < 2); l955 = (l955 + 1)) {
			fRec1371[l955] = 0.0f;
			
		}
		for (int l956 = 0; (l956 < 2); l956 = (l956 + 1)) {
			fRec1382[l956] = 0.0f;
			
		}
		for (int l957 = 0; (l957 < 2); l957 = (l957 + 1)) {
			fRec1380[l957] = 0.0f;
			
		}
		for (int l958 = 0; (l958 < 2); l958 = (l958 + 1)) {
			fRec1388[l958] = 0.0f;
			
		}
		for (int l959 = 0; (l959 < 2); l959 = (l959 + 1)) {
			fRec1386[l959] = 0.0f;
			
		}
		for (int l960 = 0; (l960 < 2); l960 = (l960 + 1)) {
			fRec1385[l960] = 0.0f;
			
		}
		for (int l961 = 0; (l961 < 2); l961 = (l961 + 1)) {
			fRec1383[l961] = 0.0f;
			
		}
		for (int l962 = 0; (l962 < 2); l962 = (l962 + 1)) {
			fRec1391[l962] = 0.0f;
			
		}
		for (int l963 = 0; (l963 < 2); l963 = (l963 + 1)) {
			fRec1389[l963] = 0.0f;
			
		}
		for (int l964 = 0; (l964 < 2); l964 = (l964 + 1)) {
			fRec1397[l964] = 0.0f;
			
		}
		for (int l965 = 0; (l965 < 2); l965 = (l965 + 1)) {
			fRec1395[l965] = 0.0f;
			
		}
		for (int l966 = 0; (l966 < 2); l966 = (l966 + 1)) {
			fRec1394[l966] = 0.0f;
			
		}
		for (int l967 = 0; (l967 < 2); l967 = (l967 + 1)) {
			fRec1392[l967] = 0.0f;
			
		}
		for (int l968 = 0; (l968 < 2); l968 = (l968 + 1)) {
			fRec1406[l968] = 0.0f;
			
		}
		for (int l969 = 0; (l969 < 2); l969 = (l969 + 1)) {
			fRec1404[l969] = 0.0f;
			
		}
		for (int l970 = 0; (l970 < 2); l970 = (l970 + 1)) {
			fRec1403[l970] = 0.0f;
			
		}
		for (int l971 = 0; (l971 < 2); l971 = (l971 + 1)) {
			fRec1401[l971] = 0.0f;
			
		}
		for (int l972 = 0; (l972 < 2); l972 = (l972 + 1)) {
			fRec1400[l972] = 0.0f;
			
		}
		for (int l973 = 0; (l973 < 2); l973 = (l973 + 1)) {
			fRec1398[l973] = 0.0f;
			
		}
		for (int l974 = 0; (l974 < 2); l974 = (l974 + 1)) {
			fRec1418[l974] = 0.0f;
			
		}
		for (int l975 = 0; (l975 < 2); l975 = (l975 + 1)) {
			fRec1416[l975] = 0.0f;
			
		}
		for (int l976 = 0; (l976 < 2); l976 = (l976 + 1)) {
			fRec1415[l976] = 0.0f;
			
		}
		for (int l977 = 0; (l977 < 2); l977 = (l977 + 1)) {
			fRec1413[l977] = 0.0f;
			
		}
		for (int l978 = 0; (l978 < 2); l978 = (l978 + 1)) {
			fRec1412[l978] = 0.0f;
			
		}
		for (int l979 = 0; (l979 < 2); l979 = (l979 + 1)) {
			fRec1410[l979] = 0.0f;
			
		}
		for (int l980 = 0; (l980 < 2); l980 = (l980 + 1)) {
			fRec1409[l980] = 0.0f;
			
		}
		for (int l981 = 0; (l981 < 2); l981 = (l981 + 1)) {
			fRec1407[l981] = 0.0f;
			
		}
		for (int l982 = 0; (l982 < 2); l982 = (l982 + 1)) {
			fRec1433[l982] = 0.0f;
			
		}
		for (int l983 = 0; (l983 < 2); l983 = (l983 + 1)) {
			fRec1431[l983] = 0.0f;
			
		}
		for (int l984 = 0; (l984 < 2); l984 = (l984 + 1)) {
			fRec1430[l984] = 0.0f;
			
		}
		for (int l985 = 0; (l985 < 2); l985 = (l985 + 1)) {
			fRec1428[l985] = 0.0f;
			
		}
		for (int l986 = 0; (l986 < 2); l986 = (l986 + 1)) {
			fRec1427[l986] = 0.0f;
			
		}
		for (int l987 = 0; (l987 < 2); l987 = (l987 + 1)) {
			fRec1425[l987] = 0.0f;
			
		}
		for (int l988 = 0; (l988 < 2); l988 = (l988 + 1)) {
			fRec1424[l988] = 0.0f;
			
		}
		for (int l989 = 0; (l989 < 2); l989 = (l989 + 1)) {
			fRec1422[l989] = 0.0f;
			
		}
		for (int l990 = 0; (l990 < 2); l990 = (l990 + 1)) {
			fRec1421[l990] = 0.0f;
			
		}
		for (int l991 = 0; (l991 < 2); l991 = (l991 + 1)) {
			fRec1419[l991] = 0.0f;
			
		}
		
	}
	
	virtual void init(int samplingFreq) {
		classInit(samplingFreq);
		instanceInit(samplingFreq);
	}
	virtual void instanceInit(int samplingFreq) {
		instanceConstants(samplingFreq);
		instanceResetUserInterface();
		instanceClear();
	}
	
	virtual mydsp* clone() {
		return new mydsp();
	}
	virtual int getSampleRate() {
		return fSamplingFreq;
		
	}
	
	virtual void buildUserInterface(UI* ui_interface) {
		ui_interface->openVerticalBox("SATOswDo5");
		ui_interface->declare(&fHslider0, "unit", "dB");
		ui_interface->addHorizontalSlider("gain", &fHslider0, -10.0f, -30.0f, 10.0f, 1.0f);
		ui_interface->declare(&fHslider2, "unit", "dB");
		ui_interface->addHorizontalSlider("lf/hf", &fHslider2, 0.0f, -3.0f, 3.0f, 0.100000001f);
		ui_interface->addCheckButton("mute", &fCheckbox0);
		ui_interface->declare(&fHslider1, "unit", "Hz");
		ui_interface->addHorizontalSlider("xover", &fHslider1, 400.0f, 200.0f, 800.0f, 20.0f);
		ui_interface->closeBox();
		
	}
	
	virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) {
		FAUSTFLOAT* input0 = inputs[0];
		FAUSTFLOAT* input1 = inputs[1];
		FAUSTFLOAT* input2 = inputs[2];
		FAUSTFLOAT* input3 = inputs[3];
		FAUSTFLOAT* input4 = inputs[4];
		FAUSTFLOAT* input5 = inputs[5];
		FAUSTFLOAT* input6 = inputs[6];
		FAUSTFLOAT* input7 = inputs[7];
		FAUSTFLOAT* input8 = inputs[8];
		FAUSTFLOAT* input9 = inputs[9];
		FAUSTFLOAT* input10 = inputs[10];
		FAUSTFLOAT* input11 = inputs[11];
		FAUSTFLOAT* input12 = inputs[12];
		FAUSTFLOAT* input13 = inputs[13];
		FAUSTFLOAT* input14 = inputs[14];
		FAUSTFLOAT* input15 = inputs[15];
		FAUSTFLOAT* input16 = inputs[16];
		FAUSTFLOAT* input17 = inputs[17];
		FAUSTFLOAT* input18 = inputs[18];
		FAUSTFLOAT* input19 = inputs[19];
		FAUSTFLOAT* input20 = inputs[20];
		FAUSTFLOAT* input21 = inputs[21];
		FAUSTFLOAT* input22 = inputs[22];
		FAUSTFLOAT* input23 = inputs[23];
		FAUSTFLOAT* input24 = inputs[24];
		FAUSTFLOAT* input25 = inputs[25];
		FAUSTFLOAT* input26 = inputs[26];
		FAUSTFLOAT* input27 = inputs[27];
		FAUSTFLOAT* input28 = inputs[28];
		FAUSTFLOAT* input29 = inputs[29];
		FAUSTFLOAT* input30 = inputs[30];
		FAUSTFLOAT* input31 = inputs[31];
		FAUSTFLOAT* input32 = inputs[32];
		FAUSTFLOAT* input33 = inputs[33];
		FAUSTFLOAT* input34 = inputs[34];
		FAUSTFLOAT* input35 = inputs[35];
		FAUSTFLOAT* output0 = outputs[0];
		FAUSTFLOAT* output1 = outputs[1];
		FAUSTFLOAT* output2 = outputs[2];
		FAUSTFLOAT* output3 = outputs[3];
		FAUSTFLOAT* output4 = outputs[4];
		FAUSTFLOAT* output5 = outputs[5];
		FAUSTFLOAT* output6 = outputs[6];
		FAUSTFLOAT* output7 = outputs[7];
		FAUSTFLOAT* output8 = outputs[8];
		FAUSTFLOAT* output9 = outputs[9];
		FAUSTFLOAT* output10 = outputs[10];
		FAUSTFLOAT* output11 = outputs[11];
		FAUSTFLOAT* output12 = outputs[12];
		FAUSTFLOAT* output13 = outputs[13];
		FAUSTFLOAT* output14 = outputs[14];
		FAUSTFLOAT* output15 = outputs[15];
		FAUSTFLOAT* output16 = outputs[16];
		FAUSTFLOAT* output17 = outputs[17];
		FAUSTFLOAT* output18 = outputs[18];
		FAUSTFLOAT* output19 = outputs[19];
		FAUSTFLOAT* output20 = outputs[20];
		FAUSTFLOAT* output21 = outputs[21];
		FAUSTFLOAT* output22 = outputs[22];
		FAUSTFLOAT* output23 = outputs[23];
		FAUSTFLOAT* output24 = outputs[24];
		FAUSTFLOAT* output25 = outputs[25];
		FAUSTFLOAT* output26 = outputs[26];
		FAUSTFLOAT* output27 = outputs[27];
		FAUSTFLOAT* output28 = outputs[28];
		FAUSTFLOAT* output29 = outputs[29];
		FAUSTFLOAT* output30 = outputs[30];
		float fSlow0 = (0.00100000005f * (float((float(fCheckbox0) < 0.5f)) * powf(10.0f, (0.0500000007f * float(fHslider0)))));
		float fSlow1 = (0.00100000005f * float(fHslider1));
		float fSlow2 = (0.00100000005f * powf(10.0f, (0.0500000007f * float(fHslider2))));
		for (int i = 0; (i < count); i = (i + 1)) {
			fRec0[0] = (fSlow0 + (0.999000013f * fRec0[1]));
			float fTemp0 = (fConst3 * fRec1[1]);
			float fTemp1 = (fConst6 * fRec4[1]);
			float fTemp2 = (fConst7 * fRec7[1]);
			fRec17[0] = (fSlow1 + (0.999000013f * fRec17[1]));
			float fTemp3 = tanf((fConst10 * fRec17[0]));
			float fTemp4 = ((fTemp3 * (fTemp3 + -2.0f)) + 1.0f);
			float fTemp5 = mydsp_faustpower2_f(fTemp3);
			float fTemp6 = (fTemp5 + -1.0f);
			float fTemp7 = ((fTemp3 * (fTemp3 + 2.0f)) + 1.0f);
			fRec16[0] = (float(input25[i]) - (((fRec16[2] * fTemp4) + (2.0f * (fRec16[1] * fTemp6))) / fTemp7));
			fRec18[0] = (fSlow2 + (0.999000013f * fRec18[1]));
			float fTemp8 = (fRec18[0] * fTemp7);
			float fTemp9 = (0.0f - (2.0f / fTemp7));
			float fTemp10 = ((((fRec16[2] + (fRec16[0] + (2.0f * fRec16[1]))) * fTemp5) / fTemp8) + (0.205712318f * (fRec18[0] * (0.0f - ((fRec16[1] * fTemp9) + ((fRec16[0] + fRec16[2]) / fTemp7))))));
			fRec19[0] = (float(input27[i]) - (((fRec19[2] * fTemp4) + (2.0f * (fRec19[1] * fTemp6))) / fTemp7));
			float fTemp11 = ((((fRec19[2] + (fRec19[0] + (2.0f * fRec19[1]))) * fTemp5) / fTemp8) + (0.205712318f * (fRec18[0] * (0.0f - ((fRec19[1] * fTemp9) + ((fRec19[0] + fRec19[2]) / fTemp7))))));
			fRec20[0] = (float(input28[i]) - (((fRec20[2] * fTemp4) + (2.0f * (fRec20[1] * fTemp6))) / fTemp7));
			float fTemp12 = ((((fRec20[2] + (fRec20[0] + (2.0f * fRec20[1]))) * fTemp5) / fTemp8) + (0.205712318f * (fRec18[0] * (0.0f - ((fRec20[1] * fTemp9) + ((fRec20[0] + fRec20[2]) / fTemp7))))));
			fRec21[0] = (float(input29[i]) - (((fRec21[2] * fTemp4) + (2.0f * (fRec21[1] * fTemp6))) / fTemp7));
			float fTemp13 = ((((fRec21[2] + (fRec21[0] + (2.0f * fRec21[1]))) * fTemp5) / fTemp8) + (0.205712318f * (fRec18[0] * (0.0f - ((fRec21[1] * fTemp9) + ((fRec21[0] + fRec21[2]) / fTemp7))))));
			fRec22[0] = (float(input30[i]) - (((fRec22[2] * fTemp4) + (2.0f * (fRec22[1] * fTemp6))) / fTemp7));
			float fTemp14 = ((((fRec22[2] + (fRec22[0] + (2.0f * fRec22[1]))) * fTemp5) / fTemp8) + (0.205712318f * (fRec18[0] * (0.0f - ((fRec22[1] * fTemp9) + ((fRec22[0] + fRec22[2]) / fTemp7))))));
			fRec23[0] = (float(input31[i]) - (((fRec23[2] * fTemp4) + (2.0f * (fRec23[1] * fTemp6))) / fTemp7));
			float fTemp15 = ((((fRec23[2] + (fRec23[0] + (2.0f * fRec23[1]))) * fTemp5) / fTemp8) + (0.205712318f * (fRec18[0] * (0.0f - ((fRec23[1] * fTemp9) + ((fRec23[0] + fRec23[2]) / fTemp7))))));
			fRec24[0] = (float(input32[i]) - (((fRec24[2] * fTemp4) + (2.0f * (fRec24[1] * fTemp6))) / fTemp7));
			float fTemp16 = ((((fRec24[2] + (fRec24[0] + (2.0f * fRec24[1]))) * fTemp5) / fTemp8) + (0.205712318f * (fRec18[0] * (0.0f - ((fRec24[1] * fTemp9) + ((fRec24[0] + fRec24[2]) / fTemp7))))));
			fRec25[0] = (float(input35[i]) - (((fRec25[2] * fTemp4) + (2.0f * (fRec25[1] * fTemp6))) / fTemp7));
			float fTemp17 = ((((fRec25[2] + (fRec25[0] + (2.0f * fRec25[1]))) * fTemp5) / fTemp8) + (0.205712318f * (fRec18[0] * (0.0f - ((fRec25[1] * fTemp9) + ((fRec25[0] + fRec25[2]) / fTemp7))))));
			fRec26[0] = (float(input26[i]) - (((fRec26[2] * fTemp4) + (2.0f * (fRec26[1] * fTemp6))) / fTemp7));
			float fTemp18 = ((((fRec26[2] + (fRec26[0] + (2.0f * fRec26[1]))) * fTemp5) / fTemp8) + (0.205712318f * (fRec18[0] * (0.0f - ((fRec26[1] * fTemp9) + ((fRec26[0] + fRec26[2]) / fTemp7))))));
			fRec27[0] = (float(input33[i]) - (((fRec27[2] * fTemp4) + (2.0f * (fRec27[1] * fTemp6))) / fTemp7));
			float fTemp19 = ((((fRec27[2] + (fRec27[0] + (2.0f * fRec27[1]))) * fTemp5) / fTemp8) + (0.205712318f * (fRec18[0] * (0.0f - ((fRec27[1] * fTemp9) + ((fRec27[0] + fRec27[2]) / fTemp7))))));
			fRec28[0] = (float(input34[i]) - (((fRec28[2] * fTemp4) + (2.0f * (fRec28[1] * fTemp6))) / fTemp7));
			float fTemp20 = ((((fRec28[2] + (fRec28[0] + (2.0f * fRec28[1]))) * fTemp5) / fTemp8) + (0.205712318f * (fRec18[0] * (0.0f - ((fRec28[1] * fTemp9) + ((fRec28[0] + fRec28[2]) / fTemp7))))));
			float fTemp21 = (fConst9 * (((((((((2.02489991e-06f * fTemp10) + (2.118e-07f * fTemp11)) + (3.36058001e-05f * fTemp12)) + (3.48120011e-06f * fTemp13)) + (0.0339396112f * fTemp14)) + (1.24500005e-07f * fTemp15)) + (3.80839992e-06f * fTemp16)) + (1.02590002e-06f * fTemp17)) - (((2.08450001e-06f * fTemp18) + (2.97540009e-06f * fTemp19)) + (3.3284e-06f * fTemp20))));
			float fTemp22 = (fConst11 * fRec10[1]);
			float fTemp23 = (fConst12 * fRec13[1]);
			fRec15[0] = (fTemp21 + (fTemp22 + (fRec15[1] + fTemp23)));
			fRec13[0] = fRec15[0];
			float fRec14 = ((fTemp23 + fTemp22) + fTemp21);
			fRec12[0] = (fRec13[0] + fRec12[1]);
			fRec10[0] = fRec12[0];
			float fRec11 = fRec14;
			fRec9[0] = (fTemp1 + (fTemp2 + (fRec11 + fRec9[1])));
			fRec7[0] = fRec9[0];
			float fRec8 = (fTemp1 + (fRec11 + fTemp2));
			fRec6[0] = (fRec7[0] + fRec6[1]);
			fRec4[0] = fRec6[0];
			float fRec5 = fRec8;
			fRec3[0] = (fTemp0 + (fRec5 + fRec3[1]));
			fRec1[0] = fRec3[0];
			float fRec2 = (fRec5 + fTemp0);
			float fTemp24 = (fConst14 * fRec29[1]);
			float fTemp25 = (fConst15 * fRec32[1]);
			fRec41[0] = (float(input18[i]) - (((fRec41[2] * fTemp4) + (2.0f * (fRec41[1] * fTemp6))) / fTemp7));
			float fTemp26 = ((((fRec41[2] + (fRec41[0] + (2.0f * fRec41[1]))) * fTemp5) / fTemp8) + (0.422004998f * (fRec18[0] * (0.0f - ((fRec41[1] * fTemp9) + ((fRec41[0] + fRec41[2]) / fTemp7))))));
			fRec42[0] = (float(input20[i]) - (((fRec42[2] * fTemp4) + (2.0f * (fRec42[1] * fTemp6))) / fTemp7));
			float fTemp27 = ((((fRec42[2] + (fRec42[0] + (2.0f * fRec42[1]))) * fTemp5) / fTemp8) + (0.422004998f * (fRec18[0] * (0.0f - ((fRec42[1] * fTemp9) + ((fRec42[0] + fRec42[2]) / fTemp7))))));
			fRec43[0] = (float(input21[i]) - (((fRec43[2] * fTemp4) + (2.0f * (fRec43[1] * fTemp6))) / fTemp7));
			float fTemp28 = ((((fRec43[2] + (fRec43[0] + (2.0f * fRec43[1]))) * fTemp5) / fTemp8) + (0.422004998f * (fRec18[0] * (0.0f - ((fRec43[1] * fTemp9) + ((fRec43[0] + fRec43[2]) / fTemp7))))));
			fRec44[0] = (float(input22[i]) - (((fRec44[2] * fTemp4) + (2.0f * (fRec44[1] * fTemp6))) / fTemp7));
			float fTemp29 = ((((fRec44[2] + (fRec44[0] + (2.0f * fRec44[1]))) * fTemp5) / fTemp8) + (0.422004998f * (fRec18[0] * (0.0f - ((fRec44[1] * fTemp9) + ((fRec44[0] + fRec44[2]) / fTemp7))))));
			fRec45[0] = (float(input16[i]) - (((fRec45[2] * fTemp4) + (2.0f * (fRec45[1] * fTemp6))) / fTemp7));
			float fTemp30 = ((((fRec45[2] + (fRec45[0] + (2.0f * fRec45[1]))) * fTemp5) / fTemp8) + (0.422004998f * (fRec18[0] * (0.0f - ((fRec45[1] * fTemp9) + ((fRec45[0] + fRec45[2]) / fTemp7))))));
			fRec46[0] = (float(input17[i]) - (((fRec46[2] * fTemp4) + (2.0f * (fRec46[1] * fTemp6))) / fTemp7));
			float fTemp31 = ((((fRec46[2] + (fRec46[0] + (2.0f * fRec46[1]))) * fTemp5) / fTemp8) + (0.422004998f * (fRec18[0] * (0.0f - ((fRec46[1] * fTemp9) + ((fRec46[0] + fRec46[2]) / fTemp7))))));
			fRec47[0] = (float(input19[i]) - (((fRec47[2] * fTemp4) + (2.0f * (fRec47[1] * fTemp6))) / fTemp7));
			float fTemp32 = ((((fRec47[2] + (fRec47[0] + (2.0f * fRec47[1]))) * fTemp5) / fTemp8) + (0.422004998f * (fRec18[0] * (0.0f - ((fRec47[1] * fTemp9) + ((fRec47[0] + fRec47[2]) / fTemp7))))));
			fRec48[0] = (float(input23[i]) - (((fRec48[2] * fTemp4) + (2.0f * (fRec48[1] * fTemp6))) / fTemp7));
			float fTemp33 = ((((fRec48[2] + (fRec48[0] + (2.0f * fRec48[1]))) * fTemp5) / fTemp8) + (0.422004998f * (fRec18[0] * (0.0f - ((fRec48[1] * fTemp9) + ((fRec48[0] + fRec48[2]) / fTemp7))))));
			fRec49[0] = (float(input24[i]) - (((fRec49[2] * fTemp4) + (2.0f * (fRec49[1] * fTemp6))) / fTemp7));
			float fTemp34 = ((((fRec49[2] + (fRec49[0] + (2.0f * fRec49[1]))) * fTemp5) / fTemp8) + (0.422004998f * (fRec18[0] * (0.0f - ((fRec49[1] * fTemp9) + ((fRec49[0] + fRec49[2]) / fTemp7))))));
			float fTemp35 = (fConst17 * (((((3.41539999e-05f * fTemp26) + (0.0695557967f * fTemp27)) + (2.40729992e-06f * fTemp28)) + (6.38829988e-06f * fTemp29)) - (((((8.202e-07f * fTemp30) + (1.64300005e-07f * fTemp31)) + (5.32120021e-06f * fTemp32)) + (2.43790009e-06f * fTemp33)) + (1.43909995e-06f * fTemp34))));
			float fTemp36 = (fConst18 * fRec35[1]);
			float fTemp37 = (fConst19 * fRec38[1]);
			fRec40[0] = (fTemp35 + (fTemp36 + (fRec40[1] + fTemp37)));
			fRec38[0] = fRec40[0];
			float fRec39 = ((fTemp37 + fTemp36) + fTemp35);
			fRec37[0] = (fRec38[0] + fRec37[1]);
			fRec35[0] = fRec37[0];
			float fRec36 = fRec39;
			fRec34[0] = (fTemp24 + (fTemp25 + (fRec36 + fRec34[1])));
			fRec32[0] = fRec34[0];
			float fRec33 = (fTemp24 + (fRec36 + fTemp25));
			fRec31[0] = (fRec32[0] + fRec31[1]);
			fRec29[0] = fRec31[0];
			float fRec30 = fRec33;
			fRec59[0] = (float(input10[i]) - (((2.0f * (fRec59[1] * fTemp6)) + (fRec59[2] * fTemp4)) / fTemp7));
			float fTemp38 = (((((fRec59[0] + (2.0f * fRec59[1])) + fRec59[2]) * fTemp5) / fTemp8) + (0.628249943f * (fRec18[0] * (0.0f - ((fRec59[1] * fTemp9) + ((fRec59[0] + fRec59[2]) / fTemp7))))));
			fRec60[0] = (float(input12[i]) - (((fRec60[2] * fTemp4) + (2.0f * (fRec60[1] * fTemp6))) / fTemp7));
			float fTemp39 = ((((fRec60[2] + (fRec60[0] + (2.0f * fRec60[1]))) * fTemp5) / fTemp8) + (0.628249943f * (fRec18[0] * (0.0f - ((fRec60[1] * fTemp9) + ((fRec60[0] + fRec60[2]) / fTemp7))))));
			fRec61[0] = (float(input13[i]) - (((fRec61[2] * fTemp4) + (2.0f * (fRec61[1] * fTemp6))) / fTemp7));
			float fTemp40 = ((((fRec61[2] + (fRec61[0] + (2.0f * fRec61[1]))) * fTemp5) / fTemp8) + (0.628249943f * (fRec18[0] * (0.0f - ((fRec61[1] * fTemp9) + ((fRec61[0] + fRec61[2]) / fTemp7))))));
			fRec62[0] = (float(input14[i]) - (((fRec62[2] * fTemp4) + (2.0f * (fRec62[1] * fTemp6))) / fTemp7));
			float fTemp41 = ((((fRec62[2] + (fRec62[0] + (2.0f * fRec62[1]))) * fTemp5) / fTemp8) + (0.628249943f * (fRec18[0] * (0.0f - ((fRec62[1] * fTemp9) + ((fRec62[0] + fRec62[2]) / fTemp7))))));
			fRec63[0] = (float(input11[i]) - (((fRec63[2] * fTemp4) + (2.0f * (fRec63[1] * fTemp6))) / fTemp7));
			float fTemp42 = ((((fRec63[2] + (fRec63[0] + (2.0f * fRec63[1]))) * fTemp5) / fTemp8) + (0.628249943f * (fRec18[0] * (0.0f - ((fRec63[1] * fTemp9) + ((fRec63[0] + fRec63[2]) / fTemp7))))));
			fRec64[0] = (float(input9[i]) - (((fRec64[2] * fTemp4) + (2.0f * (fRec64[1] * fTemp6))) / fTemp7));
			float fTemp43 = ((((fRec64[2] + (fRec64[0] + (2.0f * fRec64[1]))) * fTemp5) / fTemp8) + (0.628249943f * (fRec18[0] * (0.0f - ((fRec64[1] * fTemp9) + ((fRec64[0] + fRec64[2]) / fTemp7))))));
			fRec65[0] = (float(input15[i]) - (((fRec65[2] * fTemp4) + (2.0f * (fRec65[1] * fTemp6))) / fTemp7));
			float fTemp44 = ((((fRec65[2] + (fRec65[0] + (2.0f * fRec65[1]))) * fTemp5) / fTemp8) + (0.628249943f * (fRec18[0] * (0.0f - ((fRec65[1] * fTemp9) + ((fRec65[0] + fRec65[2]) / fTemp7))))));
			float fTemp45 = (fConst22 * (((((2.55382001e-05f * fTemp38) + (0.102538198f * fTemp39)) + (3.89539991e-06f * fTemp40)) + (5.84349982e-06f * fTemp41)) - (((1.23203999e-05f * fTemp42) + (1.65499998e-07f * fTemp43)) + (1.21899996e-06f * fTemp44))));
			float fTemp46 = (fConst23 * fRec53[1]);
			float fTemp47 = (fConst24 * fRec56[1]);
			fRec58[0] = (fTemp45 + (fTemp46 + (fRec58[1] + fTemp47)));
			fRec56[0] = fRec58[0];
			float fRec57 = ((fTemp47 + fTemp46) + fTemp45);
			fRec55[0] = (fRec56[0] + fRec55[1]);
			fRec53[0] = fRec55[0];
			float fRec54 = fRec57;
			float fTemp48 = (fConst25 * fRec50[1]);
			fRec52[0] = ((fRec54 + fRec52[1]) + fTemp48);
			fRec50[0] = fRec52[0];
			float fRec51 = (fRec54 + fTemp48);
			fRec69[0] = (float(input2[i]) - (((fTemp4 * fRec69[2]) + (2.0f * (fTemp6 * fRec69[1]))) / fTemp7));
			float fTemp49 = (((fTemp5 * (fRec69[2] + (fRec69[0] + (2.0f * fRec69[1])))) / fTemp8) + (0.932469487f * (fRec18[0] * (0.0f - ((fTemp9 * fRec69[1]) + ((fRec69[0] + fRec69[2]) / fTemp7))))));
			fRec70[0] = (float(input3[i]) - (((fTemp4 * fRec70[2]) + (2.0f * (fTemp6 * fRec70[1]))) / fTemp7));
			float fTemp50 = (((fTemp5 * (fRec70[2] + (fRec70[0] + (2.0f * fRec70[1])))) / fTemp8) + (0.932469487f * (fRec18[0] * (0.0f - ((fTemp9 * fRec70[1]) + ((fRec70[0] + fRec70[2]) / fTemp7))))));
			fRec71[0] = (float(input1[i]) - (((fTemp4 * fRec71[2]) + (2.0f * (fTemp6 * fRec71[1]))) / fTemp7));
			float fTemp51 = (((fTemp5 * (fRec71[2] + (fRec71[0] + (2.0f * fRec71[1])))) / fTemp8) + (0.932469487f * (fRec18[0] * (0.0f - ((fTemp9 * fRec71[1]) + ((fRec71[0] + fRec71[2]) / fTemp7))))));
			float fTemp52 = (fConst27 * (((0.116276413f * fTemp49) + (2.27310011e-06f * fTemp50)) - (8.45809973e-06f * fTemp51)));
			float fTemp53 = (fConst28 * fRec66[1]);
			fRec68[0] = (fTemp52 + (fRec68[1] + fTemp53));
			fRec66[0] = fRec68[0];
			float fRec67 = (fTemp53 + fTemp52);
			fRec78[0] = (float(input4[i]) - (((fTemp4 * fRec78[2]) + (2.0f * (fTemp6 * fRec78[1]))) / fTemp7));
			float fTemp54 = (((fTemp5 * (fRec78[2] + (fRec78[0] + (2.0f * fRec78[1])))) / fTemp8) + (0.804249108f * (fRec18[0] * (0.0f - ((fTemp9 * fRec78[1]) + ((fRec78[0] + fRec78[2]) / fTemp7))))));
			fRec79[0] = (float(input6[i]) - (((fTemp4 * fRec79[2]) + (2.0f * (fTemp6 * fRec79[1]))) / fTemp7));
			float fTemp55 = (((fTemp5 * (fRec79[2] + (fRec79[0] + (2.0f * fRec79[1])))) / fTemp8) + (0.804249108f * (fRec18[0] * (0.0f - ((fTemp9 * fRec79[1]) + ((fRec79[0] + fRec79[2]) / fTemp7))))));
			fRec80[0] = (float(input7[i]) - (((fTemp4 * fRec80[2]) + (2.0f * (fTemp6 * fRec80[1]))) / fTemp7));
			float fTemp56 = (((fTemp5 * (fRec80[2] + (fRec80[0] + (2.0f * fRec80[1])))) / fTemp8) + (0.804249108f * (fRec18[0] * (0.0f - ((fTemp9 * fRec80[1]) + ((fRec80[0] + fRec80[2]) / fTemp7))))));
			fRec81[0] = (float(input8[i]) - (((fTemp4 * fRec81[2]) + (2.0f * (fTemp6 * fRec81[1]))) / fTemp7));
			float fTemp57 = (((fTemp5 * (fRec81[2] + (fRec81[0] + (2.0f * fRec81[1])))) / fTemp8) + (0.804249108f * (fRec18[0] * (0.0f - ((fTemp9 * fRec81[1]) + ((fRec81[0] + fRec81[2]) / fTemp7))))));
			fRec82[0] = (float(input5[i]) - (((fTemp4 * fRec82[2]) + (2.0f * (fTemp6 * fRec82[1]))) / fTemp7));
			float fTemp58 = (((fTemp5 * (fRec82[2] + (fRec82[0] + (2.0f * fRec82[1])))) / fTemp8) + (0.804249108f * (fRec18[0] * (0.0f - ((fTemp9 * fRec82[1]) + ((fRec82[0] + fRec82[2]) / fTemp7))))));
			float fTemp59 = (fConst30 * (((((1.26164996e-05f * fTemp54) + (0.121653795f * fTemp55)) + (3.81099994e-06f * fTemp56)) + (3.20460003e-06f * fTemp57)) - (1.34709999e-05f * fTemp58)));
			float fTemp60 = (fConst31 * fRec72[1]);
			float fTemp61 = (fConst32 * fRec75[1]);
			fRec77[0] = (fTemp59 + (fTemp60 + (fRec77[1] + fTemp61)));
			fRec75[0] = fRec77[0];
			float fRec76 = ((fTemp61 + fTemp60) + fTemp59);
			fRec74[0] = (fRec75[0] + fRec74[1]);
			fRec72[0] = fRec74[0];
			float fRec73 = fRec76;
			fRec83[0] = (float(input0[i]) - (((fRec83[2] * fTemp4) + (2.0f * (fRec83[1] * fTemp6))) / fTemp7));
			float fTemp62 = (((fTemp5 * (fRec83[2] + (fRec83[0] + (2.0f * fRec83[1])))) / fTemp8) + (fRec18[0] * (0.0f - ((fRec83[1] * fTemp9) + ((fRec83[0] + fRec83[2]) / fTemp7)))));
			fVec0[(IOTA & 2047)] = ((fRec2 + (fRec30 + (fRec51 + (fRec67 + fRec73)))) + (0.074275896f * fTemp62));
			output0[i] = FAUSTFLOAT((0.752947509f * (fRec0[0] * fVec0[((IOTA - iConst33) & 2047)])));
			float fTemp63 = (fConst35 * fRec84[1]);
			float fTemp64 = (fConst37 * fRec87[1]);
			float fTemp65 = (fConst38 * fRec90[1]);
			float fTemp66 = (fConst40 * (((0.00223134551f * fTemp17) + ((0.0121775558f * fTemp12) + (0.000518797373f * fTemp20))) - ((0.026137758f * fTemp19) + ((0.0263615996f * fTemp16) + ((0.00645789597f * fTemp15) + (((((0.00140696089f * fTemp10) + (0.0142251756f * fTemp18)) + (0.0179660842f * fTemp11)) + (0.00993176922f * fTemp13)) + (0.0223619975f * fTemp14)))))));
			float fTemp67 = (fConst41 * fRec93[1]);
			float fTemp68 = (fConst42 * fRec96[1]);
			fRec98[0] = (fTemp66 + (fTemp67 + (fRec98[1] + fTemp68)));
			fRec96[0] = fRec98[0];
			float fRec97 = ((fTemp68 + fTemp67) + fTemp66);
			fRec95[0] = (fRec96[0] + fRec95[1]);
			fRec93[0] = fRec95[0];
			float fRec94 = fRec97;
			fRec92[0] = (fTemp64 + (fTemp65 + (fRec94 + fRec92[1])));
			fRec90[0] = fRec92[0];
			float fRec91 = (fTemp64 + (fRec94 + fTemp65));
			fRec89[0] = (fRec90[0] + fRec89[1]);
			fRec87[0] = fRec89[0];
			float fRec88 = fRec91;
			fRec86[0] = (fTemp63 + (fRec88 + fRec86[1]));
			fRec84[0] = fRec86[0];
			float fRec85 = (fRec88 + fTemp63);
			float fTemp69 = (fConst45 * ((((0.0291331671f * fTemp26) + (0.0212215427f * fTemp32)) + (0.00277468446f * fTemp28)) - ((0.000136244402f * fTemp34) + ((0.0274711903f * fTemp33) + ((((0.00832507852f * fTemp30) + (0.0151941972f * fTemp31)) + (0.0291951057f * fTemp27)) + (0.0426470339f * fTemp29))))));
			float fTemp70 = (fConst46 * fRec108[1]);
			float fTemp71 = (fConst47 * fRec105[1]);
			fRec110[0] = (((fTemp69 + fRec110[1]) + fTemp70) + fTemp71);
			fRec108[0] = fRec110[0];
			float fRec109 = ((fTemp69 + fTemp70) + fTemp71);
			fRec107[0] = (fRec108[0] + fRec107[1]);
			fRec105[0] = fRec107[0];
			float fRec106 = fRec109;
			float fTemp72 = (fConst48 * fRec102[1]);
			float fTemp73 = (fConst49 * fRec99[1]);
			fRec104[0] = (((fRec106 + fRec104[1]) + fTemp72) + fTemp73);
			fRec102[0] = fRec104[0];
			float fRec103 = ((fRec106 + fTemp72) + fTemp73);
			fRec101[0] = (fRec102[0] + fRec101[1]);
			fRec99[0] = fRec101[0];
			float fRec100 = fRec103;
			float fTemp74 = (fConst51 * fRec111[1]);
			float fTemp75 = (fConst53 * ((((0.0338524766f * fTemp38) + (0.0587121285f * fTemp42)) + (0.0167919695f * fTemp40)) - ((0.0165827759f * fTemp44) + (((0.00809382275f * fTemp43) + (0.0106742699f * fTemp39)) + (0.0429489762f * fTemp41)))));
			float fTemp76 = (fConst54 * fRec114[1]);
			float fTemp77 = (fConst55 * fRec117[1]);
			fRec119[0] = (fTemp75 + (fTemp76 + (fRec119[1] + fTemp77)));
			fRec117[0] = fRec119[0];
			float fRec118 = ((fTemp77 + fTemp76) + fTemp75);
			fRec116[0] = (fRec117[0] + fRec116[1]);
			fRec114[0] = fRec116[0];
			float fRec115 = fRec118;
			fRec113[0] = (fTemp74 + (fRec115 + fRec113[1]));
			fRec111[0] = fRec113[0];
			float fRec112 = (fRec115 + fTemp74);
			float fTemp78 = (fConst57 * (((0.0502111912f * fTemp51) + (0.0583267435f * fTemp49)) + (0.0173864271f * fTemp50)));
			float fTemp79 = (fConst58 * fRec120[1]);
			fRec122[0] = (fTemp78 + (fRec122[1] + fTemp79));
			fRec120[0] = fRec122[0];
			float fRec121 = (fTemp79 + fTemp78);
			float fTemp80 = (fConst60 * (((((0.021890806f * fTemp54) + (0.0733522624f * fTemp58)) + (0.0276794825f * fTemp55)) + (0.0240465272f * fTemp56)) - (0.0258579031f * fTemp57)));
			float fTemp81 = (fConst61 * fRec123[1]);
			float fTemp82 = (fConst62 * fRec126[1]);
			fRec128[0] = (fTemp80 + (fTemp81 + (fRec128[1] + fTemp82)));
			fRec126[0] = fRec128[0];
			float fRec127 = ((fTemp82 + fTemp81) + fTemp80);
			fRec125[0] = (fRec126[0] + fRec125[1]);
			fRec123[0] = fRec125[0];
			float fRec124 = fRec127;
			fVec1[(IOTA & 1023)] = ((0.0487409718f * fTemp62) + (fRec85 + (fRec100 + (fRec112 + (fRec121 + fRec124)))));
			output1[i] = FAUSTFLOAT((0.810182214f * (fRec0[0] * fVec1[((IOTA - iConst63) & 1023)])));
			float fTemp83 = (fConst65 * fRec129[1]);
			float fTemp84 = (fConst67 * (((0.0122100972f * fTemp44) + (((0.0122129228f * fTemp43) + (0.0442641154f * fTemp42)) + (6.1572e-06f * fTemp41))) - (((0.0582878366f * fTemp38) + (0.016099954f * fTemp39)) + (0.044274468f * fTemp40))));
			float fTemp85 = (fConst68 * fRec132[1]);
			float fTemp86 = (fConst69 * fRec135[1]);
			fRec137[0] = (fTemp84 + (fTemp85 + (fRec137[1] + fTemp86)));
			fRec135[0] = fRec137[0];
			float fRec136 = ((fTemp86 + fTemp85) + fTemp84);
			fRec134[0] = (fRec135[0] + fRec134[1]);
			fRec132[0] = fRec134[0];
			float fRec133 = fRec136;
			fRec131[0] = (fTemp83 + (fRec133 + fRec131[1]));
			fRec129[0] = fRec131[0];
			float fRec130 = (fRec133 + fTemp83);
			float fTemp87 = (fConst71 * ((((0.0594701208f * fTemp58) + (0.0264332425f * fTemp55)) + (4.01599982e-06f * fTemp57)) - ((0.036625877f * fTemp54) + (0.0594800413f * fTemp56))));
			float fTemp88 = (fConst72 * fRec138[1]);
			float fTemp89 = (fConst73 * fRec141[1]);
			fRec143[0] = (fTemp87 + (fTemp88 + (fRec143[1] + fTemp89)));
			fRec141[0] = fRec143[0];
			float fRec142 = ((fTemp89 + fTemp88) + fTemp87);
			fRec140[0] = (fRec141[0] + fRec140[1]);
			fRec138[0] = fRec140[0];
			float fRec139 = fRec142;
			float fTemp90 = (fConst75 * (((0.0419778749f * fTemp51) + (0.0627192035f * fTemp49)) - (0.0419842787f * fTemp50)));
			float fTemp91 = (fConst76 * fRec144[1]);
			fRec146[0] = (fTemp90 + (fRec146[1] + fTemp91));
			fRec144[0] = fRec146[0];
			float fRec145 = (fTemp91 + fTemp90);
			float fTemp92 = (fConst78 * fRec147[1]);
			float fTemp93 = (fConst79 * fRec150[1]);
			float fTemp94 = (fConst81 * (((0.0210462008f * fTemp33) + (((0.0210493542f * fTemp31) + (0.0115808602f * fTemp32)) + (6.95949984e-06f * fTemp29))) - (((((7.51400023e-07f * fTemp30) + (0.0533764362f * fTemp26)) + (0.0332423709f * fTemp27)) + (0.0115898829f * fTemp28)) + (0.0044932845f * fTemp34))));
			float fTemp95 = (fConst82 * fRec153[1]);
			float fTemp96 = (fConst83 * fRec156[1]);
			fRec158[0] = (fTemp94 + (fTemp95 + (fRec158[1] + fTemp96)));
			fRec156[0] = fRec158[0];
			float fRec157 = ((fTemp96 + fTemp95) + fTemp94);
			fRec155[0] = (fRec156[0] + fRec155[1]);
			fRec153[0] = fRec155[0];
			float fRec154 = fRec157;
			fRec152[0] = (fTemp92 + (fTemp93 + (fRec154 + fRec152[1])));
			fRec150[0] = fRec152[0];
			float fRec151 = (fTemp92 + (fRec154 + fTemp93));
			fRec149[0] = (fRec150[0] + fRec149[1]);
			fRec147[0] = fRec149[0];
			float fRec148 = fRec151;
			float fTemp97 = (fConst85 * fRec159[1]);
			float fTemp98 = (fConst87 * fRec162[1]);
			float fTemp99 = (fConst88 * fRec165[1]);
			float fTemp100 = (fConst90 * (((0.0216708332f * fTemp19) + ((7.82900042e-06f * fTemp16) + (((0.00126394688f * fTemp10) + (0.02167283f * fTemp11)) + (0.0122393128f * fTemp15)))) - ((0.00126343733f * fTemp17) + (((((6.0550002e-07f * fTemp18) + (0.0272370838f * fTemp12)) + (0.0122451317f * fTemp13)) + (0.0213034488f * fTemp14)) + (0.00825717021f * fTemp20)))));
			float fTemp101 = (fConst91 * fRec168[1]);
			float fTemp102 = (fConst92 * fRec171[1]);
			fRec173[0] = (fTemp100 + (fTemp101 + (fRec173[1] + fTemp102)));
			fRec171[0] = fRec173[0];
			float fRec172 = ((fTemp102 + fTemp101) + fTemp100);
			fRec170[0] = (fRec171[0] + fRec170[1]);
			fRec168[0] = fRec170[0];
			float fRec169 = fRec172;
			fRec167[0] = (fTemp98 + (fTemp99 + (fRec169 + fRec167[1])));
			fRec165[0] = fRec167[0];
			float fRec166 = (fTemp98 + (fRec169 + fTemp99));
			fRec164[0] = (fRec165[0] + fRec164[1]);
			fRec162[0] = fRec164[0];
			float fRec163 = fRec166;
			fRec161[0] = (fTemp97 + (fRec163 + fRec161[1]));
			fRec159[0] = fRec161[0];
			float fRec160 = (fRec163 + fTemp97);
			fVec2[(IOTA & 1023)] = ((0.053797558f * fTemp62) + (fRec130 + (fRec139 + (fRec145 + (fRec148 + fRec160)))));
			output2[i] = FAUSTFLOAT((0.809967816f * (fRec0[0] * fVec2[((IOTA - iConst93) & 1023)])));
			float fTemp103 = (fConst35 * fRec174[1]);
			float fTemp104 = (fConst37 * fRec177[1]);
			float fTemp105 = (fConst38 * fRec180[1]);
			float fTemp106 = (fConst40 * ((((0.026512254f * fTemp16) + ((((0.0146310972f * fTemp18) + (0.0153087759f * fTemp12)) + (0.00134079799f * fTemp13)) + (0.00502167083f * fTemp15))) + (0.00843984634f * fTemp20)) - (((((0.00518922973f * fTemp10) + (0.0224228185f * fTemp11)) + (0.0234423932f * fTemp14)) + (0.0224194806f * fTemp19)) + (0.00138401077f * fTemp17))));
			float fTemp107 = (fConst41 * fRec183[1]);
			float fTemp108 = (fConst42 * fRec186[1]);
			fRec188[0] = (fTemp106 + (fTemp107 + (fRec188[1] + fTemp108)));
			fRec186[0] = fRec188[0];
			float fRec187 = ((fTemp108 + fTemp107) + fTemp106);
			fRec185[0] = (fRec186[0] + fRec185[1]);
			fRec183[0] = fRec185[0];
			float fRec184 = fRec187;
			fRec182[0] = (fTemp104 + (fTemp105 + (fRec184 + fRec182[1])));
			fRec180[0] = fRec182[0];
			float fRec181 = (fTemp104 + (fRec184 + fTemp105));
			fRec179[0] = (fRec180[0] + fRec179[1]);
			fRec177[0] = fRec179[0];
			float fRec178 = fRec181;
			fRec176[0] = (fTemp103 + (fRec178 + fRec176[1]));
			fRec174[0] = fRec176[0];
			float fRec175 = (fRec178 + fTemp103);
			float fTemp109 = (fConst49 * fRec189[1]);
			float fTemp110 = (fConst48 * fRec192[1]);
			float fTemp111 = (fConst45 * (((((0.00847988017f * fTemp30) + (0.024813259f * fTemp26)) + (0.0429758169f * fTemp29)) + (0.0048902398f * fTemp34)) - (((((0.0216123518f * fTemp31) + (0.00675196899f * fTemp32)) + (0.0251518711f * fTemp27)) + (0.0251863506f * fTemp28)) + (0.0216073338f * fTemp33))));
			float fTemp112 = (fConst47 * fRec195[1]);
			float fTemp113 = (fConst46 * fRec198[1]);
			fRec200[0] = (fTemp111 + (fTemp112 + (fRec200[1] + fTemp113)));
			fRec198[0] = fRec200[0];
			float fRec199 = ((fTemp113 + fTemp112) + fTemp111);
			fRec197[0] = (fRec198[0] + fRec197[1]);
			fRec195[0] = fRec197[0];
			float fRec196 = fRec199;
			fRec194[0] = (fTemp109 + (fTemp110 + (fRec196 + fRec194[1])));
			fRec192[0] = fRec194[0];
			float fRec193 = (fTemp109 + (fRec196 + fTemp110));
			fRec191[0] = (fRec192[0] + fRec191[1]);
			fRec189[0] = fRec191[0];
			float fRec190 = fRec193;
			float fTemp114 = (fConst51 * fRec201[1]);
			float fTemp115 = (fConst53 * (((0.0249754917f * fTemp38) + (0.0432558395f * fTemp41)) - (((0.0124645401f * fTemp43) + (((0.0154083548f * fTemp42) + (0.00523858657f * fTemp39)) + (0.0574972518f * fTemp40))) + (0.0124602364f * fTemp44))));
			float fTemp116 = (fConst54 * fRec204[1]);
			float fTemp117 = (fConst55 * fRec207[1]);
			fRec209[0] = (fTemp115 + (fTemp116 + (fRec209[1] + fTemp117)));
			fRec207[0] = fRec209[0];
			float fRec208 = ((fTemp117 + fTemp116) + fTemp115);
			fRec206[0] = (fRec207[0] + fRec206[1]);
			fRec204[0] = fRec206[0];
			float fRec205 = fRec208;
			fRec203[0] = (fTemp114 + (fRec205 + fRec203[1]));
			fRec201[0] = fRec203[0];
			float fRec202 = (fRec205 + fTemp114);
			float fTemp118 = (fConst57 * ((0.0539395995f * fTemp49) - ((0.0120689478f * fTemp51) + (0.0450384244f * fTemp50))));
			float fTemp119 = (fConst58 * fRec210[1]);
			fRec212[0] = (fTemp118 + (fRec212[1] + fTemp119));
			fRec210[0] = fRec212[0];
			float fRec211 = (fTemp119 + fTemp118);
			float fTemp120 = (fConst60 * ((((0.0150248362f * fTemp54) + (0.0289373379f * fTemp55)) + (0.0260208491f * fTemp57)) - ((0.0181305222f * fTemp58) + (0.0676586777f * fTemp56))));
			float fTemp121 = (fConst61 * fRec213[1]);
			float fTemp122 = (fConst62 * fRec216[1]);
			fRec218[0] = (fTemp120 + (fTemp121 + (fRec218[1] + fTemp122)));
			fRec216[0] = fRec218[0];
			float fRec217 = ((fTemp122 + fTemp121) + fTemp120);
			fRec215[0] = (fRec216[0] + fRec215[1]);
			fRec213[0] = fRec215[0];
			float fRec214 = fRec217;
			fVec3[(IOTA & 1023)] = ((0.0436849333f * fTemp62) + (fRec175 + (fRec190 + (fRec202 + (fRec211 + fRec214)))));
			output3[i] = FAUSTFLOAT((0.810182214f * (fRec0[0] * fVec3[((IOTA - iConst63) & 1023)])));
			float fTemp123 = (fConst37 * fRec222[1]);
			float fTemp124 = (fConst38 * fRec225[1]);
			float fTemp125 = (fConst40 * (((0.0005163881f * fTemp20) + ((((((0.00142081606f * fTemp10) + (0.0179761387f * fTemp11)) + (0.0121874176f * fTemp12)) + (0.00993892085f * fTemp13)) + (0.00646593235f * fTemp15)) + (0.0261413269f * fTemp19))) - ((((0.0142199956f * fTemp18) + (0.0223533325f * fTemp14)) + (0.0263587143f * fTemp16)) + (0.00222470704f * fTemp17))));
			float fTemp126 = (fConst42 * fRec231[1]);
			float fTemp127 = (fConst41 * fRec228[1]);
			fRec233[0] = (((fTemp125 + fRec233[1]) + fTemp126) + fTemp127);
			fRec231[0] = fRec233[0];
			float fRec232 = ((fTemp125 + fTemp126) + fTemp127);
			fRec230[0] = (fRec231[0] + fRec230[1]);
			fRec228[0] = fRec230[0];
			float fRec229 = fRec232;
			fRec227[0] = (fTemp123 + (fTemp124 + (fRec229 + fRec227[1])));
			fRec225[0] = fRec227[0];
			float fRec226 = (fTemp123 + (fRec229 + fTemp124));
			fRec224[0] = (fRec225[0] + fRec224[1]);
			fRec222[0] = fRec224[0];
			float fRec223 = fRec226;
			float fTemp128 = (fRec223 + (fConst35 * fRec219[1]));
			fRec221[0] = (fTemp128 + fRec221[1]);
			fRec219[0] = fRec221[0];
			float fRec220 = fTemp128;
			float fTemp129 = (fConst49 * fRec234[1]);
			float fTemp130 = (fConst48 * fRec237[1]);
			float fTemp131 = (fConst45 * ((((0.0151941935f * fTemp31) + (0.0291471407f * fTemp26)) + (0.0274789408f * fTemp33)) - (((0.04264443f * fTemp29) + ((((0.00832541194f * fTemp30) + (0.021207504f * fTemp32)) + (0.029193176f * fTemp27)) + (0.00277382997f * fTemp28))) + (0.000145507001f * fTemp34))));
			float fTemp132 = (fConst47 * fRec240[1]);
			float fTemp133 = (fConst46 * fRec243[1]);
			fRec245[0] = (fTemp131 + (fTemp132 + (fRec245[1] + fTemp133)));
			fRec243[0] = fRec245[0];
			float fRec244 = ((fTemp133 + fTemp132) + fTemp131);
			fRec242[0] = (fRec243[0] + fRec242[1]);
			fRec240[0] = fRec242[0];
			float fRec241 = fRec244;
			fRec239[0] = (fTemp129 + (fTemp130 + (fRec241 + fRec239[1])));
			fRec237[0] = fRec239[0];
			float fRec238 = (fTemp129 + (fRec241 + fTemp130));
			fRec236[0] = (fRec237[0] + fRec236[1]);
			fRec234[0] = fRec236[0];
			float fRec235 = fRec238;
			float fTemp134 = (fConst53 * ((((0.00808941945f * fTemp43) + (0.0338699184f * fTemp38)) + (0.0165906455f * fTemp44)) - ((((0.0586978495f * fTemp42) + (0.0106811747f * fTemp39)) + (0.016799517f * fTemp40)) + (0.0429462641f * fTemp41))));
			float fTemp135 = (fConst55 * fRec252[1]);
			float fTemp136 = (fConst54 * fRec249[1]);
			fRec254[0] = (((fTemp134 + fRec254[1]) + fTemp135) + fTemp136);
			fRec252[0] = fRec254[0];
			float fRec253 = ((fTemp134 + fTemp135) + fTemp136);
			fRec251[0] = (fRec252[0] + fRec251[1]);
			fRec249[0] = fRec251[0];
			float fRec250 = fRec253;
			float fTemp137 = (fConst51 * fRec246[1]);
			fRec248[0] = ((fRec250 + fRec248[1]) + fTemp137);
			fRec246[0] = fRec248[0];
			float fRec247 = (fRec250 + fTemp137);
			float fTemp138 = (fConst57 * ((0.0583168715f * fTemp49) - ((0.050208047f * fTemp51) + (0.0173970331f * fTemp50))));
			float fTemp139 = (fConst58 * fRec255[1]);
			fRec257[0] = (fTemp138 + (fRec257[1] + fTemp139));
			fRec255[0] = fRec257[0];
			float fRec256 = (fTemp139 + fTemp138);
			float fTemp140 = (fConst60 * (((0.0219038911f * fTemp54) + (0.0276680924f * fTemp55)) - (((0.0733433738f * fTemp58) + (0.0240597557f * fTemp56)) + (0.0258559622f * fTemp57))));
			float fTemp141 = (fConst61 * fRec258[1]);
			float fTemp142 = (fConst62 * fRec261[1]);
			fRec263[0] = (fTemp140 + (fTemp141 + (fRec263[1] + fTemp142)));
			fRec261[0] = fRec263[0];
			float fRec262 = ((fTemp142 + fTemp141) + fTemp140);
			fRec260[0] = (fRec261[0] + fRec260[1]);
			fRec258[0] = fRec260[0];
			float fRec259 = fRec262;
			fVec4[(IOTA & 1023)] = ((fRec220 + (fRec235 + (fRec247 + (fRec256 + fRec259)))) + (0.0487356819f * fTemp62));
			output4[i] = FAUSTFLOAT((0.810182214f * (fRec0[0] * fVec4[((IOTA - iConst63) & 1023)])));
			float fTemp143 = (fConst90 * (((0.00127564825f * fTemp17) + ((3.34629999e-06f * fTemp18) + (0.0122297723f * fTemp13))) - ((0.00824802462f * fTemp20) + ((0.0216729455f * fTemp19) + (((0.0122392038f * fTemp15) + ((((0.00126983703f * fTemp10) + (0.0216718353f * fTemp11)) + (0.0272416491f * fTemp12)) + (0.0213053767f * fTemp14))) + (4.63339984e-06f * fTemp16))))));
			float fTemp144 = (fConst92 * fRec276[1]);
			float fTemp145 = (fConst91 * fRec273[1]);
			fRec278[0] = (((fTemp143 + fRec278[1]) + fTemp144) + fTemp145);
			fRec276[0] = fRec278[0];
			float fRec277 = ((fTemp143 + fTemp144) + fTemp145);
			fRec275[0] = (fRec276[0] + fRec275[1]);
			fRec273[0] = fRec275[0];
			float fRec274 = fRec277;
			float fTemp146 = (fConst88 * fRec270[1]);
			float fTemp147 = (fConst87 * fRec267[1]);
			fRec272[0] = (((fRec274 + fRec272[1]) + fTemp146) + fTemp147);
			fRec270[0] = fRec272[0];
			float fRec271 = ((fRec274 + fTemp146) + fTemp147);
			fRec269[0] = (fRec270[0] + fRec269[1]);
			fRec267[0] = fRec269[0];
			float fRec268 = fRec271;
			float fTemp148 = (fConst85 * fRec264[1]);
			fRec266[0] = ((fRec268 + fRec266[1]) + fTemp148);
			fRec264[0] = fRec266[0];
			float fRec265 = (fRec268 + fTemp148);
			float fTemp149 = (fConst78 * fRec279[1]);
			float fTemp150 = (fConst79 * fRec282[1]);
			float fTemp151 = (fConst81 * (((8.29700014e-07f * fTemp29) + ((8.51399989e-07f * fTemp30) + (0.0115920417f * fTemp28))) - ((0.00448405789f * fTemp34) + (((((0.0210450049f * fTemp31) + (0.0533769578f * fTemp26)) + (0.0116007207f * fTemp32)) + (0.0332350805f * fTemp27)) + (0.0210410822f * fTemp33)))));
			float fTemp152 = (fConst82 * fRec285[1]);
			float fTemp153 = (fConst83 * fRec288[1]);
			fRec290[0] = (fTemp151 + (fTemp152 + (fRec290[1] + fTemp153)));
			fRec288[0] = fRec290[0];
			float fRec289 = ((fTemp153 + fTemp152) + fTemp151);
			fRec287[0] = (fRec288[0] + fRec287[1]);
			fRec285[0] = fRec287[0];
			float fRec286 = fRec289;
			fRec284[0] = (fTemp149 + (fTemp150 + (fRec286 + fRec284[1])));
			fRec282[0] = fRec284[0];
			float fRec283 = (fTemp149 + (fRec286 + fTemp150));
			fRec281[0] = (fRec282[0] + fRec281[1]);
			fRec279[0] = fRec281[0];
			float fRec280 = fRec283;
			float fTemp154 = (fConst65 * fRec291[1]);
			float fTemp155 = (fConst67 * (((0.0442743786f * fTemp40) + (5.98499992e-06f * fTemp41)) - (((0.0122086443f * fTemp43) + (((0.0582810603f * fTemp38) + (0.0442765392f * fTemp42)) + (0.0160852037f * fTemp39))) + (0.0122040147f * fTemp44))));
			float fTemp156 = (fConst68 * fRec294[1]);
			float fTemp157 = (fConst69 * fRec297[1]);
			fRec299[0] = (fTemp155 + (fTemp156 + (fRec299[1] + fTemp157)));
			fRec297[0] = fRec299[0];
			float fRec298 = ((fTemp157 + fTemp156) + fTemp155);
			fRec296[0] = (fRec297[0] + fRec296[1]);
			fRec294[0] = fRec296[0];
			float fRec295 = fRec298;
			fRec293[0] = (fTemp154 + (fRec295 + fRec293[1]));
			fRec291[0] = fRec293[0];
			float fRec292 = (fRec295 + fTemp154);
			float fTemp158 = (fConst75 * (((0.062723957f * fTemp49) + (0.0419793911f * fTemp50)) - (0.0419737063f * fTemp51)));
			float fTemp159 = (fConst76 * fRec300[1]);
			fRec302[0] = (fTemp158 + (fRec302[1] + fTemp159));
			fRec300[0] = fRec302[0];
			float fRec301 = (fTemp159 + fTemp158);
			float fTemp160 = (fConst71 * ((((0.0264462121f * fTemp55) + (0.059475638f * fTemp56)) + (5.71769988e-06f * fTemp57)) - ((0.0366183743f * fTemp54) + (0.0594708472f * fTemp58))));
			float fTemp161 = (fConst72 * fRec303[1]);
			float fTemp162 = (fConst73 * fRec306[1]);
			fRec308[0] = (fTemp160 + (fTemp161 + (fRec308[1] + fTemp162)));
			fRec306[0] = fRec308[0];
			float fRec307 = ((fTemp162 + fTemp161) + fTemp160);
			fRec305[0] = (fRec306[0] + fRec305[1]);
			fRec303[0] = fRec305[0];
			float fRec304 = fRec307;
			fVec5[(IOTA & 1023)] = ((fRec265 + (fRec280 + (fRec292 + (fRec301 + fRec304)))) + (0.0537970774f * fTemp62));
			output5[i] = FAUSTFLOAT((0.809967816f * (fRec0[0] * fVec5[((IOTA - iConst93) & 1023)])));
			float fTemp163 = (fConst49 * fRec309[1]);
			float fTemp164 = (fConst48 * fRec312[1]);
			float fTemp165 = (fConst45 * (((0.00488989148f * fTemp34) + ((0.0216042716f * fTemp33) + ((0.0429839194f * fTemp29) + (((((0.00846256502f * fTemp30) + (0.0216079839f * fTemp31)) + (0.0248232279f * fTemp26)) + (0.00674414216f * fTemp32)) + (0.0251808409f * fTemp28))))) - (0.0251621306f * fTemp27)));
			float fTemp166 = (fConst47 * fRec315[1]);
			float fTemp167 = (fConst46 * fRec318[1]);
			fRec320[0] = (fTemp165 + (fTemp166 + (fRec320[1] + fTemp167)));
			fRec318[0] = fRec320[0];
			float fRec319 = ((fTemp167 + fTemp166) + fTemp165);
			fRec317[0] = (fRec318[0] + fRec317[1]);
			fRec315[0] = fRec317[0];
			float fRec316 = fRec319;
			fRec314[0] = (fTemp163 + (fTemp164 + (fRec316 + fRec314[1])));
			fRec312[0] = fRec314[0];
			float fRec313 = (fTemp163 + (fRec316 + fTemp164));
			fRec311[0] = (fRec312[0] + fRec311[1]);
			fRec309[0] = fRec311[0];
			float fRec310 = fRec313;
			float fTemp168 = (fConst51 * fRec321[1]);
			float fTemp169 = (fConst53 * (((0.0124570848f * fTemp44) + ((0.0432575345f * fTemp41) + ((0.0124552557f * fTemp43) + (((0.0249784216f * fTemp38) + (0.0154068097f * fTemp42)) + (0.0574973635f * fTemp40))))) - (0.0052484558f * fTemp39)));
			float fTemp170 = (fConst54 * fRec324[1]);
			float fTemp171 = (fConst55 * fRec327[1]);
			fRec329[0] = (fTemp169 + (fTemp170 + (fRec329[1] + fTemp171)));
			fRec327[0] = fRec329[0];
			float fRec328 = ((fTemp171 + fTemp170) + fTemp169);
			fRec326[0] = (fRec327[0] + fRec326[1]);
			fRec324[0] = fRec326[0];
			float fRec325 = fRec328;
			fRec323[0] = (fTemp168 + (fRec325 + fRec323[1]));
			fRec321[0] = fRec323[0];
			float fRec322 = (fRec325 + fTemp168);
			float fTemp172 = (fConst57 * (((0.0120678125f * fTemp51) + (0.0539331064f * fTemp49)) + (0.0450365283f * fTemp50)));
			float fTemp173 = (fConst58 * fRec330[1]);
			fRec332[0] = (fTemp172 + (fRec332[1] + fTemp173));
			fRec330[0] = fRec332[0];
			float fRec331 = (fTemp173 + fTemp172);
			float fTemp174 = (fConst60 * (((((0.0150224213f * fTemp54) + (0.0181299727f * fTemp58)) + (0.0289299879f * fTemp55)) + (0.0676582232f * fTemp56)) + (0.0260189939f * fTemp57)));
			float fTemp175 = (fConst61 * fRec333[1]);
			float fTemp176 = (fConst62 * fRec336[1]);
			fRec338[0] = (fTemp174 + (fTemp175 + (fRec338[1] + fTemp176)));
			fRec336[0] = fRec338[0];
			float fRec337 = ((fTemp176 + fTemp175) + fTemp174);
			fRec335[0] = (fRec336[0] + fRec335[1]);
			fRec333[0] = fRec335[0];
			float fRec334 = fRec337;
			float fTemp177 = (fConst35 * fRec339[1]);
			float fTemp178 = (fConst37 * fRec342[1]);
			float fTemp179 = (fConst38 * fRec345[1]);
			float fTemp180 = (fConst40 * (((0.00139022875f * fTemp17) + ((0.00843670592f * fTemp20) + ((0.0224210471f * fTemp19) + (((((0.00516870944f * fTemp10) + (0.0146124065f * fTemp18)) + (0.022433497f * fTemp11)) + (0.0153158382f * fTemp12)) + (0.0265227742f * fTemp16))))) - ((0.00503520714f * fTemp15) + ((0.00135706866f * fTemp13) + (0.023445176f * fTemp14)))));
			float fTemp181 = (fConst41 * fRec348[1]);
			float fTemp182 = (fConst42 * fRec351[1]);
			fRec353[0] = (fTemp180 + (fTemp181 + (fRec353[1] + fTemp182)));
			fRec351[0] = fRec353[0];
			float fRec352 = ((fTemp182 + fTemp181) + fTemp180);
			fRec350[0] = (fRec350[1] + fRec351[0]);
			fRec348[0] = fRec350[0];
			float fRec349 = fRec352;
			fRec347[0] = ((fTemp178 + (fRec347[1] + fTemp179)) + fRec349);
			fRec345[0] = fRec347[0];
			float fRec346 = ((fTemp179 + fTemp178) + fRec349);
			fRec344[0] = (fRec344[1] + fRec345[0]);
			fRec342[0] = fRec344[0];
			float fRec343 = fRec346;
			fRec341[0] = ((fRec341[1] + fTemp177) + fRec343);
			fRec339[0] = fRec341[0];
			float fRec340 = (fTemp177 + fRec343);
			fVec6[(IOTA & 1023)] = ((0.0436802469f * fTemp62) + ((fRec310 + (fRec322 + (fRec331 + fRec334))) + fRec340));
			output6[i] = FAUSTFLOAT((0.810182214f * (fRec0[0] * fVec6[((IOTA - iConst63) & 1023)])));
			float fTemp183 = (fConst95 * (((0.0444157235f * fTemp51) + (0.0155758914f * fTemp49)) - (0.0022798581f * fTemp50)));
			float fTemp184 = (fConst96 * fRec354[1]);
			fRec356[0] = (fTemp183 + (fRec356[1] + fTemp184));
			fRec354[0] = fRec356[0];
			float fRec355 = (fTemp184 + fTemp183);
			float fTemp185 = (fConst98 * ((0.0300963297f * fTemp58) - ((((0.00395526737f * fTemp54) + (0.0187431257f * fTemp55)) + (0.0026183913f * fTemp56)) + (0.0431320332f * fTemp57))));
			float fTemp186 = (fConst99 * fRec357[1]);
			float fTemp187 = (fConst100 * fRec360[1]);
			fRec362[0] = (fTemp185 + (fTemp186 + (fRec362[1] + fTemp187)));
			fRec360[0] = fRec362[0];
			float fRec361 = ((fTemp187 + fTemp186) + fTemp185);
			fRec359[0] = (fRec359[1] + fRec360[0]);
			fRec357[0] = fRec359[0];
			float fRec358 = fRec361;
			float fTemp188 = (fConst102 * fRec363[1]);
			float fTemp189 = (fConst104 * ((0.00467731338f * fTemp44) - (((0.0389770418f * fTemp43) + ((((0.00530663319f * fTemp38) + (0.0152090061f * fTemp42)) + (0.0237736925f * fTemp39)) + (0.000768534199f * fTemp40))) + (0.0325269736f * fTemp41))));
			float fTemp190 = (fConst105 * fRec366[1]);
			float fTemp191 = (fConst106 * fRec369[1]);
			fRec371[0] = (fTemp189 + (fTemp190 + (fRec371[1] + fTemp191)));
			fRec369[0] = fRec371[0];
			float fRec370 = ((fTemp191 + fTemp190) + fTemp189);
			fRec368[0] = (fRec368[1] + fRec369[0]);
			fRec366[0] = fRec368[0];
			float fRec367 = fRec370;
			fRec365[0] = ((fRec365[1] + fTemp188) + fRec367);
			fRec363[0] = fRec365[0];
			float fRec364 = (fTemp188 + fRec367);
			float fTemp192 = (fConst108 * fRec372[1]);
			float fTemp193 = (fConst109 * fRec375[1]);
			float fTemp194 = (fConst111 * (((0.03340986f * fTemp34) + ((0.00701687811f * fTemp33) + ((0.00712639652f * fTemp29) + ((0.0015766914f * fTemp28) + ((0.00450910209f * fTemp30) + (0.000364105887f * fTemp27)))))) - (((0.0308037866f * fTemp31) + (0.00294012646f * fTemp26)) + (0.0306532942f * fTemp32))));
			float fTemp195 = (fConst112 * fRec378[1]);
			float fTemp196 = (fConst113 * fRec381[1]);
			fRec383[0] = (fTemp194 + (fTemp195 + (fRec383[1] + fTemp196)));
			fRec381[0] = fRec383[0];
			float fRec382 = ((fTemp196 + fTemp195) + fTemp194);
			fRec380[0] = (fRec380[1] + fRec381[0]);
			fRec378[0] = fRec380[0];
			float fRec379 = fRec382;
			fRec377[0] = ((fTemp192 + (fRec377[1] + fTemp193)) + fRec379);
			fRec375[0] = fRec377[0];
			float fRec376 = ((fTemp193 + fTemp192) + fRec379);
			fRec374[0] = (fRec374[1] + fRec375[0]);
			fRec372[0] = fRec374[0];
			float fRec373 = fRec376;
			float fTemp197 = (fConst115 * fRec384[1]);
			float fTemp198 = (fConst117 * fRec387[1]);
			float fTemp199 = (fConst118 * fRec390[1]);
			float fTemp200 = (fConst120 * (((0.0263928212f * fTemp20) + ((0.00514213322f * fTemp19) + ((0.0248516984f * fTemp16) + ((0.00228213286f * fTemp15) + (((((0.0274159759f * fTemp10) + (0.00736536924f * fTemp18)) + (0.00237595872f * fTemp11)) + (0.00136450958f * fTemp12)) + (0.0160905775f * fTemp14)))))) - ((0.00719280029f * fTemp13) + (0.00370931602f * fTemp17))));
			float fTemp201 = (fConst121 * fRec393[1]);
			float fTemp202 = (fConst122 * fRec396[1]);
			fRec398[0] = (fTemp200 + (fTemp201 + (fRec398[1] + fTemp202)));
			fRec396[0] = fRec398[0];
			float fRec397 = ((fTemp202 + fTemp201) + fTemp200);
			fRec395[0] = (fRec395[1] + fRec396[0]);
			fRec393[0] = fRec395[0];
			float fRec394 = fRec397;
			fRec392[0] = ((fTemp198 + (fRec392[1] + fTemp199)) + fRec394);
			fRec390[0] = fRec392[0];
			float fRec391 = ((fTemp199 + fTemp198) + fRec394);
			fRec389[0] = (fRec389[1] + fRec390[0]);
			fRec387[0] = fRec389[0];
			float fRec388 = fRec391;
			fRec386[0] = ((fRec386[1] + fTemp197) + fRec388);
			fRec384[0] = fRec386[0];
			float fRec385 = (fTemp197 + fRec388);
			fVec7[(IOTA & 1023)] = ((0.0283387434f * fTemp62) + ((((fRec355 + fRec358) + fRec364) + fRec373) + fRec385));
			output7[i] = FAUSTFLOAT((0.879635572f * (fRec0[0] * fVec7[((IOTA - iConst123) & 1023)])));
			float fTemp203 = (fConst125 * fRec399[1]);
			float fTemp204 = (fConst127 * ((((0.00176773127f * fTemp43) + (0.0178551711f * fTemp40)) + (0.0492927805f * fTemp44)) - ((((0.0179782622f * fTemp38) + (0.0309820846f * fTemp42)) + (0.0147790294f * fTemp39)) + (0.00857336447f * fTemp41))));
			float fTemp205 = (fConst128 * fRec402[1]);
			float fTemp206 = (fConst129 * fRec405[1]);
			fRec407[0] = (fTemp204 + (fTemp205 + (fRec407[1] + fTemp206)));
			fRec405[0] = fRec407[0];
			float fRec406 = ((fTemp206 + fTemp205) + fTemp204);
			fRec404[0] = (fRec405[0] + fRec404[1]);
			fRec402[0] = fRec404[0];
			float fRec403 = fRec406;
			fRec401[0] = (fTemp203 + (fRec403 + fRec401[1]));
			fRec399[0] = fRec401[0];
			float fRec400 = (fRec403 + fTemp203);
			float fTemp207 = (fConst131 * fRec408[1]);
			float fTemp208 = (fConst132 * fRec411[1]);
			float fTemp209 = (fConst134 * (((0.0205909833f * fTemp33) + ((0.0145821003f * fTemp29) + ((0.0112832412f * fTemp28) + ((((0.0348029062f * fTemp30) + (0.00238420023f * fTemp31)) + (0.0242457613f * fTemp26)) + (0.018440865f * fTemp27))))) - ((0.0182964131f * fTemp32) + (0.0224308036f * fTemp34))));
			float fTemp210 = (fConst135 * fRec414[1]);
			float fTemp211 = (fConst136 * fRec417[1]);
			fRec419[0] = (fTemp209 + (fTemp210 + (fRec419[1] + fTemp211)));
			fRec417[0] = fRec419[0];
			float fRec418 = ((fTemp211 + fTemp210) + fTemp209);
			fRec416[0] = (fRec417[0] + fRec416[1]);
			fRec414[0] = fRec416[0];
			float fRec415 = fRec418;
			fRec413[0] = (fTemp207 + (fTemp208 + (fRec415 + fRec413[1])));
			fRec411[0] = fRec413[0];
			float fRec412 = (fTemp207 + (fRec415 + fTemp208));
			fRec410[0] = (fRec411[0] + fRec410[1]);
			fRec408[0] = fRec410[0];
			float fRec409 = fRec412;
			float fTemp212 = (fConst138 * (((0.0463183224f * fTemp51) + (0.00839097518f * fTemp49)) - (0.0275335684f * fTemp50)));
			float fTemp213 = (fConst139 * fRec420[1]);
			fRec422[0] = (fTemp212 + (fRec422[1] + fTemp213));
			fRec420[0] = fRec422[0];
			float fRec421 = (fTemp213 + fTemp212);
			float fTemp214 = (fConst141 * ((0.0144425482f * fTemp58) - ((((0.0473840795f * fTemp54) + (0.0301495101f * fTemp55)) + (0.00914137997f * fTemp56)) + (0.0258442741f * fTemp57))));
			float fTemp215 = (fConst142 * fRec423[1]);
			float fTemp216 = (fConst143 * fRec426[1]);
			fRec428[0] = (fTemp214 + (fTemp215 + (fRec428[1] + fTemp216)));
			fRec426[0] = fRec428[0];
			float fRec427 = ((fTemp216 + fTemp215) + fTemp214);
			fRec425[0] = (fRec425[1] + fRec426[0]);
			fRec423[0] = fRec425[0];
			float fRec424 = fRec427;
			float fTemp217 = (fConst145 * fRec429[1]);
			float fTemp218 = (fConst147 * fRec432[1]);
			float fTemp219 = (fConst148 * fRec435[1]);
			float fTemp220 = (fConst150 * ((((0.0138859749f * fTemp14) + ((((0.0154887084f * fTemp18) + (0.001238061f * fTemp11)) + (0.0176886655f * fTemp12)) + (0.0158764608f * fTemp13))) + (0.00916762184f * fTemp16)) - ((0.0139576616f * fTemp17) + ((0.0124196196f * fTemp20) + (((0.0283428952f * fTemp10) + (0.00850122049f * fTemp15)) + (0.0204571579f * fTemp19))))));
			float fTemp221 = (fConst151 * fRec438[1]);
			float fTemp222 = (fConst152 * fRec441[1]);
			fRec443[0] = (fTemp220 + (fTemp221 + (fRec443[1] + fTemp222)));
			fRec441[0] = fRec443[0];
			float fRec442 = ((fTemp222 + fTemp221) + fTemp220);
			fRec440[0] = (fRec440[1] + fRec441[0]);
			fRec438[0] = fRec440[0];
			float fRec439 = fRec442;
			fRec437[0] = ((fTemp218 + (fRec437[1] + fTemp219)) + fRec439);
			fRec435[0] = fRec437[0];
			float fRec436 = ((fTemp219 + fTemp218) + fRec439);
			fRec434[0] = (fRec434[1] + fRec435[0]);
			fRec432[0] = fRec434[0];
			float fRec433 = fRec436;
			fRec431[0] = ((fRec431[1] + fTemp217) + fRec433);
			fRec429[0] = fRec431[0];
			float fRec430 = (fTemp217 + fRec433);
			fVec8[(IOTA & 1023)] = ((0.0329640992f * fTemp62) + ((((fRec400 + fRec409) + fRec421) + fRec424) + fRec430));
			output8[i] = FAUSTFLOAT((0.880171478f * (fRec0[0] * fVec8[((IOTA - iConst153) & 1023)])));
			float fTemp223 = (fConst138 * (((0.020101551f * fTemp51) + (0.00860498939f * fTemp49)) - (0.0390066057f * fTemp50)));
			float fTemp224 = (fConst139 * fRec444[1]);
			fRec446[0] = (fTemp223 + (fRec446[1] + fTemp224));
			fRec444[0] = fRec446[0];
			float fRec445 = (fTemp224 + fTemp223);
			float fTemp225 = (fConst141 * (((0.00942643546f * fTemp58) + (0.0260256138f * fTemp57)) - (((0.0360338092f * fTemp54) + (0.0236546472f * fTemp55)) + (0.0148056475f * fTemp56))));
			float fTemp226 = (fConst142 * fRec447[1]);
			float fTemp227 = (fConst143 * fRec450[1]);
			fRec452[0] = (fTemp225 + (fTemp226 + (fRec452[1] + fTemp227)));
			fRec450[0] = fRec452[0];
			float fRec451 = ((fTemp227 + fTemp226) + fTemp225);
			fRec449[0] = (fRec449[1] + fRec450[0]);
			fRec447[0] = fRec449[0];
			float fRec448 = fRec451;
			float fTemp228 = (fConst125 * fRec453[1]);
			float fTemp229 = (fConst127 * (((0.00871290267f * fTemp41) + ((0.0411527641f * fTemp43) + (0.0246891771f * fTemp40))) - ((((0.0185482372f * fTemp38) + (0.011468906f * fTemp42)) + (0.0152193001f * fTemp39)) + (0.006762248f * fTemp44))));
			float fTemp230 = (fConst128 * fRec456[1]);
			float fTemp231 = (fConst129 * fRec459[1]);
			fRec461[0] = (fTemp229 + (fTemp230 + (fRec461[1] + fTemp231)));
			fRec459[0] = fRec461[0];
			float fRec460 = ((fTemp231 + fTemp230) + fTemp229);
			fRec458[0] = (fRec458[1] + fRec459[0]);
			fRec456[0] = fRec458[0];
			float fRec457 = fRec460;
			fRec455[0] = ((fRec455[1] + fTemp228) + fRec457);
			fRec453[0] = fRec455[0];
			float fRec454 = (fTemp228 + fRec457);
			float fTemp232 = (fConst131 * fRec462[1]);
			float fTemp233 = (fConst132 * fRec465[1]);
			float fTemp234 = (fConst134 * ((((0.018874919f * fTemp28) + (((0.0212185886f * fTemp31) + (0.0155205671f * fTemp26)) + (0.0126532419f * fTemp27))) + (0.00266820239f * fTemp33)) - ((((0.035164997f * fTemp30) + (0.0117352279f * fTemp32)) + (0.0147032253f * fTemp29)) + (0.0105686747f * fTemp34))));
			float fTemp235 = (fConst135 * fRec468[1]);
			float fTemp236 = (fConst136 * fRec471[1]);
			fRec473[0] = (fTemp234 + (fTemp235 + (fRec473[1] + fTemp236)));
			fRec471[0] = fRec473[0];
			float fRec472 = ((fTemp236 + fTemp235) + fTemp234);
			fRec470[0] = (fRec470[1] + fRec471[0]);
			fRec468[0] = fRec470[0];
			float fRec469 = fRec472;
			fRec467[0] = ((fTemp232 + (fRec467[1] + fTemp233)) + fRec469);
			fRec465[0] = fRec467[0];
			float fRec466 = ((fTemp233 + fTemp232) + fRec469);
			fRec464[0] = (fRec464[1] + fRec465[0]);
			fRec462[0] = fRec464[0];
			float fRec463 = fRec466;
			float fTemp237 = (fConst145 * fRec474[1]);
			float fTemp238 = (fConst147 * fRec477[1]);
			float fTemp239 = (fConst148 * fRec480[1]);
			float fTemp240 = (fConst150 * (((((0.0144652938f * fTemp14) + (((0.022498358f * fTemp10) + (0.0184805356f * fTemp12)) + (0.00311687542f * fTemp13))) + (0.00727325957f * fTemp19)) + (0.0204225518f * fTemp17)) - (((0.00936668459f * fTemp16) + (((0.0158463754f * fTemp18) + (0.0146535337f * fTemp11)) + (0.0105599165f * fTemp15))) + (0.0131051783f * fTemp20))));
			float fTemp241 = (fConst151 * fRec483[1]);
			float fTemp242 = (fConst152 * fRec486[1]);
			fRec488[0] = (fTemp240 + (fTemp241 + (fRec488[1] + fTemp242)));
			fRec486[0] = fRec488[0];
			float fRec487 = ((fTemp242 + fTemp241) + fTemp240);
			fRec485[0] = (fRec485[1] + fRec486[0]);
			fRec483[0] = fRec485[0];
			float fRec484 = fRec487;
			fRec482[0] = ((fTemp238 + (fRec482[1] + fTemp239)) + fRec484);
			fRec480[0] = fRec482[0];
			float fRec481 = ((fTemp239 + fTemp238) + fRec484);
			fRec479[0] = (fRec479[1] + fRec480[0]);
			fRec477[0] = fRec479[0];
			float fRec478 = fRec481;
			fRec476[0] = ((fRec476[1] + fTemp237) + fRec478);
			fRec474[0] = fRec476[0];
			float fRec475 = (fTemp237 + fRec478);
			fVec9[(IOTA & 1023)] = ((0.026858544f * fTemp62) + ((((fRec445 + fRec448) + fRec454) + fRec463) + fRec475));
			output9[i] = FAUSTFLOAT((0.880171478f * (fRec0[0] * fVec9[((IOTA - iConst153) & 1023)])));
			float fTemp243 = (fConst95 * (((0.00227736658f * fTemp51) + (0.0155748995f * fTemp49)) - (0.0444059633f * fTemp50)));
			float fTemp244 = (fConst96 * fRec489[1]);
			fRec491[0] = (fTemp243 + (fRec491[1] + fTemp244));
			fRec489[0] = fRec491[0];
			float fRec490 = (fTemp244 + fTemp243);
			float fTemp245 = (fConst98 * (((0.00261935568f * fTemp58) + (0.0431238264f * fTemp57)) - (((0.00395008409f * fTemp54) + (0.0187353157f * fTemp55)) + (0.0300932992f * fTemp56))));
			float fTemp246 = (fConst99 * fRec492[1]);
			float fTemp247 = (fConst100 * fRec495[1]);
			fRec497[0] = (fTemp245 + (fTemp246 + (fRec497[1] + fTemp247)));
			fRec495[0] = fRec497[0];
			float fRec496 = ((fTemp247 + fTemp246) + fTemp245);
			fRec494[0] = (fRec494[1] + fRec495[0]);
			fRec492[0] = fRec494[0];
			float fRec493 = fRec496;
			float fTemp248 = (fConst102 * fRec498[1]);
			float fTemp249 = (fConst104 * (((0.0325262211f * fTemp41) + ((0.00467047235f * fTemp43) + ((0.000772691681f * fTemp42) + (0.0152002154f * fTemp40)))) - (((0.0237655547f * fTemp39) + (0.00530857872f * fTemp38)) + (0.0389713533f * fTemp44))));
			float fTemp250 = (fConst105 * fRec501[1]);
			float fTemp251 = (fConst106 * fRec504[1]);
			fRec506[0] = (fTemp249 + (fTemp250 + (fRec506[1] + fTemp251)));
			fRec504[0] = fRec506[0];
			float fRec505 = ((fTemp251 + fTemp250) + fTemp249);
			fRec503[0] = (fRec503[1] + fRec504[0]);
			fRec501[0] = fRec503[0];
			float fRec502 = fRec505;
			fRec500[0] = ((fRec500[1] + fTemp248) + fRec502);
			fRec498[0] = fRec500[0];
			float fRec499 = (fTemp248 + fRec502);
			float fTemp252 = (fConst108 * fRec507[1]);
			float fTemp253 = (fConst109 * fRec510[1]);
			float fTemp254 = (fConst111 * ((((0.0306398347f * fTemp28) + ((0.00702093029f * fTemp31) + (0.000365016604f * fTemp27))) + (0.0334056392f * fTemp34)) - ((0.0308090001f * fTemp33) + ((((0.00450190995f * fTemp30) + (0.00294783991f * fTemp26)) + (0.00157478405f * fTemp32)) + (0.00711879041f * fTemp29)))));
			float fTemp255 = (fConst112 * fRec513[1]);
			float fTemp256 = (fConst113 * fRec516[1]);
			fRec518[0] = (fTemp254 + (fTemp255 + (fRec518[1] + fTemp256)));
			fRec516[0] = fRec518[0];
			float fRec517 = ((fTemp256 + fTemp255) + fTemp254);
			fRec515[0] = (fRec515[1] + fRec516[0]);
			fRec513[0] = fRec515[0];
			float fRec514 = fRec517;
			fRec512[0] = ((fTemp252 + (fRec512[1] + fTemp253)) + fRec514);
			fRec510[0] = fRec512[0];
			float fRec511 = ((fTemp253 + fTemp252) + fRec514);
			fRec509[0] = (fRec509[1] + fRec510[0]);
			fRec507[0] = fRec509[0];
			float fRec508 = fRec511;
			float fTemp257 = (fConst120 * (((0.0264051445f * fTemp20) + (((0.00718313688f * fTemp15) + ((((0.0037025637f * fTemp10) + (0.0051531205f * fTemp11)) + (0.00136097113f * fTemp12)) + (0.0160809606f * fTemp14))) + (0.00236531789f * fTemp19))) - ((((0.00737311458f * fTemp18) + (0.00228301343f * fTemp13)) + (0.0248417631f * fTemp16)) + (0.0274109263f * fTemp17))));
			float fTemp258 = (fConst121 * fRec528[1]);
			float fTemp259 = (fConst122 * fRec531[1]);
			fRec533[0] = (fTemp257 + (fTemp258 + (fRec533[1] + fTemp259)));
			fRec531[0] = fRec533[0];
			float fRec532 = ((fTemp259 + fTemp258) + fTemp257);
			fRec530[0] = (fRec530[1] + fRec531[0]);
			fRec528[0] = fRec530[0];
			float fRec529 = fRec532;
			float fTemp260 = (fConst117 * fRec522[1]);
			float fTemp261 = (fConst118 * fRec525[1]);
			fRec527[0] = (fRec529 + (fTemp260 + (fRec527[1] + fTemp261)));
			fRec525[0] = fRec527[0];
			float fRec526 = ((fTemp261 + fTemp260) + fRec529);
			fRec524[0] = (fRec524[1] + fRec525[0]);
			fRec522[0] = fRec524[0];
			float fRec523 = fRec526;
			float fTemp262 = (fConst115 * fRec519[1]);
			fRec521[0] = (fRec523 + (fRec521[1] + fTemp262));
			fRec519[0] = fRec521[0];
			float fRec520 = (fTemp262 + fRec523);
			fVec10[(IOTA & 1023)] = ((0.0283328258f * fTemp62) + ((((fRec490 + fRec493) + fRec499) + fRec508) + fRec520));
			output10[i] = FAUSTFLOAT((0.879635572f * (fRec0[0] * fVec10[((IOTA - iConst123) & 1023)])));
			float fTemp263 = (fConst145 * fRec534[1]);
			float fTemp264 = (fConst147 * fRec537[1]);
			float fTemp265 = (fConst148 * fRec540[1]);
			float fTemp266 = (fConst150 * ((((((0.018074004f * fTemp18) + (0.0139631499f * fTemp11)) + (0.014911321f * fTemp14)) + (0.00515437406f * fTemp19)) + (0.0300566144f * fTemp17)) - (((0.0104542915f * fTemp16) + ((((0.0130757559f * fTemp10) + (0.0208330899f * fTemp12)) + (0.00172267831f * fTemp13)) + (0.00755713927f * fTemp15))) + (0.0189464018f * fTemp20))));
			float fTemp267 = (fConst151 * fRec543[1]);
			float fTemp268 = (fConst152 * fRec546[1]);
			fRec548[0] = (fTemp266 + (fTemp267 + (fRec548[1] + fTemp268)));
			fRec546[0] = fRec548[0];
			float fRec547 = ((fTemp268 + fTemp267) + fTemp266);
			fRec545[0] = (fRec546[0] + fRec545[1]);
			fRec543[0] = fRec545[0];
			float fRec544 = fRec547;
			fRec542[0] = (fTemp264 + (fTemp265 + (fRec544 + fRec542[1])));
			fRec540[0] = fRec542[0];
			float fRec541 = (fTemp264 + (fRec544 + fTemp265));
			fRec539[0] = (fRec540[0] + fRec539[1]);
			fRec537[0] = fRec539[0];
			float fRec538 = fRec541;
			fRec536[0] = (fTemp263 + (fRec538 + fRec536[1]));
			fRec534[0] = fRec536[0];
			float fRec535 = (fRec538 + fTemp263);
			float fTemp269 = (fConst131 * fRec549[1]);
			float fTemp270 = (fConst132 * fRec552[1]);
			float fTemp271 = (fConst134 * ((((0.0245014243f * fTemp28) + (((0.0364211053f * fTemp30) + (0.015966678f * fTemp32)) + (0.0119467424f * fTemp27))) + (0.00702388864f * fTemp33)) - ((((0.0295888055f * fTemp31) + (0.0176827759f * fTemp26)) + (0.0136140166f * fTemp29)) + (0.0262343492f * fTemp34))));
			float fTemp272 = (fConst135 * fRec555[1]);
			float fTemp273 = (fConst136 * fRec558[1]);
			fRec560[0] = (fTemp271 + (fTemp272 + (fRec560[1] + fTemp273)));
			fRec558[0] = fRec560[0];
			float fRec559 = ((fTemp273 + fTemp272) + fTemp271);
			fRec557[0] = (fRec558[0] + fRec557[1]);
			fRec555[0] = fRec557[0];
			float fRec556 = fRec559;
			fRec554[0] = (fTemp269 + (fTemp270 + (fRec556 + fRec554[1])));
			fRec552[0] = fRec554[0];
			float fRec553 = (fTemp269 + (fRec556 + fTemp270));
			fRec551[0] = (fRec552[0] + fRec551[1]);
			fRec549[0] = fRec551[0];
			float fRec550 = fRec553;
			float fTemp274 = (fConst125 * fRec561[1]);
			float fTemp275 = (fConst127 * (((0.00468057534f * fTemp44) + ((0.0111199031f * fTemp41) + (((0.0298867859f * fTemp38) + (0.0155930547f * fTemp42)) + (0.0285520405f * fTemp40)))) - ((0.0553105474f * fTemp43) + (0.0229106862f * fTemp39))));
			float fTemp276 = (fConst128 * fRec564[1]);
			float fTemp277 = (fConst129 * fRec567[1]);
			fRec569[0] = (fTemp275 + (fTemp276 + (fRec569[1] + fTemp277)));
			fRec567[0] = fRec569[0];
			float fRec568 = ((fTemp277 + fTemp276) + fTemp275);
			fRec566[0] = (fRec567[0] + fRec566[1]);
			fRec564[0] = fRec566[0];
			float fRec565 = fRec568;
			fRec563[0] = (fTemp274 + (fRec565 + fRec563[1]));
			fRec561[0] = fRec563[0];
			float fRec562 = (fRec565 + fTemp274);
			float fTemp278 = (fConst138 * ((0.0151734343f * fTemp49) - ((0.0342850015f * fTemp51) + (0.0548170209f * fTemp50))));
			float fTemp279 = (fConst139 * fRec570[1]);
			fRec572[0] = (fTemp278 + (fRec572[1] + fTemp279));
			fRec570[0] = fRec572[0];
			float fRec571 = (fTemp279 + fTemp278);
			float fTemp280 = (fConst141 * (((0.0562758073f * fTemp54) + (0.027918499f * fTemp57)) - (((0.0168836657f * fTemp58) + (0.0317739099f * fTemp55)) + (0.0239986237f * fTemp56))));
			float fTemp281 = (fConst142 * fRec573[1]);
			float fTemp282 = (fConst143 * fRec576[1]);
			fRec578[0] = (fTemp280 + (fTemp281 + (fRec578[1] + fTemp282)));
			fRec576[0] = fRec578[0];
			float fRec577 = ((fTemp282 + fTemp281) + fTemp280);
			fRec575[0] = (fRec576[0] + fRec575[1]);
			fRec573[0] = fRec575[0];
			float fRec574 = fRec577;
			fVec11[(IOTA & 1023)] = ((0.0406197943f * fTemp62) + (fRec535 + (fRec550 + (fRec562 + (fRec571 + fRec574)))));
			output11[i] = FAUSTFLOAT((0.880171478f * (fRec0[0] * fVec11[((IOTA - iConst153) & 1023)])));
			float fTemp283 = (fConst145 * fRec579[1]);
			float fTemp284 = (fConst147 * fRec582[1]);
			float fTemp285 = (fConst148 * fRec585[1]);
			float fTemp286 = (fConst150 * ((((0.0183232948f * fTemp10) + (0.0144627606f * fTemp14)) + (0.0109095126f * fTemp16)) - ((0.0261196587f * fTemp17) + ((0.00790776592f * fTemp20) + ((((((0.0188367162f * fTemp18) + (0.00479551079f * fTemp11)) + (0.0175811481f * fTemp12)) + (0.0100067295f * fTemp13)) + (0.00413021492f * fTemp15)) + (0.0146678817f * fTemp19))))));
			float fTemp287 = (fConst151 * fRec588[1]);
			float fTemp288 = (fConst152 * fRec591[1]);
			fRec593[0] = (fTemp286 + (fTemp287 + (fRec593[1] + fTemp288)));
			fRec591[0] = fRec593[0];
			float fRec592 = ((fTemp288 + fTemp287) + fTemp286);
			fRec590[0] = (fRec591[0] + fRec590[1]);
			fRec588[0] = fRec590[0];
			float fRec589 = fRec592;
			fRec587[0] = (fTemp284 + (fTemp285 + (fRec589 + fRec587[1])));
			fRec585[0] = fRec587[0];
			float fRec586 = (fTemp284 + (fRec589 + fTemp285));
			fRec584[0] = (fRec585[0] + fRec584[1]);
			fRec582[0] = fRec584[0];
			float fRec583 = fRec586;
			fRec581[0] = (fTemp283 + (fRec583 + fRec581[1]));
			fRec579[0] = fRec581[0];
			float fRec580 = (fRec583 + fTemp283);
			float fTemp289 = (fConst131 * fRec594[1]);
			float fTemp290 = (fConst132 * fRec597[1]);
			float fTemp291 = (fConst134 * (((0.0211944077f * fTemp33) + ((0.0138329612f * fTemp29) + ((0.0106544159f * fTemp28) + (((0.0020755243f * fTemp31) + (0.0194715746f * fTemp32)) + (0.0126680378f * fTemp27))))) - (((0.0371806696f * fTemp30) + (0.0160361975f * fTemp26)) + (0.00703999633f * fTemp34))));
			float fTemp292 = (fConst135 * fRec600[1]);
			float fTemp293 = (fConst136 * fRec603[1]);
			fRec605[0] = (fTemp291 + (fTemp292 + (fRec605[1] + fTemp293)));
			fRec603[0] = fRec605[0];
			float fRec604 = ((fTemp293 + fTemp292) + fTemp291);
			fRec602[0] = (fRec603[0] + fRec602[1]);
			fRec600[0] = fRec602[0];
			float fRec601 = fRec604;
			fRec599[0] = (fTemp289 + (fTemp290 + (fRec601 + fRec599[1])));
			fRec597[0] = fRec599[0];
			float fRec598 = (fTemp289 + (fRec601 + fTemp290));
			fRec596[0] = (fRec597[0] + fRec596[1]);
			fRec594[0] = fRec596[0];
			float fRec595 = fRec598;
			float fTemp294 = (fConst125 * fRec606[1]);
			float fTemp295 = (fConst127 * ((((0.0103097195f * fTemp43) + (((0.016962165f * fTemp38) + (0.0246620625f * fTemp42)) + (0.011517141f * fTemp40))) + (0.0411229059f * fTemp44)) - ((0.0151936384f * fTemp39) + (0.0113901151f * fTemp41))));
			float fTemp296 = (fConst128 * fRec609[1]);
			float fTemp297 = (fConst129 * fRec612[1]);
			fRec614[0] = (fTemp295 + (fTemp296 + (fRec614[1] + fTemp297)));
			fRec612[0] = fRec614[0];
			float fRec613 = ((fTemp297 + fTemp296) + fTemp295);
			fRec611[0] = (fRec612[0] + fRec611[1]);
			fRec609[0] = fRec611[0];
			float fRec610 = fRec613;
			fRec608[0] = (fTemp294 + (fRec610 + fRec608[1]));
			fRec606[0] = fRec608[0];
			float fRec607 = (fRec610 + fTemp294);
			float fTemp298 = (fConst138 * ((0.00858379155f * fTemp49) - ((0.0396553129f * fTemp51) + (0.0188880209f * fTemp50))));
			float fTemp299 = (fConst139 * fRec615[1]);
			fRec617[0] = (fTemp298 + (fRec617[1] + fTemp299));
			fRec615[0] = fRec617[0];
			float fRec616 = (fTemp299 + fTemp298);
			float fTemp300 = (fConst141 * ((0.0346854255f * fTemp54) - ((((0.0154645126f * fTemp58) + (0.0236423165f * fTemp55)) + (0.00820955727f * fTemp56)) + (0.028282484f * fTemp57))));
			float fTemp301 = (fConst142 * fRec618[1]);
			float fTemp302 = (fConst143 * fRec621[1]);
			fRec623[0] = (fTemp300 + (fTemp301 + (fRec623[1] + fTemp302)));
			fRec621[0] = fRec623[0];
			float fRec622 = ((fTemp302 + fTemp301) + fTemp300);
			fRec620[0] = (fRec621[0] + fRec620[1]);
			fRec618[0] = fRec620[0];
			float fRec619 = fRec622;
			fVec12[(IOTA & 1023)] = ((0.0268279463f * fTemp62) + (fRec580 + (fRec595 + (fRec607 + (fRec616 + fRec619)))));
			output12[i] = FAUSTFLOAT((0.880171478f * (fRec0[0] * fVec12[((IOTA - iConst153) & 1023)])));
			float fTemp303 = (fConst120 * (((0.003692267f * fTemp17) + (((0.024843825f * fTemp16) + ((0.0160765797f * fTemp14) + (((0.00736143766f * fTemp18) + (0.00135160645f * fTemp12)) + (0.00718600722f * fTemp13)))) + (0.026397327f * fTemp20))) - ((((0.0274045374f * fTemp10) + (0.00237601413f * fTemp11)) + (0.00228795945f * fTemp15)) + (0.00516048633f * fTemp19))));
			float fTemp304 = (fConst122 * fRec636[1]);
			float fTemp305 = (fConst121 * fRec633[1]);
			fRec638[0] = (((fTemp303 + fRec638[1]) + fTemp304) + fTemp305);
			fRec636[0] = fRec638[0];
			float fRec637 = ((fTemp303 + fTemp304) + fTemp305);
			fRec635[0] = (fRec636[0] + fRec635[1]);
			fRec633[0] = fRec635[0];
			float fRec634 = fRec637;
			float fTemp306 = (fConst118 * fRec630[1]);
			float fTemp307 = (fConst117 * fRec627[1]);
			fRec632[0] = (((fRec634 + fRec632[1]) + fTemp306) + fTemp307);
			fRec630[0] = fRec632[0];
			float fRec631 = ((fRec634 + fTemp306) + fTemp307);
			fRec629[0] = (fRec630[0] + fRec629[1]);
			fRec627[0] = fRec629[0];
			float fRec628 = fRec631;
			float fTemp308 = (fConst115 * fRec624[1]);
			fRec626[0] = ((fRec628 + fRec626[1]) + fTemp308);
			fRec624[0] = fRec626[0];
			float fRec625 = (fRec628 + fTemp308);
			float fTemp309 = (fConst108 * fRec639[1]);
			float fTemp310 = (fConst109 * fRec642[1]);
			float fTemp311 = (fConst111 * (((((((0.00449318439f * fTemp30) + (0.0308052748f * fTemp31)) + (0.0306430627f * fTemp32)) + (0.000357802608f * fTemp27)) + (0.0071184542f * fTemp29)) + (0.0333983786f * fTemp34)) - (((0.00295860111f * fTemp26) + (0.00157203316f * fTemp28)) + (0.00701842271f * fTemp33))));
			float fTemp312 = (fConst112 * fRec645[1]);
			float fTemp313 = (fConst113 * fRec648[1]);
			fRec650[0] = (fTemp311 + (fTemp312 + (fRec650[1] + fTemp313)));
			fRec648[0] = fRec650[0];
			float fRec649 = ((fTemp313 + fTemp312) + fTemp311);
			fRec647[0] = (fRec648[0] + fRec647[1]);
			fRec645[0] = fRec647[0];
			float fRec646 = fRec649;
			fRec644[0] = (fTemp309 + (fTemp310 + (fRec646 + fRec644[1])));
			fRec642[0] = fRec644[0];
			float fRec643 = (fTemp309 + (fRec646 + fTemp310));
			fRec641[0] = (fRec642[0] + fRec641[1]);
			fRec639[0] = fRec641[0];
			float fRec640 = fRec643;
			float fTemp314 = (fConst102 * fRec651[1]);
			float fTemp315 = (fConst104 * (((0.0389659628f * fTemp43) + ((0.0151934419f * fTemp42) + (0.000778859772f * fTemp40))) - ((0.00466579571f * fTemp44) + (((0.00531259878f * fTemp38) + (0.0237730574f * fTemp39)) + (0.0325311758f * fTemp41)))));
			float fTemp316 = (fConst105 * fRec654[1]);
			float fTemp317 = (fConst106 * fRec657[1]);
			fRec659[0] = (fTemp315 + (fTemp316 + (fRec659[1] + fTemp317)));
			fRec657[0] = fRec659[0];
			float fRec658 = ((fTemp317 + fTemp316) + fTemp315);
			fRec656[0] = (fRec657[0] + fRec656[1]);
			fRec654[0] = fRec656[0];
			float fRec655 = fRec658;
			fRec653[0] = (fTemp314 + (fRec655 + fRec653[1]));
			fRec651[0] = fRec653[0];
			float fRec652 = (fRec655 + fTemp314);
			float fTemp318 = (fConst95 * (((0.0155842332f * fTemp49) + (0.00227896054f * fTemp50)) - (0.044411663f * fTemp51)));
			float fTemp319 = (fConst96 * fRec660[1]);
			fRec662[0] = (fTemp318 + (fRec662[1] + fTemp319));
			fRec660[0] = fRec662[0];
			float fRec661 = (fTemp319 + fTemp318);
			float fTemp320 = (fConst98 * ((0.0026237031f * fTemp56) - ((((0.00394974416f * fTemp54) + (0.0301061943f * fTemp58)) + (0.0187329222f * fTemp55)) + (0.0431233831f * fTemp57))));
			float fTemp321 = (fConst99 * fRec663[1]);
			float fTemp322 = (fConst100 * fRec666[1]);
			fRec668[0] = (fTemp320 + (fTemp321 + (fRec668[1] + fTemp322)));
			fRec666[0] = fRec668[0];
			float fRec667 = ((fTemp322 + fTemp321) + fTemp320);
			fRec665[0] = (fRec666[0] + fRec665[1]);
			fRec663[0] = fRec665[0];
			float fRec664 = fRec667;
			fVec13[(IOTA & 1023)] = ((fRec625 + (fRec640 + (fRec652 + (fRec661 + fRec664)))) + (0.0283390991f * fTemp62));
			output13[i] = FAUSTFLOAT((0.879635572f * (fRec0[0] * fVec13[((IOTA - iConst123) & 1023)])));
			float fTemp323 = (fConst145 * fRec669[1]);
			float fTemp324 = (fConst147 * fRec672[1]);
			float fTemp325 = (fConst148 * fRec675[1]);
			float fTemp326 = (fConst150 * ((((0.0204474032f * fTemp19) + ((0.0091766445f * fTemp16) + ((0.00850280095f * fTemp15) + ((((0.0283403341f * fTemp10) + (0.0155196441f * fTemp18)) + (0.0176722612f * fTemp12)) + (0.0138718318f * fTemp14))))) + (0.0139592486f * fTemp17)) - (((0.00123196538f * fTemp11) + (0.0158767048f * fTemp13)) + (0.0124037443f * fTemp20))));
			float fTemp327 = (fConst151 * fRec678[1]);
			float fTemp328 = (fConst152 * fRec681[1]);
			fRec683[0] = (fTemp326 + (fTemp327 + (fRec683[1] + fTemp328)));
			fRec681[0] = fRec683[0];
			float fRec682 = ((fTemp328 + fTemp327) + fTemp326);
			fRec680[0] = (fRec681[0] + fRec680[1]);
			fRec678[0] = fRec680[0];
			float fRec679 = fRec682;
			fRec677[0] = (fTemp324 + (fTemp325 + (fRec679 + fRec677[1])));
			fRec675[0] = fRec677[0];
			float fRec676 = (fTemp324 + (fRec679 + fTemp325));
			fRec674[0] = (fRec675[0] + fRec674[1]);
			fRec672[0] = fRec674[0];
			float fRec673 = fRec676;
			fRec671[0] = (fTemp323 + (fRec673 + fRec671[1]));
			fRec669[0] = fRec671[0];
			float fRec670 = (fRec673 + fTemp323);
			float fTemp329 = (fConst131 * fRec684[1]);
			float fTemp330 = (fConst132 * fRec687[1]);
			float fTemp331 = (fConst134 * ((((((0.0348084718f * fTemp30) + (0.0242478326f * fTemp26)) + (0.0182906222f * fTemp32)) + (0.0184442848f * fTemp27)) + (0.0145793809f * fTemp29)) - ((0.0224309266f * fTemp34) + (((0.00235898187f * fTemp31) + (0.0112693775f * fTemp28)) + (0.0205992814f * fTemp33)))));
			float fTemp332 = (fConst135 * fRec690[1]);
			float fTemp333 = (fConst136 * fRec693[1]);
			fRec695[0] = (fTemp331 + (fTemp332 + (fRec695[1] + fTemp333)));
			fRec693[0] = fRec695[0];
			float fRec694 = ((fTemp333 + fTemp332) + fTemp331);
			fRec692[0] = (fRec693[0] + fRec692[1]);
			fRec690[0] = fRec692[0];
			float fRec691 = fRec694;
			fRec689[0] = (fTemp329 + (fTemp330 + (fRec691 + fRec689[1])));
			fRec687[0] = fRec689[0];
			float fRec688 = (fTemp329 + (fRec691 + fTemp330));
			fRec686[0] = (fRec687[0] + fRec686[1]);
			fRec684[0] = fRec686[0];
			float fRec685 = fRec688;
			float fTemp334 = (fConst125 * fRec696[1]);
			float fTemp335 = (fConst127 * ((0.0309881084f * fTemp42) - ((0.0493036173f * fTemp44) + (((0.0017651082f * fTemp43) + (((0.0179697257f * fTemp38) + (0.0147721153f * fTemp39)) + (0.0178599581f * fTemp40))) + (0.00858830567f * fTemp41)))));
			float fTemp336 = (fConst128 * fRec699[1]);
			float fTemp337 = (fConst129 * fRec702[1]);
			fRec704[0] = (fTemp335 + (fTemp336 + (fRec704[1] + fTemp337)));
			fRec702[0] = fRec704[0];
			float fRec703 = ((fTemp337 + fTemp336) + fTemp335);
			fRec701[0] = (fRec702[0] + fRec701[1]);
			fRec699[0] = fRec701[0];
			float fRec700 = fRec703;
			fRec698[0] = (fTemp334 + (fRec700 + fRec698[1]));
			fRec696[0] = fRec698[0];
			float fRec697 = (fRec700 + fTemp334);
			float fTemp338 = (fConst138 * (((0.00838882104f * fTemp49) + (0.0275408067f * fTemp50)) - (0.0463334993f * fTemp51)));
			float fTemp339 = (fConst139 * fRec705[1]);
			fRec707[0] = (fTemp338 + (fRec707[1] + fTemp339));
			fRec705[0] = fRec707[0];
			float fRec706 = (fTemp339 + fTemp338);
			float fTemp340 = (fConst141 * ((0.0091328593f * fTemp56) - ((((0.0473960564f * fTemp54) + (0.0144431572f * fTemp58)) + (0.0301579721f * fTemp55)) + (0.0258537512f * fTemp57))));
			float fTemp341 = (fConst142 * fRec708[1]);
			float fTemp342 = (fConst143 * fRec711[1]);
			fRec713[0] = (fTemp340 + (fTemp341 + (fRec713[1] + fTemp342)));
			fRec711[0] = fRec713[0];
			float fRec712 = ((fTemp342 + fTemp341) + fTemp340);
			fRec710[0] = (fRec711[0] + fRec710[1]);
			fRec708[0] = fRec710[0];
			float fRec709 = fRec712;
			fVec14[(IOTA & 1023)] = ((0.0329747647f * fTemp62) + (fRec670 + (fRec685 + (fRec697 + (fRec706 + fRec709)))));
			output14[i] = FAUSTFLOAT((0.880171478f * (fRec0[0] * fVec14[((IOTA - iConst153) & 1023)])));
			float fTemp343 = (fConst145 * fRec714[1]);
			float fTemp344 = (fConst147 * fRec717[1]);
			float fTemp345 = (fConst148 * fRec720[1]);
			float fTemp346 = (fConst150 * (((0.0105550205f * fTemp15) + (((0.0146479765f * fTemp11) + (0.0184895471f * fTemp12)) + (0.0144746369f * fTemp14))) - ((0.0204125606f * fTemp17) + ((0.0130993472f * fTemp20) + ((0.00726356776f * fTemp19) + ((((0.0224986207f * fTemp10) + (0.0158632826f * fTemp18)) + (0.00311934459f * fTemp13)) + (0.00937628094f * fTemp16)))))));
			float fTemp347 = (fConst151 * fRec723[1]);
			float fTemp348 = (fConst152 * fRec726[1]);
			fRec728[0] = (fTemp346 + (fTemp347 + (fRec728[1] + fTemp348)));
			fRec726[0] = fRec728[0];
			float fRec727 = ((fTemp348 + fTemp347) + fTemp346);
			fRec725[0] = (fRec726[0] + fRec725[1]);
			fRec723[0] = fRec725[0];
			float fRec724 = fRec727;
			fRec722[0] = (fTemp344 + (fTemp345 + (fRec724 + fRec722[1])));
			fRec720[0] = fRec722[0];
			float fRec721 = (fTemp344 + (fRec724 + fTemp345));
			fRec719[0] = (fRec720[0] + fRec719[1]);
			fRec717[0] = fRec719[0];
			float fRec718 = fRec721;
			fRec716[0] = (fTemp343 + (fRec718 + fRec716[1]));
			fRec714[0] = fRec716[0];
			float fRec715 = (fRec718 + fTemp343);
			float fTemp349 = (fConst131 * fRec729[1]);
			float fTemp350 = (fConst132 * fRec732[1]);
			float fTemp351 = (fConst134 * ((((0.015519537f * fTemp26) + (0.0117375012f * fTemp32)) + (0.0126517713f * fTemp27)) - ((0.0105577465f * fTemp34) + ((0.00265621161f * fTemp33) + ((0.0146973412f * fTemp29) + (((0.0351607874f * fTemp30) + (0.0212275907f * fTemp31)) + (0.0188851655f * fTemp28)))))));
			float fTemp352 = (fConst135 * fRec735[1]);
			float fTemp353 = (fConst136 * fRec738[1]);
			fRec740[0] = (fTemp351 + (fTemp352 + (fRec740[1] + fTemp353)));
			fRec738[0] = fRec740[0];
			float fRec739 = ((fTemp353 + fTemp352) + fTemp351);
			fRec737[0] = (fRec738[0] + fRec737[1]);
			fRec735[0] = fRec737[0];
			float fRec736 = fRec739;
			fRec734[0] = (fTemp349 + (fTemp350 + (fRec736 + fRec734[1])));
			fRec732[0] = fRec734[0];
			float fRec733 = (fTemp349 + (fRec736 + fTemp350));
			fRec731[0] = (fRec732[0] + fRec731[1]);
			fRec729[0] = fRec731[0];
			float fRec730 = fRec733;
			float fTemp354 = (fConst125 * fRec741[1]);
			float fTemp355 = (fConst127 * (((0.00677051628f * fTemp44) + ((0.0114678685f * fTemp42) + (0.0087232003f * fTemp41))) - ((0.041143775f * fTemp43) + (((0.0185491145f * fTemp38) + (0.0152238971f * fTemp39)) + (0.0246859211f * fTemp40)))));
			float fTemp356 = (fConst128 * fRec744[1]);
			float fTemp357 = (fConst129 * fRec747[1]);
			fRec749[0] = (fTemp355 + (fTemp356 + (fRec749[1] + fTemp357)));
			fRec747[0] = fRec749[0];
			float fRec748 = ((fTemp357 + fTemp356) + fTemp355);
			fRec746[0] = (fRec747[0] + fRec746[1]);
			fRec744[0] = fRec746[0];
			float fRec745 = fRec748;
			fRec743[0] = (fTemp354 + (fRec745 + fRec743[1]));
			fRec741[0] = fRec743[0];
			float fRec742 = (fRec745 + fTemp354);
			float fTemp358 = (fConst138 * (((0.00860586017f * fTemp49) + (0.0390018113f * fTemp50)) - (0.0200940184f * fTemp51)));
			float fTemp359 = (fConst139 * fRec750[1]);
			fRec752[0] = (fTemp358 + (fRec752[1] + fTemp359));
			fRec750[0] = fRec752[0];
			float fRec751 = (fTemp359 + fTemp358);
			float fTemp360 = (fConst141 * (((0.0148103526f * fTemp56) + (0.02602759f * fTemp57)) - (((0.0360228419f * fTemp54) + (0.00942459423f * fTemp58)) + (0.0236514919f * fTemp55))));
			float fTemp361 = (fConst142 * fRec753[1]);
			float fTemp362 = (fConst143 * fRec756[1]);
			fRec758[0] = (fTemp360 + (fTemp361 + (fRec758[1] + fTemp362)));
			fRec756[0] = fRec758[0];
			float fRec757 = ((fTemp362 + fTemp361) + fTemp360);
			fRec755[0] = (fRec756[0] + fRec755[1]);
			fRec753[0] = fRec755[0];
			float fRec754 = fRec757;
			fVec15[(IOTA & 1023)] = ((0.0268536117f * fTemp62) + (fRec715 + (fRec730 + (fRec742 + (fRec751 + fRec754)))));
			output15[i] = FAUSTFLOAT((0.880171478f * (fRec0[0] * fVec15[((IOTA - iConst153) & 1023)])));
			float fTemp363 = (fConst115 * fRec759[1]);
			float fTemp364 = (fConst117 * fRec762[1]);
			float fTemp365 = (fConst118 * fRec765[1]);
			float fTemp366 = (fConst120 * (((0.0274141226f * fTemp17) + (((0.0160834305f * fTemp14) + ((0.00135664304f * fTemp12) + (0.00227980269f * fTemp13))) + (0.0264118165f * fTemp20))) - ((0.00235729548f * fTemp19) + ((0.0248457957f * fTemp16) + ((((0.00370446756f * fTemp10) + (0.00736640207f * fTemp18)) + (0.00514637865f * fTemp11)) + (0.00719437189f * fTemp15))))));
			float fTemp367 = (fConst121 * fRec768[1]);
			float fTemp368 = (fConst122 * fRec771[1]);
			fRec773[0] = (fTemp366 + (fTemp367 + (fRec773[1] + fTemp368)));
			fRec771[0] = fRec773[0];
			float fRec772 = ((fTemp368 + fTemp367) + fTemp366);
			fRec770[0] = (fRec771[0] + fRec770[1]);
			fRec768[0] = fRec770[0];
			float fRec769 = fRec772;
			fRec767[0] = (fTemp364 + (fTemp365 + (fRec769 + fRec767[1])));
			fRec765[0] = fRec767[0];
			float fRec766 = (fTemp364 + (fRec769 + fTemp365));
			fRec764[0] = (fRec765[0] + fRec764[1]);
			fRec762[0] = fRec764[0];
			float fRec763 = fRec766;
			fRec761[0] = (fTemp363 + (fRec763 + fRec761[1]));
			fRec759[0] = fRec761[0];
			float fRec760 = (fRec763 + fTemp363);
			float fTemp369 = (fConst108 * fRec774[1]);
			float fTemp370 = (fConst109 * fRec777[1]);
			float fTemp371 = (fConst111 * (((0.0334027894f * fTemp34) + (((0.00157177215f * fTemp32) + (0.000356734701f * fTemp27)) + (0.0308132302f * fTemp33))) - ((0.00710847788f * fTemp29) + ((((0.00450092042f * fTemp30) + (0.00701285852f * fTemp31)) + (0.0029441677f * fTemp26)) + (0.030642027f * fTemp28)))));
			float fTemp372 = (fConst112 * fRec780[1]);
			float fTemp373 = (fConst113 * fRec783[1]);
			fRec785[0] = (fTemp371 + (fTemp372 + (fRec785[1] + fTemp373)));
			fRec783[0] = fRec785[0];
			float fRec784 = ((fTemp373 + fTemp372) + fTemp371);
			fRec782[0] = (fRec783[0] + fRec782[1]);
			fRec780[0] = fRec782[0];
			float fRec781 = fRec784;
			fRec779[0] = (fTemp369 + (fTemp370 + (fRec781 + fRec779[1])));
			fRec777[0] = fRec779[0];
			float fRec778 = (fTemp369 + (fRec781 + fTemp370));
			fRec776[0] = (fRec777[0] + fRec776[1]);
			fRec774[0] = fRec776[0];
			float fRec775 = fRec778;
			float fTemp374 = (fConst102 * fRec786[1]);
			float fTemp375 = (fConst104 * (((0.0325279534f * fTemp41) + (0.0389627181f * fTemp44)) - ((0.00466731703f * fTemp43) + ((((0.00530193001f * fTemp38) + (0.00077174278f * fTemp42)) + (0.0237659756f * fTemp39)) + (0.0151881436f * fTemp40)))));
			float fTemp376 = (fConst105 * fRec789[1]);
			float fTemp377 = (fConst106 * fRec792[1]);
			fRec794[0] = (fTemp375 + (fTemp376 + (fRec794[1] + fTemp377)));
			fRec792[0] = fRec794[0];
			float fRec793 = ((fTemp377 + fTemp376) + fTemp375);
			fRec791[0] = (fRec792[0] + fRec791[1]);
			fRec789[0] = fRec791[0];
			float fRec790 = fRec793;
			fRec788[0] = (fTemp374 + (fRec790 + fRec788[1]));
			fRec786[0] = fRec788[0];
			float fRec787 = (fRec790 + fTemp374);
			float fTemp378 = (fConst95 * (((0.015574404f * fTemp49) + (0.0443907827f * fTemp50)) - (0.0022749207f * fTemp51)));
			float fTemp379 = (fConst96 * fRec795[1]);
			fRec797[0] = (fTemp378 + (fRec797[1] + fTemp379));
			fRec795[0] = fRec797[0];
			float fRec796 = (fTemp379 + fTemp378);
			float fTemp380 = (fConst98 * (((0.030093208f * fTemp56) + (0.0431107245f * fTemp57)) - (((0.00394637231f * fTemp54) + (0.00261598756f * fTemp58)) + (0.018725859f * fTemp55))));
			float fTemp381 = (fConst99 * fRec798[1]);
			float fTemp382 = (fConst100 * fRec801[1]);
			fRec803[0] = (fTemp380 + (fTemp381 + (fRec803[1] + fTemp382)));
			fRec801[0] = fRec803[0];
			float fRec802 = ((fTemp382 + fTemp381) + fTemp380);
			fRec800[0] = (fRec801[0] + fRec800[1]);
			fRec798[0] = fRec800[0];
			float fRec799 = fRec802;
			fVec16[(IOTA & 1023)] = ((0.0283231307f * fTemp62) + (fRec760 + (fRec775 + (fRec787 + (fRec796 + fRec799)))));
			output16[i] = FAUSTFLOAT((0.879635572f * (fRec0[0] * fVec16[((IOTA - iConst123) & 1023)])));
			float fTemp383 = (fConst145 * fRec804[1]);
			float fTemp384 = (fConst147 * fRec807[1]);
			float fTemp385 = (fConst148 * fRec810[1]);
			float fTemp386 = (fConst150 * (((0.0075558261f * fTemp15) + ((0.0149142258f * fTemp14) + (((0.0130915558f * fTemp10) + (0.0180670153f * fTemp18)) + (0.00172562525f * fTemp13)))) - ((0.0300634149f * fTemp17) + ((0.0189329498f * fTemp20) + ((0.00514338585f * fTemp19) + (((0.0139677459f * fTemp11) + (0.0208352227f * fTemp12)) + (0.010448914f * fTemp16)))))));
			float fTemp387 = (fConst151 * fRec813[1]);
			float fTemp388 = (fConst152 * fRec816[1]);
			fRec818[0] = (fTemp386 + (fTemp387 + (fRec818[1] + fTemp388)));
			fRec816[0] = fRec818[0];
			float fRec817 = ((fTemp388 + fTemp387) + fTemp386);
			fRec815[0] = (fRec816[0] + fRec815[1]);
			fRec813[0] = fRec815[0];
			float fRec814 = fRec817;
			fRec812[0] = (fTemp384 + (fTemp385 + (fRec814 + fRec812[1])));
			fRec810[0] = fRec812[0];
			float fRec811 = (fTemp384 + (fRec814 + fTemp385));
			fRec809[0] = (fRec810[0] + fRec809[1]);
			fRec807[0] = fRec809[0];
			float fRec808 = fRec811;
			fRec806[0] = (fTemp383 + (fRec808 + fRec806[1]));
			fRec804[0] = fRec806[0];
			float fRec805 = (fRec808 + fTemp383);
			float fTemp389 = (fConst131 * fRec819[1]);
			float fTemp390 = (fConst132 * fRec822[1]);
			float fTemp391 = (fConst134 * ((((0.0364312865f * fTemp30) + (0.0295793712f * fTemp31)) + (0.0119465757f * fTemp27)) - ((0.0262278654f * fTemp34) + ((0.00701551232f * fTemp33) + ((0.0136085004f * fTemp29) + (((0.0176875629f * fTemp26) + (0.0159679148f * fTemp32)) + (0.0245016254f * fTemp28)))))));
			float fTemp392 = (fConst135 * fRec825[1]);
			float fTemp393 = (fConst136 * fRec828[1]);
			fRec830[0] = (fTemp391 + (fTemp392 + (fRec830[1] + fTemp393)));
			fRec828[0] = fRec830[0];
			float fRec829 = ((fTemp393 + fTemp392) + fTemp391);
			fRec827[0] = (fRec828[0] + fRec827[1]);
			fRec825[0] = fRec827[0];
			float fRec826 = fRec829;
			fRec824[0] = (fTemp389 + (fTemp390 + (fRec826 + fRec824[1])));
			fRec822[0] = fRec824[0];
			float fRec823 = (fTemp389 + (fRec826 + fTemp390));
			fRec821[0] = (fRec822[0] + fRec821[1]);
			fRec819[0] = fRec821[0];
			float fRec820 = fRec823;
			float fTemp394 = (fConst125 * fRec831[1]);
			float fTemp395 = (fConst127 * ((((0.0553068481f * fTemp43) + (0.0298772734f * fTemp38)) + (0.0111225173f * fTemp41)) - ((((0.0155937243f * fTemp42) + (0.0229095872f * fTemp39)) + (0.0285480842f * fTemp40)) + (0.00467078993f * fTemp44))));
			float fTemp396 = (fConst128 * fRec834[1]);
			float fTemp397 = (fConst129 * fRec837[1]);
			fRec839[0] = (fTemp395 + (fTemp396 + (fRec839[1] + fTemp397)));
			fRec837[0] = fRec839[0];
			float fRec838 = ((fTemp397 + fTemp396) + fTemp395);
			fRec836[0] = (fRec837[0] + fRec836[1]);
			fRec834[0] = fRec836[0];
			float fRec835 = fRec838;
			fRec833[0] = (fTemp394 + (fRec835 + fRec833[1]));
			fRec831[0] = fRec833[0];
			float fRec832 = (fRec835 + fTemp394);
			float fTemp398 = (fConst138 * (((0.0342726484f * fTemp51) + (0.01516994f * fTemp49)) + (0.0548043214f * fTemp50)));
			float fTemp399 = (fConst139 * fRec840[1]);
			fRec842[0] = (fTemp398 + (fRec842[1] + fTemp399));
			fRec840[0] = fRec842[0];
			float fRec841 = (fTemp399 + fTemp398);
			float fTemp400 = (fConst141 * (((((0.0562616065f * fTemp54) + (0.0168774109f * fTemp58)) + (0.0239955988f * fTemp56)) + (0.0279191658f * fTemp57)) - (0.0317676514f * fTemp55)));
			float fTemp401 = (fConst142 * fRec843[1]);
			float fTemp402 = (fConst143 * fRec846[1]);
			fRec848[0] = (fTemp400 + (fTemp401 + (fRec848[1] + fTemp402)));
			fRec846[0] = fRec848[0];
			float fRec847 = ((fTemp402 + fTemp401) + fTemp400);
			fRec845[0] = (fRec846[0] + fRec845[1]);
			fRec843[0] = fRec845[0];
			float fRec844 = fRec847;
			fVec17[(IOTA & 1023)] = ((0.0406075418f * fTemp62) + (fRec805 + (fRec820 + (fRec832 + (fRec841 + fRec844)))));
			output17[i] = FAUSTFLOAT((0.880171478f * (fRec0[0] * fVec17[((IOTA - iConst153) & 1023)])));
			float fTemp403 = (fConst145 * fRec849[1]);
			float fTemp404 = (fConst147 * fRec852[1]);
			float fTemp405 = (fConst148 * fRec855[1]);
			float fTemp406 = (fConst150 * ((((0.0146581326f * fTemp19) + ((0.0109113045f * fTemp16) + ((0.00412313873f * fTemp15) + ((0.0144685758f * fTemp14) + ((0.00477765966f * fTemp11) + (0.00998146273f * fTemp13)))))) + (0.0261053089f * fTemp17)) - ((((0.018346563f * fTemp10) + (0.0188388396f * fTemp18)) + (0.0175905656f * fTemp12)) + (0.00791089982f * fTemp20))));
			float fTemp407 = (fConst151 * fRec858[1]);
			float fTemp408 = (fConst152 * fRec861[1]);
			fRec863[0] = (fTemp406 + (fTemp407 + (fRec863[1] + fTemp408)));
			fRec861[0] = fRec863[0];
			float fRec862 = ((fTemp408 + fTemp407) + fTemp406);
			fRec860[0] = (fRec861[0] + fRec860[1]);
			fRec858[0] = fRec860[0];
			float fRec859 = fRec862;
			fRec857[0] = (fTemp404 + (fTemp405 + (fRec859 + fRec857[1])));
			fRec855[0] = fRec857[0];
			float fRec856 = (fTemp404 + (fRec859 + fTemp405));
			fRec854[0] = (fRec855[0] + fRec854[1]);
			fRec852[0] = fRec854[0];
			float fRec853 = fRec856;
			fRec851[0] = (fTemp403 + (fRec853 + fRec851[1]));
			fRec849[0] = fRec851[0];
			float fRec850 = (fRec853 + fTemp403);
			float fTemp409 = (fConst131 * fRec864[1]);
			float fTemp410 = (fConst132 * fRec867[1]);
			float fTemp411 = (fConst134 * (((0.0126534691f * fTemp27) + (0.0138148451f * fTemp29)) - ((0.00707045803f * fTemp34) + ((((((0.0371824242f * fTemp30) + (0.00207667262f * fTemp31)) + (0.0160341635f * fTemp26)) + (0.0194886308f * fTemp32)) + (0.0106647294f * fTemp28)) + (0.0212102067f * fTemp33)))));
			float fTemp412 = (fConst135 * fRec870[1]);
			float fTemp413 = (fConst136 * fRec873[1]);
			fRec875[0] = (fTemp411 + (fTemp412 + (fRec875[1] + fTemp413)));
			fRec873[0] = fRec875[0];
			float fRec874 = ((fTemp413 + fTemp412) + fTemp411);
			fRec872[0] = (fRec873[0] + fRec872[1]);
			fRec870[0] = fRec872[0];
			float fRec871 = fRec874;
			fRec869[0] = (fTemp409 + (fTemp410 + (fRec871 + fRec869[1])));
			fRec867[0] = fRec869[0];
			float fRec868 = (fTemp409 + (fRec871 + fTemp410));
			fRec866[0] = (fRec867[0] + fRec866[1]);
			fRec864[0] = fRec866[0];
			float fRec865 = fRec868;
			float fTemp414 = (fConst125 * fRec876[1]);
			float fTemp415 = (fConst127 * ((0.0169819035f * fTemp38) - ((0.0411421359f * fTemp44) + (((0.0102870315f * fTemp43) + (((0.0246529412f * fTemp42) + (0.0152127426f * fTemp39)) + (0.0115211168f * fTemp40))) + (0.0114039099f * fTemp41)))));
			float fTemp416 = (fConst128 * fRec879[1]);
			float fTemp417 = (fConst129 * fRec882[1]);
			fRec884[0] = (fTemp415 + (fTemp416 + (fRec884[1] + fTemp417)));
			fRec882[0] = fRec884[0];
			float fRec883 = ((fTemp417 + fTemp416) + fTemp415);
			fRec881[0] = (fRec882[0] + fRec881[1]);
			fRec879[0] = fRec881[0];
			float fRec880 = fRec883;
			fRec878[0] = (fTemp414 + (fRec880 + fRec878[1]));
			fRec876[0] = fRec878[0];
			float fRec877 = (fRec880 + fTemp414);
			float fTemp418 = (fConst138 * (((0.0396705791f * fTemp51) + (0.00859803706f * fTemp49)) + (0.018905893f * fTemp50)));
			float fTemp419 = (fConst139 * fRec885[1]);
			fRec887[0] = (fTemp418 + (fRec887[1] + fTemp419));
			fRec885[0] = fRec887[0];
			float fRec886 = (fTemp419 + fTemp418);
			float fTemp420 = (fConst141 * ((((0.0347117484f * fTemp54) + (0.0154876364f * fTemp58)) + (0.00822132174f * fTemp56)) - ((0.0236457568f * fTemp55) + (0.0282789245f * fTemp57))));
			float fTemp421 = (fConst142 * fRec888[1]);
			float fTemp422 = (fConst143 * fRec891[1]);
			fRec893[0] = (fTemp420 + (fTemp421 + (fRec893[1] + fTemp422)));
			fRec891[0] = fRec893[0];
			float fRec892 = ((fTemp422 + fTemp421) + fTemp420);
			fRec890[0] = (fRec891[0] + fRec890[1]);
			fRec888[0] = fRec890[0];
			float fRec889 = fRec892;
			fVec18[(IOTA & 1023)] = ((0.026842976f * fTemp62) + (fRec850 + (fRec865 + (fRec877 + (fRec886 + fRec889)))));
			output18[i] = FAUSTFLOAT((0.880171478f * (fRec0[0] * fVec18[((IOTA - iConst153) & 1023)])));
			float fTemp423 = (fConst155 * fRec894[1]);
			float fTemp424 = (fConst156 * fRec897[1]);
			float fTemp425 = (fConst158 * ((((0.000243110801f * fTemp29) + ((4.69099996e-06f * fTemp28) + ((((6.57120017e-06f * fTemp30) + (0.0440332927f * fTemp31)) + (0.0175207537f * fTemp32)) + (0.00406657299f * fTemp27)))) + (0.0481828526f * fTemp34)) - ((8.52379981e-06f * fTemp26) + (1.13229999e-05f * fTemp33))));
			float fTemp426 = (fConst159 * fRec900[1]);
			float fTemp427 = (fConst160 * fRec903[1]);
			fRec905[0] = (fTemp425 + (fTemp426 + (fRec905[1] + fTemp427)));
			fRec903[0] = fRec905[0];
			float fRec904 = ((fTemp427 + fTemp426) + fTemp425);
			fRec902[0] = (fRec903[0] + fRec902[1]);
			fRec900[0] = fRec902[0];
			float fRec901 = fRec904;
			fRec899[0] = (fTemp423 + (fTemp424 + (fRec901 + fRec899[1])));
			fRec897[0] = fRec899[0];
			float fRec898 = (fTemp423 + (fRec901 + fTemp424));
			fRec896[0] = (fRec897[0] + fRec896[1]);
			fRec894[0] = fRec896[0];
			float fRec895 = fRec898;
			float fTemp428 = (fConst162 * fRec906[1]);
			float fTemp429 = (fConst164 * (((8.90140018e-06f * fTemp44) + (((1.15727998e-05f * fTemp38) + (0.0226103123f * fTemp39)) + (0.049382329f * fTemp41))) - ((0.0602857322f * fTemp43) + ((0.0101948977f * fTemp42) + (5.91619983e-06f * fTemp40)))));
			float fTemp430 = (fConst165 * fRec909[1]);
			float fTemp431 = (fConst166 * fRec912[1]);
			fRec914[0] = (fTemp429 + (fTemp430 + (fRec914[1] + fTemp431)));
			fRec912[0] = fRec914[0];
			float fRec913 = ((fTemp431 + fTemp430) + fTemp429);
			fRec911[0] = (fRec912[0] + fRec911[1]);
			fRec909[0] = fRec911[0];
			float fRec910 = fRec913;
			fRec908[0] = (fTemp428 + (fRec910 + fRec908[1]));
			fRec906[0] = fRec908[0];
			float fRec907 = (fRec910 + fTemp428);
			float fTemp432 = (fConst168 * ((8.12150029e-06f * fTemp56) - ((((9.05569959e-06f * fTemp54) + (0.0506631546f * fTemp58)) + (0.0202656481f * fTemp55)) + (0.0699572712f * fTemp57))));
			float fTemp433 = (fConst169 * fRec915[1]);
			float fTemp434 = (fConst170 * fRec918[1]);
			fRec920[0] = (fTemp432 + (fTemp433 + (fRec920[1] + fTemp434)));
			fRec918[0] = fRec920[0];
			float fRec919 = ((fTemp434 + fTemp433) + fTemp432);
			fRec917[0] = (fRec918[0] + fRec917[1]);
			fRec915[0] = fRec917[0];
			float fRec916 = fRec919;
			float fTemp435 = (fConst172 * fRec921[1]);
			float fTemp436 = (fConst174 * fRec924[1]);
			float fTemp437 = (fConst175 * fRec927[1]);
			float fTemp438 = (fConst177 * (((((0.0351304561f * fTemp10) + (7.99369991e-06f * fTemp12)) + (0.00765366526f * fTemp13)) + (7.53009999e-06f * fTemp19)) - ((2.73320006e-06f * fTemp17) + (((0.00744883157f * fTemp16) + ((5.48629987e-06f * fTemp15) + (((8.71649991e-06f * fTemp18) + (0.00521747768f * fTemp11)) + (0.00640302943f * fTemp14)))) + (0.0365226679f * fTemp20)))));
			float fTemp439 = (fConst178 * fRec930[1]);
			float fTemp440 = (fConst179 * fRec933[1]);
			fRec935[0] = (fTemp438 + (fTemp439 + (fRec935[1] + fTemp440)));
			fRec933[0] = fRec935[0];
			float fRec934 = ((fTemp440 + fTemp439) + fTemp438);
			fRec932[0] = (fRec933[0] + fRec932[1]);
			fRec930[0] = fRec932[0];
			float fRec931 = fRec934;
			fRec929[0] = (fTemp436 + (fTemp437 + (fRec931 + fRec929[1])));
			fRec927[0] = fRec929[0];
			float fRec928 = (fTemp436 + (fRec931 + fTemp437));
			fRec926[0] = (fRec927[0] + fRec926[1]);
			fRec924[0] = fRec926[0];
			float fRec925 = fRec928;
			fRec923[0] = (fTemp435 + (fRec925 + fRec923[1]));
			fRec921[0] = fRec923[0];
			float fRec922 = (fRec925 + fTemp435);
			float fTemp441 = (fConst181 * ((0.0753616467f * fTemp51) - ((0.0311328489f * fTemp49) + (6.40090002e-06f * fTemp50))));
			float fTemp442 = (fConst182 * fRec936[1]);
			fRec938[0] = (fTemp441 + (fRec938[1] + fTemp442));
			fRec936[0] = fRec938[0];
			float fRec937 = (fTemp442 + fTemp441);
			fVec19[0] = ((0.0513265356f * fTemp62) + (fRec895 + (fRec907 + (fRec916 + (fRec922 + fRec937)))));
			output19[i] = FAUSTFLOAT((0.999892831f * (fRec0[0] * fVec19[iConst183])));
			float fTemp443 = (fConst185 * fRec939[1]);
			float fTemp444 = (fConst187 * fRec942[1]);
			float fTemp445 = (fConst188 * fRec945[1]);
			float fTemp446 = (fConst190 * (((0.0185346399f * fTemp20) + ((0.00356533285f * fTemp15) + (0.0169491079f * fTemp19))) - ((((((((0.0258758366f * fTemp10) + (0.0320939347f * fTemp18)) + (1.72550006e-06f * fTemp11)) + (0.00654298346f * fTemp12)) + (0.0061741611f * fTemp13)) + (0.00629120832f * fTemp14)) + (0.00376938842f * fTemp16)) + (0.0149365002f * fTemp17))));
			float fTemp447 = (fConst191 * fRec948[1]);
			float fTemp448 = (fConst192 * fRec951[1]);
			fRec953[0] = (fTemp446 + (fTemp447 + (fRec953[1] + fTemp448)));
			fRec951[0] = fRec953[0];
			float fRec952 = ((fTemp448 + fTemp447) + fTemp446);
			fRec950[0] = (fRec951[0] + fRec950[1]);
			fRec948[0] = fRec950[0];
			float fRec949 = fRec952;
			fRec947[0] = (fTemp444 + (fTemp445 + (fRec949 + fRec947[1])));
			fRec945[0] = fRec947[0];
			float fRec946 = (fTemp444 + (fRec949 + fTemp445));
			fRec944[0] = (fRec945[0] + fRec944[1]);
			fRec942[0] = fRec944[0];
			float fRec943 = fRec946;
			fRec941[0] = (fTemp443 + (fRec943 + fRec941[1]));
			fRec939[0] = fRec941[0];
			float fRec940 = (fRec943 + fTemp443);
			float fTemp449 = (fConst194 * fRec954[1]);
			float fTemp450 = (fConst195 * fRec957[1]);
			float fTemp451 = (fConst197 * (((0.031893611f * fTemp30) + (0.015136118f * fTemp32)) - ((0.0184198972f * fTemp34) + ((0.0442743562f * fTemp33) + ((0.00744002871f * fTemp29) + ((((5.05890011e-06f * fTemp31) + (0.0128810871f * fTemp26)) + (0.00764794927f * fTemp27)) + (0.00874249823f * fTemp28)))))));
			float fTemp452 = (fConst198 * fRec960[1]);
			float fTemp453 = (fConst199 * fRec963[1]);
			fRec965[0] = (fTemp451 + (fTemp452 + (fRec965[1] + fTemp453)));
			fRec963[0] = fRec965[0];
			float fRec964 = ((fTemp453 + fTemp452) + fTemp451);
			fRec962[0] = (fRec963[0] + fRec962[1]);
			fRec960[0] = fRec962[0];
			float fRec961 = fRec964;
			fRec959[0] = (fTemp449 + (fTemp450 + (fRec961 + fRec959[1])));
			fRec957[0] = fRec959[0];
			float fRec958 = (fTemp449 + (fRec961 + fTemp450));
			fRec956[0] = (fRec957[0] + fRec956[1]);
			fRec954[0] = fRec956[0];
			float fRec955 = fRec958;
			float fTemp454 = (fConst201 * fRec966[1]);
			float fTemp455 = (fConst203 * (((0.0438517034f * fTemp44) + (((6.24799986e-06f * fTemp43) + (((0.0427996628f * fTemp38) + (0.00624921452f * fTemp42)) + (0.0225724299f * fTemp39))) + (0.0247061756f * fTemp41))) - (0.00360593479f * fTemp40)));
			float fTemp456 = (fConst204 * fRec969[1]);
			float fTemp457 = (fConst205 * fRec972[1]);
			fRec974[0] = (fTemp455 + (fTemp456 + (fRec974[1] + fTemp457)));
			fRec972[0] = fRec974[0];
			float fRec973 = ((fTemp457 + fTemp456) + fTemp455);
			fRec971[0] = (fRec972[0] + fRec971[1]);
			fRec969[0] = fRec971[0];
			float fRec970 = fRec973;
			fRec968[0] = (fTemp454 + (fRec970 + fRec968[1]));
			fRec966[0] = fRec968[0];
			float fRec967 = (fRec970 + fTemp454);
			float fTemp458 = (fConst207 * ((0.0476964563f * fTemp51) - ((0.0310665164f * fTemp49) + (0.0275408607f * fTemp50))));
			float fTemp459 = (fConst208 * fRec975[1]);
			fRec977[0] = (fTemp458 + (fRec977[1] + fTemp459));
			fRec975[0] = fRec977[0];
			float fRec976 = (fTemp459 + fTemp458);
			float fTemp460 = (fConst210 * ((0.0253001824f * fTemp56) - ((((0.0435637385f * fTemp54) + (0.043817725f * fTemp58)) + (0.00720053958f * fTemp55)) + (0.0251454953f * fTemp57))));
			float fTemp461 = (fConst211 * fRec978[1]);
			float fTemp462 = (fConst212 * fRec981[1]);
			fRec983[0] = (fTemp460 + (fTemp461 + (fRec983[1] + fTemp462)));
			fRec981[0] = fRec983[0];
			float fRec982 = ((fTemp462 + fTemp461) + fTemp460);
			fRec980[0] = (fRec981[0] + fRec980[1]);
			fRec978[0] = fRec980[0];
			float fRec979 = fRec982;
			output20[i] = FAUSTFLOAT((fRec0[0] * ((0.0389862582f * fTemp62) + (fRec940 + (fRec955 + (fRec967 + (fRec976 + fRec979)))))));
			float fTemp463 = (fConst190 * (((0.0338677913f * fTemp17) + ((0.0186342616f * fTemp20) + ((0.00356351072f * fTemp16) + (((((0.00633311225f * fTemp10) + (0.0317217931f * fTemp18)) + (0.0110794473f * fTemp11)) + (0.0018966872f * fTemp13)) + (0.000768460101f * fTemp15))))) - (((0.00658475701f * fTemp12) + (0.00634902483f * fTemp14)) + (0.00606975658f * fTemp19))));
			float fTemp464 = (fConst192 * fRec996[1]);
			float fTemp465 = (fConst191 * fRec993[1]);
			fRec998[0] = (((fTemp463 + fRec998[1]) + fTemp464) + fTemp465);
			fRec996[0] = fRec998[0];
			float fRec997 = ((fTemp463 + fTemp464) + fTemp465);
			fRec995[0] = (fRec996[0] + fRec995[1]);
			fRec993[0] = fRec995[0];
			float fRec994 = fRec997;
			float fTemp466 = (fConst188 * fRec990[1]);
			float fTemp467 = (fConst187 * fRec987[1]);
			fRec992[0] = (((fRec994 + fRec992[1]) + fTemp466) + fTemp467);
			fRec990[0] = fRec992[0];
			float fRec991 = ((fRec994 + fTemp466) + fTemp467);
			fRec989[0] = (fRec990[0] + fRec989[1]);
			fRec987[0] = fRec989[0];
			float fRec988 = fRec991;
			float fTemp468 = (fConst185 * fRec984[1]);
			fRec986[0] = ((fRec988 + fRec986[1]) + fTemp468);
			fRec984[0] = fRec986[0];
			float fRec985 = (fRec988 + fTemp468);
			float fTemp469 = (fConst194 * fRec999[1]);
			float fTemp470 = (fConst195 * fRec1002[1]);
			float fTemp471 = (fConst197 * (((0.00882667303f * fTemp32) + (0.00754603557f * fTemp29)) - ((0.0303849783f * fTemp34) + ((((((0.0315431543f * fTemp30) + (0.0441567563f * fTemp31)) + (0.0040892777f * fTemp26)) + (0.0017726497f * fTemp27)) + (0.0150950169f * fTemp28)) + (0.000236804699f * fTemp33)))));
			float fTemp472 = (fConst198 * fRec1005[1]);
			float fTemp473 = (fConst199 * fRec1008[1]);
			fRec1010[0] = (fTemp471 + (fTemp472 + (fRec1010[1] + fTemp473)));
			fRec1008[0] = fRec1010[0];
			float fRec1009 = ((fTemp473 + fTemp472) + fTemp471);
			fRec1007[0] = (fRec1008[0] + fRec1007[1]);
			fRec1005[0] = fRec1007[0];
			float fRec1006 = fRec1009;
			fRec1004[0] = (fTemp469 + (fTemp470 + (fRec1006 + fRec1004[1])));
			fRec1002[0] = fRec1004[0];
			float fRec1003 = (fTemp469 + (fRec1006 + fTemp470));
			fRec1001[0] = (fRec1002[0] + fRec1001[1]);
			fRec999[0] = fRec1001[0];
			float fRec1000 = fRec1003;
			float fTemp474 = (fConst201 * fRec1011[1]);
			float fTemp475 = (fConst203 * ((((0.0520841889f * fTemp43) + (((0.0428706668f * fTemp38) + (0.0225811359f * fTemp39)) + (9.50566027e-05f * fTemp40))) + (0.00860575773f * fTemp44)) - ((0.00281936885f * fTemp42) + (0.0245684087f * fTemp41))));
			float fTemp476 = (fConst204 * fRec1014[1]);
			float fTemp477 = (fConst205 * fRec1017[1]);
			fRec1019[0] = (fTemp475 + (fTemp476 + (fRec1019[1] + fTemp477)));
			fRec1017[0] = fRec1019[0];
			float fRec1018 = ((fTemp477 + fTemp476) + fTemp475);
			fRec1016[0] = (fRec1017[0] + fRec1016[1]);
			fRec1014[0] = fRec1016[0];
			float fRec1015 = fRec1018;
			fRec1013[0] = (fTemp474 + (fRec1015 + fRec1013[1]));
			fRec1011[0] = fRec1013[0];
			float fRec1012 = (fRec1015 + fTemp474);
			float fTemp478 = (fConst207 * ((0.035060972f * fTemp51) - ((0.0311201271f * fTemp49) + (0.0551089793f * fTemp50))));
			float fTemp479 = (fConst208 * fRec1020[1]);
			fRec1022[0] = (fTemp478 + (fRec1022[1] + fTemp479));
			fRec1020[0] = fRec1022[0];
			float fRec1021 = (fTemp479 + fTemp478);
			float fTemp480 = (fConst210 * (((0.0438276269f * fTemp56) + (0.0249798838f * fTemp57)) - (((0.0550406128f * fTemp54) + (0.0253856331f * fTemp58)) + (0.0137328422f * fTemp55))));
			float fTemp481 = (fConst211 * fRec1023[1]);
			float fTemp482 = (fConst212 * fRec1026[1]);
			fRec1028[0] = (fTemp480 + (fTemp481 + (fRec1028[1] + fTemp482)));
			fRec1026[0] = fRec1028[0];
			float fRec1027 = ((fTemp482 + fTemp481) + fTemp480);
			fRec1025[0] = (fRec1026[0] + fRec1025[1]);
			fRec1023[0] = fRec1025[0];
			float fRec1024 = fRec1027;
			output21[i] = FAUSTFLOAT((fRec0[0] * ((fRec985 + (fRec1000 + (fRec1012 + (fRec1021 + fRec1024)))) + (0.0451872647f * fTemp62))));
			float fTemp483 = (fConst168 * (((0.0506550409f * fTemp56) + (0.0699529573f * fTemp57)) - (((2.42639999e-06f * fTemp54) + (9.29999988e-09f * fTemp58)) + (0.0202659946f * fTemp55))));
			float fTemp484 = (fConst169 * fRec1029[1]);
			float fTemp485 = (fConst170 * fRec1032[1]);
			fRec1034[0] = (fTemp483 + (fTemp484 + (fRec1034[1] + fTemp485)));
			fRec1032[0] = fRec1034[0];
			float fRec1033 = ((fTemp485 + fTemp484) + fTemp483);
			fRec1031[0] = (fRec1032[0] + fRec1031[1]);
			fRec1029[0] = fRec1031[0];
			float fRec1030 = fRec1033;
			float fTemp486 = (fConst181 * ((1.0128e-06f * fTemp51) - ((0.0311235841f * fTemp49) + (0.0753507093f * fTemp50))));
			float fTemp487 = (fConst182 * fRec1035[1]);
			fRec1037[0] = (fTemp486 + (fRec1037[1] + fTemp487));
			fRec1035[0] = fRec1037[0];
			float fRec1036 = (fTemp487 + fTemp486);
			float fTemp488 = (fConst172 * fRec1038[1]);
			float fTemp489 = (fConst174 * fRec1041[1]);
			float fTemp490 = (fConst175 * fRec1044[1]);
			float fTemp491 = (fConst177 * ((((2.86299996e-06f * fTemp10) + (3.13240002e-06f * fTemp18)) + (0.00744262617f * fTemp16)) - ((0.0351530798f * fTemp17) + ((0.0365271121f * fTemp20) + (((0.00764689781f * fTemp15) + ((((1.48859999e-06f * fTemp11) + (2.93829999e-06f * fTemp12)) + (2.09059999e-06f * fTemp13)) + (0.00639055064f * fTemp14))) + (0.00521416403f * fTemp19))))));
			float fTemp492 = (fConst178 * fRec1047[1]);
			float fTemp493 = (fConst179 * fRec1050[1]);
			fRec1052[0] = (fTemp491 + (fTemp492 + (fRec1052[1] + fTemp493)));
			fRec1050[0] = fRec1052[0];
			float fRec1051 = ((fTemp493 + fTemp492) + fTemp491);
			fRec1049[0] = (fRec1050[0] + fRec1049[1]);
			fRec1047[0] = fRec1049[0];
			float fRec1048 = fRec1051;
			fRec1046[0] = (fTemp489 + (fTemp490 + (fRec1048 + fRec1046[1])));
			fRec1044[0] = fRec1046[0];
			float fRec1045 = (fTemp489 + (fRec1048 + fTemp490));
			fRec1043[0] = (fRec1044[0] + fRec1043[1]);
			fRec1041[0] = fRec1043[0];
			float fRec1042 = fRec1045;
			fRec1040[0] = (fTemp488 + (fRec1042 + fRec1040[1]));
			fRec1038[0] = fRec1040[0];
			float fRec1039 = (fRec1042 + fTemp488);
			float fTemp494 = (fConst162 * fRec1053[1]);
			float fTemp495 = (fConst164 * (((3.27869998e-06f * fTemp43) + (((1.25589997e-06f * fTemp38) + (0.0226109792f * fTemp39)) + (0.0101913298f * fTemp40))) - ((0.0602908395f * fTemp44) + ((1.91830009e-06f * fTemp42) + (0.0493774936f * fTemp41)))));
			float fTemp496 = (fConst165 * fRec1056[1]);
			float fTemp497 = (fConst166 * fRec1059[1]);
			fRec1061[0] = (fTemp495 + (fTemp496 + (fRec1061[1] + fTemp497)));
			fRec1059[0] = fRec1061[0];
			float fRec1060 = ((fTemp497 + fTemp496) + fTemp495);
			fRec1058[0] = (fRec1059[0] + fRec1058[1]);
			fRec1056[0] = fRec1058[0];
			float fRec1057 = fRec1060;
			fRec1055[0] = (fTemp494 + (fRec1057 + fRec1055[1]));
			fRec1053[0] = fRec1055[0];
			float fRec1054 = (fRec1057 + fTemp494);
			float fTemp498 = (fConst155 * fRec1062[1]);
			float fTemp499 = (fConst156 * fRec1065[1]);
			float fTemp500 = (fConst158 * (((0.0481978282f * fTemp34) + ((((1.86859995e-06f * fTemp26) + (2.50659991e-06f * fTemp32)) + (0.00405618129f * fTemp27)) + (0.0440328717f * fTemp33))) - ((0.000241765607f * fTemp29) + (((3.40409997e-06f * fTemp30) + (2.38200005e-06f * fTemp31)) + (0.0175152477f * fTemp28)))));
			float fTemp501 = ((fConst160 * fRec1071[1]) + (fConst159 * fRec1068[1]));
			fRec1073[0] = (fTemp500 + (fTemp501 + fRec1073[1]));
			fRec1071[0] = fRec1073[0];
			float fRec1072 = (fTemp501 + fTemp500);
			fRec1070[0] = (fRec1071[0] + fRec1070[1]);
			fRec1068[0] = fRec1070[0];
			float fRec1069 = fRec1072;
			fRec1067[0] = (fTemp498 + (fTemp499 + (fRec1069 + fRec1067[1])));
			fRec1065[0] = fRec1067[0];
			float fRec1066 = (fTemp498 + (fRec1069 + fTemp499));
			fRec1064[0] = (fRec1065[0] + fRec1064[1]);
			fRec1062[0] = fRec1064[0];
			float fRec1063 = fRec1066;
			fVec20[0] = ((0.0513156652f * fTemp62) + (fRec1030 + (fRec1036 + (fRec1039 + (fRec1054 + fRec1063)))));
			output22[i] = FAUSTFLOAT((0.999892831f * (fRec0[0] * fVec20[iConst183])));
			float fTemp502 = (fConst185 * fRec1074[1]);
			float fTemp503 = (fConst187 * fRec1077[1]);
			float fTemp504 = (fConst188 * fRec1080[1]);
			float fTemp505 = (fConst190 * (((0.0258844942f * fTemp17) + ((0.0185196511f * fTemp20) + ((2.91230003e-06f * fTemp19) + ((0.00376871321f * fTemp16) + (((0.00651526405f * fTemp12) + (0.00355658191f * fTemp13)) + (0.00616663601f * fTemp15)))))) - ((((0.0149628287f * fTemp10) + (0.0320945792f * fTemp18)) + (0.0169494059f * fTemp11)) + (0.00628286554f * fTemp14))));
			float fTemp506 = (fConst191 * fRec1083[1]);
			float fTemp507 = (fConst192 * fRec1086[1]);
			fRec1088[0] = (fTemp505 + (fTemp506 + (fRec1088[1] + fTemp507)));
			fRec1086[0] = fRec1088[0];
			float fRec1087 = ((fTemp507 + fTemp506) + fTemp505);
			fRec1085[0] = (fRec1086[0] + fRec1085[1]);
			fRec1083[0] = fRec1085[0];
			float fRec1084 = fRec1087;
			fRec1082[0] = (fTemp503 + (fTemp504 + (fRec1084 + fRec1082[1])));
			fRec1080[0] = fRec1082[0];
			float fRec1081 = (fTemp503 + (fRec1084 + fTemp504));
			fRec1079[0] = (fRec1080[0] + fRec1079[1]);
			fRec1077[0] = fRec1079[0];
			float fRec1078 = fRec1081;
			fRec1076[0] = (fTemp502 + (fRec1078 + fRec1076[1]));
			fRec1074[0] = fRec1076[0];
			float fRec1075 = (fRec1078 + fTemp502);
			float fTemp508 = (fConst194 * fRec1089[1]);
			float fTemp509 = (fConst195 * fRec1092[1]);
			float fTemp510 = (fConst197 * (((7.83720043e-06f * fTemp33) + ((((0.0319174007f * fTemp30) + (0.0442688316f * fTemp31)) + (0.0128895929f * fTemp26)) + (0.00743747782f * fTemp29))) - ((((0.00872272067f * fTemp32) + (0.00764290709f * fTemp27)) + (0.0151190255f * fTemp28)) + (0.0184101183f * fTemp34))));
			float fTemp511 = (fConst198 * fRec1095[1]);
			float fTemp512 = (fConst199 * fRec1098[1]);
			fRec1100[0] = (fTemp510 + (fTemp511 + (fRec1100[1] + fTemp512)));
			fRec1098[0] = fRec1100[0];
			float fRec1099 = ((fTemp512 + fTemp511) + fTemp510);
			fRec1097[0] = (fRec1098[0] + fRec1097[1]);
			fRec1095[0] = fRec1097[0];
			float fRec1096 = fRec1099;
			fRec1094[0] = (fTemp508 + (fTemp509 + (fRec1096 + fRec1094[1])));
			fRec1092[0] = fRec1094[0];
			float fRec1093 = (fTemp508 + (fRec1096 + fTemp509));
			fRec1091[0] = (fRec1092[0] + fRec1091[1]);
			fRec1089[0] = fRec1091[0];
			float fRec1090 = fRec1093;
			float fTemp513 = (fConst201 * fRec1101[1]);
			float fTemp514 = (fConst203 * ((0.0225571264f * fTemp39) - ((1.28254997e-05f * fTemp44) + (((0.0438632742f * fTemp43) + (((0.0427932888f * fTemp38) + (0.00361614698f * fTemp42)) + (0.00625533238f * fTemp40))) + (0.024712624f * fTemp41)))));
			float fTemp515 = (fConst204 * fRec1104[1]);
			float fTemp516 = (fConst205 * fRec1107[1]);
			fRec1109[0] = (fTemp514 + (fTemp515 + (fRec1109[1] + fTemp516)));
			fRec1107[0] = fRec1109[0];
			float fRec1108 = ((fTemp516 + fTemp515) + fTemp514);
			fRec1106[0] = (fRec1107[0] + fRec1106[1]);
			fRec1104[0] = fRec1106[0];
			float fRec1105 = fRec1108;
			fRec1103[0] = (fTemp513 + (fRec1105 + fRec1103[1]));
			fRec1101[0] = fRec1103[0];
			float fRec1102 = (fRec1105 + fTemp513);
			float fTemp517 = (fConst207 * (0.0f - (((0.0275392588f * fTemp51) + (0.0310740825f * fTemp49)) + (0.047707703f * fTemp50))));
			float fTemp518 = (fConst208 * fRec1110[1]);
			fRec1112[0] = (fTemp517 + (fRec1112[1] + fTemp518));
			fRec1110[0] = fRec1112[0];
			float fRec1111 = (fTemp518 + fTemp517);
			float fTemp519 = (fConst210 * (((((0.04356445f * fTemp54) + (0.0252987836f * fTemp58)) + (0.043821644f * fTemp56)) + (0.0251624361f * fTemp57)) - (0.00719131483f * fTemp55)));
			float fTemp520 = (fConst211 * fRec1113[1]);
			float fTemp521 = (fConst212 * fRec1116[1]);
			fRec1118[0] = (fTemp519 + (fTemp520 + (fRec1118[1] + fTemp521)));
			fRec1116[0] = fRec1118[0];
			float fRec1117 = ((fTemp521 + fTemp520) + fTemp519);
			fRec1115[0] = (fRec1116[0] + fRec1115[1]);
			fRec1113[0] = fRec1115[0];
			float fRec1114 = fRec1117;
			output23[i] = FAUSTFLOAT((fRec0[0] * ((0.0389952473f * fTemp62) + (fRec1075 + (fRec1090 + (fRec1102 + (fRec1111 + fRec1114)))))));
			float fTemp522 = (fConst185 * fRec1119[1]);
			float fTemp523 = (fConst187 * fRec1122[1]);
			float fTemp524 = (fConst188 * fRec1125[1]);
			float fTemp525 = (fConst190 * (((0.0186354369f * fTemp20) + ((((((0.0338853784f * fTemp10) + (0.0317260511f * fTemp18)) + (0.00606415141f * fTemp11)) + (0.00658797659f * fTemp12)) + (0.000767703285f * fTemp13)) + (0.0110739144f * fTemp19))) - (((0.00355676934f * fTemp16) + ((0.00634056516f * fTemp14) + (0.00188509712f * fTemp15))) + (0.00635515945f * fTemp17))));
			float fTemp526 = (fConst191 * fRec1128[1]);
			float fTemp527 = (fConst192 * fRec1131[1]);
			fRec1133[0] = (fTemp525 + (fTemp526 + (fRec1133[1] + fTemp527)));
			fRec1131[0] = fRec1133[0];
			float fRec1132 = ((fTemp527 + fTemp526) + fTemp525);
			fRec1130[0] = (fRec1131[0] + fRec1130[1]);
			fRec1128[0] = fRec1130[0];
			float fRec1129 = fRec1132;
			fRec1127[0] = (fTemp523 + (fTemp524 + (fRec1129 + fRec1127[1])));
			fRec1125[0] = fRec1127[0];
			float fRec1126 = (fTemp523 + (fRec1129 + fTemp524));
			fRec1124[0] = (fRec1125[0] + fRec1124[1]);
			fRec1122[0] = fRec1124[0];
			float fRec1123 = fRec1126;
			fRec1121[0] = (fTemp522 + (fRec1123 + fRec1121[1]));
			fRec1119[0] = fRec1121[0];
			float fRec1120 = (fRec1123 + fTemp522);
			float fTemp528 = (fConst194 * fRec1134[1]);
			float fTemp529 = (fConst195 * fRec1137[1]);
			float fTemp530 = (fConst197 * (((0.000234882798f * fTemp31) + (0.00408276776f * fTemp26)) - ((0.0303829871f * fTemp34) + ((0.0441530906f * fTemp33) + ((0.00753899058f * fTemp29) + ((((0.0315669924f * fTemp30) + (0.0150919724f * fTemp32)) + (0.00177532865f * fTemp27)) + (0.0088315988f * fTemp28)))))));
			float fTemp531 = (fConst198 * fRec1140[1]);
			float fTemp532 = (fConst199 * fRec1143[1]);
			fRec1145[0] = (fTemp530 + (fTemp531 + (fRec1145[1] + fTemp532)));
			fRec1143[0] = fRec1145[0];
			float fRec1144 = ((fTemp532 + fTemp531) + fTemp530);
			fRec1142[0] = (fRec1143[0] + fRec1142[1]);
			fRec1140[0] = fRec1142[0];
			float fRec1141 = fRec1144;
			fRec1139[0] = (fTemp528 + (fTemp529 + (fRec1141 + fRec1139[1])));
			fRec1137[0] = fRec1139[0];
			float fRec1138 = (fTemp528 + (fRec1141 + fTemp529));
			fRec1136[0] = (fRec1137[0] + fRec1136[1]);
			fRec1134[0] = fRec1136[0];
			float fRec1135 = fRec1138;
			float fTemp533 = (fConst201 * fRec1146[1]);
			float fTemp534 = (fConst203 * (((0.0520936586f * fTemp44) + ((0.0245648362f * fTemp41) + (((0.000104623199f * fTemp42) + (0.022579886f * fTemp39)) + (0.00282499567f * fTemp40)))) - ((0.00859174039f * fTemp43) + (0.0428586192f * fTemp38))));
			float fTemp535 = (fConst204 * fRec1149[1]);
			float fTemp536 = (fConst205 * fRec1152[1]);
			fRec1154[0] = (fTemp534 + (fTemp535 + (fRec1154[1] + fTemp536)));
			fRec1152[0] = fRec1154[0];
			float fRec1153 = ((fTemp536 + fTemp535) + fTemp534);
			fRec1151[0] = (fRec1152[0] + fRec1151[1]);
			fRec1149[0] = fRec1151[0];
			float fRec1150 = fRec1153;
			fRec1148[0] = (fTemp533 + (fRec1150 + fRec1148[1]));
			fRec1146[0] = fRec1148[0];
			float fRec1147 = (fRec1150 + fTemp533);
			float fTemp537 = (fConst207 * (0.0f - (((0.0551024303f * fTemp51) + (0.0311034713f * fTemp49)) + (0.0350493789f * fTemp50))));
			float fTemp538 = (fConst208 * fRec1155[1]);
			fRec1157[0] = (fTemp537 + (fRec1157[1] + fTemp538));
			fRec1155[0] = fRec1157[0];
			float fRec1156 = (fTemp538 + fTemp537);
			float fTemp539 = (fConst210 * ((((0.0550333261f * fTemp54) + (0.0438125134f * fTemp58)) + (0.0253732949f * fTemp56)) - ((0.0137405042f * fTemp55) + (0.0249889698f * fTemp57))));
			float fTemp540 = (fConst211 * fRec1158[1]);
			float fTemp541 = (fConst212 * fRec1161[1]);
			fRec1163[0] = (fTemp539 + (fTemp540 + (fRec1163[1] + fTemp541)));
			fRec1161[0] = fRec1163[0];
			float fRec1162 = ((fTemp541 + fTemp540) + fTemp539);
			fRec1160[0] = (fRec1161[0] + fRec1160[1]);
			fRec1158[0] = fRec1160[0];
			float fRec1159 = fRec1162;
			output24[i] = FAUSTFLOAT((fRec0[0] * ((0.0451742858f * fTemp62) + (fRec1120 + (fRec1135 + (fRec1147 + (fRec1156 + fRec1159)))))));
			float fTemp542 = (fConst172 * fRec1164[1]);
			float fTemp543 = (fConst174 * fRec1167[1]);
			float fTemp544 = (fConst175 * fRec1170[1]);
			float fTemp545 = (fConst177 * ((0.00521597825f * fTemp11) - ((1.67149994e-06f * fTemp17) + ((0.0365250632f * fTemp20) + ((6.54500013e-07f * fTemp19) + ((0.00744980061f * fTemp16) + ((2.40129998e-06f * fTemp15) + (((((0.0351438783f * fTemp10) + (1.44139995e-06f * fTemp18)) + (2.62150002e-06f * fTemp12)) + (0.0076408051f * fTemp13)) + (0.0063837301f * fTemp14)))))))));
			float fTemp546 = (fConst178 * fRec1173[1]);
			float fTemp547 = (fConst179 * fRec1176[1]);
			fRec1178[0] = (fTemp545 + (fTemp546 + (fRec1178[1] + fTemp547)));
			fRec1176[0] = fRec1178[0];
			float fRec1177 = ((fTemp547 + fTemp546) + fTemp545);
			fRec1175[0] = (fRec1176[0] + fRec1175[1]);
			fRec1173[0] = fRec1175[0];
			float fRec1174 = fRec1177;
			fRec1172[0] = (fTemp543 + (fTemp544 + (fRec1174 + fRec1172[1])));
			fRec1170[0] = fRec1172[0];
			float fRec1171 = (fTemp543 + (fRec1174 + fTemp544));
			fRec1169[0] = (fRec1170[0] + fRec1169[1]);
			fRec1167[0] = fRec1169[0];
			float fRec1168 = fRec1171;
			fRec1166[0] = (fTemp542 + (fRec1168 + fRec1166[1]));
			fRec1164[0] = fRec1166[0];
			float fRec1165 = (fRec1168 + fTemp542);
			float fTemp548 = (fConst155 * fRec1179[1]);
			float fTemp549 = (fConst156 * fRec1182[1]);
			float fTemp550 = (fConst158 * (((0.0481902547f * fTemp34) + ((1.53270003e-06f * fTemp33) + ((0.000241509901f * fTemp29) + ((0.00404385943f * fTemp27) + (2.74960007e-06f * fTemp28))))) - ((((4.33620016e-06f * fTemp30) + (0.0440342613f * fTemp31)) + (1.25179997e-06f * fTemp26)) + (0.0175272096f * fTemp32))));
			float fTemp551 = (fConst159 * fRec1185[1]);
			float fTemp552 = (fConst160 * fRec1188[1]);
			fRec1190[0] = (fTemp550 + (fTemp551 + (fRec1190[1] + fTemp552)));
			fRec1188[0] = fRec1190[0];
			float fRec1189 = ((fTemp552 + fTemp551) + fTemp550);
			fRec1187[0] = (fRec1188[0] + fRec1187[1]);
			fRec1185[0] = fRec1187[0];
			float fRec1186 = fRec1189;
			fRec1184[0] = (fTemp548 + (fTemp549 + (fRec1186 + fRec1184[1])));
			fRec1182[0] = fRec1184[0];
			float fRec1183 = (fTemp548 + (fRec1186 + fTemp549));
			fRec1181[0] = (fRec1182[0] + fRec1181[1]);
			fRec1179[0] = fRec1181[0];
			float fRec1180 = fRec1183;
			float fTemp553 = (fConst162 * fRec1191[1]);
			float fTemp554 = (fConst164 * ((5.53410018e-06f * fTemp44) + ((0.0493814163f * fTemp41) + ((0.060286101f * fTemp43) + ((((9.41999986e-07f * fTemp38) + (0.0101943975f * fTemp42)) + (0.0226276685f * fTemp39)) + (2.46300004e-07f * fTemp40))))));
			float fTemp555 = (fConst165 * fRec1194[1]);
			float fTemp556 = (fConst166 * fRec1197[1]);
			fRec1199[0] = (fTemp554 + (fTemp555 + (fRec1199[1] + fTemp556)));
			fRec1197[0] = fRec1199[0];
			float fRec1198 = ((fTemp556 + fTemp555) + fTemp554);
			fRec1196[0] = (fRec1197[0] + fRec1196[1]);
			fRec1194[0] = fRec1196[0];
			float fRec1195 = fRec1198;
			fRec1193[0] = (fTemp553 + (fRec1195 + fRec1193[1]));
			fRec1191[0] = fRec1193[0];
			float fRec1192 = (fRec1195 + fTemp553);
			float fTemp557 = (fConst181 * (0.0f - (((0.0753505081f * fTemp51) + (0.0311186891f * fTemp49)) + (3.0092001e-06f * fTemp50))));
			float fTemp558 = (fConst182 * fRec1200[1]);
			fRec1202[0] = (fTemp557 + (fRec1202[1] + fTemp558));
			fRec1200[0] = fRec1202[0];
			float fRec1201 = (fTemp558 + fTemp557);
			float fTemp559 = (fConst168 * ((((4.94569986e-06f * fTemp54) + (0.0506584905f * fTemp58)) + (2.134e-07f * fTemp56)) - ((0.020276377f * fTemp55) + (0.0699512139f * fTemp57))));
			float fTemp560 = (fConst169 * fRec1203[1]);
			float fTemp561 = (fConst170 * fRec1206[1]);
			fRec1208[0] = (fTemp559 + (fTemp560 + (fRec1208[1] + fTemp561)));
			fRec1206[0] = fRec1208[0];
			float fRec1207 = ((fTemp561 + fTemp560) + fTemp559);
			fRec1205[0] = (fRec1206[0] + fRec1205[1]);
			fRec1203[0] = fRec1205[0];
			float fRec1204 = fRec1207;
			fVec21[0] = ((0.051312048f * fTemp62) + (fRec1165 + (fRec1180 + (fRec1192 + (fRec1201 + fRec1204)))));
			output25[i] = FAUSTFLOAT((0.999892831f * (fRec0[0] * fVec21[iConst183])));
			float fTemp562 = (fConst185 * fRec1209[1]);
			float fTemp563 = (fConst187 * fRec1212[1]);
			float fTemp564 = (fConst188 * fRec1215[1]);
			float fTemp565 = (fConst190 * (((0.014957441f * fTemp17) + ((((0.0258946326f * fTemp10) + (1.09929999e-06f * fTemp11)) + (0.00614809478f * fTemp13)) + (0.0185195766f * fTemp20))) - ((0.0169466306f * fTemp19) + ((0.00376956514f * fTemp16) + ((0.00354469032f * fTemp15) + (((0.0321027488f * fTemp18) + (0.00651141303f * fTemp12)) + (0.00631031254f * fTemp14)))))));
			float fTemp566 = (fConst191 * fRec1218[1]);
			float fTemp567 = (fConst192 * fRec1221[1]);
			fRec1223[0] = (fTemp565 + (fTemp566 + (fRec1223[1] + fTemp567)));
			fRec1221[0] = fRec1223[0];
			float fRec1222 = ((fTemp567 + fTemp566) + fTemp565);
			fRec1220[0] = (fRec1221[0] + fRec1220[1]);
			fRec1218[0] = fRec1220[0];
			float fRec1219 = fRec1222;
			fRec1217[0] = (fTemp563 + (fTemp564 + (fRec1219 + fRec1217[1])));
			fRec1215[0] = fRec1217[0];
			float fRec1216 = (fTemp563 + (fRec1219 + fTemp564));
			fRec1214[0] = (fRec1215[0] + fRec1214[1]);
			fRec1212[0] = fRec1214[0];
			float fRec1213 = fRec1216;
			fRec1211[0] = (fTemp562 + (fRec1213 + fRec1211[1]));
			fRec1209[0] = fRec1211[0];
			float fRec1210 = (fRec1213 + fTemp562);
			float fTemp568 = (fConst194 * fRec1224[1]);
			float fTemp569 = (fConst195 * fRec1227[1]);
			float fTemp570 = (fConst197 * ((((0.0319179147f * fTemp30) + (0.00871484261f * fTemp28)) + (0.0442693345f * fTemp33)) - ((((((1.15571002e-05f * fTemp31) + (0.0128843412f * fTemp26)) + (0.0151081355f * fTemp32)) + (0.00760954386f * fTemp27)) + (0.00743750483f * fTemp29)) + (0.0184176769f * fTemp34))));
			float fTemp571 = (fConst198 * fRec1230[1]);
			float fTemp572 = (fConst199 * fRec1233[1]);
			fRec1235[0] = (fTemp570 + (fTemp571 + (fRec1235[1] + fTemp572)));
			fRec1233[0] = fRec1235[0];
			float fRec1234 = ((fTemp572 + fTemp571) + fTemp570);
			fRec1232[0] = (fRec1233[0] + fRec1232[1]);
			fRec1230[0] = fRec1232[0];
			float fRec1231 = fRec1234;
			fRec1229[0] = (fTemp568 + (fTemp569 + (fRec1231 + fRec1229[1])));
			fRec1227[0] = fRec1229[0];
			float fRec1228 = (fTemp568 + (fRec1231 + fTemp569));
			fRec1226[0] = (fRec1227[0] + fRec1226[1]);
			fRec1224[0] = fRec1226[0];
			float fRec1225 = fRec1228;
			float fTemp573 = (fConst201 * fRec1236[1]);
			float fTemp574 = (fConst203 * (((0.0247124508f * fTemp41) + ((9.34639957e-06f * fTemp43) + (((0.0427875109f * fTemp38) + (0.0225271285f * fTemp39)) + (0.00361575908f * fTemp40)))) - ((0.00625694171f * fTemp42) + (0.043863859f * fTemp44))));
			float fTemp575 = (fConst204 * fRec1239[1]);
			float fTemp576 = (fConst205 * fRec1242[1]);
			fRec1244[0] = (fTemp574 + (fTemp575 + (fRec1244[1] + fTemp576)));
			fRec1242[0] = fRec1244[0];
			float fRec1243 = ((fTemp576 + fTemp575) + fTemp574);
			fRec1241[0] = (fRec1242[0] + fRec1241[1]);
			fRec1239[0] = fRec1241[0];
			float fRec1240 = fRec1243;
			fRec1238[0] = (fTemp573 + (fRec1240 + fRec1238[1]));
			fRec1236[0] = fRec1238[0];
			float fRec1237 = (fRec1240 + fTemp573);
			float fTemp577 = (fConst207 * ((0.0275366008f * fTemp50) - ((0.0477022678f * fTemp51) + (0.0310844164f * fTemp49))));
			float fTemp578 = (fConst208 * fRec1245[1]);
			fRec1247[0] = (fTemp577 + (fRec1247[1] + fTemp578));
			fRec1245[0] = fRec1247[0];
			float fRec1246 = (fTemp578 + fTemp577);
			float fTemp579 = (fConst210 * ((0.0438174382f * fTemp58) - ((((0.0435620397f * fTemp54) + (0.00717064785f * fTemp55)) + (0.025294859f * fTemp56)) + (0.0251593087f * fTemp57))));
			float fTemp580 = (fConst211 * fRec1248[1]);
			float fTemp581 = (fConst212 * fRec1251[1]);
			fRec1253[0] = (fTemp579 + (fTemp580 + (fRec1253[1] + fTemp581)));
			fRec1251[0] = fRec1253[0];
			float fRec1252 = ((fTemp581 + fTemp580) + fTemp579);
			fRec1250[0] = (fRec1251[0] + fRec1250[1]);
			fRec1248[0] = fRec1250[0];
			float fRec1249 = fRec1252;
			output26[i] = FAUSTFLOAT((fRec0[0] * ((0.0389983803f * fTemp62) + (fRec1210 + (fRec1225 + (fRec1237 + (fRec1246 + fRec1249)))))));
			float fTemp582 = (fConst185 * fRec1254[1]);
			float fTemp583 = (fConst187 * fRec1257[1]);
			float fTemp584 = (fConst188 * fRec1260[1]);
			float fTemp585 = (fConst190 * (((0.0186418984f * fTemp20) + ((0.00607366627f * fTemp19) + ((0.0317283645f * fTemp18) + (0.00355117139f * fTemp16)))) - (((0.000773787615f * fTemp15) + (((((0.00634632306f * fTemp10) + (0.0110836644f * fTemp11)) + (0.00659921672f * fTemp12)) + (0.00187675713f * fTemp13)) + (0.00633151317f * fTemp14))) + (0.033878997f * fTemp17))));
			float fTemp586 = (fConst191 * fRec1263[1]);
			float fTemp587 = (fConst192 * fRec1266[1]);
			fRec1268[0] = (fTemp585 + (fTemp586 + (fRec1268[1] + fTemp587)));
			fRec1266[0] = fRec1268[0];
			float fRec1267 = ((fTemp587 + fTemp586) + fTemp585);
			fRec1265[0] = (fRec1266[0] + fRec1265[1]);
			fRec1263[0] = fRec1265[0];
			float fRec1264 = fRec1267;
			fRec1262[0] = (fTemp583 + (fTemp584 + (fRec1264 + fRec1262[1])));
			fRec1260[0] = fRec1262[0];
			float fRec1261 = (fTemp583 + (fRec1264 + fTemp584));
			fRec1259[0] = (fRec1260[0] + fRec1259[1]);
			fRec1257[0] = fRec1259[0];
			float fRec1258 = fRec1261;
			fRec1256[0] = (fTemp582 + (fRec1258 + fRec1256[1]));
			fRec1254[0] = fRec1256[0];
			float fRec1255 = (fRec1258 + fTemp582);
			float fTemp588 = (fConst194 * fRec1269[1]);
			float fTemp589 = (fConst195 * fRec1272[1]);
			float fTemp590 = (fConst197 * (((0.000237565604f * fTemp33) + ((0.00755442679f * fTemp29) + ((0.044161845f * fTemp31) + (0.0150947422f * fTemp28)))) - (((((0.0315532722f * fTemp30) + (0.00409164699f * fTemp26)) + (0.00884327665f * fTemp32)) + (0.00179907202f * fTemp27)) + (0.0303800162f * fTemp34))));
			float fTemp591 = (fConst198 * fRec1275[1]);
			float fTemp592 = (fConst199 * fRec1278[1]);
			fRec1280[0] = (fTemp590 + (fTemp591 + (fRec1280[1] + fTemp592)));
			fRec1278[0] = fRec1280[0];
			float fRec1279 = ((fTemp592 + fTemp591) + fTemp590);
			fRec1277[0] = (fRec1278[0] + fRec1277[1]);
			fRec1275[0] = fRec1277[0];
			float fRec1276 = fRec1279;
			fRec1274[0] = (fTemp588 + (fTemp589 + (fRec1276 + fRec1274[1])));
			fRec1272[0] = fRec1274[0];
			float fRec1273 = (fTemp588 + (fRec1276 + fTemp589));
			fRec1271[0] = (fRec1272[0] + fRec1271[1]);
			fRec1269[0] = fRec1271[0];
			float fRec1270 = fRec1273;
			float fTemp593 = (fConst201 * fRec1281[1]);
			float fTemp594 = (fConst203 * ((((0.0428688265f * fTemp38) + (0.0028221251f * fTemp42)) + (0.0225989278f * fTemp39)) - ((0.0085950112f * fTemp44) + (((0.0520783328f * fTemp43) + (8.48522977e-05f * fTemp40)) + (0.0245707203f * fTemp41)))));
			float fTemp595 = (fConst204 * fRec1284[1]);
			float fTemp596 = (fConst205 * fRec1287[1]);
			fRec1289[0] = (fTemp594 + (fTemp595 + (fRec1289[1] + fTemp596)));
			fRec1287[0] = fRec1289[0];
			float fRec1288 = ((fTemp596 + fTemp595) + fTemp594);
			fRec1286[0] = (fRec1287[0] + fRec1286[1]);
			fRec1284[0] = fRec1286[0];
			float fRec1285 = fRec1288;
			fRec1283[0] = (fTemp593 + (fRec1285 + fRec1283[1]));
			fRec1281[0] = fRec1283[0];
			float fRec1282 = (fRec1285 + fTemp593);
			float fTemp597 = (fConst207 * ((0.0550909229f * fTemp50) - ((0.0350420922f * fTemp51) + (0.031103991f * fTemp49))));
			float fTemp598 = (fConst208 * fRec1290[1]);
			fRec1292[0] = (fTemp597 + (fRec1292[1] + fTemp598));
			fRec1290[0] = fRec1292[0];
			float fRec1291 = (fTemp598 + fTemp597);
			float fTemp599 = (fConst210 * (((0.0253775977f * fTemp58) + (0.0249799546f * fTemp57)) - (((0.0550207198f * fTemp54) + (0.0137372883f * fTemp55)) + (0.0438250452f * fTemp56))));
			float fTemp600 = (fConst211 * fRec1293[1]);
			float fTemp601 = (fConst212 * fRec1296[1]);
			fRec1298[0] = (fTemp599 + (fTemp600 + (fRec1298[1] + fTemp601)));
			fRec1296[0] = fRec1298[0];
			float fRec1297 = ((fTemp601 + fTemp600) + fTemp599);
			fRec1295[0] = (fRec1296[0] + fRec1295[1]);
			fRec1293[0] = fRec1295[0];
			float fRec1294 = fRec1297;
			output27[i] = FAUSTFLOAT((fRec0[0] * ((0.0451631024f * fTemp62) + (fRec1255 + (fRec1270 + (fRec1282 + (fRec1291 + fRec1294)))))));
			float fTemp602 = (fConst172 * fRec1299[1]);
			float fTemp603 = (fConst174 * fRec1302[1]);
			float fTemp604 = (fConst175 * fRec1305[1]);
			float fTemp605 = (fConst177 * ((((0.00521355262f * fTemp19) + ((0.00744356029f * fTemp16) + ((((2.61009995e-06f * fTemp10) + (6.90700006e-07f * fTemp11)) + (2.21749997e-06f * fTemp13)) + (0.00764257694f * fTemp15)))) + (0.035143353f * fTemp17)) - ((((5.10790005e-06f * fTemp18) + (6.73790009e-06f * fTemp12)) + (0.00637228461f * fTemp14)) + (0.0365273505f * fTemp20))));
			float fTemp606 = (fConst178 * fRec1308[1]);
			float fTemp607 = (fConst179 * fRec1311[1]);
			fRec1313[0] = (fTemp605 + (fTemp606 + (fRec1313[1] + fTemp607)));
			fRec1311[0] = fRec1313[0];
			float fRec1312 = ((fTemp607 + fTemp606) + fTemp605);
			fRec1310[0] = (fRec1311[0] + fRec1310[1]);
			fRec1308[0] = fRec1310[0];
			float fRec1309 = fRec1312;
			fRec1307[0] = (fTemp603 + (fTemp604 + (fRec1309 + fRec1307[1])));
			fRec1305[0] = fRec1307[0];
			float fRec1306 = (fTemp603 + (fRec1309 + fTemp604));
			fRec1304[0] = (fRec1305[0] + fRec1304[1]);
			fRec1302[0] = fRec1304[0];
			float fRec1303 = fRec1306;
			fRec1301[0] = (fTemp602 + (fRec1303 + fRec1301[1]));
			fRec1299[0] = fRec1301[0];
			float fRec1300 = (fRec1303 + fTemp602);
			float fTemp608 = (fConst155 * fRec1314[1]);
			float fTemp609 = (fConst156 * fRec1317[1]);
			float fTemp610 = (fConst158 * ((((0.0175209474f * fTemp28) + (((5.56070017e-06f * fTemp30) + (1.77610002e-06f * fTemp26)) + (0.00404935237f * fTemp27))) + (0.0481916592f * fTemp34)) - ((0.0440295003f * fTemp33) + (((5.32720014e-06f * fTemp31) + (1.99109991e-06f * fTemp32)) + (0.000249728211f * fTemp29)))));
			float fTemp611 = (fConst159 * fRec1320[1]);
			float fTemp612 = (fConst160 * fRec1323[1]);
			fRec1325[0] = (fTemp610 + (fTemp611 + (fRec1325[1] + fTemp612)));
			fRec1323[0] = fRec1325[0];
			float fRec1324 = ((fTemp612 + fTemp611) + fTemp610);
			fRec1322[0] = (fRec1323[0] + fRec1322[1]);
			fRec1320[0] = fRec1322[0];
			float fRec1321 = fRec1324;
			fRec1319[0] = (fTemp608 + (fTemp609 + (fRec1321 + fRec1319[1])));
			fRec1317[0] = fRec1319[0];
			float fRec1318 = (fTemp608 + (fRec1321 + fTemp609));
			fRec1316[0] = (fRec1317[0] + fRec1316[1]);
			fRec1314[0] = fRec1316[0];
			float fRec1315 = fRec1318;
			float fTemp613 = (fConst162 * fRec1326[1]);
			float fTemp614 = (fConst164 * ((((6.92689991e-06f * fTemp43) + ((7.53400002e-07f * fTemp42) + (0.0226198863f * fTemp39))) + (0.0602886081f * fTemp44)) - ((0.0493706539f * fTemp41) + ((5.07279992e-06f * fTemp38) + (0.0102069797f * fTemp40)))));
			float fTemp615 = (fConst165 * fRec1329[1]);
			float fTemp616 = (fConst166 * fRec1332[1]);
			fRec1334[0] = (fTemp614 + (fTemp615 + (fRec1334[1] + fTemp616)));
			fRec1332[0] = fRec1334[0];
			float fRec1333 = ((fTemp616 + fTemp615) + fTemp614);
			fRec1331[0] = (fRec1332[0] + fRec1331[1]);
			fRec1329[0] = fRec1331[0];
			float fRec1330 = fRec1333;
			fRec1328[0] = (fTemp613 + (fRec1330 + fRec1328[1]));
			fRec1326[0] = fRec1328[0];
			float fRec1327 = (fRec1330 + fTemp613);
			float fTemp617 = (fConst181 * (((3.88260014e-06f * fTemp51) + (0.0753510371f * fTemp50)) - (0.0311100706f * fTemp49)));
			float fTemp618 = (fConst182 * fRec1335[1]);
			fRec1337[0] = (fTemp617 + (fRec1337[1] + fTemp618));
			fRec1335[0] = fRec1337[0];
			float fRec1336 = (fTemp618 + fTemp617);
			float fTemp619 = (fConst168 * (((6.40969984e-06f * fTemp54) + (0.0699535608f * fTemp57)) - (((3.11620011e-06f * fTemp58) + (0.0202828702f * fTemp55)) + (0.0506437719f * fTemp56))));
			float fTemp620 = (fConst169 * fRec1338[1]);
			float fTemp621 = (fConst170 * fRec1341[1]);
			fRec1343[0] = (fTemp619 + (fTemp620 + (fRec1343[1] + fTemp621)));
			fRec1341[0] = fRec1343[0];
			float fRec1342 = ((fTemp621 + fTemp620) + fTemp619);
			fRec1340[0] = (fRec1341[0] + fRec1340[1]);
			fRec1338[0] = fRec1340[0];
			float fRec1339 = fRec1342;
			fVec22[0] = ((0.0513112508f * fTemp62) + (fRec1300 + (fRec1315 + (fRec1327 + (fRec1336 + fRec1339)))));
			output28[i] = FAUSTFLOAT((0.999892831f * (fRec0[0] * fVec22[iConst183])));
			float fTemp622 = (fConst190 * (((0.0185239371f * fTemp20) + ((1.23627997e-05f * fTemp19) + ((((0.0149299689f * fTemp10) + (0.0169588905f * fTemp11)) + (0.00653368514f * fTemp12)) + (0.00376816979f * fTemp16)))) - (((0.00615781359f * fTemp15) + (((0.0320998356f * fTemp18) + (0.00355308014f * fTemp13)) + (0.00632324489f * fTemp14))) + (0.0258721821f * fTemp17))));
			float fTemp623 = (fConst191 * fRec1353[1]);
			float fTemp624 = (fConst192 * fRec1356[1]);
			fRec1358[0] = (fTemp622 + (fTemp623 + (fRec1358[1] + fTemp624)));
			fRec1356[0] = fRec1358[0];
			float fRec1357 = ((fTemp624 + fTemp623) + fTemp622);
			fRec1355[0] = (fRec1355[1] + fRec1356[0]);
			fRec1353[0] = fRec1355[0];
			float fRec1354 = fRec1357;
			float fTemp625 = ((fConst187 * fRec1347[1]) + fRec1354);
			float fTemp626 = (fConst188 * fRec1350[1]);
			fRec1352[0] = ((fTemp625 + fRec1352[1]) + fTemp626);
			fRec1350[0] = fRec1352[0];
			float fRec1351 = (fTemp625 + fTemp626);
			fRec1349[0] = (fRec1350[0] + fRec1349[1]);
			fRec1347[0] = fRec1349[0];
			float fRec1348 = fRec1351;
			float fTemp627 = (fConst185 * fRec1344[1]);
			fRec1346[0] = ((fRec1348 + fRec1346[1]) + fTemp627);
			fRec1344[0] = fRec1346[0];
			float fRec1345 = (fRec1348 + fTemp627);
			float fTemp628 = (fConst194 * fRec1359[1]);
			float fTemp629 = (fConst195 * fRec1362[1]);
			float fTemp630 = (fConst197 * (((0.00745327817f * fTemp29) + ((((0.031886138f * fTemp30) + (0.0128897782f * fTemp26)) + (0.00873026066f * fTemp32)) + (0.0151190162f * fTemp28))) - ((0.0184149817f * fTemp34) + (((0.0442753471f * fTemp31) + (0.00762186805f * fTemp27)) + (7.70930001e-06f * fTemp33)))));
			float fTemp631 = (fConst198 * fRec1365[1]);
			float fTemp632 = (fConst199 * fRec1368[1]);
			fRec1370[0] = (fTemp630 + (fTemp631 + (fRec1370[1] + fTemp632)));
			fRec1368[0] = fRec1370[0];
			float fRec1369 = ((fTemp632 + fTemp631) + fTemp630);
			fRec1367[0] = (fRec1368[0] + fRec1367[1]);
			fRec1365[0] = fRec1367[0];
			float fRec1366 = fRec1369;
			fRec1364[0] = (fTemp628 + (fTemp629 + (fRec1366 + fRec1364[1])));
			fRec1362[0] = fRec1364[0];
			float fRec1363 = (fTemp628 + (fRec1366 + fTemp629));
			fRec1361[0] = (fRec1362[0] + fRec1361[1]);
			fRec1359[0] = fRec1361[0];
			float fRec1360 = fRec1363;
			float fTemp633 = (fConst201 * fRec1371[1]);
			float fTemp634 = (fConst203 * (((0.0438416526f * fTemp43) + (((0.00361537864f * fTemp42) + (0.0225419905f * fTemp39)) + (0.00627214322f * fTemp40))) - ((2.67740006e-06f * fTemp44) + ((0.0427974351f * fTemp38) + (0.024716828f * fTemp41)))));
			float fTemp635 = (fConst204 * fRec1374[1]);
			float fTemp636 = (fConst205 * fRec1377[1]);
			fRec1379[0] = (fTemp634 + (fTemp635 + (fRec1379[1] + fTemp636)));
			fRec1377[0] = fRec1379[0];
			float fRec1378 = ((fTemp636 + fTemp635) + fTemp634);
			fRec1376[0] = (fRec1377[0] + fRec1376[1]);
			fRec1374[0] = fRec1376[0];
			float fRec1375 = fRec1378;
			fRec1373[0] = (fTemp633 + (fRec1375 + fRec1373[1]));
			fRec1371[0] = fRec1373[0];
			float fRec1372 = (fRec1375 + fTemp633);
			float fTemp637 = (fConst207 * (((0.0275351312f * fTemp51) + (0.0476932786f * fTemp50)) - (0.0310895834f * fTemp49)));
			float fTemp638 = (fConst208 * fRec1380[1]);
			fRec1382[0] = (fTemp637 + (fRec1382[1] + fTemp638));
			fRec1380[0] = fRec1382[0];
			float fRec1381 = (fTemp638 + fTemp637);
			float fTemp639 = (fConst210 * (((0.0435531959f * fTemp54) + (0.0251447037f * fTemp57)) - (((0.0253003445f * fTemp58) + (0.00716599543f * fTemp55)) + (0.043829482f * fTemp56))));
			float fTemp640 = (fConst211 * fRec1383[1]);
			float fTemp641 = (fConst212 * fRec1386[1]);
			fRec1388[0] = (fTemp639 + (fTemp640 + (fRec1388[1] + fTemp641)));
			fRec1386[0] = fRec1388[0];
			float fRec1387 = ((fTemp641 + fTemp640) + fTemp639);
			fRec1385[0] = (fRec1386[0] + fRec1385[1]);
			fRec1383[0] = fRec1385[0];
			float fRec1384 = fRec1387;
			output29[i] = FAUSTFLOAT((fRec0[0] * ((fRec1345 + (fRec1360 + (fRec1372 + (fRec1381 + fRec1384)))) + (0.0389937758f * fTemp62))));
			float fTemp642 = (fConst207 * (((0.055092264f * fTemp51) + (0.0350449346f * fTemp50)) - (0.0311054997f * fTemp49)));
			float fTemp643 = (fConst208 * fRec1389[1]);
			fRec1391[0] = (fTemp642 + (fRec1391[1] + fTemp643));
			fRec1389[0] = fRec1391[0];
			float fRec1390 = (fTemp643 + fTemp642);
			float fTemp644 = (fConst210 * ((0.0550222285f * fTemp54) - ((((0.0438246243f * fTemp58) + (0.0137371952f * fTemp55)) + (0.0253771991f * fTemp56)) + (0.0249779373f * fTemp57))));
			float fTemp645 = (fConst211 * fRec1392[1]);
			float fTemp646 = (fConst212 * fRec1395[1]);
			fRec1397[0] = (fTemp644 + (fTemp645 + (fRec1397[1] + fTemp646)));
			fRec1395[0] = fRec1397[0];
			float fRec1396 = ((fTemp646 + fTemp645) + fTemp644);
			fRec1394[0] = (fRec1394[1] + fRec1395[0]);
			fRec1392[0] = fRec1394[0];
			float fRec1393 = fRec1396;
			float fTemp647 = (fConst201 * fRec1398[1]);
			float fTemp648 = (fConst203 * ((((0.00859783869f * fTemp43) + (0.0225961804f * fTemp39)) + (0.024573572f * fTemp41)) - ((((0.0428675599f * fTemp38) + (8.81157976e-05f * fTemp42)) + (0.00282131881f * fTemp40)) + (0.0520746484f * fTemp44))));
			float fTemp649 = (fConst204 * fRec1401[1]);
			float fTemp650 = (fConst205 * fRec1404[1]);
			fRec1406[0] = (fTemp648 + (fTemp649 + (fRec1406[1] + fTemp650)));
			fRec1404[0] = fRec1406[0];
			float fRec1405 = ((fTemp650 + fTemp649) + fTemp648);
			fRec1403[0] = (fRec1403[1] + fRec1404[0]);
			fRec1401[0] = fRec1403[0];
			float fRec1402 = fRec1405;
			fRec1400[0] = ((fRec1400[1] + fTemp647) + fRec1402);
			fRec1398[0] = fRec1400[0];
			float fRec1399 = (fTemp647 + fRec1402);
			float fTemp651 = (fConst194 * fRec1407[1]);
			float fTemp652 = (fConst195 * fRec1410[1]);
			float fTemp653 = (fConst197 * (((((0.00409327727f * fTemp26) + (0.0150989648f * fTemp32)) + (0.00883676484f * fTemp28)) + (0.0441632979f * fTemp33)) - (((((0.0315445513f * fTemp30) + (0.000230624093f * fTemp31)) + (0.00179497129f * fTemp27)) + (0.00755251851f * fTemp29)) + (0.030378826f * fTemp34))));
			float fTemp654 = (fConst198 * fRec1413[1]);
			float fTemp655 = (fConst199 * fRec1416[1]);
			fRec1418[0] = (fTemp653 + (fTemp654 + (fRec1418[1] + fTemp655)));
			fRec1416[0] = fRec1418[0];
			float fRec1417 = ((fTemp655 + fTemp654) + fTemp653);
			fRec1415[0] = (fRec1415[1] + fRec1416[0]);
			fRec1413[0] = fRec1415[0];
			float fRec1414 = fRec1417;
			fRec1412[0] = ((fTemp651 + (fRec1412[1] + fTemp652)) + fRec1414);
			fRec1410[0] = fRec1412[0];
			float fRec1411 = ((fTemp652 + fTemp651) + fRec1414);
			fRec1409[0] = (fRec1409[1] + fRec1410[0]);
			fRec1407[0] = fRec1409[0];
			float fRec1408 = fRec1411;
			float fTemp656 = (fConst185 * fRec1419[1]);
			float fTemp657 = (fConst187 * fRec1422[1]);
			float fTemp658 = (fConst188 * fRec1425[1]);
			float fTemp659 = (fConst190 * (((0.00633678399f * fTemp17) + ((((0.0317356177f * fTemp18) + (0.0065918644f * fTemp12)) + (0.00188136951f * fTemp15)) + (0.0186356381f * fTemp20))) - ((0.01108725f * fTemp19) + (((((0.0338707082f * fTemp10) + (0.0060738991f * fTemp11)) + (0.000775189605f * fTemp13)) + (0.00633257488f * fTemp14)) + (0.00356448116f * fTemp16)))));
			float fTemp660 = (fConst191 * fRec1428[1]);
			float fTemp661 = (fConst192 * fRec1431[1]);
			fRec1433[0] = (fTemp659 + (fTemp660 + (fRec1433[1] + fTemp661)));
			fRec1431[0] = fRec1433[0];
			float fRec1432 = ((fTemp661 + fTemp660) + fTemp659);
			fRec1430[0] = (fRec1430[1] + fRec1431[0]);
			fRec1428[0] = fRec1430[0];
			float fRec1429 = fRec1432;
			fRec1427[0] = ((fTemp657 + (fRec1427[1] + fTemp658)) + fRec1429);
			fRec1425[0] = fRec1427[0];
			float fRec1426 = ((fTemp658 + fTemp657) + fRec1429);
			fRec1424[0] = (fRec1424[1] + fRec1425[0]);
			fRec1422[0] = fRec1424[0];
			float fRec1423 = fRec1426;
			fRec1421[0] = ((fRec1421[1] + fTemp656) + fRec1423);
			fRec1419[0] = fRec1421[0];
			float fRec1420 = (fTemp656 + fRec1423);
			output30[i] = FAUSTFLOAT((fRec0[0] * ((0.0451662317f * fTemp62) + ((((fRec1390 + fRec1393) + fRec1399) + fRec1408) + fRec1420))));
			fRec0[1] = fRec0[0];
			fRec17[1] = fRec17[0];
			fRec16[2] = fRec16[1];
			fRec16[1] = fRec16[0];
			fRec18[1] = fRec18[0];
			fRec19[2] = fRec19[1];
			fRec19[1] = fRec19[0];
			fRec20[2] = fRec20[1];
			fRec20[1] = fRec20[0];
			fRec21[2] = fRec21[1];
			fRec21[1] = fRec21[0];
			fRec22[2] = fRec22[1];
			fRec22[1] = fRec22[0];
			fRec23[2] = fRec23[1];
			fRec23[1] = fRec23[0];
			fRec24[2] = fRec24[1];
			fRec24[1] = fRec24[0];
			fRec25[2] = fRec25[1];
			fRec25[1] = fRec25[0];
			fRec26[2] = fRec26[1];
			fRec26[1] = fRec26[0];
			fRec27[2] = fRec27[1];
			fRec27[1] = fRec27[0];
			fRec28[2] = fRec28[1];
			fRec28[1] = fRec28[0];
			fRec15[1] = fRec15[0];
			fRec13[1] = fRec13[0];
			fRec12[1] = fRec12[0];
			fRec10[1] = fRec10[0];
			fRec9[1] = fRec9[0];
			fRec7[1] = fRec7[0];
			fRec6[1] = fRec6[0];
			fRec4[1] = fRec4[0];
			fRec3[1] = fRec3[0];
			fRec1[1] = fRec1[0];
			fRec41[2] = fRec41[1];
			fRec41[1] = fRec41[0];
			fRec42[2] = fRec42[1];
			fRec42[1] = fRec42[0];
			fRec43[2] = fRec43[1];
			fRec43[1] = fRec43[0];
			fRec44[2] = fRec44[1];
			fRec44[1] = fRec44[0];
			fRec45[2] = fRec45[1];
			fRec45[1] = fRec45[0];
			fRec46[2] = fRec46[1];
			fRec46[1] = fRec46[0];
			fRec47[2] = fRec47[1];
			fRec47[1] = fRec47[0];
			fRec48[2] = fRec48[1];
			fRec48[1] = fRec48[0];
			fRec49[2] = fRec49[1];
			fRec49[1] = fRec49[0];
			fRec40[1] = fRec40[0];
			fRec38[1] = fRec38[0];
			fRec37[1] = fRec37[0];
			fRec35[1] = fRec35[0];
			fRec34[1] = fRec34[0];
			fRec32[1] = fRec32[0];
			fRec31[1] = fRec31[0];
			fRec29[1] = fRec29[0];
			fRec59[2] = fRec59[1];
			fRec59[1] = fRec59[0];
			fRec60[2] = fRec60[1];
			fRec60[1] = fRec60[0];
			fRec61[2] = fRec61[1];
			fRec61[1] = fRec61[0];
			fRec62[2] = fRec62[1];
			fRec62[1] = fRec62[0];
			fRec63[2] = fRec63[1];
			fRec63[1] = fRec63[0];
			fRec64[2] = fRec64[1];
			fRec64[1] = fRec64[0];
			fRec65[2] = fRec65[1];
			fRec65[1] = fRec65[0];
			fRec58[1] = fRec58[0];
			fRec56[1] = fRec56[0];
			fRec55[1] = fRec55[0];
			fRec53[1] = fRec53[0];
			fRec52[1] = fRec52[0];
			fRec50[1] = fRec50[0];
			fRec69[2] = fRec69[1];
			fRec69[1] = fRec69[0];
			fRec70[2] = fRec70[1];
			fRec70[1] = fRec70[0];
			fRec71[2] = fRec71[1];
			fRec71[1] = fRec71[0];
			fRec68[1] = fRec68[0];
			fRec66[1] = fRec66[0];
			fRec78[2] = fRec78[1];
			fRec78[1] = fRec78[0];
			fRec79[2] = fRec79[1];
			fRec79[1] = fRec79[0];
			fRec80[2] = fRec80[1];
			fRec80[1] = fRec80[0];
			fRec81[2] = fRec81[1];
			fRec81[1] = fRec81[0];
			fRec82[2] = fRec82[1];
			fRec82[1] = fRec82[0];
			fRec77[1] = fRec77[0];
			fRec75[1] = fRec75[0];
			fRec74[1] = fRec74[0];
			fRec72[1] = fRec72[0];
			fRec83[2] = fRec83[1];
			fRec83[1] = fRec83[0];
			IOTA = (IOTA + 1);
			fRec98[1] = fRec98[0];
			fRec96[1] = fRec96[0];
			fRec95[1] = fRec95[0];
			fRec93[1] = fRec93[0];
			fRec92[1] = fRec92[0];
			fRec90[1] = fRec90[0];
			fRec89[1] = fRec89[0];
			fRec87[1] = fRec87[0];
			fRec86[1] = fRec86[0];
			fRec84[1] = fRec84[0];
			fRec110[1] = fRec110[0];
			fRec108[1] = fRec108[0];
			fRec107[1] = fRec107[0];
			fRec105[1] = fRec105[0];
			fRec104[1] = fRec104[0];
			fRec102[1] = fRec102[0];
			fRec101[1] = fRec101[0];
			fRec99[1] = fRec99[0];
			fRec119[1] = fRec119[0];
			fRec117[1] = fRec117[0];
			fRec116[1] = fRec116[0];
			fRec114[1] = fRec114[0];
			fRec113[1] = fRec113[0];
			fRec111[1] = fRec111[0];
			fRec122[1] = fRec122[0];
			fRec120[1] = fRec120[0];
			fRec128[1] = fRec128[0];
			fRec126[1] = fRec126[0];
			fRec125[1] = fRec125[0];
			fRec123[1] = fRec123[0];
			fRec137[1] = fRec137[0];
			fRec135[1] = fRec135[0];
			fRec134[1] = fRec134[0];
			fRec132[1] = fRec132[0];
			fRec131[1] = fRec131[0];
			fRec129[1] = fRec129[0];
			fRec143[1] = fRec143[0];
			fRec141[1] = fRec141[0];
			fRec140[1] = fRec140[0];
			fRec138[1] = fRec138[0];
			fRec146[1] = fRec146[0];
			fRec144[1] = fRec144[0];
			fRec158[1] = fRec158[0];
			fRec156[1] = fRec156[0];
			fRec155[1] = fRec155[0];
			fRec153[1] = fRec153[0];
			fRec152[1] = fRec152[0];
			fRec150[1] = fRec150[0];
			fRec149[1] = fRec149[0];
			fRec147[1] = fRec147[0];
			fRec173[1] = fRec173[0];
			fRec171[1] = fRec171[0];
			fRec170[1] = fRec170[0];
			fRec168[1] = fRec168[0];
			fRec167[1] = fRec167[0];
			fRec165[1] = fRec165[0];
			fRec164[1] = fRec164[0];
			fRec162[1] = fRec162[0];
			fRec161[1] = fRec161[0];
			fRec159[1] = fRec159[0];
			fRec188[1] = fRec188[0];
			fRec186[1] = fRec186[0];
			fRec185[1] = fRec185[0];
			fRec183[1] = fRec183[0];
			fRec182[1] = fRec182[0];
			fRec180[1] = fRec180[0];
			fRec179[1] = fRec179[0];
			fRec177[1] = fRec177[0];
			fRec176[1] = fRec176[0];
			fRec174[1] = fRec174[0];
			fRec200[1] = fRec200[0];
			fRec198[1] = fRec198[0];
			fRec197[1] = fRec197[0];
			fRec195[1] = fRec195[0];
			fRec194[1] = fRec194[0];
			fRec192[1] = fRec192[0];
			fRec191[1] = fRec191[0];
			fRec189[1] = fRec189[0];
			fRec209[1] = fRec209[0];
			fRec207[1] = fRec207[0];
			fRec206[1] = fRec206[0];
			fRec204[1] = fRec204[0];
			fRec203[1] = fRec203[0];
			fRec201[1] = fRec201[0];
			fRec212[1] = fRec212[0];
			fRec210[1] = fRec210[0];
			fRec218[1] = fRec218[0];
			fRec216[1] = fRec216[0];
			fRec215[1] = fRec215[0];
			fRec213[1] = fRec213[0];
			fRec233[1] = fRec233[0];
			fRec231[1] = fRec231[0];
			fRec230[1] = fRec230[0];
			fRec228[1] = fRec228[0];
			fRec227[1] = fRec227[0];
			fRec225[1] = fRec225[0];
			fRec224[1] = fRec224[0];
			fRec222[1] = fRec222[0];
			fRec221[1] = fRec221[0];
			fRec219[1] = fRec219[0];
			fRec245[1] = fRec245[0];
			fRec243[1] = fRec243[0];
			fRec242[1] = fRec242[0];
			fRec240[1] = fRec240[0];
			fRec239[1] = fRec239[0];
			fRec237[1] = fRec237[0];
			fRec236[1] = fRec236[0];
			fRec234[1] = fRec234[0];
			fRec254[1] = fRec254[0];
			fRec252[1] = fRec252[0];
			fRec251[1] = fRec251[0];
			fRec249[1] = fRec249[0];
			fRec248[1] = fRec248[0];
			fRec246[1] = fRec246[0];
			fRec257[1] = fRec257[0];
			fRec255[1] = fRec255[0];
			fRec263[1] = fRec263[0];
			fRec261[1] = fRec261[0];
			fRec260[1] = fRec260[0];
			fRec258[1] = fRec258[0];
			fRec278[1] = fRec278[0];
			fRec276[1] = fRec276[0];
			fRec275[1] = fRec275[0];
			fRec273[1] = fRec273[0];
			fRec272[1] = fRec272[0];
			fRec270[1] = fRec270[0];
			fRec269[1] = fRec269[0];
			fRec267[1] = fRec267[0];
			fRec266[1] = fRec266[0];
			fRec264[1] = fRec264[0];
			fRec290[1] = fRec290[0];
			fRec288[1] = fRec288[0];
			fRec287[1] = fRec287[0];
			fRec285[1] = fRec285[0];
			fRec284[1] = fRec284[0];
			fRec282[1] = fRec282[0];
			fRec281[1] = fRec281[0];
			fRec279[1] = fRec279[0];
			fRec299[1] = fRec299[0];
			fRec297[1] = fRec297[0];
			fRec296[1] = fRec296[0];
			fRec294[1] = fRec294[0];
			fRec293[1] = fRec293[0];
			fRec291[1] = fRec291[0];
			fRec302[1] = fRec302[0];
			fRec300[1] = fRec300[0];
			fRec308[1] = fRec308[0];
			fRec306[1] = fRec306[0];
			fRec305[1] = fRec305[0];
			fRec303[1] = fRec303[0];
			fRec320[1] = fRec320[0];
			fRec318[1] = fRec318[0];
			fRec317[1] = fRec317[0];
			fRec315[1] = fRec315[0];
			fRec314[1] = fRec314[0];
			fRec312[1] = fRec312[0];
			fRec311[1] = fRec311[0];
			fRec309[1] = fRec309[0];
			fRec329[1] = fRec329[0];
			fRec327[1] = fRec327[0];
			fRec326[1] = fRec326[0];
			fRec324[1] = fRec324[0];
			fRec323[1] = fRec323[0];
			fRec321[1] = fRec321[0];
			fRec332[1] = fRec332[0];
			fRec330[1] = fRec330[0];
			fRec338[1] = fRec338[0];
			fRec336[1] = fRec336[0];
			fRec335[1] = fRec335[0];
			fRec333[1] = fRec333[0];
			fRec353[1] = fRec353[0];
			fRec351[1] = fRec351[0];
			fRec350[1] = fRec350[0];
			fRec348[1] = fRec348[0];
			fRec347[1] = fRec347[0];
			fRec345[1] = fRec345[0];
			fRec344[1] = fRec344[0];
			fRec342[1] = fRec342[0];
			fRec341[1] = fRec341[0];
			fRec339[1] = fRec339[0];
			fRec356[1] = fRec356[0];
			fRec354[1] = fRec354[0];
			fRec362[1] = fRec362[0];
			fRec360[1] = fRec360[0];
			fRec359[1] = fRec359[0];
			fRec357[1] = fRec357[0];
			fRec371[1] = fRec371[0];
			fRec369[1] = fRec369[0];
			fRec368[1] = fRec368[0];
			fRec366[1] = fRec366[0];
			fRec365[1] = fRec365[0];
			fRec363[1] = fRec363[0];
			fRec383[1] = fRec383[0];
			fRec381[1] = fRec381[0];
			fRec380[1] = fRec380[0];
			fRec378[1] = fRec378[0];
			fRec377[1] = fRec377[0];
			fRec375[1] = fRec375[0];
			fRec374[1] = fRec374[0];
			fRec372[1] = fRec372[0];
			fRec398[1] = fRec398[0];
			fRec396[1] = fRec396[0];
			fRec395[1] = fRec395[0];
			fRec393[1] = fRec393[0];
			fRec392[1] = fRec392[0];
			fRec390[1] = fRec390[0];
			fRec389[1] = fRec389[0];
			fRec387[1] = fRec387[0];
			fRec386[1] = fRec386[0];
			fRec384[1] = fRec384[0];
			fRec407[1] = fRec407[0];
			fRec405[1] = fRec405[0];
			fRec404[1] = fRec404[0];
			fRec402[1] = fRec402[0];
			fRec401[1] = fRec401[0];
			fRec399[1] = fRec399[0];
			fRec419[1] = fRec419[0];
			fRec417[1] = fRec417[0];
			fRec416[1] = fRec416[0];
			fRec414[1] = fRec414[0];
			fRec413[1] = fRec413[0];
			fRec411[1] = fRec411[0];
			fRec410[1] = fRec410[0];
			fRec408[1] = fRec408[0];
			fRec422[1] = fRec422[0];
			fRec420[1] = fRec420[0];
			fRec428[1] = fRec428[0];
			fRec426[1] = fRec426[0];
			fRec425[1] = fRec425[0];
			fRec423[1] = fRec423[0];
			fRec443[1] = fRec443[0];
			fRec441[1] = fRec441[0];
			fRec440[1] = fRec440[0];
			fRec438[1] = fRec438[0];
			fRec437[1] = fRec437[0];
			fRec435[1] = fRec435[0];
			fRec434[1] = fRec434[0];
			fRec432[1] = fRec432[0];
			fRec431[1] = fRec431[0];
			fRec429[1] = fRec429[0];
			fRec446[1] = fRec446[0];
			fRec444[1] = fRec444[0];
			fRec452[1] = fRec452[0];
			fRec450[1] = fRec450[0];
			fRec449[1] = fRec449[0];
			fRec447[1] = fRec447[0];
			fRec461[1] = fRec461[0];
			fRec459[1] = fRec459[0];
			fRec458[1] = fRec458[0];
			fRec456[1] = fRec456[0];
			fRec455[1] = fRec455[0];
			fRec453[1] = fRec453[0];
			fRec473[1] = fRec473[0];
			fRec471[1] = fRec471[0];
			fRec470[1] = fRec470[0];
			fRec468[1] = fRec468[0];
			fRec467[1] = fRec467[0];
			fRec465[1] = fRec465[0];
			fRec464[1] = fRec464[0];
			fRec462[1] = fRec462[0];
			fRec488[1] = fRec488[0];
			fRec486[1] = fRec486[0];
			fRec485[1] = fRec485[0];
			fRec483[1] = fRec483[0];
			fRec482[1] = fRec482[0];
			fRec480[1] = fRec480[0];
			fRec479[1] = fRec479[0];
			fRec477[1] = fRec477[0];
			fRec476[1] = fRec476[0];
			fRec474[1] = fRec474[0];
			fRec491[1] = fRec491[0];
			fRec489[1] = fRec489[0];
			fRec497[1] = fRec497[0];
			fRec495[1] = fRec495[0];
			fRec494[1] = fRec494[0];
			fRec492[1] = fRec492[0];
			fRec506[1] = fRec506[0];
			fRec504[1] = fRec504[0];
			fRec503[1] = fRec503[0];
			fRec501[1] = fRec501[0];
			fRec500[1] = fRec500[0];
			fRec498[1] = fRec498[0];
			fRec518[1] = fRec518[0];
			fRec516[1] = fRec516[0];
			fRec515[1] = fRec515[0];
			fRec513[1] = fRec513[0];
			fRec512[1] = fRec512[0];
			fRec510[1] = fRec510[0];
			fRec509[1] = fRec509[0];
			fRec507[1] = fRec507[0];
			fRec533[1] = fRec533[0];
			fRec531[1] = fRec531[0];
			fRec530[1] = fRec530[0];
			fRec528[1] = fRec528[0];
			fRec527[1] = fRec527[0];
			fRec525[1] = fRec525[0];
			fRec524[1] = fRec524[0];
			fRec522[1] = fRec522[0];
			fRec521[1] = fRec521[0];
			fRec519[1] = fRec519[0];
			fRec548[1] = fRec548[0];
			fRec546[1] = fRec546[0];
			fRec545[1] = fRec545[0];
			fRec543[1] = fRec543[0];
			fRec542[1] = fRec542[0];
			fRec540[1] = fRec540[0];
			fRec539[1] = fRec539[0];
			fRec537[1] = fRec537[0];
			fRec536[1] = fRec536[0];
			fRec534[1] = fRec534[0];
			fRec560[1] = fRec560[0];
			fRec558[1] = fRec558[0];
			fRec557[1] = fRec557[0];
			fRec555[1] = fRec555[0];
			fRec554[1] = fRec554[0];
			fRec552[1] = fRec552[0];
			fRec551[1] = fRec551[0];
			fRec549[1] = fRec549[0];
			fRec569[1] = fRec569[0];
			fRec567[1] = fRec567[0];
			fRec566[1] = fRec566[0];
			fRec564[1] = fRec564[0];
			fRec563[1] = fRec563[0];
			fRec561[1] = fRec561[0];
			fRec572[1] = fRec572[0];
			fRec570[1] = fRec570[0];
			fRec578[1] = fRec578[0];
			fRec576[1] = fRec576[0];
			fRec575[1] = fRec575[0];
			fRec573[1] = fRec573[0];
			fRec593[1] = fRec593[0];
			fRec591[1] = fRec591[0];
			fRec590[1] = fRec590[0];
			fRec588[1] = fRec588[0];
			fRec587[1] = fRec587[0];
			fRec585[1] = fRec585[0];
			fRec584[1] = fRec584[0];
			fRec582[1] = fRec582[0];
			fRec581[1] = fRec581[0];
			fRec579[1] = fRec579[0];
			fRec605[1] = fRec605[0];
			fRec603[1] = fRec603[0];
			fRec602[1] = fRec602[0];
			fRec600[1] = fRec600[0];
			fRec599[1] = fRec599[0];
			fRec597[1] = fRec597[0];
			fRec596[1] = fRec596[0];
			fRec594[1] = fRec594[0];
			fRec614[1] = fRec614[0];
			fRec612[1] = fRec612[0];
			fRec611[1] = fRec611[0];
			fRec609[1] = fRec609[0];
			fRec608[1] = fRec608[0];
			fRec606[1] = fRec606[0];
			fRec617[1] = fRec617[0];
			fRec615[1] = fRec615[0];
			fRec623[1] = fRec623[0];
			fRec621[1] = fRec621[0];
			fRec620[1] = fRec620[0];
			fRec618[1] = fRec618[0];
			fRec638[1] = fRec638[0];
			fRec636[1] = fRec636[0];
			fRec635[1] = fRec635[0];
			fRec633[1] = fRec633[0];
			fRec632[1] = fRec632[0];
			fRec630[1] = fRec630[0];
			fRec629[1] = fRec629[0];
			fRec627[1] = fRec627[0];
			fRec626[1] = fRec626[0];
			fRec624[1] = fRec624[0];
			fRec650[1] = fRec650[0];
			fRec648[1] = fRec648[0];
			fRec647[1] = fRec647[0];
			fRec645[1] = fRec645[0];
			fRec644[1] = fRec644[0];
			fRec642[1] = fRec642[0];
			fRec641[1] = fRec641[0];
			fRec639[1] = fRec639[0];
			fRec659[1] = fRec659[0];
			fRec657[1] = fRec657[0];
			fRec656[1] = fRec656[0];
			fRec654[1] = fRec654[0];
			fRec653[1] = fRec653[0];
			fRec651[1] = fRec651[0];
			fRec662[1] = fRec662[0];
			fRec660[1] = fRec660[0];
			fRec668[1] = fRec668[0];
			fRec666[1] = fRec666[0];
			fRec665[1] = fRec665[0];
			fRec663[1] = fRec663[0];
			fRec683[1] = fRec683[0];
			fRec681[1] = fRec681[0];
			fRec680[1] = fRec680[0];
			fRec678[1] = fRec678[0];
			fRec677[1] = fRec677[0];
			fRec675[1] = fRec675[0];
			fRec674[1] = fRec674[0];
			fRec672[1] = fRec672[0];
			fRec671[1] = fRec671[0];
			fRec669[1] = fRec669[0];
			fRec695[1] = fRec695[0];
			fRec693[1] = fRec693[0];
			fRec692[1] = fRec692[0];
			fRec690[1] = fRec690[0];
			fRec689[1] = fRec689[0];
			fRec687[1] = fRec687[0];
			fRec686[1] = fRec686[0];
			fRec684[1] = fRec684[0];
			fRec704[1] = fRec704[0];
			fRec702[1] = fRec702[0];
			fRec701[1] = fRec701[0];
			fRec699[1] = fRec699[0];
			fRec698[1] = fRec698[0];
			fRec696[1] = fRec696[0];
			fRec707[1] = fRec707[0];
			fRec705[1] = fRec705[0];
			fRec713[1] = fRec713[0];
			fRec711[1] = fRec711[0];
			fRec710[1] = fRec710[0];
			fRec708[1] = fRec708[0];
			fRec728[1] = fRec728[0];
			fRec726[1] = fRec726[0];
			fRec725[1] = fRec725[0];
			fRec723[1] = fRec723[0];
			fRec722[1] = fRec722[0];
			fRec720[1] = fRec720[0];
			fRec719[1] = fRec719[0];
			fRec717[1] = fRec717[0];
			fRec716[1] = fRec716[0];
			fRec714[1] = fRec714[0];
			fRec740[1] = fRec740[0];
			fRec738[1] = fRec738[0];
			fRec737[1] = fRec737[0];
			fRec735[1] = fRec735[0];
			fRec734[1] = fRec734[0];
			fRec732[1] = fRec732[0];
			fRec731[1] = fRec731[0];
			fRec729[1] = fRec729[0];
			fRec749[1] = fRec749[0];
			fRec747[1] = fRec747[0];
			fRec746[1] = fRec746[0];
			fRec744[1] = fRec744[0];
			fRec743[1] = fRec743[0];
			fRec741[1] = fRec741[0];
			fRec752[1] = fRec752[0];
			fRec750[1] = fRec750[0];
			fRec758[1] = fRec758[0];
			fRec756[1] = fRec756[0];
			fRec755[1] = fRec755[0];
			fRec753[1] = fRec753[0];
			fRec773[1] = fRec773[0];
			fRec771[1] = fRec771[0];
			fRec770[1] = fRec770[0];
			fRec768[1] = fRec768[0];
			fRec767[1] = fRec767[0];
			fRec765[1] = fRec765[0];
			fRec764[1] = fRec764[0];
			fRec762[1] = fRec762[0];
			fRec761[1] = fRec761[0];
			fRec759[1] = fRec759[0];
			fRec785[1] = fRec785[0];
			fRec783[1] = fRec783[0];
			fRec782[1] = fRec782[0];
			fRec780[1] = fRec780[0];
			fRec779[1] = fRec779[0];
			fRec777[1] = fRec777[0];
			fRec776[1] = fRec776[0];
			fRec774[1] = fRec774[0];
			fRec794[1] = fRec794[0];
			fRec792[1] = fRec792[0];
			fRec791[1] = fRec791[0];
			fRec789[1] = fRec789[0];
			fRec788[1] = fRec788[0];
			fRec786[1] = fRec786[0];
			fRec797[1] = fRec797[0];
			fRec795[1] = fRec795[0];
			fRec803[1] = fRec803[0];
			fRec801[1] = fRec801[0];
			fRec800[1] = fRec800[0];
			fRec798[1] = fRec798[0];
			fRec818[1] = fRec818[0];
			fRec816[1] = fRec816[0];
			fRec815[1] = fRec815[0];
			fRec813[1] = fRec813[0];
			fRec812[1] = fRec812[0];
			fRec810[1] = fRec810[0];
			fRec809[1] = fRec809[0];
			fRec807[1] = fRec807[0];
			fRec806[1] = fRec806[0];
			fRec804[1] = fRec804[0];
			fRec830[1] = fRec830[0];
			fRec828[1] = fRec828[0];
			fRec827[1] = fRec827[0];
			fRec825[1] = fRec825[0];
			fRec824[1] = fRec824[0];
			fRec822[1] = fRec822[0];
			fRec821[1] = fRec821[0];
			fRec819[1] = fRec819[0];
			fRec839[1] = fRec839[0];
			fRec837[1] = fRec837[0];
			fRec836[1] = fRec836[0];
			fRec834[1] = fRec834[0];
			fRec833[1] = fRec833[0];
			fRec831[1] = fRec831[0];
			fRec842[1] = fRec842[0];
			fRec840[1] = fRec840[0];
			fRec848[1] = fRec848[0];
			fRec846[1] = fRec846[0];
			fRec845[1] = fRec845[0];
			fRec843[1] = fRec843[0];
			fRec863[1] = fRec863[0];
			fRec861[1] = fRec861[0];
			fRec860[1] = fRec860[0];
			fRec858[1] = fRec858[0];
			fRec857[1] = fRec857[0];
			fRec855[1] = fRec855[0];
			fRec854[1] = fRec854[0];
			fRec852[1] = fRec852[0];
			fRec851[1] = fRec851[0];
			fRec849[1] = fRec849[0];
			fRec875[1] = fRec875[0];
			fRec873[1] = fRec873[0];
			fRec872[1] = fRec872[0];
			fRec870[1] = fRec870[0];
			fRec869[1] = fRec869[0];
			fRec867[1] = fRec867[0];
			fRec866[1] = fRec866[0];
			fRec864[1] = fRec864[0];
			fRec884[1] = fRec884[0];
			fRec882[1] = fRec882[0];
			fRec881[1] = fRec881[0];
			fRec879[1] = fRec879[0];
			fRec878[1] = fRec878[0];
			fRec876[1] = fRec876[0];
			fRec887[1] = fRec887[0];
			fRec885[1] = fRec885[0];
			fRec893[1] = fRec893[0];
			fRec891[1] = fRec891[0];
			fRec890[1] = fRec890[0];
			fRec888[1] = fRec888[0];
			fRec905[1] = fRec905[0];
			fRec903[1] = fRec903[0];
			fRec902[1] = fRec902[0];
			fRec900[1] = fRec900[0];
			fRec899[1] = fRec899[0];
			fRec897[1] = fRec897[0];
			fRec896[1] = fRec896[0];
			fRec894[1] = fRec894[0];
			fRec914[1] = fRec914[0];
			fRec912[1] = fRec912[0];
			fRec911[1] = fRec911[0];
			fRec909[1] = fRec909[0];
			fRec908[1] = fRec908[0];
			fRec906[1] = fRec906[0];
			fRec920[1] = fRec920[0];
			fRec918[1] = fRec918[0];
			fRec917[1] = fRec917[0];
			fRec915[1] = fRec915[0];
			fRec935[1] = fRec935[0];
			fRec933[1] = fRec933[0];
			fRec932[1] = fRec932[0];
			fRec930[1] = fRec930[0];
			fRec929[1] = fRec929[0];
			fRec927[1] = fRec927[0];
			fRec926[1] = fRec926[0];
			fRec924[1] = fRec924[0];
			fRec923[1] = fRec923[0];
			fRec921[1] = fRec921[0];
			fRec938[1] = fRec938[0];
			fRec936[1] = fRec936[0];
			fVec19[1] = fVec19[0];
			fRec953[1] = fRec953[0];
			fRec951[1] = fRec951[0];
			fRec950[1] = fRec950[0];
			fRec948[1] = fRec948[0];
			fRec947[1] = fRec947[0];
			fRec945[1] = fRec945[0];
			fRec944[1] = fRec944[0];
			fRec942[1] = fRec942[0];
			fRec941[1] = fRec941[0];
			fRec939[1] = fRec939[0];
			fRec965[1] = fRec965[0];
			fRec963[1] = fRec963[0];
			fRec962[1] = fRec962[0];
			fRec960[1] = fRec960[0];
			fRec959[1] = fRec959[0];
			fRec957[1] = fRec957[0];
			fRec956[1] = fRec956[0];
			fRec954[1] = fRec954[0];
			fRec974[1] = fRec974[0];
			fRec972[1] = fRec972[0];
			fRec971[1] = fRec971[0];
			fRec969[1] = fRec969[0];
			fRec968[1] = fRec968[0];
			fRec966[1] = fRec966[0];
			fRec977[1] = fRec977[0];
			fRec975[1] = fRec975[0];
			fRec983[1] = fRec983[0];
			fRec981[1] = fRec981[0];
			fRec980[1] = fRec980[0];
			fRec978[1] = fRec978[0];
			fRec998[1] = fRec998[0];
			fRec996[1] = fRec996[0];
			fRec995[1] = fRec995[0];
			fRec993[1] = fRec993[0];
			fRec992[1] = fRec992[0];
			fRec990[1] = fRec990[0];
			fRec989[1] = fRec989[0];
			fRec987[1] = fRec987[0];
			fRec986[1] = fRec986[0];
			fRec984[1] = fRec984[0];
			fRec1010[1] = fRec1010[0];
			fRec1008[1] = fRec1008[0];
			fRec1007[1] = fRec1007[0];
			fRec1005[1] = fRec1005[0];
			fRec1004[1] = fRec1004[0];
			fRec1002[1] = fRec1002[0];
			fRec1001[1] = fRec1001[0];
			fRec999[1] = fRec999[0];
			fRec1019[1] = fRec1019[0];
			fRec1017[1] = fRec1017[0];
			fRec1016[1] = fRec1016[0];
			fRec1014[1] = fRec1014[0];
			fRec1013[1] = fRec1013[0];
			fRec1011[1] = fRec1011[0];
			fRec1022[1] = fRec1022[0];
			fRec1020[1] = fRec1020[0];
			fRec1028[1] = fRec1028[0];
			fRec1026[1] = fRec1026[0];
			fRec1025[1] = fRec1025[0];
			fRec1023[1] = fRec1023[0];
			fRec1034[1] = fRec1034[0];
			fRec1032[1] = fRec1032[0];
			fRec1031[1] = fRec1031[0];
			fRec1029[1] = fRec1029[0];
			fRec1037[1] = fRec1037[0];
			fRec1035[1] = fRec1035[0];
			fRec1052[1] = fRec1052[0];
			fRec1050[1] = fRec1050[0];
			fRec1049[1] = fRec1049[0];
			fRec1047[1] = fRec1047[0];
			fRec1046[1] = fRec1046[0];
			fRec1044[1] = fRec1044[0];
			fRec1043[1] = fRec1043[0];
			fRec1041[1] = fRec1041[0];
			fRec1040[1] = fRec1040[0];
			fRec1038[1] = fRec1038[0];
			fRec1061[1] = fRec1061[0];
			fRec1059[1] = fRec1059[0];
			fRec1058[1] = fRec1058[0];
			fRec1056[1] = fRec1056[0];
			fRec1055[1] = fRec1055[0];
			fRec1053[1] = fRec1053[0];
			fRec1073[1] = fRec1073[0];
			fRec1071[1] = fRec1071[0];
			fRec1070[1] = fRec1070[0];
			fRec1068[1] = fRec1068[0];
			fRec1067[1] = fRec1067[0];
			fRec1065[1] = fRec1065[0];
			fRec1064[1] = fRec1064[0];
			fRec1062[1] = fRec1062[0];
			fVec20[1] = fVec20[0];
			fRec1088[1] = fRec1088[0];
			fRec1086[1] = fRec1086[0];
			fRec1085[1] = fRec1085[0];
			fRec1083[1] = fRec1083[0];
			fRec1082[1] = fRec1082[0];
			fRec1080[1] = fRec1080[0];
			fRec1079[1] = fRec1079[0];
			fRec1077[1] = fRec1077[0];
			fRec1076[1] = fRec1076[0];
			fRec1074[1] = fRec1074[0];
			fRec1100[1] = fRec1100[0];
			fRec1098[1] = fRec1098[0];
			fRec1097[1] = fRec1097[0];
			fRec1095[1] = fRec1095[0];
			fRec1094[1] = fRec1094[0];
			fRec1092[1] = fRec1092[0];
			fRec1091[1] = fRec1091[0];
			fRec1089[1] = fRec1089[0];
			fRec1109[1] = fRec1109[0];
			fRec1107[1] = fRec1107[0];
			fRec1106[1] = fRec1106[0];
			fRec1104[1] = fRec1104[0];
			fRec1103[1] = fRec1103[0];
			fRec1101[1] = fRec1101[0];
			fRec1112[1] = fRec1112[0];
			fRec1110[1] = fRec1110[0];
			fRec1118[1] = fRec1118[0];
			fRec1116[1] = fRec1116[0];
			fRec1115[1] = fRec1115[0];
			fRec1113[1] = fRec1113[0];
			fRec1133[1] = fRec1133[0];
			fRec1131[1] = fRec1131[0];
			fRec1130[1] = fRec1130[0];
			fRec1128[1] = fRec1128[0];
			fRec1127[1] = fRec1127[0];
			fRec1125[1] = fRec1125[0];
			fRec1124[1] = fRec1124[0];
			fRec1122[1] = fRec1122[0];
			fRec1121[1] = fRec1121[0];
			fRec1119[1] = fRec1119[0];
			fRec1145[1] = fRec1145[0];
			fRec1143[1] = fRec1143[0];
			fRec1142[1] = fRec1142[0];
			fRec1140[1] = fRec1140[0];
			fRec1139[1] = fRec1139[0];
			fRec1137[1] = fRec1137[0];
			fRec1136[1] = fRec1136[0];
			fRec1134[1] = fRec1134[0];
			fRec1154[1] = fRec1154[0];
			fRec1152[1] = fRec1152[0];
			fRec1151[1] = fRec1151[0];
			fRec1149[1] = fRec1149[0];
			fRec1148[1] = fRec1148[0];
			fRec1146[1] = fRec1146[0];
			fRec1157[1] = fRec1157[0];
			fRec1155[1] = fRec1155[0];
			fRec1163[1] = fRec1163[0];
			fRec1161[1] = fRec1161[0];
			fRec1160[1] = fRec1160[0];
			fRec1158[1] = fRec1158[0];
			fRec1178[1] = fRec1178[0];
			fRec1176[1] = fRec1176[0];
			fRec1175[1] = fRec1175[0];
			fRec1173[1] = fRec1173[0];
			fRec1172[1] = fRec1172[0];
			fRec1170[1] = fRec1170[0];
			fRec1169[1] = fRec1169[0];
			fRec1167[1] = fRec1167[0];
			fRec1166[1] = fRec1166[0];
			fRec1164[1] = fRec1164[0];
			fRec1190[1] = fRec1190[0];
			fRec1188[1] = fRec1188[0];
			fRec1187[1] = fRec1187[0];
			fRec1185[1] = fRec1185[0];
			fRec1184[1] = fRec1184[0];
			fRec1182[1] = fRec1182[0];
			fRec1181[1] = fRec1181[0];
			fRec1179[1] = fRec1179[0];
			fRec1199[1] = fRec1199[0];
			fRec1197[1] = fRec1197[0];
			fRec1196[1] = fRec1196[0];
			fRec1194[1] = fRec1194[0];
			fRec1193[1] = fRec1193[0];
			fRec1191[1] = fRec1191[0];
			fRec1202[1] = fRec1202[0];
			fRec1200[1] = fRec1200[0];
			fRec1208[1] = fRec1208[0];
			fRec1206[1] = fRec1206[0];
			fRec1205[1] = fRec1205[0];
			fRec1203[1] = fRec1203[0];
			fVec21[1] = fVec21[0];
			fRec1223[1] = fRec1223[0];
			fRec1221[1] = fRec1221[0];
			fRec1220[1] = fRec1220[0];
			fRec1218[1] = fRec1218[0];
			fRec1217[1] = fRec1217[0];
			fRec1215[1] = fRec1215[0];
			fRec1214[1] = fRec1214[0];
			fRec1212[1] = fRec1212[0];
			fRec1211[1] = fRec1211[0];
			fRec1209[1] = fRec1209[0];
			fRec1235[1] = fRec1235[0];
			fRec1233[1] = fRec1233[0];
			fRec1232[1] = fRec1232[0];
			fRec1230[1] = fRec1230[0];
			fRec1229[1] = fRec1229[0];
			fRec1227[1] = fRec1227[0];
			fRec1226[1] = fRec1226[0];
			fRec1224[1] = fRec1224[0];
			fRec1244[1] = fRec1244[0];
			fRec1242[1] = fRec1242[0];
			fRec1241[1] = fRec1241[0];
			fRec1239[1] = fRec1239[0];
			fRec1238[1] = fRec1238[0];
			fRec1236[1] = fRec1236[0];
			fRec1247[1] = fRec1247[0];
			fRec1245[1] = fRec1245[0];
			fRec1253[1] = fRec1253[0];
			fRec1251[1] = fRec1251[0];
			fRec1250[1] = fRec1250[0];
			fRec1248[1] = fRec1248[0];
			fRec1268[1] = fRec1268[0];
			fRec1266[1] = fRec1266[0];
			fRec1265[1] = fRec1265[0];
			fRec1263[1] = fRec1263[0];
			fRec1262[1] = fRec1262[0];
			fRec1260[1] = fRec1260[0];
			fRec1259[1] = fRec1259[0];
			fRec1257[1] = fRec1257[0];
			fRec1256[1] = fRec1256[0];
			fRec1254[1] = fRec1254[0];
			fRec1280[1] = fRec1280[0];
			fRec1278[1] = fRec1278[0];
			fRec1277[1] = fRec1277[0];
			fRec1275[1] = fRec1275[0];
			fRec1274[1] = fRec1274[0];
			fRec1272[1] = fRec1272[0];
			fRec1271[1] = fRec1271[0];
			fRec1269[1] = fRec1269[0];
			fRec1289[1] = fRec1289[0];
			fRec1287[1] = fRec1287[0];
			fRec1286[1] = fRec1286[0];
			fRec1284[1] = fRec1284[0];
			fRec1283[1] = fRec1283[0];
			fRec1281[1] = fRec1281[0];
			fRec1292[1] = fRec1292[0];
			fRec1290[1] = fRec1290[0];
			fRec1298[1] = fRec1298[0];
			fRec1296[1] = fRec1296[0];
			fRec1295[1] = fRec1295[0];
			fRec1293[1] = fRec1293[0];
			fRec1313[1] = fRec1313[0];
			fRec1311[1] = fRec1311[0];
			fRec1310[1] = fRec1310[0];
			fRec1308[1] = fRec1308[0];
			fRec1307[1] = fRec1307[0];
			fRec1305[1] = fRec1305[0];
			fRec1304[1] = fRec1304[0];
			fRec1302[1] = fRec1302[0];
			fRec1301[1] = fRec1301[0];
			fRec1299[1] = fRec1299[0];
			fRec1325[1] = fRec1325[0];
			fRec1323[1] = fRec1323[0];
			fRec1322[1] = fRec1322[0];
			fRec1320[1] = fRec1320[0];
			fRec1319[1] = fRec1319[0];
			fRec1317[1] = fRec1317[0];
			fRec1316[1] = fRec1316[0];
			fRec1314[1] = fRec1314[0];
			fRec1334[1] = fRec1334[0];
			fRec1332[1] = fRec1332[0];
			fRec1331[1] = fRec1331[0];
			fRec1329[1] = fRec1329[0];
			fRec1328[1] = fRec1328[0];
			fRec1326[1] = fRec1326[0];
			fRec1337[1] = fRec1337[0];
			fRec1335[1] = fRec1335[0];
			fRec1343[1] = fRec1343[0];
			fRec1341[1] = fRec1341[0];
			fRec1340[1] = fRec1340[0];
			fRec1338[1] = fRec1338[0];
			fVec22[1] = fVec22[0];
			fRec1358[1] = fRec1358[0];
			fRec1356[1] = fRec1356[0];
			fRec1355[1] = fRec1355[0];
			fRec1353[1] = fRec1353[0];
			fRec1352[1] = fRec1352[0];
			fRec1350[1] = fRec1350[0];
			fRec1349[1] = fRec1349[0];
			fRec1347[1] = fRec1347[0];
			fRec1346[1] = fRec1346[0];
			fRec1344[1] = fRec1344[0];
			fRec1370[1] = fRec1370[0];
			fRec1368[1] = fRec1368[0];
			fRec1367[1] = fRec1367[0];
			fRec1365[1] = fRec1365[0];
			fRec1364[1] = fRec1364[0];
			fRec1362[1] = fRec1362[0];
			fRec1361[1] = fRec1361[0];
			fRec1359[1] = fRec1359[0];
			fRec1379[1] = fRec1379[0];
			fRec1377[1] = fRec1377[0];
			fRec1376[1] = fRec1376[0];
			fRec1374[1] = fRec1374[0];
			fRec1373[1] = fRec1373[0];
			fRec1371[1] = fRec1371[0];
			fRec1382[1] = fRec1382[0];
			fRec1380[1] = fRec1380[0];
			fRec1388[1] = fRec1388[0];
			fRec1386[1] = fRec1386[0];
			fRec1385[1] = fRec1385[0];
			fRec1383[1] = fRec1383[0];
			fRec1391[1] = fRec1391[0];
			fRec1389[1] = fRec1389[0];
			fRec1397[1] = fRec1397[0];
			fRec1395[1] = fRec1395[0];
			fRec1394[1] = fRec1394[0];
			fRec1392[1] = fRec1392[0];
			fRec1406[1] = fRec1406[0];
			fRec1404[1] = fRec1404[0];
			fRec1403[1] = fRec1403[0];
			fRec1401[1] = fRec1401[0];
			fRec1400[1] = fRec1400[0];
			fRec1398[1] = fRec1398[0];
			fRec1418[1] = fRec1418[0];
			fRec1416[1] = fRec1416[0];
			fRec1415[1] = fRec1415[0];
			fRec1413[1] = fRec1413[0];
			fRec1412[1] = fRec1412[0];
			fRec1410[1] = fRec1410[0];
			fRec1409[1] = fRec1409[0];
			fRec1407[1] = fRec1407[0];
			fRec1433[1] = fRec1433[0];
			fRec1431[1] = fRec1431[0];
			fRec1430[1] = fRec1430[0];
			fRec1428[1] = fRec1428[0];
			fRec1427[1] = fRec1427[0];
			fRec1425[1] = fRec1425[0];
			fRec1424[1] = fRec1424[0];
			fRec1422[1] = fRec1422[0];
			fRec1421[1] = fRec1421[0];
			fRec1419[1] = fRec1419[0];
			
		}
		
	}

	
};

//----------------------------------------------------------------------------
// SuperCollider/Faust interface
//----------------------------------------------------------------------------

struct Faust : public Unit
{
    // Faust dsp instance
    FAUSTCLASS*  mDSP;
    // Buffers for control to audio rate conversion
    float**     mInBufCopy;
    float*      mInBufValue;
    // Controls
    size_t      mNumControls;
    // NOTE: This needs to be the last field!
    //
    // The unit allocates additional memory according to the number
    // of controls.
    Control     mControls[0];

    int getNumAudioInputs() { return mDSP->getNumInputs(); }
};

// Global state

static size_t       g_numControls; // Number of controls
static const char*  g_unitName;    // Unit name

// Initialize the global state with unit name and sample rate.
void initState(const std::string& name, int sampleRate);

// Return the unit size in bytes, including static fields and controls.
static size_t unitSize();

// Convert a file name to a valid unit name.
static std::string fileNameToUnitName(const std::string& fileName);

// Convert the XML unit name to a valid class name.
static std::string normalizeClassName(const std::string& name);

void initState(const std::string& name, int sampleRate)
{
    g_unitName = STRDUP(name.c_str());

    mydsp* dsp = new FAUSTCLASS;
    ControlCounter* cc = new ControlCounter;

    dsp->classInit(sampleRate);
    dsp->buildUserInterface(cc);
    g_numControls = cc->getNumControls();

    delete dsp;
    delete cc;
}

size_t unitSize()
{
    return sizeof(Faust) + g_numControls * sizeof(Control);
}

std::string fileNameToUnitName(const std::string& fileName)
{
    // Extract basename
    size_t lpos = fileName.rfind('/', fileName.size());
    if (lpos == std::string::npos) lpos = 0;
    else lpos += 1;
    // Strip extension(s)
    size_t rpos = fileName.find('.', lpos);
    // Return substring
    return fileName.substr(lpos, rpos > lpos ? rpos - lpos : 0);
}

// Globals

static InterfaceTable* ft;

// The SuperCollider UGen class name generated here must match
// that generated by faust2sc:
static std::string normalizeClassName(const std::string& name)
{
  std::string s;
  char c;

  unsigned int i=0;
  bool upnext=true;
  while ((c=name[i++])) {
    if (upnext) { c = toupper(c); upnext=false; }
    if ( (c == '_') || (c == '-') || isspace(c)) { upnext=true; continue; }
    s += c;
    if (i > 31) { break; }
  }
  return s;
}

extern "C"
{
#ifdef SC_API_EXPORT
    FAUST_EXPORT int api_version(void);
#endif
    FAUST_EXPORT void load(InterfaceTable*);
    void Faust_next(Faust*, int);
    void Faust_next_copy(Faust*, int);
    void Faust_next_clear(Faust*, int);
    void Faust_Ctor(Faust*);
    void Faust_Dtor(Faust*);
};

inline static void fillBuffer(float* dst, int n, float v)
{
    Fill(n, dst, v);
}

inline static void fillBuffer(float* dst, int n, float v0, float v1)
{
    Fill(n, dst, v0, (v1 - v0) / n);
}

inline static void copyBuffer(float* dst, int n, float* src)
{
    Copy(n, dst, src);
}

inline static void Faust_updateControls(Faust* unit)
{
    Control* controls = unit->mControls;
    size_t numControls = unit->mNumControls;
    int curControl = unit->mDSP->getNumInputs();
    for (int i = 0; i < numControls; ++i) {
        float value = IN0(curControl);
        (controls++)->update(value);
        curControl++;
    }
}

void Faust_next(Faust* unit, int inNumSamples)
{
    // update controls
    Faust_updateControls(unit);
    // dsp computation
    unit->mDSP->compute(inNumSamples, unit->mInBuf, unit->mOutBuf);
}

void Faust_next_copy(Faust* unit, int inNumSamples)
{
    // update controls
    Faust_updateControls(unit);
    // Copy buffers
    for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
        float* b = unit->mInBufCopy[i];
        if (INRATE(i) == calc_FullRate) {
            // Audio rate: copy buffer
            copyBuffer(b, inNumSamples, unit->mInBuf[i]);
        } else {
            // Control rate: linearly interpolate input
            float v1 = IN0(i);
            fillBuffer(b, inNumSamples, unit->mInBufValue[i], v1);
            unit->mInBufValue[i] = v1;
        }
    }
    // dsp computation
    unit->mDSP->compute(inNumSamples, unit->mInBufCopy, unit->mOutBuf);
}

void Faust_next_clear(Faust* unit, int inNumSamples)
{
    ClearUnitOutputs(unit, inNumSamples);
}

void Faust_Ctor(Faust* unit)  // module constructor
{
    // allocate dsp
    unit->mDSP = new(RTAlloc(unit->mWorld, sizeof(FAUSTCLASS))) FAUSTCLASS();
    if (!unit->mDSP) {
        Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
        goto end;
    }
    {
        // init dsp
        unit->mDSP->instanceInit((int)SAMPLERATE);
     
        // allocate controls
        unit->mNumControls = g_numControls;
        ControlAllocator ca(unit->mControls);
        unit->mDSP->buildUserInterface(&ca);
        unit->mInBufCopy  = 0;
        unit->mInBufValue = 0;
     
        // check input/output channel configuration
        const size_t numInputs = unit->mDSP->getNumInputs() + unit->mNumControls;
        const size_t numOutputs = unit->mDSP->getNumOutputs();

        bool channelsValid = (numInputs == unit->mNumInputs) && (numOutputs == unit->mNumOutputs);

        if (channelsValid) {
            bool rateValid = true;
            for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
                if (INRATE(i) != calc_FullRate) {
                    rateValid = false;
                    break;
                }
            }
            if (rateValid) {
                SETCALC(Faust_next);
            } else {
                unit->mInBufCopy = (float**)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*sizeof(float*));
                if (!unit->mInBufCopy) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                // Allocate memory for input buffer copies (numInputs * bufLength)
                // and linear interpolation state (numInputs)
                // = numInputs * (bufLength + 1)
                unit->mInBufValue = (float*)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*sizeof(float));
                if (!unit->mInBufValue) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                // Aquire memory for interpolator state.
                float* mem = (float*)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*BUFLENGTH*sizeof(float));
                if (mem) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
                    // Initialize interpolator.
                    unit->mInBufValue[i] = IN0(i);
                    // Aquire buffer memory.
                    unit->mInBufCopy[i] = mem;
                    mem += BUFLENGTH;
                }
                SETCALC(Faust_next_copy);
            }
    #if !defined(NDEBUG)
            Print("Faust[%s]:\n", g_unitName);
            Print("    Inputs:   %d\n"
                  "    Outputs:  %d\n"
                  "    Callback: %s\n",
                  numInputs, numOutputs,
                  unit->mCalcFunc == (UnitCalcFunc)Faust_next ? "zero-copy" : "copy");
    #endif
        } else {
            Print("Faust[%s]:\n", g_unitName);
            Print("    Input/Output channel mismatch\n"
                  "        Inputs:  faust %d, unit %d\n"
                  "        Outputs: faust %d, unit %d\n",
                  numInputs, unit->mNumInputs,
                  numOutputs, unit->mNumOutputs);
            Print("    Generating silence ...\n");
            SETCALC(Faust_next_clear);
        }
    }
    
end:
    // Fix for https://github.com/grame-cncm/faust/issues/13
    ClearUnitOutputs(unit, 1);
}

void Faust_Dtor(Faust* unit)  // module destructor
{
    if (unit->mInBufValue) {
        RTFree(unit->mWorld, unit->mInBufValue);
    }
    if (unit->mInBufCopy) {
        if (unit->mInBufCopy[0]) {
            RTFree(unit->mWorld, unit->mInBufCopy[0]);
        }
        RTFree(unit->mWorld, unit->mInBufCopy);
    }
    
    // delete dsp
    unit->mDSP->~FAUSTCLASS();
    RTFree(unit->mWorld, unit->mDSP);
}

#ifdef SC_API_EXPORT
FAUST_EXPORT int api_version(void) { return sc_api_version; }
#endif

FAUST_EXPORT void load(InterfaceTable* inTable)
{
    ft = inTable;

    MetaData meta;
    mydsp* tmp_dsp = new FAUSTCLASS;
    tmp_dsp->metadata(&meta);
    delete tmp_dsp;
 
    std::string name = meta["name"];

    if (name.empty()) {
        name = fileNameToUnitName(__FILE__);
    }
  
    name = normalizeClassName(name);

#if !defined(NDEBUG) & defined(SC_API_EXPORT)
    Print("Faust: supercollider.cpp: sc_api_version = %d\n", sc_api_version);
#endif

    if (name.empty()) {
        // Catch empty name
        Print("Faust [supercollider.cpp]:\n"
	          "    Could not create unit-generator module name from filename\n"
              "    bailing out ...\n");
        return;
    }

    if (strncmp(name.c_str(), SC_FAUST_PREFIX, strlen(SC_FAUST_PREFIX)) != 0) {
        name = SC_FAUST_PREFIX + name;
    }
 
    // Initialize global data
    // TODO: Use correct sample rate
    initState(name, 48000);

    // Register ugen
    (*ft->fDefineUnit)(
        (char*)name.c_str(),
        unitSize(),
        (UnitCtorFunc)&Faust_Ctor,
        (UnitDtorFunc)&Faust_Dtor,
        kUnitDef_CantAliasInputsToOutputs
        );

#if !defined(NDEBUG)
    Print("Faust: %s numControls=%d\n", name.c_str(), g_numControls);
#endif // NDEBUG
}

#ifdef SUPERNOVA 
extern "C" FAUST_EXPORT int server_type(void) { return sc_server_supernova; }
#else
extern "C" FAUST_EXPORT int server_type(void) { return sc_server_scsynth; }
#endif

// EOF

#endif
