/* ------------------------------------------------------------
author: "AmbisonicDecoderToolkit"
copyright: "(c) Aaron J. Heller 2013"
license: "BSD 3-Clause License"
name: "SATOsw0o2"
version: "1.2"
Code generated with Faust 2.5.12 (https://faust.grame.fr)
Compilation options: cpp, -scal -ftz 0
------------------------------------------------------------ */

#ifndef  __mydsp_H__
#define  __mydsp_H__

//-------------------------------------------------------------------
// FAUST architecture file for SuperCollider.
// Copyright (C) 2005-2012 Stefan Kersten.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//-------------------------------------------------------------------

// If other than 'faust2sc --prefix Faust' is used, sed this as well:
#if !defined(SC_FAUST_PREFIX)
#define SC_FAUST_PREFIX "Faust"
#endif

#include <map>
#include <string>
#include <string.h>
#include <SC_PlugIn.h>

/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __dsp__
#define __dsp__

#include <string>

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

class UI;
struct Meta;

/**
 * DSP memory manager.
 */

struct dsp_memory_manager {
    
    virtual ~dsp_memory_manager() {}
    
    virtual void* allocate(size_t size) = 0;
    virtual void destroy(void* ptr) = 0;
    
};

/**
* Signal processor definition.
*/

class dsp {

    public:

        dsp() {}
        virtual ~dsp() {}

        /* Return instance number of audio inputs */
        virtual int getNumInputs() = 0;
    
        /* Return instance number of audio outputs */
        virtual int getNumOutputs() = 0;
    
        /**
         * Trigger the ui_interface parameter with instance specific calls
         * to 'addBtton', 'addVerticalSlider'... in order to build the UI.
         *
         * @param ui_interface - the user interface builder
         */
        virtual void buildUserInterface(UI* ui_interface) = 0;
    
        /* Returns the sample rate currently used by the instance */
        virtual int getSampleRate() = 0;
    
        /**
         * Global init, calls the following methods:
         * - static class 'classInit': static tables initialization
         * - 'instanceInit': constants and instance state initialization
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void init(int samplingRate) = 0;

        /**
         * Init instance state
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void instanceInit(int samplingRate) = 0;

        /**
         * Init instance constant state
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void instanceConstants(int samplingRate) = 0;
    
        /* Init default control parameters values */
        virtual void instanceResetUserInterface() = 0;
    
        /* Init instance state (delay lines...) */
        virtual void instanceClear() = 0;
 
        /**
         * Return a clone of the instance.
         *
         * @return a copy of the instance on success, otherwise a null pointer.
         */
        virtual dsp* clone() = 0;
    
        /**
         * Trigger the Meta* parameter with instance specific calls to 'declare' (key, value) metadata.
         *
         * @param m - the Meta* meta user
         */
        virtual void metadata(Meta* m) = 0;
    
        /**
         * DSP instance computation, to be called with successive in/out audio buffers.
         *
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         *
         */
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) = 0;
    
        /**
         * DSP instance computation: alternative method to be used by subclasses.
         *
         * @param date_usec - the timestamp in microsec given by audio driver.
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         *
         */
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { compute(count, inputs, outputs); }
       
};

/**
 * Generic DSP decorator.
 */

class decorator_dsp : public dsp {

    protected:

        dsp* fDSP;

    public:

        decorator_dsp(dsp* dsp = 0):fDSP(dsp) {}
        virtual ~decorator_dsp() { delete fDSP; }

        virtual int getNumInputs() { return fDSP->getNumInputs(); }
        virtual int getNumOutputs() { return fDSP->getNumOutputs(); }
        virtual void buildUserInterface(UI* ui_interface) { fDSP->buildUserInterface(ui_interface); }
        virtual int getSampleRate() { return fDSP->getSampleRate(); }
        virtual void init(int samplingRate) { fDSP->init(samplingRate); }
        virtual void instanceInit(int samplingRate) { fDSP->instanceInit(samplingRate); }
        virtual void instanceConstants(int samplingRate) { fDSP->instanceConstants(samplingRate); }
        virtual void instanceResetUserInterface() { fDSP->instanceResetUserInterface(); }
        virtual void instanceClear() { fDSP->instanceClear(); }
        virtual decorator_dsp* clone() { return new decorator_dsp(fDSP->clone()); }
        virtual void metadata(Meta* m) { fDSP->metadata(m); }
        // Beware: subclasses usually have to overload the two 'compute' methods
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(count, inputs, outputs); }
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(date_usec, count, inputs, outputs); }
    
};

/**
 * DSP factory class.
 */

class dsp_factory {
    
    protected:
    
        // So that to force sub-classes to use deleteDSPFactory(dsp_factory* factory);
        virtual ~dsp_factory() {}
    
    public:
    
        virtual std::string getName() = 0;
        virtual std::string getSHAKey() = 0;
        virtual std::string getDSPCode() = 0;
    
        virtual dsp* createDSPInstance() = 0;
    
        virtual void setMemoryManager(dsp_memory_manager* manager) = 0;
        virtual dsp_memory_manager* getMemoryManager() = 0;
    
};

/**
 * On Intel set FZ (Flush to Zero) and DAZ (Denormals Are Zero)
 * flags to avoid costly denormals.
 */

#ifdef __SSE__
    #include <xmmintrin.h>
    #ifdef __SSE2__
        #define AVOIDDENORMALS _mm_setcsr(_mm_getcsr() | 0x8040)
    #else
        #define AVOIDDENORMALS _mm_setcsr(_mm_getcsr() | 0x8000)
    #endif
#else
    #define AVOIDDENORMALS
#endif

#endif
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __UI_H__
#define __UI_H__

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

/*******************************************************************************
 * UI : Faust DSP User Interface
 * User Interface as expected by the buildUserInterface() method of a DSP.
 * This abstract class contains only the method that the Faust compiler can
 * generate to describe a DSP user interface.
 ******************************************************************************/

struct Soundfile;

class UI
{

    public:

        UI() {}

        virtual ~UI() {}

        // -- widget's layouts

        virtual void openTabBox(const char* label) = 0;
        virtual void openHorizontalBox(const char* label) = 0;
        virtual void openVerticalBox(const char* label) = 0;
        virtual void closeBox() = 0;

        // -- active widgets

        virtual void addButton(const char* label, FAUSTFLOAT* zone) = 0;
        virtual void addCheckButton(const char* label, FAUSTFLOAT* zone) = 0;
        virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;
        virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;
        virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;

        // -- passive widgets

        virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) = 0;
        virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) = 0;
    
        // -- soundfiles
    
        virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) = 0;

        // -- metadata declarations

        virtual void declare(FAUSTFLOAT*, const char*, const char*) {}
};

#endif
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/
 
#ifndef __misc__
#define __misc__

#include <algorithm>
#include <map>
#include <string.h>
#include <stdlib.h>

/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __meta__
#define __meta__

struct Meta
{
    virtual void declare(const char* key, const char* value) = 0;
    virtual ~Meta() {};
};

#endif

using std::max;
using std::min;

struct XXXX_Meta : std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key]=value; }
};

struct MY_Meta : Meta, std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key]=value; }
};

inline int lsr(int x, int n)	{ return int(((unsigned int)x) >> n); }

inline int int2pow2(int x)		{ int r = 0; while ((1<<r) < x) r++; return r; }

inline long lopt(char* argv[], const char* name, long def)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return atoi(argv[i+1]);
	return def;
}

inline bool isopt(char* argv[], const char* name)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return true;
	return false;
}

inline const char* lopts(char* argv[], const char* name, const char* def)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return argv[i+1];
	return def;
}

#endif


using namespace std;

#if defined(__GNUC__) && __GNUC__ >= 4
    #define FAUST_EXPORT __attribute__((visibility("default")))
#else
    #define FAUST_EXPORT  SC_API_EXPORT
#endif

#ifdef WIN32
    #define STRDUP _strdup
#else
    #define STRDUP strdup
#endif

//----------------------------------------------------------------------------
// Vector intrinsics
//----------------------------------------------------------------------------


//----------------------------------------------------------------------------
// Metadata
//----------------------------------------------------------------------------

class MetaData : public Meta
               , public std::map<std::string, std::string>
{
public:
    void declare(const char* key, const char* value)
    {
        (*this)[key] = value;
    }
};

//----------------------------------------------------------------------------
// Control counter
//----------------------------------------------------------------------------

class ControlCounter : public UI
{
public:
    ControlCounter()
        : mNumControlInputs(0),
          mNumControlOutputs(0)
    {}

    size_t getNumControls() const { return getNumControlInputs(); }
    size_t getNumControlInputs() const { return mNumControlInputs; }
    size_t getNumControlOutputs() const { return mNumControlOutputs; }

    // Layout widgets
    virtual void openTabBox(const char* label) { }
    virtual void openHorizontalBox(const char* label) { }
    virtual void openVerticalBox(const char* label) { }
    virtual void closeBox() { }

    // Active widgets
    virtual void addButton(const char* label, FAUSTFLOAT* zone)
    { addControlInput(); }
    virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
    { addControlInput(); }
    virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }
    virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }
    virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }

    // Passive widgets
    virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
    { addControlOutput(); }
    virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
    { addControlOutput(); }
    
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}

protected:
    void addControlInput() { mNumControlInputs++; }
    void addControlOutput() { mNumControlOutputs++; }

private:
    size_t mNumControlInputs;
    size_t mNumControlOutputs;
};

//----------------------------------------------------------------------------
// UI control
//----------------------------------------------------------------------------

struct Control
{
    typedef void (*UpdateFunction)(Control* self, FAUSTFLOAT value);

    UpdateFunction updateFunction;
    FAUSTFLOAT* zone;
    FAUSTFLOAT min, max;

    inline void update(FAUSTFLOAT value)
    {
        (*updateFunction)(this, value);
    }

    static void simpleUpdate(Control* self, FAUSTFLOAT value)
    {
        *self->zone = value;
    }
    static void boundedUpdate(Control* self, FAUSTFLOAT value)
    {
        *self->zone = sc_clip(value, self->min, self->max);
    }
};

//----------------------------------------------------------------------------
// Control allocator
//----------------------------------------------------------------------------

class ControlAllocator : public UI
{
public:
    ControlAllocator(Control* controls)
        : mControls(controls)
    { }

    // Layout widgets
    virtual void openTabBox(const char* label) { }
    virtual void openHorizontalBox(const char* label) { }
    virtual void openVerticalBox(const char* label) { }
    virtual void closeBox() { }

    // Active widgets
    virtual void addButton(const char* label, FAUSTFLOAT* zone)
    { addSimpleControl(zone); }
    virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
    { addSimpleControl(zone); }
    virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }
    virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }
    virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }

    // Passive widgets
    virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
    virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}

private:
    void addControl(Control::UpdateFunction updateFunction, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT /* step */)
    {
        Control* ctrl        = mControls++;
        ctrl->updateFunction = updateFunction;
        ctrl->zone           = zone;
        ctrl->min            = min;
        ctrl->max            = max;
    }
    void addSimpleControl(FAUSTFLOAT* zone)
    {
        addControl(Control::simpleUpdate, zone, 0.f, 0.f, 0.f);
    }
    void addBoundedControl(FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    {
        addControl(Control::boundedUpdate, zone, min, max, step);
    }

private:
    Control* mControls;
};

//----------------------------------------------------------------------------
// FAUST generated code
//----------------------------------------------------------------------------

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif 

#include <cmath>
#include <math.h>

float mydsp_faustpower2_f(float value) {
	return (value * value);
	
}

#ifndef FAUSTCLASS 
#define FAUSTCLASS mydsp
#endif
#ifdef __APPLE__ 
#define exp10f __exp10f
#define exp10 __exp10
#endif

class mydsp : public dsp {
	
 private:
	
	int fSamplingFreq;
	int iConst0;
	float fConst1;
	float fConst2;
	float fConst3;
	float fConst4;
	FAUSTFLOAT fHslider0;
	float fRec3[2];
	float fRec4[3];
	FAUSTFLOAT fHslider1;
	float fRec5[2];
	float fRec6[3];
	float fRec7[3];
	float fConst5;
	float fRec2[2];
	float fRec0[2];
	float fConst6;
	float fConst7;
	float fRec14[3];
	float fRec15[3];
	float fRec16[3];
	float fRec17[3];
	float fRec18[3];
	float fConst8;
	float fConst9;
	float fConst10;
	float fRec13[2];
	float fRec11[2];
	float fRec10[2];
	float fRec8[2];
	float fRec19[3];
	int IOTA;
	float fVec0[2048];
	int iConst11;
	FAUSTFLOAT fCheckbox0;
	FAUSTFLOAT fHslider2;
	float fRec20[2];
	float fConst12;
	float fConst13;
	float fConst14;
	float fRec23[2];
	float fRec21[2];
	float fConst15;
	float fConst16;
	float fConst17;
	float fConst18;
	float fRec29[2];
	float fRec27[2];
	float fRec26[2];
	float fRec24[2];
	float fVec1[2048];
	int iConst19;
	float fConst20;
	float fConst21;
	float fConst22;
	float fRec32[2];
	float fRec30[2];
	float fConst23;
	float fConst24;
	float fConst25;
	float fConst26;
	float fRec38[2];
	float fRec36[2];
	float fRec35[2];
	float fRec33[2];
	float fVec2[2048];
	int iConst27;
	float fRec41[2];
	float fRec39[2];
	float fRec47[2];
	float fRec45[2];
	float fRec44[2];
	float fRec42[2];
	float fVec3[2048];
	float fRec50[2];
	float fRec48[2];
	float fRec56[2];
	float fRec54[2];
	float fRec53[2];
	float fRec51[2];
	float fVec4[2048];
	float fRec59[2];
	float fRec57[2];
	float fRec65[2];
	float fRec63[2];
	float fRec62[2];
	float fRec60[2];
	float fVec5[2048];
	float fRec68[2];
	float fRec66[2];
	float fRec74[2];
	float fRec72[2];
	float fRec71[2];
	float fRec69[2];
	float fVec6[2048];
	float fConst28;
	float fConst29;
	float fConst30;
	float fRec77[2];
	float fRec75[2];
	float fConst31;
	float fConst32;
	float fConst33;
	float fConst34;
	float fRec83[2];
	float fRec81[2];
	float fRec80[2];
	float fRec78[2];
	float fVec7[1024];
	int iConst35;
	float fConst36;
	float fConst37;
	float fConst38;
	float fRec86[2];
	float fRec84[2];
	float fConst39;
	float fConst40;
	float fConst41;
	float fConst42;
	float fRec92[2];
	float fRec90[2];
	float fRec89[2];
	float fRec87[2];
	float fVec8[1024];
	int iConst43;
	float fRec95[2];
	float fRec93[2];
	float fRec101[2];
	float fRec99[2];
	float fRec98[2];
	float fRec96[2];
	float fVec9[1024];
	float fRec104[2];
	float fRec102[2];
	float fRec110[2];
	float fRec108[2];
	float fRec107[2];
	float fRec105[2];
	float fVec10[1024];
	float fRec113[2];
	float fRec111[2];
	float fRec119[2];
	float fRec117[2];
	float fRec116[2];
	float fRec114[2];
	float fVec11[1024];
	float fRec122[2];
	float fRec120[2];
	float fRec128[2];
	float fRec126[2];
	float fRec125[2];
	float fRec123[2];
	float fVec12[1024];
	float fRec131[2];
	float fRec129[2];
	float fRec137[2];
	float fRec135[2];
	float fRec134[2];
	float fRec132[2];
	float fVec13[1024];
	float fRec140[2];
	float fRec138[2];
	float fRec146[2];
	float fRec144[2];
	float fRec143[2];
	float fRec141[2];
	float fVec14[1024];
	float fRec149[2];
	float fRec147[2];
	float fRec155[2];
	float fRec153[2];
	float fRec152[2];
	float fRec150[2];
	float fVec15[1024];
	float fRec158[2];
	float fRec156[2];
	float fRec164[2];
	float fRec162[2];
	float fRec161[2];
	float fRec159[2];
	float fVec16[1024];
	float fRec167[2];
	float fRec165[2];
	float fRec173[2];
	float fRec171[2];
	float fRec170[2];
	float fRec168[2];
	float fVec17[1024];
	float fRec176[2];
	float fRec174[2];
	float fRec182[2];
	float fRec180[2];
	float fRec179[2];
	float fRec177[2];
	float fVec18[1024];
	float fConst44;
	float fConst45;
	float fConst46;
	float fRec185[2];
	float fRec183[2];
	float fConst47;
	float fConst48;
	float fConst49;
	float fConst50;
	float fRec191[2];
	float fRec189[2];
	float fRec188[2];
	float fRec186[2];
	float fVec19[3];
	int iConst51;
	float fConst52;
	float fConst53;
	float fConst54;
	float fRec194[2];
	float fRec192[2];
	float fConst55;
	float fConst56;
	float fConst57;
	float fConst58;
	float fRec200[2];
	float fRec198[2];
	float fRec197[2];
	float fRec195[2];
	float fRec203[2];
	float fRec201[2];
	float fRec209[2];
	float fRec207[2];
	float fRec206[2];
	float fRec204[2];
	float fRec212[2];
	float fRec210[2];
	float fRec218[2];
	float fRec216[2];
	float fRec215[2];
	float fRec213[2];
	float fVec20[3];
	float fRec221[2];
	float fRec219[2];
	float fRec227[2];
	float fRec225[2];
	float fRec224[2];
	float fRec222[2];
	float fRec230[2];
	float fRec228[2];
	float fRec236[2];
	float fRec234[2];
	float fRec233[2];
	float fRec231[2];
	float fRec239[2];
	float fRec237[2];
	float fRec245[2];
	float fRec243[2];
	float fRec242[2];
	float fRec240[2];
	float fVec21[3];
	float fRec248[2];
	float fRec246[2];
	float fRec254[2];
	float fRec252[2];
	float fRec251[2];
	float fRec249[2];
	float fRec257[2];
	float fRec255[2];
	float fRec263[2];
	float fRec261[2];
	float fRec260[2];
	float fRec258[2];
	float fRec266[2];
	float fRec264[2];
	float fRec272[2];
	float fRec270[2];
	float fRec269[2];
	float fRec267[2];
	float fVec22[3];
	float fRec275[2];
	float fRec273[2];
	float fRec281[2];
	float fRec279[2];
	float fRec278[2];
	float fRec276[2];
	float fRec284[2];
	float fRec282[2];
	float fRec290[2];
	float fRec288[2];
	float fRec287[2];
	float fRec285[2];
	
 public:
	
	void metadata(Meta* m) { 
		m->declare("author", "AmbisonicDecoderToolkit");
		m->declare("copyright", "(c) Aaron J. Heller 2013");
		m->declare("license", "BSD 3-Clause License");
		m->declare("name", "SATOsw0o2");
		m->declare("version", "1.2");
	}

	virtual int getNumInputs() {
		return 9;
		
	}
	virtual int getNumOutputs() {
		return 31;
		
	}
	virtual int getInputRate(int channel) {
		int rate;
		switch (channel) {
			case 0: {
				rate = 1;
				break;
			}
			case 1: {
				rate = 1;
				break;
			}
			case 2: {
				rate = 1;
				break;
			}
			case 3: {
				rate = 1;
				break;
			}
			case 4: {
				rate = 1;
				break;
			}
			case 5: {
				rate = 1;
				break;
			}
			case 6: {
				rate = 1;
				break;
			}
			case 7: {
				rate = 1;
				break;
			}
			case 8: {
				rate = 1;
				break;
			}
			default: {
				rate = -1;
				break;
			}
			
		}
		return rate;
		
	}
	virtual int getOutputRate(int channel) {
		int rate;
		switch (channel) {
			case 0: {
				rate = 1;
				break;
			}
			case 1: {
				rate = 1;
				break;
			}
			case 2: {
				rate = 1;
				break;
			}
			case 3: {
				rate = 1;
				break;
			}
			case 4: {
				rate = 1;
				break;
			}
			case 5: {
				rate = 1;
				break;
			}
			case 6: {
				rate = 1;
				break;
			}
			case 7: {
				rate = 1;
				break;
			}
			case 8: {
				rate = 1;
				break;
			}
			case 9: {
				rate = 1;
				break;
			}
			case 10: {
				rate = 1;
				break;
			}
			case 11: {
				rate = 1;
				break;
			}
			case 12: {
				rate = 1;
				break;
			}
			case 13: {
				rate = 1;
				break;
			}
			case 14: {
				rate = 1;
				break;
			}
			case 15: {
				rate = 1;
				break;
			}
			case 16: {
				rate = 1;
				break;
			}
			case 17: {
				rate = 1;
				break;
			}
			case 18: {
				rate = 1;
				break;
			}
			case 19: {
				rate = 1;
				break;
			}
			case 20: {
				rate = 1;
				break;
			}
			case 21: {
				rate = 1;
				break;
			}
			case 22: {
				rate = 1;
				break;
			}
			case 23: {
				rate = 1;
				break;
			}
			case 24: {
				rate = 1;
				break;
			}
			case 25: {
				rate = 1;
				break;
			}
			case 26: {
				rate = 1;
				break;
			}
			case 27: {
				rate = 1;
				break;
			}
			case 28: {
				rate = 1;
				break;
			}
			case 29: {
				rate = 1;
				break;
			}
			case 30: {
				rate = 1;
				break;
			}
			default: {
				rate = -1;
				break;
			}
			
		}
		return rate;
		
	}
	
	static void classInit(int samplingFreq) {
		
	}
	
	virtual void instanceConstants(int samplingFreq) {
		fSamplingFreq = samplingFreq;
		iConst0 = min(192000, max(1, fSamplingFreq));
		fConst1 = float(iConst0);
		fConst2 = ((28.482542f / fConst1) + 1.0f);
		fConst3 = (1.0f / fConst2);
		fConst4 = (3.14159274f / float(iConst0));
		fConst5 = (0.0f - (56.9650841f / (fConst1 * fConst2)));
		fConst6 = ((((2433.76538f / fConst1) + 85.4476242f) / fConst1) + 1.0f);
		fConst7 = (1.0f / fConst6);
		fConst8 = (0.0f - (((9735.06152f / fConst1) + 170.895248f) / (fConst1 * fConst6)));
		fConst9 = mydsp_faustpower2_f(fConst1);
		fConst10 = (0.0f - (9735.06152f / (fConst9 * fConst6)));
		iConst11 = int(((0.0106405718f * float(iConst0)) + 0.5f));
		fConst12 = ((24.7308426f / fConst1) + 1.0f);
		fConst13 = (1.0f / fConst12);
		fConst14 = (0.0f - (49.4616852f / (fConst1 * fConst12)));
		fConst15 = ((((1834.84363f / fConst1) + 74.1925278f) / fConst1) + 1.0f);
		fConst16 = (1.0f / fConst15);
		fConst17 = (0.0f - (7339.37451f / (fConst9 * fConst15)));
		fConst18 = (0.0f - (((7339.37451f / fConst1) + 148.385056f) / (fConst1 * fConst15)));
		iConst19 = int(((0.00797751546f * float(iConst0)) + 0.5f));
		fConst20 = ((24.7379723f / fConst1) + 1.0f);
		fConst21 = (1.0f / fConst20);
		fConst22 = (0.0f - (49.4759445f / (fConst20 * fConst1)));
		fConst23 = ((((1835.90173f / fConst1) + 74.213913f) / fConst1) + 1.0f);
		fConst24 = (1.0f / fConst23);
		fConst25 = (0.0f - (7343.60693f / (fConst23 * fConst9)));
		fConst26 = (0.0f - (((7343.60693f / fConst1) + 148.427826f) / (fConst23 * fConst1)));
		iConst27 = int(((0.00798334274f * float(iConst0)) + 0.5f));
		fConst28 = ((21.4885197f / fConst1) + 1.0f);
		fConst29 = (1.0f / fConst28);
		fConst30 = (0.0f - (42.9770393f / (fConst1 * fConst28)));
		fConst31 = ((((1385.26929f / fConst1) + 64.4655533f) / fConst1) + 1.0f);
		fConst32 = (1.0f / fConst31);
		fConst33 = (0.0f - (5541.07715f / (fConst9 * fConst31)));
		fConst34 = (0.0f - (((5541.07715f / fConst1) + 128.931107f) / (fConst1 * fConst31)));
		iConst35 = int(((0.00492694648f * float(iConst0)) + 0.5f));
		fConst36 = ((21.4750729f / fConst1) + 1.0f);
		fConst37 = (1.0f / fConst36);
		fConst38 = (0.0f - (42.9501457f / (fConst1 * fConst36)));
		fConst39 = ((((1383.53638f / fConst1) + 64.4252167f) / fConst1) + 1.0f);
		fConst40 = (1.0f / fConst39);
		fConst41 = (0.0f - (5534.14551f / (fConst9 * fConst39)));
		fConst42 = (0.0f - (((5534.14551f / fConst1) + 128.850433f) / (fConst1 * fConst39)));
		iConst43 = int(((0.00491237827f * float(iConst0)) + 0.5f));
		fConst44 = ((17.7371902f / fConst1) + 1.0f);
		fConst45 = (1.0f / fConst44);
		fConst46 = (0.0f - (35.4743805f / (fConst1 * fConst44)));
		fConst47 = ((((943.82373f / fConst1) + 53.2115707f) / fConst1) + 1.0f);
		fConst48 = (1.0f / fConst47);
		fConst49 = (0.0f - (3775.29492f / (fConst9 * fConst47)));
		fConst50 = (0.0f - (((3775.29492f / fConst1) + 106.423141f) / (fConst1 * fConst47)));
		iConst51 = int(((5.82725761e-06f * float(iConst0)) + 0.5f));
		fConst52 = ((17.7335243f / fConst1) + 1.0f);
		fConst53 = (1.0f / fConst52);
		fConst54 = (0.0f - (35.4670486f / (fConst52 * fConst1)));
		fConst55 = ((((943.433594f / fConst1) + 53.200573f) / fConst1) + 1.0f);
		fConst56 = (1.0f / fConst55);
		fConst57 = (0.0f - (3773.73438f / (fConst9 * fConst55)));
		fConst58 = (0.0f - (((3773.73438f / fConst1) + 106.401146f) / (fConst1 * fConst55)));
		
	}
	
	virtual void instanceResetUserInterface() {
		fHslider0 = FAUSTFLOAT(400.0f);
		fHslider1 = FAUSTFLOAT(0.0f);
		fCheckbox0 = FAUSTFLOAT(0.0f);
		fHslider2 = FAUSTFLOAT(-10.0f);
		
	}
	
	virtual void instanceClear() {
		for (int l0 = 0; (l0 < 2); l0 = (l0 + 1)) {
			fRec3[l0] = 0.0f;
			
		}
		for (int l1 = 0; (l1 < 3); l1 = (l1 + 1)) {
			fRec4[l1] = 0.0f;
			
		}
		for (int l2 = 0; (l2 < 2); l2 = (l2 + 1)) {
			fRec5[l2] = 0.0f;
			
		}
		for (int l3 = 0; (l3 < 3); l3 = (l3 + 1)) {
			fRec6[l3] = 0.0f;
			
		}
		for (int l4 = 0; (l4 < 3); l4 = (l4 + 1)) {
			fRec7[l4] = 0.0f;
			
		}
		for (int l5 = 0; (l5 < 2); l5 = (l5 + 1)) {
			fRec2[l5] = 0.0f;
			
		}
		for (int l6 = 0; (l6 < 2); l6 = (l6 + 1)) {
			fRec0[l6] = 0.0f;
			
		}
		for (int l7 = 0; (l7 < 3); l7 = (l7 + 1)) {
			fRec14[l7] = 0.0f;
			
		}
		for (int l8 = 0; (l8 < 3); l8 = (l8 + 1)) {
			fRec15[l8] = 0.0f;
			
		}
		for (int l9 = 0; (l9 < 3); l9 = (l9 + 1)) {
			fRec16[l9] = 0.0f;
			
		}
		for (int l10 = 0; (l10 < 3); l10 = (l10 + 1)) {
			fRec17[l10] = 0.0f;
			
		}
		for (int l11 = 0; (l11 < 3); l11 = (l11 + 1)) {
			fRec18[l11] = 0.0f;
			
		}
		for (int l12 = 0; (l12 < 2); l12 = (l12 + 1)) {
			fRec13[l12] = 0.0f;
			
		}
		for (int l13 = 0; (l13 < 2); l13 = (l13 + 1)) {
			fRec11[l13] = 0.0f;
			
		}
		for (int l14 = 0; (l14 < 2); l14 = (l14 + 1)) {
			fRec10[l14] = 0.0f;
			
		}
		for (int l15 = 0; (l15 < 2); l15 = (l15 + 1)) {
			fRec8[l15] = 0.0f;
			
		}
		for (int l16 = 0; (l16 < 3); l16 = (l16 + 1)) {
			fRec19[l16] = 0.0f;
			
		}
		IOTA = 0;
		for (int l17 = 0; (l17 < 2048); l17 = (l17 + 1)) {
			fVec0[l17] = 0.0f;
			
		}
		for (int l18 = 0; (l18 < 2); l18 = (l18 + 1)) {
			fRec20[l18] = 0.0f;
			
		}
		for (int l19 = 0; (l19 < 2); l19 = (l19 + 1)) {
			fRec23[l19] = 0.0f;
			
		}
		for (int l20 = 0; (l20 < 2); l20 = (l20 + 1)) {
			fRec21[l20] = 0.0f;
			
		}
		for (int l21 = 0; (l21 < 2); l21 = (l21 + 1)) {
			fRec29[l21] = 0.0f;
			
		}
		for (int l22 = 0; (l22 < 2); l22 = (l22 + 1)) {
			fRec27[l22] = 0.0f;
			
		}
		for (int l23 = 0; (l23 < 2); l23 = (l23 + 1)) {
			fRec26[l23] = 0.0f;
			
		}
		for (int l24 = 0; (l24 < 2); l24 = (l24 + 1)) {
			fRec24[l24] = 0.0f;
			
		}
		for (int l25 = 0; (l25 < 2048); l25 = (l25 + 1)) {
			fVec1[l25] = 0.0f;
			
		}
		for (int l26 = 0; (l26 < 2); l26 = (l26 + 1)) {
			fRec32[l26] = 0.0f;
			
		}
		for (int l27 = 0; (l27 < 2); l27 = (l27 + 1)) {
			fRec30[l27] = 0.0f;
			
		}
		for (int l28 = 0; (l28 < 2); l28 = (l28 + 1)) {
			fRec38[l28] = 0.0f;
			
		}
		for (int l29 = 0; (l29 < 2); l29 = (l29 + 1)) {
			fRec36[l29] = 0.0f;
			
		}
		for (int l30 = 0; (l30 < 2); l30 = (l30 + 1)) {
			fRec35[l30] = 0.0f;
			
		}
		for (int l31 = 0; (l31 < 2); l31 = (l31 + 1)) {
			fRec33[l31] = 0.0f;
			
		}
		for (int l32 = 0; (l32 < 2048); l32 = (l32 + 1)) {
			fVec2[l32] = 0.0f;
			
		}
		for (int l33 = 0; (l33 < 2); l33 = (l33 + 1)) {
			fRec41[l33] = 0.0f;
			
		}
		for (int l34 = 0; (l34 < 2); l34 = (l34 + 1)) {
			fRec39[l34] = 0.0f;
			
		}
		for (int l35 = 0; (l35 < 2); l35 = (l35 + 1)) {
			fRec47[l35] = 0.0f;
			
		}
		for (int l36 = 0; (l36 < 2); l36 = (l36 + 1)) {
			fRec45[l36] = 0.0f;
			
		}
		for (int l37 = 0; (l37 < 2); l37 = (l37 + 1)) {
			fRec44[l37] = 0.0f;
			
		}
		for (int l38 = 0; (l38 < 2); l38 = (l38 + 1)) {
			fRec42[l38] = 0.0f;
			
		}
		for (int l39 = 0; (l39 < 2048); l39 = (l39 + 1)) {
			fVec3[l39] = 0.0f;
			
		}
		for (int l40 = 0; (l40 < 2); l40 = (l40 + 1)) {
			fRec50[l40] = 0.0f;
			
		}
		for (int l41 = 0; (l41 < 2); l41 = (l41 + 1)) {
			fRec48[l41] = 0.0f;
			
		}
		for (int l42 = 0; (l42 < 2); l42 = (l42 + 1)) {
			fRec56[l42] = 0.0f;
			
		}
		for (int l43 = 0; (l43 < 2); l43 = (l43 + 1)) {
			fRec54[l43] = 0.0f;
			
		}
		for (int l44 = 0; (l44 < 2); l44 = (l44 + 1)) {
			fRec53[l44] = 0.0f;
			
		}
		for (int l45 = 0; (l45 < 2); l45 = (l45 + 1)) {
			fRec51[l45] = 0.0f;
			
		}
		for (int l46 = 0; (l46 < 2048); l46 = (l46 + 1)) {
			fVec4[l46] = 0.0f;
			
		}
		for (int l47 = 0; (l47 < 2); l47 = (l47 + 1)) {
			fRec59[l47] = 0.0f;
			
		}
		for (int l48 = 0; (l48 < 2); l48 = (l48 + 1)) {
			fRec57[l48] = 0.0f;
			
		}
		for (int l49 = 0; (l49 < 2); l49 = (l49 + 1)) {
			fRec65[l49] = 0.0f;
			
		}
		for (int l50 = 0; (l50 < 2); l50 = (l50 + 1)) {
			fRec63[l50] = 0.0f;
			
		}
		for (int l51 = 0; (l51 < 2); l51 = (l51 + 1)) {
			fRec62[l51] = 0.0f;
			
		}
		for (int l52 = 0; (l52 < 2); l52 = (l52 + 1)) {
			fRec60[l52] = 0.0f;
			
		}
		for (int l53 = 0; (l53 < 2048); l53 = (l53 + 1)) {
			fVec5[l53] = 0.0f;
			
		}
		for (int l54 = 0; (l54 < 2); l54 = (l54 + 1)) {
			fRec68[l54] = 0.0f;
			
		}
		for (int l55 = 0; (l55 < 2); l55 = (l55 + 1)) {
			fRec66[l55] = 0.0f;
			
		}
		for (int l56 = 0; (l56 < 2); l56 = (l56 + 1)) {
			fRec74[l56] = 0.0f;
			
		}
		for (int l57 = 0; (l57 < 2); l57 = (l57 + 1)) {
			fRec72[l57] = 0.0f;
			
		}
		for (int l58 = 0; (l58 < 2); l58 = (l58 + 1)) {
			fRec71[l58] = 0.0f;
			
		}
		for (int l59 = 0; (l59 < 2); l59 = (l59 + 1)) {
			fRec69[l59] = 0.0f;
			
		}
		for (int l60 = 0; (l60 < 2048); l60 = (l60 + 1)) {
			fVec6[l60] = 0.0f;
			
		}
		for (int l61 = 0; (l61 < 2); l61 = (l61 + 1)) {
			fRec77[l61] = 0.0f;
			
		}
		for (int l62 = 0; (l62 < 2); l62 = (l62 + 1)) {
			fRec75[l62] = 0.0f;
			
		}
		for (int l63 = 0; (l63 < 2); l63 = (l63 + 1)) {
			fRec83[l63] = 0.0f;
			
		}
		for (int l64 = 0; (l64 < 2); l64 = (l64 + 1)) {
			fRec81[l64] = 0.0f;
			
		}
		for (int l65 = 0; (l65 < 2); l65 = (l65 + 1)) {
			fRec80[l65] = 0.0f;
			
		}
		for (int l66 = 0; (l66 < 2); l66 = (l66 + 1)) {
			fRec78[l66] = 0.0f;
			
		}
		for (int l67 = 0; (l67 < 1024); l67 = (l67 + 1)) {
			fVec7[l67] = 0.0f;
			
		}
		for (int l68 = 0; (l68 < 2); l68 = (l68 + 1)) {
			fRec86[l68] = 0.0f;
			
		}
		for (int l69 = 0; (l69 < 2); l69 = (l69 + 1)) {
			fRec84[l69] = 0.0f;
			
		}
		for (int l70 = 0; (l70 < 2); l70 = (l70 + 1)) {
			fRec92[l70] = 0.0f;
			
		}
		for (int l71 = 0; (l71 < 2); l71 = (l71 + 1)) {
			fRec90[l71] = 0.0f;
			
		}
		for (int l72 = 0; (l72 < 2); l72 = (l72 + 1)) {
			fRec89[l72] = 0.0f;
			
		}
		for (int l73 = 0; (l73 < 2); l73 = (l73 + 1)) {
			fRec87[l73] = 0.0f;
			
		}
		for (int l74 = 0; (l74 < 1024); l74 = (l74 + 1)) {
			fVec8[l74] = 0.0f;
			
		}
		for (int l75 = 0; (l75 < 2); l75 = (l75 + 1)) {
			fRec95[l75] = 0.0f;
			
		}
		for (int l76 = 0; (l76 < 2); l76 = (l76 + 1)) {
			fRec93[l76] = 0.0f;
			
		}
		for (int l77 = 0; (l77 < 2); l77 = (l77 + 1)) {
			fRec101[l77] = 0.0f;
			
		}
		for (int l78 = 0; (l78 < 2); l78 = (l78 + 1)) {
			fRec99[l78] = 0.0f;
			
		}
		for (int l79 = 0; (l79 < 2); l79 = (l79 + 1)) {
			fRec98[l79] = 0.0f;
			
		}
		for (int l80 = 0; (l80 < 2); l80 = (l80 + 1)) {
			fRec96[l80] = 0.0f;
			
		}
		for (int l81 = 0; (l81 < 1024); l81 = (l81 + 1)) {
			fVec9[l81] = 0.0f;
			
		}
		for (int l82 = 0; (l82 < 2); l82 = (l82 + 1)) {
			fRec104[l82] = 0.0f;
			
		}
		for (int l83 = 0; (l83 < 2); l83 = (l83 + 1)) {
			fRec102[l83] = 0.0f;
			
		}
		for (int l84 = 0; (l84 < 2); l84 = (l84 + 1)) {
			fRec110[l84] = 0.0f;
			
		}
		for (int l85 = 0; (l85 < 2); l85 = (l85 + 1)) {
			fRec108[l85] = 0.0f;
			
		}
		for (int l86 = 0; (l86 < 2); l86 = (l86 + 1)) {
			fRec107[l86] = 0.0f;
			
		}
		for (int l87 = 0; (l87 < 2); l87 = (l87 + 1)) {
			fRec105[l87] = 0.0f;
			
		}
		for (int l88 = 0; (l88 < 1024); l88 = (l88 + 1)) {
			fVec10[l88] = 0.0f;
			
		}
		for (int l89 = 0; (l89 < 2); l89 = (l89 + 1)) {
			fRec113[l89] = 0.0f;
			
		}
		for (int l90 = 0; (l90 < 2); l90 = (l90 + 1)) {
			fRec111[l90] = 0.0f;
			
		}
		for (int l91 = 0; (l91 < 2); l91 = (l91 + 1)) {
			fRec119[l91] = 0.0f;
			
		}
		for (int l92 = 0; (l92 < 2); l92 = (l92 + 1)) {
			fRec117[l92] = 0.0f;
			
		}
		for (int l93 = 0; (l93 < 2); l93 = (l93 + 1)) {
			fRec116[l93] = 0.0f;
			
		}
		for (int l94 = 0; (l94 < 2); l94 = (l94 + 1)) {
			fRec114[l94] = 0.0f;
			
		}
		for (int l95 = 0; (l95 < 1024); l95 = (l95 + 1)) {
			fVec11[l95] = 0.0f;
			
		}
		for (int l96 = 0; (l96 < 2); l96 = (l96 + 1)) {
			fRec122[l96] = 0.0f;
			
		}
		for (int l97 = 0; (l97 < 2); l97 = (l97 + 1)) {
			fRec120[l97] = 0.0f;
			
		}
		for (int l98 = 0; (l98 < 2); l98 = (l98 + 1)) {
			fRec128[l98] = 0.0f;
			
		}
		for (int l99 = 0; (l99 < 2); l99 = (l99 + 1)) {
			fRec126[l99] = 0.0f;
			
		}
		for (int l100 = 0; (l100 < 2); l100 = (l100 + 1)) {
			fRec125[l100] = 0.0f;
			
		}
		for (int l101 = 0; (l101 < 2); l101 = (l101 + 1)) {
			fRec123[l101] = 0.0f;
			
		}
		for (int l102 = 0; (l102 < 1024); l102 = (l102 + 1)) {
			fVec12[l102] = 0.0f;
			
		}
		for (int l103 = 0; (l103 < 2); l103 = (l103 + 1)) {
			fRec131[l103] = 0.0f;
			
		}
		for (int l104 = 0; (l104 < 2); l104 = (l104 + 1)) {
			fRec129[l104] = 0.0f;
			
		}
		for (int l105 = 0; (l105 < 2); l105 = (l105 + 1)) {
			fRec137[l105] = 0.0f;
			
		}
		for (int l106 = 0; (l106 < 2); l106 = (l106 + 1)) {
			fRec135[l106] = 0.0f;
			
		}
		for (int l107 = 0; (l107 < 2); l107 = (l107 + 1)) {
			fRec134[l107] = 0.0f;
			
		}
		for (int l108 = 0; (l108 < 2); l108 = (l108 + 1)) {
			fRec132[l108] = 0.0f;
			
		}
		for (int l109 = 0; (l109 < 1024); l109 = (l109 + 1)) {
			fVec13[l109] = 0.0f;
			
		}
		for (int l110 = 0; (l110 < 2); l110 = (l110 + 1)) {
			fRec140[l110] = 0.0f;
			
		}
		for (int l111 = 0; (l111 < 2); l111 = (l111 + 1)) {
			fRec138[l111] = 0.0f;
			
		}
		for (int l112 = 0; (l112 < 2); l112 = (l112 + 1)) {
			fRec146[l112] = 0.0f;
			
		}
		for (int l113 = 0; (l113 < 2); l113 = (l113 + 1)) {
			fRec144[l113] = 0.0f;
			
		}
		for (int l114 = 0; (l114 < 2); l114 = (l114 + 1)) {
			fRec143[l114] = 0.0f;
			
		}
		for (int l115 = 0; (l115 < 2); l115 = (l115 + 1)) {
			fRec141[l115] = 0.0f;
			
		}
		for (int l116 = 0; (l116 < 1024); l116 = (l116 + 1)) {
			fVec14[l116] = 0.0f;
			
		}
		for (int l117 = 0; (l117 < 2); l117 = (l117 + 1)) {
			fRec149[l117] = 0.0f;
			
		}
		for (int l118 = 0; (l118 < 2); l118 = (l118 + 1)) {
			fRec147[l118] = 0.0f;
			
		}
		for (int l119 = 0; (l119 < 2); l119 = (l119 + 1)) {
			fRec155[l119] = 0.0f;
			
		}
		for (int l120 = 0; (l120 < 2); l120 = (l120 + 1)) {
			fRec153[l120] = 0.0f;
			
		}
		for (int l121 = 0; (l121 < 2); l121 = (l121 + 1)) {
			fRec152[l121] = 0.0f;
			
		}
		for (int l122 = 0; (l122 < 2); l122 = (l122 + 1)) {
			fRec150[l122] = 0.0f;
			
		}
		for (int l123 = 0; (l123 < 1024); l123 = (l123 + 1)) {
			fVec15[l123] = 0.0f;
			
		}
		for (int l124 = 0; (l124 < 2); l124 = (l124 + 1)) {
			fRec158[l124] = 0.0f;
			
		}
		for (int l125 = 0; (l125 < 2); l125 = (l125 + 1)) {
			fRec156[l125] = 0.0f;
			
		}
		for (int l126 = 0; (l126 < 2); l126 = (l126 + 1)) {
			fRec164[l126] = 0.0f;
			
		}
		for (int l127 = 0; (l127 < 2); l127 = (l127 + 1)) {
			fRec162[l127] = 0.0f;
			
		}
		for (int l128 = 0; (l128 < 2); l128 = (l128 + 1)) {
			fRec161[l128] = 0.0f;
			
		}
		for (int l129 = 0; (l129 < 2); l129 = (l129 + 1)) {
			fRec159[l129] = 0.0f;
			
		}
		for (int l130 = 0; (l130 < 1024); l130 = (l130 + 1)) {
			fVec16[l130] = 0.0f;
			
		}
		for (int l131 = 0; (l131 < 2); l131 = (l131 + 1)) {
			fRec167[l131] = 0.0f;
			
		}
		for (int l132 = 0; (l132 < 2); l132 = (l132 + 1)) {
			fRec165[l132] = 0.0f;
			
		}
		for (int l133 = 0; (l133 < 2); l133 = (l133 + 1)) {
			fRec173[l133] = 0.0f;
			
		}
		for (int l134 = 0; (l134 < 2); l134 = (l134 + 1)) {
			fRec171[l134] = 0.0f;
			
		}
		for (int l135 = 0; (l135 < 2); l135 = (l135 + 1)) {
			fRec170[l135] = 0.0f;
			
		}
		for (int l136 = 0; (l136 < 2); l136 = (l136 + 1)) {
			fRec168[l136] = 0.0f;
			
		}
		for (int l137 = 0; (l137 < 1024); l137 = (l137 + 1)) {
			fVec17[l137] = 0.0f;
			
		}
		for (int l138 = 0; (l138 < 2); l138 = (l138 + 1)) {
			fRec176[l138] = 0.0f;
			
		}
		for (int l139 = 0; (l139 < 2); l139 = (l139 + 1)) {
			fRec174[l139] = 0.0f;
			
		}
		for (int l140 = 0; (l140 < 2); l140 = (l140 + 1)) {
			fRec182[l140] = 0.0f;
			
		}
		for (int l141 = 0; (l141 < 2); l141 = (l141 + 1)) {
			fRec180[l141] = 0.0f;
			
		}
		for (int l142 = 0; (l142 < 2); l142 = (l142 + 1)) {
			fRec179[l142] = 0.0f;
			
		}
		for (int l143 = 0; (l143 < 2); l143 = (l143 + 1)) {
			fRec177[l143] = 0.0f;
			
		}
		for (int l144 = 0; (l144 < 1024); l144 = (l144 + 1)) {
			fVec18[l144] = 0.0f;
			
		}
		for (int l145 = 0; (l145 < 2); l145 = (l145 + 1)) {
			fRec185[l145] = 0.0f;
			
		}
		for (int l146 = 0; (l146 < 2); l146 = (l146 + 1)) {
			fRec183[l146] = 0.0f;
			
		}
		for (int l147 = 0; (l147 < 2); l147 = (l147 + 1)) {
			fRec191[l147] = 0.0f;
			
		}
		for (int l148 = 0; (l148 < 2); l148 = (l148 + 1)) {
			fRec189[l148] = 0.0f;
			
		}
		for (int l149 = 0; (l149 < 2); l149 = (l149 + 1)) {
			fRec188[l149] = 0.0f;
			
		}
		for (int l150 = 0; (l150 < 2); l150 = (l150 + 1)) {
			fRec186[l150] = 0.0f;
			
		}
		for (int l151 = 0; (l151 < 3); l151 = (l151 + 1)) {
			fVec19[l151] = 0.0f;
			
		}
		for (int l152 = 0; (l152 < 2); l152 = (l152 + 1)) {
			fRec194[l152] = 0.0f;
			
		}
		for (int l153 = 0; (l153 < 2); l153 = (l153 + 1)) {
			fRec192[l153] = 0.0f;
			
		}
		for (int l154 = 0; (l154 < 2); l154 = (l154 + 1)) {
			fRec200[l154] = 0.0f;
			
		}
		for (int l155 = 0; (l155 < 2); l155 = (l155 + 1)) {
			fRec198[l155] = 0.0f;
			
		}
		for (int l156 = 0; (l156 < 2); l156 = (l156 + 1)) {
			fRec197[l156] = 0.0f;
			
		}
		for (int l157 = 0; (l157 < 2); l157 = (l157 + 1)) {
			fRec195[l157] = 0.0f;
			
		}
		for (int l158 = 0; (l158 < 2); l158 = (l158 + 1)) {
			fRec203[l158] = 0.0f;
			
		}
		for (int l159 = 0; (l159 < 2); l159 = (l159 + 1)) {
			fRec201[l159] = 0.0f;
			
		}
		for (int l160 = 0; (l160 < 2); l160 = (l160 + 1)) {
			fRec209[l160] = 0.0f;
			
		}
		for (int l161 = 0; (l161 < 2); l161 = (l161 + 1)) {
			fRec207[l161] = 0.0f;
			
		}
		for (int l162 = 0; (l162 < 2); l162 = (l162 + 1)) {
			fRec206[l162] = 0.0f;
			
		}
		for (int l163 = 0; (l163 < 2); l163 = (l163 + 1)) {
			fRec204[l163] = 0.0f;
			
		}
		for (int l164 = 0; (l164 < 2); l164 = (l164 + 1)) {
			fRec212[l164] = 0.0f;
			
		}
		for (int l165 = 0; (l165 < 2); l165 = (l165 + 1)) {
			fRec210[l165] = 0.0f;
			
		}
		for (int l166 = 0; (l166 < 2); l166 = (l166 + 1)) {
			fRec218[l166] = 0.0f;
			
		}
		for (int l167 = 0; (l167 < 2); l167 = (l167 + 1)) {
			fRec216[l167] = 0.0f;
			
		}
		for (int l168 = 0; (l168 < 2); l168 = (l168 + 1)) {
			fRec215[l168] = 0.0f;
			
		}
		for (int l169 = 0; (l169 < 2); l169 = (l169 + 1)) {
			fRec213[l169] = 0.0f;
			
		}
		for (int l170 = 0; (l170 < 3); l170 = (l170 + 1)) {
			fVec20[l170] = 0.0f;
			
		}
		for (int l171 = 0; (l171 < 2); l171 = (l171 + 1)) {
			fRec221[l171] = 0.0f;
			
		}
		for (int l172 = 0; (l172 < 2); l172 = (l172 + 1)) {
			fRec219[l172] = 0.0f;
			
		}
		for (int l173 = 0; (l173 < 2); l173 = (l173 + 1)) {
			fRec227[l173] = 0.0f;
			
		}
		for (int l174 = 0; (l174 < 2); l174 = (l174 + 1)) {
			fRec225[l174] = 0.0f;
			
		}
		for (int l175 = 0; (l175 < 2); l175 = (l175 + 1)) {
			fRec224[l175] = 0.0f;
			
		}
		for (int l176 = 0; (l176 < 2); l176 = (l176 + 1)) {
			fRec222[l176] = 0.0f;
			
		}
		for (int l177 = 0; (l177 < 2); l177 = (l177 + 1)) {
			fRec230[l177] = 0.0f;
			
		}
		for (int l178 = 0; (l178 < 2); l178 = (l178 + 1)) {
			fRec228[l178] = 0.0f;
			
		}
		for (int l179 = 0; (l179 < 2); l179 = (l179 + 1)) {
			fRec236[l179] = 0.0f;
			
		}
		for (int l180 = 0; (l180 < 2); l180 = (l180 + 1)) {
			fRec234[l180] = 0.0f;
			
		}
		for (int l181 = 0; (l181 < 2); l181 = (l181 + 1)) {
			fRec233[l181] = 0.0f;
			
		}
		for (int l182 = 0; (l182 < 2); l182 = (l182 + 1)) {
			fRec231[l182] = 0.0f;
			
		}
		for (int l183 = 0; (l183 < 2); l183 = (l183 + 1)) {
			fRec239[l183] = 0.0f;
			
		}
		for (int l184 = 0; (l184 < 2); l184 = (l184 + 1)) {
			fRec237[l184] = 0.0f;
			
		}
		for (int l185 = 0; (l185 < 2); l185 = (l185 + 1)) {
			fRec245[l185] = 0.0f;
			
		}
		for (int l186 = 0; (l186 < 2); l186 = (l186 + 1)) {
			fRec243[l186] = 0.0f;
			
		}
		for (int l187 = 0; (l187 < 2); l187 = (l187 + 1)) {
			fRec242[l187] = 0.0f;
			
		}
		for (int l188 = 0; (l188 < 2); l188 = (l188 + 1)) {
			fRec240[l188] = 0.0f;
			
		}
		for (int l189 = 0; (l189 < 3); l189 = (l189 + 1)) {
			fVec21[l189] = 0.0f;
			
		}
		for (int l190 = 0; (l190 < 2); l190 = (l190 + 1)) {
			fRec248[l190] = 0.0f;
			
		}
		for (int l191 = 0; (l191 < 2); l191 = (l191 + 1)) {
			fRec246[l191] = 0.0f;
			
		}
		for (int l192 = 0; (l192 < 2); l192 = (l192 + 1)) {
			fRec254[l192] = 0.0f;
			
		}
		for (int l193 = 0; (l193 < 2); l193 = (l193 + 1)) {
			fRec252[l193] = 0.0f;
			
		}
		for (int l194 = 0; (l194 < 2); l194 = (l194 + 1)) {
			fRec251[l194] = 0.0f;
			
		}
		for (int l195 = 0; (l195 < 2); l195 = (l195 + 1)) {
			fRec249[l195] = 0.0f;
			
		}
		for (int l196 = 0; (l196 < 2); l196 = (l196 + 1)) {
			fRec257[l196] = 0.0f;
			
		}
		for (int l197 = 0; (l197 < 2); l197 = (l197 + 1)) {
			fRec255[l197] = 0.0f;
			
		}
		for (int l198 = 0; (l198 < 2); l198 = (l198 + 1)) {
			fRec263[l198] = 0.0f;
			
		}
		for (int l199 = 0; (l199 < 2); l199 = (l199 + 1)) {
			fRec261[l199] = 0.0f;
			
		}
		for (int l200 = 0; (l200 < 2); l200 = (l200 + 1)) {
			fRec260[l200] = 0.0f;
			
		}
		for (int l201 = 0; (l201 < 2); l201 = (l201 + 1)) {
			fRec258[l201] = 0.0f;
			
		}
		for (int l202 = 0; (l202 < 2); l202 = (l202 + 1)) {
			fRec266[l202] = 0.0f;
			
		}
		for (int l203 = 0; (l203 < 2); l203 = (l203 + 1)) {
			fRec264[l203] = 0.0f;
			
		}
		for (int l204 = 0; (l204 < 2); l204 = (l204 + 1)) {
			fRec272[l204] = 0.0f;
			
		}
		for (int l205 = 0; (l205 < 2); l205 = (l205 + 1)) {
			fRec270[l205] = 0.0f;
			
		}
		for (int l206 = 0; (l206 < 2); l206 = (l206 + 1)) {
			fRec269[l206] = 0.0f;
			
		}
		for (int l207 = 0; (l207 < 2); l207 = (l207 + 1)) {
			fRec267[l207] = 0.0f;
			
		}
		for (int l208 = 0; (l208 < 3); l208 = (l208 + 1)) {
			fVec22[l208] = 0.0f;
			
		}
		for (int l209 = 0; (l209 < 2); l209 = (l209 + 1)) {
			fRec275[l209] = 0.0f;
			
		}
		for (int l210 = 0; (l210 < 2); l210 = (l210 + 1)) {
			fRec273[l210] = 0.0f;
			
		}
		for (int l211 = 0; (l211 < 2); l211 = (l211 + 1)) {
			fRec281[l211] = 0.0f;
			
		}
		for (int l212 = 0; (l212 < 2); l212 = (l212 + 1)) {
			fRec279[l212] = 0.0f;
			
		}
		for (int l213 = 0; (l213 < 2); l213 = (l213 + 1)) {
			fRec278[l213] = 0.0f;
			
		}
		for (int l214 = 0; (l214 < 2); l214 = (l214 + 1)) {
			fRec276[l214] = 0.0f;
			
		}
		for (int l215 = 0; (l215 < 2); l215 = (l215 + 1)) {
			fRec284[l215] = 0.0f;
			
		}
		for (int l216 = 0; (l216 < 2); l216 = (l216 + 1)) {
			fRec282[l216] = 0.0f;
			
		}
		for (int l217 = 0; (l217 < 2); l217 = (l217 + 1)) {
			fRec290[l217] = 0.0f;
			
		}
		for (int l218 = 0; (l218 < 2); l218 = (l218 + 1)) {
			fRec288[l218] = 0.0f;
			
		}
		for (int l219 = 0; (l219 < 2); l219 = (l219 + 1)) {
			fRec287[l219] = 0.0f;
			
		}
		for (int l220 = 0; (l220 < 2); l220 = (l220 + 1)) {
			fRec285[l220] = 0.0f;
			
		}
		
	}
	
	virtual void init(int samplingFreq) {
		classInit(samplingFreq);
		instanceInit(samplingFreq);
	}
	virtual void instanceInit(int samplingFreq) {
		instanceConstants(samplingFreq);
		instanceResetUserInterface();
		instanceClear();
	}
	
	virtual mydsp* clone() {
		return new mydsp();
	}
	virtual int getSampleRate() {
		return fSamplingFreq;
		
	}
	
	virtual void buildUserInterface(UI* ui_interface) {
		ui_interface->openVerticalBox("SATOsw0o2");
		ui_interface->declare(&fHslider2, "unit", "dB");
		ui_interface->addHorizontalSlider("gain", &fHslider2, -10.0f, -30.0f, 10.0f, 1.0f);
		ui_interface->declare(&fHslider1, "unit", "dB");
		ui_interface->addHorizontalSlider("lf/hf", &fHslider1, 0.0f, -3.0f, 3.0f, 0.100000001f);
		ui_interface->addCheckButton("mute", &fCheckbox0);
		ui_interface->declare(&fHslider0, "unit", "Hz");
		ui_interface->addHorizontalSlider("xover", &fHslider0, 400.0f, 200.0f, 800.0f, 20.0f);
		ui_interface->closeBox();
		
	}
	
	virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) {
		FAUSTFLOAT* input0 = inputs[0];
		FAUSTFLOAT* input1 = inputs[1];
		FAUSTFLOAT* input2 = inputs[2];
		FAUSTFLOAT* input3 = inputs[3];
		FAUSTFLOAT* input4 = inputs[4];
		FAUSTFLOAT* input5 = inputs[5];
		FAUSTFLOAT* input6 = inputs[6];
		FAUSTFLOAT* input7 = inputs[7];
		FAUSTFLOAT* input8 = inputs[8];
		FAUSTFLOAT* output0 = outputs[0];
		FAUSTFLOAT* output1 = outputs[1];
		FAUSTFLOAT* output2 = outputs[2];
		FAUSTFLOAT* output3 = outputs[3];
		FAUSTFLOAT* output4 = outputs[4];
		FAUSTFLOAT* output5 = outputs[5];
		FAUSTFLOAT* output6 = outputs[6];
		FAUSTFLOAT* output7 = outputs[7];
		FAUSTFLOAT* output8 = outputs[8];
		FAUSTFLOAT* output9 = outputs[9];
		FAUSTFLOAT* output10 = outputs[10];
		FAUSTFLOAT* output11 = outputs[11];
		FAUSTFLOAT* output12 = outputs[12];
		FAUSTFLOAT* output13 = outputs[13];
		FAUSTFLOAT* output14 = outputs[14];
		FAUSTFLOAT* output15 = outputs[15];
		FAUSTFLOAT* output16 = outputs[16];
		FAUSTFLOAT* output17 = outputs[17];
		FAUSTFLOAT* output18 = outputs[18];
		FAUSTFLOAT* output19 = outputs[19];
		FAUSTFLOAT* output20 = outputs[20];
		FAUSTFLOAT* output21 = outputs[21];
		FAUSTFLOAT* output22 = outputs[22];
		FAUSTFLOAT* output23 = outputs[23];
		FAUSTFLOAT* output24 = outputs[24];
		FAUSTFLOAT* output25 = outputs[25];
		FAUSTFLOAT* output26 = outputs[26];
		FAUSTFLOAT* output27 = outputs[27];
		FAUSTFLOAT* output28 = outputs[28];
		FAUSTFLOAT* output29 = outputs[29];
		FAUSTFLOAT* output30 = outputs[30];
		float fSlow0 = (0.00100000005f * float(fHslider0));
		float fSlow1 = (0.00100000005f * powf(10.0f, (0.0500000007f * float(fHslider1))));
		float fSlow2 = (0.00100000005f * (float((float(fCheckbox0) < 0.5f)) * powf(10.0f, (0.0500000007f * float(fHslider2)))));
		for (int i = 0; (i < count); i = (i + 1)) {
			fRec3[0] = (fSlow0 + (0.999000013f * fRec3[1]));
			float fTemp0 = tanf((fConst4 * fRec3[0]));
			float fTemp1 = mydsp_faustpower2_f(fTemp0);
			float fTemp2 = ((fTemp0 * (fTemp0 + -2.0f)) + 1.0f);
			float fTemp3 = (fTemp1 + -1.0f);
			float fTemp4 = ((fTemp0 * (fTemp0 + 2.0f)) + 1.0f);
			fRec4[0] = (float(input2[i]) - (((fTemp2 * fRec4[2]) + (2.0f * (fTemp3 * fRec4[1]))) / fTemp4));
			fRec5[0] = (fSlow1 + (0.999000013f * fRec5[1]));
			float fTemp5 = (fRec5[0] * fTemp4);
			float fTemp6 = (0.0f - (2.0f / fTemp4));
			float fTemp7 = (((fTemp1 * (fRec4[2] + (fRec4[0] + (2.0f * fRec4[1])))) / fTemp5) + (0.774596691f * (fRec5[0] * (0.0f - ((fTemp6 * fRec4[1]) + ((fRec4[0] + fRec4[2]) / fTemp4))))));
			fRec6[0] = (float(input3[i]) - (((fTemp2 * fRec6[2]) + (2.0f * (fTemp3 * fRec6[1]))) / fTemp4));
			float fTemp8 = (((fTemp1 * (fRec6[2] + (fRec6[0] + (2.0f * fRec6[1])))) / fTemp5) + (0.774596691f * (fRec5[0] * (0.0f - ((fTemp6 * fRec6[1]) + ((fRec6[0] + fRec6[2]) / fTemp4))))));
			fRec7[0] = (float(input1[i]) - (((fTemp2 * fRec7[2]) + (2.0f * (fTemp3 * fRec7[1]))) / fTemp4));
			float fTemp9 = (((fTemp1 * (fRec7[2] + (fRec7[0] + (2.0f * fRec7[1])))) / fTemp5) + (0.774596691f * (fRec5[0] * (0.0f - ((fTemp6 * fRec7[1]) + ((fRec7[0] + fRec7[2]) / fTemp4))))));
			float fTemp10 = (fConst3 * (((0.144552425f * fTemp7) + (4.70970008e-06f * fTemp8)) - (4.80300014e-06f * fTemp9)));
			float fTemp11 = (fConst5 * fRec0[1]);
			fRec2[0] = (fTemp10 + (fRec2[1] + fTemp11));
			fRec0[0] = fRec2[0];
			float fRec1 = (fTemp11 + fTemp10);
			fRec14[0] = (float(input8[i]) - (((fTemp2 * fRec14[2]) + (2.0f * (fTemp3 * fRec14[1]))) / fTemp4));
			float fTemp12 = ((0.400000006f * (fRec5[0] * (0.0f - ((fTemp6 * fRec14[1]) + ((fRec14[0] + fRec14[2]) / fTemp4))))) + ((fTemp1 * (fRec14[2] + (fRec14[0] + (2.0f * fRec14[1])))) / fTemp5));
			fRec15[0] = (float(input4[i]) - (((fTemp2 * fRec15[2]) + (2.0f * (fTemp3 * fRec15[1]))) / fTemp4));
			float fTemp13 = (((fTemp1 * (fRec15[2] + (fRec15[0] + (2.0f * fRec15[1])))) / fTemp5) + (0.400000006f * (fRec5[0] * (0.0f - ((fTemp6 * fRec15[1]) + ((fRec15[0] + fRec15[2]) / fTemp4))))));
			fRec16[0] = (float(input6[i]) - (((fTemp2 * fRec16[2]) + (2.0f * (fTemp3 * fRec16[1]))) / fTemp4));
			float fTemp14 = (((fTemp1 * (fRec16[2] + (fRec16[0] + (2.0f * fRec16[1])))) / fTemp5) + (0.400000006f * (fRec5[0] * (0.0f - ((fTemp6 * fRec16[1]) + ((fRec16[0] + fRec16[2]) / fTemp4))))));
			fRec17[0] = (float(input7[i]) - (((fTemp2 * fRec17[2]) + (2.0f * (fTemp3 * fRec17[1]))) / fTemp4));
			float fTemp15 = (((fTemp1 * (fRec17[2] + (fRec17[0] + (2.0f * fRec17[1])))) / fTemp5) + (0.400000006f * (fRec5[0] * (0.0f - ((fTemp6 * fRec17[1]) + ((fRec17[0] + fRec17[2]) / fTemp4))))));
			fRec18[0] = (float(input5[i]) - (((fTemp2 * fRec18[2]) + (2.0f * (fTemp3 * fRec18[1]))) / fTemp4));
			float fTemp16 = (((fTemp1 * (fRec18[2] + (fRec18[0] + (2.0f * fRec18[1])))) / fTemp5) + (0.400000006f * (fRec5[0] * (0.0f - ((fTemp6 * fRec18[1]) + ((fRec18[0] + fRec18[2]) / fTemp4))))));
			float fTemp17 = (fConst7 * (((((1.14950001e-06f * fTemp12) + (1.98604994e-05f * fTemp13)) + (0.141460821f * fTemp14)) + (6.64250001e-06f * fTemp15)) - (7.13349982e-06f * fTemp16)));
			float fTemp18 = (fConst8 * fRec11[1]);
			float fTemp19 = (fConst10 * fRec8[1]);
			fRec13[0] = (((fTemp17 + fRec13[1]) + fTemp18) + fTemp19);
			fRec11[0] = fRec13[0];
			float fRec12 = ((fTemp17 + fTemp18) + fTemp19);
			fRec10[0] = (fRec11[0] + fRec10[1]);
			fRec8[0] = fRec10[0];
			float fRec9 = fRec12;
			fRec19[0] = (float(input0[i]) - (((fRec19[2] * fTemp2) + (2.0f * (fRec19[1] * fTemp3))) / fTemp4));
			float fTemp20 = (((fTemp1 * (fRec19[2] + (fRec19[0] + (2.0f * fRec19[1])))) / fTemp5) + (fRec5[0] * (0.0f - ((fRec19[1] * fTemp6) + ((fRec19[0] + fRec19[2]) / fTemp4)))));
			fVec0[(IOTA & 2047)] = ((fRec1 + fRec9) + (0.0951942503f * fTemp20));
			fRec20[0] = (fSlow2 + (0.999000013f * fRec20[1]));
			output0[i] = FAUSTFLOAT((0.622610331f * (fVec0[((IOTA - iConst11) & 2047)] * fRec20[0])));
			float fTemp21 = (fConst13 * (((0.0640498549f * fTemp9) + (0.0632267669f * fTemp7)) + (0.0215868391f * fTemp8)));
			float fTemp22 = (fConst14 * fRec21[1]);
			fRec23[0] = (fTemp21 + (fRec23[1] + fTemp22));
			fRec21[0] = fRec23[0];
			float fRec22 = (fTemp22 + fTemp21);
			float fTemp23 = (fConst16 * (((((0.0286792312f * fTemp13) + (0.0842565075f * fTemp16)) + (0.0191042144f * fTemp14)) + (0.0264974907f * fTemp15)) - (0.0353156179f * fTemp12)));
			float fTemp24 = (fConst17 * fRec24[1]);
			float fTemp25 = (fConst18 * fRec27[1]);
			fRec29[0] = (fTemp23 + (fTemp24 + (fRec29[1] + fTemp25)));
			fRec27[0] = fRec29[0];
			float fRec28 = ((fTemp25 + fTemp24) + fTemp23);
			fRec26[0] = (fRec26[1] + fRec27[0]);
			fRec24[0] = fRec26[0];
			float fRec25 = fRec28;
			fVec1[(IOTA & 2047)] = ((0.0579515621f * fTemp20) + (fRec22 + fRec25));
			output1[i] = FAUSTFLOAT((0.717061102f * (fVec1[((IOTA - iConst19) & 2047)] * fRec20[0])));
			float fTemp26 = (fConst21 * (((0.0527678393f * fTemp9) + (0.0669136494f * fTemp7)) - (0.0527719259f * fTemp8)));
			float fTemp27 = (fConst22 * fRec30[1]);
			fRec32[0] = (fTemp26 + (fRec32[1] + fTemp27));
			fRec30[0] = fRec32[0];
			float fRec31 = (fTemp27 + fTemp26);
			float fTemp28 = (fConst24 * (((0.066809155f * fTemp16) + (0.0161149334f * fTemp14)) - ((0.0668209344f * fTemp15) + ((0.0488329753f * fTemp13) + (1.61180003e-06f * fTemp12)))));
			float fTemp29 = (fConst25 * fRec33[1]);
			float fTemp30 = (fConst26 * fRec36[1]);
			fRec38[0] = (fTemp28 + (fTemp29 + (fRec38[1] + fTemp30)));
			fRec36[0] = fRec38[0];
			float fRec37 = ((fTemp30 + fTemp29) + fTemp28);
			fRec35[0] = (fRec35[1] + fRec36[0]);
			fRec33[0] = fRec35[0];
			float fRec34 = fRec37;
			fVec2[(IOTA & 2047)] = ((0.0633188263f * fTemp20) + (fRec31 + fRec34));
			output2[i] = FAUSTFLOAT((0.716854393f * (fVec2[((IOTA - iConst27) & 2047)] * fRec20[0])));
			float fTemp31 = (fConst13 * ((0.0595454238f * fTemp7) - ((0.0156096201f * fTemp9) + (0.0582364872f * fTemp8))));
			float fTemp32 = (fConst14 * fRec39[1]);
			fRec41[0] = (fTemp31 + (fRec41[1] + fTemp32));
			fRec39[0] = fRec41[0];
			float fRec40 = (fTemp32 + fTemp31);
			float fTemp33 = (fConst16 * (((0.022095751f * fTemp14) + ((0.02052035f * fTemp13) + (0.0355290733f * fTemp12))) - ((0.0212301388f * fTemp16) + (0.0792069137f * fTemp15))));
			float fTemp34 = (fConst17 * fRec42[1]);
			float fTemp35 = (fConst18 * fRec45[1]);
			fRec47[0] = (fTemp33 + (fTemp34 + (fRec47[1] + fTemp35)));
			fRec45[0] = fRec47[0];
			float fRec46 = ((fTemp35 + fTemp34) + fTemp33);
			fRec44[0] = (fRec44[1] + fRec45[0]);
			fRec42[0] = fRec44[0];
			float fRec43 = fRec46;
			fVec3[(IOTA & 2047)] = ((0.0525917113f * fTemp20) + (fRec40 + fRec43));
			output3[i] = FAUSTFLOAT((0.717061102f * (fVec3[((IOTA - iConst19) & 2047)] * fRec20[0])));
			float fTemp36 = (fConst13 * ((0.063209936f * fTemp7) - ((0.0640460402f * fTemp9) + (0.0216016546f * fTemp8))));
			float fTemp37 = (fConst14 * fRec48[1]);
			fRec50[0] = (fTemp36 + (fRec50[1] + fTemp37));
			fRec48[0] = fRec50[0];
			float fRec49 = (fTemp37 + fTemp36);
			float fTemp38 = (fConst16 * (((0.0287043639f * fTemp13) + (0.0190891717f * fTemp14)) - ((0.0265128072f * fTemp15) + ((0.0842392817f * fTemp16) + (0.0353211984f * fTemp12)))));
			float fTemp39 = (fConst17 * fRec51[1]);
			float fTemp40 = (fConst18 * fRec54[1]);
			fRec56[0] = (fTemp38 + (fTemp39 + (fRec56[1] + fTemp40)));
			fRec54[0] = fRec56[0];
			float fRec55 = ((fTemp40 + fTemp39) + fTemp38);
			fRec53[0] = (fRec53[1] + fRec54[0]);
			fRec51[0] = fRec53[0];
			float fRec52 = fRec55;
			fVec4[(IOTA & 2047)] = ((0.0579424426f * fTemp20) + (fRec49 + fRec52));
			output4[i] = FAUSTFLOAT((0.717061102f * (fVec4[((IOTA - iConst19) & 2047)] * fRec20[0])));
			float fTemp41 = (fConst21 * (((0.0669162646f * fTemp7) + (0.0527649f * fTemp8)) - (0.0527561419f * fTemp9)));
			float fTemp42 = (fConst22 * fRec57[1]);
			fRec59[0] = (fTemp41 + (fRec59[1] + fTemp42));
			fRec57[0] = fRec59[0];
			float fRec58 = (fTemp42 + fTemp41);
			float fTemp43 = (fConst24 * (((0.0668131635f * fTemp15) + ((0.0161357336f * fTemp14) + (1.16084002e-05f * fTemp12))) - ((0.0488342457f * fTemp13) + (0.0668111816f * fTemp16))));
			float fTemp44 = (fConst25 * fRec60[1]);
			float fTemp45 = (fConst26 * fRec63[1]);
			fRec65[0] = (fTemp43 + (fTemp44 + (fRec65[1] + fTemp45)));
			fRec63[0] = fRec65[0];
			float fRec64 = ((fTemp45 + fTemp44) + fTemp43);
			fRec62[0] = (fRec62[1] + fRec63[0]);
			fRec60[0] = fRec62[0];
			float fRec61 = fRec64;
			fVec5[(IOTA & 2047)] = ((0.06331072f * fTemp20) + (fRec58 + fRec61));
			output5[i] = FAUSTFLOAT((0.716854393f * (fVec5[((IOTA - iConst27) & 2047)] * fRec20[0])));
			float fTemp46 = (fConst13 * (((0.0156095503f * fTemp9) + (0.0595348626f * fTemp7)) + (0.0582405664f * fTemp8)));
			float fTemp47 = (fConst14 * fRec66[1]);
			fRec68[0] = (fTemp46 + (fRec68[1] + fTemp47));
			fRec66[0] = fRec68[0];
			float fRec67 = (fTemp47 + fTemp46);
			float fTemp48 = (fConst16 * ((0.0792068467f * fTemp15) + ((0.0220794007f * fTemp14) + ((0.0212266501f * fTemp16) + ((0.0205246415f * fTemp13) + (0.035535343f * fTemp12))))));
			float fTemp49 = (fConst17 * fRec69[1]);
			float fTemp50 = (fConst18 * fRec72[1]);
			fRec74[0] = (fTemp48 + (fTemp49 + (fRec74[1] + fTemp50)));
			fRec72[0] = fRec74[0];
			float fRec73 = ((fTemp50 + fTemp49) + fTemp48);
			fRec71[0] = (fRec71[1] + fRec72[0]);
			fRec69[0] = fRec71[0];
			float fRec70 = fRec73;
			fVec6[(IOTA & 2047)] = ((0.052587714f * fTemp20) + (fRec67 + fRec70));
			output6[i] = FAUSTFLOAT((0.717061102f * (fVec6[((IOTA - iConst19) & 2047)] * fRec20[0])));
			float fTemp51 = (fConst29 * (((0.0479171462f * fTemp9) + (0.0110148918f * fTemp7)) - (0.00271321135f * fTemp8)));
			float fTemp52 = (fConst30 * fRec75[1]);
			fRec77[0] = (fTemp51 + (fRec77[1] + fTemp52));
			fRec75[0] = fRec77[0];
			float fRec76 = (fTemp52 + fTemp51);
			float fTemp53 = (fConst32 * ((0.0218717307f * fTemp16) - ((0.0025010386f * fTemp15) + ((0.0249028355f * fTemp14) + ((0.0049932464f * fTemp13) + (0.047650516f * fTemp12))))));
			float fTemp54 = (fConst33 * fRec78[1]);
			float fTemp55 = (fConst34 * fRec81[1]);
			fRec83[0] = (fTemp53 + (fTemp54 + (fRec83[1] + fTemp55)));
			fRec81[0] = fRec83[0];
			float fRec82 = ((fTemp55 + fTemp54) + fTemp53);
			fRec80[0] = (fRec80[1] + fRec81[0]);
			fRec78[0] = fRec80[0];
			float fRec79 = fRec82;
			fVec7[(IOTA & 1023)] = ((0.0296740122f * fTemp20) + (fRec76 + fRec79));
			output7[i] = FAUSTFLOAT((0.825255752f * (fVec7[((IOTA - iConst35) & 1023)] * fRec20[0])));
			float fTemp56 = (fConst37 * (((0.0460188352f * fTemp9) + (0.00239910278f * fTemp7)) - (0.0275209881f * fTemp8)));
			float fTemp57 = (fConst38 * fRec84[1]);
			fRec86[0] = (fTemp56 + (fRec86[1] + fTemp57));
			fRec84[0] = fRec86[0];
			float fRec85 = (fTemp57 + fTemp56);
			float fTemp58 = (fConst40 * ((0.00402585231f * fTemp16) - ((0.00305585004f * fTemp15) + ((0.0318393186f * fTemp14) + ((0.047910817f * fTemp13) + (0.0257446915f * fTemp12))))));
			float fTemp59 = (fConst41 * fRec87[1]);
			float fTemp60 = (fConst42 * fRec90[1]);
			fRec92[0] = (fTemp58 + (fTemp59 + (fRec92[1] + fTemp60)));
			fRec90[0] = fRec92[0];
			float fRec91 = ((fTemp60 + fTemp59) + fTemp58);
			fRec89[0] = (fRec89[1] + fRec90[0]);
			fRec87[0] = fRec89[0];
			float fRec88 = fRec91;
			fVec8[(IOTA & 1023)] = ((0.0324214213f * fTemp20) + (fRec85 + fRec88));
			output8[i] = FAUSTFLOAT((0.825772464f * (fVec8[((IOTA - iConst43) & 1023)] * fRec20[0])));
			float fTemp61 = (fConst37 * (((0.0206746776f * fTemp9) + (0.00365679874f * fTemp7)) - (0.0392839573f * fTemp8)));
			float fTemp62 = (fConst38 * fRec93[1]);
			fRec95[0] = (fTemp61 + (fRec95[1] + fTemp62));
			fRec93[0] = fRec95[0];
			float fRec94 = (fTemp62 + fTemp61);
			float fTemp63 = (fConst40 * (((0.00495913578f * fTemp16) + (0.0259169079f * fTemp12)) - (((0.0375389159f * fTemp13) + (0.0261093918f * fTemp14)) + (0.00595670892f * fTemp15))));
			float fTemp64 = (fConst41 * fRec96[1]);
			float fTemp65 = (fConst42 * fRec99[1]);
			fRec101[0] = (fTemp63 + (fTemp64 + (fRec101[1] + fTemp65)));
			fRec99[0] = fRec101[0];
			float fRec100 = ((fTemp65 + fTemp64) + fTemp63);
			fRec98[0] = (fRec98[1] + fRec99[0]);
			fRec96[0] = fRec98[0];
			float fRec97 = fRec100;
			fVec9[(IOTA & 1023)] = ((0.0267503597f * fTemp20) + (fRec94 + fRec97));
			output9[i] = FAUSTFLOAT((0.825772464f * (fVec9[((IOTA - iConst43) & 1023)] * fRec20[0])));
			float fTemp66 = (fConst29 * (((0.00272243051f * fTemp9) + (0.0110227168f * fTemp7)) - (0.0479010195f * fTemp8)));
			float fTemp67 = (fConst30 * fRec102[1]);
			fRec104[0] = (fTemp66 + (fRec104[1] + fTemp67));
			fRec102[0] = fRec104[0];
			float fRec103 = (fTemp67 + fTemp66);
			float fTemp68 = (fConst32 * (((0.00250748661f * fTemp16) + (0.0476305895f * fTemp12)) - (((0.00501013687f * fTemp13) + (0.0248913877f * fTemp14)) + (0.0218859967f * fTemp15))));
			float fTemp69 = (fConst33 * fRec105[1]);
			float fTemp70 = (fConst34 * fRec108[1]);
			fRec110[0] = (fTemp68 + (fTemp69 + (fRec110[1] + fTemp70)));
			fRec108[0] = fRec110[0];
			float fRec109 = ((fTemp70 + fTemp69) + fTemp68);
			fRec107[0] = (fRec107[1] + fRec108[0]);
			fRec105[0] = fRec107[0];
			float fRec106 = fRec109;
			fVec10[(IOTA & 1023)] = ((0.029665513f * fTemp20) + (fRec103 + fRec106));
			output10[i] = FAUSTFLOAT((0.825255752f * (fVec10[((IOTA - iConst35) & 1023)] * fRec20[0])));
			float fTemp71 = (fConst37 * ((0.00846940465f * fTemp7) - ((0.0355539881f * fTemp9) + (0.0561628602f * fTemp8))));
			float fTemp72 = (fConst38 * fRec111[1]);
			fRec113[0] = (fTemp71 + (fRec113[1] + fTemp72));
			fRec111[0] = fRec113[0];
			float fRec112 = (fTemp72 + fTemp71);
			float fTemp73 = (fConst40 * (((0.0591501631f * fTemp13) + (0.0283936244f * fTemp12)) - (((0.0104484083f * fTemp16) + (0.0363615789f * fTemp14)) + (0.0130950417f * fTemp15))));
			float fTemp74 = (fConst41 * fRec114[1]);
			float fTemp75 = (fConst42 * fRec117[1]);
			fRec119[0] = (fTemp73 + (fTemp74 + (fRec119[1] + fTemp75)));
			fRec117[0] = fRec119[0];
			float fRec118 = ((fTemp75 + fTemp74) + fTemp73);
			fRec116[0] = (fRec116[1] + fRec117[0]);
			fRec114[0] = fRec116[0];
			float fRec115 = fRec118;
			fVec11[(IOTA & 1023)] = ((0.0410089977f * fTemp20) + (fRec112 + fRec115));
			output11[i] = FAUSTFLOAT((0.825772464f * (fVec11[((IOTA - iConst43) & 1023)] * fRec20[0])));
			float fTemp76 = (fConst37 * ((0.0036629024f * fTemp7) - ((0.040098194f * fTemp9) + (0.0192500781f * fTemp8))));
			float fTemp77 = (fConst38 * fRec120[1]);
			fRec122[0] = (fTemp76 + (fRec122[1] + fTemp77));
			fRec120[0] = fRec122[0];
			float fRec121 = (fTemp77 + fTemp76);
			float fTemp78 = (fConst40 * ((0.0358814336f * fTemp13) - ((0.00387390563f * fTemp15) + ((0.0261035636f * fTemp14) + ((0.00659731729f * fTemp16) + (0.0287762228f * fTemp12))))));
			float fTemp79 = (fConst41 * fRec123[1]);
			float fTemp80 = (fConst42 * fRec126[1]);
			fRec128[0] = (fTemp78 + (fTemp79 + (fRec128[1] + fTemp80)));
			fRec126[0] = fRec128[0];
			float fRec127 = ((fTemp80 + fTemp79) + fTemp78);
			fRec125[0] = (fRec125[1] + fRec126[0]);
			fRec123[0] = fRec125[0];
			float fRec124 = fRec127;
			fVec12[(IOTA & 1023)] = ((0.0267459098f * fTemp20) + (fRec121 + fRec124));
			output12[i] = FAUSTFLOAT((0.825772464f * (fVec12[((IOTA - iConst43) & 1023)] * fRec20[0])));
			float fTemp81 = (fConst29 * (((0.0110191144f * fTemp7) + (0.00271522603f * fTemp8)) - (0.0478933044f * fTemp9)));
			float fTemp82 = (fConst30 * fRec129[1]);
			fRec131[0] = (fTemp81 + (fRec131[1] + fTemp82));
			fRec129[0] = fRec131[0];
			float fRec130 = (fTemp82 + fTemp81);
			float fTemp83 = (fConst32 * ((0.0025117835f * fTemp15) - ((0.0248842426f * fTemp14) + ((0.0218761284f * fTemp16) + ((0.00499273418f * fTemp13) + (0.0476244874f * fTemp12))))));
			float fTemp84 = (fConst33 * fRec132[1]);
			float fTemp85 = (fConst34 * fRec135[1]);
			fRec137[0] = (fTemp83 + (fTemp84 + (fRec137[1] + fTemp85)));
			fRec135[0] = fRec137[0];
			float fRec136 = ((fTemp85 + fTemp84) + fTemp83);
			fRec134[0] = (fRec134[1] + fRec135[0]);
			fRec132[0] = fRec134[0];
			float fRec133 = fRec136;
			fVec13[(IOTA & 1023)] = ((0.0296611208f * fTemp20) + (fRec130 + fRec133));
			output13[i] = FAUSTFLOAT((0.825255752f * (fVec13[((IOTA - iConst35) & 1023)] * fRec20[0])));
			float fTemp86 = (fConst37 * (((0.0036679192f * fTemp7) + (0.0206729658f * fTemp8)) - (0.0392920151f * fTemp9)));
			float fTemp87 = (fConst38 * fRec138[1]);
			fRec140[0] = (fTemp86 + (fRec140[1] + fTemp87));
			fRec138[0] = fRec140[0];
			float fRec139 = (fTemp87 + fTemp86);
			float fTemp88 = (fConst40 * ((0.00496469717f * fTemp15) - ((0.0261131581f * fTemp14) + ((0.00598042877f * fTemp16) + ((0.0375401974f * fTemp13) + (0.0259301588f * fTemp12))))));
			float fTemp89 = (fConst41 * fRec141[1]);
			float fTemp90 = (fConst42 * fRec144[1]);
			fRec146[0] = (fTemp88 + (fTemp89 + (fRec146[1] + fTemp90)));
			fRec144[0] = fRec146[0];
			float fRec145 = ((fTemp90 + fTemp89) + fTemp88);
			fRec143[0] = (fRec143[1] + fRec144[0]);
			fRec141[0] = fRec143[0];
			float fRec142 = fRec145;
			fVec14[(IOTA & 1023)] = ((0.026753651f * fTemp20) + (fRec139 + fRec142));
			output14[i] = FAUSTFLOAT((0.825772464f * (fVec14[((IOTA - iConst43) & 1023)] * fRec20[0])));
			float fTemp91 = (fConst37 * (((0.00239751418f * fTemp7) + (0.0460310802f * fTemp8)) - (0.0275247861f * fTemp9)));
			float fTemp92 = (fConst38 * fRec147[1]);
			fRec149[0] = (fTemp91 + (fRec149[1] + fTemp92));
			fRec147[0] = fRec149[0];
			float fRec148 = (fTemp92 + fTemp91);
			float fTemp93 = (fConst40 * (((0.00402801251f * fTemp15) + (0.0257554352f * fTemp12)) - (((0.0479180031f * fTemp13) + (0.00304666511f * fTemp16)) + (0.031846758f * fTemp14))));
			float fTemp94 = (fConst41 * fRec150[1]);
			float fTemp95 = (fConst42 * fRec153[1]);
			fRec155[0] = (fTemp93 + (fTemp94 + (fRec155[1] + fTemp95)));
			fRec153[0] = fRec155[0];
			float fRec154 = ((fTemp95 + fTemp94) + fTemp93);
			fRec152[0] = (fRec152[1] + fRec153[0]);
			fRec150[0] = fRec152[0];
			float fRec151 = fRec154;
			fVec15[(IOTA & 1023)] = ((0.0324291773f * fTemp20) + (fRec148 + fRec151));
			output15[i] = FAUSTFLOAT((0.825772464f * (fVec15[((IOTA - iConst43) & 1023)] * fRec20[0])));
			float fTemp96 = (fConst29 * (((0.0110191759f * fTemp7) + (0.0479075424f * fTemp8)) - (0.00271278713f * fTemp9)));
			float fTemp97 = (fConst30 * fRec156[1]);
			fRec158[0] = (fTemp96 + (fRec158[1] + fTemp97));
			fRec156[0] = fRec158[0];
			float fRec157 = (fTemp97 + fTemp96);
			float fTemp98 = (fConst32 * (((0.0218786057f * fTemp15) + (0.0476387031f * fTemp12)) - (((0.0049914103f * fTemp13) + (0.00250061741f * fTemp16)) + (0.0248947348f * fTemp14))));
			float fTemp99 = (fConst34 * fRec162[1]);
			float fTemp100 = (fConst33 * fRec159[1]);
			fRec164[0] = (((fTemp98 + fRec164[1]) + fTemp99) + fTemp100);
			fRec162[0] = fRec164[0];
			float fRec163 = ((fTemp98 + fTemp99) + fTemp100);
			fRec161[0] = (fRec161[1] + fRec162[0]);
			fRec159[0] = fRec161[0];
			float fRec160 = fRec163;
			fVec16[(IOTA & 1023)] = ((fRec157 + fRec160) + (0.0296692289f * fTemp20));
			output16[i] = FAUSTFLOAT((0.825255752f * (fVec16[((IOTA - iConst35) & 1023)] * fRec20[0])));
			float fTemp101 = (fConst37 * (((0.0355585851f * fTemp9) + (0.00846020691f * fTemp7)) + (0.0561676435f * fTemp8)));
			float fTemp102 = (fConst38 * fRec165[1]);
			fRec167[0] = (fTemp101 + (fRec167[1] + fTemp102));
			fRec165[0] = fRec167[0];
			float fRec166 = (fTemp102 + fTemp101);
			float fTemp103 = (fConst40 * (((0.013077273f * fTemp15) + ((0.0104403626f * fTemp16) + ((0.05915609f * fTemp13) + (0.028395867f * fTemp12)))) - (0.0363653377f * fTemp14)));
			float fTemp104 = (fConst41 * fRec168[1]);
			float fTemp105 = (fConst42 * fRec171[1]);
			fRec173[0] = (fTemp103 + (fTemp104 + (fRec173[1] + fTemp105)));
			fRec171[0] = fRec173[0];
			float fRec172 = ((fTemp105 + fTemp104) + fTemp103);
			fRec170[0] = (fRec170[1] + fRec171[0]);
			fRec168[0] = fRec170[0];
			float fRec169 = fRec172;
			fVec17[(IOTA & 1023)] = ((0.0410131775f * fTemp20) + (fRec166 + fRec169));
			output17[i] = FAUSTFLOAT((0.825772464f * (fVec17[((IOTA - iConst43) & 1023)] * fRec20[0])));
			float fTemp106 = (fConst37 * (((0.0401038378f * fTemp9) + (0.00367245008f * fTemp7)) + (0.0192545932f * fTemp8)));
			float fTemp107 = (fConst38 * fRec174[1]);
			fRec176[0] = (fTemp106 + (fRec176[1] + fTemp107));
			fRec174[0] = fRec176[0];
			float fRec175 = (fTemp107 + fTemp106);
			float fTemp108 = (fConst40 * ((((0.0358876549f * fTemp13) + (0.00661438983f * fTemp16)) + (0.00388430688f * fTemp15)) - ((0.0261082873f * fTemp14) + (0.0287773926f * fTemp12))));
			float fTemp109 = (fConst41 * fRec177[1]);
			float fTemp110 = (fConst42 * fRec180[1]);
			fRec182[0] = (fTemp108 + (fTemp109 + (fRec182[1] + fTemp110)));
			fRec180[0] = fRec182[0];
			float fRec181 = ((fTemp110 + fTemp109) + fTemp108);
			fRec179[0] = (fRec179[1] + fRec180[0]);
			fRec177[0] = fRec179[0];
			float fRec178 = fRec181;
			fVec18[(IOTA & 1023)] = ((0.0267503932f * fTemp20) + (fRec175 + fRec178));
			output18[i] = FAUSTFLOAT((0.825772464f * (fVec18[((IOTA - iConst43) & 1023)] * fRec20[0])));
			float fTemp111 = (fConst45 * ((0.0668016002f * fTemp9) - ((0.0337675065f * fTemp7) + (1.07285996e-05f * fTemp8))));
			float fTemp112 = (fConst46 * fRec183[1]);
			fRec185[0] = (fTemp111 + (fRec185[1] + fTemp112));
			fRec183[0] = fRec185[0];
			float fRec184 = (fTemp112 + fTemp111);
			float fTemp113 = (fConst48 * ((6.85509985e-06f * fTemp15) - ((0.0130083254f * fTemp14) + ((0.055088263f * fTemp16) + ((1.71022002e-05f * fTemp13) + (0.0606702417f * fTemp12))))));
			float fTemp114 = (fConst49 * fRec186[1]);
			float fTemp115 = (fConst50 * fRec189[1]);
			fRec191[0] = (fTemp113 + (fTemp114 + (fRec191[1] + fTemp115)));
			fRec189[0] = fRec191[0];
			float fRec190 = ((fTemp115 + fTemp114) + fTemp113);
			fRec188[0] = (fRec188[1] + fRec189[0]);
			fRec186[0] = fRec188[0];
			float fRec187 = fRec190;
			fVec19[0] = ((0.0466528162f * fTemp20) + (fRec184 + fRec187));
			output19[i] = FAUSTFLOAT((0.999793351f * (fVec19[iConst51] * fRec20[0])));
			float fTemp116 = (fConst53 * ((0.0406398699f * fTemp9) - ((0.0313559137f * fTemp7) + (0.0234596878f * fTemp8))));
			float fTemp117 = (fConst54 * fRec192[1]);
			fRec194[0] = (fTemp116 + (fRec194[1] + fTemp117));
			fRec192[0] = fRec194[0];
			float fRec193 = (fTemp117 + fTemp116);
			float fTemp118 = (fConst56 * ((0.0250726212f * fTemp15) - ((0.000599099381f * fTemp14) + ((0.0434271991f * fTemp16) + ((0.0359448791f * fTemp13) + (0.020761501f * fTemp12))))));
			float fTemp119 = (fConst57 * fRec195[1]);
			float fTemp120 = (fConst58 * fRec198[1]);
			fRec200[0] = (fTemp118 + (fTemp119 + (fRec200[1] + fTemp120)));
			fRec198[0] = fRec200[0];
			float fRec199 = ((fTemp120 + fTemp119) + fTemp118);
			fRec197[0] = (fRec197[1] + fRec198[0]);
			fRec195[0] = fRec197[0];
			float fRec196 = fRec199;
			output20[i] = FAUSTFLOAT((((0.0344887897f * fTemp20) + (fRec193 + fRec196)) * fRec20[0]));
			float fTemp121 = (fConst53 * ((0.0308367889f * fTemp9) - ((0.0325823613f * fTemp7) + (0.047908619f * fTemp8))));
			float fTemp122 = (fConst54 * fRec201[1]);
			fRec203[0] = (fTemp121 + (fRec203[1] + fTemp122));
			fRec201[0] = fRec203[0];
			float fRec202 = (fTemp122 + fTemp121);
			float fTemp123 = (fConst56 * (((0.0452136435f * fTemp15) + (0.0206034295f * fTemp12)) - (((0.0471394435f * fTemp13) + (0.0269582253f * fTemp16)) + (0.00680943299f * fTemp14))));
			float fTemp124 = (fConst57 * fRec204[1]);
			float fTemp125 = (fConst58 * fRec207[1]);
			fRec209[0] = (fTemp123 + (fTemp124 + (fRec209[1] + fTemp125)));
			fRec207[0] = fRec209[0];
			float fRec208 = ((fTemp125 + fTemp124) + fTemp123);
			fRec206[0] = (fRec206[1] + fRec207[0]);
			fRec204[0] = fRec206[0];
			float fRec205 = fRec208;
			output21[i] = FAUSTFLOAT((((0.0406066403f * fTemp20) + (fRec202 + fRec205)) * fRec20[0]));
			float fTemp126 = (fConst45 * (0.0f - (((9.95899995e-07f * fTemp9) + (0.0337603986f * fTemp7)) + (0.0667955354f * fTemp8))));
			float fTemp127 = (fConst46 * fRec210[1]);
			fRec212[0] = (fTemp126 + (fRec212[1] + fTemp127));
			fRec210[0] = fRec212[0];
			float fRec211 = (fTemp127 + fTemp126);
			float fTemp128 = (fConst48 * (((0.0550848991f * fTemp15) + ((2.55600014e-07f * fTemp16) + ((1.849e-06f * fTemp13) + (0.0606719367f * fTemp12)))) - (0.0130111389f * fTemp14)));
			float fTemp129 = (fConst49 * fRec213[1]);
			float fTemp130 = (fConst50 * fRec216[1]);
			fRec218[0] = (fTemp128 + (fTemp129 + (fRec218[1] + fTemp130)));
			fRec216[0] = fRec218[0];
			float fRec217 = ((fTemp130 + fTemp129) + fTemp128);
			fRec215[0] = (fRec215[1] + fRec216[0]);
			fRec213[0] = fRec215[0];
			float fRec214 = fRec217;
			fVec20[0] = ((0.0466445908f * fTemp20) + (fRec211 + fRec214));
			output22[i] = FAUSTFLOAT((0.999793351f * (fVec20[iConst51] * fRec20[0])));
			float fTemp131 = (fConst53 * (0.0f - (((0.0234640706f * fTemp9) + (0.0313628912f * fTemp7)) + (0.0406417958f * fTemp8))));
			float fTemp132 = (fConst54 * fRec219[1]);
			fRec221[0] = (fTemp131 + (fRec221[1] + fTemp132));
			fRec219[0] = fRec221[0];
			float fRec220 = (fTemp132 + fTemp131);
			float fTemp133 = (fConst56 * (((0.0434282646f * fTemp15) + ((0.0250729267f * fTemp16) + ((0.0359500684f * fTemp13) + (0.0207570828f * fTemp12)))) - (0.000587342074f * fTemp14)));
			float fTemp134 = (fConst57 * fRec222[1]);
			float fTemp135 = (fConst58 * fRec225[1]);
			fRec227[0] = (fTemp133 + (fTemp134 + (fRec227[1] + fTemp135)));
			fRec225[0] = fRec227[0];
			float fRec226 = ((fTemp135 + fTemp134) + fTemp133);
			fRec224[0] = (fRec224[1] + fRec225[0]);
			fRec222[0] = fRec224[0];
			float fRec223 = fRec226;
			output23[i] = FAUSTFLOAT((((0.034495037f * fTemp20) + (fRec220 + fRec223)) * fRec20[0]));
			float fTemp136 = (fConst53 * (0.0f - (((0.0478971489f * fTemp9) + (0.0325675346f * fTemp7)) + (0.0308167655f * fTemp8))));
			float fTemp137 = (fConst54 * fRec228[1]);
			fRec230[0] = (fTemp136 + (fRec230[1] + fTemp137));
			fRec228[0] = fRec230[0];
			float fRec229 = (fTemp137 + fTemp136);
			float fTemp138 = (fConst56 * ((((0.0471209586f * fTemp13) + (0.0452032685f * fTemp16)) + (0.0269464068f * fTemp15)) - ((0.00681215944f * fTemp14) + (0.0206162035f * fTemp12))));
			float fTemp139 = (fConst57 * fRec231[1]);
			float fTemp140 = (fConst58 * fRec234[1]);
			fRec236[0] = (fTemp138 + (fTemp139 + (fRec236[1] + fTemp140)));
			fRec234[0] = fRec236[0];
			float fRec235 = ((fTemp140 + fTemp139) + fTemp138);
			fRec233[0] = (fRec233[1] + fRec234[0]);
			fRec231[0] = fRec233[0];
			float fRec232 = fRec235;
			output24[i] = FAUSTFLOAT((((0.0405882597f * fTemp20) + (fRec229 + fRec232)) * fRec20[0]));
			float fTemp141 = (fConst45 * (0.0f - (((0.0667844266f * fTemp9) + (0.0337536409f * fTemp7)) + (3.20599997e-06f * fTemp8))));
			float fTemp142 = (fConst46 * fRec237[1]);
			fRec239[0] = (fTemp141 + (fRec239[1] + fTemp142));
			fRec237[0] = fRec239[0];
			float fRec238 = (fTemp142 + fTemp141);
			float fTemp143 = (fConst48 * ((((5.71520013e-06f * fTemp13) + (0.055085998f * fTemp16)) + (1.07580001e-06f * fTemp15)) - ((0.0130164139f * fTemp14) + (0.0606563874f * fTemp12))));
			float fTemp144 = (fConst49 * fRec240[1]);
			float fTemp145 = (fConst50 * fRec243[1]);
			fRec245[0] = (fTemp143 + (fTemp144 + (fRec245[1] + fTemp145)));
			fRec243[0] = fRec245[0];
			float fRec244 = ((fTemp145 + fTemp144) + fTemp143);
			fRec242[0] = (fRec242[1] + fRec243[0]);
			fRec240[0] = fRec242[0];
			float fRec241 = fRec244;
			fVec21[0] = ((0.0466346815f * fTemp20) + (fRec238 + fRec241));
			output25[i] = FAUSTFLOAT((0.999793351f * (fVec21[iConst51] * fRec20[0])));
			float fTemp146 = (fConst53 * ((0.0308232103f * fTemp8) - ((0.0478976816f * fTemp9) + (0.0325714648f * fTemp7))));
			float fTemp147 = (fConst54 * fRec246[1]);
			fRec248[0] = (fTemp146 + (fRec248[1] + fTemp147));
			fRec246[0] = fRec248[0];
			float fRec247 = (fTemp147 + fTemp146);
			float fTemp148 = (fConst56 * ((0.0452032611f * fTemp16) - ((0.0269425102f * fTemp15) + ((0.00680800574f * fTemp14) + ((0.0471277423f * fTemp13) + (0.0206062663f * fTemp12))))));
			float fTemp149 = (fConst57 * fRec249[1]);
			float fTemp150 = (fConst58 * fRec252[1]);
			fRec254[0] = (fTemp148 + (fTemp149 + (fRec254[1] + fTemp150)));
			fRec252[0] = fRec254[0];
			float fRec253 = ((fTemp150 + fTemp149) + fTemp148);
			fRec251[0] = (fRec251[1] + fRec252[0]);
			fRec249[0] = fRec251[0];
			float fRec250 = fRec253;
			output26[i] = FAUSTFLOAT((((0.0405937769f * fTemp20) + (fRec247 + fRec250)) * fRec20[0]));
			float fTemp151 = (fConst53 * ((0.040651273f * fTemp8) - ((0.0234596319f * fTemp9) + (0.0313677862f * fTemp7))));
			float fTemp152 = (fConst54 * fRec255[1]);
			fRec257[0] = (fTemp151 + (fRec257[1] + fTemp152));
			fRec255[0] = fRec257[0];
			float fRec256 = (fTemp152 + fTemp151);
			float fTemp153 = (fConst56 * (((0.0250699241f * fTemp16) + (0.0207735673f * fTemp12)) - (((0.0359471589f * fTemp13) + (0.000585046422f * fTemp14)) + (0.0434349068f * fTemp15))));
			float fTemp154 = (fConst57 * fRec258[1]);
			float fTemp155 = (fConst58 * fRec261[1]);
			fRec263[0] = (fTemp153 + (fTemp154 + (fRec263[1] + fTemp155)));
			fRec261[0] = fRec263[0];
			float fRec262 = ((fTemp155 + fTemp154) + fTemp153);
			fRec260[0] = (fRec260[1] + fRec261[0]);
			fRec258[0] = fRec260[0];
			float fRec259 = fRec262;
			output27[i] = FAUSTFLOAT((((0.0345001929f * fTemp20) + (fRec256 + fRec259)) * fRec20[0]));
			float fTemp156 = (fConst45 * (((7.68480004e-06f * fTemp9) + (0.0667851865f * fTemp8)) - (0.0337466374f * fTemp7)));
			float fTemp157 = (fConst46 * fRec264[1]);
			fRec266[0] = (fTemp156 + (fRec266[1] + fTemp157));
			fRec264[0] = fRec266[0];
			float fRec265 = (fTemp157 + fTemp156);
			float fTemp158 = (fConst48 * (((1.38392998e-05f * fTemp13) + (0.0606603697f * fTemp12)) - (((3.01280011e-06f * fTemp16) + (0.0130227739f * fTemp14)) + (0.0550744645f * fTemp15))));
			float fTemp159 = (fConst49 * fRec267[1]);
			float fTemp160 = (fConst50 * fRec270[1]);
			fRec272[0] = (fTemp158 + (fTemp159 + (fRec272[1] + fTemp160)));
			fRec270[0] = fRec272[0];
			float fRec271 = ((fTemp160 + fTemp159) + fTemp158);
			fRec269[0] = (fRec269[1] + fRec270[0]);
			fRec267[0] = fRec269[0];
			float fRec268 = fRec271;
			fVec22[0] = ((0.0466337539f * fTemp20) + (fRec265 + fRec268));
			output28[i] = FAUSTFLOAT((0.999793351f * (fVec22[iConst51] * fRec20[0])));
			float fTemp161 = (fConst53 * (((0.0234466158f * fTemp9) + (0.0406251438f * fTemp8)) - (0.031370163f * fTemp7)));
			float fTemp162 = (fConst54 * fRec273[1]);
			fRec275[0] = (fTemp161 + (fRec275[1] + fTemp162));
			fRec273[0] = fRec275[0];
			float fRec274 = (fTemp162 + fTemp161);
			float fTemp163 = (fConst56 * (((0.0359259993f * fTemp13) + (0.0207571592f * fTemp12)) - (((0.0250631403f * fTemp16) + (0.000557037478f * fTemp14)) + (0.0434222557f * fTemp15))));
			float fTemp164 = (fConst57 * fRec276[1]);
			float fTemp165 = (fConst58 * fRec279[1]);
			fRec281[0] = (fTemp163 + (fTemp164 + (fRec281[1] + fTemp165)));
			fRec279[0] = fRec281[0];
			float fRec280 = ((fTemp165 + fTemp164) + fTemp163);
			fRec278[0] = (fRec278[1] + fRec279[0]);
			fRec276[0] = fRec278[0];
			float fRec277 = fRec280;
			output29[i] = FAUSTFLOAT((((0.0344871767f * fTemp20) + (fRec274 + fRec277)) * fRec20[0]));
			float fTemp166 = (fConst53 * (((0.0478948392f * fTemp9) + (0.0308185238f * fTemp8)) - (0.0325662233f * fTemp7)));
			float fTemp167 = (fConst54 * fRec282[1]);
			fRec284[0] = (fTemp166 + (fRec284[1] + fTemp167));
			fRec282[0] = fRec284[0];
			float fRec283 = (fTemp167 + fTemp166);
			float fTemp168 = (fConst56 * ((0.0471193865f * fTemp13) - ((0.0269478485f * fTemp15) + ((0.006815203f * fTemp14) + ((0.0452092178f * fTemp16) + (0.0206080098f * fTemp12))))));
			float fTemp169 = (fConst57 * fRec285[1]);
			float fTemp170 = (fConst58 * fRec288[1]);
			fRec290[0] = (fTemp168 + (fTemp169 + (fRec290[1] + fTemp170)));
			fRec288[0] = fRec290[0];
			float fRec289 = ((fTemp170 + fTemp169) + fTemp168);
			fRec287[0] = (fRec287[1] + fRec288[0]);
			fRec285[0] = fRec287[0];
			float fRec286 = fRec289;
			output30[i] = FAUSTFLOAT((((0.0405861326f * fTemp20) + (fRec283 + fRec286)) * fRec20[0]));
			fRec3[1] = fRec3[0];
			fRec4[2] = fRec4[1];
			fRec4[1] = fRec4[0];
			fRec5[1] = fRec5[0];
			fRec6[2] = fRec6[1];
			fRec6[1] = fRec6[0];
			fRec7[2] = fRec7[1];
			fRec7[1] = fRec7[0];
			fRec2[1] = fRec2[0];
			fRec0[1] = fRec0[0];
			fRec14[2] = fRec14[1];
			fRec14[1] = fRec14[0];
			fRec15[2] = fRec15[1];
			fRec15[1] = fRec15[0];
			fRec16[2] = fRec16[1];
			fRec16[1] = fRec16[0];
			fRec17[2] = fRec17[1];
			fRec17[1] = fRec17[0];
			fRec18[2] = fRec18[1];
			fRec18[1] = fRec18[0];
			fRec13[1] = fRec13[0];
			fRec11[1] = fRec11[0];
			fRec10[1] = fRec10[0];
			fRec8[1] = fRec8[0];
			fRec19[2] = fRec19[1];
			fRec19[1] = fRec19[0];
			IOTA = (IOTA + 1);
			fRec20[1] = fRec20[0];
			fRec23[1] = fRec23[0];
			fRec21[1] = fRec21[0];
			fRec29[1] = fRec29[0];
			fRec27[1] = fRec27[0];
			fRec26[1] = fRec26[0];
			fRec24[1] = fRec24[0];
			fRec32[1] = fRec32[0];
			fRec30[1] = fRec30[0];
			fRec38[1] = fRec38[0];
			fRec36[1] = fRec36[0];
			fRec35[1] = fRec35[0];
			fRec33[1] = fRec33[0];
			fRec41[1] = fRec41[0];
			fRec39[1] = fRec39[0];
			fRec47[1] = fRec47[0];
			fRec45[1] = fRec45[0];
			fRec44[1] = fRec44[0];
			fRec42[1] = fRec42[0];
			fRec50[1] = fRec50[0];
			fRec48[1] = fRec48[0];
			fRec56[1] = fRec56[0];
			fRec54[1] = fRec54[0];
			fRec53[1] = fRec53[0];
			fRec51[1] = fRec51[0];
			fRec59[1] = fRec59[0];
			fRec57[1] = fRec57[0];
			fRec65[1] = fRec65[0];
			fRec63[1] = fRec63[0];
			fRec62[1] = fRec62[0];
			fRec60[1] = fRec60[0];
			fRec68[1] = fRec68[0];
			fRec66[1] = fRec66[0];
			fRec74[1] = fRec74[0];
			fRec72[1] = fRec72[0];
			fRec71[1] = fRec71[0];
			fRec69[1] = fRec69[0];
			fRec77[1] = fRec77[0];
			fRec75[1] = fRec75[0];
			fRec83[1] = fRec83[0];
			fRec81[1] = fRec81[0];
			fRec80[1] = fRec80[0];
			fRec78[1] = fRec78[0];
			fRec86[1] = fRec86[0];
			fRec84[1] = fRec84[0];
			fRec92[1] = fRec92[0];
			fRec90[1] = fRec90[0];
			fRec89[1] = fRec89[0];
			fRec87[1] = fRec87[0];
			fRec95[1] = fRec95[0];
			fRec93[1] = fRec93[0];
			fRec101[1] = fRec101[0];
			fRec99[1] = fRec99[0];
			fRec98[1] = fRec98[0];
			fRec96[1] = fRec96[0];
			fRec104[1] = fRec104[0];
			fRec102[1] = fRec102[0];
			fRec110[1] = fRec110[0];
			fRec108[1] = fRec108[0];
			fRec107[1] = fRec107[0];
			fRec105[1] = fRec105[0];
			fRec113[1] = fRec113[0];
			fRec111[1] = fRec111[0];
			fRec119[1] = fRec119[0];
			fRec117[1] = fRec117[0];
			fRec116[1] = fRec116[0];
			fRec114[1] = fRec114[0];
			fRec122[1] = fRec122[0];
			fRec120[1] = fRec120[0];
			fRec128[1] = fRec128[0];
			fRec126[1] = fRec126[0];
			fRec125[1] = fRec125[0];
			fRec123[1] = fRec123[0];
			fRec131[1] = fRec131[0];
			fRec129[1] = fRec129[0];
			fRec137[1] = fRec137[0];
			fRec135[1] = fRec135[0];
			fRec134[1] = fRec134[0];
			fRec132[1] = fRec132[0];
			fRec140[1] = fRec140[0];
			fRec138[1] = fRec138[0];
			fRec146[1] = fRec146[0];
			fRec144[1] = fRec144[0];
			fRec143[1] = fRec143[0];
			fRec141[1] = fRec141[0];
			fRec149[1] = fRec149[0];
			fRec147[1] = fRec147[0];
			fRec155[1] = fRec155[0];
			fRec153[1] = fRec153[0];
			fRec152[1] = fRec152[0];
			fRec150[1] = fRec150[0];
			fRec158[1] = fRec158[0];
			fRec156[1] = fRec156[0];
			fRec164[1] = fRec164[0];
			fRec162[1] = fRec162[0];
			fRec161[1] = fRec161[0];
			fRec159[1] = fRec159[0];
			fRec167[1] = fRec167[0];
			fRec165[1] = fRec165[0];
			fRec173[1] = fRec173[0];
			fRec171[1] = fRec171[0];
			fRec170[1] = fRec170[0];
			fRec168[1] = fRec168[0];
			fRec176[1] = fRec176[0];
			fRec174[1] = fRec174[0];
			fRec182[1] = fRec182[0];
			fRec180[1] = fRec180[0];
			fRec179[1] = fRec179[0];
			fRec177[1] = fRec177[0];
			fRec185[1] = fRec185[0];
			fRec183[1] = fRec183[0];
			fRec191[1] = fRec191[0];
			fRec189[1] = fRec189[0];
			fRec188[1] = fRec188[0];
			fRec186[1] = fRec186[0];
			fVec19[2] = fVec19[1];
			fVec19[1] = fVec19[0];
			fRec194[1] = fRec194[0];
			fRec192[1] = fRec192[0];
			fRec200[1] = fRec200[0];
			fRec198[1] = fRec198[0];
			fRec197[1] = fRec197[0];
			fRec195[1] = fRec195[0];
			fRec203[1] = fRec203[0];
			fRec201[1] = fRec201[0];
			fRec209[1] = fRec209[0];
			fRec207[1] = fRec207[0];
			fRec206[1] = fRec206[0];
			fRec204[1] = fRec204[0];
			fRec212[1] = fRec212[0];
			fRec210[1] = fRec210[0];
			fRec218[1] = fRec218[0];
			fRec216[1] = fRec216[0];
			fRec215[1] = fRec215[0];
			fRec213[1] = fRec213[0];
			fVec20[2] = fVec20[1];
			fVec20[1] = fVec20[0];
			fRec221[1] = fRec221[0];
			fRec219[1] = fRec219[0];
			fRec227[1] = fRec227[0];
			fRec225[1] = fRec225[0];
			fRec224[1] = fRec224[0];
			fRec222[1] = fRec222[0];
			fRec230[1] = fRec230[0];
			fRec228[1] = fRec228[0];
			fRec236[1] = fRec236[0];
			fRec234[1] = fRec234[0];
			fRec233[1] = fRec233[0];
			fRec231[1] = fRec231[0];
			fRec239[1] = fRec239[0];
			fRec237[1] = fRec237[0];
			fRec245[1] = fRec245[0];
			fRec243[1] = fRec243[0];
			fRec242[1] = fRec242[0];
			fRec240[1] = fRec240[0];
			fVec21[2] = fVec21[1];
			fVec21[1] = fVec21[0];
			fRec248[1] = fRec248[0];
			fRec246[1] = fRec246[0];
			fRec254[1] = fRec254[0];
			fRec252[1] = fRec252[0];
			fRec251[1] = fRec251[0];
			fRec249[1] = fRec249[0];
			fRec257[1] = fRec257[0];
			fRec255[1] = fRec255[0];
			fRec263[1] = fRec263[0];
			fRec261[1] = fRec261[0];
			fRec260[1] = fRec260[0];
			fRec258[1] = fRec258[0];
			fRec266[1] = fRec266[0];
			fRec264[1] = fRec264[0];
			fRec272[1] = fRec272[0];
			fRec270[1] = fRec270[0];
			fRec269[1] = fRec269[0];
			fRec267[1] = fRec267[0];
			fVec22[2] = fVec22[1];
			fVec22[1] = fVec22[0];
			fRec275[1] = fRec275[0];
			fRec273[1] = fRec273[0];
			fRec281[1] = fRec281[0];
			fRec279[1] = fRec279[0];
			fRec278[1] = fRec278[0];
			fRec276[1] = fRec276[0];
			fRec284[1] = fRec284[0];
			fRec282[1] = fRec282[0];
			fRec290[1] = fRec290[0];
			fRec288[1] = fRec288[0];
			fRec287[1] = fRec287[0];
			fRec285[1] = fRec285[0];
			
		}
		
	}

	
};

//----------------------------------------------------------------------------
// SuperCollider/Faust interface
//----------------------------------------------------------------------------

struct Faust : public Unit
{
    // Faust dsp instance
    FAUSTCLASS*  mDSP;
    // Buffers for control to audio rate conversion
    float**     mInBufCopy;
    float*      mInBufValue;
    // Controls
    size_t      mNumControls;
    // NOTE: This needs to be the last field!
    //
    // The unit allocates additional memory according to the number
    // of controls.
    Control     mControls[0];

    int getNumAudioInputs() { return mDSP->getNumInputs(); }
};

// Global state

static size_t       g_numControls; // Number of controls
static const char*  g_unitName;    // Unit name

// Initialize the global state with unit name and sample rate.
void initState(const std::string& name, int sampleRate);

// Return the unit size in bytes, including static fields and controls.
static size_t unitSize();

// Convert a file name to a valid unit name.
static std::string fileNameToUnitName(const std::string& fileName);

// Convert the XML unit name to a valid class name.
static std::string normalizeClassName(const std::string& name);

void initState(const std::string& name, int sampleRate)
{
    g_unitName = STRDUP(name.c_str());

    mydsp* dsp = new FAUSTCLASS;
    ControlCounter* cc = new ControlCounter;

    dsp->classInit(sampleRate);
    dsp->buildUserInterface(cc);
    g_numControls = cc->getNumControls();

    delete dsp;
    delete cc;
}

size_t unitSize()
{
    return sizeof(Faust) + g_numControls * sizeof(Control);
}

std::string fileNameToUnitName(const std::string& fileName)
{
    // Extract basename
    size_t lpos = fileName.rfind('/', fileName.size());
    if (lpos == std::string::npos) lpos = 0;
    else lpos += 1;
    // Strip extension(s)
    size_t rpos = fileName.find('.', lpos);
    // Return substring
    return fileName.substr(lpos, rpos > lpos ? rpos - lpos : 0);
}

// Globals

static InterfaceTable* ft;

// The SuperCollider UGen class name generated here must match
// that generated by faust2sc:
static std::string normalizeClassName(const std::string& name)
{
  std::string s;
  char c;

  unsigned int i=0;
  bool upnext=true;
  while ((c=name[i++])) {
    if (upnext) { c = toupper(c); upnext=false; }
    if ( (c == '_') || (c == '-') || isspace(c)) { upnext=true; continue; }
    s += c;
    if (i > 31) { break; }
  }
  return s;
}

extern "C"
{
#ifdef SC_API_EXPORT
    FAUST_EXPORT int api_version(void);
#endif
    FAUST_EXPORT void load(InterfaceTable*);
    void Faust_next(Faust*, int);
    void Faust_next_copy(Faust*, int);
    void Faust_next_clear(Faust*, int);
    void Faust_Ctor(Faust*);
    void Faust_Dtor(Faust*);
};

inline static void fillBuffer(float* dst, int n, float v)
{
    Fill(n, dst, v);
}

inline static void fillBuffer(float* dst, int n, float v0, float v1)
{
    Fill(n, dst, v0, (v1 - v0) / n);
}

inline static void copyBuffer(float* dst, int n, float* src)
{
    Copy(n, dst, src);
}

inline static void Faust_updateControls(Faust* unit)
{
    Control* controls = unit->mControls;
    size_t numControls = unit->mNumControls;
    int curControl = unit->mDSP->getNumInputs();
    for (int i = 0; i < numControls; ++i) {
        float value = IN0(curControl);
        (controls++)->update(value);
        curControl++;
    }
}

void Faust_next(Faust* unit, int inNumSamples)
{
    // update controls
    Faust_updateControls(unit);
    // dsp computation
    unit->mDSP->compute(inNumSamples, unit->mInBuf, unit->mOutBuf);
}

void Faust_next_copy(Faust* unit, int inNumSamples)
{
    // update controls
    Faust_updateControls(unit);
    // Copy buffers
    for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
        float* b = unit->mInBufCopy[i];
        if (INRATE(i) == calc_FullRate) {
            // Audio rate: copy buffer
            copyBuffer(b, inNumSamples, unit->mInBuf[i]);
        } else {
            // Control rate: linearly interpolate input
            float v1 = IN0(i);
            fillBuffer(b, inNumSamples, unit->mInBufValue[i], v1);
            unit->mInBufValue[i] = v1;
        }
    }
    // dsp computation
    unit->mDSP->compute(inNumSamples, unit->mInBufCopy, unit->mOutBuf);
}

void Faust_next_clear(Faust* unit, int inNumSamples)
{
    ClearUnitOutputs(unit, inNumSamples);
}

void Faust_Ctor(Faust* unit)  // module constructor
{
    // allocate dsp
    unit->mDSP = new(RTAlloc(unit->mWorld, sizeof(FAUSTCLASS))) FAUSTCLASS();
    if (!unit->mDSP) {
        Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
        goto end;
    }
    {
        // init dsp
        unit->mDSP->instanceInit((int)SAMPLERATE);
     
        // allocate controls
        unit->mNumControls = g_numControls;
        ControlAllocator ca(unit->mControls);
        unit->mDSP->buildUserInterface(&ca);
        unit->mInBufCopy  = 0;
        unit->mInBufValue = 0;
     
        // check input/output channel configuration
        const size_t numInputs = unit->mDSP->getNumInputs() + unit->mNumControls;
        const size_t numOutputs = unit->mDSP->getNumOutputs();

        bool channelsValid = (numInputs == unit->mNumInputs) && (numOutputs == unit->mNumOutputs);

        if (channelsValid) {
            bool rateValid = true;
            for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
                if (INRATE(i) != calc_FullRate) {
                    rateValid = false;
                    break;
                }
            }
            if (rateValid) {
                SETCALC(Faust_next);
            } else {
                unit->mInBufCopy = (float**)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*sizeof(float*));
                if (!unit->mInBufCopy) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                // Allocate memory for input buffer copies (numInputs * bufLength)
                // and linear interpolation state (numInputs)
                // = numInputs * (bufLength + 1)
                unit->mInBufValue = (float*)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*sizeof(float));
                if (!unit->mInBufValue) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                // Aquire memory for interpolator state.
                float* mem = (float*)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*BUFLENGTH*sizeof(float));
                if (mem) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
                    // Initialize interpolator.
                    unit->mInBufValue[i] = IN0(i);
                    // Aquire buffer memory.
                    unit->mInBufCopy[i] = mem;
                    mem += BUFLENGTH;
                }
                SETCALC(Faust_next_copy);
            }
    #if !defined(NDEBUG)
            Print("Faust[%s]:\n", g_unitName);
            Print("    Inputs:   %d\n"
                  "    Outputs:  %d\n"
                  "    Callback: %s\n",
                  numInputs, numOutputs,
                  unit->mCalcFunc == (UnitCalcFunc)Faust_next ? "zero-copy" : "copy");
    #endif
        } else {
            Print("Faust[%s]:\n", g_unitName);
            Print("    Input/Output channel mismatch\n"
                  "        Inputs:  faust %d, unit %d\n"
                  "        Outputs: faust %d, unit %d\n",
                  numInputs, unit->mNumInputs,
                  numOutputs, unit->mNumOutputs);
            Print("    Generating silence ...\n");
            SETCALC(Faust_next_clear);
        }
    }
    
end:
    // Fix for https://github.com/grame-cncm/faust/issues/13
    ClearUnitOutputs(unit, 1);
}

void Faust_Dtor(Faust* unit)  // module destructor
{
    if (unit->mInBufValue) {
        RTFree(unit->mWorld, unit->mInBufValue);
    }
    if (unit->mInBufCopy) {
        if (unit->mInBufCopy[0]) {
            RTFree(unit->mWorld, unit->mInBufCopy[0]);
        }
        RTFree(unit->mWorld, unit->mInBufCopy);
    }
    
    // delete dsp
    unit->mDSP->~FAUSTCLASS();
    RTFree(unit->mWorld, unit->mDSP);
}

#ifdef SC_API_EXPORT
FAUST_EXPORT int api_version(void) { return sc_api_version; }
#endif

FAUST_EXPORT void load(InterfaceTable* inTable)
{
    ft = inTable;

    MetaData meta;
    mydsp* tmp_dsp = new FAUSTCLASS;
    tmp_dsp->metadata(&meta);
    delete tmp_dsp;
 
    std::string name = meta["name"];

    if (name.empty()) {
        name = fileNameToUnitName(__FILE__);
    }
  
    name = normalizeClassName(name);

#if !defined(NDEBUG) & defined(SC_API_EXPORT)
    Print("Faust: supercollider.cpp: sc_api_version = %d\n", sc_api_version);
#endif

    if (name.empty()) {
        // Catch empty name
        Print("Faust [supercollider.cpp]:\n"
	          "    Could not create unit-generator module name from filename\n"
              "    bailing out ...\n");
        return;
    }

    if (strncmp(name.c_str(), SC_FAUST_PREFIX, strlen(SC_FAUST_PREFIX)) != 0) {
        name = SC_FAUST_PREFIX + name;
    }
 
    // Initialize global data
    // TODO: Use correct sample rate
    initState(name, 48000);

    // Register ugen
    (*ft->fDefineUnit)(
        (char*)name.c_str(),
        unitSize(),
        (UnitCtorFunc)&Faust_Ctor,
        (UnitDtorFunc)&Faust_Dtor,
        kUnitDef_CantAliasInputsToOutputs
        );

#if !defined(NDEBUG)
    Print("Faust: %s numControls=%d\n", name.c_str(), g_numControls);
#endif // NDEBUG
}

#ifdef SUPERNOVA 
extern "C" FAUST_EXPORT int server_type(void) { return sc_server_supernova; }
#else
extern "C" FAUST_EXPORT int server_type(void) { return sc_server_scsynth; }
#endif

// EOF

#endif
