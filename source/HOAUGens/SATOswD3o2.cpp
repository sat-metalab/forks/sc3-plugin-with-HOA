/* ------------------------------------------------------------
author: "AmbisonicDecoderToolkit"
copyright: "(c) Aaron J. Heller 2013"
license: "BSD 3-Clause License"
name: "SATOswD3o2"
version: "1.2"
Code generated with Faust 2.5.12 (https://faust.grame.fr)
Compilation options: cpp, -scal -ftz 0
------------------------------------------------------------ */

#ifndef  __mydsp_H__
#define  __mydsp_H__

//-------------------------------------------------------------------
// FAUST architecture file for SuperCollider.
// Copyright (C) 2005-2012 Stefan Kersten.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//-------------------------------------------------------------------

// If other than 'faust2sc --prefix Faust' is used, sed this as well:
#if !defined(SC_FAUST_PREFIX)
#define SC_FAUST_PREFIX "Faust"
#endif

#include <map>
#include <string>
#include <string.h>
#include <SC_PlugIn.h>

/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __dsp__
#define __dsp__

#include <string>

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

class UI;
struct Meta;

/**
 * DSP memory manager.
 */

struct dsp_memory_manager {
    
    virtual ~dsp_memory_manager() {}
    
    virtual void* allocate(size_t size) = 0;
    virtual void destroy(void* ptr) = 0;
    
};

/**
* Signal processor definition.
*/

class dsp {

    public:

        dsp() {}
        virtual ~dsp() {}

        /* Return instance number of audio inputs */
        virtual int getNumInputs() = 0;
    
        /* Return instance number of audio outputs */
        virtual int getNumOutputs() = 0;
    
        /**
         * Trigger the ui_interface parameter with instance specific calls
         * to 'addBtton', 'addVerticalSlider'... in order to build the UI.
         *
         * @param ui_interface - the user interface builder
         */
        virtual void buildUserInterface(UI* ui_interface) = 0;
    
        /* Returns the sample rate currently used by the instance */
        virtual int getSampleRate() = 0;
    
        /**
         * Global init, calls the following methods:
         * - static class 'classInit': static tables initialization
         * - 'instanceInit': constants and instance state initialization
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void init(int samplingRate) = 0;

        /**
         * Init instance state
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void instanceInit(int samplingRate) = 0;

        /**
         * Init instance constant state
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void instanceConstants(int samplingRate) = 0;
    
        /* Init default control parameters values */
        virtual void instanceResetUserInterface() = 0;
    
        /* Init instance state (delay lines...) */
        virtual void instanceClear() = 0;
 
        /**
         * Return a clone of the instance.
         *
         * @return a copy of the instance on success, otherwise a null pointer.
         */
        virtual dsp* clone() = 0;
    
        /**
         * Trigger the Meta* parameter with instance specific calls to 'declare' (key, value) metadata.
         *
         * @param m - the Meta* meta user
         */
        virtual void metadata(Meta* m) = 0;
    
        /**
         * DSP instance computation, to be called with successive in/out audio buffers.
         *
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         *
         */
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) = 0;
    
        /**
         * DSP instance computation: alternative method to be used by subclasses.
         *
         * @param date_usec - the timestamp in microsec given by audio driver.
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         *
         */
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { compute(count, inputs, outputs); }
       
};

/**
 * Generic DSP decorator.
 */

class decorator_dsp : public dsp {

    protected:

        dsp* fDSP;

    public:

        decorator_dsp(dsp* dsp = 0):fDSP(dsp) {}
        virtual ~decorator_dsp() { delete fDSP; }

        virtual int getNumInputs() { return fDSP->getNumInputs(); }
        virtual int getNumOutputs() { return fDSP->getNumOutputs(); }
        virtual void buildUserInterface(UI* ui_interface) { fDSP->buildUserInterface(ui_interface); }
        virtual int getSampleRate() { return fDSP->getSampleRate(); }
        virtual void init(int samplingRate) { fDSP->init(samplingRate); }
        virtual void instanceInit(int samplingRate) { fDSP->instanceInit(samplingRate); }
        virtual void instanceConstants(int samplingRate) { fDSP->instanceConstants(samplingRate); }
        virtual void instanceResetUserInterface() { fDSP->instanceResetUserInterface(); }
        virtual void instanceClear() { fDSP->instanceClear(); }
        virtual decorator_dsp* clone() { return new decorator_dsp(fDSP->clone()); }
        virtual void metadata(Meta* m) { fDSP->metadata(m); }
        // Beware: subclasses usually have to overload the two 'compute' methods
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(count, inputs, outputs); }
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(date_usec, count, inputs, outputs); }
    
};

/**
 * DSP factory class.
 */

class dsp_factory {
    
    protected:
    
        // So that to force sub-classes to use deleteDSPFactory(dsp_factory* factory);
        virtual ~dsp_factory() {}
    
    public:
    
        virtual std::string getName() = 0;
        virtual std::string getSHAKey() = 0;
        virtual std::string getDSPCode() = 0;
    
        virtual dsp* createDSPInstance() = 0;
    
        virtual void setMemoryManager(dsp_memory_manager* manager) = 0;
        virtual dsp_memory_manager* getMemoryManager() = 0;
    
};

/**
 * On Intel set FZ (Flush to Zero) and DAZ (Denormals Are Zero)
 * flags to avoid costly denormals.
 */

#ifdef __SSE__
    #include <xmmintrin.h>
    #ifdef __SSE2__
        #define AVOIDDENORMALS _mm_setcsr(_mm_getcsr() | 0x8040)
    #else
        #define AVOIDDENORMALS _mm_setcsr(_mm_getcsr() | 0x8000)
    #endif
#else
    #define AVOIDDENORMALS
#endif

#endif
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __UI_H__
#define __UI_H__

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

/*******************************************************************************
 * UI : Faust DSP User Interface
 * User Interface as expected by the buildUserInterface() method of a DSP.
 * This abstract class contains only the method that the Faust compiler can
 * generate to describe a DSP user interface.
 ******************************************************************************/

struct Soundfile;

class UI
{

    public:

        UI() {}

        virtual ~UI() {}

        // -- widget's layouts

        virtual void openTabBox(const char* label) = 0;
        virtual void openHorizontalBox(const char* label) = 0;
        virtual void openVerticalBox(const char* label) = 0;
        virtual void closeBox() = 0;

        // -- active widgets

        virtual void addButton(const char* label, FAUSTFLOAT* zone) = 0;
        virtual void addCheckButton(const char* label, FAUSTFLOAT* zone) = 0;
        virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;
        virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;
        virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;

        // -- passive widgets

        virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) = 0;
        virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) = 0;
    
        // -- soundfiles
    
        virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) = 0;

        // -- metadata declarations

        virtual void declare(FAUSTFLOAT*, const char*, const char*) {}
};

#endif
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/
 
#ifndef __misc__
#define __misc__

#include <algorithm>
#include <map>
#include <string.h>
#include <stdlib.h>

/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __meta__
#define __meta__

struct Meta
{
    virtual void declare(const char* key, const char* value) = 0;
    virtual ~Meta() {};
};

#endif

using std::max;
using std::min;

struct XXXX_Meta : std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key]=value; }
};

struct MY_Meta : Meta, std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key]=value; }
};

inline int lsr(int x, int n)	{ return int(((unsigned int)x) >> n); }

inline int int2pow2(int x)		{ int r = 0; while ((1<<r) < x) r++; return r; }

inline long lopt(char* argv[], const char* name, long def)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return atoi(argv[i+1]);
	return def;
}

inline bool isopt(char* argv[], const char* name)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return true;
	return false;
}

inline const char* lopts(char* argv[], const char* name, const char* def)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return argv[i+1];
	return def;
}

#endif


using namespace std;

#if defined(__GNUC__) && __GNUC__ >= 4
    #define FAUST_EXPORT __attribute__((visibility("default")))
#else
    #define FAUST_EXPORT  SC_API_EXPORT
#endif

#ifdef WIN32
    #define STRDUP _strdup
#else
    #define STRDUP strdup
#endif

//----------------------------------------------------------------------------
// Vector intrinsics
//----------------------------------------------------------------------------


//----------------------------------------------------------------------------
// Metadata
//----------------------------------------------------------------------------

class MetaData : public Meta
               , public std::map<std::string, std::string>
{
public:
    void declare(const char* key, const char* value)
    {
        (*this)[key] = value;
    }
};

//----------------------------------------------------------------------------
// Control counter
//----------------------------------------------------------------------------

class ControlCounter : public UI
{
public:
    ControlCounter()
        : mNumControlInputs(0),
          mNumControlOutputs(0)
    {}

    size_t getNumControls() const { return getNumControlInputs(); }
    size_t getNumControlInputs() const { return mNumControlInputs; }
    size_t getNumControlOutputs() const { return mNumControlOutputs; }

    // Layout widgets
    virtual void openTabBox(const char* label) { }
    virtual void openHorizontalBox(const char* label) { }
    virtual void openVerticalBox(const char* label) { }
    virtual void closeBox() { }

    // Active widgets
    virtual void addButton(const char* label, FAUSTFLOAT* zone)
    { addControlInput(); }
    virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
    { addControlInput(); }
    virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }
    virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }
    virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }

    // Passive widgets
    virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
    { addControlOutput(); }
    virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
    { addControlOutput(); }
    
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}

protected:
    void addControlInput() { mNumControlInputs++; }
    void addControlOutput() { mNumControlOutputs++; }

private:
    size_t mNumControlInputs;
    size_t mNumControlOutputs;
};

//----------------------------------------------------------------------------
// UI control
//----------------------------------------------------------------------------

struct Control
{
    typedef void (*UpdateFunction)(Control* self, FAUSTFLOAT value);

    UpdateFunction updateFunction;
    FAUSTFLOAT* zone;
    FAUSTFLOAT min, max;

    inline void update(FAUSTFLOAT value)
    {
        (*updateFunction)(this, value);
    }

    static void simpleUpdate(Control* self, FAUSTFLOAT value)
    {
        *self->zone = value;
    }
    static void boundedUpdate(Control* self, FAUSTFLOAT value)
    {
        *self->zone = sc_clip(value, self->min, self->max);
    }
};

//----------------------------------------------------------------------------
// Control allocator
//----------------------------------------------------------------------------

class ControlAllocator : public UI
{
public:
    ControlAllocator(Control* controls)
        : mControls(controls)
    { }

    // Layout widgets
    virtual void openTabBox(const char* label) { }
    virtual void openHorizontalBox(const char* label) { }
    virtual void openVerticalBox(const char* label) { }
    virtual void closeBox() { }

    // Active widgets
    virtual void addButton(const char* label, FAUSTFLOAT* zone)
    { addSimpleControl(zone); }
    virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
    { addSimpleControl(zone); }
    virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }
    virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }
    virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }

    // Passive widgets
    virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
    virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}

private:
    void addControl(Control::UpdateFunction updateFunction, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT /* step */)
    {
        Control* ctrl        = mControls++;
        ctrl->updateFunction = updateFunction;
        ctrl->zone           = zone;
        ctrl->min            = min;
        ctrl->max            = max;
    }
    void addSimpleControl(FAUSTFLOAT* zone)
    {
        addControl(Control::simpleUpdate, zone, 0.f, 0.f, 0.f);
    }
    void addBoundedControl(FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    {
        addControl(Control::boundedUpdate, zone, min, max, step);
    }

private:
    Control* mControls;
};

//----------------------------------------------------------------------------
// FAUST generated code
//----------------------------------------------------------------------------

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif 

#include <cmath>
#include <math.h>

float mydsp_faustpower2_f(float value) {
	return (value * value);
	
}

#ifndef FAUSTCLASS 
#define FAUSTCLASS mydsp
#endif
#ifdef __APPLE__ 
#define exp10f __exp10f
#define exp10 __exp10
#endif

class mydsp : public dsp {
	
 private:
	
	int fSamplingFreq;
	int iConst0;
	float fConst1;
	FAUSTFLOAT fHslider0;
	float fRec0[2];
	float fConst2;
	float fConst3;
	float fConst4;
	float fConst5;
	float fRec4[2];
	float fRec2[2];
	float fRec1[3];
	FAUSTFLOAT fHslider1;
	float fRec5[2];
	float fRec6[3];
	float fRec10[2];
	float fRec8[2];
	float fRec7[3];
	float fConst6;
	float fConst7;
	float fConst8;
	float fConst9;
	float fRec17[2];
	float fRec15[2];
	float fRec14[2];
	float fRec12[2];
	float fRec11[3];
	float fRec24[2];
	float fRec22[2];
	float fRec21[2];
	float fRec19[2];
	float fRec18[3];
	float fRec31[2];
	float fRec29[2];
	float fRec28[2];
	float fRec26[2];
	float fRec25[3];
	float fRec38[2];
	float fRec36[2];
	float fRec35[2];
	float fRec33[2];
	float fRec32[3];
	float fRec42[2];
	float fRec40[2];
	float fRec39[3];
	float fRec49[2];
	float fRec47[2];
	float fRec46[2];
	float fRec44[2];
	float fRec43[3];
	FAUSTFLOAT fCheckbox0;
	FAUSTFLOAT fHslider2;
	float fRec50[2];
	float fVec0[12];
	int iConst10;
	float fVec1[13];
	int iConst11;
	float fVec2[12];
	float fVec3[12];
	float fVec4[13];
	float fVec5[12];
	int IOTA;
	float fVec6[32];
	int iConst12;
	float fVec7[32];
	int iConst13;
	float fVec8[32];
	float fVec9[32];
	float fVec10[32];
	float fVec11[32];
	float fVec12[32];
	float fVec13[32];
	float fVec14[32];
	float fVec15[32];
	float fVec16[32];
	float fVec17[32];
	float fVec18[64];
	int iConst14;
	float fVec19[64];
	int iConst15;
	float fVec20[64];
	float fVec21[64];
	float fVec22[64];
	float fVec23[64];
	float fVec24[64];
	float fVec25[64];
	float fVec26[64];
	float fVec27[64];
	float fVec28[64];
	float fVec29[64];
	
 public:
	
	void metadata(Meta* m) { 
		m->declare("author", "AmbisonicDecoderToolkit");
		m->declare("copyright", "(c) Aaron J. Heller 2013");
		m->declare("license", "BSD 3-Clause License");
		m->declare("name", "SATOswD3o2");
		m->declare("version", "1.2");
	}

	virtual int getNumInputs() {
		return 9;
		
	}
	virtual int getNumOutputs() {
		return 31;
		
	}
	virtual int getInputRate(int channel) {
		int rate;
		switch (channel) {
			case 0: {
				rate = 1;
				break;
			}
			case 1: {
				rate = 1;
				break;
			}
			case 2: {
				rate = 1;
				break;
			}
			case 3: {
				rate = 1;
				break;
			}
			case 4: {
				rate = 1;
				break;
			}
			case 5: {
				rate = 1;
				break;
			}
			case 6: {
				rate = 1;
				break;
			}
			case 7: {
				rate = 1;
				break;
			}
			case 8: {
				rate = 1;
				break;
			}
			default: {
				rate = -1;
				break;
			}
			
		}
		return rate;
		
	}
	virtual int getOutputRate(int channel) {
		int rate;
		switch (channel) {
			case 0: {
				rate = 1;
				break;
			}
			case 1: {
				rate = 1;
				break;
			}
			case 2: {
				rate = 1;
				break;
			}
			case 3: {
				rate = 1;
				break;
			}
			case 4: {
				rate = 1;
				break;
			}
			case 5: {
				rate = 1;
				break;
			}
			case 6: {
				rate = 1;
				break;
			}
			case 7: {
				rate = 1;
				break;
			}
			case 8: {
				rate = 1;
				break;
			}
			case 9: {
				rate = 1;
				break;
			}
			case 10: {
				rate = 1;
				break;
			}
			case 11: {
				rate = 1;
				break;
			}
			case 12: {
				rate = 1;
				break;
			}
			case 13: {
				rate = 1;
				break;
			}
			case 14: {
				rate = 1;
				break;
			}
			case 15: {
				rate = 1;
				break;
			}
			case 16: {
				rate = 1;
				break;
			}
			case 17: {
				rate = 1;
				break;
			}
			case 18: {
				rate = 1;
				break;
			}
			case 19: {
				rate = 1;
				break;
			}
			case 20: {
				rate = 1;
				break;
			}
			case 21: {
				rate = 1;
				break;
			}
			case 22: {
				rate = 1;
				break;
			}
			case 23: {
				rate = 1;
				break;
			}
			case 24: {
				rate = 1;
				break;
			}
			case 25: {
				rate = 1;
				break;
			}
			case 26: {
				rate = 1;
				break;
			}
			case 27: {
				rate = 1;
				break;
			}
			case 28: {
				rate = 1;
				break;
			}
			case 29: {
				rate = 1;
				break;
			}
			case 30: {
				rate = 1;
				break;
			}
			default: {
				rate = -1;
				break;
			}
			
		}
		return rate;
		
	}
	
	static void classInit(int samplingFreq) {
		
	}
	
	virtual void instanceConstants(int samplingFreq) {
		fSamplingFreq = samplingFreq;
		iConst0 = min(192000, max(1, fSamplingFreq));
		fConst1 = (3.14159274f / float(iConst0));
		fConst2 = float(iConst0);
		fConst3 = ((19.133934f / fConst2) + 1.0f);
		fConst4 = (1.0f / fConst3);
		fConst5 = (0.0f - (38.267868f / (fConst2 * fConst3)));
		fConst6 = ((((1098.32227f / fConst2) + 57.4018021f) / fConst2) + 1.0f);
		fConst7 = (1.0f / fConst6);
		fConst8 = (0.0f - (4393.28906f / (mydsp_faustpower2_f(fConst2) * fConst6)));
		fConst9 = (0.0f - (((4393.28906f / fConst2) + 114.803604f) / (fConst2 * fConst6)));
		iConst10 = int(((5.24453171e-05f * float(iConst0)) + 0.5f));
		iConst11 = int(((5.8272577e-05f * float(iConst0)) + 0.5f));
		iConst12 = int(((0.000145681435f * float(iConst0)) + 0.5f));
		iConst13 = int(((0.000134026923f * float(iConst0)) + 0.5f));
		iConst14 = int(((0.000262226589f * float(iConst0)) + 0.5f));
		iConst15 = int(((0.000256399333f * float(iConst0)) + 0.5f));
		
	}
	
	virtual void instanceResetUserInterface() {
		fHslider0 = FAUSTFLOAT(400.0f);
		fHslider1 = FAUSTFLOAT(0.0f);
		fCheckbox0 = FAUSTFLOAT(0.0f);
		fHslider2 = FAUSTFLOAT(-10.0f);
		
	}
	
	virtual void instanceClear() {
		for (int l0 = 0; (l0 < 2); l0 = (l0 + 1)) {
			fRec0[l0] = 0.0f;
			
		}
		for (int l1 = 0; (l1 < 2); l1 = (l1 + 1)) {
			fRec4[l1] = 0.0f;
			
		}
		for (int l2 = 0; (l2 < 2); l2 = (l2 + 1)) {
			fRec2[l2] = 0.0f;
			
		}
		for (int l3 = 0; (l3 < 3); l3 = (l3 + 1)) {
			fRec1[l3] = 0.0f;
			
		}
		for (int l4 = 0; (l4 < 2); l4 = (l4 + 1)) {
			fRec5[l4] = 0.0f;
			
		}
		for (int l5 = 0; (l5 < 3); l5 = (l5 + 1)) {
			fRec6[l5] = 0.0f;
			
		}
		for (int l6 = 0; (l6 < 2); l6 = (l6 + 1)) {
			fRec10[l6] = 0.0f;
			
		}
		for (int l7 = 0; (l7 < 2); l7 = (l7 + 1)) {
			fRec8[l7] = 0.0f;
			
		}
		for (int l8 = 0; (l8 < 3); l8 = (l8 + 1)) {
			fRec7[l8] = 0.0f;
			
		}
		for (int l9 = 0; (l9 < 2); l9 = (l9 + 1)) {
			fRec17[l9] = 0.0f;
			
		}
		for (int l10 = 0; (l10 < 2); l10 = (l10 + 1)) {
			fRec15[l10] = 0.0f;
			
		}
		for (int l11 = 0; (l11 < 2); l11 = (l11 + 1)) {
			fRec14[l11] = 0.0f;
			
		}
		for (int l12 = 0; (l12 < 2); l12 = (l12 + 1)) {
			fRec12[l12] = 0.0f;
			
		}
		for (int l13 = 0; (l13 < 3); l13 = (l13 + 1)) {
			fRec11[l13] = 0.0f;
			
		}
		for (int l14 = 0; (l14 < 2); l14 = (l14 + 1)) {
			fRec24[l14] = 0.0f;
			
		}
		for (int l15 = 0; (l15 < 2); l15 = (l15 + 1)) {
			fRec22[l15] = 0.0f;
			
		}
		for (int l16 = 0; (l16 < 2); l16 = (l16 + 1)) {
			fRec21[l16] = 0.0f;
			
		}
		for (int l17 = 0; (l17 < 2); l17 = (l17 + 1)) {
			fRec19[l17] = 0.0f;
			
		}
		for (int l18 = 0; (l18 < 3); l18 = (l18 + 1)) {
			fRec18[l18] = 0.0f;
			
		}
		for (int l19 = 0; (l19 < 2); l19 = (l19 + 1)) {
			fRec31[l19] = 0.0f;
			
		}
		for (int l20 = 0; (l20 < 2); l20 = (l20 + 1)) {
			fRec29[l20] = 0.0f;
			
		}
		for (int l21 = 0; (l21 < 2); l21 = (l21 + 1)) {
			fRec28[l21] = 0.0f;
			
		}
		for (int l22 = 0; (l22 < 2); l22 = (l22 + 1)) {
			fRec26[l22] = 0.0f;
			
		}
		for (int l23 = 0; (l23 < 3); l23 = (l23 + 1)) {
			fRec25[l23] = 0.0f;
			
		}
		for (int l24 = 0; (l24 < 2); l24 = (l24 + 1)) {
			fRec38[l24] = 0.0f;
			
		}
		for (int l25 = 0; (l25 < 2); l25 = (l25 + 1)) {
			fRec36[l25] = 0.0f;
			
		}
		for (int l26 = 0; (l26 < 2); l26 = (l26 + 1)) {
			fRec35[l26] = 0.0f;
			
		}
		for (int l27 = 0; (l27 < 2); l27 = (l27 + 1)) {
			fRec33[l27] = 0.0f;
			
		}
		for (int l28 = 0; (l28 < 3); l28 = (l28 + 1)) {
			fRec32[l28] = 0.0f;
			
		}
		for (int l29 = 0; (l29 < 2); l29 = (l29 + 1)) {
			fRec42[l29] = 0.0f;
			
		}
		for (int l30 = 0; (l30 < 2); l30 = (l30 + 1)) {
			fRec40[l30] = 0.0f;
			
		}
		for (int l31 = 0; (l31 < 3); l31 = (l31 + 1)) {
			fRec39[l31] = 0.0f;
			
		}
		for (int l32 = 0; (l32 < 2); l32 = (l32 + 1)) {
			fRec49[l32] = 0.0f;
			
		}
		for (int l33 = 0; (l33 < 2); l33 = (l33 + 1)) {
			fRec47[l33] = 0.0f;
			
		}
		for (int l34 = 0; (l34 < 2); l34 = (l34 + 1)) {
			fRec46[l34] = 0.0f;
			
		}
		for (int l35 = 0; (l35 < 2); l35 = (l35 + 1)) {
			fRec44[l35] = 0.0f;
			
		}
		for (int l36 = 0; (l36 < 3); l36 = (l36 + 1)) {
			fRec43[l36] = 0.0f;
			
		}
		for (int l37 = 0; (l37 < 2); l37 = (l37 + 1)) {
			fRec50[l37] = 0.0f;
			
		}
		for (int l38 = 0; (l38 < 12); l38 = (l38 + 1)) {
			fVec0[l38] = 0.0f;
			
		}
		for (int l39 = 0; (l39 < 13); l39 = (l39 + 1)) {
			fVec1[l39] = 0.0f;
			
		}
		for (int l40 = 0; (l40 < 12); l40 = (l40 + 1)) {
			fVec2[l40] = 0.0f;
			
		}
		for (int l41 = 0; (l41 < 12); l41 = (l41 + 1)) {
			fVec3[l41] = 0.0f;
			
		}
		for (int l42 = 0; (l42 < 13); l42 = (l42 + 1)) {
			fVec4[l42] = 0.0f;
			
		}
		for (int l43 = 0; (l43 < 12); l43 = (l43 + 1)) {
			fVec5[l43] = 0.0f;
			
		}
		IOTA = 0;
		for (int l44 = 0; (l44 < 32); l44 = (l44 + 1)) {
			fVec6[l44] = 0.0f;
			
		}
		for (int l45 = 0; (l45 < 32); l45 = (l45 + 1)) {
			fVec7[l45] = 0.0f;
			
		}
		for (int l46 = 0; (l46 < 32); l46 = (l46 + 1)) {
			fVec8[l46] = 0.0f;
			
		}
		for (int l47 = 0; (l47 < 32); l47 = (l47 + 1)) {
			fVec9[l47] = 0.0f;
			
		}
		for (int l48 = 0; (l48 < 32); l48 = (l48 + 1)) {
			fVec10[l48] = 0.0f;
			
		}
		for (int l49 = 0; (l49 < 32); l49 = (l49 + 1)) {
			fVec11[l49] = 0.0f;
			
		}
		for (int l50 = 0; (l50 < 32); l50 = (l50 + 1)) {
			fVec12[l50] = 0.0f;
			
		}
		for (int l51 = 0; (l51 < 32); l51 = (l51 + 1)) {
			fVec13[l51] = 0.0f;
			
		}
		for (int l52 = 0; (l52 < 32); l52 = (l52 + 1)) {
			fVec14[l52] = 0.0f;
			
		}
		for (int l53 = 0; (l53 < 32); l53 = (l53 + 1)) {
			fVec15[l53] = 0.0f;
			
		}
		for (int l54 = 0; (l54 < 32); l54 = (l54 + 1)) {
			fVec16[l54] = 0.0f;
			
		}
		for (int l55 = 0; (l55 < 32); l55 = (l55 + 1)) {
			fVec17[l55] = 0.0f;
			
		}
		for (int l56 = 0; (l56 < 64); l56 = (l56 + 1)) {
			fVec18[l56] = 0.0f;
			
		}
		for (int l57 = 0; (l57 < 64); l57 = (l57 + 1)) {
			fVec19[l57] = 0.0f;
			
		}
		for (int l58 = 0; (l58 < 64); l58 = (l58 + 1)) {
			fVec20[l58] = 0.0f;
			
		}
		for (int l59 = 0; (l59 < 64); l59 = (l59 + 1)) {
			fVec21[l59] = 0.0f;
			
		}
		for (int l60 = 0; (l60 < 64); l60 = (l60 + 1)) {
			fVec22[l60] = 0.0f;
			
		}
		for (int l61 = 0; (l61 < 64); l61 = (l61 + 1)) {
			fVec23[l61] = 0.0f;
			
		}
		for (int l62 = 0; (l62 < 64); l62 = (l62 + 1)) {
			fVec24[l62] = 0.0f;
			
		}
		for (int l63 = 0; (l63 < 64); l63 = (l63 + 1)) {
			fVec25[l63] = 0.0f;
			
		}
		for (int l64 = 0; (l64 < 64); l64 = (l64 + 1)) {
			fVec26[l64] = 0.0f;
			
		}
		for (int l65 = 0; (l65 < 64); l65 = (l65 + 1)) {
			fVec27[l65] = 0.0f;
			
		}
		for (int l66 = 0; (l66 < 64); l66 = (l66 + 1)) {
			fVec28[l66] = 0.0f;
			
		}
		for (int l67 = 0; (l67 < 64); l67 = (l67 + 1)) {
			fVec29[l67] = 0.0f;
			
		}
		
	}
	
	virtual void init(int samplingFreq) {
		classInit(samplingFreq);
		instanceInit(samplingFreq);
	}
	virtual void instanceInit(int samplingFreq) {
		instanceConstants(samplingFreq);
		instanceResetUserInterface();
		instanceClear();
	}
	
	virtual mydsp* clone() {
		return new mydsp();
	}
	virtual int getSampleRate() {
		return fSamplingFreq;
		
	}
	
	virtual void buildUserInterface(UI* ui_interface) {
		ui_interface->openVerticalBox("SATOswD3o2");
		ui_interface->declare(&fHslider2, "unit", "dB");
		ui_interface->addHorizontalSlider("gain", &fHslider2, -10.0f, -30.0f, 10.0f, 1.0f);
		ui_interface->declare(&fHslider1, "unit", "dB");
		ui_interface->addHorizontalSlider("lf/hf", &fHslider1, 0.0f, -3.0f, 3.0f, 0.100000001f);
		ui_interface->addCheckButton("mute", &fCheckbox0);
		ui_interface->declare(&fHslider0, "unit", "Hz");
		ui_interface->addHorizontalSlider("xover", &fHslider0, 400.0f, 200.0f, 800.0f, 20.0f);
		ui_interface->closeBox();
		
	}
	
	virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) {
		FAUSTFLOAT* input0 = inputs[0];
		FAUSTFLOAT* input1 = inputs[1];
		FAUSTFLOAT* input2 = inputs[2];
		FAUSTFLOAT* input3 = inputs[3];
		FAUSTFLOAT* input4 = inputs[4];
		FAUSTFLOAT* input5 = inputs[5];
		FAUSTFLOAT* input6 = inputs[6];
		FAUSTFLOAT* input7 = inputs[7];
		FAUSTFLOAT* input8 = inputs[8];
		FAUSTFLOAT* output0 = outputs[0];
		FAUSTFLOAT* output1 = outputs[1];
		FAUSTFLOAT* output2 = outputs[2];
		FAUSTFLOAT* output3 = outputs[3];
		FAUSTFLOAT* output4 = outputs[4];
		FAUSTFLOAT* output5 = outputs[5];
		FAUSTFLOAT* output6 = outputs[6];
		FAUSTFLOAT* output7 = outputs[7];
		FAUSTFLOAT* output8 = outputs[8];
		FAUSTFLOAT* output9 = outputs[9];
		FAUSTFLOAT* output10 = outputs[10];
		FAUSTFLOAT* output11 = outputs[11];
		FAUSTFLOAT* output12 = outputs[12];
		FAUSTFLOAT* output13 = outputs[13];
		FAUSTFLOAT* output14 = outputs[14];
		FAUSTFLOAT* output15 = outputs[15];
		FAUSTFLOAT* output16 = outputs[16];
		FAUSTFLOAT* output17 = outputs[17];
		FAUSTFLOAT* output18 = outputs[18];
		FAUSTFLOAT* output19 = outputs[19];
		FAUSTFLOAT* output20 = outputs[20];
		FAUSTFLOAT* output21 = outputs[21];
		FAUSTFLOAT* output22 = outputs[22];
		FAUSTFLOAT* output23 = outputs[23];
		FAUSTFLOAT* output24 = outputs[24];
		FAUSTFLOAT* output25 = outputs[25];
		FAUSTFLOAT* output26 = outputs[26];
		FAUSTFLOAT* output27 = outputs[27];
		FAUSTFLOAT* output28 = outputs[28];
		FAUSTFLOAT* output29 = outputs[29];
		FAUSTFLOAT* output30 = outputs[30];
		float fSlow0 = (0.00100000005f * float(fHslider0));
		float fSlow1 = (0.00100000005f * powf(10.0f, (0.0500000007f * float(fHslider1))));
		float fSlow2 = (0.00100000005f * (float((float(fCheckbox0) < 0.5f)) * powf(10.0f, (0.0500000007f * float(fHslider2)))));
		for (int i = 0; (i < count); i = (i + 1)) {
			fRec0[0] = (fSlow0 + (0.999000013f * fRec0[1]));
			float fTemp0 = tanf((fConst1 * fRec0[0]));
			float fTemp1 = mydsp_faustpower2_f(fTemp0);
			float fTemp2 = (fConst4 * float(input3[i]));
			float fTemp3 = (fConst5 * fRec2[1]);
			fRec4[0] = (fTemp2 + (fRec4[1] + fTemp3));
			fRec2[0] = fRec4[0];
			float fRec3 = (fTemp3 + fTemp2);
			float fTemp4 = ((fTemp0 * (fTemp0 + -2.0f)) + 1.0f);
			float fTemp5 = (fTemp1 + -1.0f);
			float fTemp6 = ((fTemp0 * (fTemp0 + 2.0f)) + 1.0f);
			fRec1[0] = (fRec3 - (((fTemp4 * fRec1[2]) + (2.0f * (fTemp5 * fRec1[1]))) / fTemp6));
			fRec5[0] = (fSlow1 + (0.999000013f * fRec5[1]));
			float fTemp7 = (fRec5[0] * fTemp6);
			float fTemp8 = (0.0f - (2.0f / fTemp6));
			float fTemp9 = (((fTemp1 * (fRec1[2] + (fRec1[0] + (2.0f * fRec1[1])))) / fTemp7) + (0.774596691f * (fRec5[0] * (0.0f - ((fTemp8 * fRec1[1]) + ((fRec1[0] + fRec1[2]) / fTemp6))))));
			fRec6[0] = (float(input0[i]) - (((fRec6[2] * fTemp4) + (2.0f * (fRec6[1] * fTemp5))) / fTemp6));
			float fTemp10 = (((fTemp1 * (fRec6[2] + (fRec6[0] + (2.0f * fRec6[1])))) / fTemp7) + (fRec5[0] * (0.0f - ((fRec6[1] * fTemp8) + ((fRec6[0] + fRec6[2]) / fTemp6)))));
			float fTemp11 = (fConst4 * float(input2[i]));
			float fTemp12 = (fConst5 * fRec8[1]);
			fRec10[0] = (fTemp11 + (fRec10[1] + fTemp12));
			fRec8[0] = fRec10[0];
			float fRec9 = (fTemp12 + fTemp11);
			fRec7[0] = (fRec9 - (((fTemp4 * fRec7[2]) + (2.0f * (fTemp5 * fRec7[1]))) / fTemp6));
			float fTemp13 = (((fTemp1 * (fRec7[2] + (fRec7[0] + (2.0f * fRec7[1])))) / fTemp7) + (0.774596691f * (fRec5[0] * (0.0f - ((fTemp8 * fRec7[1]) + ((fRec7[0] + fRec7[2]) / fTemp6))))));
			float fTemp14 = (fConst7 * float(input4[i]));
			float fTemp15 = (fConst8 * fRec12[1]);
			float fTemp16 = (fConst9 * fRec15[1]);
			fRec17[0] = (fTemp14 + (fTemp15 + (fRec17[1] + fTemp16)));
			fRec15[0] = fRec17[0];
			float fRec16 = ((fTemp16 + fTemp15) + fTemp14);
			fRec14[0] = (fRec15[0] + fRec14[1]);
			fRec12[0] = fRec14[0];
			float fRec13 = fRec16;
			fRec11[0] = (fRec13 - (((fTemp4 * fRec11[2]) + (2.0f * (fTemp5 * fRec11[1]))) / fTemp6));
			float fTemp17 = (((fTemp1 * (fRec11[2] + (fRec11[0] + (2.0f * fRec11[1])))) / fTemp7) + (0.400000006f * (fRec5[0] * (0.0f - ((fTemp8 * fRec11[1]) + ((fRec11[0] + fRec11[2]) / fTemp6))))));
			float fTemp18 = (fConst7 * float(input6[i]));
			float fTemp19 = (fConst8 * fRec19[1]);
			float fTemp20 = (fConst9 * fRec22[1]);
			fRec24[0] = (fTemp18 + (fTemp19 + (fRec24[1] + fTemp20)));
			fRec22[0] = fRec24[0];
			float fRec23 = ((fTemp20 + fTemp19) + fTemp18);
			fRec21[0] = (fRec22[0] + fRec21[1]);
			fRec19[0] = fRec21[0];
			float fRec20 = fRec23;
			fRec18[0] = (fRec20 - (((fTemp4 * fRec18[2]) + (2.0f * (fTemp5 * fRec18[1]))) / fTemp6));
			float fTemp21 = (((fTemp1 * (fRec18[2] + (fRec18[0] + (2.0f * fRec18[1])))) / fTemp7) + (0.400000006f * (fRec5[0] * (0.0f - ((fTemp8 * fRec18[1]) + ((fRec18[0] + fRec18[2]) / fTemp6))))));
			float fTemp22 = (fConst7 * float(input7[i]));
			float fTemp23 = (fConst8 * fRec26[1]);
			float fTemp24 = (fConst9 * fRec29[1]);
			fRec31[0] = (fTemp22 + (fTemp23 + (fRec31[1] + fTemp24)));
			fRec29[0] = fRec31[0];
			float fRec30 = ((fTemp24 + fTemp23) + fTemp22);
			fRec28[0] = (fRec29[0] + fRec28[1]);
			fRec26[0] = fRec28[0];
			float fRec27 = fRec30;
			fRec25[0] = (fRec27 - (((fTemp4 * fRec25[2]) + (2.0f * (fTemp5 * fRec25[1]))) / fTemp6));
			float fTemp25 = (((fTemp1 * (fRec25[2] + (fRec25[0] + (2.0f * fRec25[1])))) / fTemp7) + (0.400000006f * (fRec5[0] * (0.0f - ((fTemp8 * fRec25[1]) + ((fRec25[0] + fRec25[2]) / fTemp6))))));
			float fTemp26 = (fConst7 * float(input8[i]));
			float fTemp27 = (fConst8 * fRec33[1]);
			float fTemp28 = (fConst9 * fRec36[1]);
			fRec38[0] = (fTemp26 + (fTemp27 + (fRec38[1] + fTemp28)));
			fRec36[0] = fRec38[0];
			float fRec37 = ((fTemp28 + fTemp27) + fTemp26);
			fRec35[0] = (fRec36[0] + fRec35[1]);
			fRec33[0] = fRec35[0];
			float fRec34 = fRec37;
			fRec32[0] = (fRec34 - (((2.0f * (fTemp5 * fRec32[1])) + (fTemp4 * fRec32[2])) / fTemp6));
			float fTemp29 = ((((((2.0f * fRec32[1]) + fRec32[0]) + fRec32[2]) * fTemp1) / fTemp7) + (0.400000006f * ((0.0f - ((fTemp8 * fRec32[1]) + ((fRec32[0] + fRec32[2]) / fTemp6))) * fRec5[0])));
			float fTemp30 = (fConst4 * float(input1[i]));
			float fTemp31 = (fConst5 * fRec40[1]);
			fRec42[0] = (fTemp30 + (fRec42[1] + fTemp31));
			fRec40[0] = fRec42[0];
			float fRec41 = (fTemp31 + fTemp30);
			fRec39[0] = (fRec41 - (((fTemp4 * fRec39[2]) + (2.0f * (fTemp5 * fRec39[1]))) / fTemp6));
			float fTemp32 = (((fTemp1 * (fRec39[2] + (fRec39[0] + (2.0f * fRec39[1])))) / fTemp7) + (0.774596691f * (fRec5[0] * (0.0f - ((fTemp8 * fRec39[1]) + ((fRec39[0] + fRec39[2]) / fTemp6))))));
			float fTemp33 = (fConst7 * float(input5[i]));
			float fTemp34 = (fConst8 * fRec44[1]);
			float fTemp35 = (fConst9 * fRec47[1]);
			fRec49[0] = (fTemp33 + (fTemp34 + (fRec49[1] + fTemp35)));
			fRec47[0] = fRec49[0];
			float fRec48 = ((fTemp35 + fTemp34) + fTemp33);
			fRec46[0] = (fRec47[0] + fRec46[1]);
			fRec44[0] = fRec46[0];
			float fRec45 = fRec48;
			fRec43[0] = (fRec45 - (((fTemp4 * fRec43[2]) + (2.0f * (fTemp5 * fRec43[1]))) / fTemp6));
			float fTemp36 = (((fTemp1 * (fRec43[2] + (fRec43[0] + (2.0f * fRec43[1])))) / fTemp7) + (0.400000006f * (fRec5[0] * (0.0f - ((fTemp8 * fRec43[1]) + ((fRec43[0] + fRec43[2]) / fTemp6))))));
			fRec50[0] = (fSlow2 + (0.999000013f * fRec50[1]));
			output0[i] = FAUSTFLOAT((((((((2.45000001e-06f * fTemp9) + ((0.0473226868f * fTemp10) + (0.0769467279f * fTemp13))) + (3.70409998e-06f * fTemp17)) + (0.0872831121f * fTemp21)) + (4.46969989e-06f * fTemp25)) - (((1.50289998e-06f * fTemp29) + (2.71919998e-06f * fTemp32)) + (5.04480022e-06f * fTemp36))) * fRec50[0]));
			fVec0[0] = (((0.0176964421f * fTemp25) + ((0.0338490047f * fTemp21) + ((0.0510204248f * fTemp36) + (((0.0108339777f * fTemp9) + ((0.0463922769f * fTemp13) + ((0.0341214538f * fTemp10) + (0.0301853716f * fTemp32)))) + (0.011978806f * fTemp17))))) - (0.0133990385f * fTemp29));
			output1[i] = FAUSTFLOAT((0.998005569f * (fVec0[iConst10] * fRec50[0])));
			fVec1[0] = (((0.0352820829f * fTemp21) + (((0.0510143228f * fTemp13) + ((0.038172856f * fTemp10) + (0.0257401336f * fTemp32))) + (0.0426589735f * fTemp36))) - ((0.042658627f * fTemp25) + ((0.0195993222f * fTemp17) + ((0.0257381666f * fTemp9) + (4.50529978e-06f * fTemp29)))));
			output2[i] = FAUSTFLOAT((0.997783959f * (fVec1[iConst11] * fRec50[0])));
			fVec2[0] = (((0.0338515826f * fTemp21) + ((0.0119755501f * fTemp17) + ((0.0463907383f * fTemp13) + ((0.0341186747f * fTemp10) + (0.0133855147f * fTemp29))))) - ((0.0510147251f * fTemp25) + (((0.0108332634f * fTemp32) + (0.0301783718f * fTemp9)) + (0.017694287f * fTemp36))));
			output3[i] = FAUSTFLOAT((0.998005569f * (fVec2[iConst10] * fRec50[0])));
			fVec3[0] = (((0.0324303731f * fTemp21) + (((0.0300636496f * fTemp10) + (0.0417693704f * fTemp13)) + (0.00778828422f * fTemp17))) - ((0.012267625f * fTemp25) + ((0.0457643978f * fTemp36) + ((0.0071219611f * fTemp9) + ((0.0265663434f * fTemp32) + (0.0134784635f * fTemp29))))));
			output4[i] = FAUSTFLOAT((0.998005569f * (fVec3[iConst10] * fRec50[0])));
			fVec4[0] = (((0.0426651761f * fTemp25) + ((0.0352924652f * fTemp21) + ((0.0257430729f * fTemp9) + ((0.0510256663f * fTemp13) + ((0.0381806493f * fTemp10) + (4.58500011e-07f * fTemp29)))))) - ((0.0426623151f * fTemp36) + ((0.0257410146f * fTemp32) + (0.019598661f * fTemp17))));
			output5[i] = FAUSTFLOAT((0.997783959f * (fVec4[iConst11] * fRec50[0])));
			fVec5[0] = ((0.0457637608f * fTemp25) + ((0.0324229002f * fTemp21) + ((0.0122708166f * fTemp36) + ((0.00778933102f * fTemp17) + ((0.0265659001f * fTemp9) + ((0.0417632945f * fTemp13) + ((0.00712388381f * fTemp32) + ((0.0300600771f * fTemp10) + (0.0134779755f * fTemp29)))))))));
			output6[i] = FAUSTFLOAT((0.998005569f * (fVec5[iConst10] * fRec50[0])));
			fVec6[(IOTA & 31)] = ((((0.0200215131f * fTemp13) + ((0.0235311687f * fTemp10) + (0.0338993929f * fTemp32))) + (0.0357975774f * fTemp36)) - ((0.00217522983f * fTemp25) + ((0.00514168013f * fTemp21) + ((0.00219735736f * fTemp17) + ((0.00145658397f * fTemp9) + (0.0306473803f * fTemp29))))));
			output7[i] = FAUSTFLOAT((0.994459808f * (fVec6[((IOTA - iConst12) & 31)] * fRec50[0])));
			fVec7[(IOTA & 31)] = ((((0.0179317892f * fTemp13) + ((0.0306961462f * fTemp10) + (0.0410687849f * fTemp32))) + (0.0297248401f * fTemp36)) - ((0.0178549215f * fTemp25) + ((0.0192471165f * fTemp21) + ((0.0396838821f * fTemp17) + ((0.0242131073f * fTemp9) + (0.0220683031f * fTemp29))))));
			output8[i] = FAUSTFLOAT((0.994903028f * (fVec7[((IOTA - iConst13) & 31)] * fRec50[0])));
			fVec8[(IOTA & 31)] = (((0.0144822551f * fTemp36) + ((0.0157211255f * fTemp13) + ((0.0165882278f * fTemp32) + ((0.0242891777f * fTemp10) + (0.0222139508f * fTemp29))))) - ((0.0264675654f * fTemp25) + ((0.0132820914f * fTemp21) + ((0.0335474797f * fTemp9) + (0.0282651987f * fTemp17)))));
			output9[i] = FAUSTFLOAT((0.994903028f * (fVec8[((IOTA - iConst13) & 31)] * fRec50[0])));
			fVec9[(IOTA & 31)] = (((0.00217607711f * fTemp36) + ((0.0200138334f * fTemp13) + ((0.00145928503f * fTemp32) + ((0.0235146843f * fTemp10) + (0.0306251757f * fTemp29))))) - ((0.0357850008f * fTemp25) + ((0.00512850937f * fTemp21) + ((0.0338744335f * fTemp9) + (0.00220371434f * fTemp17)))));
			output10[i] = FAUSTFLOAT((0.994459808f * (fVec9[((IOTA - iConst12) & 31)] * fRec50[0])));
			fVec10[(IOTA & 31)] = (((0.0275476705f * fTemp17) + ((0.0157179944f * fTemp13) + ((0.0242880136f * fTemp10) + (0.0234601125f * fTemp29)))) - ((0.0270602293f * fTemp25) + ((0.0132877538f * fTemp21) + (((0.0158460233f * fTemp32) + (0.0339761786f * fTemp9)) + (0.0134490049f * fTemp36)))));
			output11[i] = FAUSTFLOAT((0.994903028f * (fVec10[((IOTA - iConst13) & 31)] * fRec50[0])));
			fVec11[(IOTA & 31)] = ((((0.0363172293f * fTemp10) + (0.0244163014f * fTemp13)) + (0.0446210802f * fTemp17)) - ((0.0242723655f * fTemp25) + ((0.0170691777f * fTemp21) + ((0.0376785174f * fTemp36) + ((0.0285261497f * fTemp9) + ((0.0464762598f * fTemp32) + (0.0232048221f * fTemp29)))))));
			output12[i] = FAUSTFLOAT((0.994903028f * (fVec11[((IOTA - iConst13) & 31)] * fRec50[0])));
			fVec12[(IOTA & 31)] = (((0.00217205891f * fTemp25) + ((0.00145378441f * fTemp9) + ((0.023518106f * fTemp10) + (0.0200154874f * fTemp13)))) - ((0.0051292195f * fTemp21) + ((((0.0306292567f * fTemp29) + (0.033879105f * fTemp32)) + (0.0021929564f * fTemp17)) + (0.0357862934f * fTemp36))));
			output13[i] = FAUSTFLOAT((0.994459808f * (fVec12[((IOTA - iConst12) & 31)] * fRec50[0])));
			fVec13[(IOTA & 31)] = ((((0.0242112409f * fTemp9) + ((0.0307002682f * fTemp10) + (0.0179308224f * fTemp13))) + (0.017853152f * fTemp25)) - ((0.019253334f * fTemp21) + ((0.0297232959f * fTemp36) + ((0.0396812446f * fTemp17) + ((0.0410771593f * fTemp32) + (0.0220802464f * fTemp29))))));
			output14[i] = FAUSTFLOAT((0.994903028f * (fVec13[((IOTA - iConst13) & 31)] * fRec50[0])));
			fVec14[(IOTA & 31)] = (((0.026488414f * fTemp25) + ((0.0335719511f * fTemp9) + ((0.015737446f * fTemp13) + ((0.0243119132f * fTemp10) + (0.0222150907f * fTemp29))))) - ((0.0132887159f * fTemp21) + ((0.014501893f * fTemp36) + ((0.0166117605f * fTemp32) + (0.0282988027f * fTemp17)))));
			output15[i] = FAUSTFLOAT((0.994903028f * (fVec14[((IOTA - iConst13) & 31)] * fRec50[0])));
			fVec15[(IOTA & 31)] = (((0.0357914977f * fTemp25) + ((0.0338919684f * fTemp9) + ((0.0200174712f * fTemp13) + ((0.0235254578f * fTemp10) + (0.0306433197f * fTemp29))))) - ((0.00513874181f * fTemp21) + ((0.0021808676f * fTemp36) + ((0.00146376574f * fTemp32) + (0.00221101847f * fTemp17)))));
			output16[i] = FAUSTFLOAT((0.994459808f * (fVec15[((IOTA - iConst12) & 31)] * fRec50[0])));
			fVec16[(IOTA & 31)] = (((0.0376720801f * fTemp25) + ((0.0242657457f * fTemp36) + ((0.0446238518f * fTemp17) + ((0.0464785807f * fTemp9) + ((0.0244102012f * fTemp13) + ((0.0285257287f * fTemp32) + ((0.0363168567f * fTemp10) + (0.0232067332f * fTemp29)))))))) - (0.0170746874f * fTemp21));
			output17[i] = FAUSTFLOAT((0.994903028f * (fVec16[((IOTA - iConst13) & 31)] * fRec50[0])));
			fVec17[(IOTA & 31)] = (((0.013463459f * fTemp25) + ((0.0270859804f * fTemp36) + (((0.015851954f * fTemp9) + ((0.0157361906f * fTemp13) + ((0.0242996197f * fTemp10) + (0.0339871384f * fTemp32)))) + (0.0275524594f * fTemp17)))) - ((0.0132781463f * fTemp21) + (0.0234640837f * fTemp29)));
			output18[i] = FAUSTFLOAT((0.994903028f * (fVec17[((IOTA - iConst13) & 31)] * fRec50[0])));
			fVec18[(IOTA & 63)] = ((((0.0588501506f * fTemp10) + (0.0887992382f * fTemp32)) + (4.19029993e-06f * fTemp25)) - ((0.0305124186f * fTemp21) + ((0.0354953706f * fTemp36) + ((1.63954992e-05f * fTemp17) + ((1.02769e-05f * fTemp9) + ((0.0229201801f * fTemp13) + (0.0842990875f * fTemp29)))))));
			output19[i] = FAUSTFLOAT((0.990027726f * (fVec18[((IOTA - iConst14) & 63)] * fRec50[0])));
			fVec19[(IOTA & 63)] = ((((0.0475860611f * fTemp10) + (0.061254438f * fTemp32)) + (0.022038091f * fTemp25)) - ((0.0203053225f * fTemp21) + ((0.0381666757f * fTemp36) + ((0.0582042076f * fTemp17) + ((0.0353607573f * fTemp9) + ((0.0270938333f * fTemp13) + (0.0336138234f * fTemp29)))))));
			output20[i] = FAUSTFLOAT((0.990249336f * (fVec19[((IOTA - iConst15) & 63)] * fRec50[0])));
			fVec20[(IOTA & 63)] = (((0.0350273214f * fTemp25) + (((0.0334427133f * fTemp29) + (0.0532584041f * fTemp10)) + (0.042079974f * fTemp32))) - ((0.0254155602f * fTemp21) + ((0.0189149324f * fTemp36) + (((0.0250215139f * fTemp13) + (0.067867741f * fTemp9)) + (0.0682127848f * fTemp17)))));
			output21[i] = FAUSTFLOAT((0.990249336f * (fVec20[((IOTA - iConst15) & 63)] * fRec50[0])));
			fVec21[(IOTA & 63)] = (((0.0354968123f * fTemp25) + ((6.81699987e-07f * fTemp32) + ((0.0588359646f * fTemp10) + (0.0842888653f * fTemp29)))) - ((0.0305128377f * fTemp21) + ((3.29199992e-07f * fTemp36) + (((0.0229164064f * fTemp13) + (0.0887830555f * fTemp9)) + (1.60189995e-06f * fTemp17)))));
			output22[i] = FAUSTFLOAT((0.990027726f * (fVec21[((IOTA - iConst14) & 63)] * fRec50[0])));
			fVec22[(IOTA & 63)] = (((0.035022445f * fTemp25) + ((0.0189076494f * fTemp36) + ((0.0682108104f * fTemp17) + ((0.0532494672f * fTemp10) + (0.0334550142f * fTemp29))))) - ((((0.0420738682f * fTemp32) + (0.0250031725f * fTemp13)) + (0.0678718835f * fTemp9)) + (0.0254423618f * fTemp21)));
			output23[i] = FAUSTFLOAT((0.990249336f * (fVec22[((IOTA - iConst15) & 63)] * fRec50[0])));
			fVec23[(IOTA & 63)] = (((((0.0476111248f * fTemp10) + (0.0582197383f * fTemp17)) + (0.0381643772f * fTemp36)) + (0.0220359024f * fTemp25)) - (((((0.0612792261f * fTemp32) + (0.0336324349f * fTemp29)) + (0.0271065626f * fTemp13)) + (0.0353708342f * fTemp9)) + (0.0202950761f * fTemp21)));
			output24[i] = FAUSTFLOAT((0.990249336f * (fVec23[((IOTA - iConst15) & 63)] * fRec50[0])));
			fVec24[(IOTA & 63)] = (((2.9099001e-06f * fTemp25) + (((0.0588502251f * fTemp10) + (4.36999983e-08f * fTemp9)) + (0.0354840085f * fTemp36))) - ((((0.0229036063f * fTemp13) + ((0.0888105035f * fTemp32) + (0.0843136683f * fTemp29))) + (4.06400005e-07f * fTemp17)) + (0.0305345897f * fTemp21)));
			output25[i] = FAUSTFLOAT((0.990027726f * (fVec24[((IOTA - iConst14) & 63)] * fRec50[0])));
			fVec25[(IOTA & 63)] = ((((0.0476081558f * fTemp10) + (0.0353744999f * fTemp9)) + (0.03817489f * fTemp36)) - ((0.022037847f * fTemp25) + ((0.0202873833f * fTemp21) + ((0.0582216233f * fTemp17) + ((0.0271157604f * fTemp13) + ((0.0612672567f * fTemp32) + (0.03361056f * fTemp29)))))));
			output26[i] = FAUSTFLOAT((0.990249336f * (fVec25[((IOTA - iConst15) & 63)] * fRec50[0])));
			fVec26[(IOTA & 63)] = (((0.0189102255f * fTemp36) + ((0.0678689703f * fTemp9) + ((0.0532465205f * fTemp10) + (0.0334601849f * fTemp29)))) - ((0.0350250863f * fTemp25) + ((0.025433531f * fTemp21) + (((0.04206644f * fTemp32) + (0.0250075795f * fTemp13)) + (0.0682025552f * fTemp17)))));
			output27[i] = FAUSTFLOAT((0.990249336f * (fVec26[((IOTA - iConst15) & 63)] * fRec50[0])));
			fVec27[(IOTA & 63)] = (((0.0887977406f * fTemp9) + (((0.0843039677f * fTemp29) + (0.0588410161f * fTemp10)) + (1.94500004e-07f * fTemp32))) - ((0.0354756378f * fTemp25) + ((0.0305338241f * fTemp21) + ((4.29940019e-06f * fTemp36) + ((3.24199988e-07f * fTemp17) + (0.0228981767f * fTemp13))))));
			output28[i] = FAUSTFLOAT((0.990027726f * (fVec27[((IOTA - iConst14) & 63)] * fRec50[0])));
			fVec28[(IOTA & 63)] = (((0.0582146868f * fTemp17) + ((0.0612718165f * fTemp9) + ((0.0353668891f * fTemp32) + ((0.0476072319f * fTemp10) + (0.0336267091f * fTemp29))))) - ((0.0381675474f * fTemp25) + ((0.0202854425f * fTemp21) + ((0.0271103531f * fTemp13) + (0.0220347978f * fTemp36)))));
			output29[i] = FAUSTFLOAT((0.990249336f * (fVec28[((IOTA - iConst15) & 63)] * fRec50[0])));
			fVec29[(IOTA & 63)] = ((((0.042072583f * fTemp9) + ((0.0532519892f * fTemp10) + (0.0678741783f * fTemp32))) + (0.0682138726f * fTemp17)) - ((0.0189079586f * fTemp25) + ((0.0254328717f * fTemp21) + ((0.0350133739f * fTemp36) + ((0.025004046f * fTemp13) + (0.0334611647f * fTemp29))))));
			output30[i] = FAUSTFLOAT((0.990249336f * (fVec29[((IOTA - iConst15) & 63)] * fRec50[0])));
			fRec0[1] = fRec0[0];
			fRec4[1] = fRec4[0];
			fRec2[1] = fRec2[0];
			fRec1[2] = fRec1[1];
			fRec1[1] = fRec1[0];
			fRec5[1] = fRec5[0];
			fRec6[2] = fRec6[1];
			fRec6[1] = fRec6[0];
			fRec10[1] = fRec10[0];
			fRec8[1] = fRec8[0];
			fRec7[2] = fRec7[1];
			fRec7[1] = fRec7[0];
			fRec17[1] = fRec17[0];
			fRec15[1] = fRec15[0];
			fRec14[1] = fRec14[0];
			fRec12[1] = fRec12[0];
			fRec11[2] = fRec11[1];
			fRec11[1] = fRec11[0];
			fRec24[1] = fRec24[0];
			fRec22[1] = fRec22[0];
			fRec21[1] = fRec21[0];
			fRec19[1] = fRec19[0];
			fRec18[2] = fRec18[1];
			fRec18[1] = fRec18[0];
			fRec31[1] = fRec31[0];
			fRec29[1] = fRec29[0];
			fRec28[1] = fRec28[0];
			fRec26[1] = fRec26[0];
			fRec25[2] = fRec25[1];
			fRec25[1] = fRec25[0];
			fRec38[1] = fRec38[0];
			fRec36[1] = fRec36[0];
			fRec35[1] = fRec35[0];
			fRec33[1] = fRec33[0];
			fRec32[2] = fRec32[1];
			fRec32[1] = fRec32[0];
			fRec42[1] = fRec42[0];
			fRec40[1] = fRec40[0];
			fRec39[2] = fRec39[1];
			fRec39[1] = fRec39[0];
			fRec49[1] = fRec49[0];
			fRec47[1] = fRec47[0];
			fRec46[1] = fRec46[0];
			fRec44[1] = fRec44[0];
			fRec43[2] = fRec43[1];
			fRec43[1] = fRec43[0];
			fRec50[1] = fRec50[0];
			for (int j0 = 11; (j0 > 0); j0 = (j0 - 1)) {
				fVec0[j0] = fVec0[(j0 - 1)];
				
			}
			for (int j1 = 12; (j1 > 0); j1 = (j1 - 1)) {
				fVec1[j1] = fVec1[(j1 - 1)];
				
			}
			for (int j2 = 11; (j2 > 0); j2 = (j2 - 1)) {
				fVec2[j2] = fVec2[(j2 - 1)];
				
			}
			for (int j3 = 11; (j3 > 0); j3 = (j3 - 1)) {
				fVec3[j3] = fVec3[(j3 - 1)];
				
			}
			for (int j4 = 12; (j4 > 0); j4 = (j4 - 1)) {
				fVec4[j4] = fVec4[(j4 - 1)];
				
			}
			for (int j5 = 11; (j5 > 0); j5 = (j5 - 1)) {
				fVec5[j5] = fVec5[(j5 - 1)];
				
			}
			IOTA = (IOTA + 1);
			
		}
		
	}

	
};

//----------------------------------------------------------------------------
// SuperCollider/Faust interface
//----------------------------------------------------------------------------

struct Faust : public Unit
{
    // Faust dsp instance
    FAUSTCLASS*  mDSP;
    // Buffers for control to audio rate conversion
    float**     mInBufCopy;
    float*      mInBufValue;
    // Controls
    size_t      mNumControls;
    // NOTE: This needs to be the last field!
    //
    // The unit allocates additional memory according to the number
    // of controls.
    Control     mControls[0];

    int getNumAudioInputs() { return mDSP->getNumInputs(); }
};

// Global state

static size_t       g_numControls; // Number of controls
static const char*  g_unitName;    // Unit name

// Initialize the global state with unit name and sample rate.
void initState(const std::string& name, int sampleRate);

// Return the unit size in bytes, including static fields and controls.
static size_t unitSize();

// Convert a file name to a valid unit name.
static std::string fileNameToUnitName(const std::string& fileName);

// Convert the XML unit name to a valid class name.
static std::string normalizeClassName(const std::string& name);

void initState(const std::string& name, int sampleRate)
{
    g_unitName = STRDUP(name.c_str());

    mydsp* dsp = new FAUSTCLASS;
    ControlCounter* cc = new ControlCounter;

    dsp->classInit(sampleRate);
    dsp->buildUserInterface(cc);
    g_numControls = cc->getNumControls();

    delete dsp;
    delete cc;
}

size_t unitSize()
{
    return sizeof(Faust) + g_numControls * sizeof(Control);
}

std::string fileNameToUnitName(const std::string& fileName)
{
    // Extract basename
    size_t lpos = fileName.rfind('/', fileName.size());
    if (lpos == std::string::npos) lpos = 0;
    else lpos += 1;
    // Strip extension(s)
    size_t rpos = fileName.find('.', lpos);
    // Return substring
    return fileName.substr(lpos, rpos > lpos ? rpos - lpos : 0);
}

// Globals

static InterfaceTable* ft;

// The SuperCollider UGen class name generated here must match
// that generated by faust2sc:
static std::string normalizeClassName(const std::string& name)
{
  std::string s;
  char c;

  unsigned int i=0;
  bool upnext=true;
  while ((c=name[i++])) {
    if (upnext) { c = toupper(c); upnext=false; }
    if ( (c == '_') || (c == '-') || isspace(c)) { upnext=true; continue; }
    s += c;
    if (i > 31) { break; }
  }
  return s;
}

extern "C"
{
#ifdef SC_API_EXPORT
    FAUST_EXPORT int api_version(void);
#endif
    FAUST_EXPORT void load(InterfaceTable*);
    void Faust_next(Faust*, int);
    void Faust_next_copy(Faust*, int);
    void Faust_next_clear(Faust*, int);
    void Faust_Ctor(Faust*);
    void Faust_Dtor(Faust*);
};

inline static void fillBuffer(float* dst, int n, float v)
{
    Fill(n, dst, v);
}

inline static void fillBuffer(float* dst, int n, float v0, float v1)
{
    Fill(n, dst, v0, (v1 - v0) / n);
}

inline static void copyBuffer(float* dst, int n, float* src)
{
    Copy(n, dst, src);
}

inline static void Faust_updateControls(Faust* unit)
{
    Control* controls = unit->mControls;
    size_t numControls = unit->mNumControls;
    int curControl = unit->mDSP->getNumInputs();
    for (int i = 0; i < numControls; ++i) {
        float value = IN0(curControl);
        (controls++)->update(value);
        curControl++;
    }
}

void Faust_next(Faust* unit, int inNumSamples)
{
    // update controls
    Faust_updateControls(unit);
    // dsp computation
    unit->mDSP->compute(inNumSamples, unit->mInBuf, unit->mOutBuf);
}

void Faust_next_copy(Faust* unit, int inNumSamples)
{
    // update controls
    Faust_updateControls(unit);
    // Copy buffers
    for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
        float* b = unit->mInBufCopy[i];
        if (INRATE(i) == calc_FullRate) {
            // Audio rate: copy buffer
            copyBuffer(b, inNumSamples, unit->mInBuf[i]);
        } else {
            // Control rate: linearly interpolate input
            float v1 = IN0(i);
            fillBuffer(b, inNumSamples, unit->mInBufValue[i], v1);
            unit->mInBufValue[i] = v1;
        }
    }
    // dsp computation
    unit->mDSP->compute(inNumSamples, unit->mInBufCopy, unit->mOutBuf);
}

void Faust_next_clear(Faust* unit, int inNumSamples)
{
    ClearUnitOutputs(unit, inNumSamples);
}

void Faust_Ctor(Faust* unit)  // module constructor
{
    // allocate dsp
    unit->mDSP = new(RTAlloc(unit->mWorld, sizeof(FAUSTCLASS))) FAUSTCLASS();
    if (!unit->mDSP) {
        Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
        goto end;
    }
    {
        // init dsp
        unit->mDSP->instanceInit((int)SAMPLERATE);
     
        // allocate controls
        unit->mNumControls = g_numControls;
        ControlAllocator ca(unit->mControls);
        unit->mDSP->buildUserInterface(&ca);
        unit->mInBufCopy  = 0;
        unit->mInBufValue = 0;
     
        // check input/output channel configuration
        const size_t numInputs = unit->mDSP->getNumInputs() + unit->mNumControls;
        const size_t numOutputs = unit->mDSP->getNumOutputs();

        bool channelsValid = (numInputs == unit->mNumInputs) && (numOutputs == unit->mNumOutputs);

        if (channelsValid) {
            bool rateValid = true;
            for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
                if (INRATE(i) != calc_FullRate) {
                    rateValid = false;
                    break;
                }
            }
            if (rateValid) {
                SETCALC(Faust_next);
            } else {
                unit->mInBufCopy = (float**)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*sizeof(float*));
                if (!unit->mInBufCopy) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                // Allocate memory for input buffer copies (numInputs * bufLength)
                // and linear interpolation state (numInputs)
                // = numInputs * (bufLength + 1)
                unit->mInBufValue = (float*)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*sizeof(float));
                if (!unit->mInBufValue) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                // Aquire memory for interpolator state.
                float* mem = (float*)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*BUFLENGTH*sizeof(float));
                if (mem) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
                    // Initialize interpolator.
                    unit->mInBufValue[i] = IN0(i);
                    // Aquire buffer memory.
                    unit->mInBufCopy[i] = mem;
                    mem += BUFLENGTH;
                }
                SETCALC(Faust_next_copy);
            }
    #if !defined(NDEBUG)
            Print("Faust[%s]:\n", g_unitName);
            Print("    Inputs:   %d\n"
                  "    Outputs:  %d\n"
                  "    Callback: %s\n",
                  numInputs, numOutputs,
                  unit->mCalcFunc == (UnitCalcFunc)Faust_next ? "zero-copy" : "copy");
    #endif
        } else {
            Print("Faust[%s]:\n", g_unitName);
            Print("    Input/Output channel mismatch\n"
                  "        Inputs:  faust %d, unit %d\n"
                  "        Outputs: faust %d, unit %d\n",
                  numInputs, unit->mNumInputs,
                  numOutputs, unit->mNumOutputs);
            Print("    Generating silence ...\n");
            SETCALC(Faust_next_clear);
        }
    }
    
end:
    // Fix for https://github.com/grame-cncm/faust/issues/13
    ClearUnitOutputs(unit, 1);
}

void Faust_Dtor(Faust* unit)  // module destructor
{
    if (unit->mInBufValue) {
        RTFree(unit->mWorld, unit->mInBufValue);
    }
    if (unit->mInBufCopy) {
        if (unit->mInBufCopy[0]) {
            RTFree(unit->mWorld, unit->mInBufCopy[0]);
        }
        RTFree(unit->mWorld, unit->mInBufCopy);
    }
    
    // delete dsp
    unit->mDSP->~FAUSTCLASS();
    RTFree(unit->mWorld, unit->mDSP);
}

#ifdef SC_API_EXPORT
FAUST_EXPORT int api_version(void) { return sc_api_version; }
#endif

FAUST_EXPORT void load(InterfaceTable* inTable)
{
    ft = inTable;

    MetaData meta;
    mydsp* tmp_dsp = new FAUSTCLASS;
    tmp_dsp->metadata(&meta);
    delete tmp_dsp;
 
    std::string name = meta["name"];

    if (name.empty()) {
        name = fileNameToUnitName(__FILE__);
    }
  
    name = normalizeClassName(name);

#if !defined(NDEBUG) & defined(SC_API_EXPORT)
    Print("Faust: supercollider.cpp: sc_api_version = %d\n", sc_api_version);
#endif

    if (name.empty()) {
        // Catch empty name
        Print("Faust [supercollider.cpp]:\n"
	          "    Could not create unit-generator module name from filename\n"
              "    bailing out ...\n");
        return;
    }

    if (strncmp(name.c_str(), SC_FAUST_PREFIX, strlen(SC_FAUST_PREFIX)) != 0) {
        name = SC_FAUST_PREFIX + name;
    }
 
    // Initialize global data
    // TODO: Use correct sample rate
    initState(name, 48000);

    // Register ugen
    (*ft->fDefineUnit)(
        (char*)name.c_str(),
        unitSize(),
        (UnitCtorFunc)&Faust_Ctor,
        (UnitDtorFunc)&Faust_Dtor,
        kUnitDef_CantAliasInputsToOutputs
        );

#if !defined(NDEBUG)
    Print("Faust: %s numControls=%d\n", name.c_str(), g_numControls);
#endif // NDEBUG
}

#ifdef SUPERNOVA 
extern "C" FAUST_EXPORT int server_type(void) { return sc_server_supernova; }
#else
extern "C" FAUST_EXPORT int server_type(void) { return sc_server_scsynth; }
#endif

// EOF

#endif
