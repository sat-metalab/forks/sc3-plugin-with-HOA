HOADecSATOswD3o{

classvar <>speakerPositions;
classvar <>sweeterPositions;
classvar <>speakerLabels;
classvar <>sweetSpot;

*initClass { 
speakerPositions = [
					Cartesian(0.0, 0.0, 8.94),  //top
					Cartesian(1.47, 5.48, 6.91),  //high
					Cartesian(-4.01, 4.01, 6.91),  //high
					Cartesian(-5.48, -1.47, 6.91),  //high
					Cartesian(-1.47, -5.48, 6.91),  //high
					Cartesian(4.01, -4.01, 6.91),  //high
					Cartesian(5.48, 1.47, 6.91),  //high
					Cartesian(0.0, 7.88, 4.21),  //middle
					Cartesian(-3.94, 6.83, 4.21),  //middle
					Cartesian(-6.83, 3.94, 4.21),  //middle
					Cartesian(-7.88, 0.0, 4.21),  //middle
					Cartesian(-6.83, -3.94, 4.21),  //middle
					Cartesian(-3.94, -6.83, 4.21),  //middle
					Cartesian(0.0, -7.88, 4.21),  //middle
					Cartesian(3.94, -6.83, 4.21),  //middle
					Cartesian(6.83, -3.94, 4.21),  //middle
					Cartesian(7.88, 0.0, 4.21),  //middle
					Cartesian(6.83, 3.94, 4.21),  //middle
					Cartesian(3.94, 6.83, 4.21),  //middle
					Cartesian(0.0, 8.9, -0.88),  //bottom
					Cartesian(-4.45, 7.71, -0.88),  //bottom
					Cartesian(-7.71, 4.45, -0.88),  //bottom
					Cartesian(-8.9, 0.0, -0.88),  //bottom
					Cartesian(-7.71, -4.45, -0.88),  //bottom
					Cartesian(-4.45, -7.71, -0.88),  //bottom
					Cartesian(0.0, -8.9, -0.88),  //bottom
					Cartesian(4.45, -7.71, -0.88),  //bottom
					Cartesian(7.71, -4.45, -0.88),  //bottom
					Cartesian(8.9, 0.0, -0.88),  //bottom
					Cartesian(7.71, 4.45, -0.88),  //bottom
					Cartesian(4.45, 7.71, -0.88)  //bottom
					];

sweeterPositions = [
					Cartesian(0.0, 0.0, 9.025),  //top
					Cartesian(1.47, 5.48, 6.995),  //high
					Cartesian(-4.01, 4.009, 6.995),  //high
					Cartesian(-5.48, -1.47, 6.995),  //high
					Cartesian(-1.47, -5.48, 6.995),  //high
					Cartesian(4.009, -4.01, 6.995),  //high
					Cartesian(5.48, 1.47, 6.995),  //high
					Cartesian(0.0, 7.88, 4.295),  //middle
					Cartesian(-3.94, 6.83, 4.295),  //middle
					Cartesian(-6.83, 3.94, 4.295),  //middle
					Cartesian(-7.88, 0.0, 4.295),  //middle
					Cartesian(-6.83, -3.94, 4.295),  //middle
					Cartesian(-3.94, -6.83, 4.295),  //middle
					Cartesian(0.0, -7.88, 4.295),  //middle
					Cartesian(3.94, -6.83, 4.295),  //middle
					Cartesian(6.83, -3.94, 4.295),  //middle
					Cartesian(7.88, 0.0, 4.295),  //middle
					Cartesian(6.83, 3.94, 4.295),  //middle
					Cartesian(3.94, 6.83, 4.295),  //middle
					Cartesian(0.0, 8.9, -0.795),  //bottom
					Cartesian(-4.45, 7.71, -0.795),  //bottom
					Cartesian(-7.71, 4.45, -0.795),  //bottom
					Cartesian(-8.9, 0.0, -0.795),  //bottom
					Cartesian(-7.71, -4.45, -0.795),  //bottom
					Cartesian(-4.45, -7.71, -0.795),  //bottom
					Cartesian(0.0, -8.9, -0.795),  //bottom
					Cartesian(4.45, -7.71, -0.795),  //bottom
					Cartesian(7.71, -4.45, -0.795),  //bottom
					Cartesian(8.9, 0.0, -0.795),  //bottom
					Cartesian(7.71, 4.45, -0.795),  //bottom
					Cartesian(4.45, 7.71, -0.795)  //bottom
					];

speakerLabels = [
					"top",
					"high",
					"high",
					"high",
					"high",
					"high",
					"high",
					"middle",
					"middle",
					"middle",
					"middle",
					"middle",
					"middle",
					"middle",
					"middle",
					"middle",
					"middle",
					"middle",
					"middle",
					"bottom",
					"bottom",
					"bottom",
					"bottom",
					"bottom",
					"bottom",
					"bottom",
					"bottom",
					"bottom",
					"bottom",
					"bottom",
					"bottom"
					];

sweetSpot = Cartesian(-0.001, -0.001, -0.086);

}

*ar { |order, in, gain(-10.0), lf_hf(0.0), mute(0.0), xover(400.0)|
// declare variables for the b-format array in
// distribute the channels from the array over in1 ... inN
// return the Ugen with the b-format channels and with the args from the *ar method
case
{order == 1}
	{ var in1, in2, in3, in4; 
	#in1,in2, in3, in4 = in; 
	^FaustSATOswD3o1.ar(in1, in2, in3, in4, gain, lf_hf, mute, xover)}

{order == 2}
	{ var in1, in2, in3, in4, in5, in6, in7, in8, in9; 
	#in1, in2, in3, in4, in5, in6, in7, in8, in9 = in; 
	^FaustSATOswD3o2.ar(in1, in2, in3, in4, in5, in6, in7, in8, in9, gain, lf_hf, mute, xover)}

{order == 3}
	{ var in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16; 
	#in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16 = in; 
	^FaustSATOswD3o3.ar(in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16, gain, lf_hf, mute, xover)}

{order == 4}
	{ var in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16, in17, in18, in19, in20, in21, in22, in23, in24, in25; 
	#in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16, in17, in18, in19, in20, in21, in22, in23, in24, in25 = in; 
	^FaustSATOswD3o4.ar(in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16, in17, in18, in19, in20, in21, in22, in23, in24, in25, gain, lf_hf, mute, xover)} 

{order == 5}
	{var in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16, in17, in18, in19, in20, in21, in22, in23, in24, in25, in26, in27, in28, in29, in30, in31, in32, in33, in34, in35, in36; 
	#in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16, in17, in18, in19, in20, in21, in22, in23, in24, in25, in26, in27, in28, in29, in30, in31, in32, in33, in34, in35, in36 = in; 
	^FaustSATOswD3o5.ar(in1,in2, in3, in4,in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16, in17, in18, in19, in20, in21, in22, in23, in24, in25, in26, in27, in28, in29, in30, in31, in32, in33, in34, in35, in36, gain, lf_hf, mute, xover)} 

{"Order 5 is not implemented for HOADecSATOswD3o".postln} 
 } 



}