// Faust Decoder Configuration File
// Written by Ambisonic Decoder Toolbox, version 8.0
// run by floriangrond on florian.local (MACI64) at 08-Feb-2018 13:19:02

//------- decoder information -------
// decoder file = /Users/floriangrond/Documents/PROJECTS/2017-SATie/adt/decoders/SATOsw0o3.dsp
// description = SATOsw0o_3h3p_allrad_5200_rE_max_2_band
// speaker array name = SATOsw0o
// horizontal order   = 3
// vertical order     = 3
// coefficient order  = acn
// coefficient scale  = N3D
// input scale        = N3D
// mixed-order scheme = HP
// input channel order: W Y Z X V T R S U Q O M K L N P 
// output speaker order: top high high high high high high middle middle middle middle middle middle middle middle middle middle middle middle bottom bottom bottom bottom bottom bottom bottom bottom bottom bottom bottom bottom 
//-------


// start decoder configuration
declare name	"SATOsw0o3";

// bands
nbands = 2;

// decoder type
decoder_type = 2;

// crossover frequency in Hz
xover_freq = hslider("xover [unit:Hz]",400,200,800,20): dezipper;

// lfhf_balance
lfhf_ratio = hslider("lf/hf [unit:dB]", 0, -3, +3, 0.1): mu.db2linear : dezipper;


// decoder order
decoder_order = 3;

// ambisonic order of each input component
co = ( 0, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3);

// use full or reduced input set
input_full_set = 1;

// delay compensation
delay_comp = 1;

// level compensation
level_comp = 1;

// nfc on input or output
nfc_output = 1;
nfc_input  = 0;

// enable output gain and muting controls
output_gain_muting = 1;

// number of speakers
ns = 31;

// radius for each speaker in meters
rs = (         6.025,         6.939,         6.937,         6.939,         6.939,         6.937,         6.939,         7.986,         7.991,         7.991,         7.986,         7.991,         7.991,         7.986,         7.991,         7.991,         7.986,         7.991,         7.991,         9.675,         9.677,         9.677,         9.675,         9.677,         9.677,         9.675,         9.677,         9.677,         9.675,         9.677,         9.677);

// per order gains, 0 for LF, 1 for HF.
//  Used to implement shelf filters, or to modify velocity matrix
//  for max_rE decoding, and so forth.  See Appendix A of BLaH6.
gamma(0) = (             1,             1,             1,             1);
gamma(1) = (             1,  0.8611363116,  0.6123336207,   0.304746985);

// gain matrix
s(00,0) = (   0.095194254,    -4.803e-06,  0.1445524256,    4.7097e-06,   1.98605e-05,   -7.1335e-06,  0.1414608142,    6.6425e-06,    1.1495e-06,   -4.7333e-06,   3.67071e-05,     -5.64e-06,  0.1056630243,     4.449e-06,    1.9603e-06,   -5.7029e-06);
s(01,0) = (  0.0579515627,  0.0640498521,  0.0632267641,  0.0215868397,   0.028679231,  0.0842565064,  0.0191042147,  0.0264974904, -0.0353156196, -0.0120159222,  0.0387602353,  0.0532056241, -0.0242027956,  0.0135681547, -0.0523547635,  -0.022670103);
s(02,0) = (  0.0633188241,  0.0527678378,  0.0669136521, -0.0527719248, -0.0488329755,  0.0668091537,   0.016114934, -0.0668209366,   -1.6118e-06,  0.0171615643, -0.0685905923,  0.0378993542, -0.0300105835, -0.0379156051,    5.2301e-06,  0.0171731571);
s(03,0) = (  0.0525917108, -0.0156096198,  0.0595454232, -0.0582364888,  0.0205203495, -0.0212301395,  0.0220957512, -0.0792069105,  0.0355290717, -0.0175366672,  0.0304364222, -0.0145489926, -0.0183884118, -0.0542868615,  0.0526964621, -0.0175308289);
s(04,0) = (  0.0579424412,   -0.06404604,   0.063209936,  -0.021601654,  0.0287043645,  -0.084239285,  0.0190891708, -0.0265128078, -0.0353211985,  0.0120212429,  0.0387873564, -0.0531799955,  -0.024201741, -0.0135731234, -0.0523548197,   0.022697173);
s(05,0) = (  0.0633107226, -0.0527561433,  0.0669162653,  0.0527649003, -0.0488342443, -0.0668111786,  0.0161357334,  0.0668131659,   1.16084e-05, -0.0171904031, -0.0685903355, -0.0379246782, -0.0299918047,  0.0379130996,    5.8118e-06, -0.0171739707);
s(06,0) = (  0.0525877148,  0.0156095505,  0.0595348628,  0.0582405673,  0.0205246414,  0.0212266495,  0.0220793998,  0.0792068473,  0.0355353416,  0.0175386924,  0.0304406293,   0.014541285, -0.0184011621,  0.0542752635,  0.0527098138,  0.0175272776);
s(07,0) = (  0.0296740126,  0.0479171477,  0.0110148918, -0.0027132114, -0.0049932463,   0.021871731, -0.0249028359, -0.0025010385,  -0.047650515, -0.0437610453,  -0.005370927, -0.0273077907, -0.0189718368,  0.0002590172, -0.0239363012,  0.0062439795);
s(08,0) = (  0.0324214197,   0.046018837,  0.0023991029, -0.0275209875, -0.0479108164,  0.0040258521, -0.0318393203, -0.0030558501, -0.0257446906,  0.0023591667, -0.0061355474, -0.0347099749, -0.0043378645,  0.0203794717, -0.0018090599,  0.0503847857);
s(09,0) = (  0.0267503599,  0.0206746772,  0.0036567988, -0.0392839579, -0.0375389174,  0.0049591357, -0.0261093924, -0.0059567087,  0.0259169071,  0.0430174369, -0.0095528041, -0.0149666641, -0.0069347494,  0.0294152746,  0.0018619662, -0.0053875652);
s(10,0) = (  0.0296655136,  0.0027224306,  0.0110227171, -0.0479010213, -0.0050101369,  0.0025074865, -0.0248913868, -0.0218859966,  0.0476305892,  0.0062652633, -0.0053833167, -0.0002608786, -0.0189852077,  0.0272907463,  0.0239483901, -0.0437391969);
s(11,0) = (  0.0410089989, -0.0355539863,  0.0084694051, -0.0561628585,  0.0591501614, -0.0104484081, -0.0363615801, -0.0130950413,  0.0283936239, -0.0584637769,  0.0181888243,  0.0211702051, -0.0137279349,  0.0361540297,  0.0042991209,  0.0062338301);
s(12,0) = (  0.0267459106, -0.0400981951,  0.0036629025,  -0.019250078,  0.0358814329, -0.0065973174,  -0.026103564, -0.0038739057,  -0.028776222,  0.0100898367,  0.0080725739,  0.0296988008, -0.0069422899,  0.0144589522, -0.0044570757,  0.0430146191);
s(13,0) = (  0.0296611206, -0.0478933057,  0.0110191146,  0.0027152261, -0.0049927344, -0.0218761281, -0.0248842425,  0.0025117834, -0.0476244873,  0.0437380589, -0.0053896185,   0.027282489,  -0.018973539, -0.0002482786, -0.0239343906, -0.0062374385);
s(14,0) = (  0.0267536501, -0.0392920154,  0.0036679192,  0.0206729663, -0.0375401977, -0.0059804289, -0.0261131572,  0.0049646974, -0.0259301593,  0.0054021545, -0.0095689419,  0.0294207829, -0.0069561433, -0.0149665644, -0.0018898678, -0.0430268298);
s(15,0) = (  0.0324291781, -0.0275247868,  0.0023975141,    0.04603108, -0.0479180029, -0.0030466651, -0.0318467568,  0.0040280123,  0.0257554345, -0.0503935464, -0.0061250506,  0.0203805257, -0.0043340975, -0.0347193496,  0.0018260766, -0.0023518533);
s(16,0) = (  0.0296692294, -0.0027127872,  0.0110191759,  0.0479075442, -0.0049914102, -0.0025006173, -0.0248947357,  0.0218786053,  0.0476387049, -0.0062404728, -0.0053676969,  0.0002575533, -0.0189775307, -0.0272957448,  0.0239409442,  0.0437487808);
s(17,0) = (  0.0410131779,  0.0355585847,  0.0084602073,  0.0561676441,  0.0591560893,  0.0104403627, -0.0363653363,  0.0130772727,  0.0283958667,  0.0584710073,  0.0181694907,   -0.02116959,  -0.013711062, -0.0361602338,  0.0042862002, -0.0062323269);
s(18,0) = (  0.0267503937,  0.0401038384,  0.0036724501,  0.0192545934,  0.0358876552,  0.0066143898, -0.0261082875,  0.0038843069, -0.0287773928,  -0.010086818,  0.0080917782, -0.0297035658, -0.0069640651, -0.0144611291, -0.0044661841, -0.0430178847);
s(19,0) = (  0.0466528147,  0.0668016026, -0.0337675055,  -1.07286e-05,  -1.71022e-05, -0.0550882631, -0.0130083255,    6.8551e-06, -0.0606702402, -0.0512112659,    8.2044e-06,  0.0005648958,  0.0250918185,   -3.3376e-06,  0.0537228291,   1.86639e-05);
s(20,0) = (  0.0344887879,   0.040639871, -0.0313559136, -0.0234596872, -0.0359448801, -0.0434272007, -0.0005990994,  0.0250726205, -0.0207615015,   -1.2337e-05,   0.041801068,  0.0143792908,  0.0202015552, -0.0083058107,  0.0241348412,  0.0352554684);
s(21,0) = (  0.0406066409,  0.0308367897, -0.0325823598, -0.0479086174, -0.0471394452,  -0.026958226, -0.0068094328,  0.0452136445,    0.02060343,   0.043254769,  0.0450634065,   0.002399101,  0.0226365345, -0.0085149706, -0.0239610212,  0.0083175286);
s(22,0) = (  0.0466445903,    -9.959e-07, -0.0337603989,  -0.066795533,     1.849e-06,     2.556e-07, -0.0130111391,  0.0550849005,  0.0606719357,   -3.0747e-06,     7.285e-07,     1.215e-07,  0.0250976389, -0.0005662956, -0.0537225815, -0.0512235415);
s(23,0) = (  0.0344950377, -0.0234640702, -0.0313628915, -0.0406417954,  0.0359500686,  0.0250729264, -0.0005873421,  0.0434282628,   0.020757083, -0.0352582801, -0.0417956219, -0.0083109274,  0.0201849783, -0.0143922051, -0.0241336562,    -6.927e-07);
s(24,0) = (  0.0405882601, -0.0478971482, -0.0325675351, -0.0308167663,  0.0471209574,   0.045203268, -0.0068121593,  0.0269464071, -0.0206162043, -0.0082933775, -0.0450532331, -0.0085099794,  0.0226408647, -0.0023992199,  0.0239630708,  0.0432581505);
s(25,0) = (  0.0466346817, -0.0667844288, -0.0337536428,    -3.206e-06,    5.7152e-06,  0.0550859995, -0.0130164142,    1.0758e-06, -0.0606563891,  0.0512015382,    -7.641e-07,  -0.000570559,  0.0251128492,      3.63e-07,  0.0537262307,    7.5451e-06);
s(26,0) = (  0.0405937777, -0.0478976806,  -0.032571465,  0.0308232105, -0.0471277415,  0.0452032601, -0.0068080059, -0.0269425106, -0.0206062663, -0.0083108951,  0.0450488669, -0.0085125775,  0.0226290848,  0.0023905996,  0.0239670768, -0.0432544375);
s(27,0) = (  0.0345001942, -0.0234596314, -0.0313677849,  0.0406512715, -0.0359471583,  0.0250699246, -0.0005850464, -0.0434349051,  0.0207735671, -0.0352645935,  0.0417933045, -0.0083114607,  0.0201795632,  0.0143922286, -0.0241428455,    2.2771e-05);
s(28,0) = (  0.0466337546,    7.6848e-06, -0.0337466361,  0.0667851888,   1.38393e-05,   -3.0128e-06, -0.0130227739, -0.0550744638,  0.0606603692,   1.68932e-05,   -4.4749e-06,    -9.436e-07,  0.0251081444,  0.0005578728, -0.0537186356,  0.0512077688);
s(29,0) = (   0.034487176,  0.0234466159, -0.0313701631,   0.040625145,  0.0359259988, -0.0250631396, -0.0005570375, -0.0434222569,  0.0207571601,  0.0352444985, -0.0417816312,  0.0083172478,  0.0201503726,  0.0144056254, -0.0241350665,   1.74662e-05);
s(30,0) = (  0.0405861338,  0.0478948401, -0.0325662228,  0.0308185246,  0.0471193876, -0.0452092181, -0.0068152029, -0.0269478486, -0.0206080102,  0.0083010781,  -0.045056999,  0.0085199964,  0.0226506933,  0.0023985584,   0.023966009, -0.0432474562);


// ----- do not change anything below here ----

// mask for full ambisonic set to channels in use
input_mask(0) = bus(nc);
input_mask(1) = (_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_);


// Start of decoder implementation.  No user serviceable parts below here!
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//declare name		"Fill in name in configuration section below";
declare version 	"1.2";
declare author 		"AmbisonicDecoderToolkit";
declare license 	"BSD 3-Clause License";
declare copyright	"(c) Aaron J. Heller 2013";

/*
Copyright (c) 2013, Aaron J. Heller
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*
  Author: Aaron J. Heller <heller@ai.sri.com>
  $Id: 21810b615fa65b96af41a7c8783d7435b41084b8 $
*/

// v 1.2, 28-Oct-2017 ajh
//  . add 6th-order NFC filters
//  . fixed error in speaker_chain and speaker_chain2, where speaker 
//    distance was being indexed by order, not speaker number

// v 1.1 remove dependancies on Faust Libraries, 23-Nov-2016 ajh
//   m = library("math.lib");
//   mu = library("music.lib");

m = environment {
  // from the old math.lib
  take (1, (xs, xxs)) = xs;
  take (1, xs) = xs;
  take (nn, (xs, xxs)) = take (nn-1, xxs);

  bus(2) = _,_; // avoids a lot of "bus(1)" labels in block diagrams
  bus(n) = par(i, n, _);

  SR = min(192000, max(1, fconstant(int fSamplingFreq, <math.h>)));
  //PI = 3.1415926535897932385;
  // quad precision value
  PI = 3.14159265358979323846264338327950288;
};

mu = environment {
   // from the old music.lib
   db2linear(x)	= pow(10, x/20.0);
};



// m.SR from math.lib is an integer, we need a float
SR = float(m.SR);

// descriptive statistics
total(c) = c:>_;

average(c) = total(c)/count(c);

count(c) = R(c) :>_ with {
 R((c,cl)) = R(c),(R(cl));
 R(c)      = 1;
};

rms(c) = R(c) :> /(count(c)) : sqrt with {
 R((c,cl)) = R(c),R(cl);
 R(c)      = (c*c);
};

sup(c) = R(c) with {
 R((c,cl)) = max(R(c),R(cl));
 R(c)      = c;
};

inf(c) = R(c) with {
 R((c,cl)) = min(R(c),R(cl));
 R(c)      = c;
};

// bus
bus(n)   = par(i,n,_);

// bus with gains
gain(c) = R(c) with {
  R((c,cl)) = R(c),R(cl);
  R(1)      = _;
  R(0)      = !:0;  // need to preserve number of outputs, faust will optimize away
  R(float(0)) = R(0);
  R(float(1)) = R(1);
  R(c)      = *(c);
};

// fir filter  (new improved, better behaved)
fir(c) = R(c) :>_ with {
  R((c,lc)) = _<: R(c), (mem:R(lc));
  R(c)      = gain(c);
};

// --- phase-matched bandsplitter from BLaH3
xover(freq,n) = par(i,n,xover1) with {

	sub(x,y) = y-x;

	k = tan(m.PI*float(freq)/m.SR);
	k2 = k^2;
	d =  (k2 + 2*k + 1);
	//d = (k2,2*k,1):>_;
	// hf numerator
	b_hf = (1/d,-2/d,1/d);
	// lf numerator
	b_lf = (k2/d, 2*k2/d, k2/d);
	// denominator
	a = (2 * (k2-1) / d, (k2 - 2*k + 1) / d);
	// 
	xover1 = _:sub ~ fir(a) <: fir(b_lf),fir(b_hf):_,*(-1);
};

shelf(freq,g_lf,g_hf) = xover(freq,1) : gain(g_lf),gain(g_hf):>_;

// from http://old.nabble.com/Re%3A--Faudiostream-devel---ANN--Faust-0.9.24-p28597267.html
//   (not used currently, do we need to worry about denormals?)
anti_denormal = pow(10,-20);
anti_denormal_ac = 1 - 1' : *(anti_denormal) : + ~ *(-1); 

// UI "dezipper"
smooth(c) = *(1-c) : +~*(c);
dezipper = smooth(0.999);

// physical constants     

temp_celcius = 20;                        
c = 331.3 * sqrt(1.0 + (temp_celcius/273.15)); // speed of sound m/s


// ---- NFC filters ----
//  see BesselPoly.m for coefficient calculations
//
// [1] J. Daniel, “Spatial Sound Encoding Including Near Field Effect:
//     Introducing Distance Coding Filters and a Viable, New Ambisonic 
//     Format ,” Preprints 23rd AES International Conference, Copenhagen,
//     2003.
// [2] Weisstein, Eric W. "Bessel Polynomial." From MathWorld--A Wolfram 
//     Web Resource. http://mathworld.wolfram.com/BesselPolynomial.html
// [3] F. Adriaensen, “Near Field filters for Higher Order
//     Ambisonics,” Jul. 2006.
// [4] J. O. Smith, “Digital State-Variable Filters,” CCRMA, May 2013.
//
// [5] "A Filter Primer", https://www.maximintegrated.com/en/app-notes/index.mvp/id/733

// first and second order state variable filters see [4]
//   FIXME FIXME this code needs to be refactored, so that the roots are 
//               passed in rather then hardwired in the code, or another 
//               API layer, or ...
svf1(g,d1)    = _ : *(g) :       (+      <: +~_, _ ) ~ *(d1)                   : !,_ ;
svf2(g,d1,d2) = _ : *(g) : (((_,_,_):> _ <: +~_, _ ) ~ *(d1) : +~_, _) ~ *(d2) : !,_ ;

//  these are Bessel filters, see [1,2]
nfc1(r,gain) = svf1(g,d1)  // r in meters
 with {
   omega = c/(float(r)*SR);
   //  1  1
   b1 = omega/2.0;
   g1 = 1.0 + b1;
   d1 = 0.0 - (2.0 * b1) / g1;
   g = gain/g1;
};

nfc2(r,gain) = svf2(g,d1,d2)
 with {
   omega = c/(float(r)*SR);
   r1 = omega/2.0;
   r2 = r1 * r1;

   // 1.000000000000000   3.00000000000000   3.00000000000000
   b1 = 3.0 * r1;
   b2 = 3.0 * r2;
   g2 = 1.0 + b1 + b2;

   d1 = 0.0 - (2.0 * b1 + 4.0 * b2) / g2;  // fixed
   d2 = 0.0 - (4.0 * b2) / g2;
   g = gain/g2;
};

nfc3(r,gain) = svf2(g,d1,d2):svf1(1.0,d3)
 with {
   omega = c/(float(r)*SR);

   r1 = omega/2.0;
   r2 = r1 * r1;

   // 1.000000000000000   3.677814645373914   6.459432693483369
   b1 = 3.677814645373914 * r1;
   b2 = 6.459432693483369 * r2;         
   g2 = 1.0 + b1 + b2;
   d1 = 0.0 - (2.0 * b1 + 4.0 * b2) / g2;  // fixed
   d2 = 0.0 - (4.0 * b2) / g2;

   // 1.000000000000000   2.322185354626086
   b3 = 2.322185354626086 * r1;
   g3 = 1.0 + b3;
   d3 = 0.0 - (2.0 * b3) / g3;

   g = gain/(g3*g2);
};

nfc4(r,gain) = svf2(g,d1,d2):svf2(1.0,d3,d4)
 with {
   omega = c/(float(r)*SR);

   r1 = omega/2.0;
   r2 = r1 * r1;
   
   // 1.000000000000000   4.207578794359250  11.487800476871168
   b1 =  4.207578794359250 * r1;
   b2 = 11.487800476871168 * r2;         
   g2 = 1.0 + b1 + b2;
   d1 = 0.0 - (2.0 * b1 + 4.0 * b2) / g2;  // fixed
   d2 = 0.0 - (4.0 * b2) / g2;

   // 1.000000000000000   5.792421205640748   9.140130890277934
   b3 = 5.792421205640748 * r1;
   b4 = 9.140130890277934 * r2;         
   g3 = 1.0 + b3 + b4;
   d3 = 0.0 - (2.0 * b3 + 4.0 * b4) / g3;  // fixed
   d4 = 0.0 - (4.0 * b4) / g3;
   
   g = gain/(g3*g2);
};

nfc5(r,gain) = svf2(g,d1,d2):svf2(1.0,d3,d4):svf1(1.0,d5)
 with {
   omega = c/(float(r)*SR);

   r1 = omega/2.0;
   r2 = r1 * r1;
   
   // 1.000000000000000   4.649348606363304  18.156315313452325
   b1 =  4.649348606363304 * r1;
   b2 = 18.156315313452325 * r2;         
   g2 = 1.0 + b1 + b2;
   d1 = 0.0 - (2.0 * b1 + 4.0 * b2) / g2;  // fixed
   d2 = 0.0 - (4.0 * b2) / g2;

   // 1.000000000000000   6.703912798306966  14.272480513279568
   b3 =  6.703912798306966 * r1;
   b4 = 14.272480513279568 * r2;         
   g3 = 1.0 + b3 + b4;
   d3 = 0.0 - (2.0 * b3 + 4 * b4) / g3;  // fixed
   d4 = 0.0 - (4.0 * b4) / g3;

   // 1.000000000000000   3.646738595329718
   b5 = 3.646738595329718 * r1;
   g4 = 1.0 + b5;
   d5 = 0.0 - (2.0 * b5) / g4;

   g = gain/(g4*g3*g2);
 };

nfc6(r,gain) = svf2(g,d11,d12):svf2(1.0,d21,d22):svf2(1.0,d31,d32)
with {
   omega = c/(float(r)*SR);

   r1 = omega/2.0;
   r2 = r1 * r1;

   // reverse Bessel Poly 6:
   //   1          21         210        1260        4725       10395       10395
   // factors:
   //   1.000000000000000   5.031864495621642  26.514025344067996
   //   1.000000000000000   7.471416712651683  20.852823177396761
   //   1.000000000000000   8.496718791726696  18.801130589570320


   // 1.000000000000000   5.031864495621642  26.514025344067996
   b11 =  5.031864495621642 * r1;
   b12 = 26.514025344067996 * r2;         
   g1 = 1.0 + b11 + b12;
   d11 = 0.0 - (2.0 * b11 + 4.0 * b12) / g1;
   d12 = 0.0 - (4.0 * b12) / g1;

   // 1.000000000000000   7.471416712651683  20.852823177396761
   b21 =  7.471416712651683 * r1;
   b22 = 20.852823177396761 * r2;         
   g2 = 1.0 + b21 + b22;
   d21 = 0.0 - (2.0 * b21 + 4.0 * b22) / g2;
   d22 = 0.0 - (4.0 * b22) / g2;

   // 1.000000000000000   8.496718791726696  18.801130589570320
   b31 =  8.496718791726696 * r1;
   b32 = 18.801130589570320 * r2;         
   g3 = 1.0 + b31 + b32;
   d31 = 0.0 - (2.0 * b31 + 4.0 * b32) / g3;
   d32 = 0.0 - (4.0 * b32) / g3;

   g = gain/(g3*g2*g1);
};


// ---- End NFC filters ----

nfc(0,r,g) = gain(g);
nfc(1,r,g) = nfc1(r,g);
nfc(2,r,g) = nfc2(r,g);
nfc(3,r,g) = nfc3(r,g);
nfc(4,r,g) = nfc4(r,g);
nfc(5,r,g) = nfc5(r,g);
nfc(6,r,g) = nfc6(r,g);

// null NFC filters to allow very high order decoders. FIXME!
nfc(o,r,g) = gain(g);

//declare name "nfctest";
//process = bus(6):(nfc(0,2,1),nfc(1,2,1),nfc(2,2,1),nfc(3,2,1),nfc(4,2,1),nfc(5,2,1)):bus(6);


// top level api to NFC filters
nfc_out(1,order,r,g) = nfc(order,r,g);
nfc_out(0,order,r,g) = _;

nfc_inp(1,order,r,g) = nfc(order,r,g);
nfc_inp(0,order,r,g) = _;



// ---- delay and level
delay(dc,r)  = R(dc,r) with {
 R(0,r) = _;  // delay_comp off
 R(1,0) = _;  // delay_comp on, but no delay
 R(1,float(0)) = R(1,0);
 R(1,r) = @(meters2samples(r));
 meters2samples(r) = int(m.SR * (float(r)/float(c)) + 0.5);
};

level(lc,r,rmax) = R(lc,r,rmax) with{
 R(0,r,rmax) = _;  // level_comp off
 R(1,r,rmax) = gain(float(r)/float(rmax));
};


delay_level(r) = R(r) with {  // delay_comp and level_comp are free variables (fix?)
 R((r,rl)) =   R(r), R(rl);
 R(r)      =   delay(delay_comp,(r_max-r)) : level(level_comp,r,r_max);
};

// ---- gates
gate(0) = !;
gate(1) = _;
dirac(i,j) = i==j;
gate_bus(order,(o,oss)) = (gate(order==o),gate_bus(order,oss));
gate_bus(order,o)       =  gate(order==o);


// route (not used)
route(n_inputs,n_outputs,outs) = m.bus(n_inputs)
                               <: par(i,n_outputs,(0,gate_bus(i,outs)):>_)
                               : m.bus(n_outputs);

//process = route(4,4,(3,1,1,2)); // test


// deinterleave 
deinterleave(n,span) = par(i,n,_) <: par(i,span, par(j,n,gate(%(j,span)==i)));


// 1 band speaker chain
speaker_chain(ispkr) = gain(s(ispkr,0))  // decoder matrix gains
		     // iterate over orders, select each order from matrix
		     <: par(jord,no,gate_bus(jord,co)
		            // sum and apply NFC filter for that order
			    // at the speaker distance
		            :> nfc_out(nfc_output,jord,m.take(ispkr+1,rs),1.0))
		     // sum the orders
	             :>_;

// 1 band speaker chain -- bad definition
// speaker_chain(i) = gain(s(i,0)) <: par(i,no,gate_bus(i,co):>nfc_out(nfc_output,i,m.take(i+1,rs),1.0)):>_;

// near field correction at input, nfc_input = 1
nfc_input_bus(nc) = par(i,nc, nfc_inp(nfc_input, m.take(i+1,co), r_bar, 1.0));

// per order gains
gamma_bus(n) = par(i,nc, gain( m.take(m.take(i+1,co)+1, gamma(n))));

// output gain and muting
output_gain = hslider("gain [unit:dB]", -10, -30, +10, 1): mu.db2linear :*(checkbox("mute")<0.5): dezipper;

gain_muting_bus(0,n) = bus(n);
gain_muting_bus(1,n) = par(i,n,*(output_gain));


// one band decoder
decoder(1,nc,ns) = nfc_input_bus(nc) 
                :  gamma_bus(0) 
                <: par(is,ns, speaker_chain(is)) 
                : delay_level(rs); 


// two band decoder
//   there are two variants of the two-band decoder
//     1. classic, with shelf-filters and one matrix
//     2. vienna,  bandsplitting filters and separate LF and HF matricies.

// classic shelf filter decoder
decoder(2,nc,ns) = nfc_input_bus(nc) 
                :  par(i,nc, shelf(xover_freq,m.take(m.take(i+1,co)+1, gamma(0))/lfhf_ratio,
                                              m.take(m.take(i+1,co)+1, gamma(1))*lfhf_ratio))
                <: par(is,ns, speaker_chain(is)) 
                :  delay_level(rs);

// vienna decoder
//   FIXME FIXME  need to incorporate lfhf_ratio
decoder(3,nc,ns) = bus(nc)
                   : nfc_input_bus(nc) 
                   : xover(xover_freq,nc) : deinterleave(2*nc,2) 
                   : (gamma_bus(0),gamma_bus(1)) : bus(2*nc)
                   <: par(j, ns, speaker_chain2(j,nc))
                   : delay_level(rs)
; 
// 2 band speaker chain for vienna decoder
// i is speaker, j is order
speaker_chain2(i,nc) = gain(s(i,0)), gain(s(i,1))
                       :> bus(nc)
                       <: par(j,no,gate_bus(j,co):>nfc_out(nfc_output,j,m.take(i+1,rs),1.0))
                       :>_ ;

//process = speaker_chain2(1,16); // test



// --------------------------------------
//  things calculated from decoder config 

// maximum and average speaker distance
r_max = sup(rs);
r_bar = (rs :> float) / float(count(rs));

// number of ambisonic orders, including 0
no = decoder_order+1;

// number of input component signals
nc = count(co);



// the top-level process
process = input_mask(input_full_set):decoder(decoder_type,nc,ns):gain_muting_bus(output_gain_muting,ns);


// End of decoder implementation.  No user serviceable parts above here!
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//EOF!
